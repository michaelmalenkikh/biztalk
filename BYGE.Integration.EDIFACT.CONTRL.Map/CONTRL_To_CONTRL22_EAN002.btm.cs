namespace BYGE.Integration.EDIFACT.CONTRL.Map {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"Microsoft.BizTalk.Edi.BaseArtifacts.Edifact_ContrlSchema+Efact_Contrl_Root", typeof(global::Microsoft.BizTalk.Edi.BaseArtifacts.Edifact_ContrlSchema.Efact_Contrl_Root))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"Microsoft.BizTalk.Edi.BaseArtifacts.Edifact_ContrlSchema+Efact_Contrl_Root", typeof(global::Microsoft.BizTalk.Edi.BaseArtifacts.Edifact_ContrlSchema.Efact_Contrl_Root))]
    public sealed class CONTRL_To_CONTRL22_EAN002 : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var userCSharp"" version=""1.0"" xmlns:ns1=""http://schemas.microsoft.com/Edi/Edifact"" xmlns:ns0=""http://schemas.microsoft.com/Edi/EdifactServiceSchema"" xmlns:userCSharp=""http://schemas.microsoft.com/BizTalk/2003/userCSharp"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/ns1:Efact_Contrl_Root"" />
  </xsl:template>
  <xsl:template match=""/ns1:Efact_Contrl_Root"">
    <xsl:variable name=""var:v1"" select=""userCSharp:StringConcat(&quot;2&quot;)"" />
    <xsl:variable name=""var:v2"" select=""userCSharp:StringConcat(&quot;EAN002&quot;)"" />
    <ns1:Efact_Contrl_Root>
      <xsl:if test=""@IsGeneratedAck"">
        <xsl:attribute name=""IsGeneratedAck"">
          <xsl:value-of select=""@IsGeneratedAck"" />
        </xsl:attribute>
      </xsl:if>
      <ns0:UNH>
        <UNH1>
          <xsl:value-of select=""ns0:UNH/UNH1/text()"" />
        </UNH1>
        <UNH2>
          <UNH2.1>
            <xsl:value-of select=""ns0:UNH/UNH2/UNH2.1/text()"" />
          </UNH2.1>
          <UNH2.2>
            <xsl:value-of select=""$var:v1"" />
          </UNH2.2>
          <UNH2.3>
            <xsl:value-of select=""$var:v1"" />
          </UNH2.3>
          <UNH2.4>
            <xsl:value-of select=""ns0:UNH/UNH2/UNH2.4/text()"" />
          </UNH2.4>
          <UNH2.5>
            <xsl:value-of select=""$var:v2"" />
          </UNH2.5>
        </UNH2>
      </ns0:UNH>
      <UCI>
        <UCI1>
          <xsl:value-of select=""UCI/UCI1/text()"" />
        </UCI1>
        <UCI2>
          <UCI2.1>
            <xsl:value-of select=""UCI/UCI2/UCI2.1/text()"" />
          </UCI2.1>
          <xsl:if test=""UCI/UCI2/UCI2.2"">
            <UCI2.2>
              <xsl:value-of select=""UCI/UCI2/UCI2.2/text()"" />
            </UCI2.2>
          </xsl:if>
        </UCI2>
        <UCI3>
          <UCI3.1>
            <xsl:value-of select=""UCI/UCI3/UCI3.1/text()"" />
          </UCI3.1>
          <xsl:if test=""UCI/UCI3/UCI3.2"">
            <UCI3.2>
              <xsl:value-of select=""UCI/UCI3/UCI3.2/text()"" />
            </UCI3.2>
          </xsl:if>
        </UCI3>
        <UCI4>
          <xsl:value-of select=""UCI/UCI4/text()"" />
        </UCI4>
      </UCI>
      <ns0:UNT>
        <UNT1>
          <xsl:value-of select=""ns0:UNT/UNT1/text()"" />
        </UNT1>
        <UNT2>
          <xsl:value-of select=""ns0:UNT/UNT2/text()"" />
        </UNT2>
      </ns0:UNT>
    </ns1:Efact_Contrl_Root>
  </xsl:template>
  <msxsl:script language=""C#"" implements-prefix=""userCSharp""><![CDATA[
public string StringConcat(string param0)
{
   return param0;
}



]]></msxsl:script>
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"Microsoft.BizTalk.Edi.BaseArtifacts.Edifact_ContrlSchema+Efact_Contrl_Root";
        
        private const global::Microsoft.BizTalk.Edi.BaseArtifacts.Edifact_ContrlSchema.Efact_Contrl_Root _srcSchemaTypeReference0 = null;
        
        private const string _strTrgSchemasList0 = @"Microsoft.BizTalk.Edi.BaseArtifacts.Edifact_ContrlSchema+Efact_Contrl_Root";
        
        private const global::Microsoft.BizTalk.Edi.BaseArtifacts.Edifact_ContrlSchema.Efact_Contrl_Root _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [1];
                _SrcSchemas[0] = @"Microsoft.BizTalk.Edi.BaseArtifacts.Edifact_ContrlSchema+Efact_Contrl_Root";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"Microsoft.BizTalk.Edi.BaseArtifacts.Edifact_ContrlSchema+Efact_Contrl_Root";
                return _TrgSchemas;
            }
        }
    }
}
