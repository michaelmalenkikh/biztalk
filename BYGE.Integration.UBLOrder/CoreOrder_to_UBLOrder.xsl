<?xml version="1.0" encoding="UTF-16"?>
<!-- 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:var="http://schemas.microsoft.com/BizTalk/2003/var" exclude-result-prefixes="msxsl var s0" version="1.0" xmlns:ns0="urn:oasis:names:specification:ubl:schema:xsd:Order-2" xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:s0="http://byg-e.dk/schemas/v10" xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2">
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:var="http://schemas.microsoft.com/BizTalk/2003/var" exclude-result-prefixes="msxsl var s0 userCSharp" version="1.0" xmlns:ns0="urn:oasis:names:specification:ubl:schema:xsd:Order-2" xmlns:s0="http://byg-e.dk/schemas/v10" xmlns:userCSharp="http://schemas.microsoft.com/BizTalk/2003/userCSharp" xmlns="urn:oasis:names:specification:ubl:schema:xsd:Order-2" xmlns:ccts="urn:oasis:names:specification:ubl:schema:xsd:CoreComponentParameters-2" xmlns:sdt="urn:oasis:names:specification:ubl:schema:xsd:SpecializedDatatypes-2" xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2" xmlns:ext="urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2" xmlns:qdt="urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2" xmlns:udt="urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <xsl:template match="/">
    <xsl:apply-templates select="/s0:Order" />
  </xsl:template>
  <xsl:template match="/s0:Order">
    <Order>
      <cbc:UBLVersionID>
        <xsl:value-of select="UBLVersionID/text()" />
      </cbc:UBLVersionID>
      <cbc:CustomizationID>
        <xsl:value-of select="CustomizationID/text()" />
      </cbc:CustomizationID>
      <cbc:ProfileID>
        <xsl:if test="ProfileID/@schemeID">
          <xsl:attribute name="schemeID">
            <xsl:value-of select="ProfileID/@schemeID" />
          </xsl:attribute>
        </xsl:if>
        <xsl:if test="ProfileID/@schemeAgencyID">
          <xsl:attribute name="schemeAgencyID">
            <xsl:value-of select="ProfileID/@schemeAgencyID" />
          </xsl:attribute>
        </xsl:if>
        <xsl:value-of select="ProfileID/text()" />
      </cbc:ProfileID>
      <cbc:ID>
        <xsl:value-of select="OrderID/text()" />
      </cbc:ID>
      <xsl:if test="CopyIndicator">
        <cbc:CopyIndicator>
          <xsl:value-of select="CopyIndicator/text()" />
        </cbc:CopyIndicator>
      </xsl:if>
      <cbc:IssueDate>
        <xsl:value-of select="IssueDate/text()" />
      </cbc:IssueDate>
      <xsl:if test="Currency">
        <cbc:DocumentCurrencyCode>
          <xsl:value-of select="Currency/text()" />
        </cbc:DocumentCurrencyCode>
      </xsl:if>
      <xsl:if test="Note">
        <cbc:Note>
          <xsl:value-of select="Note/text()" />
        </cbc:Note>
      </xsl:if>
      <cac:BuyerCustomerParty>
        <cac:Party>
          <cbc:EndpointID>
            <xsl:if test="BuyerCustomerParty/SchemeID/Endpoint">
              <xsl:attribute name="schemeID">
                <xsl:value-of select="BuyerCustomerParty/SchemeID/Endpoint/text()" />
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="BuyerCustomerParty/SchemeAgencyID/Endpoint">
              <xsl:attribute name="schemeAgencyID">
                <xsl:value-of select="BuyerCustomerParty/SchemeAgencyID/Endpoint/text()" />
              </xsl:attribute>
            </xsl:if>
            <xsl:value-of select="BuyerCustomerParty/BuyerID/text()" />
          </cbc:EndpointID>
          <cac:PartyIdentification>
            <cbc:ID>
              <xsl:if test="BuyerCustomerParty/SchemeID/Party">
                <xsl:attribute name="schemeID">
                  <xsl:value-of select="BuyerCustomerParty/SchemeID/Party/text()" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="BuyerCustomerParty/SchemeAgencyID/Party">
                <xsl:attribute name="schemeAgencyID">
                  <xsl:value-of select="BuyerCustomerParty/SchemeAgencyID/Party/text()" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="BuyerCustomerParty/PartyID">
                <xsl:value-of select="BuyerCustomerParty/PartyID/text()" />
              </xsl:if>
            </cbc:ID>
          </cac:PartyIdentification>
          <xsl:for-each select="BuyerCustomerParty/Address">
            <cac:PartyName>
              <xsl:if test="Name">
                <cbc:Name>
                  <xsl:value-of select="Name/text()" />
                </cbc:Name>
              </xsl:if>
            </cac:PartyName>
          </xsl:for-each>
          <xsl:for-each select="BuyerCustomerParty/Address">
            <cac:PostalAddress>
              <cbc:AddressFormatCode>
                <xsl:if test="AddressFormatCode/@listID">
                  <xsl:attribute name="listID">
                    <xsl:value-of select="AddressFormatCode/@listID" />
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="AddressFormatCode/@listAgencyID">
                  <xsl:attribute name="listAgencyID">
                    <xsl:value-of select="AddressFormatCode/@listAgencyID" />
                  </xsl:attribute>
                </xsl:if>
                <xsl:value-of select="AddressFormatCode/text()" />
              </cbc:AddressFormatCode>
              <xsl:if test="Street">
                <cbc:StreetName>
                  <xsl:value-of select="Street/text()" />
                </cbc:StreetName>
              </xsl:if>
              <xsl:if test="Number">
                <cbc:BuildingNumber>
                  <xsl:value-of select="Number/text()" />
                </cbc:BuildingNumber>
              </xsl:if>
              <cbc:CityName>
                <xsl:value-of select="City/text()" />
              </cbc:CityName>
              <xsl:if test="PostalCode">
                <cbc:PostalZone>
                  <xsl:value-of select="PostalCode/text()" />
                </cbc:PostalZone>
              </xsl:if>
              
              <cac:Country>
                  <cbc:IdentificationCode>
              <xsl:if test="string-length(Country/text()) != 0">  
                    <xsl:value-of select="Country/text()" />
              </xsl:if> 
              <xsl:if test="string-length(Country/text()) = 0">  
                    <xsl:text>DK</xsl:text>
              </xsl:if>                   
                  </cbc:IdentificationCode>                                 
              </cac:Country>
              
            </cac:PostalAddress>
          </xsl:for-each>

          <xsl:if test="string-length(BuyerCustomerParty/CompanyID/text()) != 0">
            <cac:PartyLegalEntity>
              <cbc:CompanyID>
                <xsl:if test="BuyerCustomerParty/SchemeID/Company">
                  <xsl:attribute name="schemeID">
                    <xsl:value-of select="BuyerCustomerParty/SchemeID/Company/text()" />
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="BuyerCustomerParty/CompanyID">
                  <xsl:value-of select="BuyerCustomerParty/CompanyID/text()" />
                </xsl:if>
              </cbc:CompanyID>
            </cac:PartyLegalEntity>
          </xsl:if>

          <xsl:for-each select="BuyerCustomerParty/Concact">
            <cac:Contact>
              <cbc:ID>
                <xsl:if test="ID/@schemeID">
                  <xsl:attribute name="schemeID">
                    <xsl:value-of select="ID/@schemeID" />
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="ID/@schemeAgencyID">
                  <xsl:attribute name="schemeAgencyID">
                    <xsl:value-of select="ID/@schemeAgencyID" />
                  </xsl:attribute>
                </xsl:if>
                <xsl:value-of select="ID/text()" />
              </cbc:ID>
              <xsl:if test="Name">
                <cbc:Name>
                  <xsl:value-of select="Name/text()" />
                </cbc:Name>
              </xsl:if>
              <xsl:if test="Telephone">
                <cbc:Telephone>
                  <xsl:value-of select="Telephone/text()" />
                </cbc:Telephone>
              </xsl:if>
              <xsl:if test="Telefax">
                <cbc:Telefax>
                  <xsl:value-of select="Telefax/text()" />
                </cbc:Telefax>
              </xsl:if>
              <xsl:if test="E-mail">
                <cbc:ElectronicMail>
                  <xsl:value-of select="E-mail/text()" />
                </cbc:ElectronicMail>
              </xsl:if>
            </cac:Contact>
          </xsl:for-each>
        </cac:Party>
      </cac:BuyerCustomerParty>
      <cac:SellerSupplierParty>
        <cac:Party>
          <cbc:EndpointID>
            <xsl:if test="SupplierCustomerParty/SchemeID/Endpoint">
              <xsl:attribute name="schemeID">
                <xsl:value-of select="SupplierCustomerParty/SchemeID/Endpoint/text()" />
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="SupplierCustomerParty/SchemeAgencyID/Endpoint">
              <xsl:attribute name="schemeAgencyID">
                <xsl:value-of select="SupplierCustomerParty/SchemeAgencyID/Endpoint/text()" />
              </xsl:attribute>
            </xsl:if>
            <xsl:value-of select="SupplierCustomerParty/SellerID/text()" />
          </cbc:EndpointID>
          <cac:PartyIdentification>
            <cbc:ID>
              <xsl:if test="SupplierCustomerParty/SchemeID/Party">
                <xsl:attribute name="schemeID">
                  <xsl:value-of select="SupplierCustomerParty/SchemeID/Party/text()" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="SupplierCustomerParty/SchemeAgencyID/Party">
                <xsl:attribute name="schemeAgencyID">
                  <xsl:value-of select="SupplierCustomerParty/SchemeAgencyID/Party/text()" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="SupplierCustomerParty/PartyID">
                <xsl:value-of select="SupplierCustomerParty/PartyID/text()" />
              </xsl:if>
            </cbc:ID>
          </cac:PartyIdentification>
          <xsl:for-each select="SupplierCustomerParty/Address">
            <cac:PartyName>
              <xsl:if test="Name">
                <cbc:Name>
                  <xsl:value-of select="Name/text()" />
                </cbc:Name>
              </xsl:if>
            </cac:PartyName>
          </xsl:for-each>
          <xsl:for-each select="SupplierCustomerParty/Address">
            <cac:PostalAddress>
              <cbc:AddressFormatCode>
                <xsl:if test="AddressFormatCode/@listID">
                  <xsl:attribute name="listID">
                    <xsl:value-of select="AddressFormatCode/@listID" />
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="AddressFormatCode/@listAgencyID">
                  <xsl:attribute name="listAgencyID">
                    <xsl:value-of select="AddressFormatCode/@listAgencyID" />
                  </xsl:attribute>
                </xsl:if>
                <xsl:value-of select="AddressFormatCode/text()" />
              </cbc:AddressFormatCode>
              <xsl:if test="Street">
                <cbc:StreetName>
                  <xsl:value-of select="Street/text()" />
                </cbc:StreetName>
              </xsl:if>
              <xsl:if test="Number">
                <cbc:BuildingNumber>
                  <xsl:value-of select="Number/text()" />
                </cbc:BuildingNumber>
              </xsl:if>
              <xsl:if test="City">
                <cbc:CityName>
                  <xsl:value-of select="City/text()" />
                </cbc:CityName>
              </xsl:if>
              <xsl:if test="PostalCode">
                <cbc:PostalZone>
                  <xsl:value-of select="PostalCode/text()" />
                </cbc:PostalZone>
              </xsl:if>
              <cac:Country>
                  <cbc:IdentificationCode>
              <xsl:if test="string-length(Country/text()) != 0">  
                    <xsl:value-of select="Country/text()" />
              </xsl:if> 
              <xsl:if test="string-length(Country/text()) = 0">  
                    <xsl:text>DK</xsl:text>
              </xsl:if>                   
                  </cbc:IdentificationCode>                                 
              </cac:Country>
            </cac:PostalAddress>
          </xsl:for-each>
          <xsl:if test="string-length(SupplierCustomerParty/CompanyID/text()) != 0">
            <cac:PartyLegalEntity>
            <cbc:CompanyID>
              <xsl:if test="SupplierCustomerParty/SchemeID/Company">
                <xsl:attribute name="schemeID">
                  <xsl:value-of select="SupplierCustomerParty/SchemeID/Company/text()" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="SupplierCustomerParty/CompanyID">
                <xsl:value-of select="SupplierCustomerParty/CompanyID/text()" />
              </xsl:if>
            </cbc:CompanyID>
          </cac:PartyLegalEntity>
          </xsl:if>   
          <xsl:for-each select="SupplierCustomerParty/Concact">
            <cac:Contact>
              <cbc:ID>
                <xsl:if test="ID/@schemeID">
                  <xsl:attribute name="schemeID">
                    <xsl:value-of select="ID/@schemeID" />
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="ID/@schemeAgencyID">
                  <xsl:attribute name="schemeAgencyID">
                    <xsl:value-of select="ID/@schemeAgencyID" />
                  </xsl:attribute>
                </xsl:if>
                <xsl:value-of select="ID/text()" />
              </cbc:ID>
              <xsl:if test="Name">
                <cbc:Name>
                  <xsl:value-of select="Name/text()" />
                </cbc:Name>
              </xsl:if>
              <xsl:if test="Telephone">
                <cbc:Telephone>
                  <xsl:value-of select="Telephone/text()" />
                </cbc:Telephone>
              </xsl:if>
              <xsl:if test="Telefax">
                <cbc:Telefax>
                  <xsl:value-of select="Telefax/text()" />
                </cbc:Telefax>
              </xsl:if>
              <xsl:if test="E-mail">
                <cbc:ElectronicMail>
                  <xsl:value-of select="E-mail/text()" />
                </cbc:ElectronicMail>
              </xsl:if>
            </cac:Contact>
          </xsl:for-each>
        </cac:Party>
      </cac:SellerSupplierParty>
      <cac:AccountingCustomerParty>
        <cac:Party>
          <cbc:EndpointID>
            <xsl:if test="AccountingCustomer/SchemeID/Endpoint">
              <xsl:attribute name="schemeID">
                <xsl:value-of select="AccountingCustomer/SchemeID/Endpoint/text()" />
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="AccountingCustomer/SchemeAgencyID/Endpoint">
              <xsl:attribute name="schemeAgencyID">
                <xsl:value-of select="AccountingCustomer/SchemeAgencyID/Endpoint/text()" />
              </xsl:attribute>
            </xsl:if>
            <xsl:value-of select="AccountingCustomer/AccBuyerID/text()" />
          </cbc:EndpointID>
          <cac:PartyIdentification>
            <cbc:ID>
              <xsl:if test="AccountingCustomer/SchemeID/Party">
                <xsl:attribute name="schemeID">
                  <xsl:value-of select="AccountingCustomer/SchemeID/Party/text()" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="AccountingCustomer/SchemeAgencyID/Party">
                <xsl:attribute name="schemeAgencyID">
                  <xsl:value-of select="AccountingCustomer/SchemeAgencyID/Party/text()" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="AccountingCustomer/PartyID">
                <xsl:value-of select="AccountingCustomer/PartyID/text()" />
              </xsl:if>
            </cbc:ID>
          </cac:PartyIdentification>
          <xsl:for-each select="AccountingCustomer/Address">
            <cac:PartyName>
              <xsl:if test="Name">
                <cbc:Name>
                  <xsl:value-of select="Name/text()" />
                </cbc:Name>
              </xsl:if>
            </cac:PartyName>
          </xsl:for-each>
          <xsl:if test="string-length(AccountingCustomer/CompanyID/text()) != 0"> 
          <cac:PartyLegalEntity>
            <cbc:CompanyID>
              <xsl:if test="AccountingCustomer/SchemeID/Company">
                <xsl:attribute name="schemeID">
                  <xsl:value-of select="AccountingCustomer/SchemeID/Company/text()" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="AccountingCustomer/CompanyID">
                <xsl:value-of select="AccountingCustomer/CompanyID/text()" />
              </xsl:if>
            </cbc:CompanyID>
          </cac:PartyLegalEntity>
          </xsl:if>   
          <xsl:for-each select="AccountingCustomer/Address">
            <cac:PostalAddress>
              <cbc:AddressFormatCode>
                <xsl:if test="AddressFormatCode/@listID">
                  <xsl:attribute name="listID">
                    <xsl:value-of select="AddressFormatCode/@listID" />
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="AddressFormatCode/@listAgencyID">
                  <xsl:attribute name="listAgencyID">
                    <xsl:value-of select="AddressFormatCode/@listAgencyID" />
                  </xsl:attribute>
                </xsl:if>
                <xsl:value-of select="AddressFormatCode/text()" />
              </cbc:AddressFormatCode>
              <xsl:if test="Street">
                <cbc:StreetName>
                  <xsl:value-of select="Street/text()" />
                </cbc:StreetName>
              </xsl:if>
              <xsl:if test="Number">
                <cbc:BuildingNumber>
                  <xsl:value-of select="Number/text()" />
                </cbc:BuildingNumber>
              </xsl:if>
              <xsl:if test="City">
                <cbc:CityName>
                  <xsl:value-of select="City/text()" />
                </cbc:CityName>
              </xsl:if>
              <xsl:if test="PostalCode">
                <cbc:PostalZone>
                  <xsl:value-of select="PostalCode/text()" />
                </cbc:PostalZone>
              </xsl:if>
              <cac:Country>
                  <cbc:IdentificationCode>
              <xsl:if test="string-length(Country/text()) != 0">  
                    <xsl:value-of select="Country/text()" />
              </xsl:if> 
              <xsl:if test="string-length(Country/text()) = 0">  
                    <xsl:text>DK</xsl:text>
              </xsl:if>                   
                  </cbc:IdentificationCode>                                 
              </cac:Country>
            </cac:PostalAddress>
          </xsl:for-each>
          <xsl:for-each select="AccountingCustomer/Concact">
            <cac:Contact>
              <cbc:ID>
                <xsl:if test="ID/@schemeID">
                  <xsl:attribute name="schemeID">
                    <xsl:value-of select="ID/@schemeID" />
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="ID/@schemeAgencyID">
                  <xsl:attribute name="schemeAgencyID">
                    <xsl:value-of select="ID/@schemeAgencyID" />
                  </xsl:attribute>
                </xsl:if>
                <xsl:value-of select="ID/text()" />
              </cbc:ID>
              <xsl:if test="Name">
                <cbc:Name>
                  <xsl:value-of select="Name/text()" />
                </cbc:Name>
              </xsl:if>
              <xsl:if test="Telephone">
                <cbc:Telephone>
                  <xsl:value-of select="Telephone/text()" />
                </cbc:Telephone>
              </xsl:if>
              <xsl:if test="Telefax">
                <cbc:Telefax>
                  <xsl:value-of select="Telefax/text()" />
                </cbc:Telefax>
              </xsl:if>
              <xsl:if test="E-mail">
                <cbc:ElectronicMail>
                  <xsl:value-of select="E-mail/text()" />
                </cbc:ElectronicMail>
              </xsl:if>
            </cac:Contact>
          </xsl:for-each>
        </cac:Party>
      </cac:AccountingCustomerParty>
      <cac:Delivery>
        <xsl:for-each select="Delivery/DeliveryLocation">
          <cac:DeliveryLocation>
            
           <xsl:if test="string-length(SchemeID/Endpoint/text()) != 0">  
             <cbc:ID>
              <xsl:if test="SchemeID/Endpoint">
                <xsl:attribute name="schemeID">
                  <xsl:value-of select="SchemeID/Endpoint/text()" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="SchemeAgencyID/Endpoint">
                <xsl:attribute name="schemeAgencyID">
                  <xsl:value-of select="SchemeAgencyID/Endpoint/text()" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="DeliveryID">
                <xsl:value-of select="DeliveryID/text()" />
              </xsl:if>
            </cbc:ID>
           </xsl:if>
        <xsl:if test="string-length(Description/text()) != 0"> 
            <xsl:if test="Description">
              <cbc:Description>
                <xsl:value-of select="Description/text()" />
              </cbc:Description>
            </xsl:if>
          </xsl:if>
            <xsl:for-each select="Address">
              <cac:Address>
              
                <xsl:for-each select="AddressFormatCode">
                   <xsl:if test="string-length(./text()) != 0">   
                  <cbc:AddressFormatCode>
                    <xsl:if test="@listID">
                      <xsl:attribute name="listID">
                        <xsl:value-of select="@listID" />
                      </xsl:attribute>
                    </xsl:if>
                    <xsl:if test="@listAgencyID">
                      <xsl:attribute name="listAgencyID">
                        <xsl:value-of select="@listAgencyID" />
                      </xsl:attribute>
                    </xsl:if>
                    <xsl:value-of select="./text()" />
                  </cbc:AddressFormatCode>
                </xsl:if>
                </xsl:for-each>
                
                  <xsl:if test="string-length(Street/text()) != 0">    
                <xsl:if test="Street">
                  <cbc:StreetName>
                    <xsl:value-of select="Street/text()" />
                  </cbc:StreetName>
                </xsl:if>
                </xsl:if>
                
                
                <xsl:if test="string-length(Number/text()) != 0">  
                <xsl:if test="Number">
                  <cbc:BuildingNumber>
                    <xsl:value-of select="Number/text()" />
                  </cbc:BuildingNumber>
                </xsl:if>
                  </xsl:if>
                    
                <xsl:if test="string-length(MarkAttention/text()) != 0">  
                <xsl:if test="MarkAttention">
                  <cbc:MarkAttention>
                    <xsl:value-of select="MarkAttention/text()" />
                  </cbc:MarkAttention>
                </xsl:if>
                </xsl:if>  
                
                <xsl:if test="string-length(City/text()) != 0">  
                <xsl:if test="City">
                  <cbc:CityName>
                    <xsl:value-of select="City/text()" />
                  </cbc:CityName>
                </xsl:if>
               </xsl:if>
                  
               <xsl:if test="string-length(PostalCode/text()) != 0">   
                <xsl:if test="PostalCode">
                  <cbc:PostalZone>
                    <xsl:value-of select="PostalCode/text()" />
                  </cbc:PostalZone>
                </xsl:if>
               </xsl:if>
                 
                <cac:Country>
               
                    <cbc:IdentificationCode>
                      <xsl:if test="string-length(Country/text()) != 0">  
                      <xsl:value-of select="Country/text()" />
                      </xsl:if>  
                      <xsl:if test="string-length(Country/text()) = 0">  
                      <xsl:text>DK</xsl:text>
                      </xsl:if>  

                    </cbc:IdentificationCode>
                
                </cac:Country>
              </cac:Address>
            </xsl:for-each>
          </cac:DeliveryLocation>
        </xsl:for-each>
        <xsl:for-each select="Delivery/DeliveryInfo">
          <xsl:for-each select="DeliveryPeriod">
            <cac:RequestedDeliveryPeriod>
              <xsl:if test="StartDate">
                <cbc:StartDate>
                  <xsl:value-of select="StartDate/text()" />
                </cbc:StartDate>
              </xsl:if>
              <xsl:if test="EndDate">
                <cbc:EndDate>
                  <xsl:value-of select="EndDate/text()" />
                </cbc:EndDate>
              </xsl:if>
            </cac:RequestedDeliveryPeriod>
          </xsl:for-each>
        </xsl:for-each>
      </cac:Delivery>
      <xsl:for-each select="Delivery/DeliveryInfo">
        <xsl:for-each select="DeliveryTerms">
          <cac:DeliveryTerms>
            <xsl:if test="SpecialTerms">
              <cbc:SpecialTerms>
                <xsl:value-of select="SpecialTerms/text()" />
              </cbc:SpecialTerms>
            </xsl:if>
          </cac:DeliveryTerms>
        </xsl:for-each>
      </xsl:for-each>
      
        <xsl:for-each select="Lines">
         <xsl:for-each select="Line">
          <cac:OrderLine> 
            <cac:LineItem>
              <cbc:ID>
                <xsl:if test="SchemeID/LineID">
                  <xsl:attribute name="schemeID">
                    <xsl:value-of select="SchemeID/LineID/text()" />
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="SchemeAgencyID/LineID">
                  <xsl:attribute name="schemeAgencyID">
                    <xsl:value-of select="SchemeAgencyID/LineID/text()" />
                  </xsl:attribute>
                </xsl:if>
                <xsl:value-of select="LineNo/text()" />
              </cbc:ID>
              <xsl:if test="Note">
                <cbc:Note>
                  <xsl:value-of select="Note/text()" />
                </cbc:Note>
              </xsl:if>
              <cbc:Quantity>
                <xsl:for-each select="Quantity/Node">
                  <xsl:if test="UnitCode">
                    <xsl:attribute name="unitCode">
                      <xsl:value-of select="UnitCode/text()" />
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:value-of select="Quantity/text()" />
                </xsl:for-each>  
              </cbc:Quantity>
              <cac:Item>
                <xsl:if test="Item/Description">
                  <cbc:Description>
                    <xsl:value-of select="Item/Description/text()" />
                  </cbc:Description>
                </xsl:if>
                <xsl:if test="Item/Name">
                  <cbc:Name>
                    <xsl:value-of select="Item/Name/text()" />
                  </cbc:Name>
                </xsl:if>
              <xsl:if test="string-length(Item/BuyerItemID/text()) != 0">   
                <cac:BuyersItemIdentification>
                  <cbc:ID>
                    <xsl:if test="Item/SchemeID/BuyItemID">
                      <xsl:attribute name="schemeID">
                        <xsl:value-of select="Item/SchemeID/BuyItemID/text()" />
                      </xsl:attribute>
                    </xsl:if>
                    <xsl:if test="Item/SchemeAgencyID/BuyItemID">
                      <xsl:attribute name="schemeAgencyID">
                        <xsl:value-of select="Item/SchemeAgencyID/BuyItemID/text()" />
                      </xsl:attribute>
                    </xsl:if>
                    <xsl:if test="Item/BuyerItemID">
                      <xsl:value-of select="Item/BuyerItemID/text()" />
                    </xsl:if>
                  </cbc:ID>
                
                </cac:BuyersItemIdentification>
               </xsl:if>
                 
               <xsl:if test="string-length(Item/SellerItemID/text()) != 0">    
                <cac:SellersItemIdentification>
                  <cbc:ID>
                    <xsl:if test="Item/SchemeID/SelItemID">
                      <xsl:attribute name="schemeID">
                        <xsl:value-of select="Item/SchemeID/SelItemID/text()" />
                      </xsl:attribute>
                    </xsl:if>
                    <xsl:if test="Item/SchemeAgencyID/SelItemID">
                      <xsl:attribute name="schemeAgencyID">
                        <xsl:value-of select="Item/SchemeAgencyID/SelItemID/text()" />
                      </xsl:attribute>
                    </xsl:if>
                    <xsl:if test="Item/SellerItemID">
                      <xsl:value-of select="Item/SellerItemID/text()" />
                    </xsl:if>
                  </cbc:ID>
                </cac:SellersItemIdentification>
               </xsl:if>
                
                
               <xsl:if test="string-length(Item/StandardItemID/text()) != 0">     
                <cac:StandardItemIdentification>
                  <cbc:ID>
                    <xsl:if test="Item/SchemeID/StdItemID">
                      <xsl:attribute name="schemeID">
                        <xsl:value-of select="Item/SchemeID/StdItemID/text()" />
                      </xsl:attribute>
                    </xsl:if>
                    <xsl:if test="Item/SchemeAgencyID/StdItemID">
                      <xsl:attribute name="schemeAgencyID">
                        <xsl:value-of select="Item/SchemeAgencyID/StdItemID/text()" />
                      </xsl:attribute>
                    </xsl:if>
                    <xsl:if test="Item/StandardItemID">
                      <xsl:value-of select="Item/StandardItemID/text()" />
                    </xsl:if>
                  </cbc:ID>
                </cac:StandardItemIdentification>
               </xsl:if>
                 
               <xsl:if test="string-length(Item/CatalogueItemID/text()) != 0">  
                <cac:CatalogueItemIdentification>
                  <cbc:ID>
                    <xsl:if test="Item/SchemeID/CatItemID">
                      <xsl:attribute name="schemeID">
                        <xsl:value-of select="Item/SchemeID/CatItemID/text()" />
                      </xsl:attribute>
                    </xsl:if>
                    <xsl:if test="Item/SchemeAgencyID/CatItemID">
                      <xsl:attribute name="schemeAgencyID">
                        <xsl:value-of select="Item/SchemeAgencyID/CatItemID/text()" />
                      </xsl:attribute>
                    </xsl:if>
                    <xsl:if test="Item/CatalogueItemID">
                      <xsl:value-of select="Item/CatalogueItemID/text()" />
                    </xsl:if>
                  </cbc:ID>
                </cac:CatalogueItemIdentification>
               </xsl:if>
                 
               <xsl:if test="string-length(Item/AdditionalItemID/text()) != 0">   
                <cac:AdditionalItemIdentification>
                  <cbc:ID>
                    <xsl:if test="Item/SchemeID/AddItemId">
                      <xsl:attribute name="schemeID">
                        <xsl:value-of select="Item/SchemeID/AddItemId/text()" />
                      </xsl:attribute>
                    </xsl:if>
                    <xsl:if test="Item/SchemeAgencyID/AddItemId">
                      <xsl:attribute name="schemeAgencyID">
                        <xsl:value-of select="Item/SchemeAgencyID/AddItemId/text()" />
                      </xsl:attribute>
                    </xsl:if>
                    <xsl:if test="Item/AdditionalItemID">
                      <xsl:value-of select="Item/AdditionalItemID/text()" />
                    </xsl:if>
                  </cbc:ID>
                </cac:AdditionalItemIdentification>
               </xsl:if>
                 
              </cac:Item>
             </cac:LineItem>
           </cac:OrderLine>
          </xsl:for-each>
        </xsl:for-each>
      
    </Order>
  </xsl:template>
</xsl:stylesheet>