namespace BYGE.Integration.UBLOrder {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:Order-2",@"Order")]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"Order"})]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.UBLOrder.UBLOrder1", typeof(global::BYGE.Integration.UBLOrder.UBLOrder1))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.UBLOrder.UBLOrder2", typeof(global::BYGE.Integration.UBLOrder.UBLOrder2))]
    public sealed class UBLOrder : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xs:schema xmlns:cac=""urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"" xmlns:cbc=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" xmlns:sdt=""urn:oasis:names:specification:ubl:schema:xsd:SpecializedDatatypes-2"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:udt=""urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2"" xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" xmlns:java=""http://xml.apache.org/xslt/java"" xmlns:ccts=""urn:oasis:names:specification:ubl:schema:xsd:CoreComponentParameters-2"" attributeFormDefault=""unqualified"" elementFormDefault=""qualified"" targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:Order-2"" xmlns:xs=""http://www.w3.org/2001/XMLSchema"">
  <xs:import schemaLocation=""BYGE.Integration.UBLOrder.UBLOrder1"" namespace=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" />
  <xs:import schemaLocation=""BYGE.Integration.UBLOrder.UBLOrder2"" namespace=""urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"" />
  <xs:annotation>
    <xs:appinfo>
      <b:references>
        <b:reference targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" />
        <b:reference targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"" />
      </b:references>
    </xs:appinfo>
  </xs:annotation>
  <xs:element name=""Order"">
    <xs:complexType>
      <xs:sequence>
        <xs:element ref=""cbc:UBLVersionID"" />
        <xs:element ref=""cbc:CustomizationID"" />
        <xs:element ref=""cbc:ProfileID"" />
        <xs:element ref=""cbc:ID"" />
        <xs:element ref=""cbc:CopyIndicator"" />
        <xs:element ref=""cbc:IssueDate"" />
        <xs:element minOccurs=""0"" ref=""cbc:DocumentCurrencyCode"" />
        <xs:element minOccurs=""0"" ref=""cbc:Note"" />
        <xs:element ref=""cac:BuyerCustomerParty"" />
        <xs:element ref=""cac:SellerSupplierParty"" />
        <xs:element ref=""cac:AccountingCustomerParty"" />
        <xs:element ref=""cac:Delivery"" />
        <xs:element ref=""cac:DeliveryTerms"" />
        <xs:element maxOccurs=""unbounded"" ref=""cac:OrderLine"" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>
</xs:schema>";
        
        public UBLOrder() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [1];
                _RootElements[0] = "Order";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
    }
}
