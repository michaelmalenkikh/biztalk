namespace BYGE.Integration.UBLOrder {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"BuyerCustomerParty", @"SellerSupplierParty", @"AccountingCustomerParty", @"Delivery", @"DeliveryTerms", @"OrderLine"})]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.UBLOrder.UBLOrder1", typeof(global::BYGE.Integration.UBLOrder.UBLOrder1))]
    public sealed class UBLOrder2 : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xs:schema xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" xmlns:tns=""urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"" attributeFormDefault=""unqualified"" elementFormDefault=""qualified"" targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"" xmlns:xs=""http://www.w3.org/2001/XMLSchema"">
  <xs:import schemaLocation=""BYGE.Integration.UBLOrder.UBLOrder1"" namespace=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" />
  <xs:annotation>
    <xs:appinfo>
      <b:references>
        <b:reference targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" />
      </b:references>
    </xs:appinfo>
  </xs:annotation>
  <xs:element name=""BuyerCustomerParty"">
    <xs:complexType>
      <xs:sequence>
        <xs:element name=""Party"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:EndpointID"" />
              <xs:element name=""PartyIdentification"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q2=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q2:ID"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" name=""PartyName"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q3=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q3:Name"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" name=""PostalAddress"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q4=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q4:AddressFormatCode"" />
                    <xs:element xmlns:q5=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q5:StreetName"" />
                    <xs:element xmlns:q6=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q6:BuildingNumber"" />
                    <xs:element xmlns:q7=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q7:CityName"" />
                    <xs:element xmlns:q8=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q8:PostalZone"" />
                    <xs:element minOccurs=""0"" name=""Country"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element xmlns:q9=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q9:IdentificationCode"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" name=""PartyLegalEntity"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q10=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q10:CompanyID"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" name=""Contact"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q11=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q11:ID"" />
                    <xs:element xmlns:q12=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q12:Name"" />
                    <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q1:Telephone"" />
                    <xs:element xmlns:q2=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q2:Telefax"" />
                    <xs:element xmlns:q13=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q13:ElectronicMail"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name=""SellerSupplierParty"">
    <xs:complexType>
      <xs:sequence>
        <xs:element name=""Party"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q14=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q14:EndpointID"" />
              <xs:element name=""PartyIdentification"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q15=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q15:ID"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" name=""PartyName"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q16=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q16:Name"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" name=""PostalAddress"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q17=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q17:AddressFormatCode"" />
                    <xs:element xmlns:q18=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q18:StreetName"" />
                    <xs:element xmlns:q19=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q19:BuildingNumber"" />
                    <xs:element xmlns:q20=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q20:CityName"" />
                    <xs:element xmlns:q21=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q21:PostalZone"" />
                    <xs:element minOccurs=""0"" name=""Country"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:IdentificationCode"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" name=""PartyLegalEntity"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q22=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q22:CompanyID"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" name=""Contact"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q23=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q23:ID"" />
                    <xs:element xmlns:q2=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q2:Name"" />
                    <xs:element xmlns:q3=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q3:Telephone"" />
                    <xs:element xmlns:q4=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q4:Telefax"" />
                    <xs:element xmlns:q5=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q5:ElectronicMail"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name=""AccountingCustomerParty"">
    <xs:complexType>
      <xs:sequence>
        <xs:element name=""Party"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q24=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q24:EndpointID"" />
              <xs:element name=""PartyIdentification"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q25=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q25:ID"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" name=""PartyName"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q6=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q6:Name"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" name=""PartyLegalEntity"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q26=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q26:CompanyID"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" name=""PostalAddress"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q7=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q7:AddressFormatCode"" />
                    <xs:element xmlns:q8=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q8:StreetName"" />
                    <xs:element xmlns:q9=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q9:BuildingNumber"" />
                    <xs:element xmlns:q10=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q10:CityName"" />
                    <xs:element xmlns:q11=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q11:PostalZone"" />
                    <xs:element minOccurs=""0"" name=""Country"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element xmlns:q12=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q12:IdentificationCode"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" name=""Contact"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q13=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q13:ID"" />
                    <xs:element xmlns:q14=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q14:Name"" />
                    <xs:element xmlns:q15=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q15:Telephone"" />
                    <xs:element xmlns:q16=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q16:Telefax"" />
                    <xs:element xmlns:q17=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q17:ElectronicMail"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name=""Delivery"">
    <xs:complexType>
      <xs:sequence>
        <xs:element minOccurs=""0"" name=""DeliveryLocation"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q1:ID"" />
              <xs:element xmlns:q2=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q2:Description"" />
              <xs:element minOccurs=""0"" name=""Address"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q3=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q3:Name"" />
                    <xs:element xmlns:q4=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q4:AddressFormatCode"" />
                    <xs:element xmlns:q5=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q5:StreetName"" />
                    <xs:element xmlns:q6=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q6:BuildingNumber"" />
                    <xs:element xmlns:q7=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q7:MarkAttention"" />
                    <xs:element xmlns:q8=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q8:CityName"" />
                    <xs:element xmlns:q9=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q9:PostalZone"" />
                    <xs:element minOccurs=""0"" name=""Country"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element xmlns:q10=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q10:IdentificationCode"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name=""RequestedDeliveryPeriod"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q34=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q34:StartDate"" />
              <xs:element xmlns:q35=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q35:EndDate"" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name=""DeliveryTerms"">
    <xs:complexType>
      <xs:sequence>
        <xs:element xmlns:q36=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q36:SpecialTerms"" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name=""OrderLine"">
    <xs:complexType>
      <xs:sequence>
        <xs:element minOccurs=""1"" maxOccurs=""unbounded"" name=""LineItem"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q37=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q37:ID"" />
              <xs:element xmlns:q20=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q20:Note"" />
              <xs:element xmlns:q38=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q38:Quantity"" />
              <xs:element name=""Item"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q39=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q39:Description"" />
                    <xs:element xmlns:q21=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" ref=""q21:Name"" />
                    <xs:element minOccurs=""0"" name=""BuyersItemIdentification"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element xmlns:q41=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q41:ID"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" name=""SellersItemIdentification"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element xmlns:q42=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q42:ID"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" name=""StandardItemIdentification"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element xmlns:q43=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q43:ID"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" name=""CatalogueItemIdentification"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element xmlns:q44=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q44:ID"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" name=""AdditionalItemIdentification"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:ID"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
</xs:schema>";
        
        public UBLOrder2() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [6];
                _RootElements[0] = "BuyerCustomerParty";
                _RootElements[1] = "SellerSupplierParty";
                _RootElements[2] = "AccountingCustomerParty";
                _RootElements[3] = "Delivery";
                _RootElements[4] = "DeliveryTerms";
                _RootElements[5] = "OrderLine";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2",@"BuyerCustomerParty")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"BuyerCustomerParty"})]
        public sealed class BuyerCustomerParty : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public BuyerCustomerParty() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "BuyerCustomerParty";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2",@"SellerSupplierParty")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"SellerSupplierParty"})]
        public sealed class SellerSupplierParty : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public SellerSupplierParty() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "SellerSupplierParty";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2",@"AccountingCustomerParty")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"AccountingCustomerParty"})]
        public sealed class AccountingCustomerParty : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public AccountingCustomerParty() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "AccountingCustomerParty";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2",@"Delivery")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"Delivery"})]
        public sealed class Delivery : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public Delivery() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "Delivery";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2",@"DeliveryTerms")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"DeliveryTerms"})]
        public sealed class DeliveryTerms : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public DeliveryTerms() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "DeliveryTerms";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2",@"OrderLine")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"OrderLine"})]
        public sealed class OrderLine : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public OrderLine() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "OrderLine";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
    }
}
