namespace BYGE.Integration.Core.Invoice {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.Core.Invoice.Invoice", typeof(global::BYGE.Integration.Core.Invoice.Invoice))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.Core.Invoice.Invoice", typeof(global::BYGE.Integration.Core.Invoice.Invoice))]
    public sealed class CoreInvoice_to_CoreInvoice : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var"" version=""1.0"" xmlns:ns0=""http://byg-e.dk/schemas/v10"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/ns0:Invoice"" />
  </xsl:template>
  <xsl:template match=""/ns0:Invoice"">
    <ns0:Invoice>
      <ProfileID>
        <xsl:value-of select=""ProfileID/text()"" />
      </ProfileID>
      <InvoiceID>
        <xsl:value-of select=""InvoiceID/text()"" />
      </InvoiceID>
      <xsl:if test=""CopyIndicator"">
        <CopyIndicator>
          <xsl:value-of select=""CopyIndicator/text()"" />
        </CopyIndicator>
      </xsl:if>
      <xsl:if test=""UUID"">
        <UUID>
          <xsl:value-of select=""UUID/text()"" />
        </UUID>
      </xsl:if>
      <TestIndicator>
        <xsl:text>0</xsl:text>
      </TestIndicator>
      <IssueDate>
        <xsl:value-of select=""IssueDate/text()"" />
      </IssueDate>
      <InvoiceType>
        <xsl:value-of select=""InvoiceType/text()"" />
      </InvoiceType>
      <xsl:for-each select=""Note"">
        <Note>
          <xsl:value-of select=""./text()"" />
        </Note>
      </xsl:for-each>
      <xsl:for-each select=""NoteLoop"">
        <NoteLoop>
          <xsl:for-each select=""Node"">
            <Node>
              <xsl:for-each select=""NoteQualifier"">
                <NoteQualifier>
                  <xsl:value-of select=""./text()"" />
                </NoteQualifier>
              </xsl:for-each>
              <xsl:for-each select=""NoteFunction"">
                <NoteFunction>
                  <xsl:value-of select=""./text()"" />
                </NoteFunction>
              </xsl:for-each>
              <xsl:for-each select=""NoteLanguage"">
                <NoteLanguage>
                  <xsl:value-of select=""./text()"" />
                </NoteLanguage>
              </xsl:for-each>
              <xsl:for-each select=""Note"">
                <xsl:variable name=""var:v1"" select=""string(./@xsi:nil) = 'true'"" />
                <xsl:if test=""string($var:v1)='true'"">
                  <Note>
                    <xsl:attribute name=""xsi:nil"">
                      <xsl:value-of select=""'true'"" />
                    </xsl:attribute>
                  </Note>
                </xsl:if>
                <xsl:if test=""string($var:v1)='false'"">
                  <Note>
                    <xsl:value-of select=""./text()"" />
                  </Note>
                </xsl:if>
              </xsl:for-each>
              <xsl:value-of select=""./text()"" />
            </Node>
          </xsl:for-each>
          <xsl:value-of select=""./text()"" />
        </NoteLoop>
      </xsl:for-each>
      <xsl:if test=""Currency"">
        <Currency>
          <xsl:value-of select=""Currency/text()"" />
        </Currency>
      </xsl:if>
      <OrderReference>
        <OrderID>
          <xsl:value-of select=""OrderReference/OrderID/text()"" />
        </OrderID>
        <OrderID>
          <xsl:value-of select=""OrderReference/OrderID/text()"" />
        </OrderID>
        <xsl:if test=""OrderReference/SalesOrderID"">
          <SalesOrderID>
            <xsl:value-of select=""OrderReference/SalesOrderID/text()"" />
          </SalesOrderID>
        </xsl:if>
        <xsl:if test=""OrderReference/SalesOrderID"">
          <SalesOrderID>
            <xsl:value-of select=""OrderReference/SalesOrderID/text()"" />
          </SalesOrderID>
        </xsl:if>
        <xsl:if test=""OrderReference/OrderDate"">
          <OrderDate>
            <xsl:value-of select=""OrderReference/OrderDate/text()"" />
          </OrderDate>
        </xsl:if>
        <xsl:if test=""OrderReference/OrderDate"">
          <OrderDate>
            <xsl:value-of select=""OrderReference/OrderDate/text()"" />
          </OrderDate>
        </xsl:if>
        <xsl:if test=""OrderReference/SalesOrderDate"">
          <xsl:variable name=""var:v2"" select=""string(OrderReference/SalesOrderDate/@xsi:nil) = 'true'"" />
          <xsl:if test=""string($var:v2)='true'"">
            <SalesOrderDate>
              <xsl:attribute name=""xsi:nil"">
                <xsl:value-of select=""'true'"" />
              </xsl:attribute>
            </SalesOrderDate>
          </xsl:if>
          <xsl:if test=""string($var:v2)='false'"">
            <SalesOrderDate>
              <xsl:value-of select=""OrderReference/SalesOrderDate/text()"" />
            </SalesOrderDate>
          </xsl:if>
        </xsl:if>
        <xsl:if test=""OrderReference/CustomerReference"">
          <CustomerReference>
            <xsl:value-of select=""OrderReference/CustomerReference/text()"" />
          </CustomerReference>
        </xsl:if>
        <xsl:if test=""OrderReference/CustomerReference"">
          <CustomerReference>
            <xsl:value-of select=""OrderReference/CustomerReference/text()"" />
          </CustomerReference>
        </xsl:if>
        <xsl:value-of select=""OrderReference/text()"" />
      </OrderReference>
      <xsl:for-each select=""ExtraOrderReference"">
        <ExtraOrderReference>
          <xsl:for-each select=""Node"">
            <Node>
              <xsl:if test=""Code"">
                <Code>
                  <xsl:value-of select=""Code/text()"" />
                </Code>
              </xsl:if>
              <xsl:if test=""Reference"">
                <Reference>
                  <xsl:value-of select=""Reference/text()"" />
                </Reference>
              </xsl:if>
              <xsl:value-of select=""./text()"" />
            </Node>
          </xsl:for-each>
          <xsl:value-of select=""./text()"" />
        </ExtraOrderReference>
      </xsl:for-each>
      <xsl:for-each select=""DTMReference"">
        <DTMReference>
          <xsl:for-each select=""Node"">
            <Node>
              <xsl:if test=""Code"">
                <Code>
                  <xsl:value-of select=""Code/text()"" />
                </Code>
              </xsl:if>
              <xsl:if test=""TimeStamp"">
                <TimeStamp>
                  <xsl:value-of select=""TimeStamp/text()"" />
                </TimeStamp>
              </xsl:if>
              <xsl:value-of select=""./text()"" />
            </Node>
          </xsl:for-each>
          <xsl:value-of select=""./text()"" />
        </DTMReference>
      </xsl:for-each>
      <xsl:for-each select=""BillingReference"">
        <BillingReference>
          <xsl:for-each select=""InvoiceDocumentReference"">
            <InvoiceDocumentReference>
              <xsl:if test=""ID"">
                <ID>
                  <xsl:value-of select=""ID/text()"" />
                </ID>
              </xsl:if>
              <xsl:if test=""IssueDate"">
                <xsl:variable name=""var:v3"" select=""string(IssueDate/@xsi:nil) = 'true'"" />
                <xsl:if test=""string($var:v3)='true'"">
                  <IssueDate>
                    <xsl:attribute name=""xsi:nil"">
                      <xsl:value-of select=""'true'"" />
                    </xsl:attribute>
                  </IssueDate>
                </xsl:if>
                <xsl:if test=""string($var:v3)='false'"">
                  <IssueDate>
                    <xsl:value-of select=""IssueDate/text()"" />
                  </IssueDate>
                </xsl:if>
              </xsl:if>
              <xsl:value-of select=""./text()"" />
            </InvoiceDocumentReference>
          </xsl:for-each>
          <xsl:value-of select=""./text()"" />
        </BillingReference>
      </xsl:for-each>
      <xsl:for-each select=""DespatchDocumentReference"">
        <DespatchDocumentReference>
          <ID>
            <xsl:value-of select=""ID/text()"" />
          </ID>
          <ID>
            <xsl:value-of select=""ID/text()"" />
          </ID>
          <xsl:value-of select=""./text()"" />
        </DespatchDocumentReference>
      </xsl:for-each>
      <xsl:for-each select=""AccountingSupplier"">
        <AccountingSupplier>
          <AccSellerID>
            <xsl:value-of select=""AccSellerID/text()"" />
          </AccSellerID>
          <AccSellerID>
            <xsl:value-of select=""AccSellerID/text()"" />
          </AccSellerID>
          <xsl:if test=""PartyID"">
            <xsl:variable name=""var:v4"" select=""string(PartyID/@xsi:nil) = 'true'"" />
            <xsl:if test=""string($var:v4)='true'"">
              <PartyID>
                <xsl:attribute name=""xsi:nil"">
                  <xsl:value-of select=""'true'"" />
                </xsl:attribute>
              </PartyID>
            </xsl:if>
            <xsl:if test=""string($var:v4)='false'"">
              <PartyID>
                <xsl:value-of select=""PartyID/text()"" />
              </PartyID>
            </xsl:if>
          </xsl:if>
          <xsl:if test=""PartyID"">
            <xsl:variable name=""var:v5"" select=""string(PartyID/@xsi:nil) = 'true'"" />
            <xsl:if test=""string($var:v5)='true'"">
              <PartyID>
                <xsl:attribute name=""xsi:nil"">
                  <xsl:value-of select=""'true'"" />
                </xsl:attribute>
              </PartyID>
            </xsl:if>
            <xsl:if test=""string($var:v5)='false'"">
              <PartyID>
                <xsl:value-of select=""PartyID/text()"" />
              </PartyID>
            </xsl:if>
          </xsl:if>
          <xsl:if test=""CompanyID"">
            <CompanyID>
              <xsl:value-of select=""CompanyID/text()"" />
            </CompanyID>
          </xsl:if>
          <xsl:if test=""CompanyID"">
            <CompanyID>
              <xsl:value-of select=""CompanyID/text()"" />
            </CompanyID>
          </xsl:if>
          <CompanyTaxID>
            <xsl:value-of select=""CompanyTaxID/text()"" />
          </CompanyTaxID>
          <CompanyTaxID>
            <xsl:value-of select=""CompanyTaxID/text()"" />
          </CompanyTaxID>
          <SchemeID>
            <xsl:if test=""SchemeID/Endpoint"">
              <Endpoint>
                <xsl:value-of select=""SchemeID/Endpoint/text()"" />
              </Endpoint>
            </xsl:if>
            <xsl:if test=""SchemeID/Endpoint"">
              <Endpoint>
                <xsl:value-of select=""SchemeID/Endpoint/text()"" />
              </Endpoint>
            </xsl:if>
            <xsl:if test=""SchemeID/Party"">
              <Party>
                <xsl:value-of select=""SchemeID/Party/text()"" />
              </Party>
            </xsl:if>
            <xsl:if test=""SchemeID/Party"">
              <Party>
                <xsl:value-of select=""SchemeID/Party/text()"" />
              </Party>
            </xsl:if>
            <xsl:if test=""SchemeID/Company"">
              <Company>
                <xsl:value-of select=""SchemeID/Company/text()"" />
              </Company>
            </xsl:if>
            <xsl:if test=""SchemeID/Company"">
              <Company>
                <xsl:value-of select=""SchemeID/Company/text()"" />
              </Company>
            </xsl:if>
            <xsl:if test=""SchemeID/CompanyTax"">
              <CompanyTax>
                <xsl:value-of select=""SchemeID/CompanyTax/text()"" />
              </CompanyTax>
            </xsl:if>
            <xsl:if test=""SchemeID/CompanyTax"">
              <CompanyTax>
                <xsl:value-of select=""SchemeID/CompanyTax/text()"" />
              </CompanyTax>
            </xsl:if>
            <xsl:value-of select=""SchemeID/text()"" />
          </SchemeID>
          <xsl:for-each select=""Address"">
            <Address>
              <xsl:if test=""Name"">
                <Name>
                  <xsl:value-of select=""Name/text()"" />
                </Name>
              </xsl:if>
              <xsl:if test=""Name"">
                <Name>
                  <xsl:value-of select=""Name/text()"" />
                </Name>
              </xsl:if>
              <xsl:if test=""Postbox"">
                <Postbox>
                  <xsl:value-of select=""Postbox/text()"" />
                </Postbox>
              </xsl:if>
              <xsl:if test=""Postbox"">
                <Postbox>
                  <xsl:value-of select=""Postbox/text()"" />
                </Postbox>
              </xsl:if>
              <xsl:if test=""AddressFormatCode"">
                <AddressFormatCode>
                  <xsl:value-of select=""AddressFormatCode/text()"" />
                </AddressFormatCode>
              </xsl:if>
              <xsl:if test=""AddressFormatCode"">
                <AddressFormatCode>
                  <xsl:value-of select=""AddressFormatCode/text()"" />
                </AddressFormatCode>
              </xsl:if>
              <xsl:if test=""Street"">
                <Street>
                  <xsl:value-of select=""Street/text()"" />
                </Street>
              </xsl:if>
              <xsl:if test=""Street"">
                <Street>
                  <xsl:value-of select=""Street/text()"" />
                </Street>
              </xsl:if>
              <xsl:if test=""Number"">
                <Number>
                  <xsl:value-of select=""Number/text()"" />
                </Number>
              </xsl:if>
              <xsl:if test=""Number"">
                <Number>
                  <xsl:value-of select=""Number/text()"" />
                </Number>
              </xsl:if>
              <xsl:if test=""AdditionalStreetName"">
                <AdditionalStreetName>
                  <xsl:value-of select=""AdditionalStreetName/text()"" />
                </AdditionalStreetName>
              </xsl:if>
              <xsl:if test=""AdditionalStreetName"">
                <AdditionalStreetName>
                  <xsl:value-of select=""AdditionalStreetName/text()"" />
                </AdditionalStreetName>
              </xsl:if>
              <xsl:if test=""Department"">
                <Department>
                  <xsl:value-of select=""Department/text()"" />
                </Department>
              </xsl:if>
              <xsl:if test=""Department"">
                <Department>
                  <xsl:value-of select=""Department/text()"" />
                </Department>
              </xsl:if>
              <xsl:if test=""City"">
                <City>
                  <xsl:value-of select=""City/text()"" />
                </City>
              </xsl:if>
              <xsl:if test=""City"">
                <City>
                  <xsl:value-of select=""City/text()"" />
                </City>
              </xsl:if>
              <xsl:if test=""PostalCode"">
                <PostalCode>
                  <xsl:value-of select=""PostalCode/text()"" />
                </PostalCode>
              </xsl:if>
              <xsl:if test=""PostalCode"">
                <PostalCode>
                  <xsl:value-of select=""PostalCode/text()"" />
                </PostalCode>
              </xsl:if>
              <xsl:if test=""Country"">
                <Country>
                  <xsl:value-of select=""Country/text()"" />
                </Country>
              </xsl:if>
              <xsl:if test=""Country"">
                <Country>
                  <xsl:value-of select=""Country/text()"" />
                </Country>
              </xsl:if>
              <xsl:value-of select=""./text()"" />
            </Address>
          </xsl:for-each>
          <xsl:for-each select=""Concact"">
            <Concact>
              <ID>
                <xsl:value-of select=""ID/text()"" />
              </ID>
              <ID>
                <xsl:value-of select=""ID/text()"" />
              </ID>
              <xsl:if test=""Name"">
                <Name>
                  <xsl:value-of select=""Name/text()"" />
                </Name>
              </xsl:if>
              <xsl:if test=""Name"">
                <Name>
                  <xsl:value-of select=""Name/text()"" />
                </Name>
              </xsl:if>
              <xsl:if test=""Telephone"">
                <Telephone>
                  <xsl:value-of select=""Telephone/text()"" />
                </Telephone>
              </xsl:if>
              <xsl:if test=""Telephone"">
                <Telephone>
                  <xsl:value-of select=""Telephone/text()"" />
                </Telephone>
              </xsl:if>
              <xsl:if test=""Telefax"">
                <Telefax>
                  <xsl:value-of select=""Telefax/text()"" />
                </Telefax>
              </xsl:if>
              <xsl:if test=""Telefax"">
                <Telefax>
                  <xsl:value-of select=""Telefax/text()"" />
                </Telefax>
              </xsl:if>
              <xsl:if test=""E-mail"">
                <E-mail>
                  <xsl:value-of select=""E-mail/text()"" />
                </E-mail>
              </xsl:if>
              <xsl:if test=""E-mail"">
                <E-mail>
                  <xsl:value-of select=""E-mail/text()"" />
                </E-mail>
              </xsl:if>
              <xsl:if test=""Note"">
                <Note>
                  <xsl:value-of select=""Note/text()"" />
                </Note>
              </xsl:if>
              <xsl:if test=""Note"">
                <Note>
                  <xsl:value-of select=""Note/text()"" />
                </Note>
              </xsl:if>
              <xsl:value-of select=""./text()"" />
            </Concact>
          </xsl:for-each>
          <xsl:for-each select=""RFFNADLoop"">
            <RFFNADLoop>
              <xsl:for-each select=""Node"">
                <Node>
                  <xsl:if test=""Code"">
                    <Code>
                      <xsl:value-of select=""Code/text()"" />
                    </Code>
                  </xsl:if>
                  <xsl:if test=""Reference"">
                    <Reference>
                      <xsl:value-of select=""Reference/text()"" />
                    </Reference>
                  </xsl:if>
                  <xsl:value-of select=""./text()"" />
                </Node>
              </xsl:for-each>
              <xsl:value-of select=""./text()"" />
            </RFFNADLoop>
          </xsl:for-each>
          <xsl:value-of select=""./text()"" />
        </AccountingSupplier>
      </xsl:for-each>
      <AccountingCustomer>
        <xsl:if test=""AccountingCustomer/SupplierAssignedAccountID"">
          <SupplierAssignedAccountID>
            <xsl:value-of select=""AccountingCustomer/SupplierAssignedAccountID/text()"" />
          </SupplierAssignedAccountID>
        </xsl:if>
        <xsl:if test=""AccountingCustomer/SupplierAssignedAccountID"">
          <SupplierAssignedAccountID>
            <xsl:value-of select=""AccountingCustomer/SupplierAssignedAccountID/text()"" />
          </SupplierAssignedAccountID>
        </xsl:if>
        <AccBuyerID>
          <xsl:value-of select=""AccountingCustomer/AccBuyerID/text()"" />
        </AccBuyerID>
        <AccBuyerID>
          <xsl:value-of select=""AccountingCustomer/AccBuyerID/text()"" />
        </AccBuyerID>
        <xsl:if test=""AccountingCustomer/PartyID"">
          <PartyID>
            <xsl:value-of select=""AccountingCustomer/PartyID/text()"" />
          </PartyID>
        </xsl:if>
        <xsl:if test=""AccountingCustomer/PartyID"">
          <PartyID>
            <xsl:value-of select=""AccountingCustomer/PartyID/text()"" />
          </PartyID>
        </xsl:if>
        <xsl:if test=""AccountingCustomer/CompanyID"">
          <CompanyID>
            <xsl:value-of select=""AccountingCustomer/CompanyID/text()"" />
          </CompanyID>
        </xsl:if>
        <xsl:if test=""AccountingCustomer/CompanyID"">
          <CompanyID>
            <xsl:value-of select=""AccountingCustomer/CompanyID/text()"" />
          </CompanyID>
        </xsl:if>
        <CompanyTaxID>
          <xsl:value-of select=""AccountingCustomer/CompanyTaxID/text()"" />
        </CompanyTaxID>
        <SchemeID>
          <xsl:if test=""AccountingCustomer/SchemeID/Endpoint"">
            <Endpoint>
              <xsl:value-of select=""AccountingCustomer/SchemeID/Endpoint/text()"" />
            </Endpoint>
          </xsl:if>
          <xsl:if test=""AccountingCustomer/SchemeID/Endpoint"">
            <Endpoint>
              <xsl:value-of select=""AccountingCustomer/SchemeID/Endpoint/text()"" />
            </Endpoint>
          </xsl:if>
          <xsl:if test=""AccountingCustomer/SchemeID/Party"">
            <Party>
              <xsl:value-of select=""AccountingCustomer/SchemeID/Party/text()"" />
            </Party>
          </xsl:if>
          <xsl:if test=""AccountingCustomer/SchemeID/Party"">
            <Party>
              <xsl:value-of select=""AccountingCustomer/SchemeID/Party/text()"" />
            </Party>
          </xsl:if>
          <xsl:if test=""AccountingCustomer/SchemeID/Company"">
            <Company>
              <xsl:value-of select=""AccountingCustomer/SchemeID/Company/text()"" />
            </Company>
          </xsl:if>
          <xsl:if test=""AccountingCustomer/SchemeID/Company"">
            <Company>
              <xsl:value-of select=""AccountingCustomer/SchemeID/Company/text()"" />
            </Company>
          </xsl:if>
          <xsl:value-of select=""AccountingCustomer/SchemeID/text()"" />
        </SchemeID>
        <xsl:for-each select=""AccountingCustomer/Address"">
          <Address>
            <xsl:if test=""Name"">
              <Name>
                <xsl:value-of select=""Name/text()"" />
              </Name>
            </xsl:if>
            <xsl:if test=""Name"">
              <Name>
                <xsl:value-of select=""Name/text()"" />
              </Name>
            </xsl:if>
            <xsl:if test=""AddressFormatCode"">
              <AddressFormatCode>
                <xsl:value-of select=""AddressFormatCode/text()"" />
              </AddressFormatCode>
            </xsl:if>
            <xsl:if test=""AddressFormatCode"">
              <AddressFormatCode>
                <xsl:value-of select=""AddressFormatCode/text()"" />
              </AddressFormatCode>
            </xsl:if>
            <xsl:if test=""Postbox"">
              <Postbox>
                <xsl:value-of select=""Postbox/text()"" />
              </Postbox>
            </xsl:if>
            <xsl:if test=""Postbox"">
              <Postbox>
                <xsl:value-of select=""Postbox/text()"" />
              </Postbox>
            </xsl:if>
            <xsl:if test=""Street"">
              <Street>
                <xsl:value-of select=""Street/text()"" />
              </Street>
            </xsl:if>
            <xsl:if test=""Street"">
              <Street>
                <xsl:value-of select=""Street/text()"" />
              </Street>
            </xsl:if>
            <xsl:if test=""Number"">
              <Number>
                <xsl:value-of select=""Number/text()"" />
              </Number>
            </xsl:if>
            <xsl:if test=""Number"">
              <Number>
                <xsl:value-of select=""Number/text()"" />
              </Number>
            </xsl:if>
            <xsl:if test=""AdditionalStreetName"">
              <AdditionalStreetName>
                <xsl:value-of select=""AdditionalStreetName/text()"" />
              </AdditionalStreetName>
            </xsl:if>
            <xsl:if test=""AdditionalStreetName"">
              <AdditionalStreetName>
                <xsl:value-of select=""AdditionalStreetName/text()"" />
              </AdditionalStreetName>
            </xsl:if>
            <xsl:if test=""Department"">
              <Department>
                <xsl:value-of select=""Department/text()"" />
              </Department>
            </xsl:if>
            <xsl:if test=""Department"">
              <Department>
                <xsl:value-of select=""Department/text()"" />
              </Department>
            </xsl:if>
            <xsl:if test=""City"">
              <City>
                <xsl:value-of select=""City/text()"" />
              </City>
            </xsl:if>
            <xsl:if test=""City"">
              <City>
                <xsl:value-of select=""City/text()"" />
              </City>
            </xsl:if>
            <xsl:if test=""PostalCode"">
              <PostalCode>
                <xsl:value-of select=""PostalCode/text()"" />
              </PostalCode>
            </xsl:if>
            <xsl:if test=""PostalCode"">
              <PostalCode>
                <xsl:value-of select=""PostalCode/text()"" />
              </PostalCode>
            </xsl:if>
            <xsl:if test=""Country"">
              <Country>
                <xsl:value-of select=""Country/text()"" />
              </Country>
            </xsl:if>
            <xsl:if test=""Country"">
              <Country>
                <xsl:value-of select=""Country/text()"" />
              </Country>
            </xsl:if>
            <xsl:value-of select=""./text()"" />
          </Address>
        </xsl:for-each>
        <xsl:for-each select=""AccountingCustomer/Concact"">
          <Concact>
            <ID>
              <xsl:value-of select=""ID/text()"" />
            </ID>
            <ID>
              <xsl:value-of select=""ID/text()"" />
            </ID>
            <xsl:if test=""Name"">
              <Name>
                <xsl:value-of select=""Name/text()"" />
              </Name>
            </xsl:if>
            <xsl:if test=""Name"">
              <Name>
                <xsl:value-of select=""Name/text()"" />
              </Name>
            </xsl:if>
            <xsl:if test=""Telephone"">
              <Telephone>
                <xsl:value-of select=""Telephone/text()"" />
              </Telephone>
            </xsl:if>
            <xsl:if test=""Telephone"">
              <Telephone>
                <xsl:value-of select=""Telephone/text()"" />
              </Telephone>
            </xsl:if>
            <xsl:if test=""Telefax"">
              <Telefax>
                <xsl:value-of select=""Telefax/text()"" />
              </Telefax>
            </xsl:if>
            <xsl:if test=""Telefax"">
              <Telefax>
                <xsl:value-of select=""Telefax/text()"" />
              </Telefax>
            </xsl:if>
            <xsl:if test=""E-mail"">
              <E-mail>
                <xsl:value-of select=""E-mail/text()"" />
              </E-mail>
            </xsl:if>
            <xsl:if test=""E-mail"">
              <E-mail>
                <xsl:value-of select=""E-mail/text()"" />
              </E-mail>
            </xsl:if>
            <xsl:if test=""Note"">
              <Note>
                <xsl:value-of select=""Note/text()"" />
              </Note>
            </xsl:if>
            <xsl:if test=""Note"">
              <Note>
                <xsl:value-of select=""Note/text()"" />
              </Note>
            </xsl:if>
            <xsl:value-of select=""./text()"" />
          </Concact>
        </xsl:for-each>
        <xsl:for-each select=""AccountingCustomer/RFFNADLoop"">
          <RFFNADLoop>
            <xsl:for-each select=""Node"">
              <Node>
                <xsl:if test=""Code"">
                  <Code>
                    <xsl:value-of select=""Code/text()"" />
                  </Code>
                </xsl:if>
                <xsl:if test=""Reference"">
                  <Reference>
                    <xsl:value-of select=""Reference/text()"" />
                  </Reference>
                </xsl:if>
                <xsl:value-of select=""./text()"" />
              </Node>
            </xsl:for-each>
            <xsl:value-of select=""./text()"" />
          </RFFNADLoop>
        </xsl:for-each>
        <xsl:value-of select=""AccountingCustomer/text()"" />
      </AccountingCustomer>
      <BuyerCustomer>
        <xsl:if test=""BuyerCustomer/BuyerID"">
          <BuyerID>
            <xsl:value-of select=""BuyerCustomer/BuyerID/text()"" />
          </BuyerID>
        </xsl:if>
        <xsl:if test=""BuyerCustomer/BuyerID"">
          <BuyerID>
            <xsl:value-of select=""BuyerCustomer/BuyerID/text()"" />
          </BuyerID>
        </xsl:if>
        <xsl:if test=""BuyerCustomer/PartyID"">
          <PartyID>
            <xsl:value-of select=""BuyerCustomer/PartyID/text()"" />
          </PartyID>
        </xsl:if>
        <xsl:if test=""BuyerCustomer/PartyID"">
          <PartyID>
            <xsl:value-of select=""BuyerCustomer/PartyID/text()"" />
          </PartyID>
        </xsl:if>
        <xsl:if test=""BuyerCustomer/CompanyID"">
          <CompanyID>
            <xsl:value-of select=""BuyerCustomer/CompanyID/text()"" />
          </CompanyID>
        </xsl:if>
        <xsl:if test=""BuyerCustomer/CompanyID"">
          <CompanyID>
            <xsl:value-of select=""BuyerCustomer/CompanyID/text()"" />
          </CompanyID>
        </xsl:if>
        <CompanyTaxID>
          <xsl:value-of select=""BuyerCustomer/CompanyTaxID/text()"" />
        </CompanyTaxID>
        <xsl:for-each select=""BuyerCustomer/SchemeID"">
          <SchemeID>
            <xsl:if test=""Endpoint"">
              <Endpoint>
                <xsl:value-of select=""Endpoint/text()"" />
              </Endpoint>
            </xsl:if>
            <xsl:if test=""Endpoint"">
              <Endpoint>
                <xsl:value-of select=""Endpoint/text()"" />
              </Endpoint>
            </xsl:if>
            <xsl:if test=""Party"">
              <Party>
                <xsl:value-of select=""Party/text()"" />
              </Party>
            </xsl:if>
            <xsl:if test=""Party"">
              <Party>
                <xsl:value-of select=""Party/text()"" />
              </Party>
            </xsl:if>
            <xsl:if test=""Company"">
              <Company>
                <xsl:value-of select=""Company/text()"" />
              </Company>
            </xsl:if>
            <xsl:if test=""Company"">
              <Company>
                <xsl:value-of select=""Company/text()"" />
              </Company>
            </xsl:if>
            <xsl:value-of select=""./text()"" />
          </SchemeID>
        </xsl:for-each>
        <xsl:for-each select=""BuyerCustomer/Address"">
          <Address>
            <Name>
              <xsl:value-of select=""Name/text()"" />
            </Name>
            <Name>
              <xsl:value-of select=""Name/text()"" />
            </Name>
            <xsl:if test=""AddressFormatCode"">
              <AddressFormatCode>
                <xsl:value-of select=""AddressFormatCode/text()"" />
              </AddressFormatCode>
            </xsl:if>
            <xsl:if test=""AddressFormatCode"">
              <AddressFormatCode>
                <xsl:value-of select=""AddressFormatCode/text()"" />
              </AddressFormatCode>
            </xsl:if>
            <xsl:if test=""Postbox"">
              <Postbox>
                <xsl:value-of select=""Postbox/text()"" />
              </Postbox>
            </xsl:if>
            <xsl:if test=""Postbox"">
              <Postbox>
                <xsl:value-of select=""Postbox/text()"" />
              </Postbox>
            </xsl:if>
            <xsl:if test=""Street"">
              <Street>
                <xsl:value-of select=""Street/text()"" />
              </Street>
            </xsl:if>
            <xsl:if test=""Street"">
              <Street>
                <xsl:value-of select=""Street/text()"" />
              </Street>
            </xsl:if>
            <xsl:if test=""Number"">
              <Number>
                <xsl:value-of select=""Number/text()"" />
              </Number>
            </xsl:if>
            <xsl:if test=""Number"">
              <Number>
                <xsl:value-of select=""Number/text()"" />
              </Number>
            </xsl:if>
            <xsl:if test=""AdditionalStreetName"">
              <AdditionalStreetName>
                <xsl:value-of select=""AdditionalStreetName/text()"" />
              </AdditionalStreetName>
            </xsl:if>
            <xsl:if test=""AdditionalStreetName"">
              <AdditionalStreetName>
                <xsl:value-of select=""AdditionalStreetName/text()"" />
              </AdditionalStreetName>
            </xsl:if>
            <xsl:if test=""Department"">
              <Department>
                <xsl:value-of select=""Department/text()"" />
              </Department>
            </xsl:if>
            <xsl:if test=""Department"">
              <Department>
                <xsl:value-of select=""Department/text()"" />
              </Department>
            </xsl:if>
            <xsl:if test=""City"">
              <City>
                <xsl:value-of select=""City/text()"" />
              </City>
            </xsl:if>
            <xsl:if test=""City"">
              <City>
                <xsl:value-of select=""City/text()"" />
              </City>
            </xsl:if>
            <xsl:if test=""PostalCode"">
              <PostalCode>
                <xsl:value-of select=""PostalCode/text()"" />
              </PostalCode>
            </xsl:if>
            <xsl:if test=""PostalCode"">
              <PostalCode>
                <xsl:value-of select=""PostalCode/text()"" />
              </PostalCode>
            </xsl:if>
            <xsl:if test=""Country"">
              <Country>
                <xsl:value-of select=""Country/text()"" />
              </Country>
            </xsl:if>
            <xsl:if test=""Country"">
              <Country>
                <xsl:value-of select=""Country/text()"" />
              </Country>
            </xsl:if>
            <xsl:value-of select=""./text()"" />
          </Address>
        </xsl:for-each>
        <xsl:for-each select=""BuyerCustomer/Concact"">
          <Concact>
            <ID>
              <xsl:value-of select=""ID/text()"" />
            </ID>
            <ID>
              <xsl:value-of select=""ID/text()"" />
            </ID>
            <xsl:if test=""Name"">
              <Name>
                <xsl:value-of select=""Name/text()"" />
              </Name>
            </xsl:if>
            <xsl:if test=""Name"">
              <Name>
                <xsl:value-of select=""Name/text()"" />
              </Name>
            </xsl:if>
            <xsl:if test=""Telephone"">
              <Telephone>
                <xsl:value-of select=""Telephone/text()"" />
              </Telephone>
            </xsl:if>
            <xsl:if test=""Telephone"">
              <Telephone>
                <xsl:value-of select=""Telephone/text()"" />
              </Telephone>
            </xsl:if>
            <xsl:if test=""Telefax"">
              <Telefax>
                <xsl:value-of select=""Telefax/text()"" />
              </Telefax>
            </xsl:if>
            <xsl:if test=""Telefax"">
              <Telefax>
                <xsl:value-of select=""Telefax/text()"" />
              </Telefax>
            </xsl:if>
            <xsl:if test=""E-mail"">
              <E-mail>
                <xsl:value-of select=""E-mail/text()"" />
              </E-mail>
            </xsl:if>
            <xsl:if test=""E-mail"">
              <E-mail>
                <xsl:value-of select=""E-mail/text()"" />
              </E-mail>
            </xsl:if>
            <xsl:if test=""Note"">
              <Note>
                <xsl:value-of select=""Note/text()"" />
              </Note>
            </xsl:if>
            <xsl:if test=""Note"">
              <Note>
                <xsl:value-of select=""Note/text()"" />
              </Note>
            </xsl:if>
            <xsl:value-of select=""./text()"" />
          </Concact>
        </xsl:for-each>
        <xsl:for-each select=""BuyerCustomer/RFFNADLoop"">
          <RFFNADLoop>
            <xsl:for-each select=""Node"">
              <Node>
                <xsl:if test=""Code"">
                  <Code>
                    <xsl:value-of select=""Code/text()"" />
                  </Code>
                </xsl:if>
                <xsl:if test=""Reference"">
                  <Reference>
                    <xsl:value-of select=""Reference/text()"" />
                  </Reference>
                </xsl:if>
                <xsl:value-of select=""./text()"" />
              </Node>
            </xsl:for-each>
            <xsl:value-of select=""./text()"" />
          </RFFNADLoop>
        </xsl:for-each>
        <xsl:value-of select=""BuyerCustomer/text()"" />
      </BuyerCustomer>
      <xsl:for-each select=""DeliveryLocation"">
        <DeliveryLocation>
          <xsl:if test=""DeliveryID"">
            <DeliveryID>
              <xsl:value-of select=""DeliveryID/text()"" />
            </DeliveryID>
          </xsl:if>
          <xsl:if test=""DeliveryID"">
            <DeliveryID>
              <xsl:value-of select=""DeliveryID/text()"" />
            </DeliveryID>
          </xsl:if>
          <xsl:if test=""PartyID"">
            <PartyID>
              <xsl:value-of select=""PartyID/text()"" />
            </PartyID>
          </xsl:if>
          <xsl:if test=""PartyID"">
            <PartyID>
              <xsl:value-of select=""PartyID/text()"" />
            </PartyID>
          </xsl:if>
          <xsl:if test=""CompanyID"">
            <CompanyID>
              <xsl:value-of select=""CompanyID/text()"" />
            </CompanyID>
          </xsl:if>
          <xsl:if test=""CompanyID"">
            <CompanyID>
              <xsl:value-of select=""CompanyID/text()"" />
            </CompanyID>
          </xsl:if>
          <xsl:for-each select=""SchemeID"">
            <SchemeID>
              <xsl:if test=""Endpoint"">
                <Endpoint>
                  <xsl:value-of select=""Endpoint/text()"" />
                </Endpoint>
              </xsl:if>
              <xsl:if test=""Endpoint"">
                <Endpoint>
                  <xsl:value-of select=""Endpoint/text()"" />
                </Endpoint>
              </xsl:if>
              <xsl:if test=""Party"">
                <Party>
                  <xsl:value-of select=""Party/text()"" />
                </Party>
              </xsl:if>
              <xsl:if test=""Party"">
                <Party>
                  <xsl:value-of select=""Party/text()"" />
                </Party>
              </xsl:if>
              <xsl:if test=""Company"">
                <Company>
                  <xsl:value-of select=""Company/text()"" />
                </Company>
              </xsl:if>
              <xsl:if test=""Company"">
                <Company>
                  <xsl:value-of select=""Company/text()"" />
                </Company>
              </xsl:if>
              <xsl:value-of select=""./text()"" />
            </SchemeID>
          </xsl:for-each>
          <xsl:for-each select=""Address"">
            <Address>
              <Name>
                <xsl:value-of select=""Name/text()"" />
              </Name>
              <Name>
                <xsl:value-of select=""Name/text()"" />
              </Name>
              <xsl:if test=""AddressFormatCode"">
                <AddressFormatCode>
                  <xsl:value-of select=""AddressFormatCode/text()"" />
                </AddressFormatCode>
              </xsl:if>
              <xsl:if test=""AddressFormatCode"">
                <AddressFormatCode>
                  <xsl:value-of select=""AddressFormatCode/text()"" />
                </AddressFormatCode>
              </xsl:if>
              <xsl:if test=""Street"">
                <Street>
                  <xsl:value-of select=""Street/text()"" />
                </Street>
              </xsl:if>
              <xsl:if test=""Street"">
                <Street>
                  <xsl:value-of select=""Street/text()"" />
                </Street>
              </xsl:if>
              <xsl:if test=""Number"">
                <Number>
                  <xsl:value-of select=""Number/text()"" />
                </Number>
              </xsl:if>
              <xsl:if test=""Number"">
                <Number>
                  <xsl:value-of select=""Number/text()"" />
                </Number>
              </xsl:if>
              <xsl:if test=""AdditionalStreetName"">
                <AdditionalStreetName>
                  <xsl:value-of select=""AdditionalStreetName/text()"" />
                </AdditionalStreetName>
              </xsl:if>
              <xsl:if test=""AdditionalStreetName"">
                <AdditionalStreetName>
                  <xsl:value-of select=""AdditionalStreetName/text()"" />
                </AdditionalStreetName>
              </xsl:if>
              <xsl:if test=""Department"">
                <Department>
                  <xsl:value-of select=""Department/text()"" />
                </Department>
              </xsl:if>
              <xsl:if test=""Department"">
                <Department>
                  <xsl:value-of select=""Department/text()"" />
                </Department>
              </xsl:if>
              <xsl:if test=""MarkAttention"">
                <MarkAttention>
                  <xsl:value-of select=""MarkAttention/text()"" />
                </MarkAttention>
              </xsl:if>
              <xsl:if test=""MarkAttention"">
                <MarkAttention>
                  <xsl:value-of select=""MarkAttention/text()"" />
                </MarkAttention>
              </xsl:if>
              <xsl:if test=""City"">
                <City>
                  <xsl:value-of select=""City/text()"" />
                </City>
              </xsl:if>
              <xsl:if test=""City"">
                <City>
                  <xsl:value-of select=""City/text()"" />
                </City>
              </xsl:if>
              <xsl:if test=""PostalCode"">
                <PostalCode>
                  <xsl:value-of select=""PostalCode/text()"" />
                </PostalCode>
              </xsl:if>
              <xsl:if test=""PostalCode"">
                <PostalCode>
                  <xsl:value-of select=""PostalCode/text()"" />
                </PostalCode>
              </xsl:if>
              <xsl:if test=""Country"">
                <Country>
                  <xsl:value-of select=""Country/text()"" />
                </Country>
              </xsl:if>
              <xsl:if test=""Country"">
                <Country>
                  <xsl:value-of select=""Country/text()"" />
                </Country>
              </xsl:if>
              <xsl:value-of select=""./text()"" />
            </Address>
          </xsl:for-each>
          <xsl:for-each select=""Concact"">
            <Concact>
              <ID>
                <xsl:value-of select=""ID/text()"" />
              </ID>
              <ID>
                <xsl:value-of select=""ID/text()"" />
              </ID>
              <xsl:if test=""Name"">
                <Name>
                  <xsl:value-of select=""Name/text()"" />
                </Name>
              </xsl:if>
              <xsl:if test=""Name"">
                <Name>
                  <xsl:value-of select=""Name/text()"" />
                </Name>
              </xsl:if>
              <xsl:if test=""Telephone"">
                <Telephone>
                  <xsl:value-of select=""Telephone/text()"" />
                </Telephone>
              </xsl:if>
              <xsl:if test=""Telephone"">
                <Telephone>
                  <xsl:value-of select=""Telephone/text()"" />
                </Telephone>
              </xsl:if>
              <xsl:if test=""Telefax"">
                <Telefax>
                  <xsl:value-of select=""Telefax/text()"" />
                </Telefax>
              </xsl:if>
              <xsl:if test=""Telefax"">
                <Telefax>
                  <xsl:value-of select=""Telefax/text()"" />
                </Telefax>
              </xsl:if>
              <xsl:if test=""E-mail"">
                <E-mail>
                  <xsl:value-of select=""E-mail/text()"" />
                </E-mail>
              </xsl:if>
              <xsl:if test=""E-mail"">
                <E-mail>
                  <xsl:value-of select=""E-mail/text()"" />
                </E-mail>
              </xsl:if>
              <xsl:if test=""Note"">
                <Note>
                  <xsl:value-of select=""Note/text()"" />
                </Note>
              </xsl:if>
              <xsl:if test=""Note"">
                <Note>
                  <xsl:value-of select=""Note/text()"" />
                </Note>
              </xsl:if>
              <xsl:value-of select=""./text()"" />
            </Concact>
          </xsl:for-each>
          <xsl:for-each select=""RFFNADLoop"">
            <RFFNADLoop>
              <xsl:for-each select=""Node"">
                <Node>
                  <xsl:if test=""Code"">
                    <Code>
                      <xsl:value-of select=""Code/text()"" />
                    </Code>
                  </xsl:if>
                  <xsl:if test=""Reference"">
                    <Reference>
                      <xsl:value-of select=""Reference/text()"" />
                    </Reference>
                  </xsl:if>
                  <xsl:value-of select=""./text()"" />
                </Node>
              </xsl:for-each>
              <xsl:value-of select=""./text()"" />
            </RFFNADLoop>
          </xsl:for-each>
          <xsl:value-of select=""./text()"" />
        </DeliveryLocation>
      </xsl:for-each>
      <xsl:for-each select=""DeliveryInfo"">
        <DeliveryInfo>
          <xsl:if test=""ActualDeliveryDate"">
            <ActualDeliveryDate>
              <xsl:value-of select=""ActualDeliveryDate/text()"" />
            </ActualDeliveryDate>
          </xsl:if>
          <xsl:if test=""ActualDeliveryDate"">
            <ActualDeliveryDate>
              <xsl:value-of select=""ActualDeliveryDate/text()"" />
            </ActualDeliveryDate>
          </xsl:if>
          <xsl:for-each select=""DeliveryTerms"">
            <DeliveryTerms>
              <xsl:if test=""SpecialTerms"">
                <SpecialTerms>
                  <xsl:value-of select=""SpecialTerms/text()"" />
                </SpecialTerms>
              </xsl:if>
              <xsl:if test=""SpecialTerms"">
                <SpecialTerms>
                  <xsl:value-of select=""SpecialTerms/text()"" />
                </SpecialTerms>
              </xsl:if>
              <xsl:value-of select=""./text()"" />
            </DeliveryTerms>
          </xsl:for-each>
          <xsl:value-of select=""./text()"" />
        </DeliveryInfo>
      </xsl:for-each>
      <PaymentMeans>
        <xsl:if test=""PaymentMeans/ID"">
          <ID>
            <xsl:value-of select=""PaymentMeans/ID/text()"" />
          </ID>
        </xsl:if>
        <xsl:if test=""PaymentMeans/ID"">
          <ID>
            <xsl:value-of select=""PaymentMeans/ID/text()"" />
          </ID>
        </xsl:if>
        <xsl:if test=""PaymentMeans/PaymentMeansCode"">
          <PaymentMeansCode>
            <xsl:value-of select=""PaymentMeans/PaymentMeansCode/text()"" />
          </PaymentMeansCode>
        </xsl:if>
        <xsl:if test=""PaymentMeans/PaymentMeansCode"">
          <PaymentMeansCode>
            <xsl:value-of select=""PaymentMeans/PaymentMeansCode/text()"" />
          </PaymentMeansCode>
        </xsl:if>
        <PaymentDueDate>
          <xsl:value-of select=""PaymentMeans/PaymentDueDate/text()"" />
        </PaymentDueDate>
        <PaymentDueDate>
          <xsl:value-of select=""PaymentMeans/PaymentDueDate/text()"" />
        </PaymentDueDate>
        <xsl:if test=""PaymentMeans/InstructionID"">
          <InstructionID>
            <xsl:value-of select=""PaymentMeans/InstructionID/text()"" />
          </InstructionID>
        </xsl:if>
        <xsl:if test=""PaymentMeans/InstructionID"">
          <InstructionID>
            <xsl:value-of select=""PaymentMeans/InstructionID/text()"" />
          </InstructionID>
        </xsl:if>
        <xsl:if test=""PaymentMeans/InstructionNote"">
          <InstructionNote>
            <xsl:value-of select=""PaymentMeans/InstructionNote/text()"" />
          </InstructionNote>
        </xsl:if>
        <xsl:if test=""PaymentMeans/InstructionNote"">
          <InstructionNote>
            <xsl:value-of select=""PaymentMeans/InstructionNote/text()"" />
          </InstructionNote>
        </xsl:if>
        <xsl:if test=""PaymentMeans/PaymentID"">
          <PaymentID>
            <xsl:value-of select=""PaymentMeans/PaymentID/text()"" />
          </PaymentID>
        </xsl:if>
        <xsl:if test=""PaymentMeans/PaymentID"">
          <PaymentID>
            <xsl:value-of select=""PaymentMeans/PaymentID/text()"" />
          </PaymentID>
        </xsl:if>
        <xsl:if test=""PaymentMeans/CreditAccountID"">
          <CreditAccountID>
            <xsl:value-of select=""PaymentMeans/CreditAccountID/text()"" />
          </CreditAccountID>
        </xsl:if>
        <xsl:if test=""PaymentMeans/CreditAccountID"">
          <CreditAccountID>
            <xsl:value-of select=""PaymentMeans/CreditAccountID/text()"" />
          </CreditAccountID>
        </xsl:if>
        <PaymentChannelCode>
          <xsl:value-of select=""PaymentMeans/PaymentChannelCode/text()"" />
          <xsl:value-of select=""PaymentMeans/PaymentChannelCode/text()"" />
        </PaymentChannelCode>
        <xsl:value-of select=""PaymentMeans/text()"" />
      </PaymentMeans>
      <xsl:for-each select=""PaymentTerms"">
        <PaymentTerms>
          <xsl:if test=""ID"">
            <ID>
              <xsl:value-of select=""ID/text()"" />
            </ID>
          </xsl:if>
          <xsl:if test=""ID"">
            <ID>
              <xsl:value-of select=""ID/text()"" />
            </ID>
          </xsl:if>
          <xsl:if test=""PaymentTermsID"">
            <PaymentTermsID>
              <xsl:value-of select=""PaymentTermsID/text()"" />
            </PaymentTermsID>
          </xsl:if>
          <xsl:if test=""PaymentMeansID"">
            <PaymentMeansID>
              <xsl:value-of select=""PaymentMeansID/text()"" />
            </PaymentMeansID>
          </xsl:if>
          <xsl:if test=""PaymentMeansID"">
            <PaymentMeansID>
              <xsl:value-of select=""PaymentMeansID/text()"" />
            </PaymentMeansID>
          </xsl:if>
          <xsl:if test=""PrepaidPaymentReferenceID"">
            <PrepaidPaymentReferenceID>
              <xsl:value-of select=""PrepaidPaymentReferenceID/text()"" />
            </PrepaidPaymentReferenceID>
          </xsl:if>
          <xsl:if test=""PrepaidPaymentReferenceID"">
            <PrepaidPaymentReferenceID>
              <xsl:value-of select=""PrepaidPaymentReferenceID/text()"" />
            </PrepaidPaymentReferenceID>
          </xsl:if>
          <xsl:for-each select=""Note"">
            <Note>
              <xsl:value-of select=""./text()"" />
            </Note>
            <Note>
              <xsl:value-of select=""./text()"" />
            </Note>
          </xsl:for-each>
          <xsl:if test=""ReferenceEventCode"">
            <ReferenceEventCode>
              <xsl:value-of select=""ReferenceEventCode/text()"" />
            </ReferenceEventCode>
          </xsl:if>
          <xsl:if test=""ReferenceEventCode"">
            <ReferenceEventCode>
              <xsl:value-of select=""ReferenceEventCode/text()"" />
            </ReferenceEventCode>
          </xsl:if>
          <xsl:if test=""SettlementDiscountPercent"">
            <SettlementDiscountPercent>
              <xsl:value-of select=""SettlementDiscountPercent/text()"" />
            </SettlementDiscountPercent>
          </xsl:if>
          <xsl:if test=""SettlementDiscountPercent"">
            <SettlementDiscountPercent>
              <xsl:value-of select=""SettlementDiscountPercent/text()"" />
            </SettlementDiscountPercent>
          </xsl:if>
          <xsl:if test=""PenaltySurchargePercent"">
            <PenaltySurchargePercent>
              <xsl:value-of select=""PenaltySurchargePercent/text()"" />
            </PenaltySurchargePercent>
          </xsl:if>
          <xsl:if test=""PenaltySurchargePercent"">
            <PenaltySurchargePercent>
              <xsl:value-of select=""PenaltySurchargePercent/text()"" />
            </PenaltySurchargePercent>
          </xsl:if>
          <xsl:if test=""Amount"">
            <Amount>
              <xsl:value-of select=""Amount/text()"" />
            </Amount>
          </xsl:if>
          <xsl:if test=""Amount"">
            <Amount>
              <xsl:value-of select=""Amount/text()"" />
            </Amount>
          </xsl:if>
          <xsl:if test=""TermsOfPaymentIdentification"">
            <TermsOfPaymentIdentification>
              <xsl:value-of select=""TermsOfPaymentIdentification/text()"" />
            </TermsOfPaymentIdentification>
          </xsl:if>
          <xsl:if test=""CodeListQualifier"">
            <CodeListQualifier>
              <xsl:value-of select=""CodeListQualifier/text()"" />
            </CodeListQualifier>
          </xsl:if>
          <xsl:if test=""CodeListResponsibleAgency"">
            <CodeListResponsibleAgency>
              <xsl:value-of select=""CodeListResponsibleAgency/text()"" />
            </CodeListResponsibleAgency>
          </xsl:if>
          <xsl:if test=""PaymentTimeReference"">
            <PaymentTimeReference>
              <xsl:value-of select=""PaymentTimeReference/text()"" />
            </PaymentTimeReference>
          </xsl:if>
          <xsl:if test=""TimeRelation"">
            <TimeRelation>
              <xsl:value-of select=""TimeRelation/text()"" />
            </TimeRelation>
          </xsl:if>
          <xsl:if test=""TypeOfPeriod"">
            <TypeOfPeriod>
              <xsl:value-of select=""TypeOfPeriod/text()"" />
            </TypeOfPeriod>
          </xsl:if>
          <xsl:if test=""NumberOfPeriods"">
            <NumberOfPeriods>
              <xsl:value-of select=""NumberOfPeriods/text()"" />
            </NumberOfPeriods>
          </xsl:if>
          <xsl:for-each select=""SettlementPeriod"">
            <SettlementPeriod>
              <xsl:if test=""StartDate"">
                <xsl:variable name=""var:v6"" select=""string(StartDate/@xsi:nil) = 'true'"" />
                <xsl:if test=""string($var:v6)='true'"">
                  <StartDate>
                    <xsl:attribute name=""xsi:nil"">
                      <xsl:value-of select=""'true'"" />
                    </xsl:attribute>
                  </StartDate>
                </xsl:if>
                <xsl:if test=""string($var:v6)='false'"">
                  <StartDate>
                    <xsl:value-of select=""StartDate/text()"" />
                  </StartDate>
                </xsl:if>
              </xsl:if>
              <xsl:if test=""StartDate"">
                <xsl:variable name=""var:v7"" select=""string(StartDate/@xsi:nil) = 'true'"" />
                <xsl:if test=""string($var:v7)='true'"">
                  <StartDate>
                    <xsl:attribute name=""xsi:nil"">
                      <xsl:value-of select=""'true'"" />
                    </xsl:attribute>
                  </StartDate>
                </xsl:if>
                <xsl:if test=""string($var:v7)='false'"">
                  <StartDate>
                    <xsl:value-of select=""StartDate/text()"" />
                  </StartDate>
                </xsl:if>
              </xsl:if>
              <xsl:if test=""EndDate"">
                <xsl:variable name=""var:v8"" select=""string(EndDate/@xsi:nil) = 'true'"" />
                <xsl:if test=""string($var:v8)='true'"">
                  <EndDate>
                    <xsl:attribute name=""xsi:nil"">
                      <xsl:value-of select=""'true'"" />
                    </xsl:attribute>
                  </EndDate>
                </xsl:if>
                <xsl:if test=""string($var:v8)='false'"">
                  <EndDate>
                    <xsl:value-of select=""EndDate/text()"" />
                  </EndDate>
                </xsl:if>
              </xsl:if>
              <xsl:if test=""EndDate"">
                <xsl:variable name=""var:v9"" select=""string(EndDate/@xsi:nil) = 'true'"" />
                <xsl:if test=""string($var:v9)='true'"">
                  <EndDate>
                    <xsl:attribute name=""xsi:nil"">
                      <xsl:value-of select=""'true'"" />
                    </xsl:attribute>
                  </EndDate>
                </xsl:if>
                <xsl:if test=""string($var:v9)='false'"">
                  <EndDate>
                    <xsl:value-of select=""EndDate/text()"" />
                  </EndDate>
                </xsl:if>
              </xsl:if>
              <xsl:if test=""Description"">
                <Description>
                  <xsl:value-of select=""Description/text()"" />
                </Description>
              </xsl:if>
              <xsl:if test=""Description"">
                <Description>
                  <xsl:value-of select=""Description/text()"" />
                </Description>
              </xsl:if>
              <xsl:value-of select=""./text()"" />
            </SettlementPeriod>
          </xsl:for-each>
          <xsl:for-each select=""PenaltyPeriod"">
            <PenaltyPeriod>
              <xsl:if test=""StartDate"">
                <xsl:variable name=""var:v10"" select=""string(StartDate/@xsi:nil) = 'true'"" />
                <xsl:if test=""string($var:v10)='true'"">
                  <StartDate>
                    <xsl:attribute name=""xsi:nil"">
                      <xsl:value-of select=""'true'"" />
                    </xsl:attribute>
                  </StartDate>
                </xsl:if>
                <xsl:if test=""string($var:v10)='false'"">
                  <StartDate>
                    <xsl:value-of select=""StartDate/text()"" />
                  </StartDate>
                </xsl:if>
              </xsl:if>
              <xsl:if test=""StartDate"">
                <xsl:variable name=""var:v11"" select=""string(StartDate/@xsi:nil) = 'true'"" />
                <xsl:if test=""string($var:v11)='true'"">
                  <StartDate>
                    <xsl:attribute name=""xsi:nil"">
                      <xsl:value-of select=""'true'"" />
                    </xsl:attribute>
                  </StartDate>
                </xsl:if>
                <xsl:if test=""string($var:v11)='false'"">
                  <StartDate>
                    <xsl:value-of select=""StartDate/text()"" />
                  </StartDate>
                </xsl:if>
              </xsl:if>
              <xsl:if test=""EndDate"">
                <xsl:variable name=""var:v12"" select=""string(EndDate/@xsi:nil) = 'true'"" />
                <xsl:if test=""string($var:v12)='true'"">
                  <EndDate>
                    <xsl:attribute name=""xsi:nil"">
                      <xsl:value-of select=""'true'"" />
                    </xsl:attribute>
                  </EndDate>
                </xsl:if>
                <xsl:if test=""string($var:v12)='false'"">
                  <EndDate>
                    <xsl:value-of select=""EndDate/text()"" />
                  </EndDate>
                </xsl:if>
              </xsl:if>
              <xsl:if test=""EndDate"">
                <xsl:variable name=""var:v13"" select=""string(EndDate/@xsi:nil) = 'true'"" />
                <xsl:if test=""string($var:v13)='true'"">
                  <EndDate>
                    <xsl:attribute name=""xsi:nil"">
                      <xsl:value-of select=""'true'"" />
                    </xsl:attribute>
                  </EndDate>
                </xsl:if>
                <xsl:if test=""string($var:v13)='false'"">
                  <EndDate>
                    <xsl:value-of select=""EndDate/text()"" />
                  </EndDate>
                </xsl:if>
              </xsl:if>
              <xsl:if test=""Description"">
                <Description>
                  <xsl:value-of select=""Description/text()"" />
                </Description>
              </xsl:if>
              <xsl:if test=""Description"">
                <Description>
                  <xsl:value-of select=""Description/text()"" />
                </Description>
              </xsl:if>
              <xsl:value-of select=""./text()"" />
            </PenaltyPeriod>
          </xsl:for-each>
          <xsl:for-each select=""TermsDiscountDueDate"">
            <TermsDiscountDueDate>
              <xsl:if test=""Code"">
                <Code>
                  <xsl:value-of select=""Code/text()"" />
                </Code>
              </xsl:if>
              <xsl:if test=""Date"">
                <Date>
                  <xsl:value-of select=""Date/text()"" />
                </Date>
              </xsl:if>
              <xsl:if test=""DateFormatCode"">
                <DateFormatCode>
                  <xsl:value-of select=""DateFormatCode/text()"" />
                </DateFormatCode>
              </xsl:if>
              <xsl:value-of select=""./text()"" />
            </TermsDiscountDueDate>
          </xsl:for-each>
          <xsl:for-each select=""TermsNetDueDate"">
            <TermsNetDueDate>
              <xsl:if test=""Code"">
                <Code>
                  <xsl:value-of select=""Code/text()"" />
                </Code>
              </xsl:if>
              <xsl:if test=""Date"">
                <Date>
                  <xsl:value-of select=""Date/text()"" />
                </Date>
              </xsl:if>
              <xsl:if test=""DateFormatCode"">
                <DateFormatCode>
                  <xsl:value-of select=""DateFormatCode/text()"" />
                </DateFormatCode>
              </xsl:if>
              <xsl:value-of select=""./text()"" />
            </TermsNetDueDate>
          </xsl:for-each>
          <xsl:for-each select=""Percentage"">
            <Percentage>
              <PercentageCode>
                <xsl:value-of select=""PercentageCode/text()"" />
              </PercentageCode>
              <xsl:if test=""PercentageValue"">
                <PercentageValue>
                  <xsl:value-of select=""PercentageValue/text()"" />
                </PercentageValue>
              </xsl:if>
              <xsl:if test=""PercentageIDCode"">
                <PercentageIDCode>
                  <xsl:value-of select=""PercentageIDCode/text()"" />
                </PercentageIDCode>
              </xsl:if>
              <xsl:if test=""CodeListIDCode"">
                <CodeListIDCode>
                  <xsl:value-of select=""CodeListIDCode/text()"" />
                </CodeListIDCode>
              </xsl:if>
              <xsl:if test=""CodeListAgencyCode"">
                <CodeListAgencyCode>
                  <xsl:value-of select=""CodeListAgencyCode/text()"" />
                </CodeListAgencyCode>
              </xsl:if>
              <xsl:if test=""StatusCode"">
                <StatusCode>
                  <xsl:value-of select=""StatusCode/text()"" />
                </StatusCode>
              </xsl:if>
              <xsl:value-of select=""./text()"" />
            </Percentage>
          </xsl:for-each>
          <xsl:for-each select=""MonetaryAmount"">
            <MonetaryAmount>
              <xsl:if test=""Code"">
                <Code>
                  <xsl:value-of select=""Code/text()"" />
                </Code>
              </xsl:if>
              <xsl:if test=""Amount"">
                <Amount>
                  <xsl:value-of select=""Amount/text()"" />
                </Amount>
              </xsl:if>
              <xsl:if test=""Currency"">
                <Currency>
                  <xsl:value-of select=""Currency/text()"" />
                </Currency>
              </xsl:if>
              <xsl:value-of select=""./text()"" />
            </MonetaryAmount>
          </xsl:for-each>
          <xsl:for-each select=""PaymentInstructions"">
            <PaymentInstructions>
              <xsl:if test=""PaymentConditionsCode"">
                <PaymentConditionsCode>
                  <xsl:value-of select=""PaymentConditionsCode/text()"" />
                </PaymentConditionsCode>
              </xsl:if>
              <xsl:if test=""PaymentGuaranteeMeansCode"">
                <PaymentGuaranteeMeansCode>
                  <xsl:value-of select=""PaymentGuaranteeMeansCode/text()"" />
                </PaymentGuaranteeMeansCode>
              </xsl:if>
              <xsl:if test=""PaymentMeansCode"">
                <PaymentMeansCode>
                  <xsl:value-of select=""PaymentMeansCode/text()"" />
                </PaymentMeansCode>
              </xsl:if>
              <xsl:if test=""CodeListIDCode"">
                <CodeListIDCode>
                  <xsl:value-of select=""CodeListIDCode/text()"" />
                </CodeListIDCode>
              </xsl:if>
              <xsl:if test=""PaymentChannelCode"">
                <PaymentChannelCode>
                  <xsl:value-of select=""PaymentChannelCode/text()"" />
                </PaymentChannelCode>
              </xsl:if>
              <xsl:value-of select=""./text()"" />
            </PaymentInstructions>
          </xsl:for-each>
          <xsl:for-each select=""FinancialInstitution"">
            <FinancialInstitution>
              <xsl:if test=""CodeQualifier"">
                <CodeQualifier>
                  <xsl:value-of select=""CodeQualifier/text()"" />
                </CodeQualifier>
              </xsl:if>
              <xsl:if test=""AccountHolderIdentifier"">
                <AccountHolderIdentifier>
                  <xsl:value-of select=""AccountHolderIdentifier/text()"" />
                </AccountHolderIdentifier>
              </xsl:if>
              <xsl:if test=""AccountHolderName"">
                <AccountHolderName>
                  <xsl:value-of select=""AccountHolderName/text()"" />
                </AccountHolderName>
              </xsl:if>
              <xsl:if test=""CurrencyIDCode"">
                <CurrencyIDCode>
                  <xsl:value-of select=""CurrencyIDCode/text()"" />
                </CurrencyIDCode>
              </xsl:if>
              <xsl:if test=""InstitutionNameCode"">
                <InstitutionNameCode>
                  <xsl:value-of select=""InstitutionNameCode/text()"" />
                </InstitutionNameCode>
              </xsl:if>
              <xsl:if test=""CodeListIDCode"">
                <CodeListIDCode>
                  <xsl:value-of select=""CodeListIDCode/text()"" />
                </CodeListIDCode>
              </xsl:if>
              <xsl:if test=""CodeListResponsibleAgencyCode"">
                <CodeListResponsibleAgencyCode>
                  <xsl:value-of select=""CodeListResponsibleAgencyCode/text()"" />
                </CodeListResponsibleAgencyCode>
              </xsl:if>
              <xsl:if test=""InstitutionBranchIdentifier"">
                <InstitutionBranchIdentifier>
                  <xsl:value-of select=""InstitutionBranchIdentifier/text()"" />
                </InstitutionBranchIdentifier>
              </xsl:if>
              <xsl:if test=""InstitutionName"">
                <InstitutionName>
                  <xsl:value-of select=""InstitutionName/text()"" />
                </InstitutionName>
              </xsl:if>
              <xsl:if test=""InstitutionBranchLocationName"">
                <InstitutionBranchLocationName>
                  <xsl:value-of select=""InstitutionBranchLocationName/text()"" />
                </InstitutionBranchLocationName>
              </xsl:if>
              <xsl:value-of select=""./text()"" />
            </FinancialInstitution>
          </xsl:for-each>
          <xsl:value-of select=""./text()"" />
        </PaymentTerms>
      </xsl:for-each>
      <xsl:for-each select=""AllowanceCharge"">
        <AllowanceCharge>
          <xsl:if test=""ID"">
            <ID>
              <xsl:value-of select=""ID/text()"" />
            </ID>
          </xsl:if>
          <xsl:if test=""ID"">
            <ID>
              <xsl:value-of select=""ID/text()"" />
            </ID>
          </xsl:if>
          <xsl:if test=""ChargeIndicator"">
            <ChargeIndicator>
              <xsl:value-of select=""ChargeIndicator/text()"" />
            </ChargeIndicator>
          </xsl:if>
          <xsl:if test=""ChargeIndicator"">
            <ChargeIndicator>
              <xsl:value-of select=""ChargeIndicator/text()"" />
            </ChargeIndicator>
          </xsl:if>
          <xsl:if test=""AllowanceChargeReasonCode"">
            <AllowanceChargeReasonCode>
              <xsl:value-of select=""AllowanceChargeReasonCode/text()"" />
            </AllowanceChargeReasonCode>
          </xsl:if>
          <xsl:if test=""AllowanceChargeReasonCode"">
            <AllowanceChargeReasonCode>
              <xsl:value-of select=""AllowanceChargeReasonCode/text()"" />
            </AllowanceChargeReasonCode>
          </xsl:if>
          <xsl:if test=""AllowanceChargeReason"">
            <AllowanceChargeReason>
              <xsl:value-of select=""AllowanceChargeReason/text()"" />
            </AllowanceChargeReason>
          </xsl:if>
          <xsl:if test=""AllowanceChargeReason"">
            <AllowanceChargeReason>
              <xsl:value-of select=""AllowanceChargeReason/text()"" />
            </AllowanceChargeReason>
          </xsl:if>
          <xsl:if test=""MultiplierFactorNumeric"">
            <MultiplierFactorNumeric>
              <xsl:value-of select=""MultiplierFactorNumeric/text()"" />
            </MultiplierFactorNumeric>
          </xsl:if>
          <xsl:if test=""MultiplierFactorNumeric"">
            <MultiplierFactorNumeric>
              <xsl:value-of select=""MultiplierFactorNumeric/text()"" />
            </MultiplierFactorNumeric>
          </xsl:if>
          <xsl:if test=""MultiplierFactorNumericCode"">
            <MultiplierFactorNumericCode>
              <xsl:value-of select=""MultiplierFactorNumericCode/text()"" />
            </MultiplierFactorNumericCode>
          </xsl:if>
          <xsl:if test=""MultiplierFactorNumericIDCode"">
            <MultiplierFactorNumericIDCode>
              <xsl:value-of select=""MultiplierFactorNumericIDCode/text()"" />
            </MultiplierFactorNumericIDCode>
          </xsl:if>
          <xsl:if test=""PrepaidIndicator"">
            <PrepaidIndicator>
              <xsl:value-of select=""PrepaidIndicator/text()"" />
            </PrepaidIndicator>
          </xsl:if>
          <xsl:if test=""PrepaidIndicator"">
            <PrepaidIndicator>
              <xsl:value-of select=""PrepaidIndicator/text()"" />
            </PrepaidIndicator>
          </xsl:if>
          <xsl:if test=""SequenceNumeric"">
            <SequenceNumeric>
              <xsl:value-of select=""SequenceNumeric/text()"" />
            </SequenceNumeric>
          </xsl:if>
          <xsl:if test=""SequenceNumeric"">
            <SequenceNumeric>
              <xsl:value-of select=""SequenceNumeric/text()"" />
            </SequenceNumeric>
          </xsl:if>
          <xsl:if test=""Amount"">
            <Amount>
              <xsl:value-of select=""Amount/text()"" />
            </Amount>
          </xsl:if>
          <xsl:if test=""Amount"">
            <Amount>
              <xsl:value-of select=""Amount/text()"" />
            </Amount>
          </xsl:if>
          <xsl:if test=""BaseAmount"">
            <BaseAmount>
              <xsl:value-of select=""BaseAmount/text()"" />
            </BaseAmount>
          </xsl:if>
          <xsl:if test=""BaseAmount"">
            <BaseAmount>
              <xsl:value-of select=""BaseAmount/text()"" />
            </BaseAmount>
          </xsl:if>
          <xsl:if test=""AccountingCost"">
            <AccountingCost>
              <xsl:value-of select=""AccountingCost/text()"" />
            </AccountingCost>
          </xsl:if>
          <xsl:if test=""AccountingCost"">
            <AccountingCost>
              <xsl:value-of select=""AccountingCost/text()"" />
            </AccountingCost>
          </xsl:if>
          <xsl:for-each select=""TaxCategory"">
            <TaxCategory>
              <xsl:if test=""ID"">
                <ID>
                  <xsl:value-of select=""ID/text()"" />
                </ID>
              </xsl:if>
              <xsl:if test=""ID"">
                <ID>
                  <xsl:value-of select=""ID/text()"" />
                </ID>
              </xsl:if>
              <xsl:if test=""Percent"">
                <Percent>
                  <xsl:value-of select=""Percent/text()"" />
                </Percent>
              </xsl:if>
              <xsl:if test=""Percent"">
                <Percent>
                  <xsl:value-of select=""Percent/text()"" />
                </Percent>
              </xsl:if>
              <xsl:if test=""PerUnitAmount"">
                <PerUnitAmount>
                  <xsl:value-of select=""PerUnitAmount/text()"" />
                </PerUnitAmount>
              </xsl:if>
              <xsl:if test=""PerUnitAmount"">
                <PerUnitAmount>
                  <xsl:value-of select=""PerUnitAmount/text()"" />
                </PerUnitAmount>
              </xsl:if>
              <xsl:for-each select=""TaxScheme"">
                <TaxScheme>
                  <xsl:if test=""ID"">
                    <ID>
                      <xsl:value-of select=""ID/text()"" />
                    </ID>
                  </xsl:if>
                  <xsl:if test=""ID"">
                    <ID>
                      <xsl:value-of select=""ID/text()"" />
                    </ID>
                  </xsl:if>
                  <xsl:if test=""Navn"">
                    <Navn>
                      <xsl:value-of select=""Navn/text()"" />
                    </Navn>
                  </xsl:if>
                  <xsl:if test=""Navn"">
                    <Navn>
                      <xsl:value-of select=""Navn/text()"" />
                    </Navn>
                  </xsl:if>
                  <xsl:value-of select=""./text()"" />
                </TaxScheme>
              </xsl:for-each>
              <xsl:value-of select=""./text()"" />
            </TaxCategory>
          </xsl:for-each>
          <xsl:value-of select=""./text()"" />
        </AllowanceCharge>
      </xsl:for-each>
      <xsl:for-each select=""TaxTotal"">
        <TaxTotal>
          <TaxAmount>
            <xsl:value-of select=""TaxAmount/text()"" />
          </TaxAmount>
          <TaxAmount>
            <xsl:value-of select=""TaxAmount/text()"" />
          </TaxAmount>
          <xsl:if test=""TaxAmountCode"">
            <TaxAmountCode>
              <xsl:value-of select=""TaxAmountCode/text()"" />
            </TaxAmountCode>
          </xsl:if>
          <xsl:if test=""Currency"">
            <Currency>
              <xsl:value-of select=""Currency/text()"" />
            </Currency>
          </xsl:if>
          <xsl:if test=""TaxEvidenceIndicator"">
            <TaxEvidenceIndicator>
              <xsl:value-of select=""TaxEvidenceIndicator/text()"" />
            </TaxEvidenceIndicator>
          </xsl:if>
          <xsl:for-each select=""TaxSubtotal"">
            <TaxSubtotal>
              <xsl:if test=""TaxCode"">
                <TaxCode>
                  <xsl:value-of select=""TaxCode/text()"" />
                </TaxCode>
              </xsl:if>
              <xsl:if test=""TaxAmount"">
                <TaxAmount>
                  <xsl:value-of select=""TaxAmount/text()"" />
                </TaxAmount>
              </xsl:if>
              <xsl:if test=""TaxAmount"">
                <TaxAmount>
                  <xsl:value-of select=""TaxAmount/text()"" />
                </TaxAmount>
              </xsl:if>
              <xsl:if test=""TaxAmountCode"">
                <TaxAmountCode>
                  <xsl:value-of select=""TaxAmountCode/text()"" />
                </TaxAmountCode>
              </xsl:if>
              <xsl:if test=""TaxAmountCurrency"">
                <TaxAmountCurrency>
                  <xsl:value-of select=""TaxAmountCurrency/text()"" />
                </TaxAmountCurrency>
              </xsl:if>
              <xsl:if test=""TaxableAmount"">
                <TaxableAmount>
                  <xsl:value-of select=""TaxableAmount/text()"" />
                </TaxableAmount>
              </xsl:if>
              <xsl:if test=""TaxableAmount"">
                <TaxableAmount>
                  <xsl:value-of select=""TaxableAmount/text()"" />
                </TaxableAmount>
              </xsl:if>
              <xsl:if test=""TaxableAmountCode"">
                <TaxableAmountCode>
                  <xsl:value-of select=""TaxableAmountCode/text()"" />
                </TaxableAmountCode>
              </xsl:if>
              <xsl:if test=""TaxableAmountCurrency"">
                <TaxableAmountCurrency>
                  <xsl:value-of select=""TaxableAmountCurrency/text()"" />
                </TaxableAmountCurrency>
              </xsl:if>
              <xsl:if test=""TaxPercent"">
                <TaxPercent>
                  <xsl:value-of select=""TaxPercent/text()"" />
                </TaxPercent>
              </xsl:if>
              <xsl:if test=""TaxPercent"">
                <TaxPercent>
                  <xsl:value-of select=""TaxPercent/text()"" />
                </TaxPercent>
              </xsl:if>
              <xsl:for-each select=""TaxCategory"">
                <TaxCategory>
                  <xsl:if test=""ID"">
                    <ID>
                      <xsl:value-of select=""ID/text()"" />
                    </ID>
                  </xsl:if>
                  <xsl:if test=""ID"">
                    <ID>
                      <xsl:value-of select=""ID/text()"" />
                    </ID>
                  </xsl:if>
                  <xsl:if test=""Percent"">
                    <Percent>
                      <xsl:value-of select=""Percent/text()"" />
                    </Percent>
                  </xsl:if>
                  <xsl:if test=""Percent"">
                    <Percent>
                      <xsl:value-of select=""Percent/text()"" />
                    </Percent>
                  </xsl:if>
                  <xsl:for-each select=""TaxScheme"">
                    <TaxScheme>
                      <xsl:if test=""ID"">
                        <ID>
                          <xsl:value-of select=""ID/text()"" />
                        </ID>
                      </xsl:if>
                      <xsl:if test=""ID"">
                        <ID>
                          <xsl:value-of select=""ID/text()"" />
                        </ID>
                      </xsl:if>
                      <xsl:if test=""Navn"">
                        <Navn>
                          <xsl:value-of select=""Navn/text()"" />
                        </Navn>
                      </xsl:if>
                      <xsl:if test=""Navn"">
                        <Navn>
                          <xsl:value-of select=""Navn/text()"" />
                        </Navn>
                      </xsl:if>
                      <xsl:if test=""TaxTypeCode"">
                        <TaxTypeCode>
                          <xsl:value-of select=""TaxTypeCode/text()"" />
                        </TaxTypeCode>
                      </xsl:if>
                      <xsl:if test=""TaxTypeCode"">
                        <TaxTypeCode>
                          <xsl:value-of select=""TaxTypeCode/text()"" />
                        </TaxTypeCode>
                      </xsl:if>
                      <xsl:value-of select=""./text()"" />
                    </TaxScheme>
                  </xsl:for-each>
                  <xsl:value-of select=""./text()"" />
                </TaxCategory>
              </xsl:for-each>
              <xsl:value-of select=""./text()"" />
            </TaxSubtotal>
          </xsl:for-each>
          <xsl:value-of select=""./text()"" />
        </TaxTotal>
      </xsl:for-each>
      <Total>
        <LineTotalAmount>
          <xsl:value-of select=""Total/LineTotalAmount/text()"" />
        </LineTotalAmount>
        <LineTotalAmount>
          <xsl:value-of select=""Total/LineTotalAmount/text()"" />
        </LineTotalAmount>
        <xsl:if test=""Total/TaxExclAmount"">
          <TaxExclAmount>
            <xsl:value-of select=""Total/TaxExclAmount/text()"" />
          </TaxExclAmount>
        </xsl:if>
        <xsl:if test=""Total/TaxExclAmount"">
          <TaxExclAmount>
            <xsl:value-of select=""Total/TaxExclAmount/text()"" />
          </TaxExclAmount>
        </xsl:if>
        <xsl:if test=""Total/TaxInclAmount"">
          <TaxInclAmount>
            <xsl:value-of select=""Total/TaxInclAmount/text()"" />
          </TaxInclAmount>
        </xsl:if>
        <xsl:if test=""Total/TaxInclAmount"">
          <TaxInclAmount>
            <xsl:value-of select=""Total/TaxInclAmount/text()"" />
          </TaxInclAmount>
        </xsl:if>
        <xsl:if test=""Total/AllowanceTotalAmount"">
          <AllowanceTotalAmount>
            <xsl:value-of select=""Total/AllowanceTotalAmount/text()"" />
          </AllowanceTotalAmount>
        </xsl:if>
        <xsl:if test=""Total/AllowanceTotalAmount"">
          <AllowanceTotalAmount>
            <xsl:value-of select=""Total/AllowanceTotalAmount/text()"" />
          </AllowanceTotalAmount>
        </xsl:if>
        <xsl:if test=""Total/ChargeTotalAmount"">
          <ChargeTotalAmount>
            <xsl:value-of select=""Total/ChargeTotalAmount/text()"" />
          </ChargeTotalAmount>
        </xsl:if>
        <xsl:if test=""Total/ChargeTotalAmount"">
          <ChargeTotalAmount>
            <xsl:value-of select=""Total/ChargeTotalAmount/text()"" />
          </ChargeTotalAmount>
        </xsl:if>
        <xsl:if test=""Total/TaxAmount"">
          <TaxAmount>
            <xsl:value-of select=""Total/TaxAmount/text()"" />
          </TaxAmount>
        </xsl:if>
        <xsl:if test=""Total/PrepaidAmount"">
          <PrepaidAmount>
            <xsl:value-of select=""Total/PrepaidAmount/text()"" />
          </PrepaidAmount>
        </xsl:if>
        <xsl:if test=""Total/PrepaidAmount"">
          <PrepaidAmount>
            <xsl:value-of select=""Total/PrepaidAmount/text()"" />
          </PrepaidAmount>
        </xsl:if>
        <xsl:if test=""Total/PayableRoundingAmount"">
          <PayableRoundingAmount>
            <xsl:value-of select=""Total/PayableRoundingAmount/text()"" />
          </PayableRoundingAmount>
        </xsl:if>
        <xsl:if test=""Total/PayableRoundingAmount"">
          <PayableRoundingAmount>
            <xsl:value-of select=""Total/PayableRoundingAmount/text()"" />
          </PayableRoundingAmount>
        </xsl:if>
        <PayableAmount>
          <xsl:value-of select=""Total/PayableAmount/text()"" />
        </PayableAmount>
        <PayableAmount>
          <xsl:value-of select=""Total/PayableAmount/text()"" />
        </PayableAmount>
        <xsl:if test=""Total/DutyTaxFeeTotalAmount"">
          <DutyTaxFeeTotalAmount>
            <xsl:value-of select=""Total/DutyTaxFeeTotalAmount/text()"" />
          </DutyTaxFeeTotalAmount>
        </xsl:if>
        <xsl:for-each select=""Total/ExtraTotalMOALoop1"">
          <ExtraTotalMOALoop1>
            <Node>
              <xsl:if test=""Node/Code"">
                <Code>
                  <xsl:value-of select=""Node/Code/text()"" />
                </Code>
              </xsl:if>
              <xsl:if test=""Node/Amount"">
                <Amount>
                  <xsl:value-of select=""Node/Amount/text()"" />
                </Amount>
              </xsl:if>
              <xsl:if test=""Node/Currency"">
                <Currency>
                  <xsl:value-of select=""Node/Currency/text()"" />
                </Currency>
              </xsl:if>
              <xsl:value-of select=""Node/text()"" />
            </Node>
            <xsl:value-of select=""./text()"" />
          </ExtraTotalMOALoop1>
        </xsl:for-each>
        <xsl:value-of select=""Total/text()"" />
      </Total>
      <Lines>
        <xsl:for-each select=""Lines/Line"">
          <Line>
            <LineNo>
              <xsl:value-of select=""LineNo/text()"" />
            </LineNo>
            <LineNo>
              <xsl:value-of select=""LineNo/text()"" />
            </LineNo>
            <xsl:if test=""Note"">
              <Note>
                <xsl:value-of select=""Note/text()"" />
              </Note>
            </xsl:if>
            <xsl:if test=""Note"">
              <Note>
                <xsl:value-of select=""Note/text()"" />
              </Note>
            </xsl:if>
            <NoteLoop>
              <Node>
                <xsl:if test=""NoteLoop/Node/NoteQualifier"">
                  <NoteQualifier>
                    <xsl:value-of select=""NoteLoop/Node/NoteQualifier/text()"" />
                  </NoteQualifier>
                </xsl:if>
                <xsl:if test=""NoteLoop/Node/NoteFunction"">
                  <NoteFunction>
                    <xsl:value-of select=""NoteLoop/Node/NoteFunction/text()"" />
                  </NoteFunction>
                </xsl:if>
                <xsl:if test=""NoteLoop/Node/NoteLanguage"">
                  <NoteLanguage>
                    <xsl:value-of select=""NoteLoop/Node/NoteLanguage/text()"" />
                  </NoteLanguage>
                </xsl:if>
                <xsl:if test=""NoteLoop/Node/Note"">
                  <xsl:variable name=""var:v14"" select=""string(NoteLoop/Node/Note/@xsi:nil) = 'true'"" />
                  <xsl:if test=""string($var:v14)='true'"">
                    <Note>
                      <xsl:attribute name=""xsi:nil"">
                        <xsl:value-of select=""'true'"" />
                      </xsl:attribute>
                    </Note>
                  </xsl:if>
                  <xsl:if test=""string($var:v14)='false'"">
                    <Note>
                      <xsl:value-of select=""NoteLoop/Node/Note/text()"" />
                    </Note>
                  </xsl:if>
                </xsl:if>
                <xsl:if test=""NoteLoop/Node"">
                  <xsl:value-of select=""NoteLoop/Node/text()"" />
                </xsl:if>
              </Node>
              <xsl:if test=""NoteLoop"">
                <xsl:value-of select=""NoteLoop/text()"" />
              </xsl:if>
            </NoteLoop>
            <Quantity>
              <xsl:value-of select=""Quantity/text()"" />
            </Quantity>
            <Quantity>
              <xsl:value-of select=""Quantity/text()"" />
            </Quantity>
            <UnitCode>
              <xsl:value-of select=""UnitCode/text()"" />
            </UnitCode>
            <UnitCode>
              <xsl:value-of select=""UnitCode/text()"" />
            </UnitCode>
            <ExtraQuantityLoop>
              <Node>
                <xsl:if test=""ExtraQuantityLoop/Node/Code"">
                  <Code>
                    <xsl:value-of select=""ExtraQuantityLoop/Node/Code/text()"" />
                  </Code>
                </xsl:if>
                <xsl:if test=""ExtraQuantityLoop/Node/Quantity"">
                  <Quantity>
                    <xsl:value-of select=""ExtraQuantityLoop/Node/Quantity/text()"" />
                  </Quantity>
                </xsl:if>
                <xsl:if test=""ExtraQuantityLoop/Node/UnitCode"">
                  <UnitCode>
                    <xsl:value-of select=""ExtraQuantityLoop/Node/UnitCode/text()"" />
                  </UnitCode>
                </xsl:if>
                <xsl:if test=""ExtraQuantityLoop/Node"">
                  <xsl:value-of select=""ExtraQuantityLoop/Node/text()"" />
                </xsl:if>
              </Node>
              <xsl:if test=""ExtraQuantityLoop"">
                <xsl:value-of select=""ExtraQuantityLoop/text()"" />
              </xsl:if>
            </ExtraQuantityLoop>
            <LineAmountTotal>
              <xsl:value-of select=""LineAmountTotal/text()"" />
            </LineAmountTotal>
            <LineAmountTotal>
              <xsl:value-of select=""LineAmountTotal/text()"" />
            </LineAmountTotal>
            <xsl:if test=""Currency"">
              <Currency>
                <xsl:value-of select=""Currency/text()"" />
              </Currency>
            </xsl:if>
            <ExtraMOALoop>
              <Node>
                <xsl:if test=""ExtraMOALoop/Node/Code"">
                  <Code>
                    <xsl:value-of select=""ExtraMOALoop/Node/Code/text()"" />
                  </Code>
                </xsl:if>
                <xsl:if test=""ExtraMOALoop/Node/Amount"">
                  <Amount>
                    <xsl:value-of select=""ExtraMOALoop/Node/Amount/text()"" />
                  </Amount>
                </xsl:if>
                <xsl:if test=""ExtraMOALoop/Node/Currency"">
                  <Currency>
                    <xsl:value-of select=""ExtraMOALoop/Node/Currency/text()"" />
                  </Currency>
                </xsl:if>
                <xsl:if test=""ExtraMOALoop/Node"">
                  <xsl:value-of select=""ExtraMOALoop/Node/text()"" />
                </xsl:if>
              </Node>
              <xsl:if test=""ExtraMOALoop"">
                <xsl:value-of select=""ExtraMOALoop/text()"" />
              </xsl:if>
            </ExtraMOALoop>
            <xsl:if test=""FreeOfChargeIndicator"">
              <FreeOfChargeIndicator>
                <xsl:value-of select=""FreeOfChargeIndicator/text()"" />
              </FreeOfChargeIndicator>
            </xsl:if>
            <xsl:if test=""FreeOfChargeIndicator"">
              <FreeOfChargeIndicator>
                <xsl:value-of select=""FreeOfChargeIndicator/text()"" />
              </FreeOfChargeIndicator>
            </xsl:if>
            <Delivery>
              <xsl:if test=""Delivery/ID"">
                <ID>
                  <xsl:value-of select=""Delivery/ID/text()"" />
                </ID>
              </xsl:if>
              <xsl:if test=""Delivery/ID"">
                <ID>
                  <xsl:value-of select=""Delivery/ID/text()"" />
                </ID>
              </xsl:if>
              <xsl:if test=""Delivery/Quantity"">
                <Quantity>
                  <xsl:value-of select=""Delivery/Quantity/text()"" />
                </Quantity>
              </xsl:if>
              <xsl:if test=""Delivery/Quantity"">
                <Quantity>
                  <xsl:value-of select=""Delivery/Quantity/text()"" />
                </Quantity>
              </xsl:if>
              <xsl:if test=""Delivery/UnitCode"">
                <UnitCode>
                  <xsl:value-of select=""Delivery/UnitCode/text()"" />
                </UnitCode>
              </xsl:if>
              <xsl:if test=""Delivery/UnitCode"">
                <UnitCode>
                  <xsl:value-of select=""Delivery/UnitCode/text()"" />
                </UnitCode>
              </xsl:if>
              <xsl:if test=""Delivery"">
                <xsl:value-of select=""Delivery/text()"" />
              </xsl:if>
            </Delivery>
            <ConsumerUnit>
              <xsl:if test=""ConsumerUnit/Quantity"">
                <Quantity>
                  <xsl:value-of select=""ConsumerUnit/Quantity/text()"" />
                </Quantity>
              </xsl:if>
              <xsl:if test=""ConsumerUnit/UnitCode"">
                <UnitCode>
                  <xsl:value-of select=""ConsumerUnit/UnitCode/text()"" />
                </UnitCode>
              </xsl:if>
              <xsl:if test=""ConsumerUnit"">
                <xsl:value-of select=""ConsumerUnit/text()"" />
              </xsl:if>
            </ConsumerUnit>
            <OrderReference>
              <xsl:if test=""OrderReference/LineID"">
                <LineID>
                  <xsl:value-of select=""OrderReference/LineID/text()"" />
                </LineID>
              </xsl:if>
              <xsl:if test=""OrderReference/SalesOrderLineID"">
                <SalesOrderLineID>
                  <xsl:value-of select=""OrderReference/SalesOrderLineID/text()"" />
                </SalesOrderLineID>
              </xsl:if>
              <Node>
                <xsl:if test=""OrderReference/Node/Code"">
                  <Code>
                    <xsl:value-of select=""OrderReference/Node/Code/text()"" />
                  </Code>
                </xsl:if>
                <xsl:if test=""OrderReference/Node/Reference"">
                  <Reference>
                    <xsl:value-of select=""OrderReference/Node/Reference/text()"" />
                  </Reference>
                </xsl:if>
                <xsl:if test=""OrderReference/Node"">
                  <xsl:value-of select=""OrderReference/Node/text()"" />
                </xsl:if>
              </Node>
              <xsl:if test=""OrderReference"">
                <xsl:value-of select=""OrderReference/text()"" />
              </xsl:if>
            </OrderReference>
            <Item>
              <xsl:if test=""Item/BuyerItemID"">
                <BuyerItemID>
                  <xsl:value-of select=""Item/BuyerItemID/text()"" />
                </BuyerItemID>
              </xsl:if>
              <xsl:if test=""Item/BuyerItemID"">
                <BuyerItemID>
                  <xsl:value-of select=""Item/BuyerItemID/text()"" />
                </BuyerItemID>
              </xsl:if>
              <xsl:if test=""Item/SellerItemID"">
                <SellerItemID>
                  <xsl:value-of select=""Item/SellerItemID/text()"" />
                </SellerItemID>
              </xsl:if>
              <xsl:if test=""Item/SellerItemID"">
                <SellerItemID>
                  <xsl:value-of select=""Item/SellerItemID/text()"" />
                </SellerItemID>
              </xsl:if>
              <xsl:if test=""Item/StandardItemID"">
                <StandardItemID>
                  <xsl:value-of select=""Item/StandardItemID/text()"" />
                </StandardItemID>
              </xsl:if>
              <xsl:if test=""Item/StandardItemID"">
                <StandardItemID>
                  <xsl:value-of select=""Item/StandardItemID/text()"" />
                </StandardItemID>
              </xsl:if>
              <xsl:if test=""Item/AdditionalItemID"">
                <AdditionalItemID>
                  <xsl:value-of select=""Item/AdditionalItemID/text()"" />
                </AdditionalItemID>
              </xsl:if>
              <xsl:if test=""Item/AdditionalItemID"">
                <AdditionalItemID>
                  <xsl:value-of select=""Item/AdditionalItemID/text()"" />
                </AdditionalItemID>
              </xsl:if>
              <xsl:if test=""Item/CatalogueItemID"">
                <CatalogueItemID>
                  <xsl:value-of select=""Item/CatalogueItemID/text()"" />
                </CatalogueItemID>
              </xsl:if>
              <xsl:if test=""Item/CatalogueItemID"">
                <CatalogueItemID>
                  <xsl:value-of select=""Item/CatalogueItemID/text()"" />
                </CatalogueItemID>
              </xsl:if>
              <xsl:if test=""Item/Name"">
                <Name>
                  <xsl:value-of select=""Item/Name/text()"" />
                </Name>
              </xsl:if>
              <xsl:if test=""Item/Name"">
                <Name>
                  <xsl:value-of select=""Item/Name/text()"" />
                </Name>
              </xsl:if>
              <xsl:if test=""Item/Description"">
                <Description>
                  <xsl:value-of select=""Item/Description/text()"" />
                </Description>
              </xsl:if>
              <xsl:if test=""Item/Description"">
                <Description>
                  <xsl:value-of select=""Item/Description/text()"" />
                </Description>
              </xsl:if>
              <xsl:if test=""Item/PackQuantity"">
                <PackQuantity>
                  <xsl:value-of select=""Item/PackQuantity/text()"" />
                </PackQuantity>
              </xsl:if>
              <xsl:if test=""Item/PackQuantity"">
                <PackQuantity>
                  <xsl:value-of select=""Item/PackQuantity/text()"" />
                </PackQuantity>
              </xsl:if>
              <xsl:if test=""Item/UnitCode"">
                <UnitCode>
                  <xsl:value-of select=""Item/UnitCode/text()"" />
                </UnitCode>
              </xsl:if>
              <xsl:if test=""Item/UnitCode"">
                <UnitCode>
                  <xsl:value-of select=""Item/UnitCode/text()"" />
                </UnitCode>
              </xsl:if>
              <ExtraPIALoop>
                <Node>
                  <xsl:if test=""Item/ExtraPIALoop/Node/DigitalCode"">
                    <DigitalCode>
                      <xsl:value-of select=""Item/ExtraPIALoop/Node/DigitalCode/text()"" />
                    </DigitalCode>
                  </xsl:if>
                  <xsl:if test=""Item/ExtraPIALoop/Node/PIA"">
                    <PIA>
                      <xsl:value-of select=""Item/ExtraPIALoop/Node/PIA/text()"" />
                    </PIA>
                  </xsl:if>
                  <xsl:if test=""Item/ExtraPIALoop/Node/Code"">
                    <Code>
                      <xsl:value-of select=""Item/ExtraPIALoop/Node/Code/text()"" />
                    </Code>
                  </xsl:if>
                  <xsl:if test=""Item/ExtraPIALoop/Node"">
                    <xsl:value-of select=""Item/ExtraPIALoop/Node/text()"" />
                  </xsl:if>
                </Node>
                <xsl:if test=""Item/ExtraPIALoop"">
                  <xsl:value-of select=""Item/ExtraPIALoop/text()"" />
                </xsl:if>
              </ExtraPIALoop>
              <SchemeID>
                <xsl:if test=""Item/SchemeID/StdItemID"">
                  <StdItemID>
                    <xsl:value-of select=""Item/SchemeID/StdItemID/text()"" />
                  </StdItemID>
                </xsl:if>
                <xsl:if test=""Item/SchemeID/BuyItemID"">
                  <BuyItemID>
                    <xsl:value-of select=""Item/SchemeID/BuyItemID/text()"" />
                  </BuyItemID>
                </xsl:if>
                <xsl:if test=""Item/SchemeID/SelItemID"">
                  <SelItemID>
                    <xsl:value-of select=""Item/SchemeID/SelItemID/text()"" />
                  </SelItemID>
                </xsl:if>
                <xsl:if test=""Item/SchemeID/AddItemID"">
                  <AddItemID>
                    <xsl:value-of select=""Item/SchemeID/AddItemID/text()"" />
                  </AddItemID>
                </xsl:if>
                <xsl:if test=""Item/SchemeID/CatItemID"">
                  <CatItemID>
                    <xsl:value-of select=""Item/SchemeID/CatItemID/text()"" />
                  </CatItemID>
                </xsl:if>
                <xsl:if test=""Item/SchemeID"">
                  <xsl:value-of select=""Item/SchemeID/text()"" />
                </xsl:if>
              </SchemeID>
              <SchemeAgencyID>
                <xsl:if test=""Item/SchemeAgencyID/StdItemID"">
                  <StdItemID>
                    <xsl:value-of select=""Item/SchemeAgencyID/StdItemID/text()"" />
                  </StdItemID>
                </xsl:if>
                <xsl:if test=""Item/SchemeAgencyID/BuyItemID"">
                  <BuyItemID>
                    <xsl:value-of select=""Item/SchemeAgencyID/BuyItemID/text()"" />
                  </BuyItemID>
                </xsl:if>
                <xsl:if test=""Item/SchemeAgencyID/SelItemID"">
                  <SelItemID>
                    <xsl:value-of select=""Item/SchemeAgencyID/SelItemID/text()"" />
                  </SelItemID>
                </xsl:if>
                <xsl:if test=""Item/SchemeAgencyID/AddItemID"">
                  <AddItemID>
                    <xsl:value-of select=""Item/SchemeAgencyID/AddItemID/text()"" />
                  </AddItemID>
                </xsl:if>
                <xsl:if test=""Item/SchemeAgencyID/CatItemID"">
                  <CatItemID>
                    <xsl:value-of select=""Item/SchemeAgencyID/CatItemID/text()"" />
                  </CatItemID>
                </xsl:if>
                <xsl:if test=""Item/SchemeAgencyID"">
                  <xsl:value-of select=""Item/SchemeAgencyID/text()"" />
                </xsl:if>
              </SchemeAgencyID>
              <xsl:value-of select=""Item/text()"" />
            </Item>
            <PriceNet>
              <Price>
                <xsl:value-of select=""PriceNet/Price/text()"" />
              </Price>
              <Price>
                <xsl:value-of select=""PriceNet/Price/text()"" />
              </Price>
              <Quantity>
                <xsl:value-of select=""PriceNet/Quantity/text()"" />
              </Quantity>
              <Quantity>
                <xsl:value-of select=""PriceNet/Quantity/text()"" />
              </Quantity>
              <xsl:if test=""PriceNet/UnitCode"">
                <UnitCode>
                  <xsl:value-of select=""PriceNet/UnitCode/text()"" />
                </UnitCode>
              </xsl:if>
              <xsl:if test=""PriceNet/UnitCode"">
                <UnitCode>
                  <xsl:value-of select=""PriceNet/UnitCode/text()"" />
                </UnitCode>
              </xsl:if>
              <xsl:if test=""PriceNet/PristypeCode"">
                <PristypeCode>
                  <xsl:value-of select=""PriceNet/PristypeCode/text()"" />
                </PristypeCode>
              </xsl:if>
              <xsl:if test=""PriceNet/PristypeCode"">
                <PristypeCode>
                  <xsl:value-of select=""PriceNet/PristypeCode/text()"" />
                </PristypeCode>
              </xsl:if>
              <xsl:if test=""PriceNet/PristypeTekst"">
                <PristypeTekst>
                  <xsl:value-of select=""PriceNet/PristypeTekst/text()"" />
                </PristypeTekst>
              </xsl:if>
              <xsl:if test=""PriceNet/PristypeTekst"">
                <PristypeTekst>
                  <xsl:value-of select=""PriceNet/PristypeTekst/text()"" />
                </PristypeTekst>
              </xsl:if>
              <xsl:value-of select=""PriceNet/text()"" />
            </PriceNet>
            <PriceGross>
              <xsl:if test=""PriceGross/Price"">
                <Price>
                  <xsl:value-of select=""PriceGross/Price/text()"" />
                </Price>
              </xsl:if>
              <xsl:if test=""PriceGross/Price"">
                <Price>
                  <xsl:value-of select=""PriceGross/Price/text()"" />
                </Price>
              </xsl:if>
              <xsl:if test=""PriceGross/Quantity"">
                <Quantity>
                  <xsl:value-of select=""PriceGross/Quantity/text()"" />
                </Quantity>
              </xsl:if>
              <xsl:if test=""PriceGross/Quantity"">
                <Quantity>
                  <xsl:value-of select=""PriceGross/Quantity/text()"" />
                </Quantity>
              </xsl:if>
              <xsl:if test=""PriceGross/UnitCode"">
                <UnitCode>
                  <xsl:value-of select=""PriceGross/UnitCode/text()"" />
                </UnitCode>
              </xsl:if>
              <xsl:if test=""PriceGross/UnitCode"">
                <UnitCode>
                  <xsl:value-of select=""PriceGross/UnitCode/text()"" />
                </UnitCode>
              </xsl:if>
              <xsl:if test=""PriceGross/PristypeCode"">
                <PristypeCode>
                  <xsl:value-of select=""PriceGross/PristypeCode/text()"" />
                </PristypeCode>
              </xsl:if>
              <xsl:if test=""PriceGross/PristypeCode"">
                <PristypeCode>
                  <xsl:value-of select=""PriceGross/PristypeCode/text()"" />
                </PristypeCode>
              </xsl:if>
              <xsl:if test=""PriceGross/PristypeTekst"">
                <PristypeTekst>
                  <xsl:value-of select=""PriceGross/PristypeTekst/text()"" />
                </PristypeTekst>
              </xsl:if>
              <xsl:if test=""PriceGross/PristypeTekst"">
                <PristypeTekst>
                  <xsl:value-of select=""PriceGross/PristypeTekst/text()"" />
                </PristypeTekst>
              </xsl:if>
              <xsl:if test=""PriceGross"">
                <xsl:value-of select=""PriceGross/text()"" />
              </xsl:if>
            </PriceGross>
            <AllowanceCharge>
              <xsl:if test=""AllowanceCharge/ID"">
                <ID>
                  <xsl:value-of select=""AllowanceCharge/ID/text()"" />
                </ID>
              </xsl:if>
              <xsl:if test=""AllowanceCharge/ID"">
                <ID>
                  <xsl:value-of select=""AllowanceCharge/ID/text()"" />
                </ID>
              </xsl:if>
              <xsl:if test=""AllowanceCharge/ChargeIndicator"">
                <ChargeIndicator>
                  <xsl:value-of select=""AllowanceCharge/ChargeIndicator/text()"" />
                </ChargeIndicator>
              </xsl:if>
              <xsl:if test=""AllowanceCharge/ChargeIndicator"">
                <ChargeIndicator>
                  <xsl:value-of select=""AllowanceCharge/ChargeIndicator/text()"" />
                </ChargeIndicator>
              </xsl:if>
              <xsl:if test=""AllowanceCharge/AllowanceChargeReasonCode"">
                <AllowanceChargeReasonCode>
                  <xsl:value-of select=""AllowanceCharge/AllowanceChargeReasonCode/text()"" />
                </AllowanceChargeReasonCode>
              </xsl:if>
              <xsl:if test=""AllowanceCharge/AllowanceChargeReasonCode"">
                <AllowanceChargeReasonCode>
                  <xsl:value-of select=""AllowanceCharge/AllowanceChargeReasonCode/text()"" />
                </AllowanceChargeReasonCode>
              </xsl:if>
              <xsl:if test=""AllowanceCharge/AllowanceChargeReason"">
                <AllowanceChargeReason>
                  <xsl:value-of select=""AllowanceCharge/AllowanceChargeReason/text()"" />
                </AllowanceChargeReason>
              </xsl:if>
              <xsl:if test=""AllowanceCharge/AllowanceChargeReason"">
                <AllowanceChargeReason>
                  <xsl:value-of select=""AllowanceCharge/AllowanceChargeReason/text()"" />
                </AllowanceChargeReason>
              </xsl:if>
              <xsl:if test=""AllowanceCharge/MultiplierFactorNumeric"">
                <MultiplierFactorNumeric>
                  <xsl:value-of select=""AllowanceCharge/MultiplierFactorNumeric/text()"" />
                </MultiplierFactorNumeric>
              </xsl:if>
              <xsl:if test=""AllowanceCharge/MultiplierFactorNumeric"">
                <MultiplierFactorNumeric>
                  <xsl:value-of select=""AllowanceCharge/MultiplierFactorNumeric/text()"" />
                </MultiplierFactorNumeric>
              </xsl:if>
              <xsl:if test=""AllowanceCharge/MultiplierFactorNumericCode"">
                <MultiplierFactorNumericCode>
                  <xsl:value-of select=""AllowanceCharge/MultiplierFactorNumericCode/text()"" />
                </MultiplierFactorNumericCode>
              </xsl:if>
              <xsl:if test=""AllowanceCharge/PrepaidIndicator"">
                <PrepaidIndicator>
                  <xsl:value-of select=""AllowanceCharge/PrepaidIndicator/text()"" />
                </PrepaidIndicator>
              </xsl:if>
              <xsl:if test=""AllowanceCharge/PrepaidIndicator"">
                <PrepaidIndicator>
                  <xsl:value-of select=""AllowanceCharge/PrepaidIndicator/text()"" />
                </PrepaidIndicator>
              </xsl:if>
              <xsl:if test=""AllowanceCharge/SequenceNumeric"">
                <SequenceNumeric>
                  <xsl:value-of select=""AllowanceCharge/SequenceNumeric/text()"" />
                </SequenceNumeric>
              </xsl:if>
              <xsl:if test=""AllowanceCharge/SequenceNumeric"">
                <SequenceNumeric>
                  <xsl:value-of select=""AllowanceCharge/SequenceNumeric/text()"" />
                </SequenceNumeric>
              </xsl:if>
              <xsl:if test=""AllowanceCharge/Amount"">
                <Amount>
                  <xsl:value-of select=""AllowanceCharge/Amount/text()"" />
                </Amount>
              </xsl:if>
              <xsl:if test=""AllowanceCharge/Amount"">
                <Amount>
                  <xsl:value-of select=""AllowanceCharge/Amount/text()"" />
                </Amount>
              </xsl:if>
              <xsl:if test=""AllowanceCharge/AmountCode"">
                <AmountCode>
                  <xsl:value-of select=""AllowanceCharge/AmountCode/text()"" />
                </AmountCode>
              </xsl:if>
              <xsl:if test=""AllowanceCharge/BaseAmount"">
                <BaseAmount>
                  <xsl:value-of select=""AllowanceCharge/BaseAmount/text()"" />
                </BaseAmount>
              </xsl:if>
              <xsl:if test=""AllowanceCharge/BaseAmount"">
                <BaseAmount>
                  <xsl:value-of select=""AllowanceCharge/BaseAmount/text()"" />
                </BaseAmount>
              </xsl:if>
              <TaxCategory>
                <xsl:if test=""AllowanceCharge/TaxCategory/ID"">
                  <ID>
                    <xsl:value-of select=""AllowanceCharge/TaxCategory/ID/text()"" />
                  </ID>
                </xsl:if>
                <xsl:if test=""AllowanceCharge/TaxCategory/ID"">
                  <ID>
                    <xsl:value-of select=""AllowanceCharge/TaxCategory/ID/text()"" />
                  </ID>
                </xsl:if>
                <xsl:if test=""AllowanceCharge/TaxCategory/Percent"">
                  <Percent>
                    <xsl:value-of select=""AllowanceCharge/TaxCategory/Percent/text()"" />
                  </Percent>
                </xsl:if>
                <xsl:if test=""AllowanceCharge/TaxCategory/Percent"">
                  <Percent>
                    <xsl:value-of select=""AllowanceCharge/TaxCategory/Percent/text()"" />
                  </Percent>
                </xsl:if>
                <xsl:if test=""AllowanceCharge/TaxCategory/PerUnitAmount"">
                  <PerUnitAmount>
                    <xsl:value-of select=""AllowanceCharge/TaxCategory/PerUnitAmount/text()"" />
                  </PerUnitAmount>
                </xsl:if>
                <xsl:if test=""AllowanceCharge/TaxCategory/PerUnitAmount"">
                  <PerUnitAmount>
                    <xsl:value-of select=""AllowanceCharge/TaxCategory/PerUnitAmount/text()"" />
                  </PerUnitAmount>
                </xsl:if>
                <TaxScheme>
                  <xsl:if test=""AllowanceCharge/TaxCategory/TaxScheme/ID"">
                    <ID>
                      <xsl:value-of select=""AllowanceCharge/TaxCategory/TaxScheme/ID/text()"" />
                    </ID>
                  </xsl:if>
                  <xsl:if test=""AllowanceCharge/TaxCategory/TaxScheme/ID"">
                    <ID>
                      <xsl:value-of select=""AllowanceCharge/TaxCategory/TaxScheme/ID/text()"" />
                    </ID>
                  </xsl:if>
                  <xsl:if test=""AllowanceCharge/TaxCategory/TaxScheme/Navn"">
                    <Navn>
                      <xsl:value-of select=""AllowanceCharge/TaxCategory/TaxScheme/Navn/text()"" />
                    </Navn>
                  </xsl:if>
                  <xsl:if test=""AllowanceCharge/TaxCategory/TaxScheme/Navn"">
                    <Navn>
                      <xsl:value-of select=""AllowanceCharge/TaxCategory/TaxScheme/Navn/text()"" />
                    </Navn>
                  </xsl:if>
                  <xsl:if test=""AllowanceCharge/TaxCategory/TaxScheme"">
                    <xsl:value-of select=""AllowanceCharge/TaxCategory/TaxScheme/text()"" />
                  </xsl:if>
                </TaxScheme>
                <xsl:if test=""AllowanceCharge/TaxCategory"">
                  <xsl:value-of select=""AllowanceCharge/TaxCategory/text()"" />
                </xsl:if>
              </TaxCategory>
              <xsl:if test=""AllowanceCharge"">
                <xsl:value-of select=""AllowanceCharge/text()"" />
              </xsl:if>
            </AllowanceCharge>
            <TaxTotal>
              <xsl:if test=""TaxTotal/TaxAmount"">
                <TaxAmount>
                  <xsl:value-of select=""TaxTotal/TaxAmount/text()"" />
                </TaxAmount>
              </xsl:if>
              <xsl:if test=""TaxTotal/TaxAmount"">
                <TaxAmount>
                  <xsl:value-of select=""TaxTotal/TaxAmount/text()"" />
                </TaxAmount>
              </xsl:if>
              <xsl:if test=""TaxTotal/TaxAmountCode"">
                <TaxAmountCode>
                  <xsl:value-of select=""TaxTotal/TaxAmountCode/text()"" />
                </TaxAmountCode>
              </xsl:if>
              <xsl:if test=""TaxTotal/Currency"">
                <Currency>
                  <xsl:value-of select=""TaxTotal/Currency/text()"" />
                </Currency>
              </xsl:if>
              <xsl:if test=""TaxTotal/TaxEvidenceIndicator"">
                <TaxEvidenceIndicator>
                  <xsl:value-of select=""TaxTotal/TaxEvidenceIndicator/text()"" />
                </TaxEvidenceIndicator>
              </xsl:if>
              <TaxSubtotal>
                <xsl:if test=""TaxTotal/TaxSubtotal/TaxCode"">
                  <TaxCode>
                    <xsl:value-of select=""TaxTotal/TaxSubtotal/TaxCode/text()"" />
                  </TaxCode>
                </xsl:if>
                <xsl:if test=""TaxTotal/TaxSubtotal/TaxAmount"">
                  <TaxAmount>
                    <xsl:value-of select=""TaxTotal/TaxSubtotal/TaxAmount/text()"" />
                  </TaxAmount>
                </xsl:if>
                <xsl:if test=""TaxTotal/TaxSubtotal/TaxAmount"">
                  <TaxAmount>
                    <xsl:value-of select=""TaxTotal/TaxSubtotal/TaxAmount/text()"" />
                  </TaxAmount>
                </xsl:if>
                <xsl:if test=""TaxTotal/TaxSubtotal/TaxAmountCode"">
                  <TaxAmountCode>
                    <xsl:value-of select=""TaxTotal/TaxSubtotal/TaxAmountCode/text()"" />
                  </TaxAmountCode>
                </xsl:if>
                <xsl:if test=""TaxTotal/TaxSubtotal/TaxAmountCurrency"">
                  <TaxAmountCurrency>
                    <xsl:value-of select=""TaxTotal/TaxSubtotal/TaxAmountCurrency/text()"" />
                  </TaxAmountCurrency>
                </xsl:if>
                <xsl:if test=""TaxTotal/TaxSubtotal/TaxableAmount"">
                  <TaxableAmount>
                    <xsl:value-of select=""TaxTotal/TaxSubtotal/TaxableAmount/text()"" />
                  </TaxableAmount>
                </xsl:if>
                <xsl:if test=""TaxTotal/TaxSubtotal/TaxableAmount"">
                  <TaxableAmount>
                    <xsl:value-of select=""TaxTotal/TaxSubtotal/TaxableAmount/text()"" />
                  </TaxableAmount>
                </xsl:if>
                <xsl:if test=""TaxTotal/TaxSubtotal/TaxableAmountCode"">
                  <TaxableAmountCode>
                    <xsl:value-of select=""TaxTotal/TaxSubtotal/TaxableAmountCode/text()"" />
                  </TaxableAmountCode>
                </xsl:if>
                <xsl:if test=""TaxTotal/TaxSubtotal/TaxableAmountCurrency"">
                  <TaxableAmountCurrency>
                    <xsl:value-of select=""TaxTotal/TaxSubtotal/TaxableAmountCurrency/text()"" />
                  </TaxableAmountCurrency>
                </xsl:if>
                <xsl:if test=""TaxTotal/TaxSubtotal/TaxPercent"">
                  <TaxPercent>
                    <xsl:value-of select=""TaxTotal/TaxSubtotal/TaxPercent/text()"" />
                  </TaxPercent>
                </xsl:if>
                <xsl:if test=""TaxTotal/TaxSubtotal/TaxPercent"">
                  <TaxPercent>
                    <xsl:value-of select=""TaxTotal/TaxSubtotal/TaxPercent/text()"" />
                  </TaxPercent>
                </xsl:if>
                <TaxCategory>
                  <xsl:if test=""TaxTotal/TaxSubtotal/TaxCategory/ID"">
                    <ID>
                      <xsl:value-of select=""TaxTotal/TaxSubtotal/TaxCategory/ID/text()"" />
                    </ID>
                  </xsl:if>
                  <xsl:if test=""TaxTotal/TaxSubtotal/TaxCategory/ID"">
                    <ID>
                      <xsl:value-of select=""TaxTotal/TaxSubtotal/TaxCategory/ID/text()"" />
                    </ID>
                  </xsl:if>
                  <xsl:if test=""TaxTotal/TaxSubtotal/TaxCategory/Percent"">
                    <Percent>
                      <xsl:value-of select=""TaxTotal/TaxSubtotal/TaxCategory/Percent/text()"" />
                    </Percent>
                  </xsl:if>
                  <xsl:if test=""TaxTotal/TaxSubtotal/TaxCategory/Percent"">
                    <Percent>
                      <xsl:value-of select=""TaxTotal/TaxSubtotal/TaxCategory/Percent/text()"" />
                    </Percent>
                  </xsl:if>
                  <TaxScheme>
                    <xsl:if test=""TaxTotal/TaxSubtotal/TaxCategory/TaxScheme/ID"">
                      <ID>
                        <xsl:value-of select=""TaxTotal/TaxSubtotal/TaxCategory/TaxScheme/ID/text()"" />
                      </ID>
                    </xsl:if>
                    <xsl:if test=""TaxTotal/TaxSubtotal/TaxCategory/TaxScheme/ID"">
                      <ID>
                        <xsl:value-of select=""TaxTotal/TaxSubtotal/TaxCategory/TaxScheme/ID/text()"" />
                      </ID>
                    </xsl:if>
                    <xsl:if test=""TaxTotal/TaxSubtotal/TaxCategory/TaxScheme/Navn"">
                      <Navn>
                        <xsl:value-of select=""TaxTotal/TaxSubtotal/TaxCategory/TaxScheme/Navn/text()"" />
                      </Navn>
                    </xsl:if>
                    <xsl:if test=""TaxTotal/TaxSubtotal/TaxCategory/TaxScheme/Navn"">
                      <Navn>
                        <xsl:value-of select=""TaxTotal/TaxSubtotal/TaxCategory/TaxScheme/Navn/text()"" />
                      </Navn>
                    </xsl:if>
                    <xsl:if test=""TaxTotal/TaxSubtotal/TaxCategory/TaxScheme"">
                      <xsl:value-of select=""TaxTotal/TaxSubtotal/TaxCategory/TaxScheme/text()"" />
                    </xsl:if>
                  </TaxScheme>
                  <xsl:if test=""TaxTotal/TaxSubtotal/TaxCategory"">
                    <xsl:value-of select=""TaxTotal/TaxSubtotal/TaxCategory/text()"" />
                  </xsl:if>
                </TaxCategory>
                <xsl:if test=""TaxTotal/TaxSubtotal"">
                  <xsl:value-of select=""TaxTotal/TaxSubtotal/text()"" />
                </xsl:if>
              </TaxSubtotal>
              <xsl:if test=""TaxTotal"">
                <xsl:value-of select=""TaxTotal/text()"" />
              </xsl:if>
            </TaxTotal>
            <Period>
              <xsl:if test=""Period/ID"">
                <ID>
                  <xsl:value-of select=""Period/ID/text()"" />
                </ID>
              </xsl:if>
              <xsl:if test=""Period/ID"">
                <ID>
                  <xsl:value-of select=""Period/ID/text()"" />
                </ID>
              </xsl:if>
              <xsl:if test=""Period/FromDate"">
                <FromDate>
                  <xsl:value-of select=""Period/FromDate/text()"" />
                </FromDate>
              </xsl:if>
              <xsl:if test=""Period/FromDate"">
                <FromDate>
                  <xsl:value-of select=""Period/FromDate/text()"" />
                </FromDate>
              </xsl:if>
              <xsl:if test=""Period/ToDate"">
                <ToDate>
                  <xsl:value-of select=""Period/ToDate/text()"" />
                </ToDate>
              </xsl:if>
              <xsl:if test=""Period/ToDate"">
                <ToDate>
                  <xsl:value-of select=""Period/ToDate/text()"" />
                </ToDate>
              </xsl:if>
              <xsl:if test=""Period"">
                <xsl:value-of select=""Period/text()"" />
              </xsl:if>
            </Period>
            <xsl:value-of select=""./text()"" />
          </Line>
        </xsl:for-each>
        <xsl:value-of select=""Lines/text()"" />
      </Lines>
    </ns0:Invoice>
  </xsl:template>
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"BYGE.Integration.Core.Invoice.Invoice";
        
        private const global::BYGE.Integration.Core.Invoice.Invoice _srcSchemaTypeReference0 = null;
        
        private const string _strTrgSchemasList0 = @"BYGE.Integration.Core.Invoice.Invoice";
        
        private const global::BYGE.Integration.Core.Invoice.Invoice _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [1];
                _SrcSchemas[0] = @"BYGE.Integration.Core.Invoice.Invoice";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"BYGE.Integration.Core.Invoice.Invoice";
                return _TrgSchemas;
            }
        }
    }
}
