namespace BYGE.Integration.Core.Invoice {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [Schema(@"http://byg-e.dk/schemas/v10",@"Invoice")]
    [Microsoft.XLANGs.BaseTypes.PropertyAttribute(typeof(global::BYGE.Integration.Core.Properties.MessageID), XPath = @"/*[local-name()='Invoice' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='InvoiceID' and namespace-uri()='']", XsdType = @"string")]
    [Microsoft.XLANGs.BaseTypes.PropertyAttribute(typeof(global::BYGE.Integration.Core.Properties.SenderID), XPath = @"/*[local-name()='Invoice' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='AccountingSupplier' and namespace-uri()='']/*[local-name()='AccSellerID' and namespace-uri()='']", XsdType = @"string")]
    [Microsoft.XLANGs.BaseTypes.PropertyAttribute(typeof(global::BYGE.Integration.Core.Properties.BuyerID), XPath = @"/*[local-name()='Invoice' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='BuyerCustomer' and namespace-uri()='']/*[local-name()='BuyerID' and namespace-uri()='']", XsdType = @"string")]
    [Microsoft.XLANGs.BaseTypes.PropertyAttribute(typeof(global::BYGE.Integration.Core.Properties.ReceiverID), XPath = @"/*[local-name()='Invoice' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='AccountingCustomer' and namespace-uri()='']/*[local-name()='AccBuyerID' and namespace-uri()='']", XsdType = @"string")]
    [Microsoft.XLANGs.BaseTypes.PropertyAttribute(typeof(global::BYGE.Integration.Core.Properties.InvoiceType), XPath = @"/*[local-name()='Invoice' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='InvoiceType' and namespace-uri()='']", XsdType = @"string")]
    [Microsoft.XLANGs.BaseTypes.PropertyAttribute(typeof(global::BYGE.Integration.Core.Properties.IssueDate), XPath = @"/*[local-name()='Invoice' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='IssueDate' and namespace-uri()='']", XsdType = @"date")]
    [Microsoft.XLANGs.BaseTypes.PropertyAttribute(typeof(global::BYGE.Integration.Core.Properties.SenderName), XPath = @"/*[local-name()='Invoice' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='AccountingSupplier' and namespace-uri()='']/*[local-name()='Address' and namespace-uri()='']/*[local-name()='Name' and namespace-uri()='']", XsdType = @"string")]
    [Microsoft.XLANGs.BaseTypes.PropertyAttribute(typeof(global::BYGE.Integration.Core.Properties.ReceiverName), XPath = @"/*[local-name()='Invoice' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='AccountingCustomer' and namespace-uri()='']/*[local-name()='Address' and namespace-uri()='']/*[local-name()='Name' and namespace-uri()='']", XsdType = @"string")]
    [Microsoft.XLANGs.BaseTypes.PropertyAttribute(typeof(global::BYGE.Integration.Core.Properties.BuyerName), XPath = @"/*[local-name()='Invoice' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='BuyerCustomer' and namespace-uri()='']/*[local-name()='Address' and namespace-uri()='']/*[local-name()='Name' and namespace-uri()='']", XsdType = @"string")]
    [Microsoft.XLANGs.BaseTypes.PropertyAttribute(typeof(global::BYGE.Integration.Core.Properties.TestIndicator), XPath = @"/*[local-name()='Invoice' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='TestIndicator' and namespace-uri()='']", XsdType = @"string")]
    [Microsoft.XLANGs.BaseTypes.PropertyAttribute(typeof(global::BYGE.Integration.Core.Properties.UNB2Qualifier), XPath = @"/*[local-name()='Invoice' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='UNB2Qualifier' and namespace-uri()='']", XsdType = @"string")]
    [Microsoft.XLANGs.BaseTypes.PropertyAttribute(typeof(global::BYGE.Integration.Core.Properties.UNB3Qualifier), XPath = @"/*[local-name()='Invoice' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='UNB3Qualifier' and namespace-uri()='']", XsdType = @"string")]
    [Microsoft.XLANGs.BaseTypes.PropertyAttribute(typeof(global::BYGE.Integration.Core.Properties.UNB2Alt), XPath = @"/*[local-name()='Invoice' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='UNB2Alt' and namespace-uri()='']", XsdType = @"string")]
    [Microsoft.XLANGs.BaseTypes.PropertyAttribute(typeof(global::BYGE.Integration.Core.Properties.UNB3Alt), XPath = @"/*[local-name()='Invoice' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='UNB3Alt' and namespace-uri()='']", XsdType = @"string")]
    [Microsoft.XLANGs.BaseTypes.PropertyAttribute(typeof(global::BYGE.Integration.Core.Properties.UNB2AltQualifier), XPath = @"/*[local-name()='Invoice' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='UNB2AltQualifier' and namespace-uri()='']", XsdType = @"string")]
    [Microsoft.XLANGs.BaseTypes.PropertyAttribute(typeof(global::BYGE.Integration.Core.Properties.UNB3AltQualifier), XPath = @"/*[local-name()='Invoice' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='UNB3AltQualifier' and namespace-uri()='']", XsdType = @"string")]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"Invoice"})]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.Core.Properties.CoreProperties", typeof(global::BYGE.Integration.Core.Properties.CoreProperties))]
    public sealed class Invoice : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xs:schema xmlns=""http://byg-e.dk/schemas/v10"" xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" xmlns:ns0=""https://BYGE.Integration.Core.Properties.CoreProperties"" targetNamespace=""http://byg-e.dk/schemas/v10"" xmlns:xs=""http://www.w3.org/2001/XMLSchema"">
  <xs:annotation>
    <xs:appinfo>
      <b:imports>
        <b:namespace prefix=""ns0"" uri=""https://BYGE.Integration.Core.Properties.CoreProperties"" location=""BYGE.Integration.Core.Properties.CoreProperties"" />
      </b:imports>
    </xs:appinfo>
  </xs:annotation>
  <xs:element name=""Invoice"">
    <xs:annotation>
      <xs:appinfo>
        <b:properties>
          <b:property name=""ns0:MessageID"" xpath=""/*[local-name()='Invoice' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='InvoiceID' and namespace-uri()='']"" />
          <b:property name=""ns0:SenderID"" xpath=""/*[local-name()='Invoice' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='AccountingSupplier' and namespace-uri()='']/*[local-name()='AccSellerID' and namespace-uri()='']"" />
          <b:property name=""ns0:BuyerID"" xpath=""/*[local-name()='Invoice' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='BuyerCustomer' and namespace-uri()='']/*[local-name()='BuyerID' and namespace-uri()='']"" />
          <b:property name=""ns0:ReceiverID"" xpath=""/*[local-name()='Invoice' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='AccountingCustomer' and namespace-uri()='']/*[local-name()='AccBuyerID' and namespace-uri()='']"" />
          <b:property name=""ns0:InvoiceType"" xpath=""/*[local-name()='Invoice' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='InvoiceType' and namespace-uri()='']"" />
          <b:property name=""ns0:IssueDate"" xpath=""/*[local-name()='Invoice' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='IssueDate' and namespace-uri()='']"" />
          <b:property name=""ns0:SenderName"" xpath=""/*[local-name()='Invoice' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='AccountingSupplier' and namespace-uri()='']/*[local-name()='Address' and namespace-uri()='']/*[local-name()='Name' and namespace-uri()='']"" />
          <b:property name=""ns0:ReceiverName"" xpath=""/*[local-name()='Invoice' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='AccountingCustomer' and namespace-uri()='']/*[local-name()='Address' and namespace-uri()='']/*[local-name()='Name' and namespace-uri()='']"" />
          <b:property name=""ns0:BuyerName"" xpath=""/*[local-name()='Invoice' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='BuyerCustomer' and namespace-uri()='']/*[local-name()='Address' and namespace-uri()='']/*[local-name()='Name' and namespace-uri()='']"" />
          <b:property name=""ns0:TestIndicator"" xpath=""/*[local-name()='Invoice' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='TestIndicator' and namespace-uri()='']"" />
          <b:property name=""ns0:UNB2Qualifier"" xpath=""/*[local-name()='Invoice' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='UNB2Qualifier' and namespace-uri()='']"" />
          <b:property name=""ns0:UNB3Qualifier"" xpath=""/*[local-name()='Invoice' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='UNB3Qualifier' and namespace-uri()='']"" />
          <b:property name=""ns0:UNB2Alt"" xpath=""/*[local-name()='Invoice' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='UNB2Alt' and namespace-uri()='']"" />
          <b:property name=""ns0:UNB3Alt"" xpath=""/*[local-name()='Invoice' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='UNB3Alt' and namespace-uri()='']"" />
          <b:property name=""ns0:UNB2AltQualifier"" xpath=""/*[local-name()='Invoice' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='UNB2AltQualifier' and namespace-uri()='']"" />
          <b:property name=""ns0:UNB3AltQualifier"" xpath=""/*[local-name()='Invoice' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='UNB3AltQualifier' and namespace-uri()='']"" />
        </b:properties>
      </xs:appinfo>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CustomizationID"" type=""xs:string"" />
        <xs:element minOccurs=""1"" maxOccurs=""1"" name=""ProfileID"" type=""xs:string"" />
        <xs:element minOccurs=""1"" maxOccurs=""1"" name=""InvoiceID"" type=""xs:string"" />
        <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CopyIndicator"" type=""xs:string"" />
        <xs:element minOccurs=""0"" maxOccurs=""1"" name=""UUID"" type=""xs:string"" />
        <xs:element default=""0"" name=""TestIndicator"" type=""xs:string"" />
        <xs:element name=""UNB2Qualifier"" type=""xs:string"" />
        <xs:element name=""UNB3Qualifier"" type=""xs:string"" />
        <xs:element name=""UNB2Alt"" type=""xs:string"" />
        <xs:element name=""UNB3Alt"" type=""xs:string"" />
        <xs:element name=""UNB2AltQualifier"" type=""xs:string"" />
        <xs:element name=""UNB3AltQualifier"" type=""xs:string"" />
        <xs:element minOccurs=""1"" maxOccurs=""1"" name=""IssueDate"" type=""xs:date"" />
        <xs:element minOccurs=""0"" name=""DueDate"" type=""xs:date"" />
        <xs:element minOccurs=""1"" maxOccurs=""1"" name=""InvoiceType"" type=""xs:string"" />
        <xs:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Note"" type=""xs:string"" />
        <xs:element minOccurs=""0"" maxOccurs=""99"" name=""NoteLoop"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""0"" maxOccurs=""99"" name=""Node"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""99"" name=""NoteQualifier"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""99"" name=""NoteFunction"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""99"" name=""NoteLanguage"" type=""xs:date"" />
                    <xs:element minOccurs=""0"" maxOccurs=""99"" name=""Note"" nillable=""true"" type=""xs:date"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""0"" name=""TaxPointDate"" type=""xs:string"" />
        <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Currency"" type=""xs:string"" />
        <xs:element minOccurs=""0"" name=""TaxCurrencyCode"" type=""xs:string"" />
        <xs:element minOccurs=""0"" name=""AccountingCost"" type=""xs:string"" />
        <xs:element minOccurs=""0"" name=""BuyerReference"" type=""xs:string"" />
        <xs:element minOccurs=""0"" name=""InvoicePeriod"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""0"" name=""StartDate"" type=""xs:date"" />
              <xs:element minOccurs=""0"" name=""StartTime"" type=""xs:time"" />
              <xs:element minOccurs=""0"" name=""EndDate"" type=""xs:date"" />
              <xs:element minOccurs=""0"" name=""EndTime"" type=""xs:time"" />
              <xs:element minOccurs=""0"" name=""DurationMeasure"" type=""xs:string"" />
              <xs:element minOccurs=""0"" name=""DescriptionCode"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Description"" type=""xs:string"" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""1"" maxOccurs=""1"" name=""OrderReference"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""1"" maxOccurs=""1"" name=""OrderID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""SalesOrderID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""OrderDate"" type=""xs:date"" />
              <xs:element minOccurs=""0"" name=""SalesOrderDate"" nillable=""true"" type=""xs:date"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CustomerReference"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""OrderDateFormat"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""SalesOrderDateFormat"" type=""xs:string"" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""0"" maxOccurs=""99"" name=""ExtraOrderReference"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""0"" maxOccurs=""99"" name=""Node"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Code"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Reference"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""DateCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Date"" type=""xs:date"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""DateFormat"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""0"" maxOccurs=""99"" name=""DTMReference"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""0"" maxOccurs=""99"" name=""Node"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Code"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TimeStamp"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""0"" maxOccurs=""1"" name=""BillingReference"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""InvoiceDocumentReference"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""IssueDate"" nillable=""true"" type=""xs:date"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""DateFormat"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""0"" maxOccurs=""1"" name=""DespatchDocumentReference"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""1"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ReceiptDocumentReference"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""1"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""0"" maxOccurs=""1"" name=""OriginatorDocumentReference"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""1"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ContractDocumentReference"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""1"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AdditionalDocumentReference"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""1"" maxOccurs=""1"" name=""ID"">
                <xs:complexType>
                  <xs:attribute name=""schemeID"" type=""xs:string"" />
                </xs:complexType>
              </xs:element>
              <xs:element name=""DocumentTypeCode"" type=""xs:string"" />
              <xs:element name=""DocumentDescription"" type=""xs:string"" />
              <xs:element minOccurs=""0"" name=""Attachment"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" name=""EmbeddedDocumentBinaryObject"">
                      <xs:complexType>
                        <xs:attribute name=""mimeCode"" type=""xs:string"" />
                        <xs:attribute name=""filename"" type=""xs:string"" />
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" name=""ExternalReference"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name=""URI"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ProjectReference"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""1"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AccountingSupplier"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""1"" maxOccurs=""1"" name=""AccSellerID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PartyID"" nillable=""true"" type=""xs:string"" />
              <xs:element minOccurs=""0"" name=""CompanyID"" type=""xs:string"" />
              <xs:element name=""CompanyTaxID"" type=""xs:string"" />
              <xs:element minOccurs=""1"" maxOccurs=""1"" name=""SchemeID"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Endpoint"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Party"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Company"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CompanyTax"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Address"">
                <xs:complexType>
                  <xs:sequence minOccurs=""0"" maxOccurs=""1"">
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Name"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Postbox"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AddressFormatCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Street"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Number"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AdditionalStreetName"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Department"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""City"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PostalCode"" type=""xs:string"" />
                    <xs:element name=""CountrySubentity"" type=""xs:string"" />
                    <xs:element name=""AddressLine"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name=""Line"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Country"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Concact"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""1"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Code"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Name"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Telephone"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Telefax"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""E-mail"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Note"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""RFFNADLoop"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""99"" name=""Node"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Code"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Reference"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PartyTaxScheme"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""1"" maxOccurs=""1"" name=""CompanyID"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxScheme"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""1"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PartyLegalEntity"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element name=""RegistrationName"" type=""xs:string"" />
                    <xs:element minOccurs=""1"" maxOccurs=""1"" name=""CompanyID"">
                      <xs:complexType>
                        <xs:attribute name=""schemeID"" type=""xs:string"" />
                      </xs:complexType>
                    </xs:element>
                    <xs:element name=""CompanyLegalForm"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""FinancialInstitution"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CodeQualifier"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AccountHolderIdentifier"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AccountHolderName"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CurrencyIDCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""InstitutionNameCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CodeListIDCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CodeListResponsibleAgencyCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""InstitutionBranchIdentifier"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""InstitutionName"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""InstitutionBranchLocationName"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""1"" maxOccurs=""1"" name=""AccountingCustomer"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""SupplierAssignedAccountID"" type=""xs:string"" />
              <xs:element minOccurs=""1"" maxOccurs=""1"" name=""AccBuyerID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PartyID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CompanyID"" type=""xs:string"" />
              <xs:element name=""CompanyTaxID"" type=""xs:string"" />
              <xs:element minOccurs=""1"" maxOccurs=""1"" name=""SchemeID"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Endpoint"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Party"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Company"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Address"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Name"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AddressFormatCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Postbox"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Street"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Number"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AdditionalStreetName"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Department"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""City"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PostalCode"" type=""xs:string"" />
                    <xs:element name=""CountrySubentity"" type=""xs:string"" />
                    <xs:element name=""AddressLine"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name=""Line"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Country"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Concact"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""1"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" name=""Code"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Name"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Telephone"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Telefax"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""E-mail"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Note"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""RFFNADLoop"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""99"" name=""Node"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Code"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Reference"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" name=""PartyTaxScheme"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""1"" maxOccurs=""1"" name=""CompanyID"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxScheme"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""1"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PartyLegalEntity"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""RegistrationName"" type=""xs:string"" />
                    <xs:element minOccurs=""1"" maxOccurs=""1"" name=""CompanyID"">
                      <xs:complexType>
                        <xs:attribute name=""schemeID"" type=""xs:string"" />
                      </xs:complexType>
                    </xs:element>
                    <xs:element name=""CompanyLegalForm"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""FinancialInstitution"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CodeQualifier"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AccountHolderIdentifier"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AccountHolderName"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CurrencyIDCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""InstitutionNameCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CodeListIDCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CodeListResponsibleAgencyCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""InstitutionBranchIdentifier"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""InstitutionName"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""InstitutionBranchLocationName"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""1"" maxOccurs=""1"" name=""BuyerCustomer"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""BuyerID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PartyID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CompanyID"" type=""xs:string"" />
              <xs:element name=""CompanyTaxID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""SchemeID"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Endpoint"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Party"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Company"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PartyLegalEntity"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""RegistrationName"" type=""xs:string"" />
                    <xs:element maxOccurs=""1"" name=""CompanyID"">
                      <xs:complexType>
                        <xs:attribute name=""schemeID"" type=""xs:string"" />
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CompanyLegalForm"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Address"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element name=""Name"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AddressFormatCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Postbox"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Street"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Number"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" name=""AdditionalStreetName"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Department"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""City"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PostalCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CountrySubentity"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AddressLine"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""1"" maxOccurs=""1"" name=""Line"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Country"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Concact"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""1"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" name=""Code"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Name"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Telephone"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Telefax"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""E-mail"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Note"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""RFFNADLoop"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""99"" name=""Node"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Code"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Reference"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PartyTaxScheme"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""1"" maxOccurs=""1"" name=""CompanyID"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxScheme"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""1"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""FinancialInstitution"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CodeQualifier"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AccountHolderIdentifier"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AccountHolderName"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CurrencyIDCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""InstitutionNameCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CodeListIDCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CodeListResponsibleAgencyCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""InstitutionBranchIdentifier"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""InstitutionName"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""InstitutionBranchLocationName"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""0"" maxOccurs=""1"" name=""DeliveryLocation"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""DeliveryID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PartyID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CompanyID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""SchemeID"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Endpoint"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Party"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Company"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Address"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""1"" maxOccurs=""1"" name=""Name"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AddressFormatCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Street"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Number"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" name=""AdditionalStreetName"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Department"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""MarkAttention"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""City"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PostalCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" name=""CountrySubentity"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" name=""AddressLine"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name=""Line"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Country"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Concact"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""1"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" name=""Code"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Name"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Telephone"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Telefax"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""E-mail"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Note"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""RFFNADLoop"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""99"" name=""Node"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Code"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Reference"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""FinancialInstitution"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CodeQualifier"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AccountHolderIdentifier"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AccountHolderName"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CurrencyIDCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""InstitutionNameCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CodeListIDCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CodeListResponsibleAgencyCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""InstitutionBranchIdentifier"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""InstitutionName"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""InstitutionBranchLocationName"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""0"" maxOccurs=""1"" name=""DeliveryInfo"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ActualDeliveryDate"" type=""xs:date"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""DeliveryTerms"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""SpecialTerms"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""1"" maxOccurs=""1"" name=""PaymentMeans"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PaymentMeansCode"" type=""xs:string"" />
              <xs:element minOccurs=""1"" maxOccurs=""1"" name=""PaymentDueDate"" type=""xs:date"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""InstructionID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""InstructionNote"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PaymentID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CreditAccountID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CardAccount"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""1"" maxOccurs=""1"" name=""PrimaryAccountNumberID"" type=""xs:string"" />
                    <xs:element minOccurs=""1"" maxOccurs=""1"" name=""NetworkID"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""HolderName"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PayeeFinancialAccount"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""1"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Name"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""FinancialInstitutionBranch"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""1"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
                          <xs:element name=""Name"" type=""xs:string"" />
                          <xs:element name=""FinancialInstitution"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name=""ID"" type=""xs:string"" />
                                <xs:element name=""Name"" type=""xs:string"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PaymentMandate"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PayerFinancialAccount"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""1"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Attributes"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PaymentMeansCode"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""name"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""PaymentChannelCode"">
                <xs:complexType>
                  <xs:attribute name=""listAgencyID"" type=""xs:string"" />
                  <xs:attribute name=""listID"" type=""xs:string"" />
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""0"" maxOccurs=""5"" name=""PaymentTerms"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PaymentTermsID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" name=""PaymentMeansID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PrepaidPaymentReferenceID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""2"" name=""Note"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ReferenceEventCode"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""SettlementDiscountPercent"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PenaltySurchargePercent"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Amount"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TermsOfPaymentIdentification"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CodeListQualifier"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CodeListResponsibleAgency"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PaymentTimeReference"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TimeRelation"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TypeOfPeriod"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""NumberOfPeriods"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""SettlementPeriod"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""StartDate"" nillable=""true"" type=""xs:date"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""EndDate"" nillable=""true"" type=""xs:date"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Description"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PenaltyPeriod"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""StartDate"" nillable=""true"" type=""xs:date"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""EndDate"" nillable=""true"" type=""xs:date"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Description"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TermsDiscountDueDate"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Code"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Date"" type=""xs:date"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""DateFormatCode"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TermsNetDueDate"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Code"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Date"" type=""xs:date"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""DateFormatCode"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Percentage"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""1"" maxOccurs=""1"" name=""PercentageCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PercentageValue"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PercentageIDCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CodeListIDCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CodeListAgencyCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""StatusCode"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""MonetaryAmount"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Code"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Amount"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Currency"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PaymentInstructions"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PaymentConditionsCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PaymentGuaranteeMeansCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PaymentMeansCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CodeListIDCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PaymentChannelCode"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""FinancialInstitution"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CodeQualifier"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AccountHolderIdentifier"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AccountHolderName"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CurrencyIDCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""InstitutionNameCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CodeListIDCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CodeListResponsibleAgencyCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""InstitutionBranchIdentifier"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""InstitutionName"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""InstitutionBranchLocationName"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""0"" maxOccurs=""5"" name=""PrepaidPayment"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""0"" name=""ID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" name=""PaidAmount"" type=""xs:unsignedByte"" />
              <xs:element minOccurs=""0"" name=""ReceivedDate"" type=""xs:date"" />
              <xs:element minOccurs=""0"" name=""PaidDate"" type=""xs:date"" />
              <xs:element minOccurs=""0"" name=""PaidTime"" type=""xs:time"" />
              <xs:element minOccurs=""0"" name=""InstructionID"" type=""xs:string"" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""0"" maxOccurs=""unbounded"" name=""AllowanceCharge"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ChargeIndicator"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AllowanceChargeReasonCode"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AllowanceChargeReason"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""MultiplierFactorNumeric"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""MultiplierFactorNumericCode"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""MultiplierFactorNumericIDCode"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PrepaidIndicator"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""SequenceNumeric"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Amount"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""BaseAmount"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AccountingCost"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxCategory"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Percent"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PerUnitAmount"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxScheme"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Navn"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Attributes"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Amount"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CurrencyID"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""BaseAmount"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CurrencyID"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""1"" maxOccurs=""10"" name=""TaxTotal"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""1"" maxOccurs=""1"" name=""TaxAmount"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxAmountCode"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Currency"" type=""xs:string"" />
              <xs:element minOccurs=""0"" name=""TaxEvidenceIndicator"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxSubtotal"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxAmount"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxAmountCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxAmountCurrency"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxableAmount"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxableAmountCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxableAmountCurrency"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxPercent"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxCategory"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Percent"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" name=""TaxExemptionReasonCode"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" name=""TaxExemptionReason"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxScheme"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Navn"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxTypeCode"" type=""xs:string"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""1"" maxOccurs=""1"" name=""Total"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""1"" maxOccurs=""1"" name=""LineTotalAmount"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxExclAmount"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxInclAmount"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AllowanceTotalAmount"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ChargeTotalAmount"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxAmount"" type=""xs:string"" />
              <xs:element minOccurs=""0"" name=""TaxableAmount"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PrepaidAmount"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PayableRoundingAmount"" type=""xs:string"" />
              <xs:element minOccurs=""1"" maxOccurs=""1"" name=""PayableAmount"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""DutyTaxFeeTotalAmount"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""99"" name=""ExtraTotalMOALoop1"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element name=""Node"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Code"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Amount"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Currency"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""Attributes"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""LineTotalAmount"">
                      <xs:complexType>
                        <xs:attribute name=""CurrencyID"" type=""xs:string"" />
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxExclAmount"">
                      <xs:complexType>
                        <xs:attribute name=""CurrencyID"" type=""xs:string"" />
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxInclAmount"">
                      <xs:complexType>
                        <xs:attribute name=""CurrencyID"" type=""xs:string"" />
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AllowanceTotalAmount"">
                      <xs:complexType>
                        <xs:attribute name=""CurrencyID"" type=""xs:string"" />
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ChargeTotalAmount"">
                      <xs:complexType>
                        <xs:attribute name=""CurrencyID"" type=""xs:string"" />
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxAmount"">
                      <xs:complexType>
                        <xs:attribute name=""CurrencyID"" type=""xs:string"" />
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxableAmount"">
                      <xs:complexType>
                        <xs:attribute name=""CurrencyID"" type=""xs:string"" />
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PrepaidAmount"">
                      <xs:complexType>
                        <xs:attribute name=""CurrencyID"" type=""xs:string"" />
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PayableRoundingAmount"">
                      <xs:complexType>
                        <xs:attribute name=""CurrencyID"" type=""xs:string"" />
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PayableAmount"">
                      <xs:complexType>
                        <xs:attribute name=""CurrencyID"" type=""xs:string"" />
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""DutyTaxFeeTotalAmount"">
                      <xs:complexType />
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""1"" maxOccurs=""1"" name=""Lines"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""1"" maxOccurs=""unbounded"" name=""Line"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""1"" maxOccurs=""1"" name=""LineNo"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Note"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""99"" name=""NoteLoop"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" maxOccurs=""99"" name=""Node"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element minOccurs=""0"" maxOccurs=""99"" name=""NoteQualifier"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""99"" name=""NoteFunction"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""99"" name=""NoteLanguage"" type=""xs:date"" />
                                <xs:element minOccurs=""0"" maxOccurs=""99"" name=""Note"" nillable=""true"" type=""xs:date"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""1"" maxOccurs=""1"" name=""Quantity"" type=""xs:string"" />
                    <xs:element minOccurs=""1"" maxOccurs=""1"" name=""UnitCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ExtraQuantityLoop"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" maxOccurs=""99"" name=""Node"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Code"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Quantity"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""UnitCode"" type=""xs:string"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""1"" maxOccurs=""1"" name=""LineAmountTotal"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" name=""Currency"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" name=""AccountingCost"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ExtraMOALoop"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" maxOccurs=""99"" name=""Node"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Code"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Amount"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Currency"" type=""xs:string"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""FreeOfChargeIndicator"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" name=""Delivery"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" name=""ID"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" name=""Quantity"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" name=""UnitCode"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" name=""ConsumerUnit"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" name=""Quantity"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" name=""UnitCode"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""OrderReference"">
                      <xs:complexType>
                        <xs:sequence minOccurs=""1"" maxOccurs=""1"">
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""LineID"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""SalesOrderLineID"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""99"" name=""Node"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Code"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Reference"" type=""xs:string"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" name=""DocumentReference"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name=""ID"">
                            <xs:complexType>
                              <xs:attribute name=""schemeID"" type=""xs:string"" />
                            </xs:complexType>
                          </xs:element>
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""DocumentTypeCode"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""OrderLineReference"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" name=""LineID"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""OrderReference"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name=""ID"">
                                  <xs:complexType>
                                    <xs:attribute name=""SchemeID"" type=""xs:string"" />
                                  </xs:complexType>
                                </xs:element>
                                <xs:element minOccurs=""0"" name=""Reference"" type=""xs:string"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element minOccurs=""0"" name=""SalesOrderLineID"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""1"" maxOccurs=""1"" name=""Item"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""BuyerItemID"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""SellerItemID"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""StandardItemID"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AdditionalItemID"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CatalogueItemID"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""5"" name=""Name"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Description"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PackQuantity"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""UnitCode"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""5"" name=""ExtraPIALoop"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element minOccurs=""0"" maxOccurs=""5"" name=""Node"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element minOccurs=""0"" maxOccurs=""1"" name=""DigitalCode"" type=""xs:string"" />
                                      <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PIA"" type=""xs:string"" />
                                      <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Code"" type=""xs:string"" />
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""SchemeID"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""StdItemID"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""BuyItemID"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""SelItemID"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AddItemID"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CatItemID"" type=""xs:string"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""SchemeAgencyID"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""StdItemID"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""BuyItemID"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""SelItemID"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AddItemID"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CatItemID"" type=""xs:string"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""OriginCountry"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element minOccurs=""1"" maxOccurs=""1"" name=""IdentificationCode"" type=""xs:string"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element minOccurs=""0"" maxOccurs=""5"" name=""CommodityClassification"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element minOccurs=""1"" maxOccurs=""1"" name=""ItemClassificationCode"">
                                  <xs:complexType>
                                    <xs:attribute name=""listID"" type=""xs:string"" />
                                    <xs:attribute name=""listVersionID"" type=""xs:string"" />
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ClassifiedTaxCategory"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element minOccurs=""1"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
                                <xs:element name=""Percent"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxScheme"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element minOccurs=""1"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element minOccurs=""0"" maxOccurs=""5"" name=""AdditionalItemProperty"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element minOccurs=""1"" maxOccurs=""1"" name=""Name"" type=""xs:string"" />
                                <xs:element minOccurs=""1"" maxOccurs=""1"" name=""Value"" type=""xs:string"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""1"" maxOccurs=""1"" name=""PriceNet"">
                      <xs:complexType>
                        <xs:sequence minOccurs=""1"" maxOccurs=""1"">
                          <xs:element minOccurs=""1"" maxOccurs=""1"" name=""Price"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" name=""CurrencyID"" type=""xs:string"" />
                          <xs:element minOccurs=""1"" maxOccurs=""1"" name=""Quantity"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""UnitCode"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PristypeCode"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PristypeTekst"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AllowanceCharge"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element minOccurs=""1"" maxOccurs=""1"" name=""ChargeIndicator"" type=""xs:string"" />
                                <xs:element minOccurs=""1"" maxOccurs=""1"" name=""Amount"">
                                  <xs:complexType>
                                    <xs:attribute name=""currencyID"" type=""xs:string"" />
                                  </xs:complexType>
                                </xs:element>
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""BaseAmount"">
                                  <xs:complexType>
                                    <xs:attribute name=""currencyID"" type=""xs:string"" />
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PriceGross"">
                      <xs:complexType>
                        <xs:sequence minOccurs=""1"" maxOccurs=""1"">
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Price"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" name=""CurrencyID"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Quantity"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""UnitCode"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PristypeCode"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PristypeTekst"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AllowanceCharge"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element minOccurs=""1"" maxOccurs=""1"" name=""ChargeIndicator"" type=""xs:string"" />
                                <xs:element minOccurs=""1"" maxOccurs=""1"" name=""Amount"">
                                  <xs:complexType>
                                    <xs:attribute name=""currencyID"" type=""xs:string"" />
                                  </xs:complexType>
                                </xs:element>
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""BaseAmount"">
                                  <xs:complexType>
                                    <xs:attribute name=""currencyID"" type=""xs:string"" />
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element name=""OrderableUnitFactorRate"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""unbounded"" name=""AllowanceCharge"">
                      <xs:complexType>
                        <xs:sequence minOccurs=""0"" maxOccurs=""1"">
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ChargeIndicator"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AllowanceChargeReasonCode"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AllowanceChargeReason"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""MultiplierFactorNumeric"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""MultiplierFactorNumericCode"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PrepaidIndicator"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""SequenceNumeric"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Amount"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AmountCode"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""BaseAmount"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxCategory"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Percent"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" name=""PerUnitAmount"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxScheme"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
                                      <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Navn"" type=""xs:string"" />
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element name=""Attributes"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name=""Amount"">
                                  <xs:complexType>
                                    <xs:attribute name=""CurrencyID"" type=""xs:string"" />
                                  </xs:complexType>
                                </xs:element>
                                <xs:element name=""BaseAmount"">
                                  <xs:complexType>
                                    <xs:attribute name=""CurrencyID"" type=""xs:string"" />
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""5"" name=""TaxTotal"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" name=""TaxAmount"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" name=""TaxAmountCode"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" name=""Currency"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" name=""TaxEvidenceIndicator"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" name=""TaxSubtotal"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element minOccurs=""0"" name=""TaxCode"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" name=""TaxAmount"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" name=""TaxAmountCode"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" name=""TaxAmountCurrency"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" name=""TaxableAmount"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" name=""TaxableAmountCode"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" name=""TaxableAmountCurrency"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" name=""TaxPercent"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" name=""TaxCategory"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element minOccurs=""0"" name=""ID"" type=""xs:string"" />
                                      <xs:element minOccurs=""0"" name=""Percent"" type=""xs:string"" />
                                      <xs:element minOccurs=""0"" name=""TaxScheme"">
                                        <xs:complexType>
                                          <xs:sequence>
                                            <xs:element minOccurs=""0"" name=""ID"" type=""xs:string"" />
                                            <xs:element minOccurs=""0"" name=""Navn"" type=""xs:string"" />
                                          </xs:sequence>
                                        </xs:complexType>
                                      </xs:element>
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Period"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""FromDate"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ToDate"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Attributes"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""LineAmountTotal"">
                            <xs:complexType>
                              <xs:attribute name=""CurrencyID"" type=""xs:string"" />
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
</xs:schema>";
        
        private const global::BYGE.Integration.Core.Properties.CoreProperties  __DummyVar0 = null;
        
        public Invoice() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [1];
                _RootElements[0] = "Invoice";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
    }
}
