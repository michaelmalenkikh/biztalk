namespace BYGE.Integration.UBLUTS {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"UBLVersionID", @"CustomizationID", @"ProfileID", @"ID", @"CopyIndicator", @"IssueDate", @"UtilityStatementTypeCode", @"Note", @"DocumentCurrencyCode", @"DocumentTypeCode", @"EndpointID", @"Name", @"AddressFormatCode", @"StreetName", @"BuildingNumber", @"CityName", @"PostalZone", @"IdentificationCode", @"CompanyID", @"Telephone", @"ElectronicMail", @"ConsumptionID", @"SpecificationTypeCode", @"MarkAttention", @"TaxAmount", @"TaxableAmount", @"Percent", @"LineExtensionAmount", 
@"PayableAmount", @"InvoicedQuantity", @"StartDate", @"EndDate", @"SubscriberID", @"Description", @"PackQuantity", @"ConsumptionType", @"CurrentChargeType", @"OneTimeChargeType", @"PriceAmount"})]
    public sealed class UBLUTS1 : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xs:schema xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" xmlns:tns=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" attributeFormDefault=""unqualified"" elementFormDefault=""qualified"" targetNamespace=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" xmlns:xs=""http://www.w3.org/2001/XMLSchema"">
  <xs:element name=""UBLVersionID"" type=""xs:decimal"" />
  <xs:element name=""CustomizationID"" type=""xs:string"" />
  <xs:element name=""ProfileID"">
    <xs:complexType>
      <xs:simpleContent>
        <xs:extension base=""xs:string"">
          <xs:attribute name=""schemeAgencyID"" type=""xs:unsignedShort"" use=""required"" />
          <xs:attribute name=""schemeID"" type=""xs:string"" use=""required"" />
        </xs:extension>
      </xs:simpleContent>
    </xs:complexType>
  </xs:element>
  <xs:element name=""ID"">
    <xs:complexType>
      <xs:simpleContent>
        <xs:extension base=""xs:string"">
          <xs:attribute name=""schemeAgencyID"" type=""xs:string"" use=""optional"" />
          <xs:attribute name=""schemeID"" type=""xs:string"" use=""optional"" />
        </xs:extension>
      </xs:simpleContent>
    </xs:complexType>
  </xs:element>
  <xs:element name=""CopyIndicator"" type=""xs:boolean"" />
  <xs:element name=""IssueDate"" type=""xs:date"" />
  <xs:element name=""UtilityStatementTypeCode"">
    <xs:complexType>
      <xs:simpleContent>
        <xs:extension base=""xs:string"">
          <xs:attribute name=""listAgencyID"" type=""xs:unsignedShort"" use=""required"" />
          <xs:attribute name=""listID"" type=""xs:string"" use=""required"" />
        </xs:extension>
      </xs:simpleContent>
    </xs:complexType>
  </xs:element>
  <xs:element name=""Note"" type=""xs:string"" />
  <xs:element name=""DocumentCurrencyCode"" type=""xs:string"" />
  <xs:element name=""DocumentTypeCode"">
    <xs:complexType>
      <xs:simpleContent>
        <xs:extension base=""xs:string"">
          <xs:attribute name=""listAgencyID"" type=""xs:unsignedShort"" use=""required"" />
          <xs:attribute name=""listID"" type=""xs:string"" use=""required"" />
        </xs:extension>
      </xs:simpleContent>
    </xs:complexType>
  </xs:element>
  <xs:element name=""EndpointID"">
    <xs:complexType>
      <xs:simpleContent>
        <xs:extension base=""xs:string"">
          <xs:attribute name=""schemeID"" type=""xs:string"" use=""required"" />
          <xs:attribute name=""schemeAgencyID"" type=""xs:unsignedByte"" use=""optional"" />
        </xs:extension>
      </xs:simpleContent>
    </xs:complexType>
  </xs:element>
  <xs:element name=""Name"" type=""xs:string"" />
  <xs:element name=""AddressFormatCode"">
    <xs:complexType>
      <xs:simpleContent>
        <xs:extension base=""xs:string"">
          <xs:attribute name=""listAgencyID"" type=""xs:unsignedShort"" use=""required"" />
          <xs:attribute name=""listID"" type=""xs:string"" use=""required"" />
        </xs:extension>
      </xs:simpleContent>
    </xs:complexType>
  </xs:element>
  <xs:element name=""StreetName"" type=""xs:string"" />
  <xs:element name=""BuildingNumber"" type=""xs:unsignedByte"" />
  <xs:element name=""CityName"" type=""xs:string"" />
  <xs:element name=""PostalZone"" type=""xs:unsignedShort"" />
  <xs:element name=""IdentificationCode"" type=""xs:string"" />
  <xs:element name=""CompanyID"">
    <xs:complexType>
      <xs:simpleContent>
        <xs:extension base=""xs:string"">
          <xs:attribute name=""schemeID"" type=""xs:string"" use=""required"" />
        </xs:extension>
      </xs:simpleContent>
    </xs:complexType>
  </xs:element>
  <xs:element name=""Telephone"" type=""xs:string"" />
  <xs:element name=""ElectronicMail"" type=""xs:string"" />
  <xs:element name=""ConsumptionID"" type=""xs:string"" />
  <xs:element name=""SpecificationTypeCode"">
    <xs:complexType>
      <xs:simpleContent>
        <xs:extension base=""xs:string"">
          <xs:attribute name=""listAgencyID"" type=""xs:unsignedShort"" use=""required"" />
          <xs:attribute name=""listID"" type=""xs:string"" use=""required"" />
        </xs:extension>
      </xs:simpleContent>
    </xs:complexType>
  </xs:element>
  <xs:element name=""MarkAttention"" type=""xs:string"" />
  <xs:element name=""TaxAmount"">
    <xs:complexType>
      <xs:simpleContent>
        <xs:extension base=""xs:decimal"">
          <xs:attribute name=""currencyID"" type=""xs:string"" use=""required"" />
        </xs:extension>
      </xs:simpleContent>
    </xs:complexType>
  </xs:element>
  <xs:element name=""TaxableAmount"">
    <xs:complexType>
      <xs:simpleContent>
        <xs:extension base=""xs:decimal"">
          <xs:attribute name=""currencyID"" type=""xs:string"" use=""required"" />
        </xs:extension>
      </xs:simpleContent>
    </xs:complexType>
  </xs:element>
  <xs:element name=""Percent"" type=""xs:unsignedByte"" />
  <xs:element name=""LineExtensionAmount"">
    <xs:complexType>
      <xs:simpleContent>
        <xs:extension base=""xs:decimal"">
          <xs:attribute name=""currencyID"" type=""xs:string"" use=""required"" />
        </xs:extension>
      </xs:simpleContent>
    </xs:complexType>
  </xs:element>
  <xs:element name=""PayableAmount"">
    <xs:complexType>
      <xs:simpleContent>
        <xs:extension base=""xs:decimal"">
          <xs:attribute name=""currencyID"" type=""xs:string"" use=""required"" />
        </xs:extension>
      </xs:simpleContent>
    </xs:complexType>
  </xs:element>
  <xs:element name=""InvoicedQuantity"">
    <xs:complexType>
      <xs:simpleContent>
        <xs:extension base=""xs:decimal"">
          <xs:attribute name=""unitCode"" type=""xs:string"" use=""required"" />
        </xs:extension>
      </xs:simpleContent>
    </xs:complexType>
  </xs:element>
  <xs:element name=""StartDate"" type=""xs:date"" />
  <xs:element name=""EndDate"" type=""xs:date"" />
  <xs:element name=""SubscriberID"" type=""xs:string"" />
  <xs:element name=""Description"" type=""xs:string"" />
  <xs:element name=""PackQuantity"">
    <xs:complexType>
      <xs:simpleContent>
        <xs:extension base=""xs:decimal"">
          <xs:attribute name=""unitCode"" type=""xs:string"" use=""required"" />
        </xs:extension>
      </xs:simpleContent>
    </xs:complexType>
  </xs:element>
  <xs:element name=""ConsumptionType"" type=""xs:string"" />
  <xs:element name=""CurrentChargeType"" type=""xs:string"" />
  <xs:element name=""OneTimeChargeType"" type=""xs:string"" />
  <xs:element name=""PriceAmount"">
    <xs:complexType>
      <xs:simpleContent>
        <xs:extension base=""xs:decimal"">
          <xs:attribute name=""currencyID"" type=""xs:string"" use=""required"" />
        </xs:extension>
      </xs:simpleContent>
    </xs:complexType>
  </xs:element>
</xs:schema>";
        
        public UBLUTS1() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [39];
                _RootElements[0] = "UBLVersionID";
                _RootElements[1] = "CustomizationID";
                _RootElements[2] = "ProfileID";
                _RootElements[3] = "ID";
                _RootElements[4] = "CopyIndicator";
                _RootElements[5] = "IssueDate";
                _RootElements[6] = "UtilityStatementTypeCode";
                _RootElements[7] = "Note";
                _RootElements[8] = "DocumentCurrencyCode";
                _RootElements[9] = "DocumentTypeCode";
                _RootElements[10] = "EndpointID";
                _RootElements[11] = "Name";
                _RootElements[12] = "AddressFormatCode";
                _RootElements[13] = "StreetName";
                _RootElements[14] = "BuildingNumber";
                _RootElements[15] = "CityName";
                _RootElements[16] = "PostalZone";
                _RootElements[17] = "IdentificationCode";
                _RootElements[18] = "CompanyID";
                _RootElements[19] = "Telephone";
                _RootElements[20] = "ElectronicMail";
                _RootElements[21] = "ConsumptionID";
                _RootElements[22] = "SpecificationTypeCode";
                _RootElements[23] = "MarkAttention";
                _RootElements[24] = "TaxAmount";
                _RootElements[25] = "TaxableAmount";
                _RootElements[26] = "Percent";
                _RootElements[27] = "LineExtensionAmount";
                _RootElements[28] = "PayableAmount";
                _RootElements[29] = "InvoicedQuantity";
                _RootElements[30] = "StartDate";
                _RootElements[31] = "EndDate";
                _RootElements[32] = "SubscriberID";
                _RootElements[33] = "Description";
                _RootElements[34] = "PackQuantity";
                _RootElements[35] = "ConsumptionType";
                _RootElements[36] = "CurrentChargeType";
                _RootElements[37] = "OneTimeChargeType";
                _RootElements[38] = "PriceAmount";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"UBLVersionID")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"UBLVersionID"})]
        public sealed class UBLVersionID : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public UBLVersionID() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "UBLVersionID";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"CustomizationID")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"CustomizationID"})]
        public sealed class CustomizationID : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public CustomizationID() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "CustomizationID";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"ProfileID")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"ProfileID"})]
        public sealed class ProfileID : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public ProfileID() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "ProfileID";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"ID")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"ID"})]
        public sealed class ID : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public ID() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "ID";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"CopyIndicator")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"CopyIndicator"})]
        public sealed class CopyIndicator : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public CopyIndicator() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "CopyIndicator";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"IssueDate")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"IssueDate"})]
        public sealed class IssueDate : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public IssueDate() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "IssueDate";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"UtilityStatementTypeCode")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"UtilityStatementTypeCode"})]
        public sealed class UtilityStatementTypeCode : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public UtilityStatementTypeCode() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "UtilityStatementTypeCode";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"Note")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"Note"})]
        public sealed class Note : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public Note() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "Note";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"DocumentCurrencyCode")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"DocumentCurrencyCode"})]
        public sealed class DocumentCurrencyCode : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public DocumentCurrencyCode() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "DocumentCurrencyCode";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"DocumentTypeCode")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"DocumentTypeCode"})]
        public sealed class DocumentTypeCode : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public DocumentTypeCode() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "DocumentTypeCode";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"EndpointID")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"EndpointID"})]
        public sealed class EndpointID : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public EndpointID() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "EndpointID";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"Name")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"Name"})]
        public sealed class Name : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public Name() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "Name";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"AddressFormatCode")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"AddressFormatCode"})]
        public sealed class AddressFormatCode : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public AddressFormatCode() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "AddressFormatCode";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"StreetName")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"StreetName"})]
        public sealed class StreetName : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public StreetName() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "StreetName";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"BuildingNumber")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"BuildingNumber"})]
        public sealed class BuildingNumber : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public BuildingNumber() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "BuildingNumber";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"CityName")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"CityName"})]
        public sealed class CityName : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public CityName() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "CityName";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"PostalZone")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"PostalZone"})]
        public sealed class PostalZone : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public PostalZone() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "PostalZone";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"IdentificationCode")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"IdentificationCode"})]
        public sealed class IdentificationCode : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public IdentificationCode() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "IdentificationCode";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"CompanyID")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"CompanyID"})]
        public sealed class CompanyID : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public CompanyID() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "CompanyID";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"Telephone")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"Telephone"})]
        public sealed class Telephone : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public Telephone() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "Telephone";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"ElectronicMail")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"ElectronicMail"})]
        public sealed class ElectronicMail : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public ElectronicMail() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "ElectronicMail";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"ConsumptionID")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"ConsumptionID"})]
        public sealed class ConsumptionID : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public ConsumptionID() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "ConsumptionID";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"SpecificationTypeCode")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"SpecificationTypeCode"})]
        public sealed class SpecificationTypeCode : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public SpecificationTypeCode() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "SpecificationTypeCode";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"MarkAttention")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"MarkAttention"})]
        public sealed class MarkAttention : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public MarkAttention() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "MarkAttention";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"TaxAmount")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"TaxAmount"})]
        public sealed class TaxAmount : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public TaxAmount() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "TaxAmount";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"TaxableAmount")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"TaxableAmount"})]
        public sealed class TaxableAmount : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public TaxableAmount() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "TaxableAmount";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"Percent")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"Percent"})]
        public sealed class Percent : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public Percent() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "Percent";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"LineExtensionAmount")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"LineExtensionAmount"})]
        public sealed class LineExtensionAmount : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public LineExtensionAmount() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "LineExtensionAmount";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"PayableAmount")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"PayableAmount"})]
        public sealed class PayableAmount : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public PayableAmount() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "PayableAmount";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"InvoicedQuantity")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"InvoicedQuantity"})]
        public sealed class InvoicedQuantity : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public InvoicedQuantity() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "InvoicedQuantity";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"StartDate")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"StartDate"})]
        public sealed class StartDate : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public StartDate() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "StartDate";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"EndDate")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"EndDate"})]
        public sealed class EndDate : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public EndDate() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "EndDate";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"SubscriberID")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"SubscriberID"})]
        public sealed class SubscriberID : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public SubscriberID() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "SubscriberID";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"Description")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"Description"})]
        public sealed class Description : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public Description() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "Description";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"PackQuantity")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"PackQuantity"})]
        public sealed class PackQuantity : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public PackQuantity() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "PackQuantity";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"ConsumptionType")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"ConsumptionType"})]
        public sealed class ConsumptionType : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public ConsumptionType() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "ConsumptionType";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"CurrentChargeType")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"CurrentChargeType"})]
        public sealed class CurrentChargeType : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public CurrentChargeType() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "CurrentChargeType";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"OneTimeChargeType")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"OneTimeChargeType"})]
        public sealed class OneTimeChargeType : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public OneTimeChargeType() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "OneTimeChargeType";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2",@"PriceAmount")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"PriceAmount"})]
        public sealed class PriceAmount : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public PriceAmount() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "PriceAmount";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
    }
}
