namespace BYGE.Integration.UBLUTS {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"ParentDocumentReference", @"SenderParty", @"ReceiverParty", @"SubscriberConsumption"})]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.UBLUTS.UBLUTS1", typeof(global::BYGE.Integration.UBLUTS.UBLUTS1))]
    public sealed class UBLUTS2 : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xs:schema xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" xmlns:tns=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonAggregateComponents-2"" attributeFormDefault=""unqualified"" elementFormDefault=""qualified"" targetNamespace=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonAggregateComponents-2"" xmlns:xs=""http://www.w3.org/2001/XMLSchema"">
  <xs:import schemaLocation=""BYGE.Integration.UBLUTS.UBLUTS1"" namespace=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" />
  <xs:annotation>
    <xs:appinfo>
      <b:references>
        <b:reference targetNamespace=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" />
      </b:references>
    </xs:appinfo>
  </xs:annotation>
  <xs:element name=""ParentDocumentReference"">
    <xs:complexType>
      <xs:sequence>
        <xs:element xmlns:q1=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:ID"" />
        <xs:element xmlns:q2=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q2:IssueDate"" />
        <xs:element xmlns:q3=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q3:DocumentTypeCode"" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name=""SenderParty"">
    <xs:complexType>
      <xs:sequence>
        <xs:element xmlns:q4=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q4:EndpointID"" />
        <xs:element name=""PartyIdentification"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q5=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q5:ID"" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name=""PartyName"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q6=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q6:Name"" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name=""PostalAddress"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q7=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q7:AddressFormatCode"" />
              <xs:element xmlns:q8=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q8:StreetName"" />
              <xs:element xmlns:q9=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q9:BuildingNumber"" />
              <xs:element xmlns:q10=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q10:CityName"" />
              <xs:element xmlns:q11=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q11:PostalZone"" />
              <xs:element name=""Country"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q12=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q12:IdentificationCode"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name=""PartyTaxScheme"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q13=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q13:CompanyID"" />
              <xs:element name=""TaxScheme"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q14=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q14:ID"" />
                    <xs:element xmlns:q15=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q15:Name"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name=""PartyLegalEntity"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q16=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q16:CompanyID"" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name=""Contact"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q17=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q17:ID"" />
              <xs:element xmlns:q18=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q18:Name"" />
              <xs:element xmlns:q19=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q19:Telephone"" />
              <xs:element xmlns:q20=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q20:ElectronicMail"" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name=""ReceiverParty"">
    <xs:complexType>
      <xs:sequence>
        <xs:element xmlns:q21=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q21:EndpointID"" />
        <xs:element name=""PartyIdentification"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q22=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q22:ID"" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name=""PartyName"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q23=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q23:Name"" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name=""PostalAddress"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q24=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q24:AddressFormatCode"" />
              <xs:element xmlns:q25=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q25:StreetName"" />
              <xs:element xmlns:q26=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q26:BuildingNumber"" />
              <xs:element xmlns:q27=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q27:CityName"" />
              <xs:element xmlns:q28=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q28:PostalZone"" />
              <xs:element name=""Country"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q29=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q29:IdentificationCode"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name=""Contact"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q30=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q30:ID"" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name=""SubscriberConsumption"">
    <xs:complexType>
      <xs:sequence>
        <xs:element xmlns:q31=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q31:ConsumptionID"" />
        <xs:element xmlns:q32=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q32:SpecificationTypeCode"" />
        <xs:element name=""UtilityConsumptionPoint"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q33=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q33:ID"" />
              <xs:element name=""Address"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q34=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q34:AddressFormatCode"" />
                    <xs:element xmlns:q35=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q35:StreetName"" />
                    <xs:element xmlns:q36=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q36:BuildingNumber"" />
                    <xs:element xmlns:q37=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q37:MarkAttention"" />
                    <xs:element xmlns:q38=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q38:CityName"" />
                    <xs:element xmlns:q39=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q39:PostalZone"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name=""Consumption"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q40=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q40:UtilityStatementTypeCode"" />
              <xs:element name=""TaxTotal"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q41=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q41:TaxAmount"" />
                    <xs:element maxOccurs=""unbounded"" name=""TaxSubtotal"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element xmlns:q42=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q42:TaxableAmount"" />
                          <xs:element xmlns:q43=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q43:TaxAmount"" />
                          <xs:element name=""TaxCategory"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element xmlns:q44=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q44:ID"" />
                                <xs:element xmlns:q45=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q45:Percent"" />
                                <xs:element name=""TaxScheme"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element xmlns:q46=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q46:ID"" />
                                      <xs:element xmlns:q47=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q47:Name"" />
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""LegalMonetaryTotal"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q48=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q48:LineExtensionAmount"" />
                    <xs:element xmlns:q49=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q49:PayableAmount"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element maxOccurs=""unbounded"" name=""SupplierConsumption"">
          <xs:complexType>
            <xs:sequence>
              <xs:element name=""Consumption"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element name=""TaxTotal"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element xmlns:q50=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q50:TaxAmount"" />
                          <xs:element maxOccurs=""unbounded"" name=""TaxSubtotal"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element xmlns:q51=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q51:TaxableAmount"" />
                                <xs:element xmlns:q52=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q52:TaxAmount"" />
                                <xs:element name=""TaxCategory"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element xmlns:q53=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q53:ID"" />
                                      <xs:element xmlns:q54=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q54:Percent"" />
                                      <xs:element name=""TaxScheme"">
                                        <xs:complexType>
                                          <xs:sequence>
                                            <xs:element xmlns:q55=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q55:ID"" />
                                            <xs:element xmlns:q56=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q56:Name"" />
                                          </xs:sequence>
                                        </xs:complexType>
                                      </xs:element>
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element name=""LegalMonetaryTotal"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element xmlns:q57=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q57:LineExtensionAmount"" />
                          <xs:element xmlns:q58=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q58:PayableAmount"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element maxOccurs=""unbounded"" name=""ConsumptionLine"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q59=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q59:ID"" />
                    <xs:element xmlns:q60=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q60:InvoicedQuantity"" />
                    <xs:element xmlns:q61=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q61:LineExtensionAmount"" />
                    <xs:element name=""Period"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element xmlns:q62=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q62:StartDate"" />
                          <xs:element xmlns:q63=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q63:EndDate"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element name=""UtilityItem"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element xmlns:q64=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q64:ID"" />
                          <xs:element xmlns:q65=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q65:SubscriberID"" />
                          <xs:element xmlns:q66=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q66:Description"" />
                          <xs:element xmlns:q67=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q67:PackQuantity"" />
                          <xs:element xmlns:q68=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q68:ConsumptionType"" />
                          <xs:element xmlns:q69=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q69:CurrentChargeType"" />
                          <xs:element xmlns:q70=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q70:OneTimeChargeType"" />
                          <xs:element name=""TaxCategory"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element xmlns:q71=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q71:ID"" />
                                <xs:element xmlns:q72=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q72:Percent"" />
                                <xs:element name=""TaxScheme"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element xmlns:q73=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q73:ID"" />
                                      <xs:element xmlns:q74=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q74:Name"" />
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element name=""Price"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element xmlns:q75=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" ref=""q75:PriceAmount"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
</xs:schema>";
        
        public UBLUTS2() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [4];
                _RootElements[0] = "ParentDocumentReference";
                _RootElements[1] = "SenderParty";
                _RootElements[2] = "ReceiverParty";
                _RootElements[3] = "SubscriberConsumption";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonAggregateComponents-2",@"ParentDocumentReference")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"ParentDocumentReference"})]
        public sealed class ParentDocumentReference : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public ParentDocumentReference() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "ParentDocumentReference";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonAggregateComponents-2",@"SenderParty")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"SenderParty"})]
        public sealed class SenderParty : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public SenderParty() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "SenderParty";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonAggregateComponents-2",@"ReceiverParty")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"ReceiverParty"})]
        public sealed class ReceiverParty : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public ReceiverParty() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "ReceiverParty";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:CommonAggregateComponents-2",@"SubscriberConsumption")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"SubscriberConsumption"})]
        public sealed class SubscriberConsumption : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public SubscriberConsumption() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "SubscriberConsumption";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
    }
}
