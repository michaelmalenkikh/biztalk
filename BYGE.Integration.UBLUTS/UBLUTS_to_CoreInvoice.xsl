<?xml version="1.0" encoding="UTF-16"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:var="http://schemas.microsoft.com/BizTalk/2003/var" exclude-result-prefixes="msxsl var s2 s1 s0" version="1.0" xmlns:s1="urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2" xmlns:s2="urn:oioubl:names:specification:oioubl:schema:xsd:UtilityStatement-2" xmlns:ns0="http://byg-e.dk/schemas/v10" xmlns:s0="urn:oioubl:names:specification:oioubl:schema:xsd:CommonAggregateComponents-2">
  <xsl:output omit-xml-declaration="yes" method="xml" version="1.0" />
  <!-- This mapping is being used. Make all adjustments here -->
  <!-- BDB/SJ 200124: Only use the first SubscriberConsumption. Use all Lines in UTS to the first SubscriberConsumption  -->
  <!-- BDB/SJ 200124: In BitBucket but not finish  -->
  <!-- BDB/SJ 200225: for-each SubscriberConsumption, SupplierConsumption/ConsumptionLine is added -->
  <!-- BDB/SJ 200320: Sum() added  -->
  <xsl:template match="/">
    <xsl:apply-templates select="/s2:UtilityStatement" />
  </xsl:template>
  <xsl:template match="/s2:UtilityStatement">
    <ns0:Invoice>
      <ProfileID>
        <xsl:value-of select="s1:ProfileID/text()" />
      </ProfileID>
      <InvoiceID>
        <xsl:value-of select="s1:ID/text()" />
      </InvoiceID>
      <CopyIndicator>
        <xsl:value-of select="s1:CopyIndicator/text()" />
      </CopyIndicator>
      <IssueDate>
        <xsl:value-of select="s1:IssueDate/text()" />
      </IssueDate>
      <InvoiceType>
        <xsl:value-of select="s1:UtilityStatementTypeCode/text()" />
      </InvoiceType>
      <Note>
        <xsl:value-of select="s1:Note/text()" />
      </Note>
      <Currency>
        <xsl:value-of select="s1:DocumentCurrencyCode/text()" />
      </Currency>
      <OrderReference>
        <OrderID>
          <xsl:value-of select="s0:ParentDocumentReference/s1:ID/text()" />
        </OrderID>
        <OrderDate>
          <xsl:value-of select="s0:ParentDocumentReference/s1:IssueDate/text()" />
        </OrderDate>
      </OrderReference>
      <AccountingSupplier>
        <AccSellerID>
          <xsl:value-of select="s0:SenderParty/s1:EndpointID/text()" />
        </AccSellerID>
        <PartyID>
          <xsl:value-of select="s0:SenderParty/s0:PartyIdentification/s1:ID/text()" />
        </PartyID>
        <CompanyID>
          <xsl:value-of select="s0:SenderParty/s0:PartyLegalEntity/s1:CompanyID/text()" />
        </CompanyID>
        <CompanyTaxID>
          <xsl:value-of select="s0:SenderParty/s0:PartyTaxScheme/s1:CompanyID/text()" />
        </CompanyTaxID>
        <SchemeID>
          <Endpoint>
            <xsl:value-of select="s0:SenderParty/s1:EndpointID/@schemeID" />
          </Endpoint>
          <xsl:if test="s0:SenderParty/s0:PartyIdentification/s1:ID/@schemeID">
            <Party>
              <xsl:value-of select="s0:SenderParty/s0:PartyIdentification/s1:ID/@schemeID" />
            </Party>
          </xsl:if>
          <Company>
            <xsl:value-of select="s0:SenderParty/s0:PartyLegalEntity/s1:CompanyID/@schemeID" />
          </Company>
          <CompanyTax>
            <xsl:value-of select="s0:SenderParty/s0:PartyTaxScheme/s1:CompanyID/@schemeID" />
          </CompanyTax>
        </SchemeID>
        <Address>
          <Name>
            <xsl:value-of select="s0:SenderParty/s0:PartyName/s1:Name/text()" />
          </Name>
          <AddressFormatCode>
            <xsl:value-of select="s0:SenderParty/s0:PostalAddress/s1:AddressFormatCode/text()" />
          </AddressFormatCode>
          <Street>
            <xsl:value-of select="s0:SenderParty/s0:PostalAddress/s1:StreetName/text()" />
          </Street>
          <Number>
            <xsl:value-of select="s0:SenderParty/s0:PostalAddress/s1:BuildingNumber/text()" />
          </Number>
          <City>
            <xsl:value-of select="s0:SenderParty/s0:PostalAddress/s1:CityName/text()" />
          </City>
          <PostalCode>
            <xsl:value-of select="s0:SenderParty/s0:PostalAddress/s1:PostalZone/text()" />
          </PostalCode>
          <Country>
            <xsl:value-of select="s0:SenderParty/s0:PostalAddress/s0:Country/s1:IdentificationCode/text()" />
          </Country>
        </Address>
        <Concact>
          <ID>
            <xsl:value-of select="s0:SenderParty/s0:Contact/s1:ID/text()" />
          </ID>
          <Name>
            <xsl:value-of select="s0:SenderParty/s0:Contact/s1:Name/text()" />
          </Name>
          <Telephone>
            <xsl:value-of select="s0:SenderParty/s0:Contact/s1:Telephone/text()" />
          </Telephone>
          <E-mail>
            <xsl:value-of select="s0:SenderParty/s0:Contact/s1:ElectronicMail/text()" />
          </E-mail>
        </Concact>
      </AccountingSupplier>
      <AccountingCustomer>
        <AccBuyerID>
          <xsl:value-of select="s0:ReceiverParty/s1:EndpointID/text()" />
        </AccBuyerID>
        <PartyID>
          <xsl:value-of select="s0:ReceiverParty/s0:PartyIdentification/s1:ID/text()" />
        </PartyID>
        <SchemeID>
          <Endpoint>
            <xsl:value-of select="s0:ReceiverParty/s1:EndpointID/@schemeID" />
          </Endpoint>
          <xsl:if test="s0:ReceiverParty/s0:PartyIdentification/s1:ID/@schemeID">
            <Party>
              <xsl:value-of select="s0:ReceiverParty/s0:PartyIdentification/s1:ID/@schemeID" />
            </Party>
          </xsl:if>
        </SchemeID>
        <Address>
          <Name>
            <xsl:value-of select="s0:ReceiverParty/s0:PartyName/s1:Name/text()" />
          </Name>
          <Street>
            <xsl:value-of select="s0:ReceiverParty/s0:PostalAddress/s1:StreetName/text()" />
          </Street>
          <Number>
            <xsl:value-of select="s0:ReceiverParty/s0:PostalAddress/s1:BuildingNumber/text()" />
          </Number>
          <AdditionalStreetName>
            <xsl:value-of select="s0:ReceiverParty/s0:PostalAddress/s1:AddressFormatCode/text()" />
          </AdditionalStreetName>
          <City>
            <xsl:value-of select="s0:ReceiverParty/s0:PostalAddress/s1:CityName/text()" />
          </City>
          <PostalCode>
            <xsl:value-of select="s0:ReceiverParty/s0:PostalAddress/s1:PostalZone/text()" />
          </PostalCode>
          <Country>
            <xsl:value-of select="s0:ReceiverParty/s0:PostalAddress/s0:Country/s1:IdentificationCode/text()" />
          </Country>
        </Address>
        <Concact>
          <ID>
            <xsl:value-of select="s0:ReceiverParty/s0:Contact/s1:ID/text()" />
          </ID>
        </Concact>
      </AccountingCustomer>
      <BuyerCustomer>
        <BuyerID>
          <xsl:value-of select="s0:ReceiverParty/s1:EndpointID/text()" />
        </BuyerID>
        <PartyID>
          <xsl:value-of select="s0:ReceiverParty/s0:PartyIdentification/s1:ID/text()" />
        </PartyID>
        <SchemeID>
          <Endpoint>
            <xsl:value-of select="s0:ReceiverParty/s1:EndpointID/@schemeID" />
          </Endpoint>
          <xsl:if test="s0:ReceiverParty/s0:PartyIdentification/s1:ID/@schemeID">
            <Party>
              <xsl:value-of select="s0:ReceiverParty/s0:PartyIdentification/s1:ID/@schemeID" />
            </Party>
          </xsl:if>
        </SchemeID>
        <Address>
          <Name>
            <xsl:value-of select="s0:ReceiverParty/s0:PartyName/s1:Name/text()" />
          </Name>
          <AddressFormatCode>
            <xsl:value-of select="s0:ReceiverParty/s0:PostalAddress/s1:AddressFormatCode/text()" />
          </AddressFormatCode>
          <Street>
            <xsl:value-of select="s0:ReceiverParty/s0:PostalAddress/s1:StreetName/text()" />
          </Street>
          <Number>
            <xsl:value-of select="s0:ReceiverParty/s0:PostalAddress/s1:BuildingNumber/text()" />
          </Number>
          <City>
            <xsl:value-of select="s0:ReceiverParty/s0:PostalAddress/s1:CityName/text()" />
          </City>
          <PostalCode>
            <xsl:value-of select="s0:ReceiverParty/s0:PostalAddress/s1:PostalZone/text()" />
          </PostalCode>
          <Country>
            <xsl:value-of select="s0:ReceiverParty/s0:PostalAddress/s0:Country/s1:IdentificationCode/text()" />
          </Country>
        </Address>
        <Concact>
          <ID>
            <xsl:value-of select="s0:ReceiverParty/s0:Contact/s1:ID/text()" />
          </ID>
        </Concact>
      </BuyerCustomer>
      <!-- BDB/SJ 200124: Only use the first SubscriberConsumption. -->
      <DeliveryLocation>
        <DeliveryID>          
          <xsl:value-of select="s0:SubscriberConsumption[1]/s1:ConsumptionID/text()" />
        </DeliveryID>
        <Address>
          <Name>
            <xsl:value-of select="s0:SubscriberConsumption[1]/s0:UtilityConsumptionPoint/s0:Address/s1:MarkAttention/text()" />
          </Name>
          <AddressFormatCode>
            <xsl:value-of select="s0:SubscriberConsumption[1]/s0:UtilityConsumptionPoint/s0:Address/s1:AddressFormatCode/text()" />
          </AddressFormatCode>
          <Street>
            <xsl:value-of select="s0:SubscriberConsumption[1]/s0:UtilityConsumptionPoint/s0:Address/s1:StreetName/text()" />
          </Street>
          <Number>
            <xsl:value-of select="s0:SubscriberConsumption[1]/s0:UtilityConsumptionPoint/s0:Address/s1:BuildingNumber/text()" />
          </Number>
          <City>
            <xsl:value-of select="s0:SubscriberConsumption[1]/s0:UtilityConsumptionPoint/s0:Address/s1:CityName/text()" />
          </City>
          <PostalCode>
            <xsl:value-of select="s0:SubscriberConsumption[1]/s0:UtilityConsumptionPoint/s0:Address/s1:PostalZone/text()" />
          </PostalCode>
        </Address>
      </DeliveryLocation>
      <!-- BDB/SJ 200320: Sum() added  -->
      <TaxTotal>
        <TaxAmount>
          <xsl:value-of select="sum(//s0:SubscriberConsumption/s0:SupplierConsumption/s0:Consumption/s0:TaxTotal/s1:TaxAmount)" />
        </TaxAmount>
        <TaxSubtotal>
          <TaxAmount>
            <xsl:value-of select="sum(//s0:SubscriberConsumption/s0:SupplierConsumption/s0:Consumption/s0:TaxTotal/s0:TaxSubtotal/s1:TaxAmount)" />
          </TaxAmount>
          <TaxableAmount>
            <xsl:value-of select="sum(//s0:SubscriberConsumption/s0:SupplierConsumption/s0:Consumption/s0:TaxTotal/s0:TaxSubtotal/s1:TaxableAmount)" />
          </TaxableAmount>
          <TaxPercent>
            <xsl:value-of select="s0:SubscriberConsumption[1]/s0:SupplierConsumption/s0:Consumption/s0:TaxTotal/s0:TaxSubtotal/s0:TaxCategory/s1:Percent/text()" />
          </TaxPercent>
          <TaxCategory>
            <ID>
              <xsl:value-of select="s0:SubscriberConsumption[1]/s0:Consumption/s0:TaxTotal/s0:TaxSubtotal/s0:TaxCategory/s1:ID/text()" />              
            </ID>
            <Percent>
              <xsl:value-of select="s0:SubscriberConsumption[1]/s0:Consumption/s0:TaxTotal/s0:TaxSubtotal/s0:TaxCategory/s1:Percent/text()" />
            </Percent>
            <TaxScheme>
              <ID>
                <xsl:value-of select="s0:SubscriberConsumption[1]/s0:Consumption/s0:TaxTotal/s0:TaxSubtotal/s0:TaxCategory/s0:TaxScheme/s1:ID/text()" />
              </ID>
              <Navn>
                <xsl:value-of select="s0:SubscriberConsumption[1]/s0:Consumption/s0:TaxTotal/s0:TaxSubtotal/s0:TaxCategory/s0:TaxScheme/s1:Name/text()" />
              </Navn>
              <xsl:if test="s0:SubscriberConsumption[1]/s0:Consumption/s0:TaxTotal/s0:TaxSubtotal/s0:TaxCategory/s0:TaxScheme/s1:ID/@schemeID">
                <TaxTypeCode>
                  <xsl:value-of select="s0:SubscriberConsumption[1]/s0:Consumption/s0:TaxTotal/s0:TaxSubtotal/s0:TaxCategory/s0:TaxScheme/s1:ID/@schemeID" />
                </TaxTypeCode>
              </xsl:if>
            </TaxScheme>
          </TaxCategory>
        </TaxSubtotal>
      </TaxTotal>
      <!-- BDB/SJ 200320: Sum() added  -->
      <Total>
        <LineTotalAmount>
          <xsl:value-of select="sum(//s0:SubscriberConsumption/s0:SupplierConsumption/s0:Consumption/s0:LegalMonetaryTotal/s1:LineExtensionAmount)" />
        </LineTotalAmount>
        <TaxExclAmount>
          <xsl:value-of select="sum(//s0:SubscriberConsumption/s0:SupplierConsumption/s0:Consumption/s0:TaxTotal/s1:TaxAmount)" />
        </TaxExclAmount>
        <PayableAmount>
          <xsl:value-of select="sum(//s0:SubscriberConsumption/s0:SupplierConsumption/s0:Consumption/s0:LegalMonetaryTotal/s1:PayableAmount)" />
        </PayableAmount>
      </Total>  
      <!-- BDB/SJ 200124: Use all Lines in UTS to the first SubscriberConsumption  -->
      <!-- BDB/SJ 200225: for-each SubscriberConsumption, SupplierConsumption/ConsumptionLine is added -->
      <Lines>
        <xsl:for-each select="s0:SubscriberConsumption/s0:SupplierConsumption/s0:ConsumptionLine">
          <Line>
            <LineNo>
              <xsl:value-of select="s1:ID/text()" />
            </LineNo>
            <Quantity>
              <xsl:value-of select="s1:InvoicedQuantity/text()" />
            </Quantity>
            <UnitCode>
              <xsl:value-of select="s1:InvoicedQuantity/@unitCode" />
            </UnitCode>
            <LineAmountTotal>
              <xsl:value-of select="s1:LineExtensionAmount/text()" />
            </LineAmountTotal>
            <OrderLineReference>
              <LineID>
                <xsl:value-of select="s0:UtilityItem/s1:SubscriberID/text()" />
              </LineID>
            </OrderLineReference>
            <Item>
              <SellerItemID>
                <xsl:value-of select="s0:UtilityItem/s1:ID/text()" />
              </SellerItemID>
              <Description>
                <xsl:value-of select="s0:UtilityItem/s1:Description/text()" />
              </Description>
              <PackQuantity>
                <xsl:value-of select="s0:UtilityItem/s1:PackQuantity/text()" />
              </PackQuantity>
              <UnitCode>
                <xsl:value-of select="s0:UtilityItem/s1:PackQuantity/@unitCode" />
              </UnitCode>
            </Item>
            <PriceNet>
              <Price>
                <xsl:value-of select="s0:Price/s1:PriceAmount/text()" />
              </Price>
            </PriceNet>
            <PriceGross>
              <Price>
                <xsl:value-of select="s0:Price/s1:PriceAmount/text()" />
              </Price>
            </PriceGross>
            <Period>
              <ID>
                <xsl:value-of select="s0:UtilityItem/s1:ConsumptionType/text()" />
              </ID>
              <FromDate>
                <xsl:value-of select="s0:Period/s1:StartDate/text()" />
              </FromDate>
              <ToDate>
                <xsl:value-of select="s0:Period/s1:EndDate/text()" />
              </ToDate>
            </Period>
          </Line>
        </xsl:for-each>
      </Lines>
    </ns0:Invoice>
  </xsl:template>
</xsl:stylesheet>