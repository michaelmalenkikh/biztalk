namespace BYGE.Integration.UBLUTS {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [Schema(@"urn:oioubl:names:specification:oioubl:schema:xsd:UtilityStatement-2",@"UtilityStatement")]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"UtilityStatement"})]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.UBLUTS.UBLUTS1", typeof(global::BYGE.Integration.UBLUTS.UBLUTS1))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.UBLUTS.UBLUTS2", typeof(global::BYGE.Integration.UBLUTS.UBLUTS2))]
    public sealed class UBLUTS : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xs:schema xmlns:soapenv=""http://www.w3.org/2003/05/soap-envelope"" xmlns:cbc=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" xmlns:sdt=""urn:oioubl:names:specification:oioubl:schema:xsd:SpecializedDatatypes-2"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:udt=""urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2"" xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" xmlns:xenc=""http://www.w3.org/2001/04/xmlenc#"" xmlns:cac=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonAggregateComponents-2"" xmlns:wsu=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"" xmlns:ccts=""urn:un:unece:uncefact:documentation:2"" attributeFormDefault=""unqualified"" elementFormDefault=""qualified"" targetNamespace=""urn:oioubl:names:specification:oioubl:schema:xsd:UtilityStatement-2"" xmlns:xs=""http://www.w3.org/2001/XMLSchema"">
  <xs:import schemaLocation=""BYGE.Integration.UBLUTS.UBLUTS1"" namespace=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" />
  <xs:import schemaLocation=""BYGE.Integration.UBLUTS.UBLUTS2"" namespace=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonAggregateComponents-2"" />
  <xs:annotation>
    <xs:appinfo>
      <b:references>
        <b:reference targetNamespace=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonBasicComponents-2"" />
        <b:reference targetNamespace=""urn:oioubl:names:specification:oioubl:schema:xsd:CommonAggregateComponents-2"" />
      </b:references>
    </xs:appinfo>
  </xs:annotation>
  <xs:element name=""UtilityStatement"">
    <xs:complexType>
      <xs:sequence>
        <xs:element ref=""cbc:UBLVersionID"" />
        <xs:element ref=""cbc:CustomizationID"" />
        <xs:element ref=""cbc:ProfileID"" />
        <xs:element ref=""cbc:ID"" />
        <xs:element ref=""cbc:CopyIndicator"" />
        <xs:element ref=""cbc:IssueDate"" />
        <xs:element ref=""cbc:UtilityStatementTypeCode"" />
        <xs:element ref=""cbc:Note"" />
        <xs:element ref=""cbc:DocumentCurrencyCode"" />
        <xs:element ref=""cac:ParentDocumentReference"" />
        <xs:element ref=""cac:SenderParty"" />
        <xs:element ref=""cac:ReceiverParty"" />
        <xs:element maxOccurs=""unbounded"" ref=""cac:SubscriberConsumption"" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>
</xs:schema>";
        
        public UBLUTS() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [1];
                _RootElements[0] = "UtilityStatement";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
    }
}
