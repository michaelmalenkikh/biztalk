namespace BYGE.Integration.UBLCreditnote {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.UBLCreditnote.UBLCreditnote", typeof(global::BYGE.Integration.UBLCreditnote.UBLCreditnote))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.Core.Invoice.Invoice", typeof(global::BYGE.Integration.Core.Invoice.Invoice))]
    public sealed class UBLCreditnote_to_CoreInvoice : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var s2 s1 s0 userCSharp"" version=""1.0"" xmlns:s2=""urn:oasis:names:specification:ubl:schema:xsd:CreditNote-2"" xmlns:ns0=""http://byg-e.dk/schemas/v10"" xmlns:s1=""urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"" xmlns:s0=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" xmlns:userCSharp=""http://schemas.microsoft.com/BizTalk/2003/userCSharp"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/s2:CreditNote"" />
  </xsl:template>
  <xsl:template match=""/s2:CreditNote"">
    <xsl:variable name=""var:v1"" select=""userCSharp:LogicalNe(string(s0:ID/text()) , &quot;''&quot;)"" />
    <xsl:variable name=""var:v3"" select=""userCSharp:LogicalNe(string(s1:AccountingCustomerParty/s1:Party/s0:EndpointID/text()) , &quot;57900023057331&quot;)"" />
    <xsl:variable name=""var:v4"" select=""string(s1:AccountingCustomerParty/s1:Party/s0:EndpointID/text())"" />
    <xsl:variable name=""var:v5"" select=""userCSharp:LogicalNe($var:v4 , &quot;5790002305733&quot;)"" />
    <xsl:variable name=""var:v6"" select=""userCSharp:LogicalNe($var:v4 , &quot;5790002502347&quot;)"" />
    <xsl:variable name=""var:v7"" select=""userCSharp:LogicalAnd(string($var:v3) , string($var:v5) , string($var:v6))"" />
    <xsl:variable name=""var:v9"" select=""userCSharp:StringSize(string(s1:PayeeParty/s0:EndpointID/text()))"" />
    <xsl:variable name=""var:v10"" select=""userCSharp:LogicalNe(string($var:v9) , &quot;0&quot;)"" />
    <xsl:variable name=""var:v16"" select=""userCSharp:LogicalEq(string($var:v9) , &quot;0&quot;)"" />
    <xsl:variable name=""var:v18"" select=""userCSharp:LogicalNe($var:v4 , &quot;57900023057331&quot;)"" />
    <xsl:variable name=""var:v19"" select=""userCSharp:LogicalAnd(string($var:v18) , string($var:v5) , string($var:v6))"" />
    <xsl:variable name=""var:v21"" select=""userCSharp:StringSize(string(s1:PayeeParty/s1:PartyIdentification/s0:ID/text()))"" />
    <xsl:variable name=""var:v22"" select=""userCSharp:LogicalNe(string($var:v21) , &quot;0&quot;)"" />
    <xsl:variable name=""var:v28"" select=""userCSharp:LogicalEq(string($var:v21) , &quot;0&quot;)"" />
    <xsl:variable name=""var:v30"" select=""userCSharp:LogicalEq($var:v4 , &quot;57900023057331&quot;)"" />
    <xsl:variable name=""var:v31"" select=""userCSharp:LogicalEq($var:v4 , &quot;5790002305733&quot;)"" />
    <xsl:variable name=""var:v32"" select=""userCSharp:LogicalEq($var:v4 , &quot;5790002502347&quot;)"" />
    <xsl:variable name=""var:v33"" select=""userCSharp:LogicalOr(string($var:v30) , string($var:v31) , string($var:v32))"" />
    <xsl:variable name=""var:v36"" select=""userCSharp:StringSize(string(s1:PayeeParty/s0:EndpointID/@schemeID))"" />
    <xsl:variable name=""var:v37"" select=""userCSharp:LogicalNe(string($var:v36) , &quot;0&quot;)"" />
    <xsl:variable name=""var:v39"" select=""userCSharp:LogicalEq(string($var:v36) , &quot;0&quot;)"" />
    <xsl:variable name=""var:v42"" select=""userCSharp:StringSize(string(s1:PayeeParty/s1:PartyIdentification/s0:ID/@schemeID))"" />
    <xsl:variable name=""var:v43"" select=""userCSharp:LogicalNe(string($var:v42) , &quot;0&quot;)"" />
    <xsl:variable name=""var:v45"" select=""userCSharp:LogicalEq(string($var:v42) , &quot;0&quot;)"" />
    <xsl:variable name=""var:v69"" select=""userCSharp:StringLowerCase(string(s1:AllowanceCharge/s0:ChargeIndicator/text()))"" />
    <xsl:variable name=""var:v77"" select=""string(s1:LegalMonetaryTotal/s0:LineExtensionAmount/text())"" />
    <ns0:Invoice>
      <ProfileID>
        <xsl:value-of select=""s0:ProfileID/text()"" />
      </ProfileID>
      <InvoiceID>
        <xsl:value-of select=""s0:ID/text()"" />
      </InvoiceID>
      <CopyIndicator>
        <xsl:value-of select=""s0:CopyIndicator/text()"" />
      </CopyIndicator>
      <TestIndicator>
        <xsl:text>0</xsl:text>
      </TestIndicator>
      <IssueDate>
        <xsl:value-of select=""s0:IssueDate/text()"" />
      </IssueDate>
      <xsl:if test=""string($var:v1)='true'"">
        <xsl:variable name=""var:v2"" select=""&quot;381&quot;"" />
        <InvoiceType>
          <xsl:value-of select=""$var:v2"" />
        </InvoiceType>
      </xsl:if>
      <Note>
        <xsl:value-of select=""s0:Note/text()"" />
      </Note>
      <Currency>
        <xsl:value-of select=""s0:DocumentCurrencyCode/text()"" />
      </Currency>
      <OrderReference>
        <OrderID>
          <xsl:value-of select=""s1:OrderReference/s0:ID/text()"" />
        </OrderID>
        <SalesOrderID>
          <xsl:value-of select=""s1:OrderReference/s0:SalesOrderID/text()"" />
        </SalesOrderID>
        <OrderDate>
          <xsl:value-of select=""s1:OrderReference/s0:IssueDate/text()"" />
        </OrderDate>
      </OrderReference>
      <DespatchDocumentReference>
        <ID>
          <xsl:value-of select=""s1:DiscrepancyResponse/s0:ReferenceID/text()"" />
        </ID>
      </DespatchDocumentReference>
      <AccountingSupplier>
        <AccSellerID>
          <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s0:EndpointID/text()"" />
        </AccSellerID>
        <PartyID>
          <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PartyIdentification/s0:ID/text()"" />
        </PartyID>
        <CompanyID>
          <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PartyLegalEntity/s0:CompanyID/text()"" />
        </CompanyID>
        <CompanyTaxID>
          <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PartyTaxScheme/s0:CompanyID/text()"" />
        </CompanyTaxID>
        <SchemeID>
          <Endpoint>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s0:EndpointID/@schemeID"" />
          </Endpoint>
          <xsl:if test=""s1:AccountingSupplierParty/s1:Party/s1:PartyIdentification/s0:ID/@schemeID"">
            <Party>
              <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PartyIdentification/s0:ID/@schemeID"" />
            </Party>
          </xsl:if>
          <Company>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PartyLegalEntity/s0:CompanyID/@schemeID"" />
          </Company>
          <CompanyTax>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PartyTaxScheme/s0:CompanyID/@schemeID"" />
          </CompanyTax>
        </SchemeID>
        <Address>
          <Name>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PartyName/s0:Name/text()"" />
          </Name>
          <AddressFormatCode>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PostalAddress/s0:AddressFormatCode/text()"" />
          </AddressFormatCode>
          <Street>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PostalAddress/s0:StreetName/text()"" />
          </Street>
          <AdditionalStreetName>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PostalAddress/s0:AdditionalStreetName/text()"" />
          </AdditionalStreetName>
          <City>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PostalAddress/s0:CityName/text()"" />
          </City>
          <PostalCode>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PostalAddress/s0:PostalZone/text()"" />
          </PostalCode>
          <Country>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PostalAddress/s1:Country/s0:IdentificationCode/text()"" />
          </Country>
        </Address>
        <Concact>
          <ID>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:SellerContact/s0:ID/text()"" />
          </ID>
          <Name>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:SellerContact/s0:Name/text()"" />
          </Name>
          <Telephone>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:SellerContact/s0:Telephone/text()"" />
          </Telephone>
          <Telefax>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:SellerContact/s0:Telefax/text()"" />
          </Telefax>
          <E-mail>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:SellerContact/s0:ElectronicMail/text()"" />
          </E-mail>
        </Concact>
      </AccountingSupplier>
      <AccountingCustomer>
        <SupplierAssignedAccountID>
          <xsl:value-of select=""s1:AccountingCustomerParty/s0:SupplierAssignedAccountID/text()"" />
        </SupplierAssignedAccountID>
        <AccBuyerID>
          <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s0:EndpointID/text()"" />
        </AccBuyerID>
        <PartyID>
          <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PartyIdentification/s0:ID/text()"" />
        </PartyID>
        <CompanyID>
          <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PartyLegalEntity/s0:CompanyID/text()"" />
        </CompanyID>
        <SchemeID>
          <Endpoint>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s0:EndpointID/@schemeID"" />
          </Endpoint>
          <xsl:if test=""s1:AccountingCustomerParty/s1:Party/s1:PartyIdentification/s0:ID/@schemeID"">
            <Party>
              <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PartyIdentification/s0:ID/@schemeID"" />
            </Party>
          </xsl:if>
          <Company>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PartyLegalEntity/s0:CompanyID/@schemeID"" />
          </Company>
        </SchemeID>
        <Address>
          <Name>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PartyName/s0:Name/text()"" />
          </Name>
          <AddressFormatCode>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PostalAddress/s0:AddressFormatCode/text()"" />
          </AddressFormatCode>
          <Street>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PostalAddress/s0:StreetName/text()"" />
          </Street>
          <AdditionalStreetName>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PostalAddress/s0:AdditionalStreetName/text()"" />
          </AdditionalStreetName>
          <City>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PostalAddress/s0:CityName/text()"" />
          </City>
          <PostalCode>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PostalAddress/s0:PostalZone/text()"" />
          </PostalCode>
          <Country>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PostalAddress/s1:Country/s0:IdentificationCode/text()"" />
          </Country>
        </Address>
        <Concact>
          <ID>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:Contact/s0:ID/text()"" />
          </ID>
          <Name>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:Contact/s0:Name/text()"" />
          </Name>
          <Telephone>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:Contact/s0:Telephone/text()"" />
          </Telephone>
          <Telefax>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:Contact/s0:Telefax/text()"" />
          </Telefax>
          <E-mail>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:Contact/s0:ElectronicMail/text()"" />
          </E-mail>
        </Concact>
      </AccountingCustomer>
      <BuyerCustomer>
        <xsl:if test=""string($var:v7)='true'"">
          <xsl:variable name=""var:v8"" select=""s1:AccountingCustomerParty/s1:Party/s0:EndpointID/text()"" />
          <BuyerID>
            <xsl:value-of select=""$var:v8"" />
          </BuyerID>
        </xsl:if>
        <xsl:if test=""string($var:v10)='true'"">
          <xsl:variable name=""var:v11"" select=""userCSharp:LogicalEq($var:v4 , &quot;57900023057331&quot;)"" />
          <xsl:variable name=""var:v12"" select=""userCSharp:LogicalEq($var:v4 , &quot;5790002305733&quot;)"" />
          <xsl:variable name=""var:v13"" select=""userCSharp:LogicalEq($var:v4 , &quot;5790002502347&quot;)"" />
          <xsl:variable name=""var:v14"" select=""userCSharp:LogicalOr(string($var:v11) , string($var:v12) , string($var:v13))"" />
          <xsl:if test=""string($var:v14)='true'"">
            <xsl:variable name=""var:v15"" select=""s1:PayeeParty/s0:EndpointID/text()"" />
            <BuyerID>
              <xsl:value-of select=""$var:v15"" />
            </BuyerID>
          </xsl:if>
        </xsl:if>
        <xsl:if test=""string($var:v16)='true'"">
          <xsl:variable name=""var:v17"" select=""s1:AccountingCustomerParty/s1:Party/s1:Contact/s0:ID/text()"" />
          <BuyerID>
            <xsl:value-of select=""$var:v17"" />
          </BuyerID>
        </xsl:if>
        <xsl:if test=""string($var:v19)='true'"">
          <xsl:variable name=""var:v20"" select=""s1:AccountingCustomerParty/s1:Party/s0:EndpointID/text()"" />
          <PartyID>
            <xsl:value-of select=""$var:v20"" />
          </PartyID>
        </xsl:if>
        <xsl:if test=""string($var:v22)='true'"">
          <xsl:variable name=""var:v23"" select=""userCSharp:LogicalEq($var:v4 , &quot;57900023057331&quot;)"" />
          <xsl:variable name=""var:v24"" select=""userCSharp:LogicalEq($var:v4 , &quot;5790002305733&quot;)"" />
          <xsl:variable name=""var:v25"" select=""userCSharp:LogicalEq($var:v4 , &quot;5790002502347&quot;)"" />
          <xsl:variable name=""var:v26"" select=""userCSharp:LogicalOr(string($var:v23) , string($var:v24) , string($var:v25))"" />
          <xsl:if test=""string($var:v26)='true'"">
            <xsl:variable name=""var:v27"" select=""s1:PayeeParty/s1:PartyIdentification/s0:ID/text()"" />
            <PartyID>
              <xsl:value-of select=""$var:v27"" />
            </PartyID>
          </xsl:if>
        </xsl:if>
        <xsl:if test=""string($var:v28)='true'"">
          <xsl:variable name=""var:v29"" select=""s1:AccountingCustomerParty/s1:Party/s1:Contact/s0:ID/text()"" />
          <PartyID>
            <xsl:value-of select=""$var:v29"" />
          </PartyID>
        </xsl:if>
        <xsl:if test=""string($var:v33)='true'"">
          <xsl:variable name=""var:v34"" select=""s1:PayeeParty/s1:PartyLegalEntity/s0:CompanyID/text()"" />
          <CompanyID>
            <xsl:value-of select=""$var:v34"" />
          </CompanyID>
        </xsl:if>
        <SchemeID>
          <xsl:if test=""string($var:v19)='true'"">
            <xsl:variable name=""var:v35"" select=""s1:AccountingCustomerParty/s1:Party/s0:EndpointID/@schemeID"" />
            <Endpoint>
              <xsl:value-of select=""$var:v35"" />
            </Endpoint>
          </xsl:if>
          <xsl:if test=""string($var:v37)='true'"">
            <xsl:if test=""string($var:v33)='true'"">
              <xsl:variable name=""var:v38"" select=""s1:PayeeParty/s0:EndpointID/@schemeID"" />
              <Endpoint>
                <xsl:value-of select=""$var:v38"" />
              </Endpoint>
            </xsl:if>
          </xsl:if>
          <xsl:if test=""string($var:v39)='true'"">
            <xsl:variable name=""var:v40"" select=""s1:AccountingCustomerParty/s1:Party/s1:Contact/s0:ID/@schemeID"" />
            <Endpoint>
              <xsl:value-of select=""$var:v40"" />
            </Endpoint>
          </xsl:if>
          <xsl:if test=""string($var:v19)='true'"">
            <xsl:variable name=""var:v41"" select=""s1:AccountingCustomerParty/s1:Party/s1:PartyIdentification/s0:ID/@schemeID"" />
            <Party>
              <xsl:value-of select=""$var:v41"" />
            </Party>
          </xsl:if>
          <xsl:if test=""string($var:v43)='true'"">
            <xsl:if test=""string($var:v33)='true'"">
              <xsl:variable name=""var:v44"" select=""s1:PayeeParty/s1:PartyIdentification/s0:ID/@schemeID"" />
              <Party>
                <xsl:value-of select=""$var:v44"" />
              </Party>
            </xsl:if>
          </xsl:if>
          <xsl:if test=""string($var:v45)='true'"">
            <xsl:variable name=""var:v46"" select=""s1:AccountingCustomerParty/s1:Party/s1:Contact/s0:ID/@schemeID"" />
            <Party>
              <xsl:value-of select=""$var:v46"" />
            </Party>
          </xsl:if>
          <xsl:if test=""string($var:v33)='true'"">
            <xsl:variable name=""var:v47"" select=""s1:PayeeParty/s1:PartyLegalEntity/s0:CompanyID/@schemeID"" />
            <Company>
              <xsl:value-of select=""$var:v47"" />
            </Company>
          </xsl:if>
          <Company>
            <xsl:value-of select=""s1:PayeeParty/s1:PartyName/text()"" />
          </Company>
        </SchemeID>
        <Address>
          <xsl:if test=""string($var:v19)='true'"">
            <xsl:variable name=""var:v48"" select=""s1:AccountingCustomerParty/s1:Party/s1:PartyName/s0:Name/text()"" />
            <Name>
              <xsl:value-of select=""$var:v48"" />
            </Name>
          </xsl:if>
          <xsl:if test=""string($var:v33)='true'"">
            <xsl:variable name=""var:v49"" select=""s1:PayeeParty/s1:PartyName/s0:Name/text()"" />
            <Name>
              <xsl:value-of select=""$var:v49"" />
            </Name>
          </xsl:if>
          <xsl:if test=""string($var:v33)='true'"">
            <xsl:variable name=""var:v50"" select=""s1:PayeeParty/s1:PostalAddress/s0:AddressFormatCode/text()"" />
            <AddressFormatCode>
              <xsl:value-of select=""$var:v50"" />
            </AddressFormatCode>
          </xsl:if>
          <xsl:if test=""string($var:v33)='true'"">
            <xsl:variable name=""var:v51"" select=""s1:PayeeParty/s1:PostalAddress/s0:StreetName/text()"" />
            <Street>
              <xsl:value-of select=""$var:v51"" />
            </Street>
          </xsl:if>
          <xsl:if test=""string($var:v33)='true'"">
            <xsl:variable name=""var:v52"" select=""s1:PayeeParty/s1:PostalAddress/s0:AdditionalStreetName/text()"" />
            <AdditionalStreetName>
              <xsl:value-of select=""$var:v52"" />
            </AdditionalStreetName>
          </xsl:if>
          <xsl:if test=""string($var:v33)='true'"">
            <xsl:variable name=""var:v53"" select=""s1:PayeeParty/s1:PostalAddress/s0:CityName/text()"" />
            <City>
              <xsl:value-of select=""$var:v53"" />
            </City>
          </xsl:if>
          <xsl:if test=""string($var:v33)='true'"">
            <xsl:variable name=""var:v54"" select=""s1:PayeeParty/s1:PostalAddress/s0:PostalZone/text()"" />
            <PostalCode>
              <xsl:value-of select=""$var:v54"" />
            </PostalCode>
          </xsl:if>
          <xsl:if test=""string($var:v33)='true'"">
            <xsl:variable name=""var:v55"" select=""s1:PayeeParty/s1:PostalAddress/s1:Country/s0:IdentificationCode/text()"" />
            <Country>
              <xsl:value-of select=""$var:v55"" />
            </Country>
          </xsl:if>
        </Address>
      </BuyerCustomer>
      <DeliveryLocation>
        <xsl:if test=""string($var:v19)='true'"">
          <xsl:variable name=""var:v56"" select=""s1:PayeeParty/s0:EndpointID/text()"" />
          <DeliveryID>
            <xsl:value-of select=""$var:v56"" />
          </DeliveryID>
        </xsl:if>
        <DeliveryID>
          <xsl:value-of select=""s1:PayeeParty/s0:EndpointID/text()"" />
        </DeliveryID>
        <xsl:if test=""string($var:v19)='true'"">
          <xsl:variable name=""var:v57"" select=""s1:PayeeParty/s1:PartyIdentification/s0:ID/text()"" />
          <PartyID>
            <xsl:value-of select=""$var:v57"" />
          </PartyID>
        </xsl:if>
        <PartyID>
          <xsl:value-of select=""s1:PayeeParty/s0:EndpointID/text()"" />
        </PartyID>
        <xsl:if test=""string($var:v19)='true'"">
          <xsl:variable name=""var:v58"" select=""s1:PayeeParty/s1:PartyLegalEntity/s0:CompanyID/text()"" />
          <CompanyID>
            <xsl:value-of select=""$var:v58"" />
          </CompanyID>
        </xsl:if>
        <SchemeID>
          <xsl:if test=""string($var:v19)='true'"">
            <xsl:variable name=""var:v59"" select=""s1:PayeeParty/s0:EndpointID/@schemeID"" />
            <Endpoint>
              <xsl:value-of select=""$var:v59"" />
            </Endpoint>
          </xsl:if>
          <xsl:if test=""string($var:v19)='true'"">
            <xsl:variable name=""var:v60"" select=""s1:PayeeParty/s1:PartyIdentification/s0:ID/@schemeID"" />
            <Party>
              <xsl:value-of select=""$var:v60"" />
            </Party>
          </xsl:if>
          <xsl:if test=""string($var:v19)='true'"">
            <xsl:variable name=""var:v61"" select=""s1:PayeeParty/s1:PartyLegalEntity/s0:CompanyID/@schemeID"" />
            <Company>
              <xsl:value-of select=""$var:v61"" />
            </Company>
          </xsl:if>
          <xsl:value-of select=""s1:PayeeParty/s0:EndpointID/@schemeID"" />
        </SchemeID>
        <Address>
          <xsl:if test=""string($var:v19)='true'"">
            <xsl:variable name=""var:v62"" select=""s1:PayeeParty/s1:PartyName/s0:Name/text()"" />
            <Name>
              <xsl:value-of select=""$var:v62"" />
            </Name>
          </xsl:if>
          <Name>
            <xsl:value-of select=""s1:PayeeParty/s1:PartyName/s0:Name/text()"" />
          </Name>
          <xsl:if test=""string($var:v19)='true'"">
            <xsl:variable name=""var:v63"" select=""s1:PayeeParty/s1:PostalAddress/s0:AddressFormatCode/text()"" />
            <AddressFormatCode>
              <xsl:value-of select=""$var:v63"" />
            </AddressFormatCode>
          </xsl:if>
          <AddressFormatCode>
            <xsl:value-of select=""s1:PayeeParty/s1:PostalAddress/s0:AddressFormatCode/text()"" />
          </AddressFormatCode>
          <xsl:if test=""string($var:v19)='true'"">
            <xsl:variable name=""var:v64"" select=""s1:PayeeParty/s1:PostalAddress/s0:StreetName/text()"" />
            <Street>
              <xsl:value-of select=""$var:v64"" />
            </Street>
          </xsl:if>
          <Street>
            <xsl:value-of select=""s1:PayeeParty/s1:PostalAddress/s0:StreetName/text()"" />
          </Street>
          <xsl:if test=""string($var:v19)='true'"">
            <xsl:variable name=""var:v65"" select=""s1:PayeeParty/s1:PostalAddress/s0:AdditionalStreetName/text()"" />
            <AdditionalStreetName>
              <xsl:value-of select=""$var:v65"" />
            </AdditionalStreetName>
          </xsl:if>
          <AdditionalStreetName>
            <xsl:value-of select=""s1:PayeeParty/s1:PostalAddress/s0:AdditionalStreetName/text()"" />
          </AdditionalStreetName>
          <xsl:if test=""string($var:v19)='true'"">
            <xsl:variable name=""var:v66"" select=""s1:PayeeParty/s1:PostalAddress/s0:CityName/text()"" />
            <City>
              <xsl:value-of select=""$var:v66"" />
            </City>
          </xsl:if>
          <City>
            <xsl:value-of select=""s1:PayeeParty/s1:PostalAddress/s0:CityName/text()"" />
          </City>
          <xsl:if test=""string($var:v19)='true'"">
            <xsl:variable name=""var:v67"" select=""s1:PayeeParty/s1:PostalAddress/s0:PostalZone/text()"" />
            <PostalCode>
              <xsl:value-of select=""$var:v67"" />
            </PostalCode>
          </xsl:if>
          <PostalCode>
            <xsl:value-of select=""s1:PayeeParty/s1:PostalAddress/s0:PostalZone/text()"" />
          </PostalCode>
          <xsl:if test=""string($var:v19)='true'"">
            <xsl:variable name=""var:v68"" select=""s1:PayeeParty/s1:PostalAddress/s1:Country/s0:IdentificationCode/text()"" />
            <Country>
              <xsl:value-of select=""$var:v68"" />
            </Country>
          </xsl:if>
          <Country>
            <xsl:value-of select=""s1:PayeeParty/s1:PostalAddress/s1:Country/s0:IdentificationCode/text()"" />
          </Country>
        </Address>
      </DeliveryLocation>
      <AllowanceCharge>
        <ChargeIndicator>
          <xsl:value-of select=""$var:v69"" />
        </ChargeIndicator>
        <AllowanceChargeReasonCode>
          <xsl:value-of select=""s1:AllowanceCharge/s0:AllowanceChargeReasonCode/text()"" />
        </AllowanceChargeReasonCode>
        <AllowanceChargeReason>
          <xsl:value-of select=""s1:AllowanceCharge/s0:AllowanceChargeReason/text()"" />
        </AllowanceChargeReason>
        <xsl:variable name=""var:v70"" select=""userCSharp:AddDecimals(string(s1:AllowanceCharge/s0:MultiplierFactorNumeric/text()) , &quot;4&quot;)"" />
        <MultiplierFactorNumeric>
          <xsl:value-of select=""$var:v70"" />
        </MultiplierFactorNumeric>
        <SequenceNumeric>
          <xsl:value-of select=""s1:AllowanceCharge/s0:SequenceNumeric/text()"" />
        </SequenceNumeric>
        <xsl:variable name=""var:v71"" select=""userCSharp:AddDecimals(string(s1:AllowanceCharge/s0:Amount/text()) , &quot;2&quot;)"" />
        <Amount>
          <xsl:value-of select=""$var:v71"" />
        </Amount>
        <xsl:variable name=""var:v72"" select=""userCSharp:AddDecimals(string(s1:AllowanceCharge/s0:BaseAmount/text()) , &quot;2&quot;)"" />
        <BaseAmount>
          <xsl:value-of select=""$var:v72"" />
        </BaseAmount>
        <Attributes>
          <Amount>
            <CurrencyID>
              <xsl:value-of select=""s1:AllowanceCharge/s0:Amount/@currencyID"" />
            </CurrencyID>
          </Amount>
          <BaseAmount>
            <CurrencyID>
              <xsl:value-of select=""s1:AllowanceCharge/s0:BaseAmount/@currencyID"" />
            </CurrencyID>
          </BaseAmount>
        </Attributes>
      </AllowanceCharge>
      <TaxTotal>
        <xsl:variable name=""var:v73"" select=""userCSharp:AddDecimals(string(s1:TaxTotal/s0:TaxAmount/text()) , &quot;2&quot;)"" />
        <TaxAmount>
          <xsl:value-of select=""$var:v73"" />
        </TaxAmount>
        <TaxSubtotal>
          <xsl:variable name=""var:v74"" select=""userCSharp:AddDecimals(string(s1:TaxTotal/s1:TaxSubtotal/s0:TaxAmount/text()) , &quot;2&quot;)"" />
          <TaxAmount>
            <xsl:value-of select=""$var:v74"" />
          </TaxAmount>
          <xsl:variable name=""var:v75"" select=""userCSharp:AddDecimals(string(s1:TaxTotal/s1:TaxSubtotal/s0:TaxableAmount/text()) , &quot;2&quot;)"" />
          <TaxableAmount>
            <xsl:value-of select=""$var:v75"" />
          </TaxableAmount>
          <TaxCategory>
            <ID>
              <xsl:value-of select=""s1:TaxTotal/s1:TaxSubtotal/s1:TaxCategory/s0:ID/text()"" />
            </ID>
            <Percent>
              <xsl:value-of select=""s1:TaxTotal/s1:TaxSubtotal/s1:TaxCategory/s0:Percent/text()"" />
            </Percent>
            <TaxScheme>
              <ID>
                <xsl:value-of select=""s1:TaxTotal/s1:TaxSubtotal/s1:TaxCategory/s1:TaxScheme/s0:ID/text()"" />
              </ID>
              <Navn>
                <xsl:value-of select=""s1:TaxTotal/s1:TaxSubtotal/s1:TaxCategory/s1:TaxScheme/s0:Name/text()"" />
              </Navn>
            </TaxScheme>
          </TaxCategory>
        </TaxSubtotal>
      </TaxTotal>
      <Total>
        <xsl:variable name=""var:v76"" select=""userCSharp:AddDecimals(string(s1:LegalMonetaryTotal/s0:LineExtensionAmount/text()) , &quot;2&quot;)"" />
        <LineTotalAmount>
          <xsl:value-of select=""$var:v76"" />
        </LineTotalAmount>
        <xsl:variable name=""var:v78"" select=""userCSharp:GetTaxExclusiveAmount(string(s1:LegalMonetaryTotal/s0:TaxExclusiveAmount/text()) , $var:v77 , string(s1:TaxTotal/s1:TaxSubtotal/s1:TaxCategory/s0:Percent/text()) , &quot;2&quot;)"" />
        <TaxExclAmount>
          <xsl:value-of select=""$var:v78"" />
        </TaxExclAmount>
        <xsl:variable name=""var:v79"" select=""userCSharp:AddDecimals(string(s1:LegalMonetaryTotal/s0:TaxInclusiveAmount/text()) , &quot;2&quot;)"" />
        <TaxInclAmount>
          <xsl:value-of select=""$var:v79"" />
        </TaxInclAmount>
        <xsl:variable name=""var:v80"" select=""userCSharp:AddDecimals(string(s1:LegalMonetaryTotal/s0:PayableAmount/text()) , &quot;2&quot;)"" />
        <PayableAmount>
          <xsl:value-of select=""$var:v80"" />
        </PayableAmount>
      </Total>
      <Lines>
        <xsl:for-each select=""s1:CreditNoteLine"">
          <xsl:variable name=""var:v87"" select=""userCSharp:StringLowerCase(string(s1:Price/s1:AllowanceCharge/s0:ChargeIndicator/text()))"" />
          <xsl:variable name=""var:v91"" select=""string(s1:TaxTotal/s0:TaxAmount/text())"" />
          <xsl:variable name=""var:v93"" select=""string(s1:TaxTotal/s1:TaxSubtotal/s0:TaxAmount/text())"" />
          <xsl:variable name=""var:v95"" select=""string(s1:TaxTotal/s1:TaxSubtotal/s0:TaxableAmount/text())"" />
          <xsl:variable name=""var:v97"" select=""string(s1:TaxTotal/s1:TaxSubtotal/s1:TaxCategory/s0:Percent/text())"" />
          <Line>
            <LineNo>
              <xsl:value-of select=""s0:ID/text()"" />
            </LineNo>
            <Note>
              <xsl:value-of select=""s0:Note/text()"" />
            </Note>
            <NoteLoop>
              <Node>
                <Note>
                  <xsl:value-of select=""s0:Note/text()"" />
                </Note>
              </Node>
            </NoteLoop>
            <xsl:variable name=""var:v81"" select=""userCSharp:AddDecimals(string(s0:CreditedQuantity/text()) , &quot;3&quot;)"" />
            <Quantity>
              <xsl:value-of select=""$var:v81"" />
            </Quantity>
            <UnitCode>
              <xsl:value-of select=""s0:CreditedQuantity/@unitCode"" />
            </UnitCode>
            <xsl:variable name=""var:v82"" select=""userCSharp:AddDecimals(string(s0:LineExtensionAmount/text()) , &quot;2&quot;)"" />
            <LineAmountTotal>
              <xsl:value-of select=""$var:v82"" />
            </LineAmountTotal>
            <Item>
              <SellerItemID>
                <xsl:value-of select=""s1:Item/s1:SellersItemIdentification/s0:ID/text()"" />
              </SellerItemID>
              <StandardItemID>
                <xsl:value-of select=""s1:Item/s1:StandardItemIdentification/s0:ID/text()"" />
              </StandardItemID>
              <AdditionalItemID>
                <xsl:value-of select=""s1:Item/s1:AdditionalItemIdentification/s0:ID/text()"" />
              </AdditionalItemID>
              <Name>
                <xsl:value-of select=""s1:Item/s0:Name/text()"" />
              </Name>
              <Description>
                <xsl:value-of select=""s1:Item/s0:Description/text()"" />
              </Description>
            </Item>
            <PriceNet>
              <xsl:variable name=""var:v83"" select=""userCSharp:AddDecimals(string(s1:Price/s0:PriceAmount/text()) , &quot;2&quot; , &quot;2&quot;)"" />
              <Price>
                <xsl:value-of select=""$var:v83"" />
              </Price>
              <xsl:variable name=""var:v84"" select=""userCSharp:AddDecimals(string(s1:Price/s0:BaseQuantity/text()) , &quot;3&quot;)"" />
              <Quantity>
                <xsl:value-of select=""$var:v84"" />
              </Quantity>
              <UnitCode>
                <xsl:value-of select=""s1:Price/s0:BaseQuantity/@unitCode"" />
              </UnitCode>
            </PriceNet>
            <PriceGross>
              <xsl:variable name=""var:v85"" select=""userCSharp:AddDecimals(string(s1:PricingReference/s1:AlternativeConditionPrice/s0:PriceAmount/text()) , &quot;2&quot; , &quot;2&quot;)"" />
              <Price>
                <xsl:value-of select=""$var:v85"" />
              </Price>
              <xsl:variable name=""var:v86"" select=""userCSharp:AddDecimals(string(s1:PricingReference/s1:AlternativeConditionPrice/s0:BaseQuantity/text()) , &quot;3&quot;)"" />
              <Quantity>
                <xsl:value-of select=""$var:v86"" />
              </Quantity>
              <UnitCode>
                <xsl:value-of select=""s1:PricingReference/s1:AlternativeConditionPrice/s0:BaseQuantity/@unitCode"" />
              </UnitCode>
              <PristypeCode>
                <xsl:value-of select=""s1:PricingReference/s1:AlternativeConditionPrice/s0:PriceTypeCode/text()"" />
              </PristypeCode>
              <OrderableUnitFactorRate>
                <xsl:value-of select=""s1:PricingReference/s1:AlternativeConditionPrice/s0:OrderableUnitFactorRate/text()"" />
              </OrderableUnitFactorRate>
            </PriceGross>
            <AllowanceCharge>
              <ChargeIndicator>
                <xsl:value-of select=""$var:v87"" />
              </ChargeIndicator>
              <AllowanceChargeReasonCode>
                <xsl:value-of select=""s1:Price/s1:AllowanceCharge/s0:AllowanceChargeReasonCode/text()"" />
              </AllowanceChargeReasonCode>
              <AllowanceChargeReason>
                <xsl:value-of select=""s1:Price/s1:AllowanceCharge/s0:AllowanceChargeReason/text()"" />
              </AllowanceChargeReason>
              <xsl:variable name=""var:v88"" select=""userCSharp:AddDecimals(string(s1:Price/s1:AllowanceCharge/s0:MultiplierFactorNumeric/text()) , &quot;4&quot;)"" />
              <MultiplierFactorNumeric>
                <xsl:value-of select=""$var:v88"" />
              </MultiplierFactorNumeric>
              <SequenceNumeric>
                <xsl:value-of select=""s1:Price/s1:AllowanceCharge/s0:SequenceNumeric/text()"" />
              </SequenceNumeric>
              <xsl:variable name=""var:v89"" select=""userCSharp:AddDecimals(string(s1:Price/s1:AllowanceCharge/s0:Amount/text()) , &quot;2&quot;)"" />
              <Amount>
                <xsl:value-of select=""$var:v89"" />
              </Amount>
              <xsl:variable name=""var:v90"" select=""userCSharp:AddDecimals(string(s1:Price/s1:AllowanceCharge/s0:BaseAmount/text()) , &quot;2&quot;)"" />
              <BaseAmount>
                <xsl:value-of select=""$var:v90"" />
              </BaseAmount>
              <Attributes>
                <Amount>
                  <xsl:attribute name=""CurrencyID"">
                    <xsl:value-of select=""s1:Price/s1:AllowanceCharge/s0:Amount/@currencyID"" />
                  </xsl:attribute>
                </Amount>
                <BaseAmount>
                  <xsl:attribute name=""CurrencyID"">
                    <xsl:value-of select=""s1:Price/s1:AllowanceCharge/s0:BaseAmount/@currencyID"" />
                  </xsl:attribute>
                </BaseAmount>
              </Attributes>
            </AllowanceCharge>
            <TaxTotal>
              <xsl:variable name=""var:v92"" select=""userCSharp:AddDecimals($var:v91 , &quot;2&quot;)"" />
              <TaxAmount>
                <xsl:value-of select=""$var:v92"" />
              </TaxAmount>
              <TaxSubtotal>
                <xsl:variable name=""var:v94"" select=""userCSharp:AddDecimals($var:v93 , &quot;2&quot;)"" />
                <TaxAmount>
                  <xsl:value-of select=""$var:v94"" />
                </TaxAmount>
                <xsl:variable name=""var:v96"" select=""userCSharp:AddDecimals($var:v95 , &quot;2&quot;)"" />
                <TaxableAmount>
                  <xsl:value-of select=""$var:v96"" />
                </TaxableAmount>
                <TaxCategory>
                  <ID>
                    <xsl:value-of select=""s1:TaxTotal/s1:TaxSubtotal/s1:TaxCategory/s0:ID/text()"" />
                  </ID>
                  <xsl:variable name=""var:v98"" select=""userCSharp:AddDecimals($var:v97 , &quot;4&quot;)"" />
                  <Percent>
                    <xsl:value-of select=""$var:v98"" />
                  </Percent>
                  <TaxScheme>
                    <ID>
                      <xsl:value-of select=""s1:TaxTotal/s1:TaxSubtotal/s1:TaxCategory/s1:TaxScheme/s0:ID/text()"" />
                    </ID>
                    <Navn>
                      <xsl:value-of select=""s1:TaxTotal/s1:TaxSubtotal/s1:TaxCategory/s1:TaxScheme/s0:Name/text()"" />
                    </Navn>
                  </TaxScheme>
                </TaxCategory>
              </TaxSubtotal>
            </TaxTotal>
          </Line>
        </xsl:for-each>
      </Lines>
    </ns0:Invoice>
  </xsl:template>
  <msxsl:script language=""C#"" implements-prefix=""userCSharp""><![CDATA[
public bool LogicalNe(string val1, string val2)
{
	bool ret = false;
	double d1 = 0;
	double d2 = 0;
	if (IsNumeric(val1, ref d1) && IsNumeric(val2, ref d2))
	{
		ret = d1 != d2;
	}
	else
	{
		ret = String.Compare(val1, val2, StringComparison.Ordinal) != 0;
	}
	return ret;
}


public bool LogicalAnd(string param0, string param1, string param2)
{
	return ValToBool(param0) && ValToBool(param1) && ValToBool(param2);
	return false;
}


public string StringLowerCase(string str)
{
	if (str == null)
	{
		return """";
	}
	return str.ToLower(System.Globalization.CultureInfo.InvariantCulture);
}



public string AddDecimals(string OriginalAmount, string numberOfDecimals)
        {
            if (string.IsNullOrWhiteSpace(OriginalAmount))
                return OriginalAmount;
            decimal d = System.Convert.ToDecimal(OriginalAmount, System.Globalization.CultureInfo.InvariantCulture);

// d = d / 100;

string format = ""0."" + new string('0', System.Convert.ToInt32(numberOfDecimals));
            string NewAmount = d.ToString(format, System.Globalization.CultureInfo.InvariantCulture);
            return NewAmount;
		}	

public int StringSize(string str)
{
	if (str == null)
	{
		return 0;
	}
	return str.Length;
}


public bool LogicalEq(string val1, string val2)
{
	bool ret = false;
	double d1 = 0;
	double d2 = 0;
	if (IsNumeric(val1, ref d1) && IsNumeric(val2, ref d2))
	{
		ret = d1 == d2;
	}
	else
	{
		ret = String.Compare(val1, val2, StringComparison.Ordinal) == 0;
	}
	return ret;
}


public bool LogicalOr(string param0, string param1, string param2)
{
	return ValToBool(param0) || ValToBool(param1) || ValToBool(param2);
	return false;
}


public string GetTaxExclusiveAmount(string TaxExclAmount, string LineExtAmount, string VAT, string numberOfDecimals)
{
 string TaxExclusiveAmount = TaxExclAmount;

if ((string.IsNullOrWhiteSpace(TaxExclAmount)) && (!(string.IsNullOrWhiteSpace(LineExtAmount))) && (!(string.IsNullOrWhiteSpace(VAT))))
{    
 decimal TXA = (System.Convert.ToDecimal(LineExtAmount, System.Globalization.CultureInfo.InvariantCulture)*System.Convert.ToDecimal(VAT, System.Globalization.CultureInfo.InvariantCulture))/100;
 string format = ""0."" + new string('0', System.Convert.ToInt32(numberOfDecimals));
 TaxExclusiveAmount = TXA.ToString(format, System.Globalization.CultureInfo.InvariantCulture);
}

 return TaxExclusiveAmount;
}

public string AddDecimals(string OriginalAmount, string numberOfDecimals, string numberOfDashes)
        {
            if (string.IsNullOrWhiteSpace(OriginalAmount))
                return OriginalAmount;
            decimal d = System.Convert.ToDecimal(OriginalAmount, System.Globalization.CultureInfo.InvariantCulture);
string format = ""0."" + new string('0', System.Convert.ToInt32(numberOfDecimals)) + new string('#', System.Convert.ToInt32(numberOfDashes));
            string NewAmount = d.ToString(format, System.Globalization.CultureInfo.InvariantCulture);
            return NewAmount;
		}	


public bool IsNumeric(string val)
{
	if (val == null)
	{
		return false;
	}
	double d = 0;
	return Double.TryParse(val, System.Globalization.NumberStyles.AllowThousands | System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out d);
}

public bool IsNumeric(string val, ref double d)
{
	if (val == null)
	{
		return false;
	}
	return Double.TryParse(val, System.Globalization.NumberStyles.AllowThousands | System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out d);
}

public bool ValToBool(string val)
{
	if (val != null)
	{
		if (string.Compare(val, bool.TrueString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return true;
		}
		if (string.Compare(val, bool.FalseString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return false;
		}
		val = val.Trim();
		if (string.Compare(val, bool.TrueString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return true;
		}
		if (string.Compare(val, bool.FalseString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return false;
		}
		double d = 0;
		if (IsNumeric(val, ref d))
		{
			return (d > 0);
		}
	}
	return false;
}


]]></msxsl:script>
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"BYGE.Integration.UBLCreditnote.UBLCreditnote";
        
        private const global::BYGE.Integration.UBLCreditnote.UBLCreditnote _srcSchemaTypeReference0 = null;
        
        private const string _strTrgSchemasList0 = @"BYGE.Integration.Core.Invoice.Invoice";
        
        private const global::BYGE.Integration.Core.Invoice.Invoice _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [1];
                _SrcSchemas[0] = @"BYGE.Integration.UBLCreditnote.UBLCreditnote";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"BYGE.Integration.Core.Invoice.Invoice";
                return _TrgSchemas;
            }
        }
    }
}
