namespace BYGE.Integration.UBLCreditnote {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.UBLCreditnote.UBLCreditnote", typeof(global::BYGE.Integration.UBLCreditnote.UBLCreditnote))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.Core.Invoice.Invoice", typeof(global::BYGE.Integration.Core.Invoice.Invoice))]
    public sealed class UBLCreditnote_to_CoreInvoice_Copy : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var s2 s1 s0 userCSharp"" version=""1.0"" xmlns:s2=""urn:oasis:names:specification:ubl:schema:xsd:CreditNote-2"" xmlns:ns0=""http://byg-e.dk/schemas/v10"" xmlns:s1=""urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"" xmlns:s0=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" xmlns:userCSharp=""http://schemas.microsoft.com/BizTalk/2003/userCSharp"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/s2:CreditNote"" />
  </xsl:template>
  <xsl:template match=""/s2:CreditNote"">
    <xsl:variable name=""var:v1"" select=""&quot;381&quot;"" />
    <ns0:Invoice>
      <ProfileID>
        <xsl:value-of select=""s0:ProfileID/text()"" />
      </ProfileID>
      <InvoiceID>
        <xsl:value-of select=""s0:ID/text()"" />
      </InvoiceID>
      <CopyIndicator>
        <xsl:value-of select=""s0:CopyIndicator/text()"" />
      </CopyIndicator>
      <TestIndicator>
        <xsl:text>0</xsl:text>
      </TestIndicator>
      <IssueDate>
        <xsl:value-of select=""s0:IssueDate/text()"" />
      </IssueDate>
      <InvoiceType>
        <xsl:value-of select=""$var:v1"" />
      </InvoiceType>
      <Note>
        <xsl:value-of select=""s0:Note/text()"" />
      </Note>
      <Currency>
        <xsl:value-of select=""s0:DocumentCurrencyCode/text()"" />
      </Currency>
      <OrderReference>
        <OrderID>
          <xsl:value-of select=""s1:OrderReference/s0:ID/text()"" />
        </OrderID>
        <SalesOrderID>
          <xsl:value-of select=""s1:OrderReference/s0:SalesOrderID/text()"" />
        </SalesOrderID>
        <OrderDate>
          <xsl:value-of select=""s1:OrderReference/s0:IssueDate/text()"" />
        </OrderDate>
      </OrderReference>
      <DespatchDocumentReference>
        <ID>
          <xsl:value-of select=""s1:DiscrepancyResponse/s0:ReferenceID/text()"" />
        </ID>
      </DespatchDocumentReference>
      <AccountingSupplier>
        <AccSellerID>
          <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s0:EndpointID/text()"" />
        </AccSellerID>
        <PartyID>
          <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PartyIdentification/s0:ID/text()"" />
        </PartyID>
        <CompanyID>
          <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PartyLegalEntity/s0:CompanyID/text()"" />
        </CompanyID>
        <CompanyTaxID>
          <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PartyTaxScheme/s0:CompanyID/text()"" />
        </CompanyTaxID>
        <SchemeID>
          <Endpoint>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s0:EndpointID/@schemeID"" />
          </Endpoint>
          <xsl:if test=""s1:AccountingSupplierParty/s1:Party/s1:PartyIdentification/s0:ID/@schemeID"">
            <Party>
              <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PartyIdentification/s0:ID/@schemeID"" />
            </Party>
          </xsl:if>
          <Company>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PartyLegalEntity/s0:CompanyID/@schemeID"" />
          </Company>
          <CompanyTax>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PartyTaxScheme/s0:CompanyID/@schemeID"" />
          </CompanyTax>
        </SchemeID>
        <Address>
          <Name>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PartyName/s0:Name/text()"" />
          </Name>
          <AddressFormatCode>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PostalAddress/s0:AddressFormatCode/text()"" />
          </AddressFormatCode>
          <Street>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PostalAddress/s0:StreetName/text()"" />
          </Street>
          <AdditionalStreetName>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PostalAddress/s0:AdditionalStreetName/text()"" />
          </AdditionalStreetName>
          <City>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PostalAddress/s0:CityName/text()"" />
          </City>
          <PostalCode>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PostalAddress/s0:PostalZone/text()"" />
          </PostalCode>
          <Country>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PostalAddress/s1:Country/s0:IdentificationCode/text()"" />
          </Country>
        </Address>
        <Concact>
          <ID>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:Contact/s0:ID/text()"" />
          </ID>
          <Name>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:Contact/s0:Name/text()"" />
          </Name>
          <Telephone>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:Contact/s0:Telephone/text()"" />
          </Telephone>
          <Telefax>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:Contact/s0:Telefax/text()"" />
          </Telefax>
          <E-mail>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:Contact/s0:ElectronicMail/text()"" />
          </E-mail>
        </Concact>
      </AccountingSupplier>
      <AccountingCustomer>
        <SupplierAssignedAccountID>
          <xsl:value-of select=""s1:AccountingCustomerParty/s0:SupplierAssignedAccountID/text()"" />
        </SupplierAssignedAccountID>
        <AccBuyerID>
          <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s0:EndpointID/text()"" />
        </AccBuyerID>
        <PartyID>
          <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PartyIdentification/s0:ID/text()"" />
        </PartyID>
        <CompanyID>
          <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PartyLegalEntity/s0:CompanyID/text()"" />
        </CompanyID>
        <SchemeID>
          <Endpoint>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s0:EndpointID/@schemeID"" />
          </Endpoint>
          <xsl:if test=""s1:AccountingCustomerParty/s1:Party/s1:PartyIdentification/s0:ID/@schemeID"">
            <Party>
              <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PartyIdentification/s0:ID/@schemeID"" />
            </Party>
          </xsl:if>
          <Company>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PartyLegalEntity/s0:CompanyID/@schemeID"" />
          </Company>
        </SchemeID>
        <Address>
          <Name>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PartyName/s0:Name/text()"" />
          </Name>
          <AddressFormatCode>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PostalAddress/s0:AddressFormatCode/text()"" />
          </AddressFormatCode>
          <Street>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PostalAddress/s0:StreetName/text()"" />
          </Street>
          <AdditionalStreetName>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PostalAddress/s0:AdditionalStreetName/text()"" />
          </AdditionalStreetName>
          <City>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PostalAddress/s0:CityName/text()"" />
          </City>
          <PostalCode>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PostalAddress/s0:PostalZone/text()"" />
          </PostalCode>
          <Country>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PostalAddress/s1:Country/s0:IdentificationCode/text()"" />
          </Country>
        </Address>
        <Concact>
          <ID>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:Contact/s0:ID/text()"" />
          </ID>
          <Name>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:Contact/s0:Name/text()"" />
          </Name>
          <Telephone>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:Contact/s0:Telephone/text()"" />
          </Telephone>
          <Telefax>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:Contact/s0:Telefax/text()"" />
          </Telefax>
          <E-mail>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:Contact/s0:ElectronicMail/text()"" />
          </E-mail>
        </Concact>
      </AccountingCustomer>
      <BuyerCustomer>
        <BuyerID>
          <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s0:EndpointID/text()"" />
        </BuyerID>
        <PartyID>
          <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PartyIdentification/s0:ID/text()"" />
        </PartyID>
        <SchemeID>
          <Endpoint>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s0:EndpointID/@schemeID"" />
          </Endpoint>
          <xsl:if test=""s1:AccountingCustomerParty/s1:Party/s1:PartyIdentification/s0:ID/@schemeID"">
            <Party>
              <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PartyIdentification/s0:ID/@schemeID"" />
            </Party>
          </xsl:if>
        </SchemeID>
        <Address>
          <Name>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PartyName/s0:Name/text()"" />
          </Name>
        </Address>
      </BuyerCustomer>
      <DeliveryLocation>
        <DeliveryID>
          <xsl:value-of select=""s1:PayeeParty/s0:EndpointID/text()"" />
        </DeliveryID>
        <PartyID>
          <xsl:value-of select=""s1:PayeeParty/s1:PartyIdentification/s0:ID/text()"" />
        </PartyID>
        <CompanyID>
          <xsl:value-of select=""s1:PayeeParty/s1:PartyLegalEntity/s0:CompanyID/text()"" />
        </CompanyID>
        <SchemeID>
          <Endpoint>
            <xsl:value-of select=""s1:PayeeParty/s0:EndpointID/@schemeID"" />
          </Endpoint>
          <xsl:if test=""s1:PayeeParty/s1:PartyIdentification/s0:ID/@schemeID"">
            <Party>
              <xsl:value-of select=""s1:PayeeParty/s1:PartyIdentification/s0:ID/@schemeID"" />
            </Party>
          </xsl:if>
          <Company>
            <xsl:value-of select=""s1:PayeeParty/s1:PartyLegalEntity/s0:CompanyID/@schemeID"" />
          </Company>
        </SchemeID>
        <Address>
          <Name>
            <xsl:value-of select=""s1:PayeeParty/s1:PartyName/s0:Name/text()"" />
          </Name>
          <AddressFormatCode>
            <xsl:value-of select=""s1:PayeeParty/s1:PostalAddress/s0:AddressFormatCode/text()"" />
          </AddressFormatCode>
          <Street>
            <xsl:value-of select=""s1:PayeeParty/s1:PostalAddress/s0:StreetName/text()"" />
          </Street>
          <AdditionalStreetName>
            <xsl:value-of select=""s1:PayeeParty/s1:PostalAddress/s0:AdditionalStreetName/text()"" />
          </AdditionalStreetName>
          <City>
            <xsl:value-of select=""s1:PayeeParty/s1:PostalAddress/s0:CityName/text()"" />
          </City>
          <PostalCode>
            <xsl:value-of select=""s1:PayeeParty/s1:PostalAddress/s0:PostalZone/text()"" />
          </PostalCode>
          <Country>
            <xsl:value-of select=""s1:PayeeParty/s1:PostalAddress/s1:Country/s0:IdentificationCode/text()"" />
          </Country>
        </Address>
      </DeliveryLocation>
      <TaxTotal>
        <xsl:variable name=""var:v2"" select=""userCSharp:AddDecimals(string(s1:TaxTotal/s0:TaxAmount/text()) , &quot;2&quot;)"" />
        <TaxAmount>
          <xsl:value-of select=""$var:v2"" />
        </TaxAmount>
        <TaxSubtotal>
          <xsl:variable name=""var:v3"" select=""userCSharp:AddDecimals(string(s1:TaxTotal/s1:TaxSubtotal/s0:TaxAmount/text()) , &quot;2&quot;)"" />
          <TaxAmount>
            <xsl:value-of select=""$var:v3"" />
          </TaxAmount>
          <xsl:variable name=""var:v4"" select=""userCSharp:AddDecimals(string(s1:TaxTotal/s1:TaxSubtotal/s0:TaxableAmount/text()) , &quot;2&quot;)"" />
          <TaxableAmount>
            <xsl:value-of select=""$var:v4"" />
          </TaxableAmount>
          <TaxCategory>
            <ID>
              <xsl:value-of select=""s1:TaxTotal/s1:TaxSubtotal/s1:TaxCategory/s0:ID/text()"" />
            </ID>
            <Percent>
              <xsl:value-of select=""s1:TaxTotal/s1:TaxSubtotal/s1:TaxCategory/s0:Percent/text()"" />
            </Percent>
            <TaxScheme>
              <ID>
                <xsl:value-of select=""s1:TaxTotal/s1:TaxSubtotal/s1:TaxCategory/s1:TaxScheme/s0:ID/text()"" />
              </ID>
              <Navn>
                <xsl:value-of select=""s1:TaxTotal/s1:TaxSubtotal/s1:TaxCategory/s1:TaxScheme/s0:Name/text()"" />
              </Navn>
            </TaxScheme>
          </TaxCategory>
        </TaxSubtotal>
      </TaxTotal>
      <Total>
        <xsl:variable name=""var:v5"" select=""userCSharp:AddDecimals(string(s1:LegalMonetaryTotal/s0:LineExtensionAmount/text()) , &quot;2&quot;)"" />
        <LineTotalAmount>
          <xsl:value-of select=""$var:v5"" />
        </LineTotalAmount>
        <xsl:variable name=""var:v6"" select=""userCSharp:AddDecimals(string(s1:LegalMonetaryTotal/s0:TaxExclusiveAmount/text()) , &quot;2&quot;)"" />
        <TaxExclAmount>
          <xsl:value-of select=""$var:v6"" />
        </TaxExclAmount>
        <xsl:variable name=""var:v7"" select=""userCSharp:AddDecimals(string(s1:LegalMonetaryTotal/s0:TaxInclusiveAmount/text()) , &quot;2&quot;)"" />
        <TaxInclAmount>
          <xsl:value-of select=""$var:v7"" />
        </TaxInclAmount>
        <xsl:variable name=""var:v8"" select=""userCSharp:AddDecimals(string(s1:LegalMonetaryTotal/s0:PayableAmount/text()) , &quot;2&quot;)"" />
        <PayableAmount>
          <xsl:value-of select=""$var:v8"" />
        </PayableAmount>
      </Total>
      <Lines>
        <xsl:for-each select=""s1:CreditNoteLine"">
          <xsl:variable name=""var:v15"" select=""string(s1:TaxTotal/s0:TaxAmount/text())"" />
          <xsl:variable name=""var:v17"" select=""string(s1:TaxTotal/s1:TaxSubtotal/s0:TaxAmount/text())"" />
          <xsl:variable name=""var:v19"" select=""string(s1:TaxTotal/s1:TaxSubtotal/s0:TaxableAmount/text())"" />
          <Line>
            <LineNo>
              <xsl:value-of select=""s0:ID/text()"" />
            </LineNo>
            <xsl:variable name=""var:v9"" select=""userCSharp:AddDecimals(string(s0:CreditedQuantity/text()) , &quot;3&quot;)"" />
            <Quantity>
              <xsl:value-of select=""$var:v9"" />
            </Quantity>
            <UnitCode>
              <xsl:value-of select=""s0:CreditedQuantity/@unitCode"" />
            </UnitCode>
            <xsl:variable name=""var:v10"" select=""userCSharp:AddDecimals(string(s0:LineExtensionAmount/text()) , &quot;2&quot;)"" />
            <LineAmountTotal>
              <xsl:value-of select=""$var:v10"" />
            </LineAmountTotal>
            <Item>
              <SellerItemID>
                <xsl:value-of select=""s1:Item/s1:SellersItemIdentification/s0:ID/text()"" />
              </SellerItemID>
              <StandardItemID>
                <xsl:value-of select=""s1:Item/s1:StandardItemIdentification/s0:ID/text()"" />
              </StandardItemID>
              <AdditionalItemID>
                <xsl:value-of select=""s1:Item/s1:AdditionalItemIdentification/s0:ID/text()"" />
              </AdditionalItemID>
              <Name>
                <xsl:value-of select=""s1:Item/s0:Name/text()"" />
              </Name>
              <Description>
                <xsl:value-of select=""s1:Item/s0:Description/text()"" />
              </Description>
            </Item>
            <PriceNet>
              <xsl:variable name=""var:v11"" select=""userCSharp:AddDecimals(string(s1:Price/s0:PriceAmount/text()) , &quot;2&quot;)"" />
              <Price>
                <xsl:value-of select=""$var:v11"" />
              </Price>
              <xsl:variable name=""var:v12"" select=""userCSharp:AddDecimals(string(s1:Price/s0:BaseQuantity/text()) , &quot;3&quot;)"" />
              <Quantity>
                <xsl:value-of select=""$var:v12"" />
              </Quantity>
              <UnitCode>
                <xsl:value-of select=""s1:Price/s0:BaseQuantity/@unitCode"" />
              </UnitCode>
            </PriceNet>
            <PriceGross>
              <xsl:variable name=""var:v13"" select=""userCSharp:AddDecimals(string(s1:PricingReference/s1:AlternativeConditionPrice/s0:PriceAmount/text()) , &quot;2&quot;)"" />
              <Price>
                <xsl:value-of select=""$var:v13"" />
              </Price>
              <xsl:variable name=""var:v14"" select=""userCSharp:AddDecimals(string(s1:PricingReference/s1:AlternativeConditionPrice/s0:BaseQuantity/text()) , &quot;3&quot;)"" />
              <Quantity>
                <xsl:value-of select=""$var:v14"" />
              </Quantity>
              <UnitCode>
                <xsl:value-of select=""s1:PricingReference/s1:AlternativeConditionPrice/s0:BaseQuantity/@unitCode"" />
              </UnitCode>
              <PristypeCode>
                <xsl:value-of select=""s1:PricingReference/s1:AlternativeConditionPrice/s0:PriceTypeCode/text()"" />
              </PristypeCode>
            </PriceGross>
            <TaxTotal>
              <xsl:variable name=""var:v16"" select=""userCSharp:AddDecimals($var:v15 , &quot;2&quot;)"" />
              <TaxAmount>
                <xsl:value-of select=""$var:v16"" />
              </TaxAmount>
              <TaxSubtotal>
                <xsl:variable name=""var:v18"" select=""userCSharp:AddDecimals($var:v17 , &quot;2&quot;)"" />
                <TaxAmount>
                  <xsl:value-of select=""$var:v18"" />
                </TaxAmount>
                <xsl:variable name=""var:v20"" select=""userCSharp:AddDecimals($var:v19 , &quot;2&quot;)"" />
                <TaxableAmount>
                  <xsl:value-of select=""$var:v20"" />
                </TaxableAmount>
                <TaxCategory>
                  <ID>
                    <xsl:value-of select=""s1:TaxTotal/s1:TaxSubtotal/s1:TaxCategory/s0:ID/text()"" />
                  </ID>
                  <Percent>
                    <xsl:value-of select=""s1:TaxTotal/s1:TaxSubtotal/s1:TaxCategory/s0:Percent/text()"" />
                  </Percent>
                  <TaxScheme>
                    <ID>
                      <xsl:value-of select=""s1:TaxTotal/s1:TaxSubtotal/s1:TaxCategory/s1:TaxScheme/s0:ID/text()"" />
                    </ID>
                    <Navn>
                      <xsl:value-of select=""s1:TaxTotal/s1:TaxSubtotal/s1:TaxCategory/s1:TaxScheme/s0:Name/text()"" />
                    </Navn>
                  </TaxScheme>
                </TaxCategory>
              </TaxSubtotal>
            </TaxTotal>
          </Line>
        </xsl:for-each>
      </Lines>
    </ns0:Invoice>
  </xsl:template>
  <msxsl:script language=""C#"" implements-prefix=""userCSharp""><![CDATA[

public string AddDecimals(string OriginalAmount, string numberOfDecimals)
        {
            if (string.IsNullOrWhiteSpace(OriginalAmount))
                return OriginalAmount;
            decimal d = System.Convert.ToDecimal(OriginalAmount, System.Globalization.CultureInfo.InvariantCulture);
string format = ""0."" + new string('0', System.Convert.ToInt32(numberOfDecimals));
            string NewAmount = d.ToString(format, System.Globalization.CultureInfo.InvariantCulture);
            return NewAmount;
		}	


]]></msxsl:script>
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"BYGE.Integration.UBLCreditnote.UBLCreditnote";
        
        private const global::BYGE.Integration.UBLCreditnote.UBLCreditnote _srcSchemaTypeReference0 = null;
        
        private const string _strTrgSchemasList0 = @"BYGE.Integration.Core.Invoice.Invoice";
        
        private const global::BYGE.Integration.Core.Invoice.Invoice _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [1];
                _SrcSchemas[0] = @"BYGE.Integration.UBLCreditnote.UBLCreditnote";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"BYGE.Integration.Core.Invoice.Invoice";
                return _TrgSchemas;
            }
        }
    }
}
