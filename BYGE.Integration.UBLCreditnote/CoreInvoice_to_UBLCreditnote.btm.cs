namespace BYGE.Integration.UBLCreditnote {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.Core.Invoice.Invoice", typeof(global::BYGE.Integration.Core.Invoice.Invoice))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.UBLCreditnote.UBLCreditnote", typeof(global::BYGE.Integration.UBLCreditnote.UBLCreditnote))]
    public sealed class CoreInvoice_to_UBLCreditnote : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-8""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var s0 userCSharp"" version=""1.0"" xmlns:ns0=""urn:oasis:names:specification:ubl:schema:xsd:CreditNote-2"" xmlns:s0=""http://byg-e.dk/schemas/v10"" xmlns:userCSharp=""http://schemas.microsoft.com/BizTalk/2003/userCSharp"" xmlns=""urn:oasis:names:specification:ubl:schema:xsd:CreditNote-2"" xmlns:cac=""urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"" xmlns:cbc=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" xmlns:qdt=""urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2"" xmlns:ext=""urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:udt=""urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2"" xmlns:ccts=""urn:oasis:names:specification:ubl:schema:xsd:CoreComponentParameters-2"" xmlns:sdt=""urn:oasis:names:specification:ubl:schema:xsd:SpecializedDatatypes-2"">
  <!-- This mapping is being used. Make all adjustments here -->
  <!-- BDB/SJ 210420: New CRENOT mapping -->
  <!-- BDB/SJ 220420: All test="" changed to test=""string-length -->
  <!-- BDB/SJ 290420: Unitcode adjusted and userCSharp added for Street/Number -->
  <!-- BDB/SJ 290420: InvoiceType promoted in CoreInvoice -->
  <!-- BDB/SJ 130520: AdditionalStreetName is not allowed in outgoing format -->
  <!-- BDB/SJ 130520: PayeeParty EndpointID and PartyIdentification out if no content -->
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/s0:Invoice"" />
  </xsl:template>
  <xsl:template match=""/s0:Invoice"">
    <ns0:CreditNote xsi:schemaLocation=""urn:oasis:names:specification:ubl:schema:xsd:CreditNote-2 UBLCreditnote.xsd"">
      <!-- Saving fixed values for later - Start -->
      <xsl:variable name=""Text320"" select=""'320'"" />
      <!-- Saving fixed values for later - End -->
      <cbc:UBLVersionID>
        <xsl:value-of select=""'2.0'"" />
      </cbc:UBLVersionID>
      <cbc:CustomizationID>
        <xsl:value-of select=""'OIOUBL-2.02'"" />
      </cbc:CustomizationID>
      <cbc:ProfileID>
        <xsl:attribute name=""schemeID"">
          <xsl:value-of select=""'urn:oioubl:id:profileid-1.2'"" />
        </xsl:attribute>
        <xsl:attribute name=""schemeAgencyID"">
          <xsl:value-of select=""$Text320"" />
        </xsl:attribute>
        <xsl:value-of select=""ProfileID/text()"" />
      </cbc:ProfileID>
      <cbc:ID>
        <xsl:value-of select=""InvoiceID/text()"" />
      </cbc:ID>
      <xsl:if test=""string-length(CopyIndicator/text()) != 0"">
        <cbc:CopyIndicator>
          <xsl:value-of select=""CopyIndicator/text()"" />
        </cbc:CopyIndicator>
      </xsl:if>
      <cbc:IssueDate>
        <xsl:value-of select=""IssueDate/text()"" />
      </cbc:IssueDate>         

  
      <xsl:if test=""string-length(Note/text()) != 0"">
        <cbc:Note>
          <xsl:value-of select=""Note/text()"" />
        </cbc:Note>
      </xsl:if>
      <!-- Saving Currency for later to all Amount and Price segments -->
      <xsl:variable name=""CurrencyCode"" select=""Currency/text()"" />
      <xsl:if test=""string-length($CurrencyCode) = 0"">
        <xsl:value-of select=""'DKK'"" />
      </xsl:if>
      <cbc:DocumentCurrencyCode>
        <xsl:value-of select=""$CurrencyCode"" />
      </cbc:DocumentCurrencyCode>
      <xsl:if test=""string-length(DespatchDocumentReference/ID/text()) != 0"">
        <cac:DiscrepancyResponse>
          <cbc:ReferenceID>
            <xsl:value-of select=""DespatchDocumentReference/ID/text()"" />
          </cbc:ReferenceID>
        </cac:DiscrepancyResponse>
      </xsl:if>
      <cac:OrderReference>
        <cbc:ID>
          <xsl:value-of select=""OrderReference/OrderID/text()"" />
        </cbc:ID>
        <xsl:if test=""string-length(OrderReference/SalesOrderID/text()) != 0"">
          <cbc:SalesOrderID>
            <xsl:value-of select=""OrderReference/SalesOrderID/text()"" />
          </cbc:SalesOrderID>
        </xsl:if>
        <xsl:if test=""string-length(OrderReference/OrderDate/text()) != 0"">
          <cbc:IssueDate>
            <xsl:value-of select=""OrderReference/OrderDate/text()"" />
          </cbc:IssueDate>
        </xsl:if>
      </cac:OrderReference>
      <!-- AccountingSupplierParty -->  
      <cac:AccountingSupplierParty>
        <cac:Party>
          <cbc:EndpointID>
            <xsl:if test=""string-length(AccountingSupplier/SchemeID/Endpoint/text()) != 0"">
              <xsl:attribute name=""schemeID"">
                <xsl:value-of select=""AccountingSupplier/SchemeID/Endpoint/text()"" />
              </xsl:attribute>
            </xsl:if>
            <xsl:value-of select=""AccountingSupplier/AccSellerID/text()"" />
          </cbc:EndpointID>
          <cac:PartyIdentification>
            <cbc:ID>
              <xsl:if test=""string-length(AccountingSupplier/SchemeID/Party/text()) != 0"">
                <xsl:attribute name=""schemeID"">
                  <xsl:value-of select=""AccountingSupplier/SchemeID/Party/text()"" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test=""string-length(AccountingSupplier/PartyID/text()) != 0"">
                <xsl:value-of select=""AccountingSupplier/PartyID/text()"" />
              </xsl:if>
            </cbc:ID>
          </cac:PartyIdentification>
          <xsl:for-each select=""AccountingSupplier/Address"">
            <cac:PartyName>
              <xsl:if test=""string-length(Name/text()) != 0"">
                <cbc:Name>
                  <xsl:value-of select=""Name/text()"" />
                </cbc:Name>
              </xsl:if>
            </cac:PartyName>
            <cac:PostalAddress>
              <cbc:AddressFormatCode>
                <xsl:attribute name=""listAgencyID"">
                  <xsl:value-of select=""$Text320"" />
                </xsl:attribute>
                <xsl:attribute name=""listID"">
                  <xsl:value-of select=""'urn:oioubl:codelist:addressformatcode-1.1'"" />
                </xsl:attribute>
                <xsl:if test=""string-length(AddressFormatCode/text()) = 0"">
                  <xsl:value-of select=""'StructuredDK'"" />
                </xsl:if>
                <xsl:if test=""string-length(AddressFormatCode/text()) != 0"">
                  <xsl:value-of select=""AddressFormatCode/text()"" />
                </xsl:if>
              </cbc:AddressFormatCode>
              <xsl:if test=""string-length(Street/text()) != 0"">
                <xsl:variable name=""fullStreetName"" select=""Street/text()"" />
                <cbc:StreetName>
                  <!-- xsl:value-of select=""Street/text()"" / -->
                  <xsl:value-of select=""userCSharp:GetStreetName($fullStreetName)"" />
                </cbc:StreetName>
                <cbc:BuildingNumber>
                  <xsl:choose>
                    <xsl:when test=""string-length(Number/text()) = 0"">
                      <xsl:value-of select=""userCSharp:GetStreetNumber($fullStreetName)"" />
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select=""Number/text()"" />
                    </xsl:otherwise>
                  </xsl:choose>
                </cbc:BuildingNumber>
              </xsl:if>
              <!-- BDB/SJ 130520: AdditionalStreetName is not allowed in outgoing format
              <xsl:if test=""string-length(AdditionalStreetName/text()) != 0"">
                <cbc:AdditionalStreetName>
                  <xsl:value-of select=""AdditionalStreetName/text()"" />
                </cbc:AdditionalStreetName>
              </xsl:if                  -->
              <xsl:if test=""string-length(City/text()) != 0"">
                <cbc:CityName>
                  <xsl:value-of select=""City/text()"" />
                </cbc:CityName>
              </xsl:if>
              <xsl:if test=""string-length(PostalCode/text()) != 0"">
                <cbc:PostalZone>
                  <xsl:value-of select=""PostalCode/text()"" />
                </cbc:PostalZone>
              </xsl:if>
              <cac:Country>
                <xsl:if test=""string-length(Country/text()) != 0"">
                  <cbc:IdentificationCode>
                    <xsl:value-of select=""Country/text()"" />
                  </cbc:IdentificationCode>
                </xsl:if>
              </cac:Country>
            </cac:PostalAddress>
          </xsl:for-each>
          <xsl:if test=""string-length(AccountingSupplier/CompanyTaxID/text()) != 0"">
            <cac:PartyTaxScheme>
              <cbc:CompanyID>
                <xsl:attribute name=""schemeID"">
                  <xsl:value-of select=""AccountingSupplier/SchemeID/CompanyTax/text()"" />
                </xsl:attribute>
                <xsl:value-of select=""AccountingSupplier/CompanyTaxID/text()"" />
              </cbc:CompanyID>
            </cac:PartyTaxScheme>
          </xsl:if>
          <xsl:if test=""string-length(AccountingSupplier/CompanyID/text()) != 0"">
            <cac:PartyLegalEntity>
              <cbc:CompanyID>
                <xsl:if test=""string-length(AccountingSupplier/SchemeID/Company/text()) != 0"">
                  <xsl:attribute name=""schemeID"">
                    <xsl:value-of select=""AccountingSupplier/SchemeID/Company/text()"" />
                  </xsl:attribute>
                </xsl:if>
                <xsl:value-of select=""AccountingSupplier/CompanyID/text()"" />
              </cbc:CompanyID>
            </cac:PartyLegalEntity>
          </xsl:if>  
        </cac:Party>
        <xsl:for-each select=""AccountingSupplier/Concact"">
          <cac:SellerContact>
            <cbc:ID>
              <xsl:value-of select=""ID/text()"" />
            </cbc:ID>
            <xsl:if test=""string-length(Name/text()) != 0"">
              <cbc:Name>
                <xsl:value-of select=""Name/text()"" />
              </cbc:Name>
            </xsl:if>
            <xsl:if test=""string-length(Telephone/text()) != 0"">
              <cbc:Telephone>
                <xsl:value-of select=""Telephone/text()"" />
              </cbc:Telephone>
            </xsl:if>
            <xsl:if test=""string-length(Telefax/text()) != 0"">
              <cbc:Telefax>
                <xsl:value-of select=""Telefax/text()"" />
              </cbc:Telefax>
            </xsl:if>
            <xsl:if test=""string-length(E-mail/text()) != 0"">
              <cbc:ElectronicMail>
                <xsl:value-of select=""E-mail/text()"" />
              </cbc:ElectronicMail>
            </xsl:if>
          </cac:SellerContact>
        </xsl:for-each>
      </cac:AccountingSupplierParty>
      <!-- AccountingCustomerParty -->  
      <cac:AccountingCustomerParty>
        <xsl:if test=""string-length(AccountingCustomer/SupplierAssignedAccountID/text()) != 0"">
          <cbc:SupplierAssignedAccountID>
            <xsl:value-of select=""AccountingCustomer/SupplierAssignedAccountID/text()"" />
          </cbc:SupplierAssignedAccountID>
        </xsl:if>
        <cac:Party>
          <cbc:EndpointID>
            <xsl:if test=""string-length(AccountingCustomer/SchemeID/Endpoint/text()) != 0"">
              <xsl:attribute name=""schemeID"">
                <xsl:value-of select=""AccountingCustomer/SchemeID/Endpoint/text()"" />
              </xsl:attribute>
            </xsl:if>
            <xsl:value-of select=""AccountingCustomer/AccBuyerID/text()"" />
          </cbc:EndpointID>
          <cac:PartyIdentification>
            <cbc:ID>
              <xsl:if test=""string-length(AccountingCustomer/SchemeID/Party/text()) != 0"">
                <xsl:attribute name=""schemeID"">
                  <xsl:value-of select=""AccountingCustomer/SchemeID/Party/text()"" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test=""string-length(AccountingCustomer/PartyID/text()) != 0"">
                <xsl:value-of select=""AccountingCustomer/PartyID/text()"" />
              </xsl:if>
            </cbc:ID>
          </cac:PartyIdentification>
          <xsl:for-each select=""AccountingCustomer/Address"">
            <cac:PartyName>
              <xsl:if test=""string-length(Name/text()) != 0"">
                <cbc:Name>
                  <xsl:value-of select=""Name/text()"" />
                </cbc:Name>
              </xsl:if>
            </cac:PartyName>
            <cac:PostalAddress>
              <cbc:AddressFormatCode>
                <xsl:attribute name=""listAgencyID"">
                  <xsl:value-of select=""$Text320"" />
                </xsl:attribute>
                <xsl:attribute name=""listID"">
                  <xsl:value-of select=""'urn:oioubl:codelist:addressformatcode-1.1'"" />
                </xsl:attribute>
                <xsl:if test=""string-length(AddressFormatCode/text()) = 0"">
                  <xsl:value-of select=""'StructuredDK'"" />
                </xsl:if>
                <xsl:if test=""string-length(AddressFormatCode/text()) != 0"">
                  <xsl:value-of select=""AddressFormatCode/text()"" />
                </xsl:if>
              </cbc:AddressFormatCode>
              <xsl:if test=""string-length(Street/text()) != 0"">
                <xsl:variable name=""fullStreetName"" select=""Street/text()"" />
                <cbc:StreetName>
                  <!-- xsl:value-of select=""Street/text()"" / -->
                  <xsl:value-of select=""userCSharp:GetStreetName($fullStreetName)"" />
                </cbc:StreetName>
                <cbc:BuildingNumber>
                  <xsl:choose>
                    <xsl:when test=""string-length(Number/text()) = 0"">
                      <xsl:value-of select=""userCSharp:GetStreetNumber($fullStreetName)"" />
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select=""Number/text()"" />
                    </xsl:otherwise>
                  </xsl:choose>
                </cbc:BuildingNumber>
              </xsl:if>
              <!-- BDB/SJ 130520: AdditionalStreetName is not allowed in outgoing format 
              <xsl:if test=""string-length(AdditionalStreetName/text()) != 0"">
                <cbc:AdditionalStreetName>
                  <xsl:value-of select=""AdditionalStreetName/text()"" />
                </cbc:AdditionalStreetName>
              </xsl:if                  -->
              <xsl:if test=""string-length(City/text()) != 0"">
                <cbc:CityName>
                  <xsl:value-of select=""City/text()"" />
                </cbc:CityName>
              </xsl:if>
              <xsl:if test=""string-length(PostalCode/text()) != 0"">
                <cbc:PostalZone>
                  <xsl:value-of select=""PostalCode/text()"" />
                </cbc:PostalZone>
              </xsl:if>
              <cac:Country>
                <xsl:if test=""string-length(Country/text()) != 0"">
                  <cbc:IdentificationCode>
                    <xsl:value-of select=""Country/text()"" />
                  </cbc:IdentificationCode>
                </xsl:if>
              </cac:Country>
            </cac:PostalAddress>
          </xsl:for-each>
          <xsl:if test=""string-length(AccountingCustomer/CompanyID/text()) != 0"">
            <cac:PartyLegalEntity>
              <cbc:CompanyID>
                <xsl:if test=""string-length(AccountingCustomer/SchemeID/Company/text()) != 0"">
                  <xsl:attribute name=""schemeID"">
                    <xsl:value-of select=""AccountingCustomer/SchemeID/Company/text()"" />
                  </xsl:attribute>
                </xsl:if>
                <xsl:value-of select=""AccountingCustomer/CompanyID/text()"" />
              </cbc:CompanyID>
            </cac:PartyLegalEntity>
          </xsl:if>  
          <xsl:for-each select=""AccountingCustomer/Concact"">
            <cac:Contact>
              <cbc:ID>
                <xsl:value-of select=""ID/text()"" />
              </cbc:ID>
              <xsl:if test=""string-length(Name/text()) != 0"">
                <cbc:Name>
                  <xsl:value-of select=""Name/text()"" />
                </cbc:Name>
              </xsl:if>
              <xsl:if test=""string-length(Telephone/text()) != 0"">
                <cbc:Telephone>
                  <xsl:value-of select=""Telephone/text()"" />
                </cbc:Telephone>
              </xsl:if>
              <xsl:if test=""string-length(Telefax/text()) != 0"">
                <cbc:Telefax>
                  <xsl:value-of select=""Telefax/text()"" />
                </cbc:Telefax>
              </xsl:if>
              <xsl:if test=""string-length(E-mail/text()) != 0"">
                <cbc:ElectronicMail>
                  <xsl:value-of select=""E-mail/text()"" />
                </cbc:ElectronicMail>
              </xsl:if>
            </cac:Contact>
          </xsl:for-each>
        </cac:Party>
      </cac:AccountingCustomerParty>
      <!-- PayeeParty -->
      <!-- BDB/SJ 130520: PayeeParty EndpointID and PartyIdentification out if no content -->
      <cac:PayeeParty>
        <xsl:for-each select=""BuyerCustomer"">
          <xsl:if test=""string-length(BuyerID/text()) != 0"">
            <cbc:EndpointID>
              <xsl:if test=""string-length(SchemeID/Endpoint/text()) != 0"">
                <xsl:attribute name=""schemeID"">
                  <xsl:value-of select=""SchemeID/Endpoint/text()"" />
                </xsl:attribute>
              </xsl:if>
              <xsl:value-of select=""BuyerID/text()"" />
            </cbc:EndpointID>
          </xsl:if>  
          <xsl:if test=""string-length(PartyID/text()) != 0"">
            <cac:PartyIdentification>    
              <cbc:ID>
                <xsl:if test=""string-length(SchemeID/Party/text()) != 0"">
                  <xsl:attribute name=""schemeID"">
                    <xsl:value-of select=""SchemeID/Party/text()"" />
                  </xsl:attribute>
                </xsl:if>
                <xsl:value-of select=""PartyID/text()"" />
              </cbc:ID>
            </cac:PartyIdentification>
          </xsl:if>  
          <xsl:for-each select=""Address"">
            <cac:PartyName>
              <cbc:Name>
                <xsl:value-of select=""Name/text()"" />
              </cbc:Name>
            </cac:PartyName>
            <cac:PostalAddress>
              <cbc:AddressFormatCode>
                <xsl:attribute name=""listAgencyID"">
                  <xsl:value-of select=""$Text320"" />
                </xsl:attribute>
                <xsl:attribute name=""listID"">
                  <xsl:value-of select=""'urn:oioubl:codelist:addressformatcode-1.1'"" />
                </xsl:attribute>
                <xsl:if test=""string-length(AddressFormatCode/text()) = 0"">
                  <xsl:value-of select=""'StructuredDK'"" />
                </xsl:if>
                <xsl:if test=""string-length(AddressFormatCode/text()) != 0"">
                  <xsl:value-of select=""AddressFormatCode/text()"" />
                </xsl:if>
              </cbc:AddressFormatCode>
              <xsl:if test=""string-length(Street/text()) != 0"">
                <xsl:variable name=""fullStreetName"" select=""Street/text()"" />
                <cbc:StreetName>
                  <!-- xsl:value-of select=""Street/text()"" / -->
                  <xsl:value-of select=""userCSharp:GetStreetName($fullStreetName)"" />
                </cbc:StreetName>
                <cbc:BuildingNumber>
                  <xsl:choose>
                    <xsl:when test=""string-length(Number/text()) = 0"">
                      <xsl:value-of select=""userCSharp:GetStreetNumber($fullStreetName)"" />
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select=""Number/text()"" />
                    </xsl:otherwise>
                  </xsl:choose>
                </cbc:BuildingNumber>
              </xsl:if>
              <!-- BDB/SJ 130520: AdditionalStreetName is not allowed in outgoing format 
              <xsl:if test=""string-length(AdditionalStreetName/text()) != 0"">
                <cbc:AdditionalStreetName>
                  <xsl:value-of select=""AdditionalStreetName/text()"" />
                </cbc:AdditionalStreetName>
              </xsl:if                  -->
              <xsl:if test=""string-length(City/text()) != 0"">
                <cbc:CityName>
                  <xsl:value-of select=""City/text()"" />
                </cbc:CityName>
              </xsl:if>
              <xsl:if test=""string-length(PostalCode/text()) != 0"">
                <cbc:PostalZone>
                  <xsl:value-of select=""PostalCode/text()"" />
                </cbc:PostalZone>
              </xsl:if>
              <cac:Country>
                <xsl:if test=""string-length(Country/text()) != 0"">
                  <cbc:IdentificationCode>
                    <xsl:value-of select=""Country/text()"" />
                  </cbc:IdentificationCode>
                </xsl:if>
              </cac:Country>
            </cac:PostalAddress>
          </xsl:for-each>
          <xsl:if test=""string-length(CompanyID/text()) != 0"">
            <cac:PartyLegalEntity>
              <cbc:CompanyID>
                <xsl:if test=""string-length(SchemeID/Company/text()) != 0"">
                  <xsl:attribute name=""schemeID"">
                    <xsl:value-of select=""SchemeID/Company/text()"" />
                  </xsl:attribute>
                </xsl:if>
                <xsl:value-of select=""CompanyID/text()"" />
              </cbc:CompanyID>
            </cac:PartyLegalEntity>
          </xsl:if>  
        </xsl:for-each>
      </cac:PayeeParty>







      <!-- TaxTotal -->
      <cac:TaxTotal>
        <cbc:TaxAmount>
          <xsl:attribute name=""currencyID"">
            <xsl:value-of select=""$CurrencyCode"" />
          </xsl:attribute>
          <xsl:value-of select=""TaxTotal/TaxAmount/text()"" />
        </cbc:TaxAmount>
        <cac:TaxSubtotal>
          <xsl:for-each select=""TaxTotal/TaxSubtotal"">
            <cbc:TaxableAmount>
              <xsl:if test=""string-length(TaxableAmount/text()) != 0"">
                <xsl:attribute name=""currencyID"">
                  <xsl:value-of select=""$CurrencyCode"" />
                </xsl:attribute>
                <xsl:value-of select=""TaxableAmount/text()"" />
              </xsl:if>
            </cbc:TaxableAmount>
            <cbc:TaxAmount>
              <xsl:if test=""string-length(TaxAmount/text()) != 0"">
                <xsl:attribute name=""currencyID"">
                  <xsl:value-of select=""$CurrencyCode"" />
                </xsl:attribute>
                <xsl:value-of select=""TaxAmount/text()"" />
              </xsl:if>
            </cbc:TaxAmount>
            <!-- TaxCategory - Only map if present -->
            <xsl:for-each select=""TaxCategory"">
              <cac:TaxCategory>
                <cbc:ID>
                  <xsl:attribute name=""schemeAgencyID"">
                    <xsl:value-of select=""$Text320"" />
                  </xsl:attribute>
                  <xsl:attribute name=""schemeID"">
                    <xsl:value-of select=""'urn:oioubl:id:taxcategoryid-1.1'"" />
                  </xsl:attribute>
                  <xsl:value-of select=""ID/text()"" />
                </cbc:ID>
                <cbc:Percent>
                  <xsl:if test=""string-length(Percent/text()) = 0"">
                    <xsl:value-of select=""'0.0000'"" />
                  </xsl:if>
                  <xsl:if test=""string-length(Percent/text()) != 0"">
                    <xsl:value-of select=""Percent/text()"" />
                  </xsl:if>
                </cbc:Percent>
                <cac:TaxScheme>
                  <cbc:ID>
                    <xsl:attribute name=""schemeAgencyID"">
                      <xsl:value-of select=""$Text320"" />
                    </xsl:attribute>
                    <xsl:attribute name=""schemeID"">
                      <xsl:value-of select=""'urn:oioubl:id:taxschemeid-1.1'"" />
                    </xsl:attribute>
                    <xsl:if test=""string-length(TaxScheme/ID/text()) = 0"">
                      <xsl:value-of select=""'63'"" />
                    </xsl:if>
                    <xsl:if test=""string-length(TaxScheme/ID/text()) != 0"">
                      <xsl:value-of select=""TaxScheme/ID/text()"" />
                    </xsl:if>
                  </cbc:ID>
                  <!-- If no Name use TaxTypeCode -->
                  <xsl:if test=""string-length(TaxScheme/Navn/text()) != 0"">
                    <cbc:Name>
                      <xsl:value-of select=""TaxScheme/Navn/text()"" />
                    </cbc:Name>
                  </xsl:if>
                  <xsl:if test=""string-length(TaxScheme/Navn/text()) = 0"">
                    <cbc:Name>
                      <!-- xsl:value-of select=""TaxScheme/TaxTypeCode/text()"" / -->
                      <xsl:value-of select=""'Moms'"" />
                    </cbc:Name>
                  </xsl:if>
                </cac:TaxScheme>
              </cac:TaxCategory>
            </xsl:for-each>
          </xsl:for-each>
        </cac:TaxSubtotal>
      </cac:TaxTotal>
      <!-- LegalMonetaryTotal -->
      <cac:LegalMonetaryTotal>
        <cbc:LineExtensionAmount>
          <xsl:attribute name=""currencyID"">
            <xsl:value-of select=""$CurrencyCode"" />
          </xsl:attribute>
          <xsl:value-of select=""Total/LineTotalAmount/text()"" />
        </cbc:LineExtensionAmount>
        <xsl:if test=""string-length(Total/TaxExclAmount/text()) != 0"">
          <cbc:TaxExclusiveAmount>
            <xsl:attribute name=""currencyID"">
              <xsl:value-of select=""$CurrencyCode"" />
            </xsl:attribute>
            <xsl:value-of select=""Total/TaxExclAmount/text()"" />
          </cbc:TaxExclusiveAmount>
        </xsl:if>
        <xsl:if test=""string-length(Total/TaxInclAmount/text()) != 0"">
          <cbc:TaxInclusiveAmount>
            <xsl:attribute name=""currencyID"">
              <xsl:value-of select=""$CurrencyCode"" />
            </xsl:attribute>
            <xsl:value-of select=""Total/TaxInclAmount/text()"" />
          </cbc:TaxInclusiveAmount>
        </xsl:if>
        <xsl:if test=""string-length(Total/PayableAmount/text()) != 0"">
          <cbc:PayableAmount>
            <xsl:attribute name=""currencyID"">
              <xsl:value-of select=""$CurrencyCode"" />
            </xsl:attribute>
            <xsl:value-of select=""Total/PayableAmount/text()"" />
          </cbc:PayableAmount>
        </xsl:if>
      </cac:LegalMonetaryTotal>
      <!-- Lines/Line -->
      <xsl:for-each select=""Lines/Line"">
        <cac:CreditNoteLine>
          <cbc:ID>
            <xsl:value-of select=""LineNo/text()"" />
          </cbc:ID>
          <cbc:CreditedQuantity>
            <xsl:attribute name=""unitCode"">
              <xsl:choose>
                <xsl:when test=""string-length(UnitCode/text()) = 0"">
                  <xsl:value-of select=""'EA'"" />
                </xsl:when>
                <xsl:when test=""UnitCode/text() ='PCE'"">
                  <xsl:value-of select=""'EA'"" />
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select=""UnitCode/text()"" />
                </xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>
            <xsl:value-of select=""Quantity/text()"" />
          </cbc:CreditedQuantity>
          <cbc:LineExtensionAmount>
            <xsl:attribute name=""currencyID"">
              <xsl:value-of select=""$CurrencyCode"" />
            </xsl:attribute>
            <xsl:value-of select=""LineAmountTotal/text()"" />
          </cbc:LineExtensionAmount>
          <!-- Line Grossprice -->
          <cac:PricingReference>
            <cac:AlternativeConditionPrice>
              <cbc:PriceAmount>
                <xsl:attribute name=""currencyID"">
                  <xsl:value-of select=""$CurrencyCode"" />
                </xsl:attribute>
                <xsl:if test=""string-length(PriceGross/Price/text()) = 0"">
                  <xsl:value-of select=""'1.00'"" />
                </xsl:if>
                <xsl:if test=""string-length(PriceGross/Price/text()) != 0"">
                  <xsl:value-of select=""PriceGross/Price/text()"" />
                </xsl:if>
              </cbc:PriceAmount>
              <cbc:BaseQuantity>
                <xsl:attribute name=""unitCode"">
                  <xsl:choose>
                    <xsl:when test=""string-length(PriceGross/UnitCode/text()) = 0"">
                      <xsl:value-of select=""'EA'"" />
                    </xsl:when>
                    <xsl:when test=""PriceGross/UnitCode/text() ='PCE'"">
                      <xsl:value-of select=""'EA'"" />
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select=""PriceGross/UnitCode/text()"" />
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
                <xsl:if test=""string-length(PriceGross/Quantity/text()) = 0"">
                  <xsl:value-of select=""'1.000'"" />
                </xsl:if>
                <xsl:if test=""string-length(PriceGross/Quantity/text()) != 0"">
                  <xsl:value-of select=""PriceGross/Quantity/text()"" />
                </xsl:if>               
              </cbc:BaseQuantity>
              <xsl:if test=""string-length(PriceGross/PristypeCode/text()) != 0"">
                <cbc:PriceTypeCode>
                  <xsl:value-of select=""PriceGross/PristypeCode/text()"" />
                </cbc:PriceTypeCode>
              </xsl:if>
            </cac:AlternativeConditionPrice>
          </cac:PricingReference>
          <!-- Line TaxTotal -->
          <xsl:if test=""string-length(TaxTotal/TaxAmount/text()) != 0"">
            <cac:TaxTotal>
              <cbc:TaxAmount>
                <xsl:if test=""string-length(TaxTotal/TaxAmount/text()) != 0"">
                  <xsl:attribute name=""currencyID"">
                    <xsl:value-of select=""$CurrencyCode"" />
                  </xsl:attribute>
                  <xsl:if test=""string-length(TaxTotal/TaxAmount/text()) = 0"">
                    <xsl:value-of select=""'0.00'"" />
                  </xsl:if>
                  <xsl:if test=""string-length(TaxTotal/TaxAmount/text()) != 0"">
                    <xsl:value-of select=""TaxTotal/TaxAmount/text()"" />
                  </xsl:if>
                </xsl:if>
              </cbc:TaxAmount>
              <cac:TaxSubtotal>
                <cbc:TaxableAmount>
                  <xsl:if test=""string-length(TaxTotal/TaxSubtotal/TaxableAmount/text()) != 0"">
                    <xsl:attribute name=""currencyID"">
                      <xsl:value-of select=""$CurrencyCode"" />
                    </xsl:attribute>
                    <xsl:if test=""string-length(TaxTotal/TaxSubtotal/TaxableAmount/text()) = 0"">
                      <xsl:value-of select=""'0.00'"" />
                    </xsl:if>
                    <xsl:if test=""string-length(TaxTotal/TaxSubtotal/TaxableAmount/text()) != 0"">
                      <xsl:value-of select=""TaxTotal/TaxSubtotal/TaxableAmount/text()"" />
                    </xsl:if>
                  </xsl:if>
                </cbc:TaxableAmount>
                <cbc:TaxAmount>
                  <xsl:attribute name=""currencyID"">
                    <xsl:value-of select=""$CurrencyCode"" />
                  </xsl:attribute>
                  <xsl:if test=""string-length(TaxTotal/TaxSubtotal/TaxAmount/text()) = 0"">
                    <xsl:value-of select=""'0.00'"" />
                  </xsl:if>
                  <xsl:if test=""string-length(TaxTotal/TaxSubtotal/TaxAmount/text()) != 0"">
                    <xsl:value-of select=""TaxTotal/TaxSubtotal/TaxAmount/text()"" />
                  </xsl:if>
                </cbc:TaxAmount>
                <!-- TaxCategory - Only map if present -->
                <xsl:for-each select=""TaxTotal/TaxSubtotal/TaxCategory"">
                  <cac:TaxCategory>
                    <cbc:ID>
                      <xsl:attribute name=""schemeAgencyID"">
                        <xsl:value-of select=""$Text320"" />
                      </xsl:attribute>
                      <xsl:attribute name=""schemeID"">
                        <xsl:value-of select=""'urn:oioubl:id:taxcategoryid-1.1'"" />
                      </xsl:attribute>
                      <xsl:value-of select=""ID/text()"" />
                    </cbc:ID>
                    <cbc:Percent>
                      <xsl:if test=""string-length(Percent/text()) = 0"">
                        <xsl:value-of select=""'0.0000'"" />
                      </xsl:if>
                      <xsl:if test=""string-length(Percent/text()) != 0"">
                        <xsl:value-of select=""Percent/text()"" />
                      </xsl:if>
                    </cbc:Percent>
                    <cac:TaxScheme>
                      <cbc:ID>
                        <xsl:attribute name=""schemeAgencyID"">
                          <xsl:value-of select=""$Text320"" />
                        </xsl:attribute>
                        <xsl:attribute name=""schemeID"">
                          <xsl:value-of select=""'urn:oioubl:id:taxschemeid-1.1'"" />
                        </xsl:attribute>
                        <xsl:if test=""string-length(TaxScheme/ID/text()) = 0"">
                          <xsl:value-of select=""'63'"" />
                        </xsl:if>
                        <xsl:if test=""string-length(TaxScheme/ID/text()) != 0"">
                          <xsl:value-of select=""TaxScheme/ID/text()"" />
                        </xsl:if>
                      </cbc:ID>
                      <!-- If no Name use TaxTypeCode -->
                      <xsl:if test=""string-length(TaxScheme/Navn/text()) != 0"">
                        <cbc:Name>
                          <xsl:value-of select=""TaxScheme/Navn/text()"" />
                        </cbc:Name>
                      </xsl:if>
                      <xsl:if test=""string-length(TaxScheme/Navn/text()) = 0"">
                        <cbc:Name>
                          <!-- xsl:value-of select=""TaxScheme/TaxTypeCode/text()"" / -->
                          <xsl:value-of select=""'Moms'"" />
                        </cbc:Name>
                      </xsl:if>
                    </cac:TaxScheme>
                  </cac:TaxCategory>
                </xsl:for-each>
              </cac:TaxSubtotal>
            </cac:TaxTotal>
          </xsl:if>  
          <!-- Line Item -->
          <cac:Item>
            <xsl:if test=""string-length(Item/Description/text()) != 0"">
              <cbc:Description>
                <xsl:value-of select=""Item/Description/text()"" />
              </cbc:Description>
            </xsl:if>
            <xsl:if test=""string-length(Item/Name/text()) != 0"">
              <cbc:Name>
                <xsl:value-of select=""Item/Name/text()"" />
              </cbc:Name>
            </xsl:if>
            <xsl:if test=""string-length(Item/BuyerItemID/text()) != 0"">
              <cac:BuyersItemIdentification>
                <cbc:ID>
                  <xsl:value-of select=""Item/BuyerItemID/text()"" />
                </cbc:ID>
              </cac:BuyersItemIdentification>
            </xsl:if>
            <xsl:if test=""string-length(Item/SellerItemID/text()) != 0"">
              <cac:SellersItemIdentification>
                <cbc:ID>
                  <xsl:value-of select=""Item/SellerItemID/text()"" />
                </cbc:ID>
              </cac:SellersItemIdentification>
            </xsl:if>
            <xsl:if test=""string-length(Item/StandardItemID/text()) != 0"">
              <cac:StandardItemIdentification>
                <cbc:ID>
                  <xsl:attribute name=""schemeID"">
                    <xsl:value-of select=""'EAN'"" />
                  </xsl:attribute>
                  <xsl:value-of select=""Item/StandardItemID/text()"" />
                </cbc:ID>
              </cac:StandardItemIdentification>
            </xsl:if>
            <xsl:if test=""string-length(Item/AdditionalItemID/text()) != 0"">
              <cac:AdditionalItemIdentification>
                <cbc:ID>
                  <xsl:attribute name=""schemeID"">
                    <xsl:value-of select=""'DB'"" />
                  </xsl:attribute>
                  <xsl:value-of select=""Item/AdditionalItemID/text()"" />
                </cbc:ID>
              </cac:AdditionalItemIdentification>
            </xsl:if>
            <xsl:if test=""string-length(Item/CatalogueItemID/text()) != 0"">
              <cac:CatalogueItemIdentification>
                <cbc:ID>
                  <xsl:attribute name=""schemeID"">
                    <xsl:value-of select=""'DB'"" />
                  </xsl:attribute>
                  <xsl:value-of select=""Item/CatalogueItemID/text()"" />
                </cbc:ID>
              </cac:CatalogueItemIdentification>
            </xsl:if>
          </cac:Item>
          <!-- Line Netprice -->
          <cac:Price>
            <cbc:PriceAmount>
              <xsl:attribute name=""currencyID"">
                <xsl:value-of select=""$CurrencyCode"" />
              </xsl:attribute>
              <xsl:if test=""string-length(PriceNet/Price/text()) = 0"">
                <xsl:value-of select=""'1.00'"" />
              </xsl:if>
              <xsl:if test=""string-length(PriceNet/Price/text()) != 0"">
                <xsl:value-of select=""PriceNet/Price/text()"" />
              </xsl:if>
            </cbc:PriceAmount>
            <cbc:BaseQuantity>
              <xsl:attribute name=""unitCode"">
                <xsl:choose>
                  <xsl:when test=""string-length(PriceNet/UnitCode/text()) = 0"">
                    <xsl:value-of select=""'EA'"" />
                  </xsl:when>
                  <xsl:when test=""PriceNet/UnitCode/text() ='PCE'"">
                    <xsl:value-of select=""'EA'"" />
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select=""PriceNet/UnitCode/text()"" />
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:attribute>
              <xsl:if test=""string-length(PriceNet/Quantity/text()) = 0"">
                <xsl:value-of select=""'1.000'"" />
              </xsl:if>
              <xsl:if test=""string-length(PriceNet/Quantity/text()) != 0"">
                <xsl:value-of select=""PriceNet/Quantity/text()"" />
              </xsl:if>
            </cbc:BaseQuantity>
            <xsl:if test=""string-length(PriceNet/PristypeCode/text()) != 0"">
              <cbc:PriceTypeCode>
                <xsl:value-of select=""PriceNet/PristypeCode/text()"" />
              </cbc:PriceTypeCode>  
            </xsl:if>  
            <xsl:if test=""string-length(PriceNet/PristypeTekst/text()) != 0"">
              <cbc:PriceType>
                <xsl:value-of select=""PriceNet/PristypeTekst/text()"" />
              </cbc:PriceType>
            </xsl:if>
          </cac:Price>
        </cac:CreditNoteLine>
      </xsl:for-each>
    </ns0:CreditNote>
  </xsl:template>

  <msxsl:script language=""C#"" implements-prefix=""userCSharp"">
    <![CDATA[

  public string GetStreetNumber(string fullStreetName)
  {
      System.Text.RegularExpressions.Match match = System.Text.RegularExpressions.Regex.Match(fullStreetName, @""[0-9]"");
      int indexOfFirstNumber = match.Index;
      if (!match.Success)
      {
          return ""0"";
      }
      string streetName = fullStreetName.Substring(0, indexOfFirstNumber-1);
      string streetNumber = fullStreetName.Substring(indexOfFirstNumber);
      return streetNumber;
  }

  public string GetStreetName(string fullStreetName)
  {
      System.Text.RegularExpressions.Match match = System.Text.RegularExpressions.Regex.Match(fullStreetName, @""[0-9]"");
      int indexOfFirstNumber = match.Index;
      if (!match.Success)
      {
          return fullStreetName;
      }
      if (indexOfFirstNumber < 1)
      {
          return fullStreetName;
      }
      string streetName = fullStreetName.Substring(0, indexOfFirstNumber - 1);
      string streetNumber = fullStreetName.Substring(indexOfFirstNumber);
      return streetName;
  }
  
]]>
  </msxsl:script>  
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"BYGE.Integration.Core.Invoice.Invoice";
        
        private const global::BYGE.Integration.Core.Invoice.Invoice _srcSchemaTypeReference0 = null;
        
        private const string _strTrgSchemasList0 = @"BYGE.Integration.UBLCreditnote.UBLCreditnote";
        
        private const global::BYGE.Integration.UBLCreditnote.UBLCreditnote _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [1];
                _SrcSchemas[0] = @"BYGE.Integration.Core.Invoice.Invoice";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"BYGE.Integration.UBLCreditnote.UBLCreditnote";
                return _TrgSchemas;
            }
        }
    }
}
