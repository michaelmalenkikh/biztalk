namespace BYGE.Integration.UBLCreditnote {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"DiscrepancyResponse", @"OrderReference", @"AccountingSupplierParty", @"AccountingCustomerParty", @"PayeeParty", @"AllowanceCharge", @"TaxTotal", @"LegalMonetaryTotal", @"CreditNoteLine"})]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.UBLCreditnote.UBLCreditnote1", typeof(global::BYGE.Integration.UBLCreditnote.UBLCreditnote1))]
    public sealed class UBLCreditnote2 : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xs:schema xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" xmlns:tns=""urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"" attributeFormDefault=""unqualified"" elementFormDefault=""qualified"" targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"" xmlns:xs=""http://www.w3.org/2001/XMLSchema"">
  <xs:import schemaLocation=""BYGE.Integration.UBLCreditnote.UBLCreditnote1"" namespace=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" />
  <xs:annotation>
    <xs:appinfo>
      <b:references>
        <b:reference targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" />
      </b:references>
    </xs:appinfo>
  </xs:annotation>
  <xs:element name=""DiscrepancyResponse"">
    <xs:complexType>
      <xs:sequence>
        <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:ReferenceID"" />
        <xs:element xmlns:q2=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q2:Description"" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name=""OrderReference"">
    <xs:complexType>
      <xs:sequence>
        <xs:element xmlns:q3=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q3:ID"" />
        <xs:element xmlns:q4=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q4:SalesOrderID"" />
        <xs:element xmlns:q5=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q5:IssueDate"" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name=""AccountingSupplierParty"">
    <xs:complexType>
      <xs:sequence>
        <xs:element name=""Party"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q6=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q6:WebsiteURI"" />
              <xs:element xmlns:q7=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q7:EndpointID"" />
              <xs:element name=""PartyIdentification"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q8=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q8:ID"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""PartyName"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q9=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q9:Name"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""PostalAddress"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q10=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q10:AddressFormatCode"" />
                    <xs:element xmlns:q11=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q11:StreetName"" />
                    <xs:element xmlns:q12=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q12:AdditionalStreetName"" />
                    <xs:element xmlns:q13=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q13:InhouseMail"" />
                    <xs:element xmlns:q14=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q14:CityName"" />
                    <xs:element xmlns:q15=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q15:PostalZone"" />
                    <xs:element name=""Country"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element xmlns:q16=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q16:IdentificationCode"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""PartyTaxScheme"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q17=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q17:CompanyID"" />
                    <xs:element name=""TaxScheme"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element xmlns:q18=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q18:ID"" />
                          <xs:element xmlns:q19=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q19:Name"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""PartyLegalEntity"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q20=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q20:RegistrationName"" />
                    <xs:element xmlns:q21=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q21:CompanyID"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""Contact"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:ID"" />
                    <xs:element xmlns:q2=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q2:Name"" />
                    <xs:element xmlns:q3=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q3:Telephone"" />
                    <xs:element xmlns:q4=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q4:Telefax"" />
                    <xs:element xmlns:q5=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q5:ElectronicMail"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name=""SellerContact"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q6=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q6:ID"" />
              <xs:element xmlns:q7=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q7:Name"" />
              <xs:element xmlns:q8=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q8:Telephone"" />
              <xs:element xmlns:q9=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q9:Telefax"" />
              <xs:element xmlns:q10=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q10:ElectronicMail"" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name=""AccountingCustomerParty"">
    <xs:complexType>
      <xs:sequence>
        <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:SupplierAssignedAccountID"" />
        <xs:element name=""Party"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q26=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q26:EndpointID"" />
              <xs:element name=""PartyIdentification"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q27=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q27:ID"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""PartyName"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q28=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q28:Name"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""PostalAddress"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q29=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q29:AddressFormatCode"" />
                    <xs:element xmlns:q30=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q30:StreetName"" />
                    <xs:element xmlns:q31=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q31:AdditionalStreetName"" />
                    <xs:element xmlns:q32=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q32:CityName"" />
                    <xs:element xmlns:q33=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q33:PostalZone"" />
                    <xs:element name=""Country"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element xmlns:q34=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q34:IdentificationCode"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""PartyTaxScheme"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:CompanyID"" />
                    <xs:element name=""TaxScheme"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element xmlns:q2=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q2:ID"" />
                          <xs:element xmlns:q3=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q3:Name"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""PartyLegalEntity"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q4=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q4:RegistrationName"" />
                    <xs:element xmlns:q5=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q5:CompanyID"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""Contact"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q35=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q35:ID"" />
                    <xs:element xmlns:q36=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q36:Name"" />
                    <xs:element xmlns:q37=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q37:Telephone"" />
                    <xs:element xmlns:q38=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q38:Telefax"" />
                    <xs:element xmlns:q39=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q39:ElectronicMail"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name=""PayeeParty"">
    <xs:complexType>
      <xs:sequence>
        <xs:element xmlns:q11=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q11:EndpointID"" />
        <xs:element name=""PartyIdentification"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q12=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q12:ID"" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name=""PartyName"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q13=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q13:Name"" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name=""PostalAddress"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q14=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q14:AddressFormatCode"" />
              <xs:element xmlns:q15=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q15:StreetName"" />
              <xs:element xmlns:q16=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q16:AdditionalStreetName"" />
              <xs:element xmlns:q17=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q17:InhouseMail"" />
              <xs:element xmlns:q18=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q18:CityName"" />
              <xs:element xmlns:q19=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q19:PostalZone"" />
              <xs:element name=""Country"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q20=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q20:IdentificationCode"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name=""PartyTaxScheme"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q21=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q21:CompanyID"" />
              <xs:element name=""TaxScheme"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q22=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q22:ID"" />
                    <xs:element xmlns:q23=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q23:Name"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name=""PartyLegalEntity"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q24=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q24:RegistrationName"" />
              <xs:element xmlns:q25=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q25:CompanyID"" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name=""AllowanceCharge"">
    <xs:complexType>
      <xs:sequence>
        <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:ChargeIndicator"" />
        <xs:element xmlns:q2=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q2:AllowanceChargeReasonCode"" />
        <xs:element xmlns:q3=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q3:AllowanceChargeReason"" />
        <xs:element xmlns:q4=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q4:MultiplierFactorNumeric"" />
        <xs:element xmlns:q5=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q5:SequenceNumeric"" />
        <xs:element xmlns:q6=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q6:Amount"" />
        <xs:element xmlns:q7=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q7:BaseAmount"" />
        <xs:element name=""TaxCategory"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q8=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q8:ID"" />
              <xs:element name=""TaxScheme"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q9=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q9:ID"" />
                    <xs:element xmlns:q10=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q10:Name"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name=""TaxTotal"">
    <xs:complexType>
      <xs:sequence>
        <xs:element xmlns:q40=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q40:TaxAmount"" />
        <xs:element name=""TaxSubtotal"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q41=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q41:TaxableAmount"" />
              <xs:element xmlns:q42=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q42:TaxAmount"" />
              <xs:element name=""TaxCategory"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q43=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q43:ID"" />
                    <xs:element xmlns:q44=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q44:Percent"" />
                    <xs:element name=""TaxScheme"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element xmlns:q45=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q45:ID"" />
                          <xs:element xmlns:q46=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q46:Name"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name=""LegalMonetaryTotal"">
    <xs:complexType>
      <xs:sequence>
        <xs:element xmlns:q47=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q47:LineExtensionAmount"" />
        <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:TaxExclusiveAmount"" />
        <xs:element xmlns:q2=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q2:TaxInclusiveAmount"" />
        <xs:element xmlns:q48=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q48:PayableAmount"" />
        <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:TransactionCurrencyTaxAmount"" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name=""CreditNoteLine"">
    <xs:complexType>
      <xs:sequence>
        <xs:element xmlns:q49=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q49:ID"" />
        <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:Note"" />
        <xs:element xmlns:q50=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q50:CreditedQuantity"" />
        <xs:element xmlns:q51=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q51:LineExtensionAmount"" />
        <xs:element name=""PricingReference"">
          <xs:complexType>
            <xs:sequence>
              <xs:element name=""AlternativeConditionPrice"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q52=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q52:PriceAmount"" />
                    <xs:element xmlns:q53=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q53:BaseQuantity"" />
                    <xs:element xmlns:q54=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q54:PriceTypeCode"" />
                    <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:OrderableUnitFactorRate"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name=""TaxTotal"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q55=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q55:TaxAmount"" />
              <xs:element name=""TaxSubtotal"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q56=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q56:TaxableAmount"" />
                    <xs:element xmlns:q57=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q57:TaxAmount"" />
                    <xs:element name=""TaxCategory"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element xmlns:q58=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q58:ID"" />
                          <xs:element xmlns:q59=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q59:Percent"" />
                          <xs:element name=""TaxScheme"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element xmlns:q60=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q60:ID"" />
                                <xs:element xmlns:q61=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q61:Name"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name=""Item"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q62=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q62:Description"" />
              <xs:element xmlns:q63=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q63:Name"" />
              <xs:element name=""SellersItemIdentification"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q64=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q64:ID"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""StandardItemIdentification"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q65=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q65:ID"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""AdditionalItemIdentification"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q66=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q66:ID"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name=""Price"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q67=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q67:PriceAmount"" />
              <xs:element xmlns:q68=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q68:BaseQuantity"" />
              <xs:element xmlns:q69=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q69:OrderableUnitFactorRate"" />
              <xs:element name=""AllowanceCharge"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q70=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q70:ChargeIndicator"" />
                    <xs:element xmlns:q71=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q71:AllowanceChargeReasonCode"" />
                    <xs:element xmlns:q72=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q72:AllowanceChargeReason"" />
                    <xs:element xmlns:q73=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q73:MultiplierFactorNumeric"" />
                    <xs:element xmlns:q74=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q74:SequenceNumeric"" />
                    <xs:element xmlns:q75=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q75:Amount"" />
                    <xs:element xmlns:q76=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q76:BaseAmount"" />
                    <xs:element name=""TaxCategory"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element xmlns:q77=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q77:ID"" />
                          <xs:element name=""TaxScheme"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element xmlns:q78=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q78:ID"" />
                                <xs:element xmlns:q79=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q79:Name"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
</xs:schema>";
        
        public UBLCreditnote2() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [9];
                _RootElements[0] = "DiscrepancyResponse";
                _RootElements[1] = "OrderReference";
                _RootElements[2] = "AccountingSupplierParty";
                _RootElements[3] = "AccountingCustomerParty";
                _RootElements[4] = "PayeeParty";
                _RootElements[5] = "AllowanceCharge";
                _RootElements[6] = "TaxTotal";
                _RootElements[7] = "LegalMonetaryTotal";
                _RootElements[8] = "CreditNoteLine";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2",@"DiscrepancyResponse")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"DiscrepancyResponse"})]
        public sealed class DiscrepancyResponse : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public DiscrepancyResponse() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "DiscrepancyResponse";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2",@"OrderReference")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"OrderReference"})]
        public sealed class OrderReference : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public OrderReference() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "OrderReference";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2",@"AccountingSupplierParty")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"AccountingSupplierParty"})]
        public sealed class AccountingSupplierParty : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public AccountingSupplierParty() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "AccountingSupplierParty";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2",@"AccountingCustomerParty")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"AccountingCustomerParty"})]
        public sealed class AccountingCustomerParty : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public AccountingCustomerParty() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "AccountingCustomerParty";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2",@"PayeeParty")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"PayeeParty"})]
        public sealed class PayeeParty : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public PayeeParty() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "PayeeParty";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2",@"AllowanceCharge")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"AllowanceCharge"})]
        public sealed class AllowanceCharge : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public AllowanceCharge() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "AllowanceCharge";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2",@"TaxTotal")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"TaxTotal"})]
        public sealed class TaxTotal : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public TaxTotal() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "TaxTotal";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2",@"LegalMonetaryTotal")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"LegalMonetaryTotal"})]
        public sealed class LegalMonetaryTotal : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public LegalMonetaryTotal() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "LegalMonetaryTotal";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2",@"CreditNoteLine")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"CreditNoteLine"})]
        public sealed class CreditNoteLine : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public CreditNoteLine() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "CreditNoteLine";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
    }
}
