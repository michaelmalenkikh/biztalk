namespace BYGE.Integration.Core.Properties {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Property)]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"MessageID", @"ReceiverID", @"SenderID", @"Amount", @"AccountingBuyerID", @"BuyerID", @"InvoiceType", @"DocumentType", @"IssueDate", @"ReceiverName", @"SenderName", @"BuyerName", @"TestIndicator", @"UNB2Qualifier", @"UNB3Qualifier", @"UNB2Alt", @"UNB3Alt", @"UNB2AltQualifier", @"UNB3AltQualifier", @"BuyerPartyID", @"TypeDoc"})]
    public sealed class CoreProperties : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xs:schema xmlns=""http://BYGE.Integration.Core.Properties.CoreProperties"" xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" targetNamespace=""https://BYGE.Integration.Core.Properties.CoreProperties"" xmlns:xs=""http://www.w3.org/2001/XMLSchema"">
  <xs:annotation>
    <xs:appinfo>
      <b:schemaInfo schema_type=""property"" xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" />
    </xs:appinfo>
  </xs:annotation>
  <xs:element name=""MessageID"" type=""xs:string"">
    <xs:annotation>
      <xs:appinfo>
        <b:fieldInfo propertyGuid=""297be0d1-c946-44f0-9a01-ac81768327c4"" />
      </xs:appinfo>
    </xs:annotation>
  </xs:element>
  <xs:element name=""ReceiverID"" type=""xs:string"">
    <xs:annotation>
      <xs:appinfo>
        <b:fieldInfo propertyGuid=""4d8cd9cc-b16e-4dd3-9626-383f95ec3f34"" />
      </xs:appinfo>
    </xs:annotation>
  </xs:element>
  <xs:element name=""SenderID"" type=""xs:string"">
    <xs:annotation>
      <xs:appinfo>
        <b:fieldInfo propertyGuid=""5a14865b-522c-4d6f-b52e-9e50c9f33676"" />
      </xs:appinfo>
    </xs:annotation>
  </xs:element>
  <xs:element name=""Amount"" type=""xs:integer"">
    <xs:annotation>
      <xs:appinfo>
        <b:fieldInfo propertyGuid=""af63ff7b-fa97-4f5d-81d1-c74cfb7f74bc"" />
      </xs:appinfo>
    </xs:annotation>
  </xs:element>
  <xs:element name=""AccountingBuyerID"" type=""xs:string"">
    <xs:annotation>
      <xs:appinfo>
        <b:fieldInfo propertyGuid=""8bc3ab67-3372-4194-9ccc-038811b67f4e"" />
      </xs:appinfo>
    </xs:annotation>
  </xs:element>
  <xs:element name=""BuyerID"" type=""xs:string"">
    <xs:annotation>
      <xs:appinfo>
        <b:fieldInfo propertyGuid=""e0cf90a5-3a62-4ee5-b5c7-cc4f36bad42d"" />
      </xs:appinfo>
    </xs:annotation>
  </xs:element>
  <xs:element name=""InvoiceType"" type=""xs:string"">
    <xs:annotation>
      <xs:appinfo>
        <b:fieldInfo propertyGuid=""49287e3a-f3f1-40cd-8799-7d7ff4310e30"" />
      </xs:appinfo>
    </xs:annotation>
  </xs:element>
  <xs:element name=""DocumentType"" type=""xs:string"">
    <xs:annotation>
      <xs:appinfo>
        <b:fieldInfo propertyGuid=""039c4e25-d2dd-43d1-bb72-360260d1f8b2"" />
      </xs:appinfo>
    </xs:annotation>
  </xs:element>
  <xs:element name=""IssueDate"" type=""xs:date"">
    <xs:annotation>
      <xs:appinfo>
        <b:fieldInfo propertyGuid=""70e9db5c-6332-49b1-9c94-579b9561bc17"" />
      </xs:appinfo>
    </xs:annotation>
  </xs:element>
  <xs:element name=""ReceiverName"" type=""xs:string"">
    <xs:annotation>
      <xs:appinfo>
        <b:fieldInfo propertyGuid=""d131d092-e56f-405d-ac71-69028d5ae336"" />
      </xs:appinfo>
    </xs:annotation>
  </xs:element>
  <xs:element name=""SenderName"" type=""xs:string"">
    <xs:annotation>
      <xs:appinfo>
        <b:fieldInfo propertyGuid=""1ba48928-255a-4a72-af62-55f406f8be36"" />
      </xs:appinfo>
    </xs:annotation>
  </xs:element>
  <xs:element name=""BuyerName"" type=""xs:string"">
    <xs:annotation>
      <xs:appinfo>
        <b:fieldInfo propertyGuid=""1ac82233-9652-4b6c-bcab-bce38adc8904"" />
      </xs:appinfo>
    </xs:annotation>
  </xs:element>
  <xs:element name=""TestIndicator"" type=""xs:string"">
    <xs:annotation>
      <xs:appinfo>
        <b:fieldInfo propertyGuid=""6e2a5e80-25dd-4ea6-ae2a-47ba9af4dfcd"" />
      </xs:appinfo>
    </xs:annotation>
  </xs:element>
  <xs:element name=""UNB2Qualifier"" type=""xs:string"">
    <xs:annotation>
      <xs:appinfo>
        <b:fieldInfo propertyGuid=""5be8f137-a89c-4174-9e2b-73efa72ebcab"" />
      </xs:appinfo>
    </xs:annotation>
  </xs:element>
  <xs:element name=""UNB3Qualifier"" type=""xs:string"">
    <xs:annotation>
      <xs:appinfo>
        <b:fieldInfo propertyGuid=""af6d7889-d292-4d62-967f-4cf6f8f8dc92"" />
      </xs:appinfo>
    </xs:annotation>
  </xs:element>
  <xs:element name=""UNB2Alt"" type=""xs:string"">
    <xs:annotation>
      <xs:appinfo>
        <b:fieldInfo propertyGuid=""3444c756-4ecb-4360-aca4-a8d42e9a761a"" />
      </xs:appinfo>
    </xs:annotation>
  </xs:element>
  <xs:element name=""UNB3Alt"" type=""xs:string"">
    <xs:annotation>
      <xs:appinfo>
        <b:fieldInfo propertyGuid=""00ecef91-d885-4447-be91-c3613c1485c1"" />
      </xs:appinfo>
    </xs:annotation>
  </xs:element>
  <xs:element name=""UNB2AltQualifier"" type=""xs:string"">
    <xs:annotation>
      <xs:appinfo>
        <b:fieldInfo propertyGuid=""ef709422-4689-4c82-bef0-94cb3e996e13"" />
      </xs:appinfo>
    </xs:annotation>
  </xs:element>
  <xs:element name=""UNB3AltQualifier"" type=""xs:string"">
    <xs:annotation>
      <xs:appinfo>
        <b:fieldInfo propertyGuid=""6093a3fc-9f27-425c-a5c9-17c246985d66"" />
      </xs:appinfo>
    </xs:annotation>
  </xs:element>
  <xs:element name=""BuyerPartyID"" type=""xs:string"">
    <xs:annotation>
      <xs:appinfo>
        <b:fieldInfo propertyGuid=""ba94045a-959d-4a39-b7a6-8b4f2feef9f8"" />
      </xs:appinfo>
    </xs:annotation>
  </xs:element>
  <xs:element name=""TypeDoc"" type=""xs:string"">
    <xs:annotation>
      <xs:appinfo>
        <b:fieldInfo propertyGuid=""9a7bc469-1a06-4714-b9c4-63e59800aa0a"" />
      </xs:appinfo>
    </xs:annotation>
  </xs:element>
</xs:schema>";
        
        public CoreProperties() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [21];
                _RootElements[0] = "MessageID";
                _RootElements[1] = "ReceiverID";
                _RootElements[2] = "SenderID";
                _RootElements[3] = "Amount";
                _RootElements[4] = "AccountingBuyerID";
                _RootElements[5] = "BuyerID";
                _RootElements[6] = "InvoiceType";
                _RootElements[7] = "DocumentType";
                _RootElements[8] = "IssueDate";
                _RootElements[9] = "ReceiverName";
                _RootElements[10] = "SenderName";
                _RootElements[11] = "BuyerName";
                _RootElements[12] = "TestIndicator";
                _RootElements[13] = "UNB2Qualifier";
                _RootElements[14] = "UNB3Qualifier";
                _RootElements[15] = "UNB2Alt";
                _RootElements[16] = "UNB3Alt";
                _RootElements[17] = "UNB2AltQualifier";
                _RootElements[18] = "UNB3AltQualifier";
                _RootElements[19] = "BuyerPartyID";
                _RootElements[20] = "TypeDoc";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
    }
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [System.SerializableAttribute()]
    [PropertyType(@"MessageID",@"https://BYGE.Integration.Core.Properties.CoreProperties","string","System.String")]
    [PropertyGuidAttribute(@"297be0d1-c946-44f0-9a01-ac81768327c4")]
    public sealed class MessageID : Microsoft.XLANGs.BaseTypes.MessageDataPropertyBase {
        
        [System.NonSerializedAttribute()]
        private static System.Xml.XmlQualifiedName _QName = new System.Xml.XmlQualifiedName(@"MessageID", @"https://BYGE.Integration.Core.Properties.CoreProperties");
        
        private static string PropertyValueType {
            get {
                throw new System.NotSupportedException();
            }
        }
        
        public override System.Xml.XmlQualifiedName Name {
            get {
                return _QName;
            }
        }
        
        public override System.Type Type {
            get {
                return typeof(string);
            }
        }
    }
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [System.SerializableAttribute()]
    [PropertyType(@"ReceiverID",@"https://BYGE.Integration.Core.Properties.CoreProperties","string","System.String")]
    [PropertyGuidAttribute(@"4d8cd9cc-b16e-4dd3-9626-383f95ec3f34")]
    public sealed class ReceiverID : Microsoft.XLANGs.BaseTypes.MessageDataPropertyBase {
        
        [System.NonSerializedAttribute()]
        private static System.Xml.XmlQualifiedName _QName = new System.Xml.XmlQualifiedName(@"ReceiverID", @"https://BYGE.Integration.Core.Properties.CoreProperties");
        
        private static string PropertyValueType {
            get {
                throw new System.NotSupportedException();
            }
        }
        
        public override System.Xml.XmlQualifiedName Name {
            get {
                return _QName;
            }
        }
        
        public override System.Type Type {
            get {
                return typeof(string);
            }
        }
    }
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [System.SerializableAttribute()]
    [PropertyType(@"SenderID",@"https://BYGE.Integration.Core.Properties.CoreProperties","string","System.String")]
    [PropertyGuidAttribute(@"5a14865b-522c-4d6f-b52e-9e50c9f33676")]
    public sealed class SenderID : Microsoft.XLANGs.BaseTypes.MessageDataPropertyBase {
        
        [System.NonSerializedAttribute()]
        private static System.Xml.XmlQualifiedName _QName = new System.Xml.XmlQualifiedName(@"SenderID", @"https://BYGE.Integration.Core.Properties.CoreProperties");
        
        private static string PropertyValueType {
            get {
                throw new System.NotSupportedException();
            }
        }
        
        public override System.Xml.XmlQualifiedName Name {
            get {
                return _QName;
            }
        }
        
        public override System.Type Type {
            get {
                return typeof(string);
            }
        }
    }
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [System.SerializableAttribute()]
    [PropertyType(@"Amount",@"https://BYGE.Integration.Core.Properties.CoreProperties","integer","System.Decimal")]
    [PropertyGuidAttribute(@"af63ff7b-fa97-4f5d-81d1-c74cfb7f74bc")]
    public sealed class Amount : Microsoft.XLANGs.BaseTypes.MessageDataPropertyBase {
        
        [System.NonSerializedAttribute()]
        private static System.Xml.XmlQualifiedName _QName = new System.Xml.XmlQualifiedName(@"Amount", @"https://BYGE.Integration.Core.Properties.CoreProperties");
        
        private static decimal PropertyValueType {
            get {
                throw new System.NotSupportedException();
            }
        }
        
        public override System.Xml.XmlQualifiedName Name {
            get {
                return _QName;
            }
        }
        
        public override System.Type Type {
            get {
                return typeof(decimal);
            }
        }
    }
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [System.SerializableAttribute()]
    [PropertyType(@"AccountingBuyerID",@"https://BYGE.Integration.Core.Properties.CoreProperties","string","System.String")]
    [PropertyGuidAttribute(@"8bc3ab67-3372-4194-9ccc-038811b67f4e")]
    public sealed class AccountingBuyerID : Microsoft.XLANGs.BaseTypes.MessageDataPropertyBase {
        
        [System.NonSerializedAttribute()]
        private static System.Xml.XmlQualifiedName _QName = new System.Xml.XmlQualifiedName(@"AccountingBuyerID", @"https://BYGE.Integration.Core.Properties.CoreProperties");
        
        private static string PropertyValueType {
            get {
                throw new System.NotSupportedException();
            }
        }
        
        public override System.Xml.XmlQualifiedName Name {
            get {
                return _QName;
            }
        }
        
        public override System.Type Type {
            get {
                return typeof(string);
            }
        }
    }
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [System.SerializableAttribute()]
    [PropertyType(@"BuyerID",@"https://BYGE.Integration.Core.Properties.CoreProperties","string","System.String")]
    [PropertyGuidAttribute(@"e0cf90a5-3a62-4ee5-b5c7-cc4f36bad42d")]
    public sealed class BuyerID : Microsoft.XLANGs.BaseTypes.MessageDataPropertyBase {
        
        [System.NonSerializedAttribute()]
        private static System.Xml.XmlQualifiedName _QName = new System.Xml.XmlQualifiedName(@"BuyerID", @"https://BYGE.Integration.Core.Properties.CoreProperties");
        
        private static string PropertyValueType {
            get {
                throw new System.NotSupportedException();
            }
        }
        
        public override System.Xml.XmlQualifiedName Name {
            get {
                return _QName;
            }
        }
        
        public override System.Type Type {
            get {
                return typeof(string);
            }
        }
    }
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [System.SerializableAttribute()]
    [PropertyType(@"InvoiceType",@"https://BYGE.Integration.Core.Properties.CoreProperties","string","System.String")]
    [PropertyGuidAttribute(@"49287e3a-f3f1-40cd-8799-7d7ff4310e30")]
    public sealed class InvoiceType : Microsoft.XLANGs.BaseTypes.MessageDataPropertyBase {
        
        [System.NonSerializedAttribute()]
        private static System.Xml.XmlQualifiedName _QName = new System.Xml.XmlQualifiedName(@"InvoiceType", @"https://BYGE.Integration.Core.Properties.CoreProperties");
        
        private static string PropertyValueType {
            get {
                throw new System.NotSupportedException();
            }
        }
        
        public override System.Xml.XmlQualifiedName Name {
            get {
                return _QName;
            }
        }
        
        public override System.Type Type {
            get {
                return typeof(string);
            }
        }
    }
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [System.SerializableAttribute()]
    [PropertyType(@"DocumentType",@"https://BYGE.Integration.Core.Properties.CoreProperties","string","System.String")]
    [PropertyGuidAttribute(@"039c4e25-d2dd-43d1-bb72-360260d1f8b2")]
    public sealed class DocumentType : Microsoft.XLANGs.BaseTypes.MessageDataPropertyBase {
        
        [System.NonSerializedAttribute()]
        private static System.Xml.XmlQualifiedName _QName = new System.Xml.XmlQualifiedName(@"DocumentType", @"https://BYGE.Integration.Core.Properties.CoreProperties");
        
        private static string PropertyValueType {
            get {
                throw new System.NotSupportedException();
            }
        }
        
        public override System.Xml.XmlQualifiedName Name {
            get {
                return _QName;
            }
        }
        
        public override System.Type Type {
            get {
                return typeof(string);
            }
        }
    }
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [System.SerializableAttribute()]
    [PropertyType(@"IssueDate",@"https://BYGE.Integration.Core.Properties.CoreProperties","date","System.DateTime")]
    [PropertyGuidAttribute(@"70e9db5c-6332-49b1-9c94-579b9561bc17")]
    public sealed class IssueDate : Microsoft.XLANGs.BaseTypes.MessageDataPropertyBase {
        
        [System.NonSerializedAttribute()]
        private static System.Xml.XmlQualifiedName _QName = new System.Xml.XmlQualifiedName(@"IssueDate", @"https://BYGE.Integration.Core.Properties.CoreProperties");
        
        private static System.DateTime PropertyValueType {
            get {
                throw new System.NotSupportedException();
            }
        }
        
        public override System.Xml.XmlQualifiedName Name {
            get {
                return _QName;
            }
        }
        
        public override System.Type Type {
            get {
                return typeof(System.DateTime);
            }
        }
    }
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [System.SerializableAttribute()]
    [PropertyType(@"ReceiverName",@"https://BYGE.Integration.Core.Properties.CoreProperties","string","System.String")]
    [PropertyGuidAttribute(@"d131d092-e56f-405d-ac71-69028d5ae336")]
    public sealed class ReceiverName : Microsoft.XLANGs.BaseTypes.MessageDataPropertyBase {
        
        [System.NonSerializedAttribute()]
        private static System.Xml.XmlQualifiedName _QName = new System.Xml.XmlQualifiedName(@"ReceiverName", @"https://BYGE.Integration.Core.Properties.CoreProperties");
        
        private static string PropertyValueType {
            get {
                throw new System.NotSupportedException();
            }
        }
        
        public override System.Xml.XmlQualifiedName Name {
            get {
                return _QName;
            }
        }
        
        public override System.Type Type {
            get {
                return typeof(string);
            }
        }
    }
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [System.SerializableAttribute()]
    [PropertyType(@"SenderName",@"https://BYGE.Integration.Core.Properties.CoreProperties","string","System.String")]
    [PropertyGuidAttribute(@"1ba48928-255a-4a72-af62-55f406f8be36")]
    public sealed class SenderName : Microsoft.XLANGs.BaseTypes.MessageDataPropertyBase {
        
        [System.NonSerializedAttribute()]
        private static System.Xml.XmlQualifiedName _QName = new System.Xml.XmlQualifiedName(@"SenderName", @"https://BYGE.Integration.Core.Properties.CoreProperties");
        
        private static string PropertyValueType {
            get {
                throw new System.NotSupportedException();
            }
        }
        
        public override System.Xml.XmlQualifiedName Name {
            get {
                return _QName;
            }
        }
        
        public override System.Type Type {
            get {
                return typeof(string);
            }
        }
    }
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [System.SerializableAttribute()]
    [PropertyType(@"BuyerName",@"https://BYGE.Integration.Core.Properties.CoreProperties","string","System.String")]
    [PropertyGuidAttribute(@"1ac82233-9652-4b6c-bcab-bce38adc8904")]
    public sealed class BuyerName : Microsoft.XLANGs.BaseTypes.MessageDataPropertyBase {
        
        [System.NonSerializedAttribute()]
        private static System.Xml.XmlQualifiedName _QName = new System.Xml.XmlQualifiedName(@"BuyerName", @"https://BYGE.Integration.Core.Properties.CoreProperties");
        
        private static string PropertyValueType {
            get {
                throw new System.NotSupportedException();
            }
        }
        
        public override System.Xml.XmlQualifiedName Name {
            get {
                return _QName;
            }
        }
        
        public override System.Type Type {
            get {
                return typeof(string);
            }
        }
    }
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [System.SerializableAttribute()]
    [PropertyType(@"TestIndicator",@"https://BYGE.Integration.Core.Properties.CoreProperties","string","System.String")]
    [PropertyGuidAttribute(@"6e2a5e80-25dd-4ea6-ae2a-47ba9af4dfcd")]
    public sealed class TestIndicator : Microsoft.XLANGs.BaseTypes.MessageDataPropertyBase {
        
        [System.NonSerializedAttribute()]
        private static System.Xml.XmlQualifiedName _QName = new System.Xml.XmlQualifiedName(@"TestIndicator", @"https://BYGE.Integration.Core.Properties.CoreProperties");
        
        private static string PropertyValueType {
            get {
                throw new System.NotSupportedException();
            }
        }
        
        public override System.Xml.XmlQualifiedName Name {
            get {
                return _QName;
            }
        }
        
        public override System.Type Type {
            get {
                return typeof(string);
            }
        }
    }
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [System.SerializableAttribute()]
    [PropertyType(@"UNB2Qualifier",@"https://BYGE.Integration.Core.Properties.CoreProperties","string","System.String")]
    [PropertyGuidAttribute(@"5be8f137-a89c-4174-9e2b-73efa72ebcab")]
    public sealed class UNB2Qualifier : Microsoft.XLANGs.BaseTypes.MessageDataPropertyBase {
        
        [System.NonSerializedAttribute()]
        private static System.Xml.XmlQualifiedName _QName = new System.Xml.XmlQualifiedName(@"UNB2Qualifier", @"https://BYGE.Integration.Core.Properties.CoreProperties");
        
        private static string PropertyValueType {
            get {
                throw new System.NotSupportedException();
            }
        }
        
        public override System.Xml.XmlQualifiedName Name {
            get {
                return _QName;
            }
        }
        
        public override System.Type Type {
            get {
                return typeof(string);
            }
        }
    }
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [System.SerializableAttribute()]
    [PropertyType(@"UNB3Qualifier",@"https://BYGE.Integration.Core.Properties.CoreProperties","string","System.String")]
    [PropertyGuidAttribute(@"af6d7889-d292-4d62-967f-4cf6f8f8dc92")]
    public sealed class UNB3Qualifier : Microsoft.XLANGs.BaseTypes.MessageDataPropertyBase {
        
        [System.NonSerializedAttribute()]
        private static System.Xml.XmlQualifiedName _QName = new System.Xml.XmlQualifiedName(@"UNB3Qualifier", @"https://BYGE.Integration.Core.Properties.CoreProperties");
        
        private static string PropertyValueType {
            get {
                throw new System.NotSupportedException();
            }
        }
        
        public override System.Xml.XmlQualifiedName Name {
            get {
                return _QName;
            }
        }
        
        public override System.Type Type {
            get {
                return typeof(string);
            }
        }
    }
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [System.SerializableAttribute()]
    [PropertyType(@"UNB2Alt",@"https://BYGE.Integration.Core.Properties.CoreProperties","string","System.String")]
    [PropertyGuidAttribute(@"3444c756-4ecb-4360-aca4-a8d42e9a761a")]
    public sealed class UNB2Alt : Microsoft.XLANGs.BaseTypes.MessageDataPropertyBase {
        
        [System.NonSerializedAttribute()]
        private static System.Xml.XmlQualifiedName _QName = new System.Xml.XmlQualifiedName(@"UNB2Alt", @"https://BYGE.Integration.Core.Properties.CoreProperties");
        
        private static string PropertyValueType {
            get {
                throw new System.NotSupportedException();
            }
        }
        
        public override System.Xml.XmlQualifiedName Name {
            get {
                return _QName;
            }
        }
        
        public override System.Type Type {
            get {
                return typeof(string);
            }
        }
    }
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [System.SerializableAttribute()]
    [PropertyType(@"UNB3Alt",@"https://BYGE.Integration.Core.Properties.CoreProperties","string","System.String")]
    [PropertyGuidAttribute(@"00ecef91-d885-4447-be91-c3613c1485c1")]
    public sealed class UNB3Alt : Microsoft.XLANGs.BaseTypes.MessageDataPropertyBase {
        
        [System.NonSerializedAttribute()]
        private static System.Xml.XmlQualifiedName _QName = new System.Xml.XmlQualifiedName(@"UNB3Alt", @"https://BYGE.Integration.Core.Properties.CoreProperties");
        
        private static string PropertyValueType {
            get {
                throw new System.NotSupportedException();
            }
        }
        
        public override System.Xml.XmlQualifiedName Name {
            get {
                return _QName;
            }
        }
        
        public override System.Type Type {
            get {
                return typeof(string);
            }
        }
    }
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [System.SerializableAttribute()]
    [PropertyType(@"UNB2AltQualifier",@"https://BYGE.Integration.Core.Properties.CoreProperties","string","System.String")]
    [PropertyGuidAttribute(@"ef709422-4689-4c82-bef0-94cb3e996e13")]
    public sealed class UNB2AltQualifier : Microsoft.XLANGs.BaseTypes.MessageDataPropertyBase {
        
        [System.NonSerializedAttribute()]
        private static System.Xml.XmlQualifiedName _QName = new System.Xml.XmlQualifiedName(@"UNB2AltQualifier", @"https://BYGE.Integration.Core.Properties.CoreProperties");
        
        private static string PropertyValueType {
            get {
                throw new System.NotSupportedException();
            }
        }
        
        public override System.Xml.XmlQualifiedName Name {
            get {
                return _QName;
            }
        }
        
        public override System.Type Type {
            get {
                return typeof(string);
            }
        }
    }
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [System.SerializableAttribute()]
    [PropertyType(@"UNB3AltQualifier",@"https://BYGE.Integration.Core.Properties.CoreProperties","string","System.String")]
    [PropertyGuidAttribute(@"6093a3fc-9f27-425c-a5c9-17c246985d66")]
    public sealed class UNB3AltQualifier : Microsoft.XLANGs.BaseTypes.MessageDataPropertyBase {
        
        [System.NonSerializedAttribute()]
        private static System.Xml.XmlQualifiedName _QName = new System.Xml.XmlQualifiedName(@"UNB3AltQualifier", @"https://BYGE.Integration.Core.Properties.CoreProperties");
        
        private static string PropertyValueType {
            get {
                throw new System.NotSupportedException();
            }
        }
        
        public override System.Xml.XmlQualifiedName Name {
            get {
                return _QName;
            }
        }
        
        public override System.Type Type {
            get {
                return typeof(string);
            }
        }
    }
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [System.SerializableAttribute()]
    [PropertyType(@"BuyerPartyID",@"https://BYGE.Integration.Core.Properties.CoreProperties","string","System.String")]
    [PropertyGuidAttribute(@"ba94045a-959d-4a39-b7a6-8b4f2feef9f8")]
    public sealed class BuyerPartyID : Microsoft.XLANGs.BaseTypes.MessageDataPropertyBase {
        
        [System.NonSerializedAttribute()]
        private static System.Xml.XmlQualifiedName _QName = new System.Xml.XmlQualifiedName(@"BuyerPartyID", @"https://BYGE.Integration.Core.Properties.CoreProperties");
        
        private static string PropertyValueType {
            get {
                throw new System.NotSupportedException();
            }
        }
        
        public override System.Xml.XmlQualifiedName Name {
            get {
                return _QName;
            }
        }
        
        public override System.Type Type {
            get {
                return typeof(string);
            }
        }
    }
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [System.SerializableAttribute()]
    [PropertyType(@"TypeDoc",@"https://BYGE.Integration.Core.Properties.CoreProperties","string","System.String")]
    [PropertyGuidAttribute(@"9a7bc469-1a06-4714-b9c4-63e59800aa0a")]
    public sealed class TypeDoc : Microsoft.XLANGs.BaseTypes.MessageDataPropertyBase {
        
        [System.NonSerializedAttribute()]
        private static System.Xml.XmlQualifiedName _QName = new System.Xml.XmlQualifiedName(@"TypeDoc", @"https://BYGE.Integration.Core.Properties.CoreProperties");
        
        private static string PropertyValueType {
            get {
                throw new System.NotSupportedException();
            }
        }
        
        public override System.Xml.XmlQualifiedName Name {
            get {
                return _QName;
            }
        }
        
        public override System.Type Type {
            get {
                return typeof(string);
            }
        }
    }
}
