namespace BYGE.Integration.XLBYG.Invoice.Index.Maps {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.Core.Invoice.Invoice", typeof(global::BYGE.Integration.Core.Invoice.Invoice))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.XLBYG.Invoice.Index.Schemas.InvoiceIndex", typeof(global::BYGE.Integration.XLBYG.Invoice.Index.Schemas.InvoiceIndex))]
    public sealed class Core_Invoice_to_XLBYG_Invoice_Index_Copy : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var s0"" version=""1.0"" xmlns:s0=""http://byg-e.dk/schemas/v10"" xmlns:ns0=""http://BYGE.Integration.XLBYG.Invoice.Index.Schemas.InvoiceIndex"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/s0:Invoice"" />
  </xsl:template>
  <xsl:template match=""/s0:Invoice"">
    <ns0:XLBYGInvoiceIndex>
      <DocType>
        <xsl:value-of select=""InvoiceType/text()"" />
      </DocType>
      <xsl:if test=""BuyerCustomer/BuyerID"">
        <NADBY>
          <xsl:value-of select=""BuyerCustomer/BuyerID/text()"" />
        </NADBY>
      </xsl:if>
      <NADSU>
        <xsl:value-of select=""AccountingSupplier/AccSellerID/text()"" />
      </NADSU>
      <InvoicID>
        <xsl:value-of select=""InvoiceID/text()"" />
      </InvoicID>
      <Separator1>
        <xsl:text>+</xsl:text>
      </Separator1>
      <xsl:if test=""Total/LineTotalAmount"">
        <PayableAmount>
          <xsl:value-of select=""Total/LineTotalAmount/text()"" />
        </PayableAmount>
      </xsl:if>
      <Separator2>
        <xsl:text>+</xsl:text>
      </Separator2>
      <TaxAmount>
        <xsl:value-of select=""TaxTotal/TaxAmount/text()"" />
      </TaxAmount>
      <Separator3>
        <xsl:text>+</xsl:text>
      </Separator3>
      <UBLExtensions_Content_ExtensionAmount>
        <xsl:value-of select=""Total/PayableAmount/text()"" />
      </UBLExtensions_Content_ExtensionAmount>
      <IssueDate>
        <xsl:value-of select=""IssueDate/text()"" />
      </IssueDate>
      <PaymentDate>
        <xsl:value-of select=""PaymentMeans/PaymentDueDate/text()"" />
      </PaymentDate>
      <AgreementID>
        <xsl:text>0000</xsl:text>
      </AgreementID>
      <UnderAgreement>
        <xsl:text>0000</xsl:text>
      </UnderAgreement>
      <RequisitionsID>
        <xsl:value-of select=""OrderReference/OrderID/text()"" />
      </RequisitionsID>
      <CasheDiscount>
        <xsl:text> </xsl:text>
      </CasheDiscount>
      <Separator4>
        <xsl:text>+</xsl:text>
      </Separator4>
      <xsl:if test=""TaxTotal/TaxSubtotal/TaxableAmount"">
        <LegalMonetaryTotal_LineExtensionAmount>
          <xsl:value-of select=""TaxTotal/TaxSubtotal/TaxableAmount/text()"" />
        </LegalMonetaryTotal_LineExtensionAmount>
      </xsl:if>
      <ScanningsID>
        <xsl:text> </xsl:text>
      </ScanningsID>
      <xsl:if test=""AllowanceCharge/MultiplierFactorNumeric"">
        <TaxPercentage>
          <xsl:value-of select=""AllowanceCharge/MultiplierFactorNumeric/text()"" />
        </TaxPercentage>
      </xsl:if>
      <X>
        <xsl:text>X</xsl:text>
      </X>
      <xsl:if test=""UUID"">
        <GUID>
          <xsl:value-of select=""UUID/text()"" />
        </GUID>
      </xsl:if>
    </ns0:XLBYGInvoiceIndex>
  </xsl:template>
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"BYGE.Integration.Core.Invoice.Invoice";
        
        private const global::BYGE.Integration.Core.Invoice.Invoice _srcSchemaTypeReference0 = null;
        
        private const string _strTrgSchemasList0 = @"BYGE.Integration.XLBYG.Invoice.Index.Schemas.InvoiceIndex";
        
        private const global::BYGE.Integration.XLBYG.Invoice.Index.Schemas.InvoiceIndex _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [1];
                _SrcSchemas[0] = @"BYGE.Integration.Core.Invoice.Invoice";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"BYGE.Integration.XLBYG.Invoice.Index.Schemas.InvoiceIndex";
                return _TrgSchemas;
            }
        }
    }
}
