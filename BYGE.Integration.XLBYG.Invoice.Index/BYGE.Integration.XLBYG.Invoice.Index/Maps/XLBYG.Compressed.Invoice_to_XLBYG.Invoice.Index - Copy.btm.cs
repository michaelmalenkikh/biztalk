namespace BYGE.Integration.XLBYG.Invoice.Index.Maps {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.XLBYGCompressedInvoice", typeof(global::BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.XLBYGCompressedInvoice))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.XLBYG.Invoice.Index.Schemas.InvoiceIndex", typeof(global::BYGE.Integration.XLBYG.Invoice.Index.Schemas.InvoiceIndex))]
    public sealed class XLBYG_Compressed_Invoice_to_XLBYG_Invoice_Index_Copy : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var s0"" version=""1.0"" xmlns:s0=""http://BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.InvoiceTest01"" xmlns:ns0=""http://BYGE.Integration.XLBYG.Invoice.Index.Schemas.InvoiceIndex"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/s0:XLBYGCompressedInvoice"" />
  </xsl:template>
  <xsl:template match=""/s0:XLBYGCompressedInvoice"">
    <ns0:XLBYGInvoiceIndex>
      <DocType>
        <xsl:value-of select=""Header/DocType/text()"" />
      </DocType>
      <NADBY>
        <xsl:value-of select=""Footer/UNBRCV/text()"" />
      </NADBY>
      <NADSU>
        <xsl:value-of select=""Footer/UNBSND/text()"" />
      </NADSU>
      <InvoicID>
        <xsl:value-of select=""Footer/InvoicID/text()"" />
      </InvoicID>
      <Separator1>
        <xsl:text>+</xsl:text>
      </Separator1>
      <PayableAmount>
        <xsl:value-of select=""Footer/LegalMonetaryTotal_PayableAmount/text()"" />
      </PayableAmount>
      <Separator2>
        <xsl:text>+</xsl:text>
      </Separator2>
      <TaxAmount>
        <xsl:value-of select=""Footer/TaxTotal_TaxAmount/text()"" />
      </TaxAmount>
      <Separator3>
        <xsl:text>+</xsl:text>
      </Separator3>
      <UBLExtensions_Content_ExtensionAmount>
        <xsl:value-of select=""Footer/UBLExtensionsContent_ExtensionAmount/text()"" />
      </UBLExtensions_Content_ExtensionAmount>
      <IssueDate>
        <xsl:value-of select=""Header/IssueDate/text()"" />
      </IssueDate>
      <PaymentDate>
        <xsl:value-of select=""Header/PaymentDate/text()"" />
      </PaymentDate>
      <AgreementID>
        <xsl:text>0000</xsl:text>
      </AgreementID>
      <UnderAgreement>
        <xsl:text>0000</xsl:text>
      </UnderAgreement>
      <RequisitionsID>
        <xsl:text> </xsl:text>
      </RequisitionsID>
      <CasheDiscount>
        <xsl:text> </xsl:text>
      </CasheDiscount>
      <Separator4>
        <xsl:text>+</xsl:text>
      </Separator4>
      <LegalMonetaryTotal_LineExtensionAmount>
        <xsl:value-of select=""Footer/LegalMonetaryTotal_LineExtensionAmount/text()"" />
      </LegalMonetaryTotal_LineExtensionAmount>
      <ScanningsID>
        <xsl:text> </xsl:text>
      </ScanningsID>
      <TaxPercentage>
        <xsl:text> </xsl:text>
      </TaxPercentage>
      <X>
        <xsl:text>X</xsl:text>
      </X>
    </ns0:XLBYGInvoiceIndex>
  </xsl:template>
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.XLBYGCompressedInvoice";
        
        private const global::BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.XLBYGCompressedInvoice _srcSchemaTypeReference0 = null;
        
        private const string _strTrgSchemasList0 = @"BYGE.Integration.XLBYG.Invoice.Index.Schemas.InvoiceIndex";
        
        private const global::BYGE.Integration.XLBYG.Invoice.Index.Schemas.InvoiceIndex _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [1];
                _SrcSchemas[0] = @"BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.XLBYGCompressedInvoice";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"BYGE.Integration.XLBYG.Invoice.Index.Schemas.InvoiceIndex";
                return _TrgSchemas;
            }
        }
    }
}
