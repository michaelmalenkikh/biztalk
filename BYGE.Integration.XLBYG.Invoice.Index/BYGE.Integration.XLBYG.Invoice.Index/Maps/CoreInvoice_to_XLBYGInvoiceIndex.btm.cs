namespace BYGE.Integration.XLBYG.Invoice.Index {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.Core.Invoice.Invoice", typeof(global::BYGE.Integration.Core.Invoice.Invoice))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.XLBYG.Invoice.Index.Schemas.InvoiceIndex", typeof(global::BYGE.Integration.XLBYG.Invoice.Index.Schemas.InvoiceIndex))]
    public sealed class CoreInvoice_to_XLBYGInvoiceIndex : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var s0"" version=""1.0"" xmlns:s0=""http://byg-e.dk/schemas/v10"" xmlns:ns0=""http://BYGE.Integration.XLBYG.Invoice.Index.Schemas.InvoiceIndex"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/s0:Invoice"" />
  </xsl:template>
  <xsl:template match=""/s0:Invoice"">
    <ns0:XLBYGInvoiceIndex>
      <DocType>
        <xsl:text>D</xsl:text>
      </DocType>
      <Separator1>
        <xsl:text>+</xsl:text>
      </Separator1>
      <PayableAmount>
        <xsl:text>00000000000</xsl:text>
      </PayableAmount>
      <Separator2>
        <xsl:text>+</xsl:text>
      </Separator2>
      <TaxAmount>
        <xsl:text>00000000000</xsl:text>
      </TaxAmount>
      <Separator3>
        <xsl:text>+</xsl:text>
      </Separator3>
      <UBLExtensions_Content_ExtensionAmount>
        <xsl:text>00000000000</xsl:text>
      </UBLExtensions_Content_ExtensionAmount>
      <AgreementID>
        <xsl:text>0000</xsl:text>
      </AgreementID>
      <UnderAgreement>
        <xsl:text>0000</xsl:text>
      </UnderAgreement>
      <RequisitionsID>
        <xsl:text> </xsl:text>
      </RequisitionsID>
      <CasheDiscount>
        <xsl:text> </xsl:text>
      </CasheDiscount>
      <Separator4>
        <xsl:text>+</xsl:text>
      </Separator4>
      <LegalMonetaryTotal_LineExtensionAmount>
        <xsl:text>00000000000</xsl:text>
      </LegalMonetaryTotal_LineExtensionAmount>
      <ScanningsID>
        <xsl:text> </xsl:text>
      </ScanningsID>
      <TaxPercentage>
        <xsl:text> </xsl:text>
      </TaxPercentage>
      <X>
        <xsl:text>X</xsl:text>
      </X>
    </ns0:XLBYGInvoiceIndex>
  </xsl:template>
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"BYGE.Integration.Core.Invoice.Invoice";
        
        private const global::BYGE.Integration.Core.Invoice.Invoice _srcSchemaTypeReference0 = null;
        
        private const string _strTrgSchemasList0 = @"BYGE.Integration.XLBYG.Invoice.Index.Schemas.InvoiceIndex";
        
        private const global::BYGE.Integration.XLBYG.Invoice.Index.Schemas.InvoiceIndex _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [1];
                _SrcSchemas[0] = @"BYGE.Integration.Core.Invoice.Invoice";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"BYGE.Integration.XLBYG.Invoice.Index.Schemas.InvoiceIndex";
                return _TrgSchemas;
            }
        }
    }
}
