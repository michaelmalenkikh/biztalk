namespace BYGE.Integration.XLBYG.Invoice.Index.Schemas {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [Schema(@"http://BYGE.Integration.XLBYG.Invoice.Index.Schemas.InvoiceIndex",@"XLBYGInvoiceIndex")]
    [Microsoft.XLANGs.BaseTypes.PropertyAttribute(typeof(global::BYGE.Integration.XLBYG.Invoice.Index.PropertySchema.IssueDate), XPath = @"/*[local-name()='XLBYGInvoiceIndex' and namespace-uri()='http://BYGE.Integration.XLBYG.Invoice.Index.Schemas.InvoiceIndex']/*[local-name()='IssueDate' and namespace-uri()='']", XsdType = @"string")]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"XLBYGInvoiceIndex"})]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.XLBYG.Invoice.Index.PropertySchema.PropertySchema", typeof(global::BYGE.Integration.XLBYG.Invoice.Index.PropertySchema.PropertySchema))]
    public sealed class InvoiceIndex : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xs:schema xmlns=""http://BYGE.Integration.XLBYG.Invoice.Index.Schemas.InvoiceIndex"" xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" xmlns:ns0=""https://BYGE.Integration.XLBYG.Invoice.Index.PropertySchema"" targetNamespace=""http://BYGE.Integration.XLBYG.Invoice.Index.Schemas.InvoiceIndex"" xmlns:xs=""http://www.w3.org/2001/XMLSchema"">
  <xs:annotation>
    <xs:appinfo>
      <schemaEditorExtension:schemaInfo namespaceAlias=""b"" extensionClass=""Microsoft.BizTalk.FlatFileExtension.FlatFileExtension"" standardName=""Flat File"" xmlns:schemaEditorExtension=""http://schemas.microsoft.com/BizTalk/2003/SchemaEditorExtensions"" />
      <b:schemaInfo standard=""Flat File"" codepage=""65001"" default_pad_char="" "" pad_char_type=""char"" count_positions_by_byte=""false"" parser_optimization=""speed"" lookahead_depth=""3"" suppress_empty_nodes=""false"" generate_empty_nodes=""true"" allow_early_termination=""false"" early_terminate_optional_fields=""false"" allow_message_breakup_of_infix_root=""false"" compile_parse_tables=""false"" root_reference=""XLBYGInvoiceIndex"" />
      <b:imports>
        <b:namespace prefix=""ns0"" uri=""https://BYGE.Integration.XLBYG.Invoice.Index.PropertySchema"" location=""BYGE.Integration.XLBYG.Invoice.Index.PropertySchema.PropertySchema"" />
      </b:imports>
    </xs:appinfo>
  </xs:annotation>
  <xs:element name=""XLBYGInvoiceIndex"">
    <xs:annotation>
      <xs:appinfo>
        <b:recordInfo structure=""positional"" sequence_number=""1"" preserve_delimiter_for_empty_data=""true"" suppress_trailing_delimiters=""false"" />
        <b:properties>
          <b:property name=""ns0:IssueDate"" xpath=""/*[local-name()='XLBYGInvoiceIndex' and namespace-uri()='http://BYGE.Integration.XLBYG.Invoice.Index.Schemas.InvoiceIndex']/*[local-name()='IssueDate' and namespace-uri()='']"" />
        </b:properties>
      </xs:appinfo>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:annotation>
          <xs:appinfo>
            <groupInfo sequence_number=""0"" xmlns=""http://schemas.microsoft.com/BizTalk/2003"" />
          </xs:appinfo>
        </xs:annotation>
        <xs:element default=""D"" name=""DocType"" type=""xs:string"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo justification=""left"" pos_offset=""0"" pos_length=""1"" sequence_number=""1"" />
            </xs:appinfo>
          </xs:annotation>
        </xs:element>
        <xs:element name=""NADBY"" type=""xs:string"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo justification=""left"" pos_offset=""0"" pos_length=""13"" sequence_number=""2"" />
            </xs:appinfo>
          </xs:annotation>
        </xs:element>
        <xs:element name=""NADSU"" type=""xs:string"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo justification=""left"" pos_offset=""0"" pos_length=""13"" sequence_number=""3"" />
            </xs:appinfo>
          </xs:annotation>
        </xs:element>
        <xs:element name=""InvoicID"" type=""xs:string"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo justification=""right"" pos_offset=""0"" pos_length=""17"" sequence_number=""4"" />
            </xs:appinfo>
          </xs:annotation>
        </xs:element>
        <xs:element default=""+"" name=""Separator1"" type=""xs:string"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo justification=""left"" pos_offset=""0"" pos_length=""1"" sequence_number=""5"" />
            </xs:appinfo>
          </xs:annotation>
        </xs:element>
        <xs:element default=""00000000000"" name=""PayableAmount"" type=""xs:string"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo justification=""left"" pos_offset=""0"" pos_length=""11"" sequence_number=""6"" />
            </xs:appinfo>
          </xs:annotation>
        </xs:element>
        <xs:element default=""+"" name=""Separator2"" type=""xs:string"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo justification=""left"" pos_offset=""0"" pos_length=""1"" sequence_number=""7"" />
            </xs:appinfo>
          </xs:annotation>
        </xs:element>
        <xs:element default=""00000000000"" name=""TaxAmount"" type=""xs:string"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo justification=""left"" pos_offset=""0"" pos_length=""11"" sequence_number=""8"" />
            </xs:appinfo>
          </xs:annotation>
        </xs:element>
        <xs:element default=""+"" name=""Separator3"" type=""xs:string"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo justification=""left"" pos_offset=""0"" pos_length=""1"" sequence_number=""9"" />
            </xs:appinfo>
          </xs:annotation>
        </xs:element>
        <xs:element default=""00000000000"" name=""UBLExtensions_Content_ExtensionAmount"" type=""xs:string"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo justification=""left"" pos_offset=""0"" pos_length=""11"" sequence_number=""10"" />
            </xs:appinfo>
          </xs:annotation>
        </xs:element>
        <xs:element name=""IssueDate"" type=""xs:string"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo justification=""left"" pos_offset=""0"" pos_length=""6"" sequence_number=""11"" />
            </xs:appinfo>
          </xs:annotation>
        </xs:element>
        <xs:element name=""PaymentDate"" type=""xs:string"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo justification=""left"" pos_offset=""0"" pos_length=""6"" sequence_number=""12"" />
            </xs:appinfo>
          </xs:annotation>
        </xs:element>
        <xs:element default=""0000"" name=""AgreementID"" type=""xs:string"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo justification=""left"" pos_offset=""0"" pos_length=""4"" sequence_number=""13"" />
            </xs:appinfo>
          </xs:annotation>
        </xs:element>
        <xs:element default=""0000"" name=""UnderAgreement"" type=""xs:string"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo justification=""left"" pos_offset=""0"" pos_length=""4"" sequence_number=""14"" />
            </xs:appinfo>
          </xs:annotation>
        </xs:element>
        <xs:element default="" "" name=""RequisitionsID"" type=""xs:string"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo justification=""left"" pos_offset=""0"" pos_length=""30"" sequence_number=""15"" />
            </xs:appinfo>
          </xs:annotation>
        </xs:element>
        <xs:element default="" "" name=""CasheDiscount"" type=""xs:string"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo justification=""left"" pos_offset=""0"" pos_length=""12"" sequence_number=""16"" />
            </xs:appinfo>
          </xs:annotation>
        </xs:element>
        <xs:element default=""+"" name=""Separator4"" type=""xs:string"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo justification=""left"" pos_offset=""0"" pos_length=""1"" sequence_number=""17"" />
            </xs:appinfo>
          </xs:annotation>
        </xs:element>
        <xs:element default=""00000000000"" name=""LegalMonetaryTotal_LineExtensionAmount"" type=""xs:string"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo justification=""left"" pos_offset=""0"" pos_length=""11"" sequence_number=""18"" />
            </xs:appinfo>
          </xs:annotation>
        </xs:element>
        <xs:element default="" "" name=""ScanningsID"" type=""xs:string"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo justification=""left"" pos_offset=""0"" pos_length=""17"" sequence_number=""19"" />
            </xs:appinfo>
          </xs:annotation>
        </xs:element>
        <xs:element default="" "" name=""TaxPercentage"" type=""xs:string"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo justification=""left"" pos_offset=""0"" pos_length=""5"" sequence_number=""20"" />
            </xs:appinfo>
          </xs:annotation>
        </xs:element>
        <xs:element default=""X"" name=""X"" type=""xs:string"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo justification=""left"" pos_offset=""0"" pos_length=""1"" sequence_number=""21"" />
            </xs:appinfo>
          </xs:annotation>
        </xs:element>
        <xs:element name=""GUID"" type=""xs:string"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo justification=""left"" pos_offset=""0"" pos_length=""36"" sequence_number=""22"" />
            </xs:appinfo>
          </xs:annotation>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
</xs:schema>";
        
        public InvoiceIndex() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [1];
                _RootElements[0] = "XLBYGInvoiceIndex";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
    }
}
