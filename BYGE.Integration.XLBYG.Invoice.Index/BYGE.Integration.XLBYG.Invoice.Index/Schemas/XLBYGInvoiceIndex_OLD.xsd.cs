namespace BYGE.Integration.XLBYG.Invoice.Index {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [Schema(@"http://BYGE.Integration.XLBYG.Compressed.Invoice.FlatFileSchema1",@"XLBYGInvoiceIndex")]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"XLBYGInvoiceIndex"})]
    public sealed class XLBYGCompressedInvoice : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xs:schema xmlns=""http://BYGE.Integration.XLBYG.Compressed.Invoice.FlatFileSchema1"" xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" targetNamespace=""http://BYGE.Integration.XLBYG.Compressed.Invoice.FlatFileSchema1"" xmlns:xs=""http://www.w3.org/2001/XMLSchema"">
  <xs:annotation>
    <xs:appinfo>
      <b:schemaInfo standard=""Flat File"" root_reference=""XLBYGInvoiceIndex"" default_pad_char="" "" pad_char_type=""char"" count_positions_by_byte=""false"" parser_optimization=""speed"" lookahead_depth=""3"" suppress_empty_nodes=""false"" generate_empty_nodes=""true"" allow_early_termination=""false"" early_terminate_optional_fields=""false"" allow_message_breakup_of_infix_root=""false"" compile_parse_tables=""false"" />
      <schemaEditorExtension:schemaInfo namespaceAlias=""b"" extensionClass=""Microsoft.BizTalk.FlatFileExtension.FlatFileExtension"" standardName=""Flat File"" xmlns:schemaEditorExtension=""http://schemas.microsoft.com/BizTalk/2003/SchemaEditorExtensions"" />
    </xs:appinfo>
  </xs:annotation>
  <xs:element name=""XLBYGInvoiceIndex"">
    <xs:annotation>
      <xs:appinfo>
        <b:recordInfo structure=""positional"" preserve_delimiter_for_empty_data=""true"" suppress_trailing_delimiters=""false"" sequence_number=""1"" />
      </xs:appinfo>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:annotation>
          <xs:appinfo>
            <b:groupInfo sequence_number=""0"" />
          </xs:appinfo>
        </xs:annotation>
        <xs:element minOccurs=""0"" default=""D"" name=""Doctype"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo justification=""left"" pos_length=""1"" sequence_number=""1"" />
            </xs:appinfo>
          </xs:annotation>
          <xs:simpleType>
            <xs:restriction base=""xs:string"" />
          </xs:simpleType>
        </xs:element>
        <xs:element minOccurs=""1"" maxOccurs=""1"" name=""NADBY"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo justification=""left"" pos_length=""13"" sequence_number=""2"" />
            </xs:appinfo>
          </xs:annotation>
          <xs:simpleType>
            <xs:restriction base=""xs:string"" />
          </xs:simpleType>
        </xs:element>
        <xs:element minOccurs=""1"" maxOccurs=""1"" name=""NADSU"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo justification=""left"" pos_length=""13"" sequence_number=""3"" />
            </xs:appinfo>
          </xs:annotation>
          <xs:simpleType>
            <xs:restriction base=""xs:string"" />
          </xs:simpleType>
        </xs:element>
        <xs:element minOccurs=""1"" maxOccurs=""1"" name=""InvoicID"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo justification=""left"" pos_length=""17"" sequence_number=""4"" />
            </xs:appinfo>
          </xs:annotation>
          <xs:simpleType>
            <xs:restriction base=""xs:string"">
              <xs:length value=""13"" />
            </xs:restriction>
          </xs:simpleType>
        </xs:element>
        <xs:element minOccurs=""1"" maxOccurs=""1"" name=""PayableAmount"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo sequence_number=""5"" justification=""left"" pos_length=""12"" />
            </xs:appinfo>
          </xs:annotation>
          <xs:simpleType>
            <xs:restriction base=""xs:string"" />
          </xs:simpleType>
        </xs:element>
        <xs:element minOccurs=""1"" maxOccurs=""1"" name=""TaxAmount"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo sequence_number=""6"" justification=""left"" pos_length=""12"" />
            </xs:appinfo>
          </xs:annotation>
          <xs:simpleType>
            <xs:restriction base=""xs:string"" />
          </xs:simpleType>
        </xs:element>
        <xs:element minOccurs=""0"" name=""UBLExtensions_Content_ExtensionAmount"" nillable=""true"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo sequence_number=""7"" justification=""left"" pos_length=""12"" />
            </xs:appinfo>
          </xs:annotation>
          <xs:simpleType>
            <xs:restriction base=""xs:string"" />
          </xs:simpleType>
        </xs:element>
        <xs:element minOccurs=""0"" name=""IssueDate"" nillable=""true"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo sequence_number=""8"" justification=""left"" pos_length=""6"" />
            </xs:appinfo>
          </xs:annotation>
          <xs:simpleType>
            <xs:restriction base=""xs:date"" />
          </xs:simpleType>
        </xs:element>
        <xs:element minOccurs=""0"" name=""PaymentDate"" nillable=""true"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo sequence_number=""9"" justification=""left"" pos_length=""6"" />
            </xs:appinfo>
          </xs:annotation>
          <xs:simpleType>
            <xs:restriction base=""xs:date"" />
          </xs:simpleType>
        </xs:element>
        <xs:element minOccurs=""0"" name=""AgreementID"" nillable=""true"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo sequence_number=""10"" justification=""left"" pos_length=""4"" />
            </xs:appinfo>
          </xs:annotation>
          <xs:simpleType>
            <xs:restriction base=""xs:string"" />
          </xs:simpleType>
        </xs:element>
        <xs:element minOccurs=""0"" name=""AgreementCode"" nillable=""true"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo sequence_number=""11"" justification=""left"" pos_length=""4"" />
            </xs:appinfo>
          </xs:annotation>
          <xs:simpleType>
            <xs:restriction base=""xs:string"" />
          </xs:simpleType>
        </xs:element>
        <xs:element minOccurs=""0"" name=""Text_RequisitionID"" nillable=""true"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo sequence_number=""12"" justification=""left"" pos_length=""30"" />
            </xs:appinfo>
          </xs:annotation>
          <xs:simpleType>
            <xs:restriction base=""xs:string"" />
          </xs:simpleType>
        </xs:element>
        <xs:element minOccurs=""0"" name=""CashDiscount_NotUsed"" nillable=""true"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo sequence_number=""13"" justification=""left"" pos_length=""12"" />
            </xs:appinfo>
          </xs:annotation>
          <xs:simpleType>
            <xs:restriction base=""xs:string"" />
          </xs:simpleType>
        </xs:element>
        <xs:element minOccurs=""0"" name=""LegalMonetaryTotal_LineExtensionAmount"" nillable=""true"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo sequence_number=""14"" justification=""left"" pos_length=""12"" />
            </xs:appinfo>
          </xs:annotation>
          <xs:simpleType>
            <xs:restriction base=""xs:string"" />
          </xs:simpleType>
        </xs:element>
        <xs:element minOccurs=""0"" name=""ScanningsID"" nillable=""true"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo sequence_number=""15"" justification=""left"" pos_length=""17"" />
            </xs:appinfo>
          </xs:annotation>
          <xs:simpleType>
            <xs:restriction base=""xs:string"" />
          </xs:simpleType>
        </xs:element>
        <xs:element minOccurs=""1"" maxOccurs=""1"" default=""02500"" name=""ReceivedTaxPercentage"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo sequence_number=""16"" justification=""left"" pos_length=""5"" />
            </xs:appinfo>
          </xs:annotation>
          <xs:simpleType>
            <xs:restriction base=""xs:string"" />
          </xs:simpleType>
        </xs:element>
        <xs:element minOccurs=""1"" maxOccurs=""1"" default=""X"" name=""Filter"">
          <xs:annotation>
            <xs:appinfo>
              <b:fieldInfo sequence_number=""17"" justification=""left"" pos_length=""1"" />
            </xs:appinfo>
          </xs:annotation>
          <xs:simpleType>
            <xs:restriction base=""xs:string"" />
          </xs:simpleType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
</xs:schema>";
        
        public XLBYGCompressedInvoice() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [1];
                _RootElements[0] = "XLBYGInvoiceIndex";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
    }
}
