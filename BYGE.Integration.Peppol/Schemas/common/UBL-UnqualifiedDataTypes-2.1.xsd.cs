namespace BYGE.Integration.PeppolInvoice {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [System.SerializableAttribute()]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.PeppolInvoice.CCTS_CCT_SchemaModule_2_1", typeof(global::BYGE.Integration.PeppolInvoice.CCTS_CCT_SchemaModule_2_1))]
    public sealed class UBL_UnqualifiedDataTypes_2_1 : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xsd:schema xmlns:ccts-cct=""urn:un:unece:uncefact:data:specification:CoreComponentTypeSchemaModule:2"" xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" xmlns:ccts=""urn:un:unece:uncefact:documentation:2"" attributeFormDefault=""unqualified"" elementFormDefault=""qualified"" targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:UnqualifiedDataTypes-2"" version=""2.1"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
  <xsd:import schemaLocation=""BYGE.Integration.PeppolInvoice.CCTS_CCT_SchemaModule_2_1"" namespace=""urn:un:unece:uncefact:data:specification:CoreComponentTypeSchemaModule:2"" />
  <xsd:annotation>
    <xsd:appinfo>
      <references xmlns=""http://schemas.microsoft.com/BizTalk/2003"">
        <reference targetNamespace=""urn:un:unece:uncefact:data:specification:CoreComponentTypeSchemaModule:2"" />
      </references>
    </xsd:appinfo>
  </xsd:annotation>
  <xsd:complexType name=""AmountType"">
    <xsd:annotation>
      <xsd:documentation xml:lang=""en"">
        <ccts:UniqueID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UBLUDT000001</ccts:UniqueID>
        <ccts:CategoryCode xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UDT</ccts:CategoryCode>
        <ccts:DictionaryEntryName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Amount. Type</ccts:DictionaryEntryName>
        <ccts:VersionID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">1.0</ccts:VersionID>
        <ccts:Definition xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">A number of monetary units specified using a given unit of currency.</ccts:Definition>
        <ccts:RepresentationTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Amount</ccts:RepresentationTermName>
      </xsd:documentation>
    </xsd:annotation>
    <xsd:simpleContent>
      <xsd:restriction base=""ccts-cct:AmountType"">
        <xsd:attribute name=""currencyID"" type=""xsd:normalizedString"" use=""required"">
          <xsd:annotation>
            <xsd:documentation xml:lang=""en"">
              <ccts:UniqueID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UNDT000001-SC2</ccts:UniqueID>
              <ccts:CategoryCode xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">SC</ccts:CategoryCode>
              <ccts:DictionaryEntryName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Amount. Currency. Identifier</ccts:DictionaryEntryName>
              <ccts:Definition xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">The currency of the amount.</ccts:Definition>
              <ccts:ObjectClass xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Amount Currency</ccts:ObjectClass>
              <ccts:PropertyTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Identification</ccts:PropertyTermName>
              <ccts:RepresentationTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Identifier</ccts:RepresentationTermName>
              <ccts:PrimitiveType xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">string</ccts:PrimitiveType>
              <ccts:UsageRule xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Reference UNECE Rec 9, using 3-letter alphabetic codes.</ccts:UsageRule>
            </xsd:documentation>
          </xsd:annotation>
        </xsd:attribute>
      </xsd:restriction>
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:complexType name=""BinaryObjectType"">
    <xsd:annotation>
      <xsd:documentation xml:lang=""en"">
        <ccts:UniqueID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UBLUDT000002</ccts:UniqueID>
        <ccts:CategoryCode xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UDT</ccts:CategoryCode>
        <ccts:DictionaryEntryName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Binary Object. Type</ccts:DictionaryEntryName>
        <ccts:VersionID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">1.0</ccts:VersionID>
        <ccts:Definition xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">A set of finite-length sequences of binary octets.</ccts:Definition>
        <ccts:RepresentationTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Binary Object</ccts:RepresentationTermName>
        <ccts:PrimitiveType xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">binary</ccts:PrimitiveType>
      </xsd:documentation>
    </xsd:annotation>
    <xsd:simpleContent>
      <xsd:restriction base=""ccts-cct:BinaryObjectType"">
        <xsd:attribute name=""mimeCode"" type=""xsd:normalizedString"" use=""required"">
          <xsd:annotation>
            <xsd:documentation xml:lang=""en"">
              <ccts:UniqueID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UNDT000002-SC3</ccts:UniqueID>
              <ccts:CategoryCode xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">SC</ccts:CategoryCode>
              <ccts:DictionaryEntryName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Binary Object. Mime. Code</ccts:DictionaryEntryName>
              <ccts:Definition xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">The mime type of the binary object.</ccts:Definition>
              <ccts:ObjectClass xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Binary Object</ccts:ObjectClass>
              <ccts:PropertyTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Mime</ccts:PropertyTermName>
              <ccts:RepresentationTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Code</ccts:RepresentationTermName>
              <ccts:PrimitiveType xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">string</ccts:PrimitiveType>
            </xsd:documentation>
          </xsd:annotation>
        </xsd:attribute>
      </xsd:restriction>
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:complexType name=""GraphicType"">
    <xsd:annotation>
      <xsd:documentation xml:lang=""en"">
        <ccts:UniqueID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UBLUDT000003</ccts:UniqueID>
        <ccts:CategoryCode xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UDT</ccts:CategoryCode>
        <ccts:DictionaryEntryName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Graphic. Type</ccts:DictionaryEntryName>
        <ccts:VersionID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">1.0</ccts:VersionID>
        <ccts:Definition xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">A diagram, graph, mathematical curve, or similar representation.</ccts:Definition>
        <ccts:RepresentationTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Graphic</ccts:RepresentationTermName>
        <ccts:PrimitiveType xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">binary</ccts:PrimitiveType>
      </xsd:documentation>
    </xsd:annotation>
    <xsd:simpleContent>
      <xsd:restriction base=""ccts-cct:BinaryObjectType"">
        <xsd:attribute name=""mimeCode"" type=""xsd:normalizedString"" use=""required"">
          <xsd:annotation>
            <xsd:documentation xml:lang=""en"">
              <ccts:UniqueID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UNDT000003-SC3</ccts:UniqueID>
              <ccts:CategoryCode xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">SC</ccts:CategoryCode>
              <ccts:DictionaryEntryName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Graphic. Mime. Code</ccts:DictionaryEntryName>
              <ccts:Definition xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">The mime type of the graphic object.</ccts:Definition>
              <ccts:ObjectClass xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Graphic</ccts:ObjectClass>
              <ccts:PropertyTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Mime</ccts:PropertyTermName>
              <ccts:RepresentationTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Code</ccts:RepresentationTermName>
              <ccts:PrimitiveType xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">normalizedString</ccts:PrimitiveType>
            </xsd:documentation>
          </xsd:annotation>
        </xsd:attribute>
      </xsd:restriction>
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:complexType name=""PictureType"">
    <xsd:annotation>
      <xsd:documentation xml:lang=""en"">
        <ccts:UniqueID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UBLUDT000004</ccts:UniqueID>
        <ccts:CategoryCode xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UDT</ccts:CategoryCode>
        <ccts:DictionaryEntryName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Picture. Type</ccts:DictionaryEntryName>
        <ccts:VersionID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">1.0</ccts:VersionID>
        <ccts:Definition xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">A diagram, graph, mathematical curve, or similar representation.</ccts:Definition>
        <ccts:RepresentationTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Picture</ccts:RepresentationTermName>
        <ccts:PrimitiveType xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">binary</ccts:PrimitiveType>
      </xsd:documentation>
    </xsd:annotation>
    <xsd:simpleContent>
      <xsd:restriction base=""ccts-cct:BinaryObjectType"">
        <xsd:attribute name=""mimeCode"" type=""xsd:normalizedString"" use=""required"">
          <xsd:annotation>
            <xsd:documentation xml:lang=""en"">
              <ccts:UniqueID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UNDT000004-SC3</ccts:UniqueID>
              <ccts:CategoryCode xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">SC</ccts:CategoryCode>
              <ccts:DictionaryEntryName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Picture. Mime. Code</ccts:DictionaryEntryName>
              <ccts:Definition xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">The mime type of the picture object.</ccts:Definition>
              <ccts:ObjectClass xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Picture</ccts:ObjectClass>
              <ccts:PropertyTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Mime</ccts:PropertyTermName>
              <ccts:RepresentationTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Code</ccts:RepresentationTermName>
              <ccts:PrimitiveType xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">normalizedString</ccts:PrimitiveType>
            </xsd:documentation>
          </xsd:annotation>
        </xsd:attribute>
      </xsd:restriction>
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:complexType name=""SoundType"">
    <xsd:annotation>
      <xsd:documentation xml:lang=""en"">
        <ccts:UniqueID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UBLUDT000005</ccts:UniqueID>
        <ccts:CategoryCode xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UDT</ccts:CategoryCode>
        <ccts:DictionaryEntryName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Sound. Type</ccts:DictionaryEntryName>
        <ccts:VersionID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">1.0</ccts:VersionID>
        <ccts:Definition xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">An audio representation.</ccts:Definition>
        <ccts:RepresentationTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Sound</ccts:RepresentationTermName>
        <ccts:PrimitiveType xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">binary</ccts:PrimitiveType>
      </xsd:documentation>
    </xsd:annotation>
    <xsd:simpleContent>
      <xsd:restriction base=""ccts-cct:BinaryObjectType"">
        <xsd:attribute name=""mimeCode"" type=""xsd:normalizedString"" use=""required"">
          <xsd:annotation>
            <xsd:documentation xml:lang=""en"">
              <ccts:UniqueID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UNDT000005-SC3</ccts:UniqueID>
              <ccts:CategoryCode xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">SC</ccts:CategoryCode>
              <ccts:DictionaryEntryName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Sound. Mime. Code</ccts:DictionaryEntryName>
              <ccts:Definition xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">The mime type of the sound object.</ccts:Definition>
              <ccts:ObjectClass xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Sound</ccts:ObjectClass>
              <ccts:PropertyTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Mime</ccts:PropertyTermName>
              <ccts:RepresentationTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Code</ccts:RepresentationTermName>
              <ccts:PrimitiveType xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">normalizedString</ccts:PrimitiveType>
            </xsd:documentation>
          </xsd:annotation>
        </xsd:attribute>
      </xsd:restriction>
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:complexType name=""VideoType"">
    <xsd:annotation>
      <xsd:documentation xml:lang=""en"">
        <ccts:UniqueID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UBLUDT000006</ccts:UniqueID>
        <ccts:CategoryCode xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UDT</ccts:CategoryCode>
        <ccts:DictionaryEntryName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Video. Type</ccts:DictionaryEntryName>
        <ccts:VersionID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">1.0</ccts:VersionID>
        <ccts:Definition xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">A video representation.</ccts:Definition>
        <ccts:RepresentationTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Video</ccts:RepresentationTermName>
        <ccts:PrimitiveType xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">binary</ccts:PrimitiveType>
      </xsd:documentation>
    </xsd:annotation>
    <xsd:simpleContent>
      <xsd:restriction base=""ccts-cct:BinaryObjectType"">
        <xsd:attribute name=""mimeCode"" type=""xsd:normalizedString"" use=""required"">
          <xsd:annotation>
            <xsd:documentation xml:lang=""en"">
              <ccts:UniqueID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UNDT000006-SC3</ccts:UniqueID>
              <ccts:CategoryCode xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">SC</ccts:CategoryCode>
              <ccts:DictionaryEntryName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Video. Mime. Code</ccts:DictionaryEntryName>
              <ccts:Definition xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">The mime type of the video object.</ccts:Definition>
              <ccts:ObjectClass xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Video</ccts:ObjectClass>
              <ccts:PropertyTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Mime</ccts:PropertyTermName>
              <ccts:RepresentationTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Code</ccts:RepresentationTermName>
              <ccts:PrimitiveType xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">normalizedString</ccts:PrimitiveType>
            </xsd:documentation>
          </xsd:annotation>
        </xsd:attribute>
      </xsd:restriction>
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:complexType name=""CodeType"">
    <xsd:annotation>
      <xsd:documentation xml:lang=""en"">
        <ccts:UniqueID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UBLUDT000007</ccts:UniqueID>
        <ccts:CategoryCode xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UDT</ccts:CategoryCode>
        <ccts:DictionaryEntryName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Code. Type</ccts:DictionaryEntryName>
        <ccts:VersionID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">1.0</ccts:VersionID>
        <ccts:Definition xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">A character string (letters, figures, or symbols) that for brevity and/or language independence may be used to represent or replace a definitive value or text of an attribute, together with relevant supplementary information.</ccts:Definition>
        <ccts:RepresentationTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Code</ccts:RepresentationTermName>
        <ccts:PrimitiveType xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">string</ccts:PrimitiveType>
        <ccts:UsageRule xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Other supplementary components in the CCT are captured as part of the token and name for the schema module containing the code list and thus, are not declared as attributes. </ccts:UsageRule>
      </xsd:documentation>
    </xsd:annotation>
    <xsd:simpleContent>
      <xsd:extension base=""ccts-cct:CodeType"" />
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:complexType name=""DateTimeType"">
    <xsd:annotation>
      <xsd:documentation xml:lang=""en"">
        <ccts:UniqueID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UBLUDT000008</ccts:UniqueID>
        <ccts:CategoryCode xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UDT</ccts:CategoryCode>
        <ccts:DictionaryEntryName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Date Time. Type</ccts:DictionaryEntryName>
        <ccts:VersionID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">1.0</ccts:VersionID>
        <ccts:Definition xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">A particular point in the progression of time, together with relevant supplementary information.</ccts:Definition>
        <ccts:RepresentationTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Date Time</ccts:RepresentationTermName>
        <ccts:PrimitiveType xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">string</ccts:PrimitiveType>
        <ccts:UsageRule xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Can be used for a date and/or time.</ccts:UsageRule>
      </xsd:documentation>
    </xsd:annotation>
    <xsd:simpleContent>
      <xsd:extension base=""xsd:dateTime"" />
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:complexType name=""DateType"">
    <xsd:annotation>
      <xsd:documentation xml:lang=""en"">
        <ccts:UniqueID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UBLUDT000009</ccts:UniqueID>
        <ccts:CategoryCode xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UDT</ccts:CategoryCode>
        <ccts:DictionaryEntryName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Date. Type</ccts:DictionaryEntryName>
        <ccts:VersionID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">1.0</ccts:VersionID>
        <ccts:Definition xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">One calendar day according the Gregorian calendar.</ccts:Definition>
        <ccts:RepresentationTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Date</ccts:RepresentationTermName>
        <ccts:PrimitiveType xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">string</ccts:PrimitiveType>
      </xsd:documentation>
    </xsd:annotation>
    <xsd:simpleContent>
      <xsd:extension base=""xsd:date"" />
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:complexType name=""TimeType"">
    <xsd:annotation>
      <xsd:documentation xml:lang=""en"">
        <ccts:UniqueID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UBLUDT0000010</ccts:UniqueID>
        <ccts:CategoryCode xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UDT</ccts:CategoryCode>
        <ccts:DictionaryEntryName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Time. Type</ccts:DictionaryEntryName>
        <ccts:VersionID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">1.0</ccts:VersionID>
        <ccts:Definition xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">An instance of time that occurs every day.</ccts:Definition>
        <ccts:RepresentationTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Time</ccts:RepresentationTermName>
        <ccts:PrimitiveType xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">string</ccts:PrimitiveType>
      </xsd:documentation>
    </xsd:annotation>
    <xsd:simpleContent>
      <xsd:extension base=""xsd:time"" />
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:complexType name=""IdentifierType"">
    <xsd:annotation>
      <xsd:documentation xml:lang=""en"">
        <ccts:UniqueID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UBLUDT0000011</ccts:UniqueID>
        <ccts:CategoryCode xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UDT</ccts:CategoryCode>
        <ccts:DictionaryEntryName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Identifier. Type</ccts:DictionaryEntryName>
        <ccts:VersionID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">1.0</ccts:VersionID>
        <ccts:Definition xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">A character string to identify and uniquely distinguish one instance of an object in an identification scheme from all other objects in the same scheme, together with relevant supplementary information.</ccts:Definition>
        <ccts:RepresentationTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Identifier</ccts:RepresentationTermName>
        <ccts:PrimitiveType xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">string</ccts:PrimitiveType>
        <ccts:UsageRule xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Other supplementary components in the CCT are captured as part of the token and name for the schema module containing the identifier list and thus, are not declared as attributes. </ccts:UsageRule>
      </xsd:documentation>
    </xsd:annotation>
    <xsd:simpleContent>
      <xsd:extension base=""ccts-cct:IdentifierType"" />
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:complexType name=""IndicatorType"">
    <xsd:annotation>
      <xsd:documentation xml:lang=""en"">
        <ccts:UniqueID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UBLUDT0000012</ccts:UniqueID>
        <ccts:CategoryCode xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UDT</ccts:CategoryCode>
        <ccts:DictionaryEntryName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Indicator. Type</ccts:DictionaryEntryName>
        <ccts:VersionID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">1.0</ccts:VersionID>
        <ccts:Definition xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">A list of two mutually exclusive Boolean values that express the only possible states of a property.</ccts:Definition>
        <ccts:RepresentationTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Indicator</ccts:RepresentationTermName>
        <ccts:PrimitiveType xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">string</ccts:PrimitiveType>
      </xsd:documentation>
    </xsd:annotation>
    <xsd:simpleContent>
      <xsd:extension base=""xsd:boolean"" />
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:complexType name=""MeasureType"">
    <xsd:annotation>
      <xsd:documentation xml:lang=""en"">
        <ccts:UniqueID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UBLUDT0000013</ccts:UniqueID>
        <ccts:CategoryCode xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UDT</ccts:CategoryCode>
        <ccts:DictionaryEntryName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Measure. Type</ccts:DictionaryEntryName>
        <ccts:VersionID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">1.0</ccts:VersionID>
        <ccts:Definition xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">A numeric value determined by measuring an object using a specified unit of measure.</ccts:Definition>
        <ccts:RepresentationTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Measure</ccts:RepresentationTermName>
        <ccts:PropertyTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Type</ccts:PropertyTermName>
        <ccts:PrimitiveType xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">decimal</ccts:PrimitiveType>
      </xsd:documentation>
    </xsd:annotation>
    <xsd:simpleContent>
      <xsd:restriction base=""ccts-cct:MeasureType"">
        <xsd:attribute name=""unitCode"" type=""xsd:normalizedString"" use=""required"">
          <xsd:annotation>
            <xsd:documentation xml:lang=""en"">
              <ccts:UniqueID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UNDT000013-SC2</ccts:UniqueID>
              <ccts:CategoryCode xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">SC</ccts:CategoryCode>
              <ccts:DictionaryEntryName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Measure. Unit. Code</ccts:DictionaryEntryName>
              <ccts:Definition xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">The type of unit of measure.</ccts:Definition>
              <ccts:ObjectClass xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Measure Unit</ccts:ObjectClass>
              <ccts:PropertyTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Code</ccts:PropertyTermName>
              <ccts:RepresentationTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Code</ccts:RepresentationTermName>
              <ccts:PrimitiveType xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">normalizedString</ccts:PrimitiveType>
              <ccts:UsageRule xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Reference UNECE Rec. 20 and X12 355</ccts:UsageRule>
            </xsd:documentation>
          </xsd:annotation>
        </xsd:attribute>
      </xsd:restriction>
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:complexType name=""NumericType"">
    <xsd:annotation>
      <xsd:documentation xml:lang=""en"">
        <ccts:UniqueID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UBLUDT0000014</ccts:UniqueID>
        <ccts:CategoryCode xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UDT</ccts:CategoryCode>
        <ccts:DictionaryEntryName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Numeric. Type</ccts:DictionaryEntryName>
        <ccts:VersionID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">1.0</ccts:VersionID>
        <ccts:Definition xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Numeric information that is assigned or is determined by calculation, counting, or sequencing. It does not require a unit of quantity or unit of measure.</ccts:Definition>
        <ccts:RepresentationTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Numeric</ccts:RepresentationTermName>
        <ccts:PrimitiveType xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">string</ccts:PrimitiveType>
      </xsd:documentation>
    </xsd:annotation>
    <xsd:simpleContent>
      <xsd:extension base=""ccts-cct:NumericType"" />
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:complexType name=""ValueType"">
    <xsd:annotation>
      <xsd:documentation xml:lang=""en"">
        <ccts:UniqueID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UBLUDT0000015</ccts:UniqueID>
        <ccts:CategoryCode xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UDT</ccts:CategoryCode>
        <ccts:VersionID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">1.0</ccts:VersionID>
        <ccts:DictionaryEntryName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Value. Type</ccts:DictionaryEntryName>
        <ccts:Definition xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Numeric information that is assigned or is determined by calculation, counting, or sequencing. It does not require a unit of quantity or unit of measure.</ccts:Definition>
        <ccts:RepresentationTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Value</ccts:RepresentationTermName>
        <ccts:PrimitiveType xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">string</ccts:PrimitiveType>
      </xsd:documentation>
    </xsd:annotation>
    <xsd:simpleContent>
      <xsd:extension base=""ccts-cct:NumericType"" />
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:complexType name=""PercentType"">
    <xsd:annotation>
      <xsd:documentation xml:lang=""en"">
        <ccts:UniqueID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UBLUDT0000016</ccts:UniqueID>
        <ccts:CategoryCode xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UDT</ccts:CategoryCode>
        <ccts:VersionID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">1.0</ccts:VersionID>
        <ccts:DictionaryEntryName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Percent. Type</ccts:DictionaryEntryName>
        <ccts:Definition xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Numeric information that is assigned or is determined by calculation, counting, or sequencing and is expressed as a percentage. It does not require a unit of quantity or unit of measure.</ccts:Definition>
        <ccts:RepresentationTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Percent</ccts:RepresentationTermName>
        <ccts:PrimitiveType xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">string</ccts:PrimitiveType>
      </xsd:documentation>
    </xsd:annotation>
    <xsd:simpleContent>
      <xsd:extension base=""ccts-cct:NumericType"" />
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:complexType name=""RateType"">
    <xsd:annotation>
      <xsd:documentation xml:lang=""en"">
        <ccts:UniqueID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UBLUDT0000017</ccts:UniqueID>
        <ccts:CategoryCode xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UDT</ccts:CategoryCode>
        <ccts:VersionID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">1.0</ccts:VersionID>
        <ccts:DictionaryEntryName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Rate. Type</ccts:DictionaryEntryName>
        <ccts:Definition xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">A numeric expression of a rate that is assigned or is determined by calculation, counting, or sequencing. It does not require a unit of quantity or unit of measure.</ccts:Definition>
        <ccts:RepresentationTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Rate</ccts:RepresentationTermName>
        <ccts:PrimitiveType xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">string</ccts:PrimitiveType>
      </xsd:documentation>
    </xsd:annotation>
    <xsd:simpleContent>
      <xsd:extension base=""ccts-cct:NumericType"" />
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:complexType name=""QuantityType"">
    <xsd:annotation>
      <xsd:documentation xml:lang=""en"">
        <ccts:UniqueID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UBLUDT0000018</ccts:UniqueID>
        <ccts:CategoryCode xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UDT</ccts:CategoryCode>
        <ccts:DictionaryEntryName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Quantity. Type</ccts:DictionaryEntryName>
        <ccts:VersionID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">1.0</ccts:VersionID>
        <ccts:Definition xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">A counted number of non-monetary units, possibly including a fractional part.</ccts:Definition>
        <ccts:RepresentationTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Quantity</ccts:RepresentationTermName>
        <ccts:PrimitiveType xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">decimal</ccts:PrimitiveType>
      </xsd:documentation>
    </xsd:annotation>
    <xsd:simpleContent>
      <xsd:extension base=""ccts-cct:QuantityType"" />
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:complexType name=""TextType"">
    <xsd:annotation>
      <xsd:documentation xml:lang=""en"">
        <ccts:UniqueID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UBLUDT0000019</ccts:UniqueID>
        <ccts:CategoryCode xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UDT</ccts:CategoryCode>
        <ccts:DictionaryEntryName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Text. Type</ccts:DictionaryEntryName>
        <ccts:VersionID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">1.0</ccts:VersionID>
        <ccts:Definition xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">A character string (i.e. a finite set of characters), generally in the form of words of a language.</ccts:Definition>
        <ccts:RepresentationTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Text</ccts:RepresentationTermName>
        <ccts:PrimitiveType xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">string</ccts:PrimitiveType>
      </xsd:documentation>
    </xsd:annotation>
    <xsd:simpleContent>
      <xsd:extension base=""ccts-cct:TextType"" />
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:complexType name=""NameType"">
    <xsd:annotation>
      <xsd:documentation xml:lang=""en"">
        <ccts:UniqueID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UBLUDT0000020</ccts:UniqueID>
        <ccts:CategoryCode xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">UDT</ccts:CategoryCode>
        <ccts:DictionaryEntryName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Name. Type</ccts:DictionaryEntryName>
        <ccts:VersionID xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">1.0</ccts:VersionID>
        <ccts:Definition xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">A character string that constitutes the distinctive designation of a person, place, thing or concept.</ccts:Definition>
        <ccts:RepresentationTermName xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">Name</ccts:RepresentationTermName>
        <ccts:PrimitiveType xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">string</ccts:PrimitiveType>
      </xsd:documentation>
    </xsd:annotation>
    <xsd:simpleContent>
      <xsd:extension base=""ccts-cct:TextType"" />
    </xsd:simpleContent>
  </xsd:complexType>
</xsd:schema>";
        
        public UBL_UnqualifiedDataTypes_2_1() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [0];
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
    }
}
