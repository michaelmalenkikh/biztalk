namespace BYGE.Integration.PeppolInvoice {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [System.SerializableAttribute()]
    public sealed class UBL_ExtensionContentDataType_2_1 : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xsd:schema xmlns=""urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2"" xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" attributeFormDefault=""unqualified"" elementFormDefault=""qualified"" targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2"" version=""2.1"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
  <xsd:complexType name=""ExtensionContentType"">
    <xsd:sequence>
      <xsd:any minOccurs=""1"" maxOccurs=""1"" namespace=""##other"" processContents=""lax"">
        <xsd:annotation>
          <xsd:documentation>
            Any element in any namespace other than the UBL extension
            namespace is allowed to be the apex element of an extension.
            Only those elements found in the UBL schemas and in the
            trees of schemas imported in this module are validated.
            Any element for which there is no schema declaration in any
            of the trees of schemas passes validation and is not
            treated as a schema constraint violation.
          </xsd:documentation>
        </xsd:annotation>
      </xsd:any>
    </xsd:sequence>
  </xsd:complexType>
</xsd:schema>";
        
        public UBL_ExtensionContentDataType_2_1() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [0];
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
    }
}
