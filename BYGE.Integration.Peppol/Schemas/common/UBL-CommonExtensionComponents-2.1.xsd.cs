namespace BYGE.Integration.PeppolInvoice {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"UBLExtensions", @"UBLExtension", @"ExtensionAgencyID", @"ExtensionAgencyName", @"ExtensionAgencyURI", @"ExtensionContent", @"ExtensionReason", @"ExtensionReasonCode", @"ExtensionURI", @"ExtensionVersionID"})]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.PeppolInvoice.UBL_UnqualifiedDataTypes_2_1", typeof(global::BYGE.Integration.PeppolInvoice.UBL_UnqualifiedDataTypes_2_1))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.PeppolInvoice.UBL_CommonBasicComponents_2_1", typeof(global::BYGE.Integration.PeppolInvoice.UBL_CommonBasicComponents_2_1))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.PeppolInvoice.UBL_ExtensionContentDataType_2_1", typeof(global::BYGE.Integration.PeppolInvoice.UBL_ExtensionContentDataType_2_1))]
    public sealed class UBL_CommonExtensionComponents_2_1 : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xsd:schema xmlns=""urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2"" xmlns:cbc=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" xmlns:udt=""urn:oasis:names:specification:ubl:schema:xsd:UnqualifiedDataTypes-2"" attributeFormDefault=""unqualified"" elementFormDefault=""qualified"" targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2"" version=""2.1"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
  <xsd:import schemaLocation=""BYGE.Integration.PeppolInvoice.UBL_UnqualifiedDataTypes_2_1"" namespace=""urn:oasis:names:specification:ubl:schema:xsd:UnqualifiedDataTypes-2"" />
  <xsd:import schemaLocation=""BYGE.Integration.PeppolInvoice.UBL_CommonBasicComponents_2_1"" namespace=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" />
  <xsd:include schemaLocation=""BYGE.Integration.PeppolInvoice.UBL_ExtensionContentDataType_2_1"" />
  <xsd:annotation>
    <xsd:appinfo>
      <references xmlns=""http://schemas.microsoft.com/BizTalk/2003"">
        <reference targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" />
        <reference targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:QualifiedDataTypes-2"" />
        <reference targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:UnqualifiedDataTypes-2"" />
        <reference targetNamespace=""urn:un:unece:uncefact:data:specification:CoreComponentTypeSchemaModule:2"" />
      </references>
    </xsd:appinfo>
  </xsd:annotation>
  <xsd:element name=""UBLExtensions"" type=""UBLExtensionsType"">
    <xsd:annotation>
      <xsd:documentation>
        A container for all extensions present in the document.
      </xsd:documentation>
    </xsd:annotation>
  </xsd:element>
  <xsd:complexType name=""UBLExtensionsType"">
    <xsd:annotation>
      <xsd:documentation>
        A container for all extensions present in the document.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs=""1"" maxOccurs=""unbounded"" ref=""UBLExtension"">
        <xsd:annotation>
          <xsd:documentation>
            A single extension for private use.
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:element name=""UBLExtension"" type=""UBLExtensionType"">
    <xsd:annotation>
      <xsd:documentation>
        A single extension for private use.
      </xsd:documentation>
    </xsd:annotation>
  </xsd:element>
  <xsd:complexType name=""UBLExtensionType"">
    <xsd:annotation>
      <xsd:documentation>
        A single extension for private use.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""cbc:ID"">
        <xsd:annotation>
          <xsd:documentation>
            An identifier for the Extension assigned by the creator of the extension.
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""cbc:Name"">
        <xsd:annotation>
          <xsd:documentation>
            A name for the Extension assigned by the creator of the extension.
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""ExtensionAgencyID"">
        <xsd:annotation>
          <xsd:documentation>
            An agency that maintains one or more Extensions.
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""ExtensionAgencyName"">
        <xsd:annotation>
          <xsd:documentation>
            The name of the agency that maintains the Extension.
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""ExtensionVersionID"">
        <xsd:annotation>
          <xsd:documentation>
            The version of the Extension.
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""ExtensionAgencyURI"">
        <xsd:annotation>
          <xsd:documentation>
            A URI for the Agency that maintains the Extension.
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""ExtensionURI"">
        <xsd:annotation>
          <xsd:documentation>
            A URI for the Extension.
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""ExtensionReasonCode"">
        <xsd:annotation>
          <xsd:documentation>
            A code for reason the Extension is being included.
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""ExtensionReason"">
        <xsd:annotation>
          <xsd:documentation>
            A description of the reason for the Extension.
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""1"" maxOccurs=""1"" ref=""ExtensionContent"">
        <xsd:annotation>
          <xsd:documentation>
            The definition of the extension content.
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:element name=""ExtensionAgencyID"" type=""ExtensionAgencyIDType"" />
  <xsd:element name=""ExtensionAgencyName"" type=""ExtensionAgencyNameType"" />
  <xsd:element name=""ExtensionAgencyURI"" type=""ExtensionAgencyURIType"" />
  <xsd:element name=""ExtensionContent"" type=""ExtensionContentType"" />
  <xsd:element name=""ExtensionReason"" type=""ExtensionReasonType"" />
  <xsd:element name=""ExtensionReasonCode"" type=""ExtensionReasonCodeType"" />
  <xsd:element name=""ExtensionURI"" type=""ExtensionURIType"" />
  <xsd:element name=""ExtensionVersionID"" type=""ExtensionVersionIDType"" />
  <xsd:complexType name=""ExtensionAgencyIDType"">
    <xsd:simpleContent>
      <xsd:extension base=""udt:IdentifierType"" />
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:complexType name=""ExtensionAgencyNameType"">
    <xsd:simpleContent>
      <xsd:extension base=""udt:TextType"" />
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:complexType name=""ExtensionAgencyURIType"">
    <xsd:simpleContent>
      <xsd:extension base=""udt:IdentifierType"" />
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:complexType name=""ExtensionReasonType"">
    <xsd:simpleContent>
      <xsd:extension base=""udt:TextType"" />
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:complexType name=""ExtensionReasonCodeType"">
    <xsd:simpleContent>
      <xsd:extension base=""udt:CodeType"" />
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:complexType name=""ExtensionURIType"">
    <xsd:simpleContent>
      <xsd:extension base=""udt:IdentifierType"" />
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:complexType name=""ExtensionVersionIDType"">
    <xsd:simpleContent>
      <xsd:extension base=""udt:IdentifierType"" />
    </xsd:simpleContent>
  </xsd:complexType>
</xsd:schema>";
        
        public UBL_CommonExtensionComponents_2_1() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [10];
                _RootElements[0] = "UBLExtensions";
                _RootElements[1] = "UBLExtension";
                _RootElements[2] = "ExtensionAgencyID";
                _RootElements[3] = "ExtensionAgencyName";
                _RootElements[4] = "ExtensionAgencyURI";
                _RootElements[5] = "ExtensionContent";
                _RootElements[6] = "ExtensionReason";
                _RootElements[7] = "ExtensionReasonCode";
                _RootElements[8] = "ExtensionURI";
                _RootElements[9] = "ExtensionVersionID";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2",@"UBLExtensions")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"UBLExtensions"})]
        public sealed class UBLExtensions : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public UBLExtensions() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "UBLExtensions";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2",@"UBLExtension")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"UBLExtension"})]
        public sealed class UBLExtension : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public UBLExtension() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "UBLExtension";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2",@"ExtensionAgencyID")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"ExtensionAgencyID"})]
        public sealed class ExtensionAgencyID : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public ExtensionAgencyID() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "ExtensionAgencyID";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2",@"ExtensionAgencyName")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"ExtensionAgencyName"})]
        public sealed class ExtensionAgencyName : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public ExtensionAgencyName() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "ExtensionAgencyName";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2",@"ExtensionAgencyURI")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"ExtensionAgencyURI"})]
        public sealed class ExtensionAgencyURI : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public ExtensionAgencyURI() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "ExtensionAgencyURI";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2",@"ExtensionContent")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"ExtensionContent"})]
        public sealed class ExtensionContent : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public ExtensionContent() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "ExtensionContent";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2",@"ExtensionReason")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"ExtensionReason"})]
        public sealed class ExtensionReason : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public ExtensionReason() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "ExtensionReason";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2",@"ExtensionReasonCode")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"ExtensionReasonCode"})]
        public sealed class ExtensionReasonCode : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public ExtensionReasonCode() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "ExtensionReasonCode";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2",@"ExtensionURI")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"ExtensionURI"})]
        public sealed class ExtensionURI : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public ExtensionURI() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "ExtensionURI";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2",@"ExtensionVersionID")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"ExtensionVersionID"})]
        public sealed class ExtensionVersionID : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public ExtensionVersionID() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "ExtensionVersionID";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
    }
}
