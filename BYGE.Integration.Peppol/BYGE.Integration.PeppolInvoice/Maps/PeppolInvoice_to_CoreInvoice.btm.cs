namespace BYGE.Integration.PeppolInvoice.Maps {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.PeppolInvoice.UBL_Invoice_2_1", typeof(global::BYGE.Integration.PeppolInvoice.UBL_Invoice_2_1))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.Core.Invoice.Invoice", typeof(global::BYGE.Integration.Core.Invoice.Invoice))]
    public sealed class PeppolInvoice_to_CoreInvoice : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var s0 s6 s4 s5 s3 s1 s2 userCSharp"" version=""1.0"" xmlns:s0=""urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2"" xmlns:s6=""urn:oasis:names:specification:ubl:schema:xsd:UnqualifiedDataTypes-2"" xmlns:ns0=""http://byg-e.dk/schemas/v10"" xmlns:s4=""urn:un:unece:uncefact:data:specification:CoreComponentTypeSchemaModule:2"" xmlns:s5=""urn:oasis:names:specification:ubl:schema:xsd:QualifiedDataTypes-2"" xmlns:s3=""urn:oasis:names:specification:ubl:schema:xsd:Invoice-2"" xmlns:s1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" xmlns:s2=""urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"" xmlns:userCSharp=""http://schemas.microsoft.com/BizTalk/2003/userCSharp"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/s3:Invoice"" />
  </xsl:template>
  <xsl:template match=""/s3:Invoice"">
    <ns0:Invoice>
      <xsl:if test=""s1:CustomizationID"">
        <CustomizationID>
          <xsl:value-of select=""s1:CustomizationID/text()"" />
        </CustomizationID>
      </xsl:if>
      <xsl:if test=""s1:ProfileID"">
        <ProfileID>
          <xsl:value-of select=""s1:ProfileID/text()"" />
        </ProfileID>
      </xsl:if>
      <InvoiceID>
        <xsl:value-of select=""s1:ID/text()"" />
      </InvoiceID>
      <TestIndicator>
        <xsl:text>0</xsl:text>
      </TestIndicator>
      <IssueDate>
        <xsl:value-of select=""s1:IssueDate/text()"" />
      </IssueDate>
      <xsl:if test=""s1:DueDate"">
        <DueDate>
          <xsl:value-of select=""s1:DueDate/text()"" />
        </DueDate>
      </xsl:if>
      <xsl:if test=""s1:InvoiceTypeCode"">
        <InvoiceType>
          <xsl:value-of select=""s1:InvoiceTypeCode/text()"" />
        </InvoiceType>
      </xsl:if>
      <xsl:if test=""s1:TaxPointDate"">
        <TaxPointDate>
          <xsl:value-of select=""s1:TaxPointDate/text()"" />
        </TaxPointDate>
      </xsl:if>
      <xsl:if test=""s1:DocumentCurrencyCode"">
        <Currency>
          <xsl:value-of select=""s1:DocumentCurrencyCode/text()"" />
        </Currency>
      </xsl:if>
      <xsl:if test=""s1:TaxCurrencyCode"">
        <TaxCurrencyCode>
          <xsl:value-of select=""s1:TaxCurrencyCode/text()"" />
        </TaxCurrencyCode>
      </xsl:if>
      <xsl:if test=""s1:AccountingCost"">
        <AccountingCost>
          <xsl:value-of select=""s1:AccountingCost/text()"" />
        </AccountingCost>
      </xsl:if>
      <xsl:if test=""s1:BuyerReference"">
        <BuyerReference>
          <xsl:value-of select=""s1:BuyerReference/text()"" />
        </BuyerReference>
      </xsl:if>
      <xsl:for-each select=""s2:InvoicePeriod"">
        <xsl:for-each select=""s1:DescriptionCode"">
          <InvoicePeriod>
            <xsl:if test=""../s1:StartDate"">
              <StartDate>
                <xsl:value-of select=""../s1:StartDate/text()"" />
              </StartDate>
            </xsl:if>
            <xsl:if test=""../s1:StartTime"">
              <StartTime>
                <xsl:value-of select=""../s1:StartTime/text()"" />
              </StartTime>
            </xsl:if>
            <xsl:if test=""../s1:EndDate"">
              <EndDate>
                <xsl:value-of select=""../s1:EndDate/text()"" />
              </EndDate>
            </xsl:if>
            <xsl:if test=""../s1:EndTime"">
              <EndTime>
                <xsl:value-of select=""../s1:EndTime/text()"" />
              </EndTime>
            </xsl:if>
            <xsl:if test=""../s1:DurationMeasure"">
              <DurationMeasure>
                <xsl:value-of select=""../s1:DurationMeasure/text()"" />
              </DurationMeasure>
            </xsl:if>
            <DescriptionCode>
              <xsl:value-of select=""./text()"" />
            </DescriptionCode>
            <xsl:if test=""../s1:Description"">
              <Description>
                <xsl:value-of select=""../s1:Description/text()"" />
              </Description>
            </xsl:if>
          </InvoicePeriod>
        </xsl:for-each>
      </xsl:for-each>
      <xsl:for-each select=""s2:OrderReference"">
        <OrderReference>
          <OrderID>
            <xsl:value-of select=""s1:ID/text()"" />
          </OrderID>
          <xsl:if test=""s1:SalesOrderID"">
            <SalesOrderID>
              <xsl:value-of select=""s1:SalesOrderID/text()"" />
            </SalesOrderID>
          </xsl:if>
        </OrderReference>
      </xsl:for-each>
      <BillingReference>
        <xsl:for-each select=""s2:BillingReference"">
          <xsl:for-each select=""s2:InvoiceDocumentReference"">
            <InvoiceDocumentReference>
              <ID>
                <xsl:value-of select=""s1:ID/text()"" />
              </ID>
              <xsl:if test=""s1:IssueDate"">
                <IssueDate>
                  <xsl:value-of select=""s1:IssueDate/text()"" />
                </IssueDate>
              </xsl:if>
            </InvoiceDocumentReference>
          </xsl:for-each>
        </xsl:for-each>
      </BillingReference>
      <xsl:for-each select=""s2:DespatchDocumentReference"">
        <DespatchDocumentReference>
          <ID>
            <xsl:value-of select=""s1:ID/text()"" />
          </ID>
        </DespatchDocumentReference>
      </xsl:for-each>
      <xsl:for-each select=""s2:ReceiptDocumentReference"">
        <ReceiptDocumentReference>
          <ID>
            <xsl:value-of select=""s1:ID/text()"" />
          </ID>
        </ReceiptDocumentReference>
      </xsl:for-each>
      <xsl:for-each select=""s2:OriginatorDocumentReference"">
        <OriginatorDocumentReference>
          <ID>
            <xsl:value-of select=""s1:ID/text()"" />
          </ID>
        </OriginatorDocumentReference>
      </xsl:for-each>
      <xsl:for-each select=""s2:ContractDocumentReference"">
        <ContractDocumentReference>
          <ID>
            <xsl:value-of select=""s1:ID/text()"" />
          </ID>
        </ContractDocumentReference>
      </xsl:for-each>
      <xsl:for-each select=""s2:AdditionalDocumentReference"">
        <AdditionalDocumentReference>
          <ID>
            <xsl:if test=""s1:ID/@schemeID"">
              <xsl:attribute name=""schemeID"">
                <xsl:value-of select=""s1:ID/@schemeID"" />
              </xsl:attribute>
            </xsl:if>
            <xsl:value-of select=""s1:ID/text()"" />
          </ID>
          <xsl:if test=""s1:DocumentTypeCode"">
            <DocumentTypeCode>
              <xsl:value-of select=""s1:DocumentTypeCode/text()"" />
            </DocumentTypeCode>
          </xsl:if>
          <xsl:if test=""s1:DocumentDescription"">
            <DocumentDescription>
              <xsl:value-of select=""s1:DocumentDescription/text()"" />
            </DocumentDescription>
          </xsl:if>
          <Attachment>
            <EmbeddedDocumentBinaryObject>
              <xsl:attribute name=""mimeCode"">
                <xsl:value-of select=""s2:Attachment/s1:EmbeddedDocumentBinaryObject/@mimeCode"" />
              </xsl:attribute>
              <xsl:if test=""s2:Attachment/s1:EmbeddedDocumentBinaryObject/@filename"">
                <xsl:attribute name=""filename"">
                  <xsl:value-of select=""s2:Attachment/s1:EmbeddedDocumentBinaryObject/@filename"" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test=""s2:Attachment/s1:EmbeddedDocumentBinaryObject"">
                <xsl:value-of select=""s2:Attachment/s1:EmbeddedDocumentBinaryObject/text()"" />
              </xsl:if>
            </EmbeddedDocumentBinaryObject>
            <ExternalReference>
              <xsl:if test=""s2:Attachment/s2:ExternalReference/s1:URI"">
                <URI>
                  <xsl:value-of select=""s2:Attachment/s2:ExternalReference/s1:URI/text()"" />
                </URI>
              </xsl:if>
            </ExternalReference>
          </Attachment>
        </AdditionalDocumentReference>
      </xsl:for-each>
      <xsl:for-each select=""s2:ProjectReference"">
        <ProjectReference>
          <ID>
            <xsl:value-of select=""s1:ID/text()"" />
          </ID>
        </ProjectReference>
      </xsl:for-each>
      <xsl:for-each select=""s2:AccountingSupplierParty/s2:Party"">
        <xsl:for-each select=""s1:EndpointID"">
          <AccountingSupplier>
            <AccSellerID>
              <xsl:value-of select=""./text()"" />
            </AccSellerID>
            <xsl:for-each select=""../s2:PartyIdentification"">
              <xsl:for-each select=""s1:ID"">
                <SchemeID>
                  <xsl:if test=""../../s1:EndpointID/@schemeID"">
                    <Endpoint>
                      <xsl:value-of select=""../../s1:EndpointID/@schemeID"" />
                    </Endpoint>
                  </xsl:if>
                  <xsl:variable name=""var:v1"" select=""userCSharp:ReturnNotEmpty(string(@schemeID))"" />
                  <Party>
                    <xsl:value-of select=""$var:v1"" />
                  </Party>
                </SchemeID>
              </xsl:for-each>
            </xsl:for-each>
            <xsl:for-each select=""../s2:PartyName"">
              <Address>
                <Name>
                  <xsl:value-of select=""s1:Name/text()"" />
                </Name>
                <xsl:if test=""../s2:PostalAddress/s1:Postbox"">
                  <Postbox>
                    <xsl:value-of select=""../s2:PostalAddress/s1:Postbox/text()"" />
                  </Postbox>
                </xsl:if>
                <xsl:if test=""../s2:PostalAddress/s1:AddressFormatCode"">
                  <AddressFormatCode>
                    <xsl:value-of select=""../s2:PostalAddress/s1:AddressFormatCode/text()"" />
                  </AddressFormatCode>
                </xsl:if>
                <xsl:if test=""../s2:PostalAddress/s1:StreetName"">
                  <Street>
                    <xsl:value-of select=""../s2:PostalAddress/s1:StreetName/text()"" />
                  </Street>
                </xsl:if>
                <xsl:if test=""../s2:PostalAddress/s1:BuildingNumber"">
                  <Number>
                    <xsl:value-of select=""../s2:PostalAddress/s1:BuildingNumber/text()"" />
                  </Number>
                </xsl:if>
                <xsl:if test=""../s2:PostalAddress/s1:AdditionalStreetName"">
                  <AdditionalStreetName>
                    <xsl:value-of select=""../s2:PostalAddress/s1:AdditionalStreetName/text()"" />
                  </AdditionalStreetName>
                </xsl:if>
                <xsl:if test=""../s2:PostalAddress/s1:Department"">
                  <Department>
                    <xsl:value-of select=""../s2:PostalAddress/s1:Department/text()"" />
                  </Department>
                </xsl:if>
                <xsl:if test=""../s2:PostalAddress/s1:CityName"">
                  <City>
                    <xsl:value-of select=""../s2:PostalAddress/s1:CityName/text()"" />
                  </City>
                </xsl:if>
                <xsl:if test=""../s2:PostalAddress/s1:PostalZone"">
                  <PostalCode>
                    <xsl:value-of select=""../s2:PostalAddress/s1:PostalZone/text()"" />
                  </PostalCode>
                </xsl:if>
                <xsl:if test=""../s2:PostalAddress/s1:CountrySubentity"">
                  <CountrySubentity>
                    <xsl:value-of select=""../s2:PostalAddress/s1:CountrySubentity/text()"" />
                  </CountrySubentity>
                </xsl:if>
                <xsl:for-each select=""../s2:PostalAddress"">
                  <xsl:for-each select=""s2:AddressLine"">
                    <AddressLine>
                      <Line>
                        <xsl:value-of select=""s1:Line/text()"" />
                      </Line>
                    </AddressLine>
                  </xsl:for-each>
                </xsl:for-each>
                <xsl:if test=""../s2:PostalAddress/s2:Country/s1:IdentificationCode"">
                  <Country>
                    <xsl:value-of select=""../s2:PostalAddress/s2:Country/s1:IdentificationCode/text()"" />
                  </Country>
                </xsl:if>
              </Address>
            </xsl:for-each>
            <xsl:for-each select=""../s2:Contact"">
              <xsl:for-each select=""s1:Note"">
                <Concact>
                  <xsl:if test=""../s1:ID"">
                    <ID>
                      <xsl:value-of select=""../s1:ID/text()"" />
                    </ID>
                  </xsl:if>
                  <xsl:if test=""../s1:Name"">
                    <Name>
                      <xsl:value-of select=""../s1:Name/text()"" />
                    </Name>
                  </xsl:if>
                  <xsl:if test=""../s1:Telephone"">
                    <Telephone>
                      <xsl:value-of select=""../s1:Telephone/text()"" />
                    </Telephone>
                  </xsl:if>
                  <xsl:if test=""../s1:Telefax"">
                    <Telefax>
                      <xsl:value-of select=""../s1:Telefax/text()"" />
                    </Telefax>
                  </xsl:if>
                  <xsl:if test=""../s1:ElectronicMail"">
                    <E-mail>
                      <xsl:value-of select=""../s1:ElectronicMail/text()"" />
                    </E-mail>
                  </xsl:if>
                  <Note>
                    <xsl:value-of select=""./text()"" />
                  </Note>
                </Concact>
              </xsl:for-each>
            </xsl:for-each>
            <xsl:for-each select=""../s2:PartyTaxScheme"">
              <xsl:for-each select=""s1:CompanyID"">
                <PartyTaxScheme>
                  <CompanyID>
                    <xsl:value-of select=""./text()"" />
                  </CompanyID>
                </PartyTaxScheme>
              </xsl:for-each>
            </xsl:for-each>
            <xsl:for-each select=""../s2:PartyTaxScheme"">
              <xsl:for-each select=""s2:TaxScheme/s1:ID"">
                <TaxScheme>
                  <ID>
                    <xsl:value-of select=""./text()"" />
                  </ID>
                </TaxScheme>
              </xsl:for-each>
            </xsl:for-each>
            <xsl:for-each select=""../s2:PartyLegalEntity"">
              <xsl:for-each select=""s1:RegistrationName"">
                <PartyLegalEntity>
                  <RegistrationName>
                    <xsl:value-of select=""./text()"" />
                  </RegistrationName>
                  <xsl:for-each select=""../s1:CompanyID"">
                    <CompanyID>
                      <xsl:if test=""@schemeID"">
                        <xsl:attribute name=""schemeID"">
                          <xsl:value-of select=""@schemeID"" />
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:value-of select=""./text()"" />
                    </CompanyID>
                  </xsl:for-each>
                  <xsl:if test=""../s1:CompanyLegalForm"">
                    <CompanyLegalForm>
                      <xsl:value-of select=""../s1:CompanyLegalForm/text()"" />
                    </CompanyLegalForm>
                  </xsl:if>
                </PartyLegalEntity>
              </xsl:for-each>
            </xsl:for-each>
          </AccountingSupplier>
        </xsl:for-each>
      </xsl:for-each>
      <xsl:for-each select=""s2:AccountingCustomerParty/s2:Party"">
        <xsl:for-each select=""s2:PartyIdentification"">
          <xsl:for-each select=""s1:ID"">
            <AccountingCustomer>
              <xsl:if test=""../../s1:EndpointID"">
                <AccBuyerID>
                  <xsl:value-of select=""../../s1:EndpointID/text()"" />
                </AccBuyerID>
              </xsl:if>
              <PartyID>
                <xsl:value-of select=""./text()"" />
              </PartyID>
              <SchemeID>
                <xsl:if test=""../../s1:EndpointID/@schemeID"">
                  <Endpoint>
                    <xsl:value-of select=""../../s1:EndpointID/@schemeID"" />
                  </Endpoint>
                </xsl:if>
                <xsl:if test=""@schemeID"">
                  <Party>
                    <xsl:value-of select=""@schemeID"" />
                  </Party>
                </xsl:if>
              </SchemeID>
              <xsl:for-each select=""../../s2:PartyName"">
                <Address>
                  <Name>
                    <xsl:value-of select=""s1:Name/text()"" />
                  </Name>
                  <xsl:if test=""../s2:PostalAddress/s1:Postbox"">
                    <Postbox>
                      <xsl:value-of select=""../s2:PostalAddress/s1:Postbox/text()"" />
                    </Postbox>
                  </xsl:if>
                  <xsl:if test=""../s2:PostalAddress/s1:StreetName"">
                    <Street>
                      <xsl:value-of select=""../s2:PostalAddress/s1:StreetName/text()"" />
                    </Street>
                  </xsl:if>
                  <xsl:if test=""../s2:PostalAddress/s1:BuildingNumber"">
                    <Number>
                      <xsl:value-of select=""../s2:PostalAddress/s1:BuildingNumber/text()"" />
                    </Number>
                  </xsl:if>
                  <xsl:if test=""../s2:PostalAddress/s1:AdditionalStreetName"">
                    <AdditionalStreetName>
                      <xsl:value-of select=""../s2:PostalAddress/s1:AdditionalStreetName/text()"" />
                    </AdditionalStreetName>
                  </xsl:if>
                  <xsl:if test=""../s2:PostalAddress/s1:Department"">
                    <Department>
                      <xsl:value-of select=""../s2:PostalAddress/s1:Department/text()"" />
                    </Department>
                  </xsl:if>
                  <xsl:if test=""../s2:PostalAddress/s1:CityName"">
                    <City>
                      <xsl:value-of select=""../s2:PostalAddress/s1:CityName/text()"" />
                    </City>
                  </xsl:if>
                  <xsl:if test=""../s2:PostalAddress/s1:PostalZone"">
                    <PostalCode>
                      <xsl:value-of select=""../s2:PostalAddress/s1:PostalZone/text()"" />
                    </PostalCode>
                  </xsl:if>
                  <xsl:if test=""../s2:PostalAddress/s1:CountrySubentity"">
                    <CountrySubentity>
                      <xsl:value-of select=""../s2:PostalAddress/s1:CountrySubentity/text()"" />
                    </CountrySubentity>
                  </xsl:if>
                  <xsl:for-each select=""../s2:PostalAddress"">
                    <xsl:for-each select=""s2:AddressLine"">
                      <AddressLine>
                        <Line>
                          <xsl:value-of select=""s1:Line/text()"" />
                        </Line>
                      </AddressLine>
                    </xsl:for-each>
                  </xsl:for-each>
                  <xsl:if test=""../s2:PostalAddress/s2:Country/s1:IdentificationCode"">
                    <Country>
                      <xsl:value-of select=""../s2:PostalAddress/s2:Country/s1:IdentificationCode/text()"" />
                    </Country>
                  </xsl:if>
                </Address>
              </xsl:for-each>
              <xsl:for-each select=""../../s2:Contact"">
                <xsl:for-each select=""s1:Name"">
                  <Concact>
                    <xsl:variable name=""var:v2"" select=""userCSharp:ReturnNotEmpty()"" />
                    <ID>
                      <xsl:value-of select=""$var:v2"" />
                    </ID>
                    <Name>
                      <xsl:value-of select=""./text()"" />
                    </Name>
                    <xsl:if test=""../s1:Telephone"">
                      <Telephone>
                        <xsl:value-of select=""../s1:Telephone/text()"" />
                      </Telephone>
                    </xsl:if>
                    <xsl:if test=""../s1:Telefax"">
                      <Telefax>
                        <xsl:value-of select=""../s1:Telefax/text()"" />
                      </Telefax>
                    </xsl:if>
                    <xsl:if test=""../s1:ElectronicMail"">
                      <E-mail>
                        <xsl:value-of select=""../s1:ElectronicMail/text()"" />
                      </E-mail>
                    </xsl:if>
                  </Concact>
                </xsl:for-each>
              </xsl:for-each>
              <xsl:for-each select=""../../s2:PartyTaxScheme"">
                <xsl:for-each select=""s1:CompanyID"">
                  <PartyTaxScheme>
                    <CompanyID>
                      <xsl:value-of select=""./text()"" />
                    </CompanyID>
                  </PartyTaxScheme>
                </xsl:for-each>
              </xsl:for-each>
              <xsl:for-each select=""../../s2:PartyTaxScheme"">
                <xsl:for-each select=""s2:TaxScheme/s1:ID"">
                  <TaxScheme>
                    <ID>
                      <xsl:value-of select=""./text()"" />
                    </ID>
                  </TaxScheme>
                </xsl:for-each>
              </xsl:for-each>
              <xsl:for-each select=""../../s2:PartyLegalEntity"">
                <xsl:for-each select=""s1:RegistrationName"">
                  <PartyLegalEntity>
                    <RegistrationName>
                      <xsl:value-of select=""./text()"" />
                    </RegistrationName>
                    <xsl:for-each select=""../s1:CompanyID"">
                      <CompanyID>
                        <xsl:if test=""@schemeID"">
                          <xsl:attribute name=""schemeID"">
                            <xsl:value-of select=""@schemeID"" />
                          </xsl:attribute>
                        </xsl:if>
                        <xsl:value-of select=""./text()"" />
                      </CompanyID>
                    </xsl:for-each>
                  </PartyLegalEntity>
                </xsl:for-each>
              </xsl:for-each>
            </AccountingCustomer>
          </xsl:for-each>
        </xsl:for-each>
      </xsl:for-each>
      <xsl:for-each select=""s2:PayeeParty"">
        <xsl:for-each select=""s2:PartyIdentification"">
          <xsl:for-each select=""s1:ID"">
            <BuyerCustomer>
              <xsl:if test=""../../s1:EndpointID"">
                <BuyerID>
                  <xsl:value-of select=""../../s1:EndpointID/text()"" />
                </BuyerID>
              </xsl:if>
              <PartyID>
                <xsl:value-of select=""./text()"" />
              </PartyID>
              <SchemeID>
                <xsl:if test=""../../s1:EndpointID/@schemeID"">
                  <Endpoint>
                    <xsl:value-of select=""../../s1:EndpointID/@schemeID"" />
                  </Endpoint>
                </xsl:if>
                <xsl:if test=""@schemeID"">
                  <Party>
                    <xsl:value-of select=""@schemeID"" />
                  </Party>
                </xsl:if>
              </SchemeID>
              <xsl:for-each select=""../../s2:PartyLegalEntity"">
                <xsl:for-each select=""s1:RegistrationName"">
                  <PartyLegalEntity>
                    <RegistrationName>
                      <xsl:value-of select=""./text()"" />
                    </RegistrationName>
                    <xsl:for-each select=""../s1:CompanyID"">
                      <CompanyID>
                        <xsl:if test=""@schemeID"">
                          <xsl:attribute name=""schemeID"">
                            <xsl:value-of select=""@schemeID"" />
                          </xsl:attribute>
                        </xsl:if>
                        <xsl:value-of select=""./text()"" />
                      </CompanyID>
                    </xsl:for-each>
                  </PartyLegalEntity>
                </xsl:for-each>
              </xsl:for-each>
              <xsl:for-each select=""../../s2:PartyName"">
                <Address>
                  <Name>
                    <xsl:value-of select=""s1:Name/text()"" />
                  </Name>
                  <xsl:if test=""../s2:PostalAddress/s1:AddressFormatCode"">
                    <AddressFormatCode>
                      <xsl:value-of select=""../s2:PostalAddress/s1:AddressFormatCode/text()"" />
                    </AddressFormatCode>
                  </xsl:if>
                  <xsl:if test=""../s2:PostalAddress/s1:Postbox"">
                    <Postbox>
                      <xsl:value-of select=""../s2:PostalAddress/s1:Postbox/text()"" />
                    </Postbox>
                  </xsl:if>
                  <xsl:if test=""../s2:PostalAddress/s1:StreetName"">
                    <Street>
                      <xsl:value-of select=""../s2:PostalAddress/s1:StreetName/text()"" />
                    </Street>
                  </xsl:if>
                  <xsl:if test=""../s2:PostalAddress/s1:BuildingNumber"">
                    <Number>
                      <xsl:value-of select=""../s2:PostalAddress/s1:BuildingNumber/text()"" />
                    </Number>
                  </xsl:if>
                  <xsl:if test=""../s2:PostalAddress/s1:AdditionalStreetName"">
                    <AdditionalStreetName>
                      <xsl:value-of select=""../s2:PostalAddress/s1:AdditionalStreetName/text()"" />
                    </AdditionalStreetName>
                  </xsl:if>
                  <xsl:if test=""../s2:PostalAddress/s1:Department"">
                    <Department>
                      <xsl:value-of select=""../s2:PostalAddress/s1:Department/text()"" />
                    </Department>
                  </xsl:if>
                  <xsl:if test=""../s2:PostalAddress/s1:CityName"">
                    <City>
                      <xsl:value-of select=""../s2:PostalAddress/s1:CityName/text()"" />
                    </City>
                  </xsl:if>
                  <xsl:if test=""../s2:PostalAddress/s1:PostalZone"">
                    <PostalCode>
                      <xsl:value-of select=""../s2:PostalAddress/s1:PostalZone/text()"" />
                    </PostalCode>
                  </xsl:if>
                  <xsl:if test=""../s2:PostalAddress/s1:CountrySubentity"">
                    <CountrySubentity>
                      <xsl:value-of select=""../s2:PostalAddress/s1:CountrySubentity/text()"" />
                    </CountrySubentity>
                  </xsl:if>
                  <xsl:for-each select=""../s2:PostalAddress"">
                    <xsl:for-each select=""s2:AddressLine"">
                      <AddressLine>
                        <Line>
                          <xsl:value-of select=""./text()"" />
                        </Line>
                      </AddressLine>
                    </xsl:for-each>
                  </xsl:for-each>
                  <xsl:if test=""../s2:PostalAddress/s2:Country/s1:IdentificationCode"">
                    <Country>
                      <xsl:value-of select=""../s2:PostalAddress/s2:Country/s1:IdentificationCode/text()"" />
                    </Country>
                  </xsl:if>
                </Address>
              </xsl:for-each>
              <xsl:for-each select=""../../s2:PartyTaxScheme"">
                <xsl:for-each select=""s1:CompanyID"">
                  <PartyTaxScheme>
                    <CompanyID>
                      <xsl:value-of select=""./text()"" />
                    </CompanyID>
                  </PartyTaxScheme>
                </xsl:for-each>
              </xsl:for-each>
              <xsl:for-each select=""../../s2:PartyTaxScheme"">
                <xsl:for-each select=""s2:TaxScheme/s1:ID"">
                  <TaxScheme>
                    <ID>
                      <xsl:value-of select=""./text()"" />
                    </ID>
                  </TaxScheme>
                </xsl:for-each>
              </xsl:for-each>
            </BuyerCustomer>
          </xsl:for-each>
        </xsl:for-each>
      </xsl:for-each>
      <xsl:for-each select=""s2:Delivery"">
        <xsl:for-each select=""s2:DeliveryLocation"">
          <xsl:for-each select=""s1:ID"">
            <DeliveryLocation>
              <DeliveryID>
                <xsl:value-of select=""./text()"" />
              </DeliveryID>
              <SchemeID>
                <xsl:if test=""@schemeID"">
                  <Endpoint>
                    <xsl:value-of select=""@schemeID"" />
                  </Endpoint>
                </xsl:if>
              </SchemeID>
              <xsl:for-each select=""../../s2:DeliveryParty"">
                <xsl:for-each select=""s2:PartyName"">
                  <Address>
                    <Name>
                      <xsl:value-of select=""s1:Name/text()"" />
                    </Name>
                    <xsl:if test=""../../s2:DeliveryLocation/s2:Address/s1:StreetName"">
                      <Street>
                        <xsl:value-of select=""../../s2:DeliveryLocation/s2:Address/s1:StreetName/text()"" />
                      </Street>
                    </xsl:if>
                    <xsl:if test=""../../s2:DeliveryLocation/s2:Address/s1:AdditionalStreetName"">
                      <AdditionalStreetName>
                        <xsl:value-of select=""../../s2:DeliveryLocation/s2:Address/s1:AdditionalStreetName/text()"" />
                      </AdditionalStreetName>
                    </xsl:if>
                    <xsl:if test=""../../s2:DeliveryLocation/s2:Address/s1:CityName"">
                      <City>
                        <xsl:value-of select=""../../s2:DeliveryLocation/s2:Address/s1:CityName/text()"" />
                      </City>
                    </xsl:if>
                    <xsl:if test=""../../s2:DeliveryLocation/s2:Address/s1:PostalZone"">
                      <PostalCode>
                        <xsl:value-of select=""../../s2:DeliveryLocation/s2:Address/s1:PostalZone/text()"" />
                      </PostalCode>
                    </xsl:if>
                    <xsl:if test=""../../s2:DeliveryLocation/s2:Address/s1:CountrySubentity"">
                      <CountrySubentity>
                        <xsl:value-of select=""../../s2:DeliveryLocation/s2:Address/s1:CountrySubentity/text()"" />
                      </CountrySubentity>
                    </xsl:if>
                    <xsl:for-each select=""../../s2:DeliveryLocation/s2:Address"">
                      <xsl:for-each select=""s2:AddressLine"">
                        <AddressLine>
                          <Line>
                            <xsl:value-of select=""s1:Line/text()"" />
                          </Line>
                        </AddressLine>
                      </xsl:for-each>
                    </xsl:for-each>
                    <xsl:if test=""../../s2:DeliveryLocation/s2:Address/s2:Country/s1:IdentificationCode"">
                      <Country>
                        <xsl:value-of select=""../../s2:DeliveryLocation/s2:Address/s2:Country/s1:IdentificationCode/text()"" />
                      </Country>
                    </xsl:if>
                  </Address>
                </xsl:for-each>
              </xsl:for-each>
            </DeliveryLocation>
          </xsl:for-each>
        </xsl:for-each>
      </xsl:for-each>
      <xsl:for-each select=""s2:Delivery"">
        <xsl:for-each select=""s1:ActualDeliveryDate"">
          <DeliveryInfo>
            <ActualDeliveryDate>
              <xsl:value-of select=""./text()"" />
            </ActualDeliveryDate>
          </DeliveryInfo>
        </xsl:for-each>
      </xsl:for-each>
      <xsl:for-each select=""s2:PaymentMeans"">
        <xsl:for-each select=""s1:PaymentID"">
          <PaymentMeans>
            <PaymentMeansCode>
              <xsl:value-of select=""../s1:PaymentMeansCode/text()"" />
            </PaymentMeansCode>
            <PaymentID>
              <xsl:value-of select=""./text()"" />
            </PaymentID>
            <xsl:for-each select=""../s2:CardAccount"">
              <CardAccount>
                <PrimaryAccountNumberID>
                  <xsl:value-of select=""s1:PrimaryAccountNumberID/text()"" />
                </PrimaryAccountNumberID>
                <NetworkID>
                  <xsl:value-of select=""s1:NetworkID/text()"" />
                </NetworkID>
                <xsl:if test=""s1:HolderName"">
                  <HolderName>
                    <xsl:value-of select=""s1:HolderName/text()"" />
                  </HolderName>
                </xsl:if>
              </CardAccount>
            </xsl:for-each>
            <xsl:for-each select=""../s2:PayerFinancialAccount"">
              <xsl:for-each select=""s1:ID"">
                <PayeeFinancialAccount>
                  <ID>
                    <xsl:value-of select=""./text()"" />
                  </ID>
                  <xsl:if test=""../s1:Name"">
                    <Name>
                      <xsl:value-of select=""../s1:Name/text()"" />
                    </Name>
                  </xsl:if>
                  <xsl:for-each select=""../s2:FinancialInstitutionBranch"">
                    <xsl:for-each select=""s1:ID"">
                      <FinancialInstitutionBranch>
                        <ID>
                          <xsl:value-of select=""./text()"" />
                        </ID>
                      </FinancialInstitutionBranch>
                    </xsl:for-each>
                  </xsl:for-each>
                </PayeeFinancialAccount>
              </xsl:for-each>
            </xsl:for-each>
            <xsl:for-each select=""../s2:PaymentMandate"">
              <xsl:for-each select=""s1:ID"">
                <PaymentMandate>
                  <ID>
                    <xsl:value-of select=""./text()"" />
                  </ID>
                  <xsl:for-each select=""../s2:PayerFinancialAccount"">
                    <xsl:for-each select=""s1:ID"">
                      <PayerFinancialAccount>
                        <ID>
                          <xsl:value-of select=""./text()"" />
                        </ID>
                      </PayerFinancialAccount>
                    </xsl:for-each>
                  </xsl:for-each>
                </PaymentMandate>
              </xsl:for-each>
            </xsl:for-each>
            <Attributes>
              <PaymentMeansCode>
                <xsl:if test=""../s1:PaymentMeansCode/@name"">
                  <name>
                    <xsl:value-of select=""../s1:PaymentMeansCode/@name"" />
                  </name>
                </xsl:if>
              </PaymentMeansCode>
            </Attributes>
          </PaymentMeans>
        </xsl:for-each>
      </xsl:for-each>
      <PaymentTerms>
        <xsl:for-each select=""s2:PaymentTerms"">
          <xsl:for-each select=""s1:Note"">
            <Note>
              <xsl:value-of select=""./text()"" />
            </Note>
          </xsl:for-each>
        </xsl:for-each>
      </PaymentTerms>
      <xsl:for-each select=""s2:AllowanceCharge"">
        <xsl:for-each select=""s1:AllowanceChargeReason"">
          <AllowanceCharge>
            <xsl:if test=""../s1:ID"">
              <ID>
                <xsl:value-of select=""../s1:ID/text()"" />
              </ID>
            </xsl:if>
            <ChargeIndicator>
              <xsl:value-of select=""../s1:ChargeIndicator/text()"" />
            </ChargeIndicator>
            <xsl:if test=""../s1:AllowanceChargeReasonCode"">
              <AllowanceChargeReasonCode>
                <xsl:value-of select=""../s1:AllowanceChargeReasonCode/text()"" />
              </AllowanceChargeReasonCode>
            </xsl:if>
            <AllowanceChargeReason>
              <xsl:value-of select=""./text()"" />
            </AllowanceChargeReason>
            <xsl:if test=""../s1:MultiplierFactorNumeric"">
              <MultiplierFactorNumeric>
                <xsl:value-of select=""../s1:MultiplierFactorNumeric/text()"" />
              </MultiplierFactorNumeric>
            </xsl:if>
            <Amount>
              <xsl:value-of select=""../s1:Amount/text()"" />
            </Amount>
            <xsl:if test=""../s1:BaseAmount"">
              <BaseAmount>
                <xsl:value-of select=""../s1:BaseAmount/text()"" />
              </BaseAmount>
            </xsl:if>
            <xsl:for-each select=""../s2:TaxCategory"">
              <xsl:for-each select=""s1:ID"">
                <TaxCategory>
                  <ID>
                    <xsl:value-of select=""./text()"" />
                  </ID>
                  <xsl:if test=""../s1:Percent"">
                    <Percent>
                      <xsl:value-of select=""../s1:Percent/text()"" />
                    </Percent>
                  </xsl:if>
                  <xsl:for-each select=""../s2:TaxScheme"">
                    <xsl:for-each select=""s1:ID"">
                      <TaxScheme>
                        <ID>
                          <xsl:value-of select=""./text()"" />
                        </ID>
                        <xsl:if test=""../s1:Name"">
                          <Navn>
                            <xsl:value-of select=""../s1:Name/text()"" />
                          </Navn>
                        </xsl:if>
                      </TaxScheme>
                    </xsl:for-each>
                  </xsl:for-each>
                </TaxCategory>
              </xsl:for-each>
            </xsl:for-each>
            <Attributes>
              <Amount>
                <CurrencyID>
                  <xsl:value-of select=""../s1:Amount/@currencyID"" />
                </CurrencyID>
              </Amount>
              <xsl:for-each select=""../s1:BaseAmount"">
                <BaseAmount>
                  <CurrencyID>
                    <xsl:value-of select=""@currencyID"" />
                  </CurrencyID>
                </BaseAmount>
              </xsl:for-each>
            </Attributes>
          </AllowanceCharge>
        </xsl:for-each>
      </xsl:for-each>
      <xsl:for-each select=""s2:TaxTotal"">
        <TaxTotal>
          <TaxAmount>
            <xsl:value-of select=""s1:TaxAmount/text()"" />
          </TaxAmount>
          <Currency>
            <xsl:value-of select=""s1:TaxAmount/@currencyID"" />
          </Currency>
          <xsl:for-each select=""s2:TaxSubtotal"">
            <TaxSubtotal>
              <TaxAmount>
                <xsl:value-of select=""s1:TaxAmount/text()"" />
              </TaxAmount>
              <TaxAmountCurrency>
                <xsl:value-of select=""s1:TaxAmount/@currencyID"" />
              </TaxAmountCurrency>
              <xsl:if test=""s1:TaxableAmount"">
                <TaxableAmount>
                  <xsl:value-of select=""s1:TaxableAmount/text()"" />
                </TaxableAmount>
              </xsl:if>
              <TaxableAmountCurrency>
                <xsl:value-of select=""s1:TaxableAmount/@currencyID"" />
              </TaxableAmountCurrency>
              <xsl:for-each select=""s2:TaxCategory/s1:TaxExemptionReason"">
                <TaxCategory>
                  <xsl:if test=""../s1:ID"">
                    <ID>
                      <xsl:value-of select=""../s1:ID/text()"" />
                    </ID>
                  </xsl:if>
                  <xsl:if test=""../s1:Percent"">
                    <Percent>
                      <xsl:value-of select=""../s1:Percent/text()"" />
                    </Percent>
                  </xsl:if>
                  <xsl:if test=""../s1:TaxExemptionReasonCode"">
                    <TaxExemptionReasonCode>
                      <xsl:value-of select=""../s1:TaxExemptionReasonCode/text()"" />
                    </TaxExemptionReasonCode>
                  </xsl:if>
                  <TaxExemptionReason>
                    <xsl:value-of select=""./text()"" />
                  </TaxExemptionReason>
                  <xsl:for-each select="".."">
                    <xsl:for-each select=""s2:TaxScheme/s1:ID"">
                      <TaxScheme>
                        <ID>
                          <xsl:value-of select=""./text()"" />
                        </ID>
                        <xsl:if test=""@schemeName"">
                          <Navn>
                            <xsl:value-of select=""@schemeName"" />
                          </Navn>
                        </xsl:if>
                      </TaxScheme>
                    </xsl:for-each>
                  </xsl:for-each>
                </TaxCategory>
              </xsl:for-each>
            </TaxSubtotal>
          </xsl:for-each>
        </TaxTotal>
      </xsl:for-each>
      <xsl:for-each select=""s2:LegalMonetaryTotal/s1:LineExtensionAmount"">
        <Total>
          <LineTotalAmount>
            <xsl:value-of select=""./text()"" />
          </LineTotalAmount>
          <xsl:if test=""../s1:TaxExclusiveAmount"">
            <TaxExclAmount>
              <xsl:value-of select=""../s1:TaxExclusiveAmount/text()"" />
            </TaxExclAmount>
          </xsl:if>
          <xsl:if test=""../s1:TaxInclusiveAmount"">
            <TaxInclAmount>
              <xsl:value-of select=""../s1:TaxInclusiveAmount/text()"" />
            </TaxInclAmount>
          </xsl:if>
          <xsl:if test=""../s1:AllowanceTotalAmount"">
            <AllowanceTotalAmount>
              <xsl:value-of select=""../s1:AllowanceTotalAmount/text()"" />
            </AllowanceTotalAmount>
          </xsl:if>
          <xsl:if test=""../s1:ChargeTotalAmount"">
            <ChargeTotalAmount>
              <xsl:value-of select=""../s1:ChargeTotalAmount/text()"" />
            </ChargeTotalAmount>
          </xsl:if>
          <xsl:if test=""../s1:PrepaidAmount"">
            <PrepaidAmount>
              <xsl:value-of select=""../s1:PrepaidAmount/text()"" />
            </PrepaidAmount>
          </xsl:if>
          <xsl:if test=""../s1:PayableRoundingAmount"">
            <PayableRoundingAmount>
              <xsl:value-of select=""../s1:PayableRoundingAmount/text()"" />
            </PayableRoundingAmount>
          </xsl:if>
          <PayableAmount>
            <xsl:value-of select=""../s1:PayableAmount/text()"" />
          </PayableAmount>
          <Attributes>
            <LineTotalAmount>
              <xsl:attribute name=""CurrencyID"">
                <xsl:value-of select=""@currencyID"" />
              </xsl:attribute>
            </LineTotalAmount>
            <xsl:for-each select=""../s1:TaxExclusiveAmount"">
              <TaxExclAmount>
                <xsl:attribute name=""CurrencyID"">
                  <xsl:value-of select=""@currencyID"" />
                </xsl:attribute>
              </TaxExclAmount>
            </xsl:for-each>
            <xsl:for-each select=""../s1:TaxInclusiveAmount"">
              <TaxInclAmount>
                <xsl:attribute name=""CurrencyID"">
                  <xsl:value-of select=""@currencyID"" />
                </xsl:attribute>
              </TaxInclAmount>
            </xsl:for-each>
            <xsl:for-each select=""../s1:AllowanceTotalAmount"">
              <AllowanceTotalAmount>
                <xsl:attribute name=""CurrencyID"">
                  <xsl:value-of select=""@currencyID"" />
                </xsl:attribute>
              </AllowanceTotalAmount>
            </xsl:for-each>
            <xsl:for-each select=""../s1:ChargeTotalAmount"">
              <ChargeTotalAmount>
                <xsl:attribute name=""CurrencyID"">
                  <xsl:value-of select=""@currencyID"" />
                </xsl:attribute>
              </ChargeTotalAmount>
            </xsl:for-each>
            <xsl:for-each select=""../s1:PrepaidAmount"">
              <PrepaidAmount>
                <xsl:attribute name=""CurrencyID"">
                  <xsl:value-of select=""@currencyID"" />
                </xsl:attribute>
              </PrepaidAmount>
            </xsl:for-each>
            <xsl:for-each select=""../s1:PayableRoundingAmount"">
              <PayableRoundingAmount>
                <xsl:attribute name=""CurrencyID"">
                  <xsl:value-of select=""@currencyID"" />
                </xsl:attribute>
              </PayableRoundingAmount>
            </xsl:for-each>
            <PayableAmount>
              <xsl:value-of select=""../s1:PayableAmount/@currencyID"" />
            </PayableAmount>
          </Attributes>
        </Total>
      </xsl:for-each>
      <Lines>
        <xsl:for-each select=""s2:InvoiceLine"">
          <xsl:for-each select=""s1:Note"">
            <Line>
              <LineNo>
                <xsl:value-of select=""../s1:ID/text()"" />
              </LineNo>
              <Note>
                <xsl:value-of select=""./text()"" />
              </Note>
              <xsl:if test=""../s1:InvoicedQuantity"">
                <Quantity>
                  <xsl:value-of select=""../s1:InvoicedQuantity/text()"" />
                </Quantity>
              </xsl:if>
              <xsl:if test=""../s1:InvoicedQuantity/@unitCode"">
                <UnitCode>
                  <xsl:value-of select=""../s1:InvoicedQuantity/@unitCode"" />
                </UnitCode>
              </xsl:if>
              <LineAmountTotal>
                <xsl:value-of select=""../s1:LineExtensionAmount/text()"" />
              </LineAmountTotal>
              <Currency>
                <xsl:value-of select=""../s1:LineExtensionAmount/@currencyID"" />
              </Currency>
              <xsl:if test=""../s1:AccountingCost"">
                <AccountingCost>
                  <xsl:value-of select=""../s1:AccountingCost/text()"" />
                </AccountingCost>
              </xsl:if>
              <xsl:for-each select=""../s2:DocumentReference"">
                <xsl:for-each select=""s1:DocumentTypeCode"">
                  <DocumentReference>
                    <ID>
                      <xsl:if test=""../s1:ID/@schemeID"">
                        <xsl:attribute name=""schemeID"">
                          <xsl:value-of select=""../s1:ID/@schemeID"" />
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:value-of select=""../s1:ID/text()"" />
                    </ID>
                    <DocumentTypeCode>
                      <xsl:value-of select=""./text()"" />
                    </DocumentTypeCode>
                  </DocumentReference>
                </xsl:for-each>
              </xsl:for-each>
              <xsl:for-each select=""../s2:OrderLineReference"">
                <OrderLineReference>
                  <LineID>
                    <xsl:value-of select=""s1:LineID/text()"" />
                  </LineID>
                </OrderLineReference>
              </xsl:for-each>
              <xsl:for-each select=""../s2:Item"">
                <xsl:for-each select=""s1:Description"">
                  <Item>
                    <BuyerItemID>
                      <xsl:value-of select=""../s2:BuyersItemIdentification/s1:ID/text()"" />
                    </BuyerItemID>
                    <SellerItemID>
                      <xsl:value-of select=""../s2:SellersItemIdentification/s1:ID/text()"" />
                    </SellerItemID>
                    <StandardItemID>
                      <xsl:value-of select=""../s2:StandardItemIdentification/s1:ID/text()"" />
                    </StandardItemID>
                    <xsl:for-each select="".."">
                      <xsl:for-each select=""s1:Name"">
                        <Name>
                          <xsl:value-of select=""./text()"" />
                        </Name>
                      </xsl:for-each>
                    </xsl:for-each>
                    <Description>
                      <xsl:value-of select=""./text()"" />
                    </Description>
                    <xsl:for-each select="".."">
                      <xsl:for-each select=""s2:StandardItemIdentification"">
                        <SchemeID>
                          <xsl:if test=""s1:ID/@schemeID"">
                            <StdItemID>
                              <xsl:value-of select=""s1:ID/@schemeID"" />
                            </StdItemID>
                          </xsl:if>
                          <xsl:if test=""../s2:BuyersItemIdentification/s1:ID/@schemeID"">
                            <BuyItemID>
                              <xsl:value-of select=""../s2:BuyersItemIdentification/s1:ID/@schemeID"" />
                            </BuyItemID>
                          </xsl:if>
                          <xsl:if test=""../s2:SellersItemIdentification/s1:ID/@schemeID"">
                            <SelItemID>
                              <xsl:value-of select=""../s2:SellersItemIdentification/s1:ID/@schemeID"" />
                            </SelItemID>
                          </xsl:if>
                        </SchemeID>
                      </xsl:for-each>
                    </xsl:for-each>
                    <xsl:for-each select="".."">
                      <xsl:for-each select=""s2:OriginCountry"">
                        <xsl:for-each select=""s1:IdentificationCode"">
                          <OriginCountry>
                            <IdentificationCode>
                              <xsl:value-of select=""./text()"" />
                            </IdentificationCode>
                          </OriginCountry>
                        </xsl:for-each>
                      </xsl:for-each>
                    </xsl:for-each>
                    <xsl:for-each select="".."">
                      <xsl:for-each select=""s2:CommodityClassification"">
                        <CommodityClassification>
                          <ItemClassificationCode>
                            <xsl:if test=""s1:ItemClassificationCode/@listID"">
                              <xsl:attribute name=""listID"">
                                <xsl:value-of select=""s1:ItemClassificationCode/@listID"" />
                              </xsl:attribute>
                            </xsl:if>
                            <xsl:if test=""s1:ItemClassificationCode/@listVersionID"">
                              <xsl:attribute name=""listVersionID"">
                                <xsl:value-of select=""s1:ItemClassificationCode/@listVersionID"" />
                              </xsl:attribute>
                            </xsl:if>
                            <xsl:if test=""s1:ItemClassificationCode"">
                              <xsl:value-of select=""s1:ItemClassificationCode/text()"" />
                            </xsl:if>
                          </ItemClassificationCode>
                        </CommodityClassification>
                      </xsl:for-each>
                    </xsl:for-each>
                    <xsl:for-each select="".."">
                      <xsl:for-each select=""s2:ClassifiedTaxCategory"">
                        <xsl:for-each select=""s1:ID"">
                          <ClassifiedTaxCategory>
                            <ID>
                              <xsl:value-of select=""./text()"" />
                            </ID>
                            <xsl:if test=""../s1:Percent"">
                              <Percent>
                                <xsl:value-of select=""../s1:Percent/text()"" />
                              </Percent>
                            </xsl:if>
                            <xsl:for-each select=""../s2:TaxScheme"">
                              <xsl:for-each select=""s1:ID"">
                                <TaxScheme>
                                  <ID>
                                    <xsl:value-of select=""./text()"" />
                                  </ID>
                                </TaxScheme>
                              </xsl:for-each>
                            </xsl:for-each>
                          </ClassifiedTaxCategory>
                        </xsl:for-each>
                      </xsl:for-each>
                    </xsl:for-each>
                    <xsl:for-each select="".."">
                      <xsl:for-each select=""s2:AdditionalItemProperty"">
                        <AdditionalItemProperty>
                          <Name>
                            <xsl:value-of select=""s1:Name/text()"" />
                          </Name>
                          <xsl:if test=""s1:Value"">
                            <Value>
                              <xsl:value-of select=""s1:Value/text()"" />
                            </Value>
                          </xsl:if>
                        </AdditionalItemProperty>
                      </xsl:for-each>
                    </xsl:for-each>
                  </Item>
                </xsl:for-each>
              </xsl:for-each>
              <xsl:for-each select=""../s2:Price"">
                <PriceNet>
                  <Price>
                    <xsl:value-of select=""s1:PriceAmount/text()"" />
                  </Price>
                  <CurrencyID>
                    <xsl:value-of select=""s1:PriceAmount/@currencyID"" />
                  </CurrencyID>
                  <xsl:if test=""s1:BaseQuantity"">
                    <Quantity>
                      <xsl:value-of select=""s1:BaseQuantity/text()"" />
                    </Quantity>
                  </xsl:if>
                  <xsl:if test=""s1:BaseQuantity/@unitCode"">
                    <UnitCode>
                      <xsl:value-of select=""s1:BaseQuantity/@unitCode"" />
                    </UnitCode>
                  </xsl:if>
                  <xsl:for-each select=""s2:AllowanceCharge"">
                    <AllowanceCharge>
                      <ChargeIndicator>
                        <xsl:value-of select=""s1:ChargeIndicator/text()"" />
                      </ChargeIndicator>
                      <Amount>
                        <xsl:attribute name=""currencyID"">
                          <xsl:value-of select=""s1:Amount/@currencyID"" />
                        </xsl:attribute>
                        <xsl:value-of select=""s1:Amount/text()"" />
                      </Amount>
                      <xsl:for-each select=""s1:BaseAmount"">
                        <BaseAmount>
                          <xsl:attribute name=""currencyID"">
                            <xsl:value-of select=""@currencyID"" />
                          </xsl:attribute>
                          <xsl:value-of select=""./text()"" />
                        </BaseAmount>
                      </xsl:for-each>
                    </AllowanceCharge>
                  </xsl:for-each>
                </PriceNet>
              </xsl:for-each>
              <xsl:for-each select=""../s2:AllowanceCharge"">
                <xsl:for-each select=""s1:AllowanceChargeReason"">
                  <AllowanceCharge>
                    <xsl:if test=""../s1:ID"">
                      <ID>
                        <xsl:value-of select=""../s1:ID/text()"" />
                      </ID>
                    </xsl:if>
                    <ChargeIndicator>
                      <xsl:value-of select=""../s1:ChargeIndicator/text()"" />
                    </ChargeIndicator>
                    <xsl:if test=""../s1:AllowanceChargeReasonCode"">
                      <AllowanceChargeReasonCode>
                        <xsl:value-of select=""../s1:AllowanceChargeReasonCode/text()"" />
                      </AllowanceChargeReasonCode>
                    </xsl:if>
                    <AllowanceChargeReason>
                      <xsl:value-of select=""./text()"" />
                    </AllowanceChargeReason>
                    <xsl:if test=""../s1:MultiplierFactorNumeric"">
                      <MultiplierFactorNumeric>
                        <xsl:value-of select=""../s1:MultiplierFactorNumeric/text()"" />
                      </MultiplierFactorNumeric>
                    </xsl:if>
                    <Amount>
                      <xsl:value-of select=""../s1:Amount/text()"" />
                    </Amount>
                    <xsl:if test=""../s1:BaseAmount"">
                      <BaseAmount>
                        <xsl:value-of select=""../s1:BaseAmount/text()"" />
                      </BaseAmount>
                    </xsl:if>
                    <Attributes>
                      <Amount>
                        <xsl:attribute name=""CurrencyID"">
                          <xsl:value-of select=""../s1:Amount/@currencyID"" />
                        </xsl:attribute>
                      </Amount>
                      <xsl:for-each select=""../s1:BaseAmount"">
                        <BaseAmount>
                          <xsl:attribute name=""CurrencyID"">
                            <xsl:value-of select=""@currencyID"" />
                          </xsl:attribute>
                        </BaseAmount>
                      </xsl:for-each>
                    </Attributes>
                  </AllowanceCharge>
                </xsl:for-each>
              </xsl:for-each>
              <xsl:for-each select=""../s2:InvoicePeriod"">
                <xsl:for-each select=""s1:StartDate"">
                  <Period>
                    <FromDate>
                      <xsl:value-of select=""./text()"" />
                    </FromDate>
                    <xsl:if test=""../s1:EndDate"">
                      <ToDate>
                        <xsl:value-of select=""../s1:EndDate/text()"" />
                      </ToDate>
                    </xsl:if>
                  </Period>
                </xsl:for-each>
              </xsl:for-each>
            </Line>
          </xsl:for-each>
        </xsl:for-each>
      </Lines>
    </ns0:Invoice>
  </xsl:template>
  <msxsl:script language=""C#"" implements-prefix=""userCSharp""><![CDATA[
public string ReturnNotEmpty()
{
  return ""Test"";
}

public string ReturnNotEmpty(string InputValue)
{
 if (String.IsNullOrWhiteSpace(InputValue))
  {
   return ""N/A"";
  }
 return InputValue;
}

public string Dummy()
{
 return ""Dummy"";
}


]]></msxsl:script>
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"BYGE.Integration.PeppolInvoice.UBL_Invoice_2_1";
        
        private const global::BYGE.Integration.PeppolInvoice.UBL_Invoice_2_1 _srcSchemaTypeReference0 = null;
        
        private const string _strTrgSchemasList0 = @"BYGE.Integration.Core.Invoice.Invoice";
        
        private const global::BYGE.Integration.Core.Invoice.Invoice _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [1];
                _SrcSchemas[0] = @"BYGE.Integration.PeppolInvoice.UBL_Invoice_2_1";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"BYGE.Integration.Core.Invoice.Invoice";
                return _TrgSchemas;
            }
        }
    }
}
