namespace BYGE.Integration.Davidsen.Invoice.Maps {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.Core.Invoice.Invoice", typeof(global::BYGE.Integration.Core.Invoice.Invoice))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.Core.Invoice.Invoice", typeof(global::BYGE.Integration.Core.Invoice.Invoice))]
    public sealed class Core_Invoice_to_Core_Invoice_Davidsen : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var"" version=""1.0"" xmlns:ns0=""http://byg-e.dk/schemas/v10"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/ns0:Invoice"" />
  </xsl:template>
  <xsl:template match=""/ns0:Invoice"">
    <ns0:Invoice>
      
      <xsl:copy-of select=""./@*[not(self::AccountingSupplier)]"" />
      <xsl:copy-of select=""./*[not(self::AccountingSupplier)]"" />

      <AccountingSupplier>
        <AccSellerID>
          <xsl:value-of select=""AccountingSupplier/AccSellerID/text()"" />
        </AccSellerID>
        <xsl:if test=""AccountingSupplier/PartyID"">         
          <PartyID>
            <xsl:if test=""AccountingSupplier/AccSellerID/text() != '5790000824014'"">
              <xsl:value-of select=""AccountingSupplier/PartyID/text()"" />
            </xsl:if>

            <xsl:if test=""AccountingSupplier/AccSellerID/text() = '5790000824014'"">

              <xsl:if test=""string-length(Total/TaxExclAmount) != 0 and Total/TaxExclAmount/text() != '0' and Total/TaxExclAmount/text() != '0.00'"">
                <xsl:if test=""string-length(Currency) != 0"">
                 <xsl:choose>
                  <xsl:when test=""translate(Currency/text(), 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ') = 'DKK'"">
                    <xsl:value-of select=""AccountingSupplier/PartyID/text()"" />
                  </xsl:when>
                  <xsl:when test=""translate(Currency/text(), 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ') = 'USD'"">
                    <xsl:text>21865</xsl:text>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select=""AccountingSupplier/PartyID/text()"" />
                  </xsl:otherwise>
                </xsl:choose>
                </xsl:if>
                <xsl:if test=""string-length(Currency) = 0"">
                  <xsl:value-of select=""AccountingSupplier/PartyID/text()"" />
                </xsl:if>  
              </xsl:if>
              
              <xsl:if test=""string-length(Total/TaxExclAmount) = 0 or Total/TaxExclAmount/text() = '0' or Total/TaxExclAmount/text() = '0.00'"">
                <xsl:if test=""string-length(Currency) != 0"">
                  <xsl:choose>
                    <xsl:when test=""translate(Currency/text(), 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ') = 'DKK'"">
                      <xsl:value-of select=""AccountingSupplier/PartyID/text()"" />
                    </xsl:when>
                    <xsl:when test=""translate(Currency/text(), 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ') = 'USD'"">
                      <xsl:text>27283</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select=""AccountingSupplier/PartyID/text()"" />
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
                <xsl:if test=""string-length(Currency) = 0"">
                  <xsl:value-of select=""AccountingSupplier/PartyID/text()"" />
                </xsl:if>
              </xsl:if>

   
            </xsl:if>

          </PartyID>
        </xsl:if>
        <xsl:if test=""AccountingSupplier/CompanyID"">
          <CompanyID>
            <xsl:value-of select=""AccountingSupplier/CompanyID/text()"" />
          </CompanyID>
        </xsl:if>
        <CompanyTaxID>
          <xsl:value-of select=""AccountingSupplier/CompanyTaxID/text()"" />
        </CompanyTaxID>
        <SchemeID>
          <xsl:if test=""AccountingSupplier/SchemeID/Endpoint"">
            <Endpoint>
              <xsl:value-of select=""AccountingSupplier/SchemeID/Endpoint/text()"" />
            </Endpoint>
          </xsl:if>
          <xsl:if test=""AccountingSupplier/SchemeID/Party"">
            <Party>
              <xsl:value-of select=""AccountingSupplier/SchemeID/Party/text()"" />
            </Party>
          </xsl:if>
          <xsl:if test=""AccountingSupplier/SchemeID/Company"">
            <Company>
              <xsl:value-of select=""AccountingSupplier/SchemeID/Company/text()"" />
            </Company>
          </xsl:if>
          <xsl:if test=""AccountingSupplier/SchemeID/CompanyTax"">
            <CompanyTax>
              <xsl:value-of select=""AccountingSupplier/SchemeID/CompanyTax/text()"" />
            </CompanyTax>
          </xsl:if>
          <xsl:value-of select=""AccountingSupplier/SchemeID/text()"" />
        </SchemeID>
        <xsl:for-each select=""AccountingSupplier/Address"">
          <Address>
            <xsl:if test=""Name"">
              <Name>
                <xsl:value-of select=""Name/text()"" />
              </Name>
            </xsl:if>
            <xsl:if test=""Postbox"">
              <Postbox>
                <xsl:value-of select=""Postbox/text()"" />
              </Postbox>
            </xsl:if>
            <xsl:if test=""AddressFormatCode"">
              <AddressFormatCode>
                <xsl:value-of select=""AddressFormatCode/text()"" />
              </AddressFormatCode>
            </xsl:if>
            <xsl:if test=""Street"">
              <Street>
                <xsl:value-of select=""Street/text()"" />
              </Street>
            </xsl:if>
            <xsl:if test=""Number"">
              <Number>
                <xsl:value-of select=""Number/text()"" />
              </Number>
            </xsl:if>
            <xsl:if test=""AdditionalStreetName"">
              <AdditionalStreetName>
                <xsl:value-of select=""AdditionalStreetName/text()"" />
              </AdditionalStreetName>
            </xsl:if>
            <xsl:if test=""Department"">
              <Department>
                <xsl:value-of select=""Department/text()"" />
              </Department>
            </xsl:if>
            <xsl:if test=""City"">
              <City>
                <xsl:value-of select=""City/text()"" />
              </City>
            </xsl:if>
            <xsl:if test=""PostalCode"">
              <PostalCode>
                <xsl:value-of select=""PostalCode/text()"" />
              </PostalCode>
            </xsl:if>
            <xsl:if test=""Country"">
              <Country>
                <xsl:value-of select=""Country/text()"" />
              </Country>
            </xsl:if>
            <xsl:value-of select=""./text()"" />
          </Address>
        </xsl:for-each>
        <xsl:for-each select=""AccountingSupplier/Concact"">
          <Concact>
            <ID>
              <xsl:value-of select=""ID/text()"" />
            </ID>
            <xsl:if test=""Name"">
              <Name>
                <xsl:value-of select=""Name/text()"" />
              </Name>
            </xsl:if>
            <xsl:if test=""Telephone"">
              <Telephone>
                <xsl:value-of select=""Telephone/text()"" />
              </Telephone>
            </xsl:if>
            <xsl:if test=""Telefax"">
              <Telefax>
                <xsl:value-of select=""Telefax/text()"" />
              </Telefax>
            </xsl:if>
            <xsl:if test=""E-mail"">
              <E-mail>
                <xsl:value-of select=""E-mail/text()"" />
              </E-mail>
            </xsl:if>
            <xsl:if test=""Note"">
              <Note>
                <xsl:value-of select=""Note/text()"" />
              </Note>
            </xsl:if> 
              <xsl:value-of select=""./text()"" />
            </Concact>
        </xsl:for-each>
        <xsl:for-each select=""AccountingSupplier/RFFNADLoop"">
          <RFFNADLoop>
            <xsl:for-each select=""Node"">
              <Node>
                <xsl:if test=""Code"">
                  <Code>
                    <xsl:value-of select=""Code/text()"" />
                  </Code>
                </xsl:if>
                <xsl:if test=""Reference"">
                  <Reference>
                    <xsl:value-of select=""Reference/text()"" />
                  </Reference>
                </xsl:if>
                <xsl:value-of select=""./text()"" />
              </Node>
            </xsl:for-each>
            <xsl:value-of select=""./text()"" />
          </RFFNADLoop>
        </xsl:for-each>
        <xsl:value-of select=""AccountingSupplier/text()"" />
      </AccountingSupplier>


    </ns0:Invoice>
  </xsl:template>
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"BYGE.Integration.Core.Invoice.Invoice";
        
        private const global::BYGE.Integration.Core.Invoice.Invoice _srcSchemaTypeReference0 = null;
        
        private const string _strTrgSchemasList0 = @"BYGE.Integration.Core.Invoice.Invoice";
        
        private const global::BYGE.Integration.Core.Invoice.Invoice _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [1];
                _SrcSchemas[0] = @"BYGE.Integration.Core.Invoice.Invoice";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"BYGE.Integration.Core.Invoice.Invoice";
                return _TrgSchemas;
            }
        }
    }
}
