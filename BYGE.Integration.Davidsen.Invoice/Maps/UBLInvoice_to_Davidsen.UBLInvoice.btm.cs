namespace BYGE.Integration.Davidsen.Invoice.Maps {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.UBLInvoice.UBLInvoice", typeof(global::BYGE.Integration.UBLInvoice.UBLInvoice))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.UBLInvoice.UBLInvoice", typeof(global::BYGE.Integration.UBLInvoice.UBLInvoice))]
    public sealed class UBLInvoice_to_Davidsen_UBLInvoice : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var"" version=""1.0"" xmlns:ns0=""urn:oasis:names:specification:ubl:schema:xsd:Invoice-2"" xmlns:cac=""urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"" xmlns:cbc=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/ns0:Invoice"" />
  </xsl:template>
  <xsl:template match=""/ns0:Invoice"">
    <ns0:Invoice>
      <xsl:copy-of select=""./@*"" />
      <xsl:copy-of select=""./*"" />
    </ns0:Invoice>
  </xsl:template>
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"BYGE.Integration.UBLInvoice.UBLInvoice";
        
        private const global::BYGE.Integration.UBLInvoice.UBLInvoice _srcSchemaTypeReference0 = null;
        
        private const string _strTrgSchemasList0 = @"BYGE.Integration.UBLInvoice.UBLInvoice";
        
        private const global::BYGE.Integration.UBLInvoice.UBLInvoice _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [1];
                _SrcSchemas[0] = @"BYGE.Integration.UBLInvoice.UBLInvoice";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"BYGE.Integration.UBLInvoice.UBLInvoice";
                return _TrgSchemas;
            }
        }
    }
}
