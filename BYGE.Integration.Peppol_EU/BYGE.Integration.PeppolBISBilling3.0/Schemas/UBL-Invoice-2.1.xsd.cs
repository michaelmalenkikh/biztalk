namespace BYGE.Integration.PeppolBISBilling3._0.Schemas {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:Invoice-2",@"Invoice")]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"Invoice"})]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_CommonAggregateComponents_2_1", typeof(global::BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_CommonAggregateComponents_2_1))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_CommonBasicComponents_2_1", typeof(global::BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_CommonBasicComponents_2_1))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_CommonExtensionComponents_2_1", typeof(global::BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_CommonExtensionComponents_2_1))]
    public sealed class UBL_Invoice_2_1 : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xsd:schema xmlns:cac=""urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"" xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" xmlns=""urn:oasis:names:specification:ubl:schema:xsd:Invoice-2"" xmlns:ext=""urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2"" xmlns:cbc=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" xmlns:ccts=""urn:un:unece:uncefact:documentation:2"" attributeFormDefault=""unqualified"" elementFormDefault=""qualified"" targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:Invoice-2"" version=""2.1"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
  <xsd:import schemaLocation=""BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_CommonAggregateComponents_2_1"" namespace=""urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"" />
  <xsd:import schemaLocation=""BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_CommonBasicComponents_2_1"" namespace=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" />
  <xsd:import schemaLocation=""BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_CommonExtensionComponents_2_1"" namespace=""urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2"" />
  <xsd:annotation>
    <xsd:appinfo>
      <references xmlns=""http://schemas.microsoft.com/BizTalk/2003"">
        <reference targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" />
        <reference targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:CommonSignatureComponents-2"" />
        <reference targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"" />
        <reference targetNamespace=""http://uri.etsi.org/01903/v1.4.1#"" />
        <reference targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:SignatureAggregateComponents-2"" />
        <reference targetNamespace=""urn:un:unece:uncefact:data:specification:CoreComponentTypeSchemaModule:2"" />
        <reference targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:UnqualifiedDataTypes-2"" />
        <reference targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:QualifiedDataTypes-2"" />
        <reference targetNamespace=""http://www.w3.org/2000/09/xmldsig#"" />
        <reference targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:SignatureBasicComponents-2"" />
        <reference targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2"" />
        <reference targetNamespace=""http://uri.etsi.org/01903/v1.3.2#"" />
      </references>
    </xsd:appinfo>
  </xsd:annotation>
  <xsd:element name=""Invoice"" type=""InvoiceType"">
    <xsd:annotation>
      <xsd:documentation>This element MUST be conveyed as the root element in any instance document based on this Schema expression</xsd:documentation>
    </xsd:annotation>
  </xsd:element>
  <xsd:complexType name=""InvoiceType"">
    <xsd:annotation>
      <xsd:documentation>
        <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
          <ccts:ComponentType>ABIE</ccts:ComponentType>
          <ccts:DictionaryEntryName>Invoice. Details</ccts:DictionaryEntryName>
          <ccts:Definition>A document used to request payment.</ccts:Definition>
          <ccts:ObjectClass>Invoice</ccts:ObjectClass>
        </ccts:Component>
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""ext:UBLExtensions"">
        <xsd:annotation>
          <xsd:documentation>A container for all extensions present in the document.</xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""cbc:UBLVersionID"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>BBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. UBL Version Identifier. Identifier</ccts:DictionaryEntryName>
              <ccts:Definition>Identifies the earliest version of the UBL 2 schema for this document type that defines all of the elements that might be encountered in the current instance.</ccts:Definition>
              <ccts:Cardinality>0..1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTerm>UBL Version Identifier</ccts:PropertyTerm>
              <ccts:RepresentationTerm>Identifier</ccts:RepresentationTerm>
              <ccts:DataType>Identifier. Type</ccts:DataType>
              <ccts:Examples>2.0.5</ccts:Examples>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""cbc:CustomizationID"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>BBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Customization Identifier. Identifier</ccts:DictionaryEntryName>
              <ccts:Definition>Identifies a user-defined customization of UBL for a specific use.</ccts:Definition>
              <ccts:Cardinality>0..1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTerm>Customization Identifier</ccts:PropertyTerm>
              <ccts:RepresentationTerm>Identifier</ccts:RepresentationTerm>
              <ccts:DataType>Identifier. Type</ccts:DataType>
              <ccts:Examples>NES</ccts:Examples>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""cbc:ProfileID"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>BBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Profile Identifier. Identifier</ccts:DictionaryEntryName>
              <ccts:Definition>Identifies a user-defined profile of the customization of UBL being used.</ccts:Definition>
              <ccts:Cardinality>0..1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTerm>Profile Identifier</ccts:PropertyTerm>
              <ccts:RepresentationTerm>Identifier</ccts:RepresentationTerm>
              <ccts:DataType>Identifier. Type</ccts:DataType>
              <ccts:Examples>BasicProcurementProcess</ccts:Examples>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""cbc:ProfileExecutionID"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>BBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Profile Execution Identifier. Identifier</ccts:DictionaryEntryName>
              <ccts:Definition>Identifies an instance of executing a profile, to associate all transactions in a collaboration.</ccts:Definition>
              <ccts:Cardinality>0..1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTerm>Profile Execution Identifier</ccts:PropertyTerm>
              <ccts:RepresentationTerm>Identifier</ccts:RepresentationTerm>
              <ccts:DataType>Identifier. Type</ccts:DataType>
              <ccts:Examples>BPP-1001</ccts:Examples>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""1"" maxOccurs=""1"" ref=""cbc:ID"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>BBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Identifier</ccts:DictionaryEntryName>
              <ccts:Definition>An identifier for this document, assigned by the sender.</ccts:Definition>
              <ccts:Cardinality>1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTerm>Identifier</ccts:PropertyTerm>
              <ccts:RepresentationTerm>Identifier</ccts:RepresentationTerm>
              <ccts:DataType>Identifier. Type</ccts:DataType>
              <ccts:AlternativeBusinessTerms>Invoice Number</ccts:AlternativeBusinessTerms>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""cbc:CopyIndicator"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>BBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Copy_ Indicator. Indicator</ccts:DictionaryEntryName>
              <ccts:Definition>Indicates whether this document is a copy (true) or not (false).</ccts:Definition>
              <ccts:Cardinality>0..1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTermQualifier>Copy</ccts:PropertyTermQualifier>
              <ccts:PropertyTerm>Indicator</ccts:PropertyTerm>
              <ccts:RepresentationTerm>Indicator</ccts:RepresentationTerm>
              <ccts:DataType>Indicator. Type</ccts:DataType>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""cbc:UUID"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>BBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. UUID. Identifier</ccts:DictionaryEntryName>
              <ccts:Definition>A universally unique identifier for an instance of this document.</ccts:Definition>
              <ccts:Cardinality>0..1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTerm>UUID</ccts:PropertyTerm>
              <ccts:RepresentationTerm>Identifier</ccts:RepresentationTerm>
              <ccts:DataType>Identifier. Type</ccts:DataType>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""1"" maxOccurs=""1"" ref=""cbc:IssueDate"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>BBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Issue Date. Date</ccts:DictionaryEntryName>
              <ccts:Definition>The date, assigned by the sender, on which this document was issued.</ccts:Definition>
              <ccts:Cardinality>1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTerm>Issue Date</ccts:PropertyTerm>
              <ccts:RepresentationTerm>Date</ccts:RepresentationTerm>
              <ccts:DataType>Date. Type</ccts:DataType>
              <ccts:AlternativeBusinessTerms>Invoice Date</ccts:AlternativeBusinessTerms>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""cbc:IssueTime"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>BBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Issue Time. Time</ccts:DictionaryEntryName>
              <ccts:Definition>The time, assigned by the sender, at which this document was issued.</ccts:Definition>
              <ccts:Cardinality>0..1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTerm>Issue Time</ccts:PropertyTerm>
              <ccts:RepresentationTerm>Time</ccts:RepresentationTerm>
              <ccts:DataType>Time. Type</ccts:DataType>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""cbc:DueDate"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>BBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Due Date. Date</ccts:DictionaryEntryName>
              <ccts:Definition>The date on which Invoice is due.</ccts:Definition>
              <ccts:Cardinality>0..1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTerm>Due Date</ccts:PropertyTerm>
              <ccts:RepresentationTerm>Date</ccts:RepresentationTerm>
              <ccts:DataType>Date. Type</ccts:DataType>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""cbc:InvoiceTypeCode"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>BBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Invoice Type Code. Code</ccts:DictionaryEntryName>
              <ccts:Definition>A code signifying the type of the Invoice.</ccts:Definition>
              <ccts:Cardinality>0..1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTerm>Invoice Type Code</ccts:PropertyTerm>
              <ccts:RepresentationTerm>Code</ccts:RepresentationTerm>
              <ccts:DataType>Code. Type</ccts:DataType>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" ref=""cbc:Note"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>BBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Note. Text</ccts:DictionaryEntryName>
              <ccts:Definition>Free-form text pertinent to this document, conveying information that is not contained explicitly in other structures.</ccts:Definition>
              <ccts:Cardinality>0..n</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTerm>Note</ccts:PropertyTerm>
              <ccts:RepresentationTerm>Text</ccts:RepresentationTerm>
              <ccts:DataType>Text. Type</ccts:DataType>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""cbc:TaxPointDate"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>BBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Tax Point Date. Date</ccts:DictionaryEntryName>
              <ccts:Definition>The date of the Invoice, used to indicate the point at which tax becomes applicable.</ccts:Definition>
              <ccts:Cardinality>0..1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTerm>Tax Point Date</ccts:PropertyTerm>
              <ccts:RepresentationTerm>Date</ccts:RepresentationTerm>
              <ccts:DataType>Date. Type</ccts:DataType>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""cbc:DocumentCurrencyCode"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>BBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Document_ Currency Code. Code</ccts:DictionaryEntryName>
              <ccts:Definition>A code signifying the default currency for this document.</ccts:Definition>
              <ccts:Cardinality>0..1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTermQualifier>Document</ccts:PropertyTermQualifier>
              <ccts:PropertyTerm>Currency Code</ccts:PropertyTerm>
              <ccts:RepresentationTerm>Code</ccts:RepresentationTerm>
              <ccts:DataTypeQualifier>Currency</ccts:DataTypeQualifier>
              <ccts:DataType>Currency_ Code. Type</ccts:DataType>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""cbc:TaxCurrencyCode"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>BBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Tax_ Currency Code. Code</ccts:DictionaryEntryName>
              <ccts:Definition>A code signifying the currency used for tax amounts in the Invoice.</ccts:Definition>
              <ccts:Cardinality>0..1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTermQualifier>Tax</ccts:PropertyTermQualifier>
              <ccts:PropertyTerm>Currency Code</ccts:PropertyTerm>
              <ccts:RepresentationTerm>Code</ccts:RepresentationTerm>
              <ccts:DataTypeQualifier>Currency</ccts:DataTypeQualifier>
              <ccts:DataType>Currency_ Code. Type</ccts:DataType>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""cbc:PricingCurrencyCode"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>BBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Pricing_ Currency Code. Code</ccts:DictionaryEntryName>
              <ccts:Definition>A code signifying the currency used for prices in the Invoice.</ccts:Definition>
              <ccts:Cardinality>0..1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTermQualifier>Pricing</ccts:PropertyTermQualifier>
              <ccts:PropertyTerm>Currency Code</ccts:PropertyTerm>
              <ccts:RepresentationTerm>Code</ccts:RepresentationTerm>
              <ccts:DataTypeQualifier>Currency</ccts:DataTypeQualifier>
              <ccts:DataType>Currency_ Code. Type</ccts:DataType>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""cbc:PaymentCurrencyCode"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>BBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Payment_ Currency Code. Code</ccts:DictionaryEntryName>
              <ccts:Definition>A code signifying the currency used for payment in the Invoice.</ccts:Definition>
              <ccts:Cardinality>0..1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTermQualifier>Payment</ccts:PropertyTermQualifier>
              <ccts:PropertyTerm>Currency Code</ccts:PropertyTerm>
              <ccts:RepresentationTerm>Code</ccts:RepresentationTerm>
              <ccts:DataTypeQualifier>Currency</ccts:DataTypeQualifier>
              <ccts:DataType>Currency_ Code. Type</ccts:DataType>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""cbc:PaymentAlternativeCurrencyCode"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>BBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Payment Alternative_ Currency Code. Code</ccts:DictionaryEntryName>
              <ccts:Definition>A code signifying the alternative currency used for payment in the Invoice.</ccts:Definition>
              <ccts:Cardinality>0..1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTermQualifier>Payment Alternative</ccts:PropertyTermQualifier>
              <ccts:PropertyTerm>Currency Code</ccts:PropertyTerm>
              <ccts:RepresentationTerm>Code</ccts:RepresentationTerm>
              <ccts:DataTypeQualifier>Currency</ccts:DataTypeQualifier>
              <ccts:DataType>Currency_ Code. Type</ccts:DataType>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""cbc:AccountingCostCode"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>BBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Accounting Cost Code. Code</ccts:DictionaryEntryName>
              <ccts:Definition>The buyer's accounting code, applied to the Invoice as a whole.</ccts:Definition>
              <ccts:Cardinality>0..1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTerm>Accounting Cost Code</ccts:PropertyTerm>
              <ccts:RepresentationTerm>Code</ccts:RepresentationTerm>
              <ccts:DataType>Code. Type</ccts:DataType>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""cbc:AccountingCost"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>BBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Accounting Cost. Text</ccts:DictionaryEntryName>
              <ccts:Definition>The buyer's accounting code, applied to the Invoice as a whole, expressed as text.</ccts:Definition>
              <ccts:Cardinality>0..1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTerm>Accounting Cost</ccts:PropertyTerm>
              <ccts:RepresentationTerm>Text</ccts:RepresentationTerm>
              <ccts:DataType>Text. Type</ccts:DataType>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""cbc:LineCountNumeric"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>BBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Line Count. Numeric</ccts:DictionaryEntryName>
              <ccts:Definition>The number of lines in the document.</ccts:Definition>
              <ccts:Cardinality>0..1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTerm>Line Count</ccts:PropertyTerm>
              <ccts:RepresentationTerm>Numeric</ccts:RepresentationTerm>
              <ccts:DataType>Numeric. Type</ccts:DataType>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""cbc:BuyerReference"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>BBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Buyer_ Reference. Text</ccts:DictionaryEntryName>
              <ccts:Definition>A reference provided by the buyer used for internal routing of the document.</ccts:Definition>
              <ccts:Cardinality>0..1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTermQualifier>Buyer</ccts:PropertyTermQualifier>
              <ccts:PropertyTerm>Reference</ccts:PropertyTerm>
              <ccts:RepresentationTerm>Text</ccts:RepresentationTerm>
              <ccts:DataType>Text. Type</ccts:DataType>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" ref=""cac:InvoicePeriod"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>ASBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Invoice_ Period. Period</ccts:DictionaryEntryName>
              <ccts:Definition>A period to which the Invoice applies.</ccts:Definition>
              <ccts:Cardinality>0..n</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTermQualifier>Invoice</ccts:PropertyTermQualifier>
              <ccts:PropertyTerm>Period</ccts:PropertyTerm>
              <ccts:AssociatedObjectClass>Period</ccts:AssociatedObjectClass>
              <ccts:RepresentationTerm>Period</ccts:RepresentationTerm>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""cac:OrderReference"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>ASBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Order Reference</ccts:DictionaryEntryName>
              <ccts:Definition>A reference to the Order with which this Invoice is associated.</ccts:Definition>
              <ccts:Cardinality>0..1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTerm>Order Reference</ccts:PropertyTerm>
              <ccts:AssociatedObjectClass>Order Reference</ccts:AssociatedObjectClass>
              <ccts:RepresentationTerm>Order Reference</ccts:RepresentationTerm>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" ref=""cac:BillingReference"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>ASBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Billing Reference</ccts:DictionaryEntryName>
              <ccts:Definition>A reference to a billing document associated with this document.</ccts:Definition>
              <ccts:Cardinality>0..n</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTerm>Billing Reference</ccts:PropertyTerm>
              <ccts:AssociatedObjectClass>Billing Reference</ccts:AssociatedObjectClass>
              <ccts:RepresentationTerm>Billing Reference</ccts:RepresentationTerm>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" ref=""cac:DespatchDocumentReference"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>ASBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Despatch_ Document Reference. Document Reference</ccts:DictionaryEntryName>
              <ccts:Definition>A reference to a Despatch Advice associated with this document.</ccts:Definition>
              <ccts:Cardinality>0..n</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTermQualifier>Despatch</ccts:PropertyTermQualifier>
              <ccts:PropertyTerm>Document Reference</ccts:PropertyTerm>
              <ccts:AssociatedObjectClass>Document Reference</ccts:AssociatedObjectClass>
              <ccts:RepresentationTerm>Document Reference</ccts:RepresentationTerm>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" ref=""cac:ReceiptDocumentReference"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>ASBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Receipt_ Document Reference. Document Reference</ccts:DictionaryEntryName>
              <ccts:Definition>A reference to a Receipt Advice associated with this document.</ccts:Definition>
              <ccts:Cardinality>0..n</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTermQualifier>Receipt</ccts:PropertyTermQualifier>
              <ccts:PropertyTerm>Document Reference</ccts:PropertyTerm>
              <ccts:AssociatedObjectClass>Document Reference</ccts:AssociatedObjectClass>
              <ccts:RepresentationTerm>Document Reference</ccts:RepresentationTerm>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" ref=""cac:StatementDocumentReference"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>ASBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Statement_ Document Reference. Document Reference</ccts:DictionaryEntryName>
              <ccts:Definition>A reference to a Statement associated with this document.</ccts:Definition>
              <ccts:Cardinality>0..n</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTermQualifier>Statement</ccts:PropertyTermQualifier>
              <ccts:PropertyTerm>Document Reference</ccts:PropertyTerm>
              <ccts:AssociatedObjectClass>Document Reference</ccts:AssociatedObjectClass>
              <ccts:RepresentationTerm>Document Reference</ccts:RepresentationTerm>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" ref=""cac:OriginatorDocumentReference"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>ASBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Originator_ Document Reference. Document Reference</ccts:DictionaryEntryName>
              <ccts:Definition>A reference to an originator document associated with this document.</ccts:Definition>
              <ccts:Cardinality>0..n</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTermQualifier>Originator</ccts:PropertyTermQualifier>
              <ccts:PropertyTerm>Document Reference</ccts:PropertyTerm>
              <ccts:AssociatedObjectClass>Document Reference</ccts:AssociatedObjectClass>
              <ccts:RepresentationTerm>Document Reference</ccts:RepresentationTerm>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" ref=""cac:ContractDocumentReference"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>ASBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Contract_ Document Reference. Document Reference</ccts:DictionaryEntryName>
              <ccts:Definition>A reference to a contract associated with this document.</ccts:Definition>
              <ccts:Cardinality>0..n</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTermQualifier>Contract</ccts:PropertyTermQualifier>
              <ccts:PropertyTerm>Document Reference</ccts:PropertyTerm>
              <ccts:AssociatedObjectClass>Document Reference</ccts:AssociatedObjectClass>
              <ccts:RepresentationTerm>Document Reference</ccts:RepresentationTerm>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" ref=""cac:AdditionalDocumentReference"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>ASBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Additional_ Document Reference. Document Reference</ccts:DictionaryEntryName>
              <ccts:Definition>A reference to an additional document associated with this document.</ccts:Definition>
              <ccts:Cardinality>0..n</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTermQualifier>Additional</ccts:PropertyTermQualifier>
              <ccts:PropertyTerm>Document Reference</ccts:PropertyTerm>
              <ccts:AssociatedObjectClass>Document Reference</ccts:AssociatedObjectClass>
              <ccts:RepresentationTerm>Document Reference</ccts:RepresentationTerm>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" ref=""cac:ProjectReference"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>ASBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Project Reference</ccts:DictionaryEntryName>
              <ccts:Definition>Information about a project.</ccts:Definition>
              <ccts:Cardinality>0..n</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTerm>Project Reference</ccts:PropertyTerm>
              <ccts:AssociatedObjectClass>Project Reference</ccts:AssociatedObjectClass>
              <ccts:RepresentationTerm>Project Reference</ccts:RepresentationTerm>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" ref=""cac:Signature"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>ASBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Signature</ccts:DictionaryEntryName>
              <ccts:Definition>A signature applied to this document.</ccts:Definition>
              <ccts:Cardinality>0..n</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTerm>Signature</ccts:PropertyTerm>
              <ccts:AssociatedObjectClass>Signature</ccts:AssociatedObjectClass>
              <ccts:RepresentationTerm>Signature</ccts:RepresentationTerm>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""1"" maxOccurs=""1"" ref=""cac:AccountingSupplierParty"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>ASBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Accounting_ Supplier Party. Supplier Party</ccts:DictionaryEntryName>
              <ccts:Definition>The accounting supplier party.</ccts:Definition>
              <ccts:Cardinality>1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTermQualifier>Accounting</ccts:PropertyTermQualifier>
              <ccts:PropertyTerm>Supplier Party</ccts:PropertyTerm>
              <ccts:AssociatedObjectClass>Supplier Party</ccts:AssociatedObjectClass>
              <ccts:RepresentationTerm>Supplier Party</ccts:RepresentationTerm>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""1"" maxOccurs=""1"" ref=""cac:AccountingCustomerParty"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>ASBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Accounting_ Customer Party. Customer Party</ccts:DictionaryEntryName>
              <ccts:Definition>The accounting customer party.</ccts:Definition>
              <ccts:Cardinality>1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTermQualifier>Accounting</ccts:PropertyTermQualifier>
              <ccts:PropertyTerm>Customer Party</ccts:PropertyTerm>
              <ccts:AssociatedObjectClass>Customer Party</ccts:AssociatedObjectClass>
              <ccts:RepresentationTerm>Customer Party</ccts:RepresentationTerm>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""cac:PayeeParty"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>ASBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Payee_ Party. Party</ccts:DictionaryEntryName>
              <ccts:Definition>The payee.</ccts:Definition>
              <ccts:Cardinality>0..1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTermQualifier>Payee</ccts:PropertyTermQualifier>
              <ccts:PropertyTerm>Party</ccts:PropertyTerm>
              <ccts:AssociatedObjectClass>Party</ccts:AssociatedObjectClass>
              <ccts:RepresentationTerm>Party</ccts:RepresentationTerm>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""cac:BuyerCustomerParty"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>ASBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Buyer_ Customer Party. Customer Party</ccts:DictionaryEntryName>
              <ccts:Definition>The buyer.</ccts:Definition>
              <ccts:Cardinality>0..1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTermQualifier>Buyer</ccts:PropertyTermQualifier>
              <ccts:PropertyTerm>Customer Party</ccts:PropertyTerm>
              <ccts:AssociatedObjectClass>Customer Party</ccts:AssociatedObjectClass>
              <ccts:RepresentationTerm>Customer Party</ccts:RepresentationTerm>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""cac:SellerSupplierParty"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>ASBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Seller_ Supplier Party. Supplier Party</ccts:DictionaryEntryName>
              <ccts:Definition>The seller.</ccts:Definition>
              <ccts:Cardinality>0..1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTermQualifier>Seller</ccts:PropertyTermQualifier>
              <ccts:PropertyTerm>Supplier Party</ccts:PropertyTerm>
              <ccts:AssociatedObjectClass>Supplier Party</ccts:AssociatedObjectClass>
              <ccts:RepresentationTerm>Supplier Party</ccts:RepresentationTerm>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""cac:TaxRepresentativeParty"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>ASBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Tax Representative_ Party. Party</ccts:DictionaryEntryName>
              <ccts:Definition>The tax representative.</ccts:Definition>
              <ccts:Cardinality>0..1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTermQualifier>Tax Representative</ccts:PropertyTermQualifier>
              <ccts:PropertyTerm>Party</ccts:PropertyTerm>
              <ccts:AssociatedObjectClass>Party</ccts:AssociatedObjectClass>
              <ccts:RepresentationTerm>Party</ccts:RepresentationTerm>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" ref=""cac:Delivery"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>ASBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Delivery</ccts:DictionaryEntryName>
              <ccts:Definition>A delivery associated with this document.</ccts:Definition>
              <ccts:Cardinality>0..n</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTerm>Delivery</ccts:PropertyTerm>
              <ccts:AssociatedObjectClass>Delivery</ccts:AssociatedObjectClass>
              <ccts:RepresentationTerm>Delivery</ccts:RepresentationTerm>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""cac:DeliveryTerms"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>ASBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Delivery Terms</ccts:DictionaryEntryName>
              <ccts:Definition>A set of delivery terms associated with this document.</ccts:Definition>
              <ccts:Cardinality>0..1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTerm>Delivery Terms</ccts:PropertyTerm>
              <ccts:AssociatedObjectClass>Delivery Terms</ccts:AssociatedObjectClass>
              <ccts:RepresentationTerm>Delivery Terms</ccts:RepresentationTerm>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" ref=""cac:PaymentMeans"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>ASBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Payment Means</ccts:DictionaryEntryName>
              <ccts:Definition>Expected means of payment.</ccts:Definition>
              <ccts:Cardinality>0..n</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTerm>Payment Means</ccts:PropertyTerm>
              <ccts:AssociatedObjectClass>Payment Means</ccts:AssociatedObjectClass>
              <ccts:RepresentationTerm>Payment Means</ccts:RepresentationTerm>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" ref=""cac:PaymentTerms"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>ASBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Payment Terms</ccts:DictionaryEntryName>
              <ccts:Definition>A set of payment terms associated with this document.</ccts:Definition>
              <ccts:Cardinality>0..n</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTerm>Payment Terms</ccts:PropertyTerm>
              <ccts:AssociatedObjectClass>Payment Terms</ccts:AssociatedObjectClass>
              <ccts:RepresentationTerm>Payment Terms</ccts:RepresentationTerm>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" ref=""cac:PrepaidPayment"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>ASBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Prepaid_ Payment. Payment</ccts:DictionaryEntryName>
              <ccts:Definition>A prepaid payment.</ccts:Definition>
              <ccts:Cardinality>0..n</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTermQualifier>Prepaid</ccts:PropertyTermQualifier>
              <ccts:PropertyTerm>Payment</ccts:PropertyTerm>
              <ccts:AssociatedObjectClass>Payment</ccts:AssociatedObjectClass>
              <ccts:RepresentationTerm>Payment</ccts:RepresentationTerm>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" ref=""cac:AllowanceCharge"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>ASBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Allowance Charge</ccts:DictionaryEntryName>
              <ccts:Definition>A discount or charge that applies to a price component.</ccts:Definition>
              <ccts:Cardinality>0..n</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTerm>Allowance Charge</ccts:PropertyTerm>
              <ccts:AssociatedObjectClass>Allowance Charge</ccts:AssociatedObjectClass>
              <ccts:RepresentationTerm>Allowance Charge</ccts:RepresentationTerm>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""cac:TaxExchangeRate"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>ASBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Tax_ Exchange Rate. Exchange Rate</ccts:DictionaryEntryName>
              <ccts:Definition>The exchange rate between the document currency and the tax currency.</ccts:Definition>
              <ccts:Cardinality>0..1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTermQualifier>Tax</ccts:PropertyTermQualifier>
              <ccts:PropertyTerm>Exchange Rate</ccts:PropertyTerm>
              <ccts:AssociatedObjectClass>Exchange Rate</ccts:AssociatedObjectClass>
              <ccts:RepresentationTerm>Exchange Rate</ccts:RepresentationTerm>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""cac:PricingExchangeRate"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>ASBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Pricing_ Exchange Rate. Exchange Rate</ccts:DictionaryEntryName>
              <ccts:Definition>The exchange rate between the document currency and the pricing currency.</ccts:Definition>
              <ccts:Cardinality>0..1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTermQualifier>Pricing</ccts:PropertyTermQualifier>
              <ccts:PropertyTerm>Exchange Rate</ccts:PropertyTerm>
              <ccts:AssociatedObjectClass>Exchange Rate</ccts:AssociatedObjectClass>
              <ccts:RepresentationTerm>Exchange Rate</ccts:RepresentationTerm>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""cac:PaymentExchangeRate"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>ASBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Payment_ Exchange Rate. Exchange Rate</ccts:DictionaryEntryName>
              <ccts:Definition>The exchange rate between the document currency and the payment currency.</ccts:Definition>
              <ccts:Cardinality>0..1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTermQualifier>Payment</ccts:PropertyTermQualifier>
              <ccts:PropertyTerm>Exchange Rate</ccts:PropertyTerm>
              <ccts:AssociatedObjectClass>Exchange Rate</ccts:AssociatedObjectClass>
              <ccts:RepresentationTerm>Exchange Rate</ccts:RepresentationTerm>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""cac:PaymentAlternativeExchangeRate"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>ASBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Payment Alternative_ Exchange Rate. Exchange Rate</ccts:DictionaryEntryName>
              <ccts:Definition>The exchange rate between the document currency and the payment alternative currency.</ccts:Definition>
              <ccts:Cardinality>0..1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTermQualifier>Payment Alternative</ccts:PropertyTermQualifier>
              <ccts:PropertyTerm>Exchange Rate</ccts:PropertyTerm>
              <ccts:AssociatedObjectClass>Exchange Rate</ccts:AssociatedObjectClass>
              <ccts:RepresentationTerm>Exchange Rate</ccts:RepresentationTerm>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" ref=""cac:TaxTotal"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>ASBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Tax Total</ccts:DictionaryEntryName>
              <ccts:Definition>The total amount of a specific type of tax.</ccts:Definition>
              <ccts:Cardinality>0..n</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTerm>Tax Total</ccts:PropertyTerm>
              <ccts:AssociatedObjectClass>Tax Total</ccts:AssociatedObjectClass>
              <ccts:RepresentationTerm>Tax Total</ccts:RepresentationTerm>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" ref=""cac:WithholdingTaxTotal"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>ASBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Withholding_ Tax Total. Tax Total</ccts:DictionaryEntryName>
              <ccts:Definition>The total withholding tax.</ccts:Definition>
              <ccts:Cardinality>0..n</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTermQualifier>Withholding</ccts:PropertyTermQualifier>
              <ccts:PropertyTerm>Tax Total</ccts:PropertyTerm>
              <ccts:AssociatedObjectClass>Tax Total</ccts:AssociatedObjectClass>
              <ccts:RepresentationTerm>Tax Total</ccts:RepresentationTerm>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""1"" maxOccurs=""1"" ref=""cac:LegalMonetaryTotal"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>ASBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Legal_ Monetary Total. Monetary Total</ccts:DictionaryEntryName>
              <ccts:Definition>The total amount payable on the Invoice, including Allowances, Charges, and Taxes.</ccts:Definition>
              <ccts:Cardinality>1</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTermQualifier>Legal</ccts:PropertyTermQualifier>
              <ccts:PropertyTerm>Monetary Total</ccts:PropertyTerm>
              <ccts:AssociatedObjectClass>Monetary Total</ccts:AssociatedObjectClass>
              <ccts:RepresentationTerm>Monetary Total</ccts:RepresentationTerm>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""1"" maxOccurs=""unbounded"" ref=""cac:InvoiceLine"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>ASBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Invoice. Invoice Line</ccts:DictionaryEntryName>
              <ccts:Definition>A line describing an invoice item.</ccts:Definition>
              <ccts:Cardinality>1..n</ccts:Cardinality>
              <ccts:ObjectClass>Invoice</ccts:ObjectClass>
              <ccts:PropertyTerm>Invoice Line</ccts:PropertyTerm>
              <ccts:AssociatedObjectClass>Invoice Line</ccts:AssociatedObjectClass>
              <ccts:RepresentationTerm>Invoice Line</ccts:RepresentationTerm>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
</xsd:schema>";
        
        public UBL_Invoice_2_1() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [1];
                _RootElements[0] = "Invoice";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
    }
}
