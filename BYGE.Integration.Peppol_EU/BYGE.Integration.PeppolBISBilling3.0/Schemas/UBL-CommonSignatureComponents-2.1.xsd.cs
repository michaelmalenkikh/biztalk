namespace BYGE.Integration.PeppolBISBilling3._0.Schemas {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonSignatureComponents-2",@"UBLDocumentSignatures")]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"UBLDocumentSignatures"})]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_SignatureAggregateComponents_2_1", typeof(global::BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_SignatureAggregateComponents_2_1))]
    public sealed class UBL_CommonSignatureComponents_2_1 : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xsd:schema xmlns=""urn:oasis:names:specification:ubl:schema:xsd:CommonSignatureComponents-2"" xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" xmlns:sac=""urn:oasis:names:specification:ubl:schema:xsd:SignatureAggregateComponents-2"" xmlns:ccts=""urn:un:unece:uncefact:documentation:2"" attributeFormDefault=""unqualified"" elementFormDefault=""qualified"" targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:CommonSignatureComponents-2"" version=""2.1"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
  <xsd:import schemaLocation=""BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_SignatureAggregateComponents_2_1"" namespace=""urn:oasis:names:specification:ubl:schema:xsd:SignatureAggregateComponents-2"" />
  <xsd:annotation>
    <xsd:appinfo>
      <references xmlns=""http://schemas.microsoft.com/BizTalk/2003"">
        <reference targetNamespace=""http://www.w3.org/2000/09/xmldsig#"" />
        <reference targetNamespace=""http://uri.etsi.org/01903/v1.3.2#"" />
        <reference targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" />
        <reference targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:UnqualifiedDataTypes-2"" />
        <reference targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:QualifiedDataTypes-2"" />
        <reference targetNamespace=""urn:un:unece:uncefact:data:specification:CoreComponentTypeSchemaModule:2"" />
        <reference targetNamespace=""http://uri.etsi.org/01903/v1.4.1#"" />
        <reference targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:SignatureAggregateComponents-2"" />
        <reference targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:SignatureBasicComponents-2"" />
      </references>
    </xsd:appinfo>
  </xsd:annotation>
  <xsd:element name=""UBLDocumentSignatures"" type=""UBLDocumentSignaturesType"" />
  <xsd:complexType name=""UBLDocumentSignaturesType"">
    <xsd:annotation>
      <xsd:documentation>
        <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
          <ccts:ComponentType>ABIE</ccts:ComponentType>
          <ccts:DictionaryEntryName>UBL Document Signatures. Details</ccts:DictionaryEntryName>
          <ccts:Definition>This class collects all signature information for a document.</ccts:Definition>
          <ccts:ObjectClass>UBL Document Signatures</ccts:ObjectClass>
        </ccts:Component>
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs=""1"" maxOccurs=""unbounded"" ref=""sac:SignatureInformation"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>ASBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>UBL Document Signatures. Signature Information</ccts:DictionaryEntryName>
              <ccts:Definition>Each of these is scaffolding for a single digital signature.</ccts:Definition>
              <ccts:Cardinality>1..n</ccts:Cardinality>
              <ccts:ObjectClass>UBL Document Signatures</ccts:ObjectClass>
              <ccts:PropertyTerm>Signature Information</ccts:PropertyTerm>
              <ccts:AssociatedObjectClass>Signature Information</ccts:AssociatedObjectClass>
              <ccts:RepresentationTerm>Signature Information</ccts:RepresentationTerm>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
</xsd:schema>";
        
        public UBL_CommonSignatureComponents_2_1() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [1];
                _RootElements[0] = "UBLDocumentSignatures";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
    }
}
