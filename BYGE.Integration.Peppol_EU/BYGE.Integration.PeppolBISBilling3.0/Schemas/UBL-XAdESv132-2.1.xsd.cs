namespace BYGE.Integration.PeppolBISBilling3._0.Schemas {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"Any", @"ObjectIdentifier", @"EncapsulatedPKIData", @"Include", @"ReferenceInfo", @"XAdESTimeStamp", @"OtherTimeStamp", @"QualifyingProperties", @"SignedProperties", @"UnsignedProperties", @"SignedSignatureProperties", @"SignedDataObjectProperties", @"UnsignedSignatureProperties", @"UnsignedDataObjectProperties", @"QualifyingPropertiesReference", @"SigningTime", @"SigningCertificate", @"SignaturePolicyIdentifier", @"SPURI", @"SPUserNotice", @"CounterSignature", @"DataObjectFormat", 
@"CommitmentTypeIndication", @"SignatureProductionPlace", @"SignerRole", @"AllDataObjectsTimeStamp", @"IndividualDataObjectsTimeStamp", @"SignatureTimeStamp", @"CompleteCertificateRefs", @"CompleteRevocationRefs", @"AttributeCertificateRefs", @"AttributeRevocationRefs", @"SigAndRefsTimeStamp", @"RefsOnlyTimeStamp", @"CertificateValues", @"RevocationValues", @"AttrAuthoritiesCertValues", @"AttributeRevocationValues", @"ArchiveTimeStamp"})]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_xmldsig_core_schema_2_1", typeof(global::BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_xmldsig_core_schema_2_1))]
    public sealed class UBL_XAdESv132_2_1 : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xsd:schema xmlns=""http://uri.etsi.org/01903/v1.3.2#"" xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" xmlns:ds=""http://www.w3.org/2000/09/xmldsig#"" elementFormDefault=""qualified"" targetNamespace=""http://uri.etsi.org/01903/v1.3.2#"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
  <xsd:import schemaLocation=""BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_xmldsig_core_schema_2_1"" namespace=""http://www.w3.org/2000/09/xmldsig#"" />
  <xsd:annotation>
    <xsd:appinfo>
      <references xmlns=""http://schemas.microsoft.com/BizTalk/2003"">
        <reference targetNamespace=""http://www.w3.org/2000/09/xmldsig#"" />
      </references>
    </xsd:appinfo>
  </xsd:annotation>
  <xsd:element name=""Any"" type=""AnyType"" />
  <xsd:complexType name=""AnyType"" mixed=""true"">
    <xsd:sequence minOccurs=""0"" maxOccurs=""unbounded"">
      <xsd:any namespace=""##any"" processContents=""lax"" />
    </xsd:sequence>
    <xsd:anyAttribute namespace=""##any"" />
  </xsd:complexType>
  <xsd:element name=""ObjectIdentifier"" type=""ObjectIdentifierType"" />
  <xsd:complexType name=""ObjectIdentifierType"">
    <xsd:sequence>
      <xsd:element name=""Identifier"" type=""IdentifierType"" />
      <xsd:element minOccurs=""0"" name=""Description"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DocumentationReferences"" type=""DocumentationReferencesType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""IdentifierType"">
    <xsd:simpleContent>
      <xsd:extension base=""xsd:anyURI"">
        <xsd:attribute name=""Qualifier"" type=""QualifierType"" use=""optional"" />
      </xsd:extension>
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:simpleType name=""QualifierType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:enumeration value=""OIDAsURI"" />
      <xsd:enumeration value=""OIDAsURN"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:complexType name=""DocumentationReferencesType"">
    <xsd:sequence maxOccurs=""unbounded"">
      <xsd:element name=""DocumentationReference"" type=""xsd:anyURI"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:element name=""EncapsulatedPKIData"" type=""EncapsulatedPKIDataType"" />
  <xsd:complexType name=""EncapsulatedPKIDataType"">
    <xsd:simpleContent>
      <xsd:extension base=""xsd:base64Binary"">
        <xsd:attribute name=""Id"" type=""xsd:ID"" use=""optional"" />
        <xsd:attribute name=""Encoding"" type=""xsd:anyURI"" use=""optional"" />
      </xsd:extension>
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:element name=""Include"" type=""IncludeType"" />
  <xsd:complexType name=""IncludeType"">
    <xsd:attribute name=""URI"" type=""xsd:anyURI"" use=""required"" />
    <xsd:attribute name=""referencedData"" type=""xsd:boolean"" use=""optional"" />
  </xsd:complexType>
  <xsd:element name=""ReferenceInfo"" type=""ReferenceInfoType"" />
  <xsd:complexType name=""ReferenceInfoType"">
    <xsd:sequence>
      <xsd:element ref=""ds:DigestMethod"" />
      <xsd:element ref=""ds:DigestValue"" />
    </xsd:sequence>
    <xsd:attribute name=""Id"" type=""xsd:ID"" use=""optional"" />
    <xsd:attribute name=""URI"" type=""xsd:anyURI"" use=""optional"" />
  </xsd:complexType>
  <xsd:complexType name=""GenericTimeStampType"" abstract=""true"">
    <xsd:sequence>
      <xsd:choice minOccurs=""0"">
        <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" ref=""Include"" />
        <xsd:element maxOccurs=""unbounded"" ref=""ReferenceInfo"" />
      </xsd:choice>
      <xsd:element minOccurs=""0"" ref=""ds:CanonicalizationMethod"" />
      <xsd:choice maxOccurs=""unbounded"">
        <xsd:element name=""EncapsulatedTimeStamp"" type=""EncapsulatedPKIDataType"" />
        <xsd:element name=""XMLTimeStamp"" type=""AnyType"" />
      </xsd:choice>
    </xsd:sequence>
    <xsd:attribute name=""Id"" type=""xsd:ID"" use=""optional"" />
  </xsd:complexType>
  <xsd:element name=""XAdESTimeStamp"" type=""XAdESTimeStampType"" />
  <xsd:complexType name=""XAdESTimeStampType"">
    <xsd:complexContent mixed=""false"">
      <xsd:restriction base=""GenericTimeStampType"">
        <xsd:sequence>
          <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" ref=""Include"" />
          <xsd:element minOccurs=""0"" ref=""ds:CanonicalizationMethod"" />
          <xsd:choice maxOccurs=""unbounded"">
            <xsd:element name=""EncapsulatedTimeStamp"" type=""EncapsulatedPKIDataType"" />
            <xsd:element name=""XMLTimeStamp"" type=""AnyType"" />
          </xsd:choice>
        </xsd:sequence>
        <xsd:attribute name=""Id"" type=""xsd:ID"" use=""optional"" />
      </xsd:restriction>
    </xsd:complexContent>
  </xsd:complexType>
  <xsd:element name=""OtherTimeStamp"" type=""OtherTimeStampType"" />
  <xsd:complexType name=""OtherTimeStampType"">
    <xsd:complexContent mixed=""false"">
      <xsd:restriction base=""GenericTimeStampType"">
        <xsd:sequence>
          <xsd:element maxOccurs=""unbounded"" ref=""ReferenceInfo"" />
          <xsd:element minOccurs=""0"" ref=""ds:CanonicalizationMethod"" />
          <xsd:choice>
            <xsd:element name=""EncapsulatedTimeStamp"" type=""EncapsulatedPKIDataType"" />
            <xsd:element name=""XMLTimeStamp"" type=""AnyType"" />
          </xsd:choice>
        </xsd:sequence>
        <xsd:attribute name=""Id"" type=""xsd:ID"" use=""optional"" />
      </xsd:restriction>
    </xsd:complexContent>
  </xsd:complexType>
  <xsd:element name=""QualifyingProperties"" type=""QualifyingPropertiesType"" />
  <xsd:complexType name=""QualifyingPropertiesType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""SignedProperties"" type=""SignedPropertiesType"" />
      <xsd:element minOccurs=""0"" name=""UnsignedProperties"" type=""UnsignedPropertiesType"" />
    </xsd:sequence>
    <xsd:attribute name=""Target"" type=""xsd:anyURI"" use=""required"" />
    <xsd:attribute name=""Id"" type=""xsd:ID"" use=""optional"" />
  </xsd:complexType>
  <xsd:element name=""SignedProperties"" type=""SignedPropertiesType"" />
  <xsd:complexType name=""SignedPropertiesType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""SignedSignatureProperties"" type=""SignedSignaturePropertiesType"" />
      <xsd:element minOccurs=""0"" name=""SignedDataObjectProperties"" type=""SignedDataObjectPropertiesType"" />
    </xsd:sequence>
    <xsd:attribute name=""Id"" type=""xsd:ID"" use=""optional"" />
  </xsd:complexType>
  <xsd:element name=""UnsignedProperties"" type=""UnsignedPropertiesType"" />
  <xsd:complexType name=""UnsignedPropertiesType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""UnsignedSignatureProperties"" type=""UnsignedSignaturePropertiesType"" />
      <xsd:element minOccurs=""0"" name=""UnsignedDataObjectProperties"" type=""UnsignedDataObjectPropertiesType"" />
    </xsd:sequence>
    <xsd:attribute name=""Id"" type=""xsd:ID"" use=""optional"" />
  </xsd:complexType>
  <xsd:element name=""SignedSignatureProperties"" type=""SignedSignaturePropertiesType"" />
  <xsd:complexType name=""SignedSignaturePropertiesType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""SigningTime"" type=""xsd:dateTime"" />
      <xsd:element minOccurs=""0"" name=""SigningCertificate"" type=""CertIDListType"" />
      <xsd:element minOccurs=""0"" name=""SignaturePolicyIdentifier"" type=""SignaturePolicyIdentifierType"" />
      <xsd:element minOccurs=""0"" name=""SignatureProductionPlace"" type=""SignatureProductionPlaceType"" />
      <xsd:element minOccurs=""0"" name=""SignerRole"" type=""SignerRoleType"" />
    </xsd:sequence>
    <xsd:attribute name=""Id"" type=""xsd:ID"" use=""optional"" />
  </xsd:complexType>
  <xsd:element name=""SignedDataObjectProperties"" type=""SignedDataObjectPropertiesType"" />
  <xsd:complexType name=""SignedDataObjectPropertiesType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""DataObjectFormat"" type=""DataObjectFormatType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""CommitmentTypeIndication"" type=""CommitmentTypeIndicationType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""AllDataObjectsTimeStamp"" type=""XAdESTimeStampType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""IndividualDataObjectsTimeStamp"" type=""XAdESTimeStampType"" />
    </xsd:sequence>
    <xsd:attribute name=""Id"" type=""xsd:ID"" use=""optional"" />
  </xsd:complexType>
  <xsd:element name=""UnsignedSignatureProperties"" type=""UnsignedSignaturePropertiesType"" />
  <xsd:complexType name=""UnsignedSignaturePropertiesType"">
    <xsd:choice maxOccurs=""unbounded"">
      <xsd:element name=""CounterSignature"" type=""CounterSignatureType"" />
      <xsd:element name=""SignatureTimeStamp"" type=""XAdESTimeStampType"" />
      <xsd:element name=""CompleteCertificateRefs"" type=""CompleteCertificateRefsType"" />
      <xsd:element name=""CompleteRevocationRefs"" type=""CompleteRevocationRefsType"" />
      <xsd:element name=""AttributeCertificateRefs"" type=""CompleteCertificateRefsType"" />
      <xsd:element name=""AttributeRevocationRefs"" type=""CompleteRevocationRefsType"" />
      <xsd:element name=""SigAndRefsTimeStamp"" type=""XAdESTimeStampType"" />
      <xsd:element name=""RefsOnlyTimeStamp"" type=""XAdESTimeStampType"" />
      <xsd:element name=""CertificateValues"" type=""CertificateValuesType"" />
      <xsd:element name=""RevocationValues"" type=""RevocationValuesType"" />
      <xsd:element name=""AttrAuthoritiesCertValues"" type=""CertificateValuesType"" />
      <xsd:element name=""AttributeRevocationValues"" type=""RevocationValuesType"" />
      <xsd:element name=""ArchiveTimeStamp"" type=""XAdESTimeStampType"" />
      <xsd:any namespace=""##other"" />
    </xsd:choice>
    <xsd:attribute name=""Id"" type=""xsd:ID"" use=""optional"" />
  </xsd:complexType>
  <xsd:element name=""UnsignedDataObjectProperties"" type=""UnsignedDataObjectPropertiesType"" />
  <xsd:complexType name=""UnsignedDataObjectPropertiesType"">
    <xsd:sequence>
      <xsd:element maxOccurs=""unbounded"" name=""UnsignedDataObjectProperty"" type=""AnyType"" />
    </xsd:sequence>
    <xsd:attribute name=""Id"" type=""xsd:ID"" use=""optional"" />
  </xsd:complexType>
  <xsd:element name=""QualifyingPropertiesReference"" type=""QualifyingPropertiesReferenceType"" />
  <xsd:complexType name=""QualifyingPropertiesReferenceType"">
    <xsd:attribute name=""URI"" type=""xsd:anyURI"" use=""required"" />
    <xsd:attribute name=""Id"" type=""xsd:ID"" use=""optional"" />
  </xsd:complexType>
  <xsd:element name=""SigningTime"" type=""xsd:dateTime"" />
  <xsd:element name=""SigningCertificate"" type=""CertIDListType"" />
  <xsd:complexType name=""CertIDListType"">
    <xsd:sequence>
      <xsd:element maxOccurs=""unbounded"" name=""Cert"" type=""CertIDType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CertIDType"">
    <xsd:sequence>
      <xsd:element name=""CertDigest"" type=""DigestAlgAndValueType"" />
      <xsd:element name=""IssuerSerial"" type=""ds:X509IssuerSerialType"" />
    </xsd:sequence>
    <xsd:attribute name=""URI"" type=""xsd:anyURI"" use=""optional"" />
  </xsd:complexType>
  <xsd:complexType name=""DigestAlgAndValueType"">
    <xsd:sequence>
      <xsd:element ref=""ds:DigestMethod"" />
      <xsd:element ref=""ds:DigestValue"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:element name=""SignaturePolicyIdentifier"" type=""SignaturePolicyIdentifierType"" />
  <xsd:complexType name=""SignaturePolicyIdentifierType"">
    <xsd:choice>
      <xsd:element name=""SignaturePolicyId"" type=""SignaturePolicyIdType"" />
      <xsd:element name=""SignaturePolicyImplied"" type=""xsd:anyType"" />
    </xsd:choice>
  </xsd:complexType>
  <xsd:complexType name=""SignaturePolicyIdType"">
    <xsd:sequence>
      <xsd:element name=""SigPolicyId"" type=""ObjectIdentifierType"" />
      <xsd:element minOccurs=""0"" ref=""ds:Transforms"" />
      <xsd:element name=""SigPolicyHash"" type=""DigestAlgAndValueType"" />
      <xsd:element minOccurs=""0"" name=""SigPolicyQualifiers"" type=""SigPolicyQualifiersListType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""SigPolicyQualifiersListType"">
    <xsd:sequence>
      <xsd:element maxOccurs=""unbounded"" name=""SigPolicyQualifier"" type=""AnyType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:element name=""SPURI"" type=""xsd:anyURI"" />
  <xsd:element name=""SPUserNotice"" type=""SPUserNoticeType"" />
  <xsd:complexType name=""SPUserNoticeType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""NoticeRef"" type=""NoticeReferenceType"" />
      <xsd:element minOccurs=""0"" name=""ExplicitText"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""NoticeReferenceType"">
    <xsd:sequence>
      <xsd:element name=""Organization"" type=""xsd:string"" />
      <xsd:element name=""NoticeNumbers"" type=""IntegerListType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""IntegerListType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""int"" type=""xsd:integer"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:element name=""CounterSignature"" type=""CounterSignatureType"" />
  <xsd:complexType name=""CounterSignatureType"">
    <xsd:sequence>
      <xsd:element ref=""ds:Signature"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:element name=""DataObjectFormat"" type=""DataObjectFormatType"" />
  <xsd:complexType name=""DataObjectFormatType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Description"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ObjectIdentifier"" type=""ObjectIdentifierType"" />
      <xsd:element minOccurs=""0"" name=""MimeType"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Encoding"" type=""xsd:anyURI"" />
    </xsd:sequence>
    <xsd:attribute name=""ObjectReference"" type=""xsd:anyURI"" use=""required"" />
  </xsd:complexType>
  <xsd:element name=""CommitmentTypeIndication"" type=""CommitmentTypeIndicationType"" />
  <xsd:complexType name=""CommitmentTypeIndicationType"">
    <xsd:sequence>
      <xsd:element name=""CommitmentTypeId"" type=""ObjectIdentifierType"" />
      <xsd:choice>
        <xsd:element maxOccurs=""unbounded"" name=""ObjectReference"" type=""xsd:anyURI"" />
        <xsd:element name=""AllSignedDataObjects"" type=""xsd:anyType"" />
      </xsd:choice>
      <xsd:element minOccurs=""0"" name=""CommitmentTypeQualifiers"" type=""CommitmentTypeQualifiersListType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CommitmentTypeQualifiersListType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""CommitmentTypeQualifier"" type=""AnyType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:element name=""SignatureProductionPlace"" type=""SignatureProductionPlaceType"" />
  <xsd:complexType name=""SignatureProductionPlaceType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""City"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""StateOrProvince"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PostalCode"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""CountryName"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:element name=""SignerRole"" type=""SignerRoleType"" />
  <xsd:complexType name=""SignerRoleType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""ClaimedRoles"" type=""ClaimedRolesListType"" />
      <xsd:element minOccurs=""0"" name=""CertifiedRoles"" type=""CertifiedRolesListType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ClaimedRolesListType"">
    <xsd:sequence>
      <xsd:element maxOccurs=""unbounded"" name=""ClaimedRole"" type=""AnyType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CertifiedRolesListType"">
    <xsd:sequence>
      <xsd:element maxOccurs=""unbounded"" name=""CertifiedRole"" type=""EncapsulatedPKIDataType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:element name=""AllDataObjectsTimeStamp"" type=""XAdESTimeStampType"" />
  <xsd:element name=""IndividualDataObjectsTimeStamp"" type=""XAdESTimeStampType"" />
  <xsd:element name=""SignatureTimeStamp"" type=""XAdESTimeStampType"" />
  <xsd:element name=""CompleteCertificateRefs"" type=""CompleteCertificateRefsType"" />
  <xsd:complexType name=""CompleteCertificateRefsType"">
    <xsd:sequence>
      <xsd:element name=""CertRefs"" type=""CertIDListType"" />
    </xsd:sequence>
    <xsd:attribute name=""Id"" type=""xsd:ID"" use=""optional"" />
  </xsd:complexType>
  <xsd:element name=""CompleteRevocationRefs"" type=""CompleteRevocationRefsType"" />
  <xsd:complexType name=""CompleteRevocationRefsType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""CRLRefs"" type=""CRLRefsType"" />
      <xsd:element minOccurs=""0"" name=""OCSPRefs"" type=""OCSPRefsType"" />
      <xsd:element minOccurs=""0"" name=""OtherRefs"" type=""OtherCertStatusRefsType"" />
    </xsd:sequence>
    <xsd:attribute name=""Id"" type=""xsd:ID"" use=""optional"" />
  </xsd:complexType>
  <xsd:complexType name=""CRLRefsType"">
    <xsd:sequence>
      <xsd:element maxOccurs=""unbounded"" name=""CRLRef"" type=""CRLRefType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CRLRefType"">
    <xsd:sequence>
      <xsd:element name=""DigestAlgAndValue"" type=""DigestAlgAndValueType"" />
      <xsd:element minOccurs=""0"" name=""CRLIdentifier"" type=""CRLIdentifierType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CRLIdentifierType"">
    <xsd:sequence>
      <xsd:element name=""Issuer"" type=""xsd:string"" />
      <xsd:element name=""IssueTime"" type=""xsd:dateTime"" />
      <xsd:element minOccurs=""0"" name=""Number"" type=""xsd:integer"" />
    </xsd:sequence>
    <xsd:attribute name=""URI"" type=""xsd:anyURI"" use=""optional"" />
  </xsd:complexType>
  <xsd:complexType name=""OCSPRefsType"">
    <xsd:sequence>
      <xsd:element maxOccurs=""unbounded"" name=""OCSPRef"" type=""OCSPRefType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""OCSPRefType"">
    <xsd:sequence>
      <xsd:element name=""OCSPIdentifier"" type=""OCSPIdentifierType"" />
      <xsd:element minOccurs=""0"" name=""DigestAlgAndValue"" type=""DigestAlgAndValueType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ResponderIDType"">
    <xsd:choice>
      <xsd:element name=""ByName"" type=""xsd:string"" />
      <xsd:element name=""ByKey"" type=""xsd:base64Binary"" />
    </xsd:choice>
  </xsd:complexType>
  <xsd:complexType name=""OCSPIdentifierType"">
    <xsd:sequence>
      <xsd:element name=""ResponderID"" type=""ResponderIDType"" />
      <xsd:element name=""ProducedAt"" type=""xsd:dateTime"" />
    </xsd:sequence>
    <xsd:attribute name=""URI"" type=""xsd:anyURI"" use=""optional"" />
  </xsd:complexType>
  <xsd:complexType name=""OtherCertStatusRefsType"">
    <xsd:sequence>
      <xsd:element maxOccurs=""unbounded"" name=""OtherRef"" type=""AnyType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:element name=""AttributeCertificateRefs"" type=""CompleteCertificateRefsType"" />
  <xsd:element name=""AttributeRevocationRefs"" type=""CompleteRevocationRefsType"" />
  <xsd:element name=""SigAndRefsTimeStamp"" type=""XAdESTimeStampType"" />
  <xsd:element name=""RefsOnlyTimeStamp"" type=""XAdESTimeStampType"" />
  <xsd:element name=""CertificateValues"" type=""CertificateValuesType"" />
  <xsd:complexType name=""CertificateValuesType"">
    <xsd:choice minOccurs=""0"" maxOccurs=""unbounded"">
      <xsd:element name=""EncapsulatedX509Certificate"" type=""EncapsulatedPKIDataType"" />
      <xsd:element name=""OtherCertificate"" type=""AnyType"" />
    </xsd:choice>
    <xsd:attribute name=""Id"" type=""xsd:ID"" use=""optional"" />
  </xsd:complexType>
  <xsd:element name=""RevocationValues"" type=""RevocationValuesType"" />
  <xsd:complexType name=""RevocationValuesType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""CRLValues"" type=""CRLValuesType"" />
      <xsd:element minOccurs=""0"" name=""OCSPValues"" type=""OCSPValuesType"" />
      <xsd:element minOccurs=""0"" name=""OtherValues"" type=""OtherCertStatusValuesType"" />
    </xsd:sequence>
    <xsd:attribute name=""Id"" type=""xsd:ID"" use=""optional"" />
  </xsd:complexType>
  <xsd:complexType name=""CRLValuesType"">
    <xsd:sequence>
      <xsd:element maxOccurs=""unbounded"" name=""EncapsulatedCRLValue"" type=""EncapsulatedPKIDataType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""OCSPValuesType"">
    <xsd:sequence>
      <xsd:element maxOccurs=""unbounded"" name=""EncapsulatedOCSPValue"" type=""EncapsulatedPKIDataType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""OtherCertStatusValuesType"">
    <xsd:sequence>
      <xsd:element maxOccurs=""unbounded"" name=""OtherValue"" type=""AnyType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:element name=""AttrAuthoritiesCertValues"" type=""CertificateValuesType"" />
  <xsd:element name=""AttributeRevocationValues"" type=""RevocationValuesType"" />
  <xsd:element name=""ArchiveTimeStamp"" type=""XAdESTimeStampType"" />
</xsd:schema>";
        
        public UBL_XAdESv132_2_1() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [39];
                _RootElements[0] = "Any";
                _RootElements[1] = "ObjectIdentifier";
                _RootElements[2] = "EncapsulatedPKIData";
                _RootElements[3] = "Include";
                _RootElements[4] = "ReferenceInfo";
                _RootElements[5] = "XAdESTimeStamp";
                _RootElements[6] = "OtherTimeStamp";
                _RootElements[7] = "QualifyingProperties";
                _RootElements[8] = "SignedProperties";
                _RootElements[9] = "UnsignedProperties";
                _RootElements[10] = "SignedSignatureProperties";
                _RootElements[11] = "SignedDataObjectProperties";
                _RootElements[12] = "UnsignedSignatureProperties";
                _RootElements[13] = "UnsignedDataObjectProperties";
                _RootElements[14] = "QualifyingPropertiesReference";
                _RootElements[15] = "SigningTime";
                _RootElements[16] = "SigningCertificate";
                _RootElements[17] = "SignaturePolicyIdentifier";
                _RootElements[18] = "SPURI";
                _RootElements[19] = "SPUserNotice";
                _RootElements[20] = "CounterSignature";
                _RootElements[21] = "DataObjectFormat";
                _RootElements[22] = "CommitmentTypeIndication";
                _RootElements[23] = "SignatureProductionPlace";
                _RootElements[24] = "SignerRole";
                _RootElements[25] = "AllDataObjectsTimeStamp";
                _RootElements[26] = "IndividualDataObjectsTimeStamp";
                _RootElements[27] = "SignatureTimeStamp";
                _RootElements[28] = "CompleteCertificateRefs";
                _RootElements[29] = "CompleteRevocationRefs";
                _RootElements[30] = "AttributeCertificateRefs";
                _RootElements[31] = "AttributeRevocationRefs";
                _RootElements[32] = "SigAndRefsTimeStamp";
                _RootElements[33] = "RefsOnlyTimeStamp";
                _RootElements[34] = "CertificateValues";
                _RootElements[35] = "RevocationValues";
                _RootElements[36] = "AttrAuthoritiesCertValues";
                _RootElements[37] = "AttributeRevocationValues";
                _RootElements[38] = "ArchiveTimeStamp";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"Any")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"Any"})]
        public sealed class Any : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public Any() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "Any";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"ObjectIdentifier")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"ObjectIdentifier"})]
        public sealed class ObjectIdentifier : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public ObjectIdentifier() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "ObjectIdentifier";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"EncapsulatedPKIData")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"EncapsulatedPKIData"})]
        public sealed class EncapsulatedPKIData : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public EncapsulatedPKIData() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "EncapsulatedPKIData";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"Include")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"Include"})]
        public sealed class Include : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public Include() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "Include";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"ReferenceInfo")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"ReferenceInfo"})]
        public sealed class ReferenceInfo : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public ReferenceInfo() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "ReferenceInfo";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"XAdESTimeStamp")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"XAdESTimeStamp"})]
        public sealed class XAdESTimeStamp : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public XAdESTimeStamp() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "XAdESTimeStamp";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"OtherTimeStamp")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"OtherTimeStamp"})]
        public sealed class OtherTimeStamp : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public OtherTimeStamp() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "OtherTimeStamp";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"QualifyingProperties")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"QualifyingProperties"})]
        public sealed class QualifyingProperties : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public QualifyingProperties() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "QualifyingProperties";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"SignedProperties")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"SignedProperties"})]
        public sealed class SignedProperties : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public SignedProperties() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "SignedProperties";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"UnsignedProperties")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"UnsignedProperties"})]
        public sealed class UnsignedProperties : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public UnsignedProperties() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "UnsignedProperties";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"SignedSignatureProperties")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"SignedSignatureProperties"})]
        public sealed class SignedSignatureProperties : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public SignedSignatureProperties() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "SignedSignatureProperties";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"SignedDataObjectProperties")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"SignedDataObjectProperties"})]
        public sealed class SignedDataObjectProperties : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public SignedDataObjectProperties() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "SignedDataObjectProperties";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"UnsignedSignatureProperties")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"UnsignedSignatureProperties"})]
        public sealed class UnsignedSignatureProperties : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public UnsignedSignatureProperties() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "UnsignedSignatureProperties";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"UnsignedDataObjectProperties")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"UnsignedDataObjectProperties"})]
        public sealed class UnsignedDataObjectProperties : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public UnsignedDataObjectProperties() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "UnsignedDataObjectProperties";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"QualifyingPropertiesReference")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"QualifyingPropertiesReference"})]
        public sealed class QualifyingPropertiesReference : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public QualifyingPropertiesReference() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "QualifyingPropertiesReference";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"SigningTime")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"SigningTime"})]
        public sealed class SigningTime : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public SigningTime() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "SigningTime";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"SigningCertificate")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"SigningCertificate"})]
        public sealed class SigningCertificate : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public SigningCertificate() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "SigningCertificate";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"SignaturePolicyIdentifier")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"SignaturePolicyIdentifier"})]
        public sealed class SignaturePolicyIdentifier : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public SignaturePolicyIdentifier() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "SignaturePolicyIdentifier";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"SPURI")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"SPURI"})]
        public sealed class SPURI : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public SPURI() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "SPURI";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"SPUserNotice")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"SPUserNotice"})]
        public sealed class SPUserNotice : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public SPUserNotice() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "SPUserNotice";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"CounterSignature")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"CounterSignature"})]
        public sealed class CounterSignature : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public CounterSignature() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "CounterSignature";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"DataObjectFormat")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"DataObjectFormat"})]
        public sealed class DataObjectFormat : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public DataObjectFormat() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "DataObjectFormat";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"CommitmentTypeIndication")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"CommitmentTypeIndication"})]
        public sealed class CommitmentTypeIndication : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public CommitmentTypeIndication() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "CommitmentTypeIndication";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"SignatureProductionPlace")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"SignatureProductionPlace"})]
        public sealed class SignatureProductionPlace : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public SignatureProductionPlace() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "SignatureProductionPlace";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"SignerRole")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"SignerRole"})]
        public sealed class SignerRole : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public SignerRole() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "SignerRole";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"AllDataObjectsTimeStamp")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"AllDataObjectsTimeStamp"})]
        public sealed class AllDataObjectsTimeStamp : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public AllDataObjectsTimeStamp() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "AllDataObjectsTimeStamp";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"IndividualDataObjectsTimeStamp")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"IndividualDataObjectsTimeStamp"})]
        public sealed class IndividualDataObjectsTimeStamp : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public IndividualDataObjectsTimeStamp() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "IndividualDataObjectsTimeStamp";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"SignatureTimeStamp")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"SignatureTimeStamp"})]
        public sealed class SignatureTimeStamp : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public SignatureTimeStamp() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "SignatureTimeStamp";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"CompleteCertificateRefs")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"CompleteCertificateRefs"})]
        public sealed class CompleteCertificateRefs : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public CompleteCertificateRefs() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "CompleteCertificateRefs";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"CompleteRevocationRefs")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"CompleteRevocationRefs"})]
        public sealed class CompleteRevocationRefs : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public CompleteRevocationRefs() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "CompleteRevocationRefs";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"AttributeCertificateRefs")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"AttributeCertificateRefs"})]
        public sealed class AttributeCertificateRefs : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public AttributeCertificateRefs() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "AttributeCertificateRefs";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"AttributeRevocationRefs")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"AttributeRevocationRefs"})]
        public sealed class AttributeRevocationRefs : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public AttributeRevocationRefs() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "AttributeRevocationRefs";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"SigAndRefsTimeStamp")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"SigAndRefsTimeStamp"})]
        public sealed class SigAndRefsTimeStamp : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public SigAndRefsTimeStamp() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "SigAndRefsTimeStamp";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"RefsOnlyTimeStamp")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"RefsOnlyTimeStamp"})]
        public sealed class RefsOnlyTimeStamp : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public RefsOnlyTimeStamp() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "RefsOnlyTimeStamp";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"CertificateValues")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"CertificateValues"})]
        public sealed class CertificateValues : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public CertificateValues() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "CertificateValues";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"RevocationValues")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"RevocationValues"})]
        public sealed class RevocationValues : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public RevocationValues() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "RevocationValues";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"AttrAuthoritiesCertValues")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"AttrAuthoritiesCertValues"})]
        public sealed class AttrAuthoritiesCertValues : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public AttrAuthoritiesCertValues() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "AttrAuthoritiesCertValues";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"AttributeRevocationValues")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"AttributeRevocationValues"})]
        public sealed class AttributeRevocationValues : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public AttributeRevocationValues() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "AttributeRevocationValues";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.3.2#",@"ArchiveTimeStamp")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"ArchiveTimeStamp"})]
        public sealed class ArchiveTimeStamp : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public ArchiveTimeStamp() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "ArchiveTimeStamp";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
    }
}
