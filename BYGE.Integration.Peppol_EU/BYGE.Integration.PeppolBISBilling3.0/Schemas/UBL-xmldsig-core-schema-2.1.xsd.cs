namespace BYGE.Integration.PeppolBISBilling3._0.Schemas {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"Signature", @"SignatureValue", @"SignedInfo", @"CanonicalizationMethod", @"SignatureMethod", @"Reference", @"Transforms", @"Transform", @"DigestMethod", @"DigestValue", @"KeyInfo", @"KeyName", @"MgmtData", @"KeyValue", @"RetrievalMethod", @"X509Data", @"PGPData", @"SPKIData", @"Object", @"Manifest", @"SignatureProperties", @"SignatureProperty", @"DSAKeyValue", @"RSAKeyValue"})]
    public sealed class UBL_xmldsig_core_schema_2_1 : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<schema xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" xmlns:ds=""http://www.w3.org/2000/09/xmldsig#"" elementFormDefault=""qualified"" targetNamespace=""http://www.w3.org/2000/09/xmldsig#"" version=""0.1"" xmlns=""http://www.w3.org/2001/XMLSchema"">
  <simpleType name=""CryptoBinary"">
    <restriction base=""base64Binary"" />
  </simpleType>
  <element name=""Signature"" type=""ds:SignatureType"" />
  <complexType name=""SignatureType"">
    <sequence>
      <element ref=""ds:SignedInfo"" />
      <element ref=""ds:SignatureValue"" />
      <element minOccurs=""0"" ref=""ds:KeyInfo"" />
      <element minOccurs=""0"" maxOccurs=""unbounded"" ref=""ds:Object"" />
    </sequence>
    <attribute name=""Id"" type=""ID"" use=""optional"" />
  </complexType>
  <element name=""SignatureValue"" type=""ds:SignatureValueType"" />
  <complexType name=""SignatureValueType"">
    <simpleContent>
      <extension base=""base64Binary"">
        <attribute name=""Id"" type=""ID"" use=""optional"" />
      </extension>
    </simpleContent>
  </complexType>
  <element name=""SignedInfo"" type=""ds:SignedInfoType"" />
  <complexType name=""SignedInfoType"">
    <sequence>
      <element ref=""ds:CanonicalizationMethod"" />
      <element ref=""ds:SignatureMethod"" />
      <element maxOccurs=""unbounded"" ref=""ds:Reference"" />
    </sequence>
    <attribute name=""Id"" type=""ID"" use=""optional"" />
  </complexType>
  <element name=""CanonicalizationMethod"" type=""ds:CanonicalizationMethodType"" />
  <complexType name=""CanonicalizationMethodType"" mixed=""true"">
    <sequence>
      <any minOccurs=""0"" maxOccurs=""unbounded"" namespace=""##any"" />
    </sequence>
    <attribute name=""Algorithm"" type=""anyURI"" use=""required"" />
  </complexType>
  <element name=""SignatureMethod"" type=""ds:SignatureMethodType"" />
  <complexType name=""SignatureMethodType"" mixed=""true"">
    <sequence>
      <element minOccurs=""0"" name=""HMACOutputLength"" type=""ds:HMACOutputLengthType"" />
      <any minOccurs=""0"" maxOccurs=""unbounded"" namespace=""##other"" />
    </sequence>
    <attribute name=""Algorithm"" type=""anyURI"" use=""required"" />
  </complexType>
  <element name=""Reference"" type=""ds:ReferenceType"" />
  <complexType name=""ReferenceType"">
    <sequence>
      <element minOccurs=""0"" ref=""ds:Transforms"" />
      <element ref=""ds:DigestMethod"" />
      <element ref=""ds:DigestValue"" />
    </sequence>
    <attribute name=""Id"" type=""ID"" use=""optional"" />
    <attribute name=""URI"" type=""anyURI"" use=""optional"" />
    <attribute name=""Type"" type=""anyURI"" use=""optional"" />
  </complexType>
  <element name=""Transforms"" type=""ds:TransformsType"" />
  <complexType name=""TransformsType"">
    <sequence>
      <element maxOccurs=""unbounded"" ref=""ds:Transform"" />
    </sequence>
  </complexType>
  <element name=""Transform"" type=""ds:TransformType"" />
  <complexType name=""TransformType"" mixed=""true"">
    <choice minOccurs=""0"" maxOccurs=""unbounded"">
      <any namespace=""##other"" processContents=""lax"" />
      <element name=""XPath"" type=""string"" />
    </choice>
    <attribute name=""Algorithm"" type=""anyURI"" use=""required"" />
  </complexType>
  <element name=""DigestMethod"" type=""ds:DigestMethodType"" />
  <complexType name=""DigestMethodType"" mixed=""true"">
    <sequence>
      <any minOccurs=""0"" maxOccurs=""unbounded"" namespace=""##other"" processContents=""lax"" />
    </sequence>
    <attribute name=""Algorithm"" type=""anyURI"" use=""required"" />
  </complexType>
  <element name=""DigestValue"" type=""ds:DigestValueType"" />
  <simpleType name=""DigestValueType"">
    <restriction base=""base64Binary"" />
  </simpleType>
  <element name=""KeyInfo"" type=""ds:KeyInfoType"" />
  <complexType name=""KeyInfoType"" mixed=""true"">
    <choice maxOccurs=""unbounded"">
      <element ref=""ds:KeyName"" />
      <element ref=""ds:KeyValue"" />
      <element ref=""ds:RetrievalMethod"" />
      <element ref=""ds:X509Data"" />
      <element ref=""ds:PGPData"" />
      <element ref=""ds:SPKIData"" />
      <element ref=""ds:MgmtData"" />
      <any namespace=""##other"" processContents=""lax"" />
    </choice>
    <attribute name=""Id"" type=""ID"" use=""optional"" />
  </complexType>
  <element name=""KeyName"" type=""string"" />
  <element name=""MgmtData"" type=""string"" />
  <element name=""KeyValue"" type=""ds:KeyValueType"" />
  <complexType name=""KeyValueType"" mixed=""true"">
    <choice>
      <element ref=""ds:DSAKeyValue"" />
      <element ref=""ds:RSAKeyValue"" />
      <any namespace=""##other"" processContents=""lax"" />
    </choice>
  </complexType>
  <element name=""RetrievalMethod"" type=""ds:RetrievalMethodType"" />
  <complexType name=""RetrievalMethodType"">
    <sequence>
      <element minOccurs=""0"" ref=""ds:Transforms"" />
    </sequence>
    <attribute name=""URI"" type=""anyURI"" />
    <attribute name=""Type"" type=""anyURI"" use=""optional"" />
  </complexType>
  <element name=""X509Data"" type=""ds:X509DataType"" />
  <complexType name=""X509DataType"">
    <sequence maxOccurs=""unbounded"">
      <choice>
        <element name=""X509IssuerSerial"" type=""ds:X509IssuerSerialType"" />
        <element name=""X509SKI"" type=""base64Binary"" />
        <element name=""X509SubjectName"" type=""string"" />
        <element name=""X509Certificate"" type=""base64Binary"" />
        <element name=""X509CRL"" type=""base64Binary"" />
        <any namespace=""##other"" processContents=""lax"" />
      </choice>
    </sequence>
  </complexType>
  <complexType name=""X509IssuerSerialType"">
    <sequence>
      <element name=""X509IssuerName"" type=""string"" />
      <element name=""X509SerialNumber"" type=""integer"" />
    </sequence>
  </complexType>
  <element name=""PGPData"" type=""ds:PGPDataType"" />
  <complexType name=""PGPDataType"">
    <choice>
      <sequence>
        <element name=""PGPKeyID"" type=""base64Binary"" />
        <element minOccurs=""0"" name=""PGPKeyPacket"" type=""base64Binary"" />
        <any minOccurs=""0"" maxOccurs=""unbounded"" namespace=""##other"" processContents=""lax"" />
      </sequence>
      <sequence>
        <element name=""PGPKeyPacket"" type=""base64Binary"" />
        <any minOccurs=""0"" maxOccurs=""unbounded"" namespace=""##other"" processContents=""lax"" />
      </sequence>
    </choice>
  </complexType>
  <element name=""SPKIData"" type=""ds:SPKIDataType"" />
  <complexType name=""SPKIDataType"">
    <sequence maxOccurs=""unbounded"">
      <element name=""SPKISexp"" type=""base64Binary"" />
      <any minOccurs=""0"" namespace=""##other"" processContents=""lax"" />
    </sequence>
  </complexType>
  <element name=""Object"" type=""ds:ObjectType"" />
  <complexType name=""ObjectType"" mixed=""true"">
    <sequence minOccurs=""0"" maxOccurs=""unbounded"">
      <any namespace=""##any"" processContents=""lax"" />
    </sequence>
    <attribute name=""Id"" type=""ID"" use=""optional"" />
    <attribute name=""MimeType"" type=""string"" use=""optional"" />
    <attribute name=""Encoding"" type=""anyURI"" use=""optional"" />
  </complexType>
  <element name=""Manifest"" type=""ds:ManifestType"" />
  <complexType name=""ManifestType"">
    <sequence>
      <element maxOccurs=""unbounded"" ref=""ds:Reference"" />
    </sequence>
    <attribute name=""Id"" type=""ID"" use=""optional"" />
  </complexType>
  <element name=""SignatureProperties"" type=""ds:SignaturePropertiesType"" />
  <complexType name=""SignaturePropertiesType"">
    <sequence>
      <element maxOccurs=""unbounded"" ref=""ds:SignatureProperty"" />
    </sequence>
    <attribute name=""Id"" type=""ID"" use=""optional"" />
  </complexType>
  <element name=""SignatureProperty"" type=""ds:SignaturePropertyType"" />
  <complexType name=""SignaturePropertyType"" mixed=""true"">
    <choice maxOccurs=""unbounded"">
      <any namespace=""##other"" processContents=""lax"" />
    </choice>
    <attribute name=""Target"" type=""anyURI"" use=""required"" />
    <attribute name=""Id"" type=""ID"" use=""optional"" />
  </complexType>
  <simpleType name=""HMACOutputLengthType"">
    <restriction base=""integer"" />
  </simpleType>
  <element name=""DSAKeyValue"" type=""ds:DSAKeyValueType"" />
  <complexType name=""DSAKeyValueType"">
    <sequence>
      <sequence minOccurs=""0"">
        <element name=""P"" type=""ds:CryptoBinary"" />
        <element name=""Q"" type=""ds:CryptoBinary"" />
      </sequence>
      <element minOccurs=""0"" name=""G"" type=""ds:CryptoBinary"" />
      <element name=""Y"" type=""ds:CryptoBinary"" />
      <element minOccurs=""0"" name=""J"" type=""ds:CryptoBinary"" />
      <sequence minOccurs=""0"">
        <element name=""Seed"" type=""ds:CryptoBinary"" />
        <element name=""PgenCounter"" type=""ds:CryptoBinary"" />
      </sequence>
    </sequence>
  </complexType>
  <element name=""RSAKeyValue"" type=""ds:RSAKeyValueType"" />
  <complexType name=""RSAKeyValueType"">
    <sequence>
      <element name=""Modulus"" type=""ds:CryptoBinary"" />
      <element name=""Exponent"" type=""ds:CryptoBinary"" />
    </sequence>
  </complexType>
</schema>";
        
        public UBL_xmldsig_core_schema_2_1() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [24];
                _RootElements[0] = "Signature";
                _RootElements[1] = "SignatureValue";
                _RootElements[2] = "SignedInfo";
                _RootElements[3] = "CanonicalizationMethod";
                _RootElements[4] = "SignatureMethod";
                _RootElements[5] = "Reference";
                _RootElements[6] = "Transforms";
                _RootElements[7] = "Transform";
                _RootElements[8] = "DigestMethod";
                _RootElements[9] = "DigestValue";
                _RootElements[10] = "KeyInfo";
                _RootElements[11] = "KeyName";
                _RootElements[12] = "MgmtData";
                _RootElements[13] = "KeyValue";
                _RootElements[14] = "RetrievalMethod";
                _RootElements[15] = "X509Data";
                _RootElements[16] = "PGPData";
                _RootElements[17] = "SPKIData";
                _RootElements[18] = "Object";
                _RootElements[19] = "Manifest";
                _RootElements[20] = "SignatureProperties";
                _RootElements[21] = "SignatureProperty";
                _RootElements[22] = "DSAKeyValue";
                _RootElements[23] = "RSAKeyValue";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
        
        [Schema(@"http://www.w3.org/2000/09/xmldsig#",@"Signature")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"Signature"})]
        public sealed class Signature : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public Signature() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "Signature";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://www.w3.org/2000/09/xmldsig#",@"SignatureValue")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"SignatureValue"})]
        public sealed class SignatureValue : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public SignatureValue() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "SignatureValue";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://www.w3.org/2000/09/xmldsig#",@"SignedInfo")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"SignedInfo"})]
        public sealed class SignedInfo : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public SignedInfo() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "SignedInfo";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://www.w3.org/2000/09/xmldsig#",@"CanonicalizationMethod")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"CanonicalizationMethod"})]
        public sealed class CanonicalizationMethod : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public CanonicalizationMethod() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "CanonicalizationMethod";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://www.w3.org/2000/09/xmldsig#",@"SignatureMethod")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"SignatureMethod"})]
        public sealed class SignatureMethod : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public SignatureMethod() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "SignatureMethod";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://www.w3.org/2000/09/xmldsig#",@"Reference")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"Reference"})]
        public sealed class Reference : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public Reference() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "Reference";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://www.w3.org/2000/09/xmldsig#",@"Transforms")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"Transforms"})]
        public sealed class Transforms : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public Transforms() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "Transforms";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://www.w3.org/2000/09/xmldsig#",@"Transform")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"Transform"})]
        public sealed class Transform : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public Transform() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "Transform";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://www.w3.org/2000/09/xmldsig#",@"DigestMethod")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"DigestMethod"})]
        public sealed class DigestMethod : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public DigestMethod() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "DigestMethod";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://www.w3.org/2000/09/xmldsig#",@"DigestValue")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"DigestValue"})]
        public sealed class DigestValue : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public DigestValue() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "DigestValue";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://www.w3.org/2000/09/xmldsig#",@"KeyInfo")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"KeyInfo"})]
        public sealed class KeyInfo : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public KeyInfo() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "KeyInfo";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://www.w3.org/2000/09/xmldsig#",@"KeyName")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"KeyName"})]
        public sealed class KeyName : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public KeyName() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "KeyName";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://www.w3.org/2000/09/xmldsig#",@"MgmtData")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"MgmtData"})]
        public sealed class MgmtData : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public MgmtData() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "MgmtData";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://www.w3.org/2000/09/xmldsig#",@"KeyValue")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"KeyValue"})]
        public sealed class KeyValue : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public KeyValue() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "KeyValue";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://www.w3.org/2000/09/xmldsig#",@"RetrievalMethod")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"RetrievalMethod"})]
        public sealed class RetrievalMethod : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public RetrievalMethod() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "RetrievalMethod";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://www.w3.org/2000/09/xmldsig#",@"X509Data")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"X509Data"})]
        public sealed class X509Data : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public X509Data() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "X509Data";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://www.w3.org/2000/09/xmldsig#",@"PGPData")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"PGPData"})]
        public sealed class PGPData : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public PGPData() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "PGPData";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://www.w3.org/2000/09/xmldsig#",@"SPKIData")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"SPKIData"})]
        public sealed class SPKIData : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public SPKIData() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "SPKIData";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://www.w3.org/2000/09/xmldsig#",@"Object")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"Object"})]
        public sealed class Object : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public Object() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "Object";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://www.w3.org/2000/09/xmldsig#",@"Manifest")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"Manifest"})]
        public sealed class Manifest : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public Manifest() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "Manifest";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://www.w3.org/2000/09/xmldsig#",@"SignatureProperties")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"SignatureProperties"})]
        public sealed class SignatureProperties : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public SignatureProperties() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "SignatureProperties";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://www.w3.org/2000/09/xmldsig#",@"SignatureProperty")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"SignatureProperty"})]
        public sealed class SignatureProperty : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public SignatureProperty() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "SignatureProperty";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://www.w3.org/2000/09/xmldsig#",@"DSAKeyValue")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"DSAKeyValue"})]
        public sealed class DSAKeyValue : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public DSAKeyValue() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "DSAKeyValue";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://www.w3.org/2000/09/xmldsig#",@"RSAKeyValue")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"RSAKeyValue"})]
        public sealed class RSAKeyValue : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public RSAKeyValue() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "RSAKeyValue";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
    }
}
