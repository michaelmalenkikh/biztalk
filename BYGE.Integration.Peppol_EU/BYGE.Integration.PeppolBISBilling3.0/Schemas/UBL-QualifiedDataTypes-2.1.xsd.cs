namespace BYGE.Integration.PeppolBISBilling3._0.Schemas {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [System.SerializableAttribute()]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_UnqualifiedDataTypes_2_1", typeof(global::BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_UnqualifiedDataTypes_2_1))]
    public sealed class UBL_QualifiedDataTypes_2_1 : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xsd:schema xmlns=""urn:oasis:names:specification:ubl:schema:xsd:QualifiedDataTypes-2"" xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" xmlns:udt=""urn:oasis:names:specification:ubl:schema:xsd:UnqualifiedDataTypes-2"" xmlns:ccts=""urn:un:unece:uncefact:documentation:2"" attributeFormDefault=""unqualified"" elementFormDefault=""qualified"" targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:QualifiedDataTypes-2"" version=""2.1"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
  <xsd:import schemaLocation=""BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_UnqualifiedDataTypes_2_1"" namespace=""urn:oasis:names:specification:ubl:schema:xsd:UnqualifiedDataTypes-2"" />
  <xsd:annotation>
    <xsd:appinfo>
      <references xmlns=""http://schemas.microsoft.com/BizTalk/2003"">
        <reference targetNamespace=""urn:un:unece:uncefact:data:specification:CoreComponentTypeSchemaModule:2"" />
        <reference targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:UnqualifiedDataTypes-2"" />
      </references>
    </xsd:appinfo>
  </xsd:annotation>
</xsd:schema>";
        
        public UBL_QualifiedDataTypes_2_1() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [0];
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
    }
}
