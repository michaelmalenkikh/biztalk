namespace BYGE.Integration.PeppolBISBilling3._0.Schemas {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"TimeStampValidationData", @"ArchiveTimeStampV2"})]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_XAdESv132_2_1", typeof(global::BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_XAdESv132_2_1))]
    public sealed class UBL_XAdESv141_2_1 : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xsd:schema xmlns=""http://uri.etsi.org/01903/v1.4.1#"" xmlns:xades=""http://uri.etsi.org/01903/v1.3.2#"" xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" elementFormDefault=""qualified"" targetNamespace=""http://uri.etsi.org/01903/v1.4.1#"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
  <xsd:import schemaLocation=""BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_XAdESv132_2_1"" namespace=""http://uri.etsi.org/01903/v1.3.2#"" />
  <xsd:annotation>
    <xsd:appinfo>
      <references xmlns=""http://schemas.microsoft.com/BizTalk/2003"">
        <reference targetNamespace=""http://www.w3.org/2000/09/xmldsig#"" />
        <reference targetNamespace=""http://uri.etsi.org/01903/v1.3.2#"" />
      </references>
    </xsd:appinfo>
  </xsd:annotation>
  <xsd:element name=""TimeStampValidationData"" type=""ValidationDataType"" />
  <xsd:complexType name=""ValidationDataType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" ref=""xades:CertificateValues"" />
      <xsd:element minOccurs=""0"" ref=""xades:RevocationValues"" />
    </xsd:sequence>
    <xsd:attribute name=""Id"" type=""xsd:ID"" use=""optional"" />
    <xsd:attribute name=""UR"" type=""xsd:anyURI"" use=""optional"" />
  </xsd:complexType>
  <xsd:element name=""ArchiveTimeStampV2"" type=""xades:XAdESTimeStampType"" />
</xsd:schema>";
        
        public UBL_XAdESv141_2_1() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [2];
                _RootElements[0] = "TimeStampValidationData";
                _RootElements[1] = "ArchiveTimeStampV2";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.4.1#",@"TimeStampValidationData")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"TimeStampValidationData"})]
        public sealed class TimeStampValidationData : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public TimeStampValidationData() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "TimeStampValidationData";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://uri.etsi.org/01903/v1.4.1#",@"ArchiveTimeStampV2")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"ArchiveTimeStampV2"})]
        public sealed class ArchiveTimeStampV2 : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public ArchiveTimeStampV2() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "ArchiveTimeStampV2";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
    }
}
