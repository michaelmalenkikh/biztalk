namespace BYGE.Integration.PeppolBISBilling3._0.Schemas {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:SignatureAggregateComponents-2",@"SignatureInformation")]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"SignatureInformation"})]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_SignatureBasicComponents_2_1", typeof(global::BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_SignatureBasicComponents_2_1))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_CommonBasicComponents_2_1", typeof(global::BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_CommonBasicComponents_2_1))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_xmldsig_core_schema_2_1", typeof(global::BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_xmldsig_core_schema_2_1))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_XAdESv132_2_1", typeof(global::BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_XAdESv132_2_1))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_XAdESv141_2_1", typeof(global::BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_XAdESv141_2_1))]
    public sealed class UBL_SignatureAggregateComponents_2_1 : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xsd:schema xmlns:ccts=""urn:un:unece:uncefact:documentation:2"" xmlns:ds=""http://www.w3.org/2000/09/xmldsig#"" xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" xmlns=""urn:oasis:names:specification:ubl:schema:xsd:SignatureAggregateComponents-2"" xmlns:cbc=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" xmlns:sbc=""urn:oasis:names:specification:ubl:schema:xsd:SignatureBasicComponents-2"" attributeFormDefault=""unqualified"" elementFormDefault=""qualified"" targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:SignatureAggregateComponents-2"" version=""2.1"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
  <xsd:import schemaLocation=""BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_SignatureBasicComponents_2_1"" namespace=""urn:oasis:names:specification:ubl:schema:xsd:SignatureBasicComponents-2"" />
  <xsd:import schemaLocation=""BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_CommonBasicComponents_2_1"" namespace=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" />
  <xsd:import schemaLocation=""BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_xmldsig_core_schema_2_1"" namespace=""http://www.w3.org/2000/09/xmldsig#"" />
  <xsd:import schemaLocation=""BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_XAdESv132_2_1"" namespace=""http://uri.etsi.org/01903/v1.3.2#"" />
  <xsd:import schemaLocation=""BYGE.Integration.PeppolBISBilling3._0.Schemas.UBL_XAdESv141_2_1"" namespace=""http://uri.etsi.org/01903/v1.4.1#"" />
  <xsd:annotation>
    <xsd:appinfo>
      <references xmlns=""http://schemas.microsoft.com/BizTalk/2003"">
        <reference targetNamespace=""http://www.w3.org/2000/09/xmldsig#"" />
        <reference targetNamespace=""http://uri.etsi.org/01903/v1.3.2#"" />
        <reference targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" />
        <reference targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:UnqualifiedDataTypes-2"" />
        <reference targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:QualifiedDataTypes-2"" />
        <reference targetNamespace=""urn:un:unece:uncefact:data:specification:CoreComponentTypeSchemaModule:2"" />
        <reference targetNamespace=""http://uri.etsi.org/01903/v1.4.1#"" />
        <reference targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:SignatureBasicComponents-2"" />
      </references>
    </xsd:appinfo>
  </xsd:annotation>
  <xsd:element name=""SignatureInformation"" type=""SignatureInformationType"" />
  <xsd:complexType name=""SignatureInformationType"">
    <xsd:annotation>
      <xsd:documentation>
        <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
          <ccts:ComponentType>ABIE</ccts:ComponentType>
          <ccts:DictionaryEntryName>Signature Information. Details</ccts:DictionaryEntryName>
          <ccts:Definition>This class captures a single signature and optionally associates to a signature in the document with the corresponding identifier.</ccts:Definition>
          <ccts:ObjectClass>Signature Information</ccts:ObjectClass>
        </ccts:Component>
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""cbc:ID"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>BBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Signature Information. Identifier</ccts:DictionaryEntryName>
              <ccts:Definition>This specifies the identifier of the signature distinguishing it from other signatures.</ccts:Definition>
              <ccts:Cardinality>0..1</ccts:Cardinality>
              <ccts:ObjectClass>Signature Information</ccts:ObjectClass>
              <ccts:PropertyTerm>Identifier</ccts:PropertyTerm>
              <ccts:RepresentationTerm>Identifier</ccts:RepresentationTerm>
              <ccts:DataType>Identifier. Type</ccts:DataType>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""sbc:ReferencedSignatureID"">
        <xsd:annotation>
          <xsd:documentation>
            <ccts:Component xmlns:ccts=""urn:un:unece:uncefact:documentation:2"">
              <ccts:ComponentType>BBIE</ccts:ComponentType>
              <ccts:DictionaryEntryName>Signature Information. Referenced Signature Identifier. Identifier</ccts:DictionaryEntryName>
              <ccts:Definition>This associates this signature with the identifier of a signature business object in the document.</ccts:Definition>
              <ccts:Cardinality>0..1</ccts:Cardinality>
              <ccts:ObjectClass>Signature Information</ccts:ObjectClass>
              <ccts:PropertyTerm>Referenced Signature Identifier</ccts:PropertyTerm>
              <ccts:RepresentationTerm>Identifier</ccts:RepresentationTerm>
              <ccts:DataType>Identifier. Type</ccts:DataType>
            </ccts:Component>
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" ref=""ds:Signature"">
        <xsd:annotation>
          <xsd:documentation>This is a single digital signature as defined by the W3C specification.</xsd:documentation>
        </xsd:annotation>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
</xsd:schema>";
        
        public UBL_SignatureAggregateComponents_2_1() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [1];
                _RootElements[0] = "SignatureInformation";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
    }
}
