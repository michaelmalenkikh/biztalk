namespace BYGE.Integration.M3Invoice {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [Schema(@"http://www.intentia.com/MBM",@"Envelope")]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"Envelope"})]
    public sealed class M3Invoice : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xs:schema xmlns=""http://www.intentia.com/MBM"" xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" attributeFormDefault=""unqualified"" elementFormDefault=""qualified"" targetNamespace=""http://www.intentia.com/MBM"" xmlns:xs=""http://www.w3.org/2001/XMLSchema"">
  <xs:element name=""Envelope"">
    <xs:complexType>
      <xs:sequence>
        <xs:element minOccurs=""1"" maxOccurs=""1"" name=""Header"">
          <xs:complexType>
            <xs:sequence>
              <xs:element maxOccurs=""1"" name=""delivery"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element name=""to"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name=""address"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element name=""from"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name=""address"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element name=""reliability"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name=""sendReceiptTo"" type=""xs:string"" />
                          <xs:element name=""receiptRequiredBy"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element maxOccurs=""1"" name=""Properties"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element name=""identity"" type=""xs:string"" />
                    <xs:element name=""SentAt"" type=""xs:string"" />
                    <xs:element name=""expiresAt"" type=""xs:string"" />
                    <xs:element name=""topic"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element maxOccurs=""1"" name=""manifest"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element name=""reference"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name=""description"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element maxOccurs=""1"" name=""process"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element name=""type"" type=""xs:string"" />
                    <xs:element name=""instance"" type=""xs:string"" />
                    <xs:element name=""handle"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""1"" maxOccurs=""99"" name=""Body"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""1"" maxOccurs=""1"" name=""INVOIC"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""1"" maxOccurs=""1"" name=""UNB"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name=""cmp01"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name=""e01_0001"" type=""xs:string"" />
                                <xs:element name=""e01_0002"" type=""xs:string"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element name=""cmp02"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name=""e01_0004"" type=""xs:string"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element name=""cmp03"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name=""e01_0010"" type=""xs:string"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element name=""cmp04"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name=""e01_0017"" type=""xs:date"" />
                                <xs:element name=""e02_0019"" type=""xs:string"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element name=""e01_0020"" type=""xs:string"" />
                          <xs:element name=""e04_0031"" type=""xs:string"" />
                          <xs:element name=""e05_0032"" type=""xs:string"" />
                          <xs:element name=""e06_0035"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""1"" maxOccurs=""1"" name=""UNH"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name=""e01_0062"" type=""xs:string"" />
                          <xs:element name=""cmp01"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name=""e01_0065"" type=""xs:string"" />
                                <xs:element name=""e02_0052"" type=""xs:string"" />
                                <xs:element name=""e03_0054"" type=""xs:string"" />
                                <xs:element name=""e04_0051"" type=""xs:string"" />
                                <xs:element name=""e05_0057"" type=""xs:string"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""1"" maxOccurs=""1"" name=""BGM"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name=""cmp01"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name=""e01_1001"" type=""xs:string"" />
                                <xs:element name=""e03_3055"" type=""xs:string"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element name=""e01_1004"" type=""xs:string"" />
                          <xs:element name=""e02_1225"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""1"" maxOccurs=""unbounded"" name=""DTM"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name=""cmp01"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name=""e01_2005"" type=""xs:string"" />
                                <xs:element name=""e02_2380"" type=""xs:date"" />
                                <xs:element name=""e03_2379"" type=""xs:string"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""1"" maxOccurs=""unbounded"" name=""FTX"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name=""e01_4451"" type=""xs:string"" />
                          <xs:element name=""cmp02"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name=""e01_4440"" type=""xs:string"" />
                                <xs:element name=""e02_4440"" type=""xs:string"" />
                                <xs:element name=""e03_4440"" type=""xs:string"" />
                                <xs:element name=""e04_4440"" type=""xs:string"" />
                                <xs:element name=""e05_4440"" type=""xs:string"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""1"" maxOccurs=""10"" name=""g001"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name=""RFF"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name=""cmp01"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element name=""e01_1153"" type=""xs:string"" />
                                      <xs:element name=""e02_1154"" type=""xs:string"" />
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element name=""DTM"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name=""cmp01"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element name=""e01_2005"" type=""xs:string"" />
                                      <xs:element name=""e02_2380"" type=""xs:date"" />
                                      <xs:element name=""e03_2379"" type=""xs:string"" />
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""3"" maxOccurs=""10"" name=""g002"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""1"" maxOccurs=""1"" name=""NAD"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name=""e01_3035"" type=""xs:string"" />
                                <xs:element name=""cmp01"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element name=""e01_3039"" type=""xs:string"" />
                                      <xs:element name=""e03_3055"" type=""xs:string"" />
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                                <xs:element name=""cmp03"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element name=""e01_3036"" type=""xs:string"" />
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                                <xs:element name=""cmp04"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element name=""e01_3042"" type=""xs:string"" />
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                                <xs:element name=""e02_3164"" type=""xs:string"" />
                                <xs:element name=""e04_3251"" type=""xs:string"" />
                                <xs:element name=""e05_3207"" type=""xs:string"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element name=""g003"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name=""RFF"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element name=""cmp01"">
                                        <xs:complexType>
                                          <xs:sequence>
                                            <xs:element name=""e01_1153"" type=""xs:string"" />
                                            <xs:element name=""e02_1154"" type=""xs:string"" />
                                          </xs:sequence>
                                        </xs:complexType>
                                      </xs:element>
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element name=""g005"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name=""CTA"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element name=""e01_3139"" type=""xs:string"" />
                                      <xs:element name=""cmp01"">
                                        <xs:complexType>
                                          <xs:sequence>
                                            <xs:element name=""e02_3412"" type=""xs:string"" />
                                          </xs:sequence>
                                        </xs:complexType>
                                      </xs:element>
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element maxOccurs=""1"" name=""g006"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name=""TAX"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name=""e01_5283"" type=""xs:string"" />
                                <xs:element name=""cmp01"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element name=""e01_5153"" type=""xs:string"" />
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                                <xs:element name=""cmp03"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element name=""e04_5278"" type=""xs:string"" />
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                                <xs:element name=""e03_5305"" type=""xs:string"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element maxOccurs=""1"" name=""g007"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name=""CUX"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name=""cmp01"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element name=""e01_6347"" type=""xs:string"" />
                                      <xs:element name=""e02_6345"" type=""xs:string"" />
                                      <xs:element name=""e03_6343"" type=""xs:string"" />
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element maxOccurs=""1"" name=""g008"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name=""PAT"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name=""e01_4279"" type=""xs:string"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element name=""DTM"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name=""cmp01"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element name=""e01_2005"" type=""xs:string"" />
                                      <xs:element name=""e02_2380"" type=""xs:date"" />
                                      <xs:element name=""e03_2379"" type=""xs:string"" />
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element maxOccurs=""1"" name=""g015"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name=""ALC"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name=""e01_5463"" type=""xs:string"" />
                                <xs:element name=""cmp02"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element name=""e01_7161"" type=""xs:string"" />
                                      <xs:element name=""e04_7160"" type=""xs:string"" />
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element name=""g018"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name=""PCD"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element name=""cmp01"">
                                        <xs:complexType>
                                          <xs:sequence>
                                            <xs:element name=""e01_5245"" type=""xs:string"" />
                                            <xs:element name=""e02_5482"" type=""xs:string"" />
                                          </xs:sequence>
                                        </xs:complexType>
                                      </xs:element>
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element name=""g019"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name=""MOA"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element name=""cmp01"">
                                        <xs:complexType>
                                          <xs:sequence>
                                            <xs:element name=""e01_5025"" type=""xs:string"" />
                                            <xs:element name=""e02_5004"" type=""xs:string"" />
                                          </xs:sequence>
                                        </xs:complexType>
                                      </xs:element>
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""1"" maxOccurs=""unbounded"" name=""g025"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""1"" maxOccurs=""1"" name=""LIN"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element minOccurs=""1"" maxOccurs=""1"" name=""e01_1082"" type=""xs:string"" />
                                <xs:element name=""cmp01"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element name=""e01_7140"" type=""xs:string"" />
                                      <xs:element name=""e02_7143"" type=""xs:string"" />
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element minOccurs=""1"" maxOccurs=""unbounded"" name=""PIA"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name=""e01_4347"" type=""xs:string"" />
                                <xs:element name=""cmp01"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element name=""e01_7140"" type=""xs:string"" />
                                      <xs:element name=""e02_7143"" type=""xs:string"" />
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                                <xs:element name=""cmp02"" type=""xs:string"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element minOccurs=""1"" maxOccurs=""1"" name=""IMD"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name=""e01_7077"" type=""xs:string"" />
                                <xs:element name=""cmp01"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element name=""e04_7008"" type=""xs:string"" />
                                      <xs:element name=""e05_7008"" type=""xs:string"" />
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element minOccurs=""1"" maxOccurs=""unbounded"" name=""QTY"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name=""cmp01"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element name=""e01_6063"" type=""xs:string"" />
                                      <xs:element name=""e02_6060"" type=""xs:string"" />
                                      <xs:element name=""e03_6411"" type=""xs:string"" />
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element minOccurs=""1"" maxOccurs=""unbounded"" name=""g026"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name=""MOA"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element name=""cmp01"">
                                        <xs:complexType>
                                          <xs:sequence>
                                            <xs:element name=""e01_5025"" type=""xs:string"" />
                                            <xs:element name=""e02_5004"" type=""xs:string"" />
                                          </xs:sequence>
                                        </xs:complexType>
                                      </xs:element>
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element minOccurs=""1"" maxOccurs=""unbounded"" name=""g028"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name=""PRI"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element name=""cmp01"">
                                        <xs:complexType>
                                          <xs:sequence>
                                            <xs:element name=""e01_5125"" type=""xs:string"" />
                                            <xs:element name=""e02_5118"" type=""xs:string"" />
                                            <xs:element name=""e05_5284"" type=""xs:string"" />
                                            <xs:element name=""e06_6411"" type=""xs:string"" />
                                          </xs:sequence>
                                        </xs:complexType>
                                      </xs:element>
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element maxOccurs=""10"" name=""g038"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name=""ALC"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element name=""e01_5463"" type=""xs:string"" />
                                      <xs:element name=""cmp02"">
                                        <xs:complexType>
                                          <xs:sequence>
                                            <xs:element name=""e01_7161"" type=""xs:string"" />
                                            <xs:element name=""e04_7160"" type=""xs:string"" />
                                          </xs:sequence>
                                        </xs:complexType>
                                      </xs:element>
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                                <xs:element name=""g040"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element name=""PCD"">
                                        <xs:complexType>
                                          <xs:sequence>
                                            <xs:element name=""cmp01"">
                                              <xs:complexType>
                                                <xs:sequence>
                                                  <xs:element name=""e01_5245"" type=""xs:string"" />
                                                  <xs:element name=""e02_5482"" type=""xs:string"" />
                                                </xs:sequence>
                                              </xs:complexType>
                                            </xs:element>
                                          </xs:sequence>
                                        </xs:complexType>
                                      </xs:element>
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                                <xs:element name=""g041"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element name=""MOA"">
                                        <xs:complexType>
                                          <xs:sequence>
                                            <xs:element name=""cmp01"">
                                              <xs:complexType>
                                                <xs:sequence>
                                                  <xs:element name=""e01_5025"" type=""xs:string"" />
                                                  <xs:element name=""e02_5004"" type=""xs:string"" />
                                                </xs:sequence>
                                              </xs:complexType>
                                            </xs:element>
                                          </xs:sequence>
                                        </xs:complexType>
                                      </xs:element>
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""1"" maxOccurs=""1"" name=""UNS"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name=""e01_0081"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""1"" maxOccurs=""10"" name=""CNT"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name=""cmp01"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name=""e01_6069"" type=""xs:string"" />
                                <xs:element name=""e02_6066"" type=""xs:string"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""1"" maxOccurs=""unbounded"" name=""g048"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name=""MOA"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name=""cmp01"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element name=""e01_5025"" type=""xs:string"" />
                                      <xs:element name=""e02_5004"" type=""xs:string"" />
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""1"" maxOccurs=""unbounded"" name=""g050"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name=""TAX"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name=""e01_5283"" type=""xs:string"" />
                                <xs:element name=""cmp01"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element name=""e01_5153"" type=""xs:string"" />
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                                <xs:element name=""cmp03"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element name=""e04_5278"" type=""xs:string"" />
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                                <xs:element name=""e03_5305"" type=""xs:string"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element name=""MOA"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name=""cmp01"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element name=""e01_5025"" type=""xs:string"" />
                                      <xs:element name=""e02_5004"" type=""xs:string"" />
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""1"" maxOccurs=""1"" name=""UNT"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name=""e01_0074"" type=""xs:string"" />
                          <xs:element name=""e02_0062"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""1"" maxOccurs=""1"" name=""UNZ"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name=""e01_0036"" type=""xs:string"" />
                          <xs:element name=""e02_0020"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
</xs:schema>";
        
        public M3Invoice() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [1];
                _RootElements[0] = "Envelope";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
    }
}
