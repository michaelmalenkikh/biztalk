namespace BYGE.Integration.M3Invoice {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.Core.Invoice.Invoice", typeof(global::BYGE.Integration.Core.Invoice.Invoice))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.M3Invoice.M3Invoice", typeof(global::BYGE.Integration.M3Invoice.M3Invoice))]
    public sealed class CoreInvoice_to_M3Invoice_Copy : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" xmlns:userCSharp=""http://schemas.microsoft.com/BizTalk/2003/userCSharp"" exclude-result-prefixes=""msxsl var s0 userCSharp"" version=""1.0"" xmlns:s0=""http://byg-e.dk/schemas/v10"" xmlns=""http://www.intentia.com/MBM"" xmlns:env=""http://www.intentia.com/MBM_Envelope"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" >
  <xsl:output omit-xml-declaration=""no"" method=""xml"" version=""1.0"" encoding=""UTF-8"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/s0:Invoice"" />
  </xsl:template>   
  <!-- BDB/SJ 191213: If no Item no. don't make e01_7140 og e02_7143-->
  <!-- BDB/SJ 191213: NAD+DP was lost (spelling mistake) -->
  <!-- BDB/SJ 191213: go15/g019 ALC/MOA+25 removed if = MOA+8 -->
  <!-- BDB/SJ 191213: AllowanceChargeReason in head added -->
  <!-- BDB/SJ 191216: DeliveryLocation/AdditionalStreetName added. Also in AccountingCustomer and BuyerCustomer -->
  <!-- BDB/SJ 191219: e01_3042 changed to e01_3042, e02_3042 and e03_3042 -->
  <!-- BDB/SJ 191219: PaymentMeans RFF+FIK added -->
  <!-- BDB/SJ 191220: If no BuyerCustomer/Address/Name, don't use from DeliveryLocation. -->
  <!-- BDB/SJ 191220: If no DeliveryLocation/DeliveryID, don't use from BuyerCustomer. -->
  <!-- BDB/SJ 191220: Concact -Telephone, Telefax, E-mail and Note added. -->
  <!-- BDB/SJ 191227: If no BuyerCustomer//PartyID or Address/Name, use from AccountingCustomer -->
  <!-- BDB/SJ 191227: MOA+176 adjusted -->
  <!-- BDB/SJ 200103: UTS added. Check on Variable $aUTS -->
  <!-- BDB/SJ 200106: Function NewGuidString and Variable $aUUID added -->
  <!-- BDB/SJ 200107: Function unitedPeriod added -->
  <!-- BDB/SJ 200109: Crenot added. Error NAD+BY name adjusted -->
  <!-- BDB/SJ 200113: Manifest/description adjusted. If TDC - g038 removed. PaymentMeans/PaymentDueDate added. MOA+125 adjusted. -->
  <!-- BDB/SJ 200113: If UTS -g005/CTA removed in NAD+BY -->
  <!-- BDB/SJ 200115: Replace("","",""."") added on CSharp, when decimal returns. -->
  <!-- BDB/SJ 200117: DeliveryLocation -If no Name, use MarkAttention else empty e01_3036-tag.  -->
  <!-- BDB/SJ 200117: If Soroto use Item Description not Name -->
  <!-- BDB/SJ 200122: Test if any PRI+AAB added -->
  <!-- BDB/SJ 200128: Spelling mistake ajusted (PRI+AAB) -->
  <!-- BDB/SJ 200225: Missing g008 PAT and DTM+13 in UTS - Wrong totals in UTS -->
  <!-- BDB/SJ 200320: AddDecAmount added -->
  <!-- BDB/SJ 200323: RemovesignAmount added -->
  <!-- BDB/SJ 200324: Payment in UTS added -->
  <!-- BDB/SJ 200515: 200128: Spelling mistake ajusted (PRI+AAB) again -->
  <!-- BDB/SJ 200604: Translate 5790001341633 (Fog/Byg-e) to 5790001679798 (Fog/DUC). -->
  <!-- BDB/MMA 200702: Using TaxTotal/TaxSubtotal/TaxCategory/TaxScheme when there is content -->
  <!-- BDB/SJ 200803: TaxTotal/TaxSubtotal/TaxCategory/TaxScheme - 7161 is always VAT and 5482 is always 25.00 -->
  <!-- BDB/SJ 200818: AllowanceChargeReason if no AllowanceChargeReasonCode on lines -->
  <!-- BDB/SJ 200818: Wrong use of ChargeIndicator adjusted (We use it differently in ingoing EDIFACT and XML) -->
  <!-- BDB/SJ 200827: If Truelink (5790001659578)- AccountingSupplier use PartyID not AccSellerID -->
  <!-- BDB/SJ 200924: If Swedoor Jeld-Wen (5790000256044) - AccountingSupplier use PartyID not AccSellerID -->
  <!-- BDB/SJ 200925: If RFF+ON has no content - use 'xxx' -->
  <!-- BDB/SJ 200926: Alborg Portland (DK36428112) some ALC moved from line to Header -->
  <!-- BDB/SJ 201006: If Sproom (5790001405458) - AccountingSupplier use CompanyID not AccSellerID -->
  <!-- BDB/SJ 201008: Check if AccountingSupplier/CompanyID has a content - added -->
  <!-- BDB/xx YYMMDD: -->
  <xsl:template match=""/s0:Invoice"">
    <!-- The Envelope -->
    <Envelope>
      <!-- Extra attribute -->
      <xsl:attribute name=""xsi:schemaLocation"">
        <xsl:value-of select=""'http://www.intentia.com/MBM MBM_INVOIC.xsd'"" />
      </xsl:attribute>
      <!-- Variable to keep date time of today -->
      <xsl:variable name=""aCurrDateTime"" select=""userCSharp:CurrentDateTime()"" />
      <!-- Variable to check if it is an Invoice or an UTS -->
      <xsl:variable name=""aUTS"" select=""InvoiceType/text()"" />
      <!-- Variable for UUID if empty -->
      <xsl:variable name=""aUUID"" select=""userCSharp:NewGuidString()"" />
      <!-- Variable for AccSeller -->
      <xsl:variable name=""aAccSeller"" select=""AccountingSupplier/AccSellerID/text()"" /> 
      <Header>
        <env:delivery>
          <!-- BDB/SJ 200604: Translate 5790001341633 (Fog/Byg-e) to 5790001679798 (Fog/DUC). -->
          <env:to>
            <env:address>
              <xsl:if test=""AccountingCustomer/AccBuyerID = '5790001341633'"">
                <xsl:value-of select=""'5790001679798'"" />
              </xsl:if>
              <xsl:if test=""AccountingCustomer/AccBuyerID != '5790001341633'"">
                <xsl:value-of select=""AccountingCustomer/AccBuyerID/text()"" />
              </xsl:if>
            </env:address>
          </env:to>
          <env:from>
            <env:address>
              <!-- Check if document is from TDC -->
              <xsl:if test=""$aAccSeller != 'DK40075291'"">
                <xsl:value-of select=""'BYG-E'"" />
              </xsl:if> 
              <xsl:if test=""$aAccSeller = 'DK40075291'"">
                <xsl:value-of select=""'BYG-E_TDC'"" />
              </xsl:if>
            </env:address>
          </env:from>
          <env:reliability>
            <env:sendReceiptTo/>
            <env:receiptRequiredBy/>
          </env:reliability>
        </env:delivery>
        <env:properties>
          <env:identity>
            <xsl:if test=""string-length(UUID) != 0"">
              <xsl:value-of select=""UUID/text()"" />
            </xsl:if> 
            <xsl:if test=""string-length(UUID) = 0"">
              <xsl:value-of select=""$aUUID"" />
            </xsl:if>
          </env:identity>
          <env:sentAt>
            <!-- xsl:value-of select=""concat(IssueDate/text(),'T00:00:00+01:00')"" / -->
            <xsl:value-of select=""$aCurrDateTime"" />
          </env:sentAt>
          <env:expiresAt/>
          <env:topic>
            <xsl:value-of select=""'http://www.intentia.com/MBM/'"" />
          </env:topic>
        </env:properties>
        <env:manifest>
          <env:reference>
            <xsl:attribute name=""uri"">
              <xsl:value-of select=""'INVOIC'"" />
            </xsl:attribute>
            <env:description>
              <!-- BDB/SJ 200113: Manifest/description adjusted -->
              <!-- Check if document is from TDC -->
              <xsl:if test=""$aAccSeller != 'DK40075291'"">
                <xsl:value-of select=""'EDIPRO-1.0-BYG-E'"" />
              </xsl:if>
              <xsl:if test=""$aAccSeller = 'DK40075291'"">
                <xsl:value-of select=""'BYG-E_TDC'"" />
              </xsl:if>
            </env:description>
          </env:reference>
        </env:manifest>
        <env:process>
          <env:type/>
          <env:instance/>
          <env:handle/>
        </env:process>
      </Header>
      <!-- The Invoice, Creditnote or Utillity Statment -->
      <Body>
        <INVOIC>
          <!-- Variable to keep issueDate for later use -->
          <xsl:variable name=""aIssueDate"" select=""IssueDate/text()"" />
          <!-- UNB -->
          <UNB>
            <cmp01>
              <e01_0001>
                <xsl:value-of select=""'UNOC'"" />
              </e01_0001>
              <e02_0002>
                <xsl:value-of select=""'3'"" />
              </e02_0002>
            </cmp01>
            <cmp02>
              <e01_0004>
                <xsl:value-of select=""AccountingSupplier/AccSellerID/text()"" />
              </e01_0004>
            </cmp02>
            <!-- BDB/SJ 200604: Translate 5790001341633 (Fog/Byg-e) to 5790001679798 (Fog/DUC). -->
            <cmp03>
              <e01_0010>
                <xsl:if test=""AccountingCustomer/AccBuyerID = '5790001341633'"">
                  <xsl:value-of select=""'5790001679798'"" />
                </xsl:if>
                <xsl:if test=""AccountingCustomer/AccBuyerID != '5790001341633'"">
                  <xsl:value-of select=""AccountingCustomer/AccBuyerID/text()"" />
                </xsl:if> 
              </e01_0010>
            </cmp03>
            <cmp04>
              <e01_0017>
               <xsl:value-of select=""userCSharp:editDate($aCurrDateTime)"" />
              </e01_0017>
              <e02_0019>
                <xsl:value-of select=""userCSharp:editTime($aCurrDateTime)"" />
              </e02_0019>
            </cmp04>
            <e01_0020>
              <xsl:if test=""string-length(UUID) != 0"">
                <xsl:value-of select=""UUID/text()"" />
              </xsl:if>
              <xsl:if test=""string-length(UUID) = 0"">
                <xsl:value-of select=""$aUUID"" />
              </xsl:if>
            </e01_0020>
            <e04_0031>
              <xsl:value-of select=""'0'"" />
            </e04_0031>
            <e05_0032/>
            <e06_0035>
              <xsl:value-of select=""'0'"" />
            </e06_0035>
          </UNB>
          <!-- UNH -->
          <UNH>
            <e01_0062>
              <xsl:if test=""string-length(UUID) != 0"">
                <xsl:value-of select=""UUID/text()"" />
              </xsl:if>
              <xsl:if test=""string-length(UUID) = 0"">
                <xsl:value-of select=""$aUUID"" />
              </xsl:if>
            </e01_0062>
            <cmp01>
              <e01_0065>
                <xsl:value-of select=""'INVOIC'"" />
              </e01_0065>
              <e02_0052>
                <xsl:value-of select=""'D'"" />
              </e02_0052>
              <e03_0054>
                <xsl:value-of select=""'96A'"" />
              </e03_0054>
              <e04_0051>
                <xsl:value-of select=""'UN'"" />
              </e04_0051>
              <e05_0057>
                <xsl:value-of select=""'EAN008'"" />
              </e05_0057>
            </cmp01>
          </UNH>
          <!-- BGM -->
          <BGM>
            <cmp01>
              <xsl:if test=""string-length(InvoiceType) != 0"">
                <e01_1001>
                  <xsl:if test=""$aUTS != 'Tele'"">
                    <xsl:value-of select=""InvoiceType/text()"" />
                  </xsl:if> 
                  <xsl:if test=""$aUTS = 'Tele'"">
                    <xsl:value-of select=""'380'"" />
                  </xsl:if> 
                </e01_1001>
                <e03_3055>
                  <xsl:value-of select=""'9'"" />
                </e03_3055>
              </xsl:if>
            </cmp01>
            <e01_1004>
              <xsl:value-of select=""InvoiceID/text()"" />
            </e01_1004>
            <e02_1225>
              <xsl:value-of select=""'9'"" />
            </e02_1225>
          </BGM>
          <!-- DTM -->
          <DTM>
            <cmp01>
              <e01_2005>
                <xsl:value-of select=""'137'"" />
              </e01_2005>
              <e02_2380>
                <xsl:value-of select=""userCSharp:editDate($aIssueDate)"" />
              </e02_2380>
              <e03_2379>
                <xsl:value-of select=""'102'"" />
              </e03_2379>
            </cmp01>
          </DTM>
          <xsl:if test=""$aUTS != 'Tele'"">
            <DTM>
              <cmp01>
                <e01_2005>
                  <xsl:value-of select=""'35'"" />
                </e01_2005>
                <e02_2380>
                  <xsl:value-of select=""userCSharp:editDate($aIssueDate)"" />
                </e02_2380>
                <e03_2379>
                  <xsl:value-of select=""'102'"" />
                </e03_2379>
              </cmp01>
            </DTM>
          </xsl:if>  
          <DTM>
            <cmp01>
              <e01_2005>
                <xsl:value-of select=""'9'"" />
              </e01_2005>
              <e02_2380>
                <xsl:value-of select=""userCSharp:editDate($aCurrDateTime)"" />
              </e02_2380>
              <e03_2379>
                <xsl:value-of select=""'102'"" />
              </e03_2379>
            </cmp01>
          </DTM>
          <!-- FTX -->
          <xsl:for-each select=""Note"">
            <xsl:if test=""string-length(.) != 0"">
              <FTX>
                <e01_4451>
                  <xsl:value-of select=""'ZZZ'"" />
                </e01_4451>
                <cmp02>
                  <e01_4440>
                    <xsl:value-of select=""userCSharp:splitNote1(.)"" />
                  </e01_4440>
                  <e02_4440>
                    <xsl:value-of select=""userCSharp:splitNote2(.)"" />
                  </e02_4440>
                  <e03_4440>
                    <xsl:value-of select=""userCSharp:splitNote3(.)"" />
                  </e03_4440>
                  <e04_4440>
                    <xsl:value-of select=""userCSharp:splitNote4(.)"" />
                  </e04_4440>
                  <e05_4440>
                    <xsl:value-of select=""userCSharp:splitNote5(.)"" />
                  </e05_4440>
                </cmp02>
              </FTX>
            </xsl:if>    
          </xsl:for-each>   
          <!-- g001 RFF -->
          <g001>
            <RFF>
              <cmp01>
                <e01_1153>
                  <xsl:value-of select=""'ON'"" />
                </e01_1153>
                <!-- BDB/SJ 200925: If RFF+ON has no content - use 'xxx' -->
                <e02_1154>
                  <xsl:if test=""string-length(OrderReference/OrderID) != 0"">
                    <xsl:value-of select=""OrderReference/OrderID/text()"" />
                  </xsl:if>
                  <xsl:if test=""string-length(OrderReference/OrderID) = 0"">
                    <xsl:value-of select=""'xxx'"" />
                  </xsl:if>
                </e02_1154>
              </cmp01>
            </RFF>
            <DTM>
              <cmp01>
                <e01_2005>
                  <xsl:value-of select=""'171'"" />
                </e01_2005>
                <e02_2380>
                  <xsl:if test=""string-length(OrderReference/OrderDate) != 0"">
                    <xsl:value-of select=""userCSharp:editDate(OrderReference/OrderDate/text())"" />
                  </xsl:if>
                  <xsl:if test=""string-length(OrderReference/OrderDate) = 0"">
                    <xsl:value-of select=""userCSharp:editDate($aIssueDate)"" />
                  </xsl:if>
                </e02_2380>
                <e03_2379>
                  <xsl:value-of select=""'102'"" />
                </e03_2379>
              </cmp01>
            </DTM>
          </g001>
          <xsl:if test=""string-length(OrderReference/SalesOrderID) != 0"">
            <g001>
              <RFF>
                <cmp01>
                  <e01_1153>
                    <xsl:value-of select=""'VN'"" />
                  </e01_1153>
                  <e02_1154>
                    <xsl:value-of select=""OrderReference/SalesOrderID/text()"" />
                  </e02_1154>
                </cmp01>
              </RFF>
            </g001>
          </xsl:if> 
          <xsl:if test=""string-length(DespatchDocumentReference/ID) != 0"">
            <g001>
              <RFF>
                <cmp01>
                  <e01_1153>
                    <xsl:value-of select=""'AAU'"" />
                  </e01_1153>
                  <e02_1154>
                    <xsl:value-of select=""DespatchDocumentReference/ID/text()"" />
                  </e02_1154>
                </cmp01>
              </RFF>
            </g001>
          </xsl:if>
          <!-- BDB/SJ 191219: PaymentMeans RFF+FIK added -->
          <xsl:if test=""string-length(PaymentMeans/PaymentID) != 0"">
            <xsl:if test=""PaymentMeans/PaymentID/text() = '71'"">
              <g001>
                <RFF>
                  <cmp01>
                    <e01_1153>
                      <xsl:value-of select=""'FIK'"" />
                    </e01_1153>
                    <e02_1154>
                      <xsl:value-of select=""userCSharp:FIKline((PaymentMeans/PaymentID), (PaymentMeans/InstructionID), (PaymentMeans/CreditAccountID))"" />
                    </e02_1154>
                  </cmp01>
                </RFF>
              </g001>
            </xsl:if>  
          </xsl:if>
          <xsl:if test=""string-length(OrderReference/CustomerReference) != 0"">
            <g001>
              <RFF>
                <cmp01>
                  <e01_1153>
                    <xsl:value-of select=""'CR'"" />
                  </e01_1153>
                  <e02_1154>
                    <xsl:value-of select=""OrderReference/CustomerReference/text()"" />
                  </e02_1154>
                </cmp01>
              </RFF>
            </g001>
          </xsl:if>
          <!-- g002 NAD BuyerCustomer -->
          <g002>
            <NAD>
              <e01_3035>
                <xsl:value-of select=""'BY'"" />
              </e01_3035>
              <!-- BDB/SJ 191227: If no BuyerCustomer/PartyID, use from AccountingCustomer -->
              <cmp01>
                <e01_3039>
                  <xsl:if test=""string-length(BuyerCustomer/PartyID) != 0"">
                    <xsl:value-of select=""BuyerCustomer/PartyID/text()"" />
                  </xsl:if> 
                  <xsl:if test=""string-length(BuyerCustomer/PartyID) = 0"">
                    <xsl:value-of select=""AccountingCustomer/PartyID/text()"" />
                  </xsl:if> 
                </e01_3039>
                <e03_3055>
                  <xsl:value-of select=""'9'"" />
                </e03_3055>
              </cmp01>
              <!-- BDB/SJ 191220: If no BuyerCustomer/Address/Name, don't use from DeliveryLocation. -->
              <!-- BDB/SJ 191227: If no BuyerCustomer/Address/Name, use from AccountingCustomer -->
              <cmp03>
                <e01_3036>
                  <xsl:if test=""string-length(BuyerCustomer/Address/Name) != 0"">
                    <xsl:value-of select=""BuyerCustomer/Address/Name/text()"" />
                  </xsl:if> 
                  <xsl:if test=""string-length(BuyerCustomer/Address/Name) = 0"">
                    <xsl:value-of select=""AccountingCustomer/Address/Name/text()"" />
                  </xsl:if> 
                </e01_3036>
              </cmp03>
              <xsl:if test=""$aUTS != 'Tele'"">
                <!-- If received in EDIFACT the content are in wrong segments -start -->
                <xsl:if test=""string-length(BuyerCustomer/Address/Department) != 0"">
                  <cmp04>
                    <e01_3042>
                      <xsl:value-of select=""BuyerCustomer/Address/Department/text()"" />   
                    </e01_3042>
                  </cmp04>
                  <e02_3164>
                    <xsl:value-of select=""BuyerCustomer/Address/Number/text()"" />              
                  </e02_3164>
                  <e04_3251>
                    <xsl:value-of select=""BuyerCustomer/Address/Street/text()"" />              
                  </e04_3251>            
                </xsl:if>
                <!-- If received in EDIFACT the content are in wrong segments -end -->
                <!-- BDB/SJ 191219: e01_3042 changed to e01_3042, e02_3042 and e03_3042 -->
                <xsl:if test=""string-length(BuyerCustomer/Address/Department) = 0"">  
                  <xsl:if test=""string-length(BuyerCustomer/Address/Street) != 0"">  
                    <cmp04>
                      <e01_3042>
                        <!-- xsl:value-of select=""BuyerCustomer/Address/Street/text(), ' ', BuyerCustomer/Address/Number/text()""  / -->
                        <xsl:value-of select=""userCSharp:unitedText((BuyerCustomer/Address/Street), (BuyerCustomer/Address/Number))"" />
                      </e01_3042>
                      <!-- BDB/SJ 191216: DeliveryLocation/AdditionalStreetName added. Also in AccountingCustomer and BuyerCustomer -->
                      <xsl:if test=""string-length(BuyerCustomer/Address/AdditionalStreetName) != 0"">
                        <e02_3042>
                          <xsl:value-of select=""BuyerCustomer/Address/AdditionalStreetName/text()"" />  
                        </e02_3042>
                      </xsl:if>
                    </cmp04>
                    <e02_3164>
                      <xsl:value-of select=""BuyerCustomer/Address/City/text()"" />              
                    </e02_3164>
                    <e04_3251>
                      <xsl:value-of select=""BuyerCustomer/Address/PostalCode/text()"" />              
                    </e04_3251>
                    <e05_3207>
                      <xsl:if test=""string-length(BuyerCustomer/Address/Country) != 0"">  
                        <xsl:value-of select=""BuyerCustomer/Address/Country/text()"" />
                      </xsl:if>  
                      <xsl:if test=""string-length(BuyerCustomer/Address/Country) = 0"">  
                        <xsl:value-of select=""'DK'"" />
                      </xsl:if> 
                    </e05_3207>
                  </xsl:if> 
                </xsl:if>
              </xsl:if>
            </NAD>
            <!-- BDB/SJ 200113: If UTS -g005/CTA removed in NAD+BY-->
            <xsl:if test=""$aUTS != 'Tele'"">
              <g005>
                <CTA>
                  <e01_3139>
                    <xsl:if test=""string-length(BuyerCustomer/Concact/ID) != 0"">
                      <xsl:choose>
                        <xsl:when test=""BuyerCustomer/Concact/ID = 'OC'"">
                          <xsl:value-of select=""'PD'"" />
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select=""BuyerCustomer/Concact/ID"" />
                        </xsl:otherwise>
                      </xsl:choose> 
                    </xsl:if>   
                    <xsl:if test=""string-length(BuyerCustomer/Concact/ID) = 0"">
                      <xsl:value-of select=""'PD'"" />
                    </xsl:if> 
                  </e01_3139>
                  <cmp01>
                    <e02_3412>
                      <xsl:if test=""string-length(BuyerCustomer/Concact/Name) != 0"">
                        <xsl:value-of select=""BuyerCustomer/Concact/Name/text()"" />
                      </xsl:if> 
                      <xsl:if test=""string-length(BuyerCustomer/Concact/Name) = 0"">
                        <xsl:value-of select=""'n/a'"" />
                      </xsl:if>  
                    </e02_3412>
                  </cmp01>
                </CTA>
                <!-- BDB/SJ 191220: Concact -Telephone, Telefax, E-mail and Note added. -->
                <xsl:if test=""string-length(BuyerCustomer/Concact/Telephone) != 0"">
                  <COM>
                    <cmp01>
                      <e01_3148>
                        <xsl:value-of select=""BuyerCustomer/Concact/Telephone/text()"" />                   
                      </e01_3148>
                      <e02_3155>
                        <xsl:value-of select=""'TE'"" />
                      </e02_3155>
                    </cmp01>
                  </COM>
                 </xsl:if>
                 <xsl:if test=""string-length(BuyerCustomer/Concact/Telefax) != 0"">
                  <COM>
                    <cmp01>
                      <e01_3148>
                        <xsl:value-of select=""BuyerCustomer/Concact/Telefax/text()"" />                   
                      </e01_3148>
                      <e02_3155>
                        <xsl:value-of select=""'TX'"" />
                      </e02_3155>
                    </cmp01>
                  </COM>
                 </xsl:if>
                 <xsl:if test=""string-length(BuyerCustomer/Concact/E-mail) != 0"">
                  <COM>
                    <cmp01>
                      <e01_3148>
                        <xsl:value-of select=""BuyerCustomer/Concact/E-mail/text()"" />
                      </e01_3148>
                      <e02_3155>
                        <xsl:value-of select=""'EM'"" />
                      </e02_3155>
                    </cmp01>
                  </COM>
                </xsl:if>
                <xsl:if test=""string-length(BuyerCustomer/Concact/Note) != 0"">
                  <COM>
                    <cmp01>
                      <e01_3148>
                        <xsl:value-of select=""BuyerCustomer/Concact/Note/text()"" />
                      </e01_3148>
                    </cmp01>
                  </COM>
                </xsl:if>
              </g005>
            </xsl:if>
            <!-- BDB/SJ 200323: If UTS -g005/CTA in NAD+BY -->
            <xsl:if test=""$aUTS = 'Tele'"">
              <g005>
                <CTA>
                  <e01_3139>
                    <xsl:value-of select=""'PD'"" />
                  </e01_3139>
                  <cmp01>
                    <e02_3412>
                      <xsl:if test=""string-length(BuyerCustomer/Concact/ID) != 0"">
                       <xsl:value-of select=""BuyerCustomer/Concact/ID/text()"" />
                      </xsl:if>
                      <xsl:if test=""string-length(BuyerCustomer/Concact/ID) = 0"">
                        <xsl:value-of select=""'n/a'"" />
                      </xsl:if>
                    </e02_3412>
                  </cmp01>
                </CTA>
              </g005>
            </xsl:if>
          </g002>
          <!-- g002 NAD AccountingSupplier -->
          <g002>
            <NAD>
              <e01_3035>
                <xsl:value-of select=""'SU'"" />
              </e01_3035>
              <cmp01>
                <e01_3039>
                  <!-- BDB/SJ 200827: If Truelink (5790001659578)- AccountingSupplier use PartyID not AccSellerID -->
                  <!-- BDB/SJ 200924: If Swedoor Jeld-Wen (5790000256044) - AccountingSupplier use PartyID not AccSellerID -->
                  <!-- BDB/SJ 201006: If Sproom (5790001405458) - AccountingSupplier use CompanyID not AccSellerID -->
                  <xsl:choose>
                    <xsl:when test=""AccountingSupplier/AccSellerID = '5790001659578'"">
                      <xsl:value-of select=""AccountingSupplier/PartyID/text()"" />
                    </xsl:when>
                    <xsl:when test=""AccountingSupplier/AccSellerID = '5790000256044'"">
                      <xsl:value-of select=""AccountingSupplier/PartyID/text()"" />
                    </xsl:when>
                    <xsl:when test=""AccountingSupplier/AccSellerID = '5790001405458'"">
                      <xsl:value-of select=""userCSharp:editCompanyID(AccountingSupplier/CompanyID)"" />
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select=""AccountingSupplier/AccSellerID/text()"" />
                    </xsl:otherwise>
                  </xsl:choose>
                </e01_3039>
                <e03_3055>
                  <xsl:value-of select=""'9'"" />
                </e03_3055>
              </cmp01>
              <cmp03>
                <e01_3036>
                  <xsl:value-of select=""AccountingSupplier/Address/Name/text()"" />
                </e01_3036>
              </cmp03>
              <!-- If received in EDIFACT the content are in wrong segments -start -->
              <xsl:if test=""string-length(AccountingSupplier/Address/Department) != 0"">
                <cmp04>
                  <e01_3042>
                    <xsl:value-of select=""AccountingSupplier/Address/Department/text()"" />   
                  </e01_3042>
                </cmp04>
                <e02_3164>
                  <xsl:value-of select=""AccountingSupplier/Address/Number/text()"" />              
                </e02_3164>
                <e04_3251>
                  <xsl:value-of select=""AccountingSupplier/Address/Street/text()"" />              
                </e04_3251>            
              </xsl:if>
              <!-- If received in EDIFACT the content are in wrong segments -end -->
              <!-- BDB/SJ 191219: e01_3042 changed to e01_3042, e02_3042 and e03_3042 -->
              <xsl:if test=""string-length(AccountingSupplier/Address/Department) = 0""> 
                <xsl:if test=""string-length(AccountingSupplier/Address/Street) != 0""> 
                  <cmp04>
                    <e01_3042>
                      <!-- xsl:value-of select=""AccountingSupplier/Address/Street/text(), ' ', AccountingSupplier/Address/Number/text()""  / -->
                      <xsl:value-of select=""userCSharp:unitedText((AccountingSupplier/Address/Street), (AccountingSupplier/Address/Number))"" />
                    </e01_3042>
                    <xsl:if test=""string-length(AccountingSupplier/Address/AdditionalStreetName) != 0"">
                      <e02_3042>
                        <xsl:value-of select=""AccountingSupplier/Address/AdditionalStreetName/text()"" />
                      </e02_3042>
                    </xsl:if>
                  </cmp04>
                  <e02_3164>
                    <xsl:value-of select=""AccountingSupplier/Address/City/text()"" />              
                  </e02_3164>
                  <e04_3251>
                    <xsl:value-of select=""AccountingSupplier/Address/PostalCode/text()"" />              
                  </e04_3251>
                  <e05_3207>
                    <xsl:if test=""string-length(AccountingSupplier/Address/Country) != 0"">  
                      <xsl:value-of select=""AccountingSupplier/Address/Country/text()"" />
                    </xsl:if>  
                   <xsl:if test=""string-length(AccountingSupplier/Address/Country) = 0"">  
                     <xsl:value-of select=""'DK'"" />
                    </xsl:if> 
                  </e05_3207>
                </xsl:if> 
              </xsl:if>  
            </NAD>
            <g003>
              <RFF>
                <cmp01>
                  <e01_1153>
                    <xsl:value-of select=""'VA'"" />
                  </e01_1153>
                  <e02_1154>
                    <!-- BDB/SJ 201008: Check if AccountingSupplier/CompanyID has a content - added -->
                    <xsl:if test=""string-length(AccountingSupplier/CompanyID) != 0"">
                      <xsl:value-of select=""userCSharp:editCompanyID(AccountingSupplier/CompanyID)"" />
                    </xsl:if>
                    <xsl:if test=""string-length(AccountingSupplier/CompanyID) = 0"">
                      <xsl:value-of select=""'n/a'"" />
                    </xsl:if>
                  </e02_1154>
                </cmp01>
              </RFF>
            </g003>
            <!-- BDB/SJ 200323: If UTS -g005/CTA in NAD+SU -->           
            <g005>
              <CTA>
                <e01_3139>
                  <xsl:if test=""string-length(AccountingSupplier/Concact/ID) != 0"">
                    <xsl:choose>
                      <xsl:when test=""$aUTS = 'Tele'"">
                        <xsl:value-of select=""'AD'"" />
                      </xsl:when>
                      <xsl:when test=""AccountingSupplier/Concact/ID = 'PD'"">
                        <xsl:value-of select=""'AD'"" />
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select=""AccountingSupplier/Concact/ID/text()"" />
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:if>    
                  <xsl:if test=""string-length(AccountingSupplier/Concact/ID) = 0"">
                    <xsl:value-of select=""'AD'"" />
                  </xsl:if>    
                </e01_3139>
                <cmp01>
                  <e02_3412>
                    <xsl:if test=""string-length(AccountingSupplier/Concact/Name) != 0"">
                      <xsl:value-of select=""AccountingSupplier/Concact/Name/text()"" />
                    </xsl:if> 
                    <xsl:if test=""string-length(AccountingSupplier/Concact/Name) = 0"">
                      <xsl:if test=""$aUTS != 'Tele'"">
                        <xsl:value-of select=""'n/a'"" />
                      </xsl:if>
                      <xsl:if test=""$aUTS = 'Tele'"">
                        <xsl:value-of select=""AccountingSupplier/Concact/ID/text()"" />
                      </xsl:if>
                    </xsl:if>  
                  </e02_3412>
                </cmp01>
              </CTA>
              <!-- BDB/SJ 191220: Concact -Telephone, Telefax, E-mail and Note added. -->                  
               <xsl:if test=""string-length(AccountingSupplier/Concact/Telephone) != 0"">
                <COM>
                  <cmp01>
                    <e01_3148>
                      <xsl:value-of select=""AccountingSupplier/Concact/Telephone/text()"" />                   
                    </e01_3148>
                    <e02_3155>
                      <xsl:value-of select=""'TE'"" />
                    </e02_3155>
                  </cmp01>
                </COM>
               </xsl:if>
               <xsl:if test=""string-length(AccountingSupplier/Concact/Telefax) != 0"">
                <COM>
                  <cmp01>
                    <e01_3148>
                      <xsl:value-of select=""AccountingSupplier/Concact/Telefax/text()"" />                   
                    </e01_3148>
                    <e02_3155>
                      <xsl:value-of select=""'TX'"" />
                    </e02_3155>
                  </cmp01>
                </COM>
               </xsl:if>
               <xsl:if test=""string-length(AccountingSupplier/Concact/E-mail) != 0"">
                <COM>
                  <cmp01>
                    <e01_3148>
                      <xsl:value-of select=""AccountingSupplier/Concact/E-mail/text()"" />
                    </e01_3148>
                    <e02_3155>
                      <xsl:value-of select=""'EM'"" />
                    </e02_3155>
                  </cmp01>
                </COM>
              </xsl:if>
              <xsl:if test=""string-length(AccountingSupplier/Concact/Note) != 0"">
                <COM>
                  <cmp01>
                    <e01_3148>
                      <xsl:value-of select=""AccountingSupplier/Concact/Note/text()"" />
                    </e01_3148>
                  </cmp01>
                </COM>
              </xsl:if>
            </g005>
          </g002>
          <!-- g002 NAD AccountingCustomer -->
          <g002>
            <NAD>
              <e01_3035>
                <xsl:value-of select=""'IV'"" />
              </e01_3035>
              <cmp01>
                <!-- BDB/SJ 200604: Translate 5790001341633 (Fog/Byg-e) to 5790001679798 (Fog/DUC). -->
                <e01_3039>
                  <xsl:if test=""AccountingCustomer/AccBuyerID = '5790001341633'"">
                    <xsl:value-of select=""'5790001679798'"" />
                  </xsl:if>
                  <xsl:if test=""AccountingCustomer/AccBuyerID != '5790001341633'"">
                    <xsl:value-of select=""AccountingCustomer/AccBuyerID/text()"" />
                  </xsl:if>
                </e01_3039>
                <e03_3055>
                  <xsl:value-of select=""'9'"" />
                </e03_3055>
              </cmp01>
              <cmp03>
                <e01_3036>
                  <xsl:if test=""string-length(AccountingCustomer/Address/Name) != 0"">
                    <xsl:value-of select=""AccountingCustomer/Address/Name/text()"" />
                  </xsl:if>  
                  <xsl:if test=""string-length(AccountingCustomer/Address/Name) = 0"">
                    <xsl:value-of select=""'Johannes Fog A/S'"" />
                  </xsl:if>  
                </e01_3036>
              </cmp03>
              <xsl:if test=""$aUTS != 'Tele'"">
                <!-- If received in EDIFACT the content are in wrong segments -start -->
                <xsl:if test=""string-length(AccountingCustomer/Address/Department) != 0"">
                  <cmp04>
                    <e01_3042>
                      <xsl:value-of select=""AccountingCustomer/Address/Department/text()"" />   
                    </e01_3042>
                  </cmp04>
                  <e02_3164>
                    <xsl:value-of select=""AccountingCustomer/Address/Number/text()"" />              
                  </e02_3164>
                  <e04_3251>
                    <xsl:value-of select=""AccountingCustomer/Address/Street/text()"" />              
                  </e04_3251>            
                </xsl:if>
                <!-- If received in EDIFACT the content are in wrong segments -end -->
                <!-- BDB/SJ 191219: e01_3042 changed to e01_3042, e02_3042 and e03_3042 -->
                <xsl:if test=""string-length(AccountingCustomer/Address/Department) = 0""> 
                  <xsl:if test=""string-length(AccountingCustomer/Address/Street) != 0""> 
                    <cmp04>
                      <e01_3042>
                        <!-- xsl:value-of select=""AccountingCustomer/Address/Street/text(), ' ', AccountingCustomer/Address/Number/text()""  / -->
                        <xsl:value-of select=""userCSharp:unitedText((AccountingCustomer/Address/Street), (AccountingCustomer/Address/Number))"" />
                      </e01_3042>
                      <!-- BDB/SJ 191216: DeliveryLocation/AdditionalStreetName added. Also in AccountingCustomer and BuyerCustomer -->
                      <xsl:if test=""string-length(AccountingCustomer/Address/AdditionalStreetName) != 0"">
                        <e02_3042>
                          <xsl:value-of select=""AccountingCustomer/Address/AdditionalStreetName/text()"" />
                        </e02_3042>
                      </xsl:if>
                    </cmp04>
                    <e02_3164>
                      <xsl:value-of select=""AccountingCustomer/Address/City/text()"" />              
                    </e02_3164>
                    <e04_3251>
                      <xsl:value-of select=""AccountingCustomer/Address/PostalCode/text()"" />              
                    </e04_3251>
                    <e05_3207>
                      <xsl:if test=""string-length(AccountingCustomer/Address/Country) != 0"">  
                        <xsl:value-of select=""AccountingCustomer/Address/Country/text()"" />
                      </xsl:if>  
                      <xsl:if test=""string-length(AccountingCustomer/Address/Country) = 0"">  
                        <xsl:value-of select=""'DK'"" />
                      </xsl:if> 
                    </e05_3207>
                  </xsl:if>  
                </xsl:if>
              </xsl:if>  
            </NAD>
            <g003>
              <RFF>
                <cmp01>
                  <e01_1153>
                    <xsl:value-of select=""'VA'"" />
                  </e01_1153>
                  <e02_1154>
                    <xsl:if test=""string-length(AccountingCustomerCompanyID) != 0"">
                      <xsl:value-of select=""userCSharp:editCompanyID(AccountingCustomer/CompanyID)"" />
                    </xsl:if>  
                    <xsl:if test=""string-length(AccountingCustomerCompanyID) = 0"">
                      <xsl:value-of select=""'16314439'"" />
                    </xsl:if>  
                  </e02_1154>
                </cmp01>
              </RFF>
            </g003>
            <!-- g005/CTA/ never on AccountingCustomer -->
          </g002> 
          <!-- g002 NAD DeliveryLocation -->
          <g002>
            <NAD>
              <e01_3035>
                <xsl:value-of select=""'DP'"" />
              </e01_3035>
              <!-- BDB/SJ 191220: If no DeliveryLocation/DeliveryID, don't use from BuyerCustomer. -->
              <xsl:if test=""string-length(DeliveryLocation/DeliveryID) != 0"">
                <cmp01>
                  <e01_3039>
                    <xsl:value-of select=""DeliveryLocation/DeliveryID/text()"" />
                  </e01_3039>               
                  <e03_3055>
                    <xsl:value-of select=""'9'"" />
                  </e03_3055>
                </cmp01>
              </xsl:if>  
              <cmp03>
                <e01_3036>
                  <!-- BDB/SJ 200117: DeliveryLocation -If no Name, use MarkAttention else empty e01_3036-tag.  -->
                  <xsl:if test=""string-length(DeliveryLocation/Address/Name) != 0"">
                    <xsl:value-of select=""DeliveryLocation/Address/Name/text()"" />
                  </xsl:if>
                  <xsl:if test=""string-length(DeliveryLocation/Address/Name) = 0"">
                    <xsl:if test=""string-length(DeliveryLocation/Address/MarkAttention) = 0"">
                      <xsl:value-of select=""DeliveryLocation/Address/Name/text()"" />
                    </xsl:if>
                    <xsl:if test=""string-length(DeliveryLocation/Address/MarkAttention) != 0"">
                      <xsl:value-of select=""DeliveryLocation/Address/MarkAttention/text()"" />
                    </xsl:if>
                  </xsl:if>
                </e01_3036>
              </cmp03>
              <!-- Variable to check if received in EDIFACT -->
              <xsl:if test=""string-length(DeliveryLocation/Address/Department) != 0"">
                <xsl:if test=""string-length(DeliveryLocation/Address/City) != 0"">
                  <xsl:variable name=""aEdifact"" select=""'no'"" />
                </xsl:if>
                <xsl:if test=""string-length(DeliveryLocation/Address/City) = 0"">
                  <xsl:variable name=""aEdifact"" select=""'yes'"" />
                </xsl:if>
              </xsl:if>
              <xsl:if test=""string-length(DeliveryLocation/Address/Department) = 0"">
                  <xsl:variable name=""aEdifact"" select=""'no'"" />
              </xsl:if>
              <!-- If received in EDIFACT the content are in wrong segments -start -->
              <xsl:choose>
                <xsl:when test=""aEdifact = 'yes'"">
                  <cmp04>
                    <e01_3042>
                      <xsl:value-of select=""DeliveryLocation/Address/Department/text()"" />
                    </e01_3042>
                  </cmp04>
                  <e02_3164>
                    <xsl:value-of select=""DeliveryLocation/Address/Number/text()"" />
                  </e02_3164>
                  <e04_3251>
                    <xsl:value-of select=""DeliveryLocation/Address/Street/text()"" />
                  </e04_3251>
                </xsl:when>
                <!-- If received in EDIFACT the content are in wrong segments -end -->
                <!-- BDB/SJ 191213: NAD+DP was lost (spelling mistake) -->
                <!-- BDB/SJ 191216: DeliveryLocation/AdditionalStreetName added. Also in AccountingCustomer and BuyerCustomer -->
                <!-- BDB/SJ 191219: e01_3042 changed to e01_3042, e02_3042 and e03_3042 -->
                <xsl:otherwise>
                  <cmp04>
                    <xsl:if test=""string-length(DeliveryLocation/Address/Department) != 0"">
                      <e01_3042>
                        <xsl:value-of select=""DeliveryLocation/Address/Department/text()"" />
                      </e01_3042>
                    </xsl:if>  
                    <!-- BDB/SJ 200117: DeliveryLocation -If no Name, use MarkAttention else empty e01_3036-tag.  -->
                    <xsl:if test=""string-length(DeliveryLocation/Address/MarkAttention) != 0"">
                      <xsl:if test=""string-length(DeliveryLocation/Address/Name) != 0"">
                        <e02_3042>
                          <xsl:value-of select=""DeliveryLocation/Address/MarkAttention/text()"" />
                        </e02_3042>
                      </xsl:if>
                    </xsl:if>
                    <xsl:if test=""string-length(DeliveryLocation/Address/Street) != 0"">
                      <e03_3042>
                        <!-- xsl:value-of select=""DeliveryLocation/Address/Street/text(), ' ', DeliveryLocation/Address/Number/text()""  / -->
                        <xsl:value-of select=""userCSharp:unitedText((DeliveryLocation/Address/Street), (DeliveryLocation/Address/Number))"" />
                      </e03_3042>
                    </xsl:if>
                    <xsl:if test=""string-length(DeliveryLocation/Address/AdditionalStreetName) != 0"">
                      <e04_3042>
                        <xsl:value-of select=""DeliveryLocation/Address/AdditionalStreetName/text()"" />
                      </e04_3042>
                    </xsl:if>
                  </cmp04>
                  <e02_3164>
                    <xsl:value-of select=""DeliveryLocation/Address/City/text()"" />
                  </e02_3164>
                  <e04_3251>
                    <xsl:value-of select=""DeliveryLocation/Address/PostalCode/text()"" />
                  </e04_3251>
                  <e05_3207>
                    <xsl:if test=""string-length(DeliveryLocation/Address/Country) != 0"">
                      <xsl:value-of select=""DeliveryLocation/Address/Country/text()"" />
                    </xsl:if>
                    <xsl:if test=""string-length(DeliveryLocation/Address/Country) = 0"">
                      <xsl:value-of select=""'DK'"" />
                    </xsl:if>
                  </e05_3207> 
                </xsl:otherwise>
              </xsl:choose>                
            </NAD>
            <!-- g003 RFF/ never on DeliveryLocation -->
            <!-- g005/CTA/ never on DeliveryLocation -->
          </g002>
          <!-- g006 TAX -->
          <g006>
            <TAX>
              <e01_5283>
                <xsl:value-of select=""'7'"" />
              </e01_5283>
              <cmp01>
                <e01_5153>
                  <xsl:if test=""string-length(TaxTotal/TaxCategory/TaxScheme/Name) != 0"">
                    <xsl:value-of select=""TaxTotal/TaxCategory/TaxScheme/Name/text()"" />
                  </xsl:if>  
                  <xsl:if test=""string-length(TaxTotal/TaxCategory/TaxScheme/Name) = 0"">
                    <xsl:value-of select=""'VAT'"" />
                  </xsl:if>
                </e01_5153>
              </cmp01>
              <cmp03>
                <e04_5278>
                  <xsl:if test=""string-length(TaxTotal/TaxSubtotal/TaxCategory/Percent) != 0"">
                    <xsl:value-of select=""userCSharp:AddDecAmount(TaxTotal/TaxSubtotal/TaxCategory/Percent/text())"" />
                  </xsl:if>  
                  <xsl:if test=""string-length(TaxTotal/TaxSubtotal/TaxCategory/Percent) = 0"">
                   <xsl:if test=""string-length(TaxTotal/TaxSubtotal/Percent) != 0"">
                     <xsl:value-of select=""userCSharp:AddDecAmount(TaxTotal/TaxSubtotal/Percent/text())"" />
                   </xsl:if>
                   <xsl:if test=""string-length(TaxTotal/TaxSubtotal/Percent) = 0"">                            
                     <xsl:if test=""TaxTotal/TaxAmount/text() != '0.00'"">   
                       <xsl:value-of select=""'25.00'"" />
                     </xsl:if>
                     <xsl:if test=""string-length(TaxTotal/TaxAmount) = 0"">   
                       <xsl:value-of select=""'0.00'"" />
                     </xsl:if>
                   </xsl:if>
                  </xsl:if>
                </e04_5278>
              </cmp03>
              <e03_5305>
                <xsl:value-of select=""'S'"" />
              </e03_5305>
            </TAX>
          </g006>
          <!-- g007 CUX -->
          <g007>
            <CUX>
              <cmp01>
                <e01_6347>
                  <xsl:value-of select=""'2'"" />
                </e01_6347>
                <e02_6345>
                  <xsl:value-of select=""Currency/text()"" />
                </e02_6345>
                <e03_6343>
                  <xsl:value-of select=""'4'"" />
                </e03_6343>
              </cmp01>
            </CUX>
          </g007>
          <!-- g008 PAT -->
          <!-- BDB/SJ 200113: PaymentMeans/PaymentDueDate added -->
          <!-- BDB/SJ 200324: Payment in UTS added -->
          <xsl:if test=""$aUTS = 'Tele'"">
            <g008>
              <PAT>
                <e01_4279>
                  <xsl:value-of select=""'3'"" />
                </e01_4279>
              </PAT>
              <DTM>
                <cmp01>
                  <e01_2005>
                    <xsl:value-of select=""'13'"" />
                  </e01_2005>
                  <e02_2380>
                    <xsl:value-of select=""userCSharp:editDate($aIssueDate)"" />
                  </e02_2380>
                  <e03_2379>
                    <xsl:value-of select=""'102'"" />
                  </e03_2379>
                </cmp01>
              </DTM>
            </g008>
          </xsl:if>
          <xsl:if test=""string-length(PaymentMeans/PaymentDueDate) != 0"">
            <g008>
              <PAT>
                <e01_4279>
                  <xsl:value-of select=""'3'"" />
                </e01_4279>
              </PAT>
              <DTM>
                <cmp01>
                  <e01_2005>
                    <xsl:value-of select=""'13'"" />
                  </e01_2005>
                  <e02_2380>
                    <xsl:value-of select=""userCSharp:editDate(PaymentMeans/PaymentDueDate)"" /> 
                  </e02_2380>
                  <e03_2379>
                    <xsl:value-of select=""'102'"" />
                  </e03_2379>
                </cmp01>
              </DTM>
            </g008>
          </xsl:if>
          <xsl:if test=""string-length(PaymentTerms/SettlementPeriod/StartDate) != 0"">
            <g008>
              <PAT>
                <e01_4279>
                  <xsl:value-of select=""PaymentTerms/ID/text()"" />
                </e01_4279>
              </PAT>
              <DTM>
                <cmp01>
                  <e01_2005>
                    <xsl:value-of select=""PaymentTerms/ReferenceEventCode/text()"" />
                  </e01_2005>
                  <e02_2380>
                    <xsl:value-of select=""PaymentTerms/SettlementPeriod/StartDate/text()"" />
                  </e02_2380>
                  <e03_2379>
                    <xsl:value-of select=""'102'"" />
                  </e03_2379>
                </cmp01>
              </DTM>
            </g008>
          </xsl:if>   
          <!-- g009 TDT-->
          <xsl:if test=""string-length(DeliveryInfo/DeliveryTerms/SpecialTerms) != 0"">
            <g009>
              <TDT>
                <e01_8051>
                  <xsl:value-of select=""'20'"" />
                </e01_8051>
                <cmp01>
                  <e02_8066>
                    <xsl:value-of select=""DeliveryInfo/DeliveryTerms/SpecialTerms/text()"" />
                  </e02_8066>
                </cmp01>
              </TDT>
            </g009>
          </xsl:if>  
          <!-- g015 ALC - AllowanceCharge -->
          <xsl:for-each select=""AllowanceCharge"">
            <xsl:if test=""string-length(Amount) != 0""> 
              <xsl:if test=""Amount/text() != '0.00'""> 
                <g015>            
                  <ALC>
                    <e01_5463>
                      <!-- BDB/SJ 200818: Wrong use of ChargeIndicator adjusted (We use it differently in ingoing EDIFACT and XML) -->
                      <xsl:choose>
                        <xsl:when test=""ID/text() = 'Rabat'"">
                          <xsl:value-of select=""'A'"" />
                        </xsl:when>
                        <xsl:when test=""ID/text() = 'Gebyr'"">
                          <xsl:value-of select=""'C'"" />
                        </xsl:when>
                        <xsl:when test=""ID/text() = 'Fragt'"">
                          <xsl:value-of select=""'C'"" />
                        </xsl:when>
                        <xsl:when test=""ID/text() = 'Afgift'"">
                          <xsl:value-of select=""'C'"" />
                        </xsl:when>
                        <!-- ChargeIndicator true=subject to VAT, false=not subject to VAT -->
                        <xsl:when test=""ChargeIndicator/text() = 'True'"">
                          <xsl:value-of select=""'C'"" />
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select=""'A'"" />
                        </xsl:otherwise>
                      </xsl:choose>
                    </e01_5463>
                    <cmp02>
                      <e01_7161>
                        <xsl:value-of select=""AllowanceChargeReasonCode/text()"" />
                      </e01_7161>
                      <!-- BDB/SJ 191213: AllowanceChargeReason in head added -->
                      <e04_7160>
                        <xsl:choose>
                          <xsl:when test=""string-length(AllowanceChargeReason) != 0"">
                            <xsl:value-of select=""AllowanceChargeReason/text()"" />
                          </xsl:when>
                          <xsl:when test=""AllowanceChargeReasonCode/text() = 'HD'"">
                            <xsl:value-of select=""'Handling Charge'"" />
                          </xsl:when>
                          <xsl:when test=""AllowanceChargeReasonCode/text() = 'CL'"">
                            <xsl:value-of select=""'Rabat'"" />
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select=""ID/text()"" />
                          </xsl:otherwise>
                        </xsl:choose>
                      </e04_7160>
                    </cmp02>
                  </ALC>
                  <!-- ALC/PCD - Percent -->
                  <g018>
                    <PCD>
                      <cmp01>
                        <e01_5245>
                          <xsl:value-of select=""'2'"" />
                        </e01_5245>
                        <e02_5482>
                           <xsl:if test=""string-length(MultiplierFactorNumeric) != 0"">
                              <xsl:value-of select=""userCSharp:MultiplyAmount(MultiplierFactorNumeric/text())"" />    
                            </xsl:if> 
                            <xsl:if test=""string-length(MultiplierFactorNumeric) = 0"">                                             
                              <xsl:value-of select=""'0.00'"" />
                            </xsl:if> 
                        </e02_5482>
                      </cmp01>
                    </PCD>
                  </g018>        
                  <!-- ALC/MOA - Amount -->
                  <!--   8 = Allowance or charge amount (normally standard) -->
                  <!--  23 = Charge amount -->
                  <!--  25 = BaseAmount -->
                  <!-- 146 = Unit price -->
                  <!-- 204 = Allowence amount -->
                  <g019>
                    <MOA>
                      <cmp01>
                        <e01_5025>
                          <xsl:if test=""string-length(AccountingCost) != 0"">
                            <xsl:value-of select=""AccountingCost/text()"" />
                          </xsl:if>
                          <xsl:if test=""string-length(AccountingCost) = 0"">
                            <xsl:value-of select=""'8'"" />
                          </xsl:if>
                        </e01_5025>
                        <e02_5004>
                          <xsl:if test=""string-length(Amount) != 0""> 
                            <xsl:value-of select=""Amount/text()"" />
                          </xsl:if> 
                          <xsl:if test=""string-length(Amount) = 0"">
                            <xsl:value-of select=""'0.00'"" />
                          </xsl:if> 
                        </e02_5004>
                      </cmp01>
                    </MOA>
                  </g019>
                  <!-- ALC/MOA - BaseAmount -->
                  <!-- BDB/SJ 191213: go15/g019 ALC/MOA+25 removed  if = MOA+8 -->
                  <xsl:if test=""string-length(BaseAmount) != 0""> 
                    <xsl:if test=""BaseAmount/text() != Amount/text()""> 
                      <g019>
                        <MOA>
                          <cmp01>
                            <e01_5025>
                              <xsl:value-of select=""'25'"" />
                            </e01_5025>
                            <e02_5004>
                              <xsl:value-of select=""BaseAmount/text()"" />
                            </e02_5004>
                          </cmp01>
                        </MOA>
                      </g019>
                    </xsl:if>
                  </xsl:if>
                  <!-- ALC/MOA - Unitprice -->
                  <xsl:if test=""string-length(TaxCategory/PerUnitAmount) != 0""> 
                    <g019>
                      <MOA>
                        <cmp01>
                          <e01_5025>
                            <xsl:value-of select=""'146'"" />
                          </e01_5025>
                          <e02_5004>
                            <xsl:value-of select=""TaxCategory/PerUnitAmount/text()"" />
                          </e02_5004>
                        </cmp01>
                      </MOA>
                    </g019>
                  </xsl:if>
                </g015>
              </xsl:if>
            </xsl:if>    
          </xsl:for-each>
          <!-- BDB/SJ 200926: Alborg Portland (DK36428112) some ALC moved from line to Header -->
          <!-- g015 ALC - AllowanceCharge - Extra for Alborg Portland -->
          <xsl:if test=""$aAccSeller = 'DK36428112'"" >
            <xsl:for-each select=""Lines/Line/AllowanceCharge"">
              <xsl:if test=""(AllowanceChargeReason = 'Environm. Surcharge' or
                             AllowanceChargeReason = 'Raw Mat. Surcharge' or
                             AllowanceChargeReason = 'Freight revenue')"" >
                <xsl:if test=""string-length(Amount) != 0"">
                  <xsl:if test=""Amount/text() != '0.00'"">
                    <g015>
                      <ALC>
                        <e01_5463>
                          <!-- BDB/SJ 200818: Wrong use of ChargeIndicator adjusted (We use it differently in ingoing EDIFACT and XML) -->
                          <xsl:choose>
                            <xsl:when test=""ID/text() = 'Rabat'"">
                              <xsl:value-of select=""'A'"" />
                            </xsl:when>
                            <xsl:when test=""ID/text() = 'Gebyr'"">
                              <xsl:value-of select=""'C'"" />
                            </xsl:when>
                            <xsl:when test=""ID/text() = 'Fragt'"">
                              <xsl:value-of select=""'C'"" />
                            </xsl:when>
                            <xsl:when test=""ID/text() = 'Afgift'"">
                              <xsl:value-of select=""'C'"" />
                            </xsl:when>
                            <!-- ChargeIndicator true=subject to VAT, false=not subject to VAT -->
                            <xsl:when test=""ChargeIndicator/text() = 'True'"">
                              <xsl:value-of select=""'C'"" />
                            </xsl:when>
                            <xsl:otherwise>
                              <xsl:value-of select=""'A'"" />
                            </xsl:otherwise>
                          </xsl:choose>
                        </e01_5463>
                        <cmp02>
                          <e01_7161>
                            <xsl:value-of select=""AllowanceChargeReasonCode/text()"" />
                          </e01_7161>
                          <!-- BDB/SJ 191213: AllowanceChargeReason in head added -->
                          <e04_7160>
                            <xsl:choose>
                              <xsl:when test=""string-length(AllowanceChargeReason) != 0"">
                                <xsl:value-of select=""AllowanceChargeReason/text()"" />
                              </xsl:when>                             
                              <xsl:otherwise>
                                <xsl:value-of select=""ID/text()"" />
                              </xsl:otherwise>
                            </xsl:choose>
                          </e04_7160>
                        </cmp02>
                      </ALC>
                      <!-- ALC/PCD - Percent -->
                      <g018>
                        <PCD>
                          <cmp01>
                            <e01_5245>
                              <xsl:value-of select=""'2'"" />
                            </e01_5245>
                            <e02_5482>
                              <xsl:if test=""string-length(MultiplierFactorNumeric) != 0"">
                                <xsl:value-of select=""userCSharp:MultiplyAmount(MultiplierFactorNumeric/text())"" />
                              </xsl:if>
                              <xsl:if test=""string-length(MultiplierFactorNumeric) = 0"">
                                <xsl:value-of select=""'0.00'"" />
                              </xsl:if>
                            </e02_5482>
                          </cmp01>
                        </PCD>
                      </g018>
                      <!-- ALC/MOA - Amount -->
                      <g019>
                        <MOA>
                          <cmp01>
                            <e01_5025>
                              <xsl:if test=""string-length(AccountingCost) != 0"">
                                <xsl:value-of select=""AccountingCost/text()"" />
                              </xsl:if>
                              <xsl:if test=""string-length(AccountingCost) = 0"">
                                <xsl:value-of select=""'8'"" />
                              </xsl:if>
                            </e01_5025>
                            <e02_5004>
                              <xsl:if test=""string-length(Amount) != 0"">
                                <xsl:value-of select=""Amount/text()"" />
                              </xsl:if>
                              <xsl:if test=""string-length(Amount) = 0"">
                                <xsl:value-of select=""'0.00'"" />
                              </xsl:if>
                            </e02_5004>
                          </cmp01>
                        </MOA>
                      </g019>
                    </g015>
                  </xsl:if>
                </xsl:if>             
              </xsl:if>
            </xsl:for-each>
          </xsl:if>  
          <!-- Invoice Lines -->
          <!-- g025 LIN -->
          <xsl:for-each select=""Lines/Line"">
            <g025>
              <LIN>
                <e01_1082>
                  <xsl:value-of select=""LineNo/text()"" />
                </e01_1082>
                <!-- BDB/SJ 191213: If no Item no. don't make e01_7140 og e02_7143 -->
                <xsl:if test=""string-length(Item/StandardItemID) != 0"">
                  <cmp01>
                    <e01_7140>
                      <xsl:value-of select=""Item/StandardItemID/text()"" />
                    </e01_7140>
                    <e02_7143>
                      <xsl:value-of select=""'EN'"" />
                    </e02_7143>
                  </cmp01>
                </xsl:if>    
              </LIN>
              <!-- g025 PIA -->
              <xsl:if test=""string-length(Item/BuyerItemID) != 0"">
                <PIA>
                  <e01_4347>
                    <xsl:value-of select=""'1'"" />
                  </e01_4347>
                  <cmp01>
                    <e01_7140>
                      <xsl:value-of select=""Item/BuyerItemID/text()"" />
                    </e01_7140>
                    <e02_7143>
                      <xsl:value-of select=""'IN'"" />
                    </e02_7143>
                  </cmp01>
                </PIA>
              </xsl:if>
              <xsl:if test=""string-length(Item/SellerItemID) != 0"">
                <PIA>
                  <e01_4347>
                    <xsl:value-of select=""'1'"" />
                  </e01_4347>
                  <cmp01>
                    <e01_7140>
                      <xsl:value-of select=""Item/SellerItemID/text()"" />
                    </e01_7140>
                    <e02_7143>
                      <xsl:value-of select=""'SA'"" />
                    </e02_7143>
                  </cmp01>
                </PIA>
              </xsl:if>  
              <xsl:if test=""string-length(Item/AdditionalItemID) != 0""> 
                <PIA>
                  <e01_4347>
                    <xsl:value-of select=""'1'"" />
                  </e01_4347>
                  <cmp01>
                    <e01_7140>
                      <xsl:value-of select=""Item/AdditionalItemID/text()"" />
                    </e01_7140>
                    <e02_7143>
                      <xsl:value-of select=""'MP'"" />
                    </e02_7143>                 
                  </cmp01>
                </PIA>
              </xsl:if>
              <xsl:if test=""string-length(Item/CatalogueItemID) != 0""> 
                <PIA>
                  <e01_4347>
                    <xsl:value-of select=""'1'"" />
                  </e01_4347>
                  <cmp01>
                    <e01_7140>
                      <xsl:value-of select=""Item/CatalogueItemID/text()"" />
                    </e01_7140>
                    <e02_7143>
                      <xsl:value-of select=""'MF'"" />
                    </e02_7143>
                  </cmp01>
                </PIA>
              </xsl:if>
              <!-- g025 IMD -->
              <IMD>
                <e01_7077>
                  <xsl:value-of select=""'F'"" />
                </e01_7077>
                <cmp01>
                  <xsl:if test=""$aUTS != 'Tele'"">
                    <e04_7008>
                      <!-- BDB/SJ 200117: If Soroto use Item Description not Name -->
                      <xsl:if test=""$aAccSeller != '5790001970659'"">
                        <xsl:value-of select=""Item/Name/text()"" />
                      </xsl:if>
                      <xsl:if test=""$aAccSeller = '5790001970659'"">
                        <xsl:value-of select=""Item/Description/text()"" />
                      </xsl:if>
                    </e04_7008>
                    <e05_7008 />  
                  </xsl:if>
                  <xsl:if test=""$aUTS = 'Tele'"">
                    <e04_7008>
                      <xsl:value-of select=""Item/Description/text()"" />
                    </e04_7008>
                    <!-- BDB/SJ 200107: Function unitedPeriod added -->
                    <e05_7008>
                      <xsl:value-of select=""userCSharp:unitedPeriod((Period/ID),(Period/FromDate),(Period/ToDate))"" />
                    </e05_7008>
                  </xsl:if>                  
                </cmp01>
              </IMD>
              <!-- g025 QTY - Delivered Quantity -->
              <xsl:if test=""$aUTS != 'Tele'"">
                <xsl:if test=""string-length(Item/PackQuantity) != 0"">   
                  <QTY>
                    <cmp01>
                      <e01_6063>
                        <xsl:value-of select=""'46'"" />
                      </e01_6063>
                      <e02_6060>
                        <xsl:value-of select=""userCSharp:AddDecQuantity(Item/PackQuantity/text())"" />   
                      </e02_6060>
                      <e03_6411>
                        <xsl:if test=""string-length(Item/UnitCode) = 0""> 
                          <xsl:value-of select=""'PCE'"" />
                        </xsl:if>  
                        <xsl:if test=""string-length(Item/UnitCode) != 0""> 
                          <xsl:choose>
                            <xsl:when test=""Item/UnitCode = 'EA'"">
                              <xsl:value-of select=""'PCE'"" />
                            </xsl:when>
                            <xsl:when test=""Item/UnitCode = 'C62'"">
                              <xsl:value-of select=""'PCE'"" />
                            </xsl:when>
                            <xsl:otherwise>
                              <xsl:value-of select=""UnitCode/text()"" />
                            </xsl:otherwise>
                          </xsl:choose> 
                        </xsl:if>    
                      </e03_6411>
                    </cmp01>
                  </QTY>
                </xsl:if>
              </xsl:if>
              <!-- g025 QTY - Invoiced Quantity -->
              <QTY>
                <cmp01>
                  <e01_6063>
                    <xsl:value-of select=""'47'"" />
                  </e01_6063>
                  <e02_6060>
                    <xsl:value-of select=""userCSharp:AddDecQuantity(Quantity/text())"" />   
                  </e02_6060>
                  <e03_6411>
                    <xsl:if test=""string-length(UnitCode) = 0""> 
                      <xsl:value-of select=""'PCE'"" />
                    </xsl:if>  
                    <xsl:if test=""string-length(UnitCode) != 0""> 
                      <xsl:choose>
                        <xsl:when test=""UnitCode = 'EA'"">
                          <xsl:value-of select=""'PCE'"" />
                        </xsl:when>
                        <xsl:when test=""UnitCode = 'C62'"">
                          <xsl:value-of select=""'PCE'"" />
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select=""UnitCode/text()"" />
                        </xsl:otherwise>
                      </xsl:choose> 
                    </xsl:if>    
                  </e03_6411>
                </cmp01>
              </QTY>
              <!-- g025 QTY - Quantity loaded -->
              <xsl:if test=""string-length(Delivery/Quantity) != 0"">
                <QTY>
                  <cmp01>
                    <e01_6063>
                      <xsl:value-of select=""Delivery/ID/text()"" />   
                    </e01_6063>
                    <e02_6060>
                      <xsl:value-of select=""userCSharp:AddDecQuantity(Delivery/Quantity/text())"" />
                    </e02_6060>
                    <e03_6411>
                      <xsl:if test=""string-length(Delivery/UnitCode) = 0"">
                        <xsl:value-of select=""'PCE'"" />
                      </xsl:if>
                      <xsl:if test=""string-length(Delivery/UnitCode) != 0"">
                        <xsl:choose>
                          <xsl:when test=""Delivery/UnitCode = 'EA'"">
                            <xsl:value-of select=""'PCE'"" />
                          </xsl:when>
                          <xsl:when test=""Delivery/UnitCode = 'C62'"">
                            <xsl:value-of select=""'PCE'"" />
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select=""Delivery/UnitCode/text()"" />
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:if>
                    </e03_6411>
                  </cmp01>
                </QTY>
              </xsl:if>
              <!-- FTX -->
              <xsl:for-each select=""Note"">
                <xsl:if test=""string-length(.) != 0"">   
                  <FTX>
                    <e01_4451>
                      <xsl:value-of select=""'ZZZ'"" />
                    </e01_4451>
                    <cmp02>
                      <e01_4440>
                        <xsl:value-of select=""userCSharp:splitNote1(.)"" />
                      </e01_4440>
                      <e02_4440>
                        <xsl:value-of select=""userCSharp:splitNote2(.)"" />               
                      </e02_4440>
                      <e03_4440>
                        <xsl:value-of select=""userCSharp:splitNote3(.)"" />
                      </e03_4440>
                      <e04_4440>
                        <xsl:value-of select=""userCSharp:splitNote4(.)"" />
                      </e04_4440>
                      <e05_4440>
                        <xsl:value-of select=""userCSharp:splitNote5(.)"" />
                      </e05_4440>
                    </cmp02>
                  </FTX>
                </xsl:if>  
              </xsl:for-each> 
              <!-- g026 MOA -->
              <g026>
                <MOA>
                  <cmp01>
                    <e01_5025>
                      <xsl:value-of select=""'203'"" />
                    </e01_5025>
                    <e02_5004>
                      <xsl:value-of select=""LineAmountTotal/text()"" />
                    </e02_5004>
                  </cmp01>
                </MOA>
              </g026>
              <!-- g028 PRI -->
              <!-- BDB/SJ 200122: Test if any PRI+AAB added -->
              <!-- BDB/SJ 200128: Spelling mistake ajusted (PRI+AAB) -->
              <xsl:if test=""string-length(PriceGross/Price) != 0"">
                <g028>
                  <PRI>
                    <cmp01>
                      <e01_5125>
                        <xsl:if test=""string-length(PriceGross/PristypeCode) != 0"">
                          <xsl:value-of select=""PriceGross/PristypeCode/text()"" />
                        </xsl:if>
                        <xsl:if test=""string-length(PriceGross/PristypeCode) = 0"">
                          <xsl:value-of select=""'AAB'"" />
                        </xsl:if>
                      </e01_5125>
                      <!-- BDB/SJ 200323: RemovesignAmount added -->
                      <e02_5118>
                        <xsl:value-of select=""userCSharp:RemovesignAmount(PriceGross/Price/text())"" />
                      </e02_5118>
                      <e05_5284>
                        <xsl:if test=""string-length(PriceGross/Quantity) != 0"">
                          <xsl:value-of select=""userCSharp:AddDecQuantity(PriceGross/Quantity/text())"" />
                        </xsl:if>
                        <xsl:if test=""string-length(PriceGross/Quantity) = 0"">
                          <xsl:value-of select=""'1.000'"" />
                        </xsl:if>
                      </e05_5284>
                      <e06_6411>
                        <xsl:if test=""string-length(PriceGross/UnitCode) = 0""> 
                          <xsl:value-of select=""'PCE'"" />
                        </xsl:if>  
                        <xsl:if test=""string-length(PriceGross/UnitCode) != 0""> 
                          <xsl:choose>
                            <xsl:when test=""PriceGross/UnitCode = 'EA'"">
                              <xsl:value-of select=""'PCE'"" />
                            </xsl:when>
                            <xsl:when test=""PriceGross/UnitCode = 'C62'"">
                              <xsl:value-of select=""'PCE'"" />
                            </xsl:when>
                            <xsl:otherwise>
                              <xsl:value-of select=""PriceGross/UnitCode/text()"" />
                            </xsl:otherwise>
                          </xsl:choose> 
                        </xsl:if>    
                     </e06_6411>     
                    </cmp01>
                  </PRI>
                </g028>
              </xsl:if> 
              <g028>
                <PRI>
                  <cmp01>
                    <e01_5125>
                      <xsl:if test=""string-length(PriceNet/PristypeCode) != 0"">
                        <xsl:value-of select=""PriceNet/PristypeCode/text()"" />
                      </xsl:if>
                      <xsl:if test=""string-length(PriceNet/PristypeCode) = 0"">
                        <xsl:value-of select=""'AAA'"" />
                      </xsl:if>
                    </e01_5125>
                    <!-- BDB/SJ 200323: RemovesignAmount added -->
                    <e02_5118>
                      <xsl:value-of select=""userCSharp:RemovesignAmount(PriceNet/Price/text())"" />
                    </e02_5118>
                    <e05_5284>
                      <xsl:if test=""string-length(PriceNet/Quantity) != 0"">
                        <xsl:value-of select=""userCSharp:AddDecQuantity(PriceNet/Quantity/text())"" />
                      </xsl:if>
                      <xsl:if test=""string-length(PriceNet/Quantity) = 0"">
                        <xsl:value-of select=""'1.000'"" />
                      </xsl:if>
                    </e05_5284>
                    <e06_6411>
                      <xsl:if test=""string-length(PriceNet/UnitCode) = 0""> 
                        <xsl:value-of select=""'PCE'"" />
                      </xsl:if>  
                      <xsl:if test=""string-length(PriceNet/UnitCode) != 0""> 
                        <xsl:choose>
                          <xsl:when test=""PriceNet/UnitCode = 'EA'"">
                            <xsl:value-of select=""'PCE'"" />
                          </xsl:when>
                          <xsl:when test=""PriceNet/UnitCode = 'C62'"">
                            <xsl:value-of select=""'PCE'"" />
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select=""PriceNet/UnitCode/text()"" />
                          </xsl:otherwise>
                        </xsl:choose> 
                      </xsl:if>    
                   </e06_6411>     
                  </cmp01>
                </PRI>
              </g028>
              <!-- g029 RFF -->
              <xsl:if test=""string-length(OrderLineReference/LineID) != 0"">
                <g029>
                  <RFF>
                    <cmp01>
                      <e01_1153>
                        <xsl:if test=""$aUTS != 'Tele'"">
                          <xsl:value-of select=""'LI'"" />
                        </xsl:if>
                        <xsl:if test=""$aUTS = 'Tele'"">
                          <xsl:value-of select=""'CR'"" />
                        </xsl:if>
                      </e01_1153>
                      <e02_1154>
                        <xsl:value-of select=""OrderLineReference/LineID/text()"" />                
                      </e02_1154>
                    </cmp01>
                  </RFF>
                </g029>
              </xsl:if> 
              <!-- g038 ALC - AllowanceCharge -->
              <!-- BDB/SJ 200113: If TDC - g038 removed -->
              <xsl:if test=""$aAccSeller != 'DK40075291'"">
                <xsl:for-each select=""AllowanceCharge"">
                  <xsl:if test=""string-length(Amount) != 0"">
                    <!-- BDB/SJ 200926: Alborg Portland (DK36428112) some ALC moved from line to Header -->
                    <xsl:if test=""($aAccSeller != 'DK36428112') or
                                  ($aAccSeller = 'DK36428112' and
                                  AllowanceChargeReason != 'Environm. Surcharge' and
                                  AllowanceChargeReason != 'Raw Mat. Surcharge' and
                                  AllowanceChargeReason != 'Freight revenue')"" >
                      <!-- TAX+EXC is placed in Allowance to give place to TAX+VAT in TaxTotal. -->
                      <g038>
                        <ALC>
                          <e01_5463>
                          <!-- BDB/SJ 200818: Wrong use of ChargeIndicator adjusted (We use it differently in ingoing EDIFACT and XML) -->
                            <xsl:choose>
                              <xsl:when test=""ID/text() = 'Rabat'"">
                                <xsl:value-of select=""'A'"" />
                              </xsl:when>
                              <xsl:when test=""ID/text() = 'Gebyr'"">
                                <xsl:value-of select=""'C'"" />
                              </xsl:when>
                              <xsl:when test=""ID/text() = 'Fragt'"">
                                <xsl:value-of select=""'C'"" />
                              </xsl:when>
                              <xsl:when test=""ID/text() = 'Afgift'"">
                                <xsl:value-of select=""'C'"" />
                              </xsl:when>
                              <!-- ChargeIndicator true=subject to VAT, false=not subject to VAT -->
                              <xsl:when test=""ChargeIndicator/text() = 'True'"">
                                <xsl:value-of select=""'C'"" />
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:value-of select=""'A'"" />
                              </xsl:otherwise>
                            </xsl:choose>
                          </e01_5463>
                          <cmp02>
                            <!-- BDB/SJ 200818: AllowanceChargeReason if no AllowanceChargeReasonCode on lines -->
                            <e01_7161>
                              <xsl:value-of select=""AllowanceChargeReasonCode/text()"" />
                            </e01_7161>
                            <e04_7160>
                              <xsl:choose>
                                <xsl:when test=""string-length(AllowanceChargeReason) != 0"">
                                  <xsl:value-of select=""AllowanceChargeReason/text()"" />
                                </xsl:when>
                                <xsl:when test=""AllowanceChargeReasonCode/text() = 'HD'"">
                                  <xsl:value-of select=""'Handling Charge'"" />
                                </xsl:when>
                                <xsl:when test=""AllowanceChargeReasonCode/text() = 'CL'"">
                                  <xsl:value-of select=""'Rabat'"" />
                                </xsl:when>
                                <xsl:otherwise>
                                  <xsl:value-of select=""ID/text()"" />
                                </xsl:otherwise>
                              </xsl:choose> 
                            </e04_7160>
                          </cmp02>
                        </ALC>
                        <!-- LIN/ALC/PCD - Percent -->
                        <g040>
                          <PCD>
                            <cmp01>
                              <e01_5245>
                                <xsl:value-of select=""'1'"" />
                              </e01_5245>
                              <e02_5482>
                                <xsl:if test=""string-length(MultiplierFactorNumeric) != 0"">
                                  <xsl:value-of select=""userCSharp:MultiplyAmount(MultiplierFactorNumeric/text())"" />    
                                </xsl:if> 
                                <xsl:if test=""string-length(MultiplierFactorNumeric) = 0"">
                                  <xsl:value-of select=""'0.00'"" /> 
                                </xsl:if> 
                              </e02_5482>
                            </cmp01>
                          </PCD>
                        </g040>
                        <g041>
                          <!-- LIN/ALC/MOA - Amounts -->
                          <!--   8 = Allowance or charge amount (normally standard) -->
                          <!--  23 = Charge amount -->
                          <!--  25 = BaseAmount -->
                          <!-- 146 = Unit price -->
                          <!-- 204 = Allowence amount -->
                          <MOA>
                            <cmp01>
                              <e01_5025>
                                <xsl:choose>
                                  <!-- ChargeIndicator true=subject to VAT, false=not subject to VAT -->
                                  <xsl:when test=""ChargeIndicator = 'True'"">
                                    <xsl:value-of select=""'23'"" />
                                  </xsl:when>
                                  <xsl:otherwise>
                                    <xsl:value-of select=""'204'"" />
                                  </xsl:otherwise>
                                </xsl:choose>  
                              </e01_5025>
                              <e02_5004>
                                <xsl:value-of select=""Amount/text()"" />
                              </e02_5004>
                            </cmp01>
                          </MOA>
                          <xsl:if test=""string-length(BaseAmount) != 0"">
                            <MOA>
                              <cmp01>
                                <e01_5025>
                                  <xsl:value-of select=""'25'"" />
                                </e01_5025>
                                <e02_5004>
                                  <xsl:value-of select=""BaseAmount/text()"" />
                                </e02_5004>
                              </cmp01>
                            </MOA>
                          </xsl:if>  
                          <xsl:if test=""string-length(TaxCategory/PerUnitAmount) != 0"">
                            <MOA>
                              <cmp01>
                                <e01_5025>
                                  <xsl:value-of select=""'146'"" />
                                </e01_5025>
                                <e02_5004>
                                  <xsl:value-of select=""TaxCategory/PerUnitAmount/text()"" />
                                </e02_5004>
                              </cmp01>
                            </MOA>
                          </xsl:if>  
                        </g041>
                      </g038>
                    </xsl:if>  
                  </xsl:if>
                </xsl:for-each>
                <!-- g038 ALC - Total -->
                <xsl:if test=""string-length(TaxTotal/TaxAmount) != 0"">
                  <xsl:if test=""TaxTotal/TaxAmount/text() != '0.00'"">
                    <g038>
                      <ALC>
                        <e01_5463>
                          <xsl:value-of select=""'C'"" />
                        </e01_5463>
                        <cmp02>
                          <!-- BDB/MMA 200702: Using TaxTotal/TaxSubtotal/TaxCategory/TaxScheme when there is content -->
                          <!-- BDB/SJ 200803: TaxTotal/TaxSubtotal/TaxCategory/TaxScheme - 7161 is always VAT and 5482 is always 25.00 -->
                          <e01_7161>
                            <!--  xsl:value-of select=""TaxTotal/TaxSubtotal/TaxCategory/TaxScheme/ID/text()"" /   -->
                            <xsl:value-of select=""'VAT'"" />
                          </e01_7161>
                          <e04_7160>
                            <xsl:value-of select=""TaxTotal/TaxSubtotal/TaxCategory/TaxScheme/Navn/text()"" />                            
                            <!--  xsl:value-of select=""'Afgift'"" /    -->
                          </e04_7160>
                        </cmp02>
                      </ALC>
                      <g040>
                        <PCD>
                          <cmp01>
                            <e01_5245>
                              <xsl:value-of select=""'2'"" />
                            </e01_5245>
                            <e02_5482>
                              <xsl:if test=""string-length(MultiplierFactorNumeric) != 0"">
                                <xsl:value-of select=""userCSharp:MultiplyAmount(MultiplierFactorNumeric/text())"" />    
                              </xsl:if> 
                              <!-- BDB/SJ 200803: TaxTotal/TaxSubtotal/TaxCategory/TaxScheme - 7161 is always VAT and 5482 is always 25.00 -->
                              <xsl:if test=""string-length(MultiplierFactorNumeric) = 0"">
                                <xsl:value-of select=""'25.00'"" />
                              </xsl:if> 
                            </e02_5482>
                          </cmp01>
                        </PCD>
                      </g040>
                      <g041>
                        <MOA>
                          <cmp01>
                            <e01_5025>
                              <xsl:value-of select=""'8'"" />
                            </e01_5025>
                            <e02_5004>
                              <xsl:value-of select=""TaxTotal/TaxAmount/text()"" />
                            </e02_5004>
                          </cmp01>
                        </MOA>
                      </g041>
                    </g038>
                  </xsl:if>
                </xsl:if>
              </xsl:if>
            </g025>
          </xsl:for-each>
          <!-- Invoice Total -->
          <UNS>
            <e01_0081>
              <xsl:value-of select=""'S'"" />
            </e01_0081>
          </UNS>
          <CNT>
            <cmp01>
              <e01_6069>
                <xsl:value-of select=""'2'"" />
              </e01_6069>
              <e02_6066>
                <xsl:value-of select=""count(Lines/Line)"" />
              </e02_6066>
            </cmp01>
          </CNT>
          <!-- g048 MOA+79 - Total line items amount -->
          <!-- BDB/SJ 200320: AddDecAmount added -->
          <g048>
            <MOA>
              <cmp01>
                <e01_5025>
                  <xsl:value-of select=""'79'"" />
                </e01_5025>
                <e02_5004>
                  <xsl:value-of select=""userCSharp:AddDecAmount(Total/LineTotalAmount/text())"" />
                </e02_5004>
              </cmp01>
            </MOA>
          </g048>
          <!-- g048 MOA+86 -	Message total monetary amount -->
          <!-- BDB/SJ 200320: AddDecAmount added -->
          <g048>
            <MOA>
              <cmp01>
                <e01_5025>
                  <xsl:value-of select=""'86'"" />
                </e01_5025>
                <e02_5004>
                  <xsl:value-of select=""userCSharp:AddDecAmount(Total/PayableAmount/text())"" />
                </e02_5004>
              </cmp01>
            </MOA>
          </g048>
          <!-- g048 MOA+176 -	Total of all duty/tax/fee amounts -->
          <!-- BDB/SJ 191227: MOA+176 adjusted -->
          <!-- BDB/SJ 200320: AddDecAmount added -->
          <g048>
            <MOA>
              <cmp01>
                <e01_5025>
                  <xsl:value-of select=""'176'"" />
                </e01_5025>
                <e02_5004>
                  <xsl:if test=""string-length(TaxTotal/TaxAmount) != 0"">
                    <xsl:if test=""TaxTotal/TaxAmount/text() != '0.00'"">   
                      <xsl:value-of select=""userCSharp:AddDecAmount(TaxTotal/TaxAmount/text())"" />     
                    </xsl:if>                
                    <xsl:if test=""TaxTotal/TaxAmount/text() = '0.00'"">   
                      <xsl:value-of select=""userCSharp:AddDecAmount(Total/TaxExclAmount/text())"" />   
                    </xsl:if> 
                  </xsl:if>
                  <xsl:if test=""string-length(TaxTotal/TaxAmount) = 0"">
                    <xsl:value-of select=""userCSharp:AddDecAmount(Total/TaxExclAmount/text())"" />   
                  </xsl:if>  
                </e02_5004>
              </cmp01>
            </MOA>
          </g048>
          <!-- g048 MOA+125 -	Taxable amount -->
          <!-- BDB/SJ 200113: MOA+125 adjusted -->
          <g048>
            <MOA>
              <cmp01>
                <e01_5025>
                  <xsl:value-of select=""'125'"" />
                </e01_5025>
                <e02_5004>
                  <xsl:if test=""string-length(TaxTotal/TaxAmount) != 0"">
                    <xsl:if test=""TaxTotal/TaxAmount/text() != '0.00'"">
                      <xsl:value-of select=""userCSharp:AddDecAmount(Total/PayableAmount/text() - TaxTotal/TaxAmount/text())"" />
                    </xsl:if>
                    <xsl:if test=""TaxTotal/TaxAmount/text() = '0.00'"">
                      <xsl:value-of select=""userCSharp:AddDecAmount(Total/PayableAmount/text() - Total/TaxExclAmount/text())"" />
                    </xsl:if>
                  </xsl:if>
                  <xsl:if test=""string-length(TaxTotal/TaxAmount) = 0"">
                    <xsl:value-of select=""userCSharp:AddDecAmount(Total/PayableAmount/text() - Total/TaxExclAmount/text())"" />
                  </xsl:if>
                </e02_5004>
              </cmp01>
            </MOA>
          </g048>
          <!-- g050 TAX -->
          <g050>
            <TAX>
              <e01_5283>
                <xsl:value-of select=""'7'"" />
              </e01_5283>
              <cmp01>
                <e01_5153>
                  <xsl:if test=""string-length(Total/TaxSubtotal/TaxCategory/TaxScheme/Navn) != 0"">
                    <xsl:value-of select=""Total/TaxSubtotal/TaxCategory/TaxScheme/Navn/text()"" />
                  </xsl:if>  
                  <xsl:if test=""string-length(Total/TaxSubtotal/TaxCategory/TaxScheme/Navn) = 0"">
                    <xsl:value-of select=""'VAT'"" />
                  </xsl:if>
                </e01_5153>
              </cmp01>
              <cmp03>
                <e04_5278>
                  <xsl:if test=""string-length(TaxTotal/TaxSubtotal/TaxCategory/Percent) != 0"">
                    <xsl:value-of select=""userCSharp:AddDecAmount(TaxTotal/TaxSubtotal/TaxCategory/Percent/text())"" />
                  </xsl:if>
                  <xsl:if test=""string-length(TaxTotal/TaxSubtotal/TaxCategory/Percent) = 0"">
                    <xsl:if test=""string-length(TaxTotal/TaxSubtotal/Percent) != 0"">
                      <xsl:value-of select=""userCSharp:AddDecAmount(TaxTotal/TaxSubtotal/Percent/text())"" />
                    </xsl:if>
                    <xsl:if test=""string-length(TaxTotal/TaxSubtotal/Percent) = 0"">                            
                      <xsl:if test=""TaxTotal/TaxAmount/text() != '0.00'"">   
                        <xsl:value-of select=""'25.00'"" />
                      </xsl:if>
                      <xsl:if test=""string-length(TaxTotal/TaxAmount) = 0"">   
                        <xsl:value-of select=""'0.00'"" />
                      </xsl:if>
                    </xsl:if>
                  </xsl:if>
                </e04_5278>
              </cmp03>
              <e03_5305>
                <xsl:value-of select=""'S'"" />
              </e03_5305>
            </TAX>
            <!-- g048 MOA+79 - Total line items amount -->
            <!-- BDB/SJ 200320: AddDecAmount added -->
            <MOA>
              <cmp01>
                <e01_5025>
                  <xsl:value-of select=""'79'"" />
                </e01_5025>
                <e02_5004>
                  <xsl:value-of select=""userCSharp:AddDecAmount(Total/LineTotalAmount/text())"" />
                </e02_5004>
              </cmp01>
            </MOA>
            <!--  g048 MOA+176 - Total of all duty/tax/fee amounts -->
            <!-- BDB/SJ 191227: MOA+176 adjusted -->
            <!-- BDB/SJ 200320: AddDecAmount added -->
            <MOA>
              <cmp01>
                <e01_5025>
                  <xsl:value-of select=""'176'"" />
                </e01_5025>
                <e02_5004>
                  <xsl:if test=""string-length(TaxTotal/TaxAmount) != 0"">
                    <xsl:if test=""TaxTotal/TaxAmount/text() != '0.00'"">   
                      <xsl:value-of select=""userCSharp:AddDecAmount(TaxTotal/TaxAmount/text())"" />
                    </xsl:if>                
                    <xsl:if test=""TaxTotal/TaxAmount/text() = '0.00'"">   
                      <xsl:value-of select=""userCSharp:AddDecAmount(Total/TaxExclAmount/text())"" />
                    </xsl:if> 
                  </xsl:if>
                  <xsl:if test=""string-length(TaxTotal/TaxAmount) = 0"">
                    <xsl:value-of select=""userCSharp:AddDecAmount(Total/TaxExclAmount/text())"" /> 
                  </xsl:if>
                </e02_5004>
              </cmp01>
            </MOA>
          </g050>
          <!-- UNT -->
          <UNT>
            <e01_0074>
              <xsl:value-of select=""'0'"" />
            </e01_0074>
            <e02_0062>
              <xsl:if test=""string-length(UUID) != 0"">
                <xsl:value-of select=""UUID/text()"" />
              </xsl:if>
              <xsl:if test=""string-length(UUID) = 0"">
                <xsl:value-of select=""$aUUID"" />
              </xsl:if>
            </e02_0062>
          </UNT>
          <!-- UNZ -->
          <UNZ>
            <e01_0036>
              <xsl:value-of select=""'1'"" />
            </e01_0036>
            <e02_0020>
              <xsl:if test=""string-length(UUID) != 0"">
                <xsl:value-of select=""UUID/text()"" />
              </xsl:if>
              <xsl:if test=""string-length(UUID) = 0"">
                <xsl:value-of select=""$aUUID"" />
              </xsl:if>
            </e02_0020>
          </UNZ>
        </INVOIC>
      </Body>
    </Envelope>
  </xsl:template>
  
  <msxsl:script language=""C#"" implements-prefix=""userCSharp"">
    <![CDATA[
   
  public string splitNote1(string aNote)
  {
      string NewNote = """";
      int aNoteLen = aNote.Length;
      if (aNoteLen == 0)
        return NewNote;
      if (aNoteLen < 60)
        NewNote = aNote.Substring(0,aNoteLen);
      else  
        NewNote = aNote.Substring(0,60);
      return NewNote;
  }    
  public string splitNote2(string aNote)
  {
      string NewNote = """";
      int aNoteLen = aNote.Length;
      if (aNoteLen < 61)
        return NewNote;
      aNoteLen = aNoteLen - 60;  
      if (aNoteLen < 60)
        NewNote = aNote.Substring(60,aNoteLen);
      else  
        NewNote = aNote.Substring(60,60);
      return NewNote;
  }
  public string splitNote3(string aNote)
  {
      string NewNote = """";
      int aNoteLen = aNote.Length;
      if (aNoteLen < 121)
        return NewNote;
      aNoteLen = aNoteLen - 120;  
      if (aNoteLen < 60)
        NewNote = aNote.Substring(120,aNoteLen);
      else  
        NewNote = aNote.Substring(120,60);
      return NewNote;
  }
  public string splitNote4(string aNote)
  {
      string NewNote = """";
      int aNoteLen = aNote.Length;
      if (aNoteLen < 181)
        return NewNote;
      aNoteLen = aNoteLen - 180;  
      if (aNoteLen < 60)
        NewNote = aNote.Substring(180,aNoteLen);
      else  
        NewNote = aNote.Substring(180,60);
      return NewNote;
  }
  public string splitNote5(string aNote)
  {
      string NewNote = """";
      int aNoteLen = aNote.Length;
      if (aNoteLen < 241)
        return NewNote;
      aNoteLen = aNoteLen - 240;  
      if (aNoteLen < 60)
        NewNote = aNote.Substring(240,aNoteLen);
      else  
        NewNote = aNote.Substring(240,60);
      return NewNote;
  }
  
  public string editDate(string aDate)
  {
      string NewDate;
      NewDate = string.Concat(aDate.Substring(0,4), aDate.Substring(5,2), aDate.Substring(8,2));
      return NewDate;
  }
  
  public static string CurrentDateTime()
  {
      return DateTime.Now.ToString(""s"");
  }
  
  public string editTime(string aTime)
  {
      string NewTime;
      NewTime = string.Concat(aTime.Substring(11,2), aTime.Substring(14,2));
      return NewTime;
  }

  public string FIKline(string aText1, string aText2, string aText3)
  {
      string NewText;
      NewText = string.Concat('+',aText1,'*',aText2,'+',aText3,'*');
      return NewText;
  }

  public string unitedText(string aText1, string aText2)
  {
      string NewText;
      NewText = string.Concat(aText1,' ',aText2);
      return NewText;
  }
  
  public string unitedPeriod(string aText1, string aText2, string aText3)
  {
      string NewText;
      NewText = string.Concat(aText1,' ',aText2,' ','-',' ',aText3);
      return NewText;
  }
  
  public string editCompanyID(string aCompanyID)
  {
      string NewCompanyID;
      if (aCompanyID.Substring(0,2) == ""DK"")
      {
         NewCompanyID = aCompanyID.Substring(2,8);   
      }
      else
      {
         NewCompanyID = aCompanyID.Substring(0,8);   
      }
      return NewCompanyID;
  }
    
  public string AddDecAmount(Double aAmount)
  {
      string NewAmount;
      NewAmount = string.Format(""{0:0.00}"", aAmount);
      return NewAmount.Replace("","",""."");
  }
  
  public string AddDecQuantity(Double aQuantity)
  {
      string NewQuantity;
      NewQuantity = string.Format(""{0:0.000}"", aQuantity);
      return NewQuantity.Replace("","",""."");
  }

  public string MultiplyAmount(Double aAmount)
  {
      string NewAmount;
      NewAmount = string.Format(""{0:0.00}"", (aAmount * 100));
      return NewAmount.Replace("","",""."");
  } 
  
    public string RemovesignAmount(Double aAmount)
  {
     string NewAmount;
      if (aAmount < 0)
        {
           aAmount = aAmount * -1;   
        }
      NewAmount = string.Format(""{0:0.00}"", aAmount);  
      return NewAmount.Replace("","",""."");
  } 
  
  public string NewGuidString()
  {
      return Guid.NewGuid().ToString();
	}
       
]]>
  </msxsl:script>
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"BYGE.Integration.Core.Invoice.Invoice";
        
        private const global::BYGE.Integration.Core.Invoice.Invoice _srcSchemaTypeReference0 = null;
        
        private const string _strTrgSchemasList0 = @"BYGE.Integration.M3Invoice.M3Invoice";
        
        private const global::BYGE.Integration.M3Invoice.M3Invoice _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [1];
                _SrcSchemas[0] = @"BYGE.Integration.Core.Invoice.Invoice";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"BYGE.Integration.M3Invoice.M3Invoice";
                return _TrgSchemas;
            }
        }
    }
}
