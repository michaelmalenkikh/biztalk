<?xml version="1.0" encoding="UTF-16"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:var="http://schemas.microsoft.com/BizTalk/2003/var" xmlns:userCSharp="http://schemas.microsoft.com/BizTalk/2003/userCSharp" exclude-result-prefixes="msxsl var s0" version="1.0" xmlns:s0="http://byg-e.dk/schemas/v10" xmlns:ns0="http://schemas.microsoft.com/BizTalk/EDI/EDIFACT/2006">
  <xsl:output omit-xml-declaration="yes" method="xml" version="1.0" />
  <xsl:template match="/">
    <xsl:apply-templates select="/s0:OrdrSP" />
  </xsl:template>
  <xsl:template match="/s0:OrdrSP">
    <ns0:EFACT_D96A_ORDRSP>       
      
     <!-- UNH SEGMENT--> 
        
      <UNH>
        <UNH1>
          <xsl:text>1</xsl:text>
        </UNH1>
        <UNH2>
          <UNH2.1>
            <xsl:text>ORDRSP</xsl:text>
          </UNH2.1>
          <UNH2.2>
            <xsl:text>D</xsl:text>
          </UNH2.2>
          <UNH2.3>
            <xsl:text>96A</xsl:text>
          </UNH2.3>
          <UNH2.4>
            <xsl:text>UN</xsl:text>
          </UNH2.4>
          <UNH2.5>
            <xsl:text>EAN008</xsl:text>
          </UNH2.5>
        </UNH2>
      </UNH>
    
      <!-- END  OF  UNH --> 
      
      <!-- BGM  SEGMENT --> 
        
      <ns0:BGM>
        <ns0:C002>
          <C00201>
           <xsl:if test="string-length(OrdrSPType) != 0">
            <xsl:value-of select="OrdrSPType/text()" />
            </xsl:if> 
          <xsl:if test="string-length(OrdrSPType) = 0">
            <xsl:text>231</xsl:text>
            </xsl:if> 
          </C00201>
        </ns0:C002>        
        <BGM02>
          <xsl:value-of select="OrdrSPID/text()" />
        </BGM02>
        <BGM03>
          <xsl:if test="string-length(OrdrSPResult) != 0">
           <xsl:value-of select="OrdrSPResult/text()" />
          </xsl:if>
          <xsl:if test="string-length(OrdrSPResult) = 0">
           <xsl:text>29</xsl:text>
          </xsl:if>
        </BGM03>          
      </ns0:BGM>
    
  <!-- END  OF  BGM --> 
      
  
  <!-- DTM  SEGMENT -->
      
      <ns0:DTM>        
        <ns0:C507>
          <C50701>
            <xsl:text>137</xsl:text>
          </C50701>
          <C50702>
          <xsl:value-of select="concat(substring(IssueDate/text(), 1, 4), substring(IssueDate/text(), 6, 2), substring(IssueDate/text(), 9, 2))" /> 
          </C50702>
          <C50703>
            <xsl:text>102</xsl:text>
          </C50703>
        </ns0:C507>            
       </ns0:DTM> 
      
    <!--!  
        <xsl:if test="string-length(OrderDate) != 0">
         <ns0:DTM>        
          <ns0:C507>
           <C50701>
            <xsl:text>171</xsl:text>
           </C50701>
           <C50702>
            <xsl:value-of select="concat(substring(OrderDate/text(), 1, 4), substring(OrderDate/text(), 6, 2), substring(OrderDate/text(), 9, 2))" /> 
           </C50702>
           <C50703>
            <xsl:text>102</xsl:text>
           </C50703>
        </ns0:C507>            
       </ns0:DTM>        
      </xsl:if>
     --> 
      <xsl:for-each select="DTMReference/Node">
       <ns0:DTM> 
        <ns0:C507>         
         <C50701>
            <xsl:value-of select="Code/text()" /> 
          </C50701>
         <C50702> 
          <xsl:value-of select="concat(substring(TimeStamp/text(), 1, 4), substring(TimeStamp/text(), 6, 2), substring(TimeStamp/text(), 9, 2))" /> 
          </C50702>
          <C50703>
            <xsl:text>102</xsl:text>
          </C50703>
        </ns0:C507>      
       </ns0:DTM> 
      </xsl:for-each>                  
      
  <!-- END  OF  DTM -->  
      
  <!--  FTX SEGMENT -->
      
      <xsl:for-each select="NoteLoop/Node">
        <FTX>
         <xsl:if test="string-length(NoteQualifier) != 0">
           <FTX01>
             <xsl:value-of select="NoteQualifier/text()" />           
           </FTX01>         
         </xsl:if>          
         <xsl:if test="string-length(NoteFunction) != 0">
           <FTX02>
             <xsl:value-of select="NoteFunction/text()" />           
           </FTX02>         
         </xsl:if> 
         <xsl:if test="string-length(NoteLanguage) != 0">
           <FTX05>
             <xsl:value-of select="NoteLanguage/text()" />           
           </FTX05>         
         </xsl:if>
          <ns0:C108>
            <C10801>
              <xsl:value-of select="Note/text()" />                       
            </C10801>          
          </ns0:C108>       
        </FTX>      
      </xsl:for-each> 
      
  <!-- END OF  FTX -->    
      
  <!-- RFF  SEGMENT -->            
      
      <xsl:for-each select="OrderReference/Node">
       <ns0:RFFLoop1>
         <ns0:RFF>        
         <ns0:C506>         
         <C50601>
            <xsl:value-of select="Code/text()" /> 
          </C50601>
         <C50602> 
            <xsl:value-of select="Reference/text()" />         
          </C50602>
        </ns0:C506>
        </ns0:RFF> 
         <xsl:if test="string-length(DTM/TimeStamp) != 0">
           <ns0:DTM_2>
             <C507_2>
               <C50701>
                 <xsl:value-of select="DTM/Code/text()" />
               </C50701>
               <C50702>
                 <xsl:value-of select="concat(substring(DTM/TimeStamp/text(), 1, 4), substring(DTM/TimeStamp/text(), 6, 2), substring(DTM/TimeStamp/text(), 9, 2))" />
               </C50702>
               <C50703>
                 <xsl:value-of select="DTM/Format/text()" />
               </C50703>
             </C507_2>  
           </ns0:DTM_2>  
         </xsl:if>  
       </ns0:RFFLoop1> 
      </xsl:for-each>                  
      
  <!-- END  OF  RFF -->    
       
  <!--  NAD SEGMEMT-->
      
  <!--  NAD+SU  SEGMENT -->     
      
      <ns0:NADLoop1>
        <ns0:NAD>          
          <NAD01>
            <xsl:text>SU</xsl:text>
          </NAD01>
          <ns0:C082>
            <C08201>
              <xsl:if test="string-length(SupplierCustomerParty/PartyID) != 0">
               <xsl:value-of select="SupplierCustomerParty/PartyID/text()" />
              </xsl:if> 
              <xsl:if test="string-length(SupplierCustomerParty/PartyID) = 0">
               <xsl:value-of select="SupplierCustomerParty/AccSellerID/text()" />
              </xsl:if>
              <!--              <xsl:value-of select="SupplierCustomerParty/SellerID/text()" /> -->
            </C08201>
            <C08203>
              <xsl:text>9</xsl:text>
            </C08203>
          </ns0:C082>
          <xsl:if test="string-length(SupplierCustomerParty/Address/Name) != 0">
          <ns0:C080>
            
              <C08001>
                <xsl:value-of select="SupplierCustomerParty/Address/Name/text()" />
              </C08001>
            
          </ns0:C080>
          </xsl:if>
          <xsl:if test="string-length(SupplierCustomerParty/Address/Street) != 0">
          <ns0:C059>
            
              <C05901>
                <xsl:value-of select="SupplierCustomerParty/Address/Street/text()" />
              </C05901>
            
          </ns0:C059>
        </xsl:if>                
          <xsl:if test="string-length(SupplierCustomerParty/Address/City) != 0">
            <NAD06>
              <xsl:value-of select="SupplierCustomerParty/Address/City/text()" />
            </NAD06>
          </xsl:if>
          <xsl:if test="string-length(SupplierCustomerParty/Address/PostalCode) != 0">
            <NAD08>
              <xsl:value-of select="SupplierCustomerParty/Address/PostalCode/text()" />
            </NAD08>
          </xsl:if>
          <xsl:if test="string-length(SupplierCustomerParty/Address/Country) != 0">
            <NAD09>
              <xsl:value-of select="SupplierCustomerParty/Address/Country/text()" />
            </NAD09>
          </xsl:if>
        </ns0:NAD>

        <xsl:for-each select="SupplierCustomerParty/RFFNADLoop/Node">
          <ns0:RFFLoop2>
            <ns0:RFF_2>
              <ns0:C506_2>
                <C50601>
                  <xsl:value-of select="Code/text()" />
                </C50601>
                <C50602>
                  <xsl:value-of select="Reference/text()" />
                </C50602>
              </ns0:C506_2>
            </ns0:RFF_2>
          </ns0:RFFLoop2>
        </xsl:for-each>

        <xsl:if test="string-length(SupplierCustomerParty/Concact/ID) != 0">
        <ns0:CTALoop1>
          <ns0:CTA>
            <CTA01>
              <xsl:if test="(string-length(SupplierCustomerParty/Concact/ID) &lt; 3 or SupplierCustomerParty/Concact/ID/text() = 'ZZZ') and string(number(SupplierCustomerParty/Concact/ID)) = 'NaN'">
                <xsl:value-of select="SupplierCustomerParty/Concact/ID/text()" />
              </xsl:if>
              <xsl:if test="(string-length(SupplierCustomerParty/Concact/ID) &gt;= 3 and SupplierCustomerParty/Concact/ID/text() != 'ZZZ') or string(number(SupplierCustomerParty/Concact/ID)) != 'NaN'">
                <xsl:text>OC</xsl:text>
              </xsl:if>
            </CTA01>
            <ns0:C056>
              <C05602>
                <xsl:value-of select="SupplierCustomerParty/Concact/Name/text()" />              
              </C05602>            
           </ns0:C056>
          </ns0:CTA>  

            <xsl:if test="string-length(SupplierCustomerParty/Concact/E-mail) != 0">
              <ns0:COM>
                <ns0:C076>
                  <C07601>
                    <xsl:value-of select="SupplierCustomerParty/Concact/E-mail/text()" />
                  </C07601>
                  <C07602>
                    <xsl:text>EM</xsl:text>
                  </C07602>
                </ns0:C076>
              </ns0:COM>
            </xsl:if>

            <xsl:if test="string-length(SupplierCustomerParty/Concact/Telephone) != 0">
              <ns0:COM>
                <ns0:C076>
                  <C07601>
                    <xsl:value-of select="SupplierCustomerParty/Concact/Telephone/text()" />
                  </C07601>
                  <C07602>
                    <xsl:text>TE</xsl:text>
                  </C07602>
                </ns0:C076>
              </ns0:COM>
            </xsl:if>

            <xsl:if test="string-length(SupplierCustomerParty/Concact/Telefax) != 0">
              <ns0:COM>
                <ns0:C076>
                  <C07601>
                    <xsl:value-of select="SupplierCustomerParty/Concact/Telefax/text()" />
                  </C07601>
                  <C07602>
                    <xsl:text>FX</xsl:text>
                  </C07602>
                </ns0:C076>
              </ns0:COM>
            </xsl:if>
        
        </ns0:CTALoop1>
        
      </xsl:if>                
        
      </ns0:NADLoop1>    
      
      
  <!--  END OF  NAD+SU  -->    
      
  <!--  NAD+BY  SEGMENT -->   
      
<ns0:NADLoop1>
        <ns0:NAD>         
          <NAD01>
            <xsl:text>BY</xsl:text>
          </NAD01>
          <ns0:C082>
            <!--<xsl:if test="BuyerCustomerParty/PartyID">-->
              <C08201>
                <xsl:if test="string-length(BuyerCustomerParty/PartyID) != 0">
                  <xsl:value-of select="BuyerCustomerParty/PartyID/text()" />
                </xsl:if>
                <xsl:if test="string-length(BuyerCustomerParty/PartyID) = 0">
                  <xsl:value-of select="BuyerCustomerParty/BuyerID/text()" />
                </xsl:if>
<!--                <xsl:value-of select="BuyerCustomerParty/PartyID/text()" />  -->
              </C08201>
            <!--</xsl:if>-->
            <C08203>
              <xsl:text>9</xsl:text>
            </C08203>
          </ns0:C082>
          <xsl:if test="string-length(BuyerCustomerParty/Address/Name) != 0">
          <ns0:C080>
            
            <C08001>
              <xsl:value-of select="BuyerCustomerParty/Address/Name/text()" />
            </C08001>
          
          </ns0:C080>
          </xsl:if>
          <xsl:if test="string-length(BuyerCustomerParty/Address/Street) != 0">
          <ns0:C059>
            
              <C05901>
                <xsl:value-of select="BuyerCustomerParty/Address/Street/text()" />
              </C05901>
            
          </ns0:C059>
          </xsl:if>
          <xsl:if test="string-length(BuyerCustomerParty/Address/City) != 0">
            <NAD06>
              <xsl:value-of select="BuyerCustomerParty/Address/City/text()" />
            </NAD06>
          </xsl:if>
          <xsl:if test="string-length(BuyerCustomerParty/Address/PostalCode) != 0">
            <NAD08>
              <xsl:value-of select="BuyerCustomerParty/Address/PostalCode/text()" />
            </NAD08>
          </xsl:if>
          <xsl:if test="string-length(BuyerCustomerParty/Address/Country) != 0">
            <NAD09>
              <xsl:value-of select="BuyerCustomerParty/Address/Country/text()" />
            </NAD09>
          </xsl:if>
        </ns0:NAD>

        <xsl:for-each select="BuyerCustomerParty/RFFNADLoop/Node">
          <ns0:RFFLoop2>
            <ns0:RFF_2>
              <ns0:C506_2>
                <C50601>
                  <xsl:value-of select="Code/text()" />
                </C50601>
                <C50602>
                  <xsl:value-of select="Reference/text()" />
                </C50602>
              </ns0:C506_2>
            </ns0:RFF_2>
          </ns0:RFFLoop2>
        </xsl:for-each>
  
      <xsl:if test="string-length(BuyerCustomerParty/Concact/ID) != 0">
        <ns0:CTALoop1>
          <ns0:CTA>
            <CTA01>
              <xsl:if test="(string-length(BuyerCustomerParty/Concact/ID) &lt; 3 or BuyerCustomerParty/Concact/ID/text() = 'ZZZ') and string(number(BuyerCustomerParty/Concact/ID)) = 'NaN'">
                <xsl:value-of select="BuyerCustomerParty/Concact/ID/text()" />
              </xsl:if>
              <xsl:if test="(string-length(BuyerCustomerParty/Concact/ID) &gt;= 3 and BuyerCustomerParty/Concact/ID/text() != 'ZZZ') or string(number(BuyerCustomerParty/Concact/ID)) != 'NaN'">
                <xsl:text>OC</xsl:text>
              </xsl:if>
            </CTA01>
            <ns0:C056>
              <C05602>
                <xsl:value-of select="BuyerCustomerParty/Concact/Name/text()" />              
              </C05602>            
           </ns0:C056>
          </ns0:CTA>            

            <xsl:if test="string-length(BuyerCustomerParty/Concact/E-mail) != 0">
              <ns0:COM>
                <ns0:C076>
                  <C07601>
                    <xsl:value-of select="BuyerCustomerParty/Concact/E-mail/text()" />
                  </C07601>
                  <C07602>
                    <xsl:text>EM</xsl:text>
                  </C07602>
                </ns0:C076>
              </ns0:COM>
            </xsl:if>

            <xsl:if test="string-length(BuyerCustomerParty/Concact/Telephone) != 0">
              <ns0:COM>

                <ns0:C076>
                  <C07601>
                    <xsl:value-of select="BuyerCustomerParty/Concact/Telephone/text()" />
                  </C07601>
                  <C07602>
                    <xsl:text>TE</xsl:text>
                  </C07602>
                </ns0:C076>
              </ns0:COM>
            </xsl:if>

            <xsl:if test="string-length(BuyerCustomerParty/Concact/Telefax) != 0">
              <ns0:COM>
                <ns0:C076>
                  <C07601>
                    <xsl:value-of select="BuyerCustomerParty/Concact/Telefax/text()" />
                  </C07601>
                  <C07602>
                    <xsl:text>FX</xsl:text>
                  </C07602>
                </ns0:C076>
              </ns0:COM>
            </xsl:if>       
        </ns0:CTALoop1>        
      </xsl:if>            
     </ns0:NADLoop1>      
            
  <!--  END OF  NAD+BY  -->
      
  <!--  NAD+IV  SEGMENT -->    
     
     <xsl:if test="string-length(AccountingCustomer/AccBuyerID) != 0">   
      <ns0:NADLoop1>        
        <ns0:NAD>
          
          <NAD01>
            <xsl:text>IV</xsl:text>
          </NAD01>
          <ns0:C082>
            <C08201>
              <xsl:if test="string-length(AccountingCustomer/PartyID) != 0">
                <xsl:value-of select="AccountingCustomer/PartyID/text()" />
              </xsl:if>
              <xsl:if test="string-length(AccountingCustomer/PartyID) = 0">
                <xsl:value-of select="AccountingCustomer/AccBuyerID/text()" />
              </xsl:if>
              <!--              <xsl:value-of select="AccountingCustomer/AccBuyerID/text()" />  -->
            </C08201>
            <C08203>
              <xsl:text>9</xsl:text>
            </C08203>
          </ns0:C082>
          <xsl:if test="string-length(AccountingCustomer/Address/Name) != 0">
          <ns0:C080>
            
              <C08001>
                <xsl:value-of select="AccountingCustomer/Address/Name/text()" />
              </C08001>
            
          </ns0:C080>
            </xsl:if>
          <xsl:if test="string-length(AccountingCustomer/Address/Street) != 0">
          <ns0:C059>
            
              <C05901>
                <xsl:value-of select="AccountingCustomer/Address/Street/text()" />
              </C05901>
            
          </ns0:C059>
          </xsl:if>
          <xsl:if test="string-length(AccountingCustomer/Address/City) != 0">
            <NAD06>
              <xsl:value-of select="AccountingCustomer/Address/City/text()" />
            </NAD06>
          </xsl:if>
          <xsl:if test="string-length(AccountingCustomer/Address/PostalCode) != 0">
            <NAD08>
              <xsl:value-of select="AccountingCustomer/Address/PostalCode/text()" />
            </NAD08>
          </xsl:if>
          <xsl:if test="string-length(AccountingCustomer/Address/Country) != 0">
            <NAD09>
              <xsl:value-of select="AccountingCustomer/Address/Country/text()" />
            </NAD09>
          </xsl:if>
        </ns0:NAD>

        <xsl:for-each select="AccountingCustomer/RFFNADLoop/Node">
          <ns0:RFFLoop2>
            <ns0:RFF_2>
              <ns0:C506_2>
                <C50601>
                  <xsl:value-of select="Code/text()" />
                </C50601>
                <C50602>
                  <xsl:value-of select="Reference/text()" />
                </C50602>
              </ns0:C506_2>
            </ns0:RFF_2>
          </ns0:RFFLoop2>
        </xsl:for-each>
        
      <xsl:if test="string-length(AccountingCustomer/Concact/ID) != 0">
        <ns0:CTALoop1>
          <ns0:CTA>
            <CTA01>              
              <xsl:if test="(string-length(AccountingCustomer/Concact/ID) &lt; 3 or AccountingCustomer/Concact/ID/text() = 'ZZZ') and string(number(AccountingCustomer/Concact/ID)) = 'NaN'">
                <xsl:value-of select="AccountingCustomer/Concact/ID/text()" />
              </xsl:if>
              <xsl:if test="(string-length(AccountingCustomer/Concact/ID) &gt;= 3 and AccountingCustomer/Concact/ID/text() != 'ZZZ') or string(number(AccountingCustomer/Concact/ID)) != 'NaN'">
                <xsl:text>OC</xsl:text>
              </xsl:if>
            </CTA01>
            <ns0:C056>
              <C05602>
                <xsl:value-of select="AccountingCustomer/Concact/Name/text()" />              
              </C05602>            
           </ns0:C056>
          </ns0:CTA>

          <xsl:if test="string-length(AccountingCustomer/Concact/E-mail) != 0">
              <ns0:COM>
                <ns0:C076>
                  <C07601>
                    <xsl:value-of select="AccountingCustomer/Concact/E-mail/text()" />
                  </C07601>
                  <C07602>
                    <xsl:text>EM</xsl:text>
                  </C07602>
                </ns0:C076>
              </ns0:COM>
            </xsl:if>

            <xsl:if test="string-length(AccountingCustomer/Concact/Telephone) != 0">
              <ns0:COM>
                <ns0:C076>
                  <C07601>
                    <xsl:value-of select="AccountingCustomer/Concact/Telephone/text()" />
                  </C07601>
                  <C07602>
                    <xsl:text>TE</xsl:text>
                  </C07602>
                </ns0:C076>
              </ns0:COM>
            </xsl:if>

            <xsl:if test=" string-length(AccountingCustomer/Concact/Telefax) != 0">
              <ns0:COM>
                <ns0:C076>
                  <C07601>
                    <xsl:value-of select="AccountingCustomer/Concact/Telefax/text()" />
                  </C07601>
                  <C07602>
                    <xsl:text>FX</xsl:text>
                  </C07602>
                </ns0:C076>
              </ns0:COM>
            </xsl:if>
      
        </ns0:CTALoop1>
        
      </xsl:if>                    
      </ns0:NADLoop1>  
     </xsl:if>  
     
  <!--  END OF  NAD+IV  -->          
      
  <!--  NAD+DP  SEGMENT -->  
      
<xsl:for-each select="Delivery/DeliveryLocation">      
<ns0:NADLoop1>
          <ns0:NAD>
            
            <NAD01>
              <xsl:text>DP</xsl:text>
            </NAD01>
            <xsl:if test="string-length(PartyID) != 0">
            <ns0:C082>
              
                <C08201>
                  <xsl:value-of select="PartyID/text()" />
                </C08201>
              
              <C08203>
                <xsl:text>9</xsl:text>
              </C08203>
            </ns0:C082>
            </xsl:if>
            <xsl:if test="string-length(Address/Name) != 0"> 
            <ns0:C080>
             
              <C08001>
                <xsl:value-of select="Address/Name/text()" />
              </C08001>
               
            </ns0:C080>
              </xsl:if>
            <xsl:if test="string-length(Address/Street) != 0">
            <ns0:C059>
              
                <C05901>
                  <xsl:value-of select="Address/Street/text()" />
                </C05901>
              
            </ns0:C059>
            </xsl:if>
            <xsl:if test="string-length(Address/City) != 0">
              <NAD06>
                <xsl:value-of select="Address/City/text()" />
              </NAD06>
            </xsl:if>
            <xsl:if test="string-length(Address/PostalCode) != 0">
              <NAD08>
                <xsl:value-of select="Address/PostalCode/text()" />
              </NAD08>
            </xsl:if>
            <xsl:if test="string-length(Address/Country) != 0">
              <NAD09>
                <xsl:value-of select="Address/Country/text()" />
              </NAD09>
            </xsl:if>
          </ns0:NAD>

        <xsl:if test="string-length(Concact/ID) != 0">
         <ns0:CTALoop1>
          <ns0:CTA>
            <CTA01>              
              <xsl:if test="(string-length(Concact/ID) &lt; 3 or Concact/ID/text() = 'ZZZ') and string(number(Concact/ID)) = 'NaN'">
                <xsl:value-of select="Concact/ID/text()" />
              </xsl:if>
              <xsl:if test="(string-length(Concact/ID) &gt;= 3 and Concact/ID/text() != 'ZZZ') or string(number(Concact/ID)) != 'NaN'">
                <xsl:text>OC</xsl:text>
              </xsl:if>
            </CTA01>
            <ns0:C056>
              <C05602>
                <xsl:value-of select="Concact/Name/text()" />              
              </C05602>            
           </ns0:C056>
          </ns0:CTA>            

            <xsl:if test="string-length(Concact/E-mail) != 0">
              <ns0:COM>
                <ns0:C076>
                  <C07601>
                    <xsl:value-of select="Concact/E-mail/text()" />
                  </C07601>
                  <C07602>
                    <xsl:text>EM</xsl:text>
                  </C07602>
                </ns0:C076>
              </ns0:COM>
            </xsl:if>

            <xsl:if test="string-length(Concact/Telephone) != 0">
              <ns0:COM>
                <ns0:C076>
                  <C07601>
                    <xsl:value-of select="Concact/Telephone/text()" />
                  </C07601>
                  <C07602>
                    <xsl:text>TE</xsl:text>
                  </C07602>
                </ns0:C076>
              </ns0:COM>
            </xsl:if>

            <xsl:if test="string-length(Concact/Telefax) != 0">
              <ns0:COM>
                <ns0:C076>
                  <C07601>
                    <xsl:value-of select="Concact/Telefax/text()" />
                  </C07601>
                  <C07602>
                    <xsl:text>FX</xsl:text>
                  </C07602>
                </ns0:C076>
              </ns0:COM>
            </xsl:if>
       
        </ns0:CTALoop1>
        
      </xsl:if>                

      
        </ns0:NADLoop1>      
      </xsl:for-each>            
           
  <!--  END OF  NAD+DP  -->          
  
  <!--  END  OF  NAD -->

  <!--  TAX SEGMENT -->
      
<xsl:for-each select="TaxTotal">
  <xsl:for-each select="TaxSubtotal">
<!--    <xsl:if test="string-length(TaxCategory/ID) != 0"> -->
      <ns0:TAXLoop1>
        <ns0:TAX>
                  <xsl:if test="string-length(TaxCode) != 0">
                    <TAX01> 
                      <xsl:value-of select="TaxCode/text()" />
                     </TAX01>
                  </xsl:if>
          <xsl:if test="string-length(TaxCode) = 0">
            <TAX01>
              <xsl-text>7</xsl-text>
            </TAX01>
          </xsl:if>

         <xsl:if test="string-length(TaxCategory/TaxScheme/Navn) != 0">
          <ns0:C241>
           <C24101>
            <xsl:value-of select="TaxCategory/TaxScheme/Navn/text()" />
           </C24101>
          </ns0:C241>
         </xsl:if>

         <xsl:if test="string-length(TaxCategory/Percent) != 0">  
          <ns0:C243>
           <C24304>
            <xsl:value-of select="TaxCategory/Percent/text()" />
           </C24304>
          </ns0:C243>
         </xsl:if>

          <!--   <xsl:if test="string-length(TaxCategory/TaxScheme/TaxTypeCode) != 0">
           <TAX06>
            <xsl:value-of select="TaxCategory/TaxScheme/TaxTypeCode/text()" /> -->
          <xsl:if test="string-length(TaxCategory/ID) != 0">
           <TAX06>
            <xsl:choose>                    
             <xsl:when test="TaxCategory/ID = 'StandardRated'">
              <xsl:text>S</xsl:text>                   
             </xsl:when>                             
             <xsl:when test="TaxCategory/ID = 'ZeroRated'">
              <xsl:text>E</xsl:text>                   
             </xsl:when>                                         
             <xsl:when test="TaxCategory/ID = 'ReverseCharge'">
              <xsl:text>AE</xsl:text>                   
             </xsl:when>                              
             <xsl:otherwise>
              <xsl:text>XZ</xsl:text>                    
             </xsl:otherwise>                                 
            </xsl:choose>        
           </TAX06>
          </xsl:if>       
         </ns0:TAX>

    <!--    <xsl:if test="string-length(TaxableAmount) != 0">
    <ns0:MOA><ns0:C516>
      <C51601>
        <xsl:text>125</xsl:text>        
      </C51601>
      <C51602>
      <xsl:choose>
       <xsl:when test="TaxableAmount/text() = '0.00'">
        <xsl:text>0</xsl:text>                  
       </xsl:when>  
       <xsl:otherwise>        
        <xsl:value-of select="TaxableAmount/text()" />  
       </xsl:otherwise>                                         
       </xsl:choose>     
      </C51602>
     </ns0:C516>
    </ns0:MOA>     
   </xsl:if>

   <xsl:if test="string-length(TaxAmount) != 0">
    <ns0:MOA>
     <ns0:C516>
      <C51601>
       <xsl:text>124</xsl:text>
      </C51601>
      <C51602>
      <xsl:choose>
       <xsl:when test="TaxableAmount/text() = '0.00'">
        <xsl:text>0</xsl:text>
       </xsl:when>
       <xsl:otherwise>
        <xsl:value-of select="TaxableAmount/text()" />
       </xsl:otherwise>
      </xsl:choose>
                
     </C51602>
    </ns0:C516>
   </ns0:MOA>
  </xsl:if>  -->
 </ns0:TAXLoop1>

   </xsl:for-each>  
  </xsl:for-each>        
            
  <!--  END OF  TAX -->    

<!--  CUX SEGMENT -->      
      
      <xsl:if test="string-length(Currency) != 0">
        <ns0:CUXLoop1>
          <ns0:CUX>
            <ns0:C504>

              <C50401>
                <xsl:text>2</xsl:text>
              </C50401>
              <C50402>
                <xsl:value-of select="Currency/text()" />
              </C50402>
              <C50403>
                <xsl:text>4</xsl:text>
              </C50403>
            </ns0:C504>
          </ns0:CUX>
        </ns0:CUXLoop1>
      </xsl:if>

      <!--  END OF CUX  -->
      
  <!--  TOD SEGMENT -->    
      
    <xsl:for-each select="Delivery/DeliveryInfo/DeliveryTerms"> 
      <ns0:TODLoop1>
        <ns0:TOD>       
         <xsl:if test="string-length(SpecialTermsCode) != 0"> 
           <TOD01>
             <xsl:value-of select="SpecialTermsCode/text()" />                                  
           </TOD01>        
         </xsl:if>  
          <xsl:if test="string-length(SpecialTermsCode2) != 0"> 
           <TOD02>
             <xsl:value-of select="SpecialTermsCode2/text()" />                                  
           </TOD02>        
         </xsl:if>   
         <ns0:C100>
          <xsl:if test="string-length(SpecialTerms) != 0">   
           <C10001>
            <xsl:value-of select="SpecialTerms/text()" />         
           </C10001>
         </xsl:if>
         <xsl:if test="string-length(SpecialTerms2) != 0">   
           <C10002>
            <xsl:value-of select="SpecialTerms2/text()" />         
           </C10002>
         </xsl:if>
         <xsl:if test="string-length(SpecialTerms3) != 0">   
           <C10003>
            <xsl:value-of select="SpecialTerms3/text()" />         
           </C10003>
         </xsl:if>
         <xsl:if test="string-length(SpecialTerms4) != 0">   
           <C10004>
            <xsl:value-of select="SpecialTerms4/text()" />         
           </C10004>
         </xsl:if>
         <xsl:if test="string-length(SpecialTerms5) != 0">   
           <C10005>
            <xsl:value-of select="SpecialTerms5/text()" />         
           </C10005>
         </xsl:if>
        </ns0:C100>        
        </ns0:TOD>  
      </ns0:TODLoop1>
     </xsl:for-each>
      
  <!-- END OF  TOD --> 
      
  <!--  ALC SEGMENT -->

      <xsl:for-each select="AllowanceCharge">
        <xsl:if test="string-length(ChargeIndicator) != 0">
          <ns0:ALCLoop1>
            <ns0:ALC>
              <ALC01>
                <xsl:choose>
                  <xsl:when test="translate(ChargeIndicator, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')='FALSE'">
                    <xsl:text>A</xsl:text>
                  </xsl:when>
                  <xsl:when test="translate(ChargeIndicator, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')='TRUE'">
                    <xsl:text>C</xsl:text>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:text>XZ</xsl:text>
                  </xsl:otherwise>
                </xsl:choose>
              </ALC01>

              <xsl:if test="string-length(ID) != 0">
                <ns0:C552>
                  <C55201>
                    <xsl:value-of select="ID/text()" />
                  </C55201>
                </ns0:C552>
              </xsl:if>

              <xsl:if test="string-length(SequenceNumeric) != 0">
                <ALC04>
                  <xsl:value-of select="SequenceNumeric/text()" />
                </ALC04>
              </xsl:if>

              <xsl:if test="string-length(AllowanceChargeReasonCode)!=0 or string-length(AllowanceChargeReason)!=0">
                <ns0:C214>
                  <xsl:if test="string-length(AllowanceChargeReasonCode)!=0">
                    <C21401>
                      <xsl:value-of select="AllowanceChargeReasonCode/text()" />
                    </C21401>
                  </xsl:if>

                  <xsl:if test="string-length(AllowanceChargeReason)!=0">
                    <C21404>
                      <xsl:value-of select="normalize-space(AllowanceChargeReason/text())" />
                    </C21404>
                  </xsl:if>

                </ns0:C214>
              </xsl:if>
            </ns0:ALC>

            <xsl:if test="string-length(MultiplierFactorNumeric) != 0">
              <ns0:PCDLoop1>
                <ns0:PCD_3>
                  <ns0:C501_3>
                    <C50101>

                      <xsl:choose>
                        <xsl:when test="translate(ChargeIndicator, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')='FALSE'">
                          <xsl:text>1</xsl:text>
                        </xsl:when>
                        <xsl:when test="translate(ChargeIndicator, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')='TRUE'">
                          <xsl:text>2</xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:text>XZ</xsl:text>
                        </xsl:otherwise>
                      </xsl:choose>

                    </C50101>
                    <C50102>
                      <xsl:value-of select="format-number(MultiplierFactorNumeric*100, '0.##')" />
                    </C50102>

                  </ns0:C501_3>
                </ns0:PCD_3>
              </ns0:PCDLoop1>
            </xsl:if>

            <xsl:if test="string-length(Amount) != 0">
              <ns0:MOALoop1>
                <ns0:MOA_3>
                  <ns0:C516_3>

                    <xsl:if test="string-length(AmountCode) != 0">
                      <C51601>
                        <xsl:value-of select="AmountCode/text()" />
                      </C51601>
                    </xsl:if>

                    <xsl:if test="string-length(AmountCode) = 0">
                      <C51601>
                        <xsl:text>8</xsl:text>
                      </C51601>
                    </xsl:if>

                    <C51602>
                      <xsl:choose>
                        <xsl:when test="Amount/text() = '0.00'">
                          <xsl:text>0</xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="Amount/text()" />
                        </xsl:otherwise>
                      </xsl:choose>
                    </C51602>

                  </ns0:C516_3>
                </ns0:MOA_3>
              </ns0:MOALoop1>

            </xsl:if>

          </ns0:ALCLoop1>
        </xsl:if>
      </xsl:for-each>

      <!--  END OF  ALC -->      

      <!--  LIN -->
      
       <xsl:for-each select="Lines/Line">
        <ns0:LINLoop1>
          
          <!--  LIN Number and  ID  -->
          
          <ns0:LIN>
            <LIN01>
              <xsl:value-of select="LineNo/text()" />
            </LIN01>
            <ns0:C212>
              <xsl:if test="string-length(Item/StandardItemID) != 0">
                <C21201>
                  <xsl:value-of select="Item/StandardItemID/text()" />
                </C21201>
              </xsl:if>
             <xsl:if test="string-length(Item/SchemeID/StdItemID) != 0"> 
              <C21202>
                <xsl:value-of select="Item/SchemeID/StdItemID/text()" />
              </C21202>
             </xsl:if>
              <xsl:if test="string-length(Item/SchemeID/StdItemID) = 0">
                <C21202>
                  <xsl:text>EN</xsl:text>
                </C21202>
              </xsl:if>
            </ns0:C212>
          </ns0:LIN>
          
          <!--  END OF  LIN Number and  ID  -->
          
          <!--  PIA -->          
            
            <xsl:if test="string-length(Item/SellerItemID) != 0">
            <ns0:PIA>  
             <ns0:PIA01>
              <xsl:text>1</xsl:text>
             </ns0:PIA01>
              <ns0:C212_2>
                <C21201>
                  <xsl:value-of select="Item/SellerItemID/text()" />
                </C21201>
                <C21202>
                  <xsl:text>SA</xsl:text>
                </C21202>
              </ns0:C212_2>
             </ns0:PIA>   
            </xsl:if>

           <xsl:if test="string-length(Item/BuyerItemID) != 0">
            <ns0:PIA>  
             <ns0:PIA01>
              <xsl:text>1</xsl:text>
             </ns0:PIA01>              
              <ns0:C212_2>
                <C21201>
                  <xsl:value-of select="Item/BuyerItemID/text()" />
                </C21201>
                <C21202>
                  <xsl:text>BP</xsl:text>
                </C21202>
              </ns0:C212_2>
             </ns0:PIA>  
            </xsl:if>

           <xsl:if test="string-length(Item/AdditionalItemID) != 0">
            <ns0:PIA>  
             <ns0:PIA01>
              <xsl:text>1</xsl:text>
             </ns0:PIA01>              
              <ns0:C212_2>
                <C21201>
                  <xsl:value-of select="Item/AdditionalItemID/text()" />
                </C21201>
                <C21202>
                  <xsl:text>MP</xsl:text>
                </C21202>
              </ns0:C212_2>
             </ns0:PIA>  
            </xsl:if>

           <xsl:if test="string-length(Item/ManufacturerItemID) != 0">
            <ns0:PIA>   
             <ns0:PIA01>
              <xsl:text>1</xsl:text>
             </ns0:PIA01>
              <ns0:C212_2>
                <C21201>
                  <xsl:value-of select="Item/ManufacturerItemID/text()" />
                </C21201>
                <C21202>
                  <xsl:text>MF</xsl:text>
                </C21202>
              </ns0:C212_2>
             </ns0:PIA>    
            </xsl:if>
            
           <xsl:for-each select="Item/ExtraPIALoop/Node"> 
             <ns0:PIA>
               <ns0:PIA01>
                 <xsl:value-of select="DigitalCode/text()" />               
               </ns0:PIA01>
               <ns0:C212_2>
                <C21201>
                  <xsl:value-of select="PIA/text()" />
                </C21201>
                <C21202>
                  <xsl:value-of select="Code/text()" />
                </C21202>
              </ns0:C212_2>   
             </ns0:PIA>   
           </xsl:for-each>            
                    
          <!--  END OF  PIA-->
          
          <!--  NAME  -->

          <xsl:if test="string-length(Item/Name) != 0">
            <ns0:IMD_2>
              <IMD01>
                <xsl:text>F</xsl:text>
              </IMD01>              
                <xsl:if test="Item/Name">
                 <ns0:C273_2> 
                  <C27304>
                    <xsl:value-of select="substring(normalize-space(Item/Name/text()), 1, 35)" />
                  </C27304>
                </ns0:C273_2>
              </xsl:if>
            </ns0:IMD_2>
          </xsl:if>

          <!--  END OF NAME -->
          
          <!--  QTY -->

          <xsl:for-each select="Quantity/Node">
            
           <ns0:QTY_3>
            <ns0:C186_3>
              <C18601>
                <xsl:value-of select="Code/text()" />             
              </C18601>
              <C18602>
                <xsl:value-of select="Quantity/text()" />             
              </C18602>              
               <xsl:if test="string-length(UnitCode) != 0">
                <C18603>
                  <xsl:value-of select="userCSharp:ConvertUnitCode('UBLtoTUN', UnitCode/text())" />
                  <!--  <xsl:value-of select="UnitCode/text()" />  -->
                 </C18603> 
               </xsl:if>                               
            </ns0:C186_3>
           </ns0:QTY_3>  
          
          </xsl:for-each>  
          
          <!--  END OF  QTY -->
          
          <!--  DTM -->
          
          <xsl:for-each select="DTMReference/Node">
            
           <ns0:DTM_13>
             <ns0:C507_13>
               <C50701>
                 <xsl:value-of select="Code/text()" />                                           
               </C50701>
               <C50702>
                 <xsl:value-of select="concat(substring(TimeStamp/text(), 1, 4), substring(TimeStamp/text(), 6, 2), substring(TimeStamp/text(), 9, 2))" />                
               </C50702>
               <C50703>
                 <xsl:text>102</xsl:text>               
               </C50703>             
             </ns0:C507_13>           
           </ns0:DTM_13>
            
          </xsl:for-each>  
          
          <!--  END OF  DTM -->
          
          <!--  MOA SECTION -->

          <xsl:if test="string-length(LineAmountTotal) != 0">
              <ns0:MOA_5>
                <ns0:C516_5>
                  <C51601>
                    <xsl:text>203</xsl:text>
                  </C51601>
                  <C51602>
                    <xsl:choose>
                      <xsl:when test="LineAmountTotal/text() = '0.00'">
                        <xsl:text>0</xsl:text>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="LineAmountTotal/text()" />
                      </xsl:otherwise>
                    </xsl:choose>
                  </C51602>
                <xsl:if test="string-length(Currency) != 0">  
                  <C51603>
                    <xsl:value-of select="Currency/text()" />
                  </C51603>                
                </xsl:if>                      
                </ns0:C516_5>
              </ns0:MOA_5>
          </xsl:if> 
          
      <!--  END OF  MOA SECTION -->
          
      <!--  FTX SEGMENT -->
      
      <xsl:for-each select="NoteLoop/Node">
        <FTX_6>
         <xsl:if test="string-length(NoteQualifier) != 0">
           <FTX01>
             <xsl:value-of select="NoteQualifier/text()" />           
           </FTX01>         
         </xsl:if>          
         <xsl:if test="string-length(NoteFunction) != 0">
           <FTX02>
             <xsl:value-of select="NoteFunction/text()" />           
           </FTX02>         
         </xsl:if> 
         <xsl:if test="string-length(NoteLanguage) != 0">
           <FTX05>
             <xsl:value-of select="NoteLanguage/text()" />           
           </FTX05>         
         </xsl:if>
          <ns0:C108_6>
            <C10801>
              <xsl:value-of select="Note/text()" />                       
            </C10801>          
          </ns0:C108_6>       
        </FTX_6>      
      </xsl:for-each> 
      
  <!-- END OF  FTX -->  
          
          <!--  PRI SECTION -->
          
          <xsl:if test="string-length(PriceNet/Price) != 0 or string-length(PriceGross/Price) != 0">
            
          <ns0:PRILoop1>
           <xsl:if test="string-length(PriceNet/Price) != 0">  
            <ns0:PRI>                            
              <ns0:C509>                  
                <xsl:if test="string-length(PriceNet/PristypeCode) != 0">                    
                  <C50901>
                  <xsl:value-of select="PriceNet/PristypeCode/text()" />
                  </C50901>
                </xsl:if>                
                <xsl:if test="string-length(PriceNet/PristypeCode) = 0">
                 <C50901>
                  <xsl:text>AAA</xsl:text>
                 </C50901>
                </xsl:if>                
                <xsl:if test="string-length(PriceNet/Price) != 0">
                  <C50902>                                     
                   <xsl:choose>
                    <xsl:when test="PriceNet/Price/text() = '0.00'">
                     <xsl:text>0</xsl:text>                  
                    </xsl:when>  
                    <xsl:otherwise>        
                     <xsl:value-of select="PriceNet/Price/text()" />  
                    </xsl:otherwise>                                         
                   </xsl:choose>                    
                  </C50902>                  
                </xsl:if> 
              <xsl:if test="string-length(PriceNet/Quantity) != 0">
                <C50905>
                  <xsl:value-of select="PriceNet/Quantity/text()" />                  
                </C50905> 
              </xsl:if>
              <xsl:if test="string-length(PriceNet/UnitCode) != 0">
                <C50906>
                 <xsl:value-of select="userCSharp:ConvertUnitCode('UBLtoTUN', PriceNet/UnitCode/text())" /> 
                 <!--  <xsl:value-of select="PriceNet/UnitCode/text()" />  -->                  
                </C50906>                              
              </xsl:if>              
              </ns0:C509>                
             </ns0:PRI>            
            </xsl:if> 
          
          <xsl:if test="string-length(PriceGross/Price) != 0">   
           <ns0:PRI>
            <ns0:C509>                                  
             <xsl:if test="string-length(PriceGross/PristypeCode) != 0">
              <C50901>
               <xsl:value-of select="PriceGross/PristypeCode/text()" />
              </C50901>
             </xsl:if>               
             <xsl:if test="string-length(PriceGross/PristypeCode) = 0">
              <C50901>
               <xsl:text>AAB</xsl:text>
              </C50901>
             </xsl:if>
             <xsl:if test="string-length(PriceGross/Price) != 0">
              <C50902>               
               <xsl:choose>
                <xsl:when test="PriceGross/Price/text() = '0.00'">
                 <xsl:text>0</xsl:text>                  
                </xsl:when>  
                <xsl:otherwise>        
                 <xsl:value-of select="PriceGross/Price/text()" />  
                </xsl:otherwise>                                         
               </xsl:choose>                    
              </C50902>
             </xsl:if>
             <xsl:if test="string-length(PriceGross/Quantity) != 0">
                <C50905>
                  <xsl:value-of select="PriceGross/Quantity/text()" />                  
                </C50905> 
              </xsl:if>
              <xsl:if test="string-length(PriceGross/UnitCode) != 0">
                <C50906>
                 <xsl:value-of select="userCSharp:ConvertUnitCode('UBLtoTUN', PriceGross/UnitCode/text())" />  
                 <!--  <xsl:value-of select="PriceGross/UnitCode/text()" />  -->                  
                </C50906>                              
              </xsl:if>
            </ns0:C509>                                          
           </ns0:PRI>
          </xsl:if> 
         </ns0:PRILoop1>   
            
        </xsl:if>    
          
          <!--  END OF  PRI -->              
          
          <!--  RFF -->
          
          <xsl:for-each select="OrderReference/Node">
            
           <ns0:RFFLoop3>
             <ns0:RFF_6>
               <ns0:C506_6>
                 <C50601>
                   <xsl:value-of select="Code/text()" />                                                            
                   </C50601>
                   <C50602>
                     <xsl:value-of select="Reference/text()" />                                                              
                   </C50602>              
               </ns0:C506_6>             
             </ns0:RFF_6>           
           </ns0:RFFLoop3>
          
          </xsl:for-each>
          
          <!--  END OF  RFF -->

          <!--  LIN TAX SEGMENT -->

               <xsl:for-each select="TaxTotal">
                <xsl:for-each select="TaxSubtotal"> 
              <!--  <xsl:if test="string-length(TaxCategory/ID) != 0"> -->
                  <ns0:TAXLoop3>
                   <ns0:TAX_3>
                    <xsl:if test="string-length(TaxCode) != 0">
                     <TAX01>
                      <xsl:value-of select="TaxCode/text()" />
                    </TAX01>
                  </xsl:if>
                   <xsl:if test="string-length(TaxCode) = 0">
                    <TAX01>
                      <xsl-text>7</xsl-text>
                    </TAX01>
                  </xsl:if>

                   <xsl:if test="string-length(TaxCategory/TaxScheme/Navn) != 0">
                    <ns0:C241_3>
                      <C24101>
                        <xsl:value-of select="TaxCategory/TaxScheme/Navn/text()" />
                      </C24101>
                    </ns0:C241_3>
                  </xsl:if>

                    <xsl:if test="string-length(TaxCategory/Percent) != 0">
                    <ns0:C243_3>
                      <C24304>
                        <xsl:value-of select="TaxCategory/Percent/text()" />
                      </C24304>
                    </ns0:C243_3>
                  </xsl:if>

                   <xsl:if test="string-length(TaxCategory/TaxScheme/TaxTypeCode) != 0">
                    <TAX06>
                      <xsl:value-of select="TaxCategory/TaxScheme/TaxTypeCode/text()" />
                    </TAX06>
                   </xsl:if> 

                </ns0:TAX_3>
                
               <xsl:if test="string-length(TaxableAmount) != 0">
                  <ns0:MOA_7>
                    <ns0:C516_7>
                      <C51601>
                        <xsl:text>125</xsl:text>
                      </C51601>
                      <C51602>
                        <xsl:choose>
                          <xsl:when test="TaxableAmount/text() = '0.00'">
                            <xsl:text>0</xsl:text>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select="TaxableAmount/text()" />
                          </xsl:otherwise>
                        </xsl:choose>                        
                      </C51602>
                    </ns0:C516_7>
                  </ns0:MOA_7>

                </xsl:if>

                <xsl:if test="string-length(TaxAmount) != 0">
                  <ns0:MOA_7>
                    <ns0:C516_7>
                      <C51601>
                        <xsl:text>124</xsl:text>

                      </C51601>
                      <C51602>
                        <xsl:choose>
                          <xsl:when test="TaxAmount/text() = '0.00'">
                            <xsl:text>0</xsl:text>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select="TaxAmount/text()" />
                          </xsl:otherwise>
                        </xsl:choose>                        
                      </C51602>
                    </ns0:C516_7>
                  </ns0:MOA_7>

                </xsl:if>

              </ns0:TAXLoop3> 
              <!--  </xsl:if>  -->
               </xsl:for-each>
              </xsl:for-each> 
           
          <!--  END OF  LIN TAX -->
                  
          <!--  ALC LINE SEGMENT  -->
                   
          <xsl:for-each select="AllowanceCharge">                
           <xsl:if test="string-length(ChargeIndicator) != 0">
             <ns0:ALCLoop2>
               <ns0:ALC_2>
                 <ALC01>
                   <xsl:choose>
                     <xsl:when test="translate(ChargeIndicator, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')='FALSE'">
                       <xsl:text>A</xsl:text>     
                     </xsl:when>                         
                     <xsl:when test="translate(ChargeIndicator, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')='TRUE'">
                       <xsl:text>C</xsl:text>                             
                     </xsl:when>                                              
                     <xsl:otherwise>
                       <xsl:text>XZ</xsl:text> 
                     </xsl:otherwise>                                         
                 </xsl:choose>
                 </ALC01>  
                 
                 <xsl:if test="string-length(ID) != 0">
                   <ns0:C552_2>
                    <C55201>
                     <xsl:value-of select="ID/text()" />
                    </C55201>
                   </ns0:C552_2>
                  </xsl:if> 
                                                 
                 <xsl:if test="string-length(SequenceNumeric) != 0"> 
                  <ALC04>
                   <xsl:value-of select="SequenceNumeric/text()" />
                  </ALC04>
                 </xsl:if>                  
                     
                 
                 <xsl:if test="string-length(AllowanceChargeReasonCode)!=0 or string-length(AllowanceChargeReason)!=0">
                   <ns0:C214_2>
                    <xsl:if test="string-length(AllowanceChargeReasonCode)!=0"> 
                     <C21401>
                      <xsl:value-of select="AllowanceChargeReasonCode/text()" />
                     </C21401>
                    </xsl:if>   
                                                            
                 <xsl:if test="string-length(AllowanceChargeReason)!=0"> 
                   <C21404>
                     <xsl:value-of select="normalize-space(AllowanceChargeReason/text())" />
                    </C21404>
                   </xsl:if>       
                 
                  </ns0:C214_2>                                                    
                 </xsl:if>                      
               </ns0:ALC_2>             
                                      
               <xsl:if test="string-length(MultiplierFactorNumeric) != 0">
                 <ns0:PCDLoop2>
                   <ns0:PCD_6>
                     <ns0:C501_6>
                       <C50101>
                       
                    <xsl:choose>
                     <xsl:when test="translate(ChargeIndicator, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')='FALSE'">
                       <xsl:text>1</xsl:text>                    
                     </xsl:when>    
                       <xsl:when test="translate(ChargeIndicator, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')='TRUE'">
                       <xsl:text>2</xsl:text>                    
                     </xsl:when>                                                                     
                     <xsl:otherwise>
                       <xsl:text>XZ</xsl:text>                     
                     </xsl:otherwise>                                         
                 </xsl:choose>                                              
                 </C50101>
                 <C50102>                     
                  <xsl:value-of select="format-number(MultiplierFactorNumeric*100, '0.##')" />
                 </C50102>                   
                </ns0:C501_6>
               </ns0:PCD_6>
              </ns0:PCDLoop2>
             </xsl:if>   
               
<xsl:if test="string-length(Amount) != 0">             
  <ns0:MOALoop2>    
    <ns0:MOA_8>      
     <ns0:C516_8>
                     
       <xsl:if test="string-length(AmountCode) != 0">  
         <C51601>
           <xsl:value-of select="AmountCode/text()" />
         </C51601>
       </xsl:if>     
       
       <xsl:if test="string-length(AmountCode) = 0">  
        <C51601>
         <xsl:text>8</xsl:text>
        </C51601>
       </xsl:if>                
      
     <C51602>
      <xsl:choose>
       <xsl:when test="Amount/text() = '0.00'">
        <xsl:text>0</xsl:text>                  
       </xsl:when>  
       <xsl:otherwise>        
        <xsl:value-of select="Amount/text()" />  
       </xsl:otherwise>                                         
      </xsl:choose> 
     </C51602>
     
     </ns0:C516_8>      
   </ns0:MOA_8>  
  </ns0:MOALoop2>               

  </xsl:if>     
                
  </ns0:ALCLoop2>                  
  </xsl:if>     
  </xsl:for-each>                              
          
          <!--  END OF  ALC LINE  -->


        </ns0:LINLoop1> 
        </xsl:for-each> 
      
  <!--  UNS --> 
      
       <ns0:UNS>
        <UNS01>
          <xsl:text>S</xsl:text>
        </UNS01>
       </ns0:UNS>
      
  <!--  END OF  UNS -->    
      
  <!--  MOA SECTION -->
      
     <xsl:if test="string-length(Total/LineTotalAmount) != 0">
       <ns0:MOA_11>
        <ns0:C516_11>
         <C51601>
          <xsl:text>79</xsl:text>
         </C51601>
         <C51602>  
          <xsl:choose>
           <xsl:when test="Total/LineTotalAmount/text() = '0.00'">
            <xsl:text>0</xsl:text>                  
           </xsl:when>  
           <xsl:otherwise>        
            <xsl:value-of select="Total/LineTotalAmount/text()" />  
           </xsl:otherwise>                                         
          </xsl:choose> 
         </C51602>
        </ns0:C516_11>
       </ns0:MOA_11>      
     </xsl:if>      
      
    <xsl:if test="string-length(Total/AllowanceTotalAmount) != 0">      
       <ns0:MOA_11>
        <ns0:C516_11>
         <C51601>
          <xsl:text>204</xsl:text>
         </C51601>
         <C51602>  
          <xsl:choose>
           <xsl:when test="Total/AllowanceTotalAmount/text() = '0.00'">
            <xsl:text>0</xsl:text>                  
           </xsl:when>  
           <xsl:otherwise>        
            <xsl:value-of select="Total/AllowanceTotalAmount/text()" />  
           </xsl:otherwise>                                         
          </xsl:choose> 
         </C51602>
        </ns0:C516_11>
       </ns0:MOA_11>
     </xsl:if>   
      
     <xsl:if test="string-length(Total/ChargeTotalAmount) != 0">
       <ns0:MOA_11>
        <ns0:C516_11>
         <C51601>
          <xsl:text>23</xsl:text>
         </C51601>
         <C51602>  
          <xsl:choose>
           <xsl:when test="Total/ChargeTotalAmount/text() = '0.00'">
            <xsl:text>0</xsl:text>                  
           </xsl:when>  
           <xsl:otherwise>        
            <xsl:value-of select="Total/ChargeTotalAmount/text()" />  
           </xsl:otherwise>                                         
          </xsl:choose> 
         </C51602>
        </ns0:C516_11>
       </ns0:MOA_11>
     </xsl:if>    
      
     <xsl:if test="string-length(Total/PrepaidAmount) != 0">
       <ns0:MOA_11>
        <ns0:C516_11>
         <C51601>
          <xsl:text>113</xsl:text>
         </C51601>
         <C51602>  
          <xsl:choose>
           <xsl:when test="Total/PrepaidAmount/text() = '0.00'">
            <xsl:text>0</xsl:text>                  
           </xsl:when>  
           <xsl:otherwise>        
            <xsl:value-of select="Total/PrepaidAmount/text()" />  
           </xsl:otherwise>                                         
          </xsl:choose> 
         </C51602>
        </ns0:C516_11>
       </ns0:MOA_11>
     </xsl:if>   
      
     <xsl:if test="string-length(Total/PayableAmount) != 0">
       <ns0:MOA_11>
        <ns0:C516_11>
         <C51601>
          <xsl:text>86</xsl:text>
         </C51601>
         <C51602>  
          <xsl:choose>
           <xsl:when test="Total/PayableAmount/text() = '0.00'">
            <xsl:text>0</xsl:text>                  
           </xsl:when>  
           <xsl:otherwise>        
            <xsl:value-of select="Total/PayableAmount/text()" />  
           </xsl:otherwise>                                         
          </xsl:choose> 
         </C51602>
        </ns0:C516_11>
       </ns0:MOA_11>
     </xsl:if>       
      
     <xsl:if test="string-length(Total/TaxExclAmount) != 0">
       <ns0:MOA_11>
        <ns0:C516_11>
         <C51601>
          <xsl:text>176</xsl:text>
         </C51601>
         <C51602>  
          <xsl:choose>
           <xsl:when test="Total/TaxExclAmount/text() = '0.00'">
            <xsl:text>0</xsl:text>                  
           </xsl:when>  
           <xsl:otherwise>        
            <xsl:value-of select="Total/TaxExclAmount/text()" />  
           </xsl:otherwise>                                         
          </xsl:choose> 
         </C51602>
        </ns0:C516_11>
       </ns0:MOA_11>
     </xsl:if>

      <xsl:if test="string-length(Total/TaxAmount) != 0">
        <ns0:MOA_11>
          <ns0:C516_11>
            <C51601>
              <xsl:text>124</xsl:text>
            </C51601>
            <C51602>
              <xsl:choose>
                <xsl:when test="Total/TaxAmount/text() = '0.00'">
                  <xsl:text>0</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="Total/TaxAmount/text()" />
                </xsl:otherwise>
              </xsl:choose>
            </C51602>
          </ns0:C516_11>
        </ns0:MOA_11>
      </xsl:if>

      <xsl:if test="string-length(Total/TaxableAmount) != 0">
        <ns0:MOA_11>
          <ns0:C516_11>
            <C51601>
              <xsl:text>125</xsl:text>
            </C51601>
            <C51602>
              <xsl:choose>
                <xsl:when test="Total/TaxableAmount/text() = '0.00'">
                  <xsl:text>0</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="Total/TaxableAmount/text()" />
                </xsl:otherwise>
              </xsl:choose>
            </C51602>
          </ns0:C516_11>
        </ns0:MOA_11>
      </xsl:if>

      <xsl:for-each select="Total/ExtraTotalMOALoop1/Node">
       <ns0:MOA_11>
        <ns0:C516_11>
         <C51601>
          <xsl:value-of select="Code/text()" />  
         </C51601>
         <C51602> 
          <xsl:choose>
           <xsl:when test="Amount/text() = '0.00'">
            <xsl:text>0</xsl:text>                  
           </xsl:when>  
           <xsl:otherwise>        
            <xsl:value-of select="Amount/text()" />  
           </xsl:otherwise>                                         
          </xsl:choose> 
         </C51602>
        </ns0:C516_11>
       </ns0:MOA_11>               
      </xsl:for-each>        
      
  <!--  MOA IS  GOOD  :)  -->    
      
  <!--  CNT -->     
      
       <ns0:CNT>
       <ns0:C270>
        <C27001>
         <xsl:text>1</xsl:text>
         </C27001>
         <C27002>
          <xsl:value-of select="sum(Lines/Line/Quantity/Node[Code='21']/Quantity)" />
         </C27002>
        </ns0:C270>
       </ns0:CNT>        
      
      <ns0:CNT>
       <ns0:C270>
        <C27001>
         <xsl:text>2</xsl:text>
         </C27001>
         <C27002>
          <xsl:value-of select="count(Lines/Line)" />
         </C27002>
        </ns0:C270>
       </ns0:CNT>        
    
  <!--  END OF  CNT -->     

    </ns0:EFACT_D96A_ORDRSP>
  </xsl:template>

  <msxsl:script language="C#" implements-prefix="userCSharp">
    <![CDATA[
  public string ConvertUnitCode(string ConversionDirection, string IngoingUnitCode)
  {
  string OutgoingUnitCode = "";

  switch (ConversionDirection)
  {
  case "TUNtoUBL":
  switch (IngoingUnitCode)
  {
  case "%KG":
  OutgoingUnitCode = "HK";
  break;
  case "HMT":
  OutgoingUnitCode = "HMT";
  break;
  case "%ST":
  OutgoingUnitCode = "CNP";
  break;
  case "TUS":
  OutgoingUnitCode = "T3";
  break;
  case "ST":
  OutgoingUnitCode = "ST";
  break;
  case "BB":
  OutgoingUnitCode = "BB";
  break;
  case "BDT":
  OutgoingUnitCode = "BE";
  break;
  case "BLK":
  OutgoingUnitCode = "D64";
  break;
  case "BX":
  OutgoingUnitCode = "BX";
  break;
  case "PCE":
  OutgoingUnitCode = "C62";
  break;
  case "BEG":
  OutgoingUnitCode = "CU";
  break;
  case "BOT":
  OutgoingUnitCode = "2W";
  break;
  case "CMT":
  OutgoingUnitCode = "CMT";
  break;
  case "CH":
  OutgoingUnitCode = "CH";
  break;
  case "DAY":
  OutgoingUnitCode = "DAY";
  break;
  case "DIS":
  OutgoingUnitCode = "DS";
  break;
  case "DLT":
  OutgoingUnitCode = "DLT";
  break;
  case "DMT":
  OutgoingUnitCode = "DMT";
  break;
  case "DK":
  OutgoingUnitCode = "CA";
  break;
  case "DS":
  OutgoingUnitCode = "TN";
  break;
  case "FL":
  OutgoingUnitCode = "BO";
  break;
  case "FOT":
  OutgoingUnitCode = "FOT";
  break;
  case "GRM":
  OutgoingUnitCode = "GRM";
  break;
  case "GRO":
  OutgoingUnitCode = "GRO";
  break;
  case "HLT":
  OutgoingUnitCode = "HLT";
  break;
  case "KAR":
  OutgoingUnitCode = "CT";
  break;
  case "KS":
  OutgoingUnitCode = "Z2";
  break;
  case "KGM":
  OutgoingUnitCode = "KGM";
  break;
  case "CMQ":
  OutgoingUnitCode = "CMQ";
  break;
  case "MTQ":
  OutgoingUnitCode = "MTQ";
  break;
  case "CMK":
  OutgoingUnitCode = "CMK";
  break;
  case "MTK":
  OutgoingUnitCode = "MTK";
  break;
  case "LGD":
  OutgoingUnitCode = "LN";
  break;
  case "LTR":
  OutgoingUnitCode = "LTR";
  break;
  case "LES":
  OutgoingUnitCode = "NL";
  break;
  case "MTR":
  OutgoingUnitCode = "MTR";
  break;
  case "MLT":
  OutgoingUnitCode = "MLT";
  break;
  case "MMT":
  OutgoingUnitCode = "MMT";
  break;
  case "PAK":
  OutgoingUnitCode = "PK";
  break;
  case "PF":
  OutgoingUnitCode = "PF";
  break;
  case "PB":
  OutgoingUnitCode = "BB";
  break;
  case "PAR":
  OutgoingUnitCode = "PR";
  break;
  case "PAT":
  OutgoingUnitCode = "CQ";
  break;
  case "PL":
  OutgoingUnitCode = "PG";
  break;
  case "PS":
  OutgoingUnitCode = "BG";
  break;
  case "RIN":
  OutgoingUnitCode = "RG";
  break;
  case "NRL":
  OutgoingUnitCode = "RO";
  break;
  case "ROR":
  OutgoingUnitCode = "TU";
  break;
  case "SP":
  OutgoingUnitCode = "BJ";
  break;
  case "SPO":
  OutgoingUnitCode = "SO";
  break;
  case "SAK":
  OutgoingUnitCode = "SA";
  break;
  case "SET":
  OutgoingUnitCode = "SET";
  break;
  case "HUR":
  OutgoingUnitCode = "HUR";
  break;
  case "TNE":
  OutgoingUnitCode = "TNE";
  break;
  case "TR":
  OutgoingUnitCode = "DR";
  break;
  case "TB":
  OutgoingUnitCode = "TB";
  break;
  case "ASK":
  OutgoingUnitCode = "CS";
  break;
  case "BÅND":
  OutgoingUnitCode = "BND";
  break;
  case "BREV":
  OutgoingUnitCode = "BRV";
  break;
  case "COL":
  OutgoingUnitCode = "COL";
  break;
  case "HAP":
  OutgoingUnitCode = "HAP";
  break;
  case "KORT":
  OutgoingUnitCode = "KOR";
  break;
  case "ROND":
  OutgoingUnitCode = "RON";
  break;
  case "SKF":
  OutgoingUnitCode = "SKF";
  break;
  case "TUBE":
  OutgoingUnitCode = "TB";
  break;
  case "XXX":
  OutgoingUnitCode = "XXX";
  break;
  default:
  OutgoingUnitCode = IngoingUnitCode;
  break;
  }
  break;

  case "UBLtoTUN":

  switch (IngoingUnitCode)
  {
  case "HK":
  OutgoingUnitCode = "%KG";
  break;
  case "HMT":
  OutgoingUnitCode = "HMT";
  break;
  case "CNP":
  OutgoingUnitCode = "%ST";
  break;
  case "T3":
  OutgoingUnitCode = "TUS";
  break;
  case "ST":
  OutgoingUnitCode = "ST";
  break;
  case "BE":
  OutgoingUnitCode = "BDT";
  break;
  case "D64":
  OutgoingUnitCode = "BLK";
  break;
  case "BX":
  OutgoingUnitCode = "BX";
  break;
  case "C62":
  OutgoingUnitCode = "PCE";
  break;
  case "CU":
  OutgoingUnitCode = "BEG";
  break;
  case "2W":
  OutgoingUnitCode = "BOT";
  break;
  case "CMT":
  OutgoingUnitCode = "CMT";
  break;
  case "CH":
  OutgoingUnitCode = "CH";
  break;
  case "DAY":
  OutgoingUnitCode = "DAY";
  break;
  case "DS":
  OutgoingUnitCode = "DIS";
  break;
  case "DLT":
  OutgoingUnitCode = "DLT";
  break;
  case "DMT":
  OutgoingUnitCode = "DMT";
  break;
  case "CA":
  OutgoingUnitCode = "DK";
  break;
  case "TN":
  OutgoingUnitCode = "DS";
  break;
  case "BO":
  OutgoingUnitCode = "FL";
  break;
  case "FOT":
  OutgoingUnitCode = "FOT";
  break;
  case "GRM":
  OutgoingUnitCode = "GRM";
  break;
  case "GRO":
  OutgoingUnitCode = "GRO";
  break;
  case "HLT":
  OutgoingUnitCode = "HLT";
  break;
  case "CT":
  OutgoingUnitCode = "KAR";
  break;
  case "Z2":
  OutgoingUnitCode = "KS";
  break;
  case "KGM":
  OutgoingUnitCode = "KGM";
  break;
  case "CMQ":
  OutgoingUnitCode = "CMQ";
  break;
  case "MTQ":
  OutgoingUnitCode = "MTQ";
  break;
  case "CMK":
  OutgoingUnitCode = "CMK";
  break;
  case "MTK":
  OutgoingUnitCode = "MTK";
  break;
  case "LN":
  OutgoingUnitCode = "LGD";
  break;
  case "LTR":
  OutgoingUnitCode = "LTR";
  break;
  case "NL":
  OutgoingUnitCode = "LES";
  break;
  case "MTR":
  OutgoingUnitCode = "MTR";
  break;
  case "MLT":
  OutgoingUnitCode = "MLT";
  break;
  case "MMT":
  OutgoingUnitCode = "MMT";
  break;
  case "PK":
  OutgoingUnitCode = "PAK";
  break;
  case "PF":
  OutgoingUnitCode = "PF";
  break;
  case "BB":
  OutgoingUnitCode = "PB";
  break;
  case "PR":
  OutgoingUnitCode = "PAR";
  break;
  case "CQ":
  OutgoingUnitCode = "PAT";
  break;
  case "PG":
  OutgoingUnitCode = "PL";
  break;
  case "BG":
  OutgoingUnitCode = "PS";
  break;
  case "RG":
  OutgoingUnitCode = "RIN";
  break;
  case "RO":
  OutgoingUnitCode = "NRL";
  break;
  case "TU":
  OutgoingUnitCode = "ROR";
  break;
  case "BJ":
  OutgoingUnitCode = "SP";
  break;
  case "SO":
  OutgoingUnitCode = "SPO";
  break;
  case "SA":
  OutgoingUnitCode = "SAK";
  break;
  case "SET":
  OutgoingUnitCode = "SET";
  break;
  case "HUR":
  OutgoingUnitCode = "HUR";
  break;
  case "TNE":
  OutgoingUnitCode = "TNE";
  break;
  case "DR":
  OutgoingUnitCode = "TR";
  break;
  case "CS":
  OutgoingUnitCode = "ASK";
  break;
  case "BND":
  OutgoingUnitCode = "BÅND";
  break;
  case "BRV":
  OutgoingUnitCode = "BREV";
  break;
  case "COL":
  OutgoingUnitCode = "COL";
  break;
  case "HAP":
  OutgoingUnitCode = "HAP";
  break;
  case "KOR":
  OutgoingUnitCode = "KORT";
  break;
  case "PB":
  OutgoingUnitCode = "PB";
  break;
  case "RON":
  OutgoingUnitCode = "ROND";
  break;
  case "SKF":
  OutgoingUnitCode = "SKF";
  break;
  case "TB":
  OutgoingUnitCode = "TB";
  break;
  case "XXX":
  OutgoingUnitCode = "XXX";
  break;
  }
  break;

  default:
  OutgoingUnitCode = IngoingUnitCode;
  break;
  }

  if (OutgoingUnitCode == "")
  {
  OutgoingUnitCode = IngoingUnitCode;
  }

  return OutgoingUnitCode;
  }
  ]]>
  </msxsl:script>  
  
</xsl:stylesheet>