namespace BYGE.Integration.EDIFACTInvoice {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.Core.Invoice.Invoice", typeof(global::BYGE.Integration.Core.Invoice.Invoice))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.EDIFACTInvoice.EFACT_D96A_INVOIC", typeof(global::BYGE.Integration.EDIFACTInvoice.EFACT_D96A_INVOIC))]
    public sealed class CoreInvoice_to_EdifactInvoice : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var s0 userCSharp"" version=""1.0"" xmlns:s0=""http://byg-e.dk/schemas/v10"" xmlns:ns0=""http://schemas.microsoft.com/BizTalk/EDI/EDIFACT/2006"" xmlns:userCSharp=""http://schemas.microsoft.com/BizTalk/2003/userCSharp"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/s0:Invoice"" />
  </xsl:template>
  <xsl:template match=""/s0:Invoice"">
    <xsl:variable name=""var:v2"" select=""userCSharp:LogicalExistence(boolean(OrderReference/OrderDate))"" />
    <ns0:EFACT_D96A_INVOIC>
      <ns0:BGM>
        <ns0:C002>
          <C00201>
            <xsl:value-of select=""InvoiceType/text()"" />
          </C00201>
        </ns0:C002>
        <BGM02>
          <xsl:value-of select=""InvoiceID/text()"" />
        </BGM02>
      </ns0:BGM>
      <ns0:DTM>
        <ns0:C507>
          <C50702>
            <xsl:value-of select=""IssueDate/text()"" />
          </C50702>
        </ns0:C507>
      </ns0:DTM>
      <ns0:FTX>
        <xsl:for-each select=""Note"">
          <ns0:C108>
            <C10801>
              <xsl:value-of select=""./text()"" />
            </C10801>
          </ns0:C108>
        </xsl:for-each>
      </ns0:FTX>
      <ns0:RFFLoop1>
        <ns0:RFF>
          <ns0:C506>
            <xsl:variable name=""var:v1"" select=""userCSharp:ReturnRFFON(string(OrderReference/OrderID/text()))"" />
            <C50601>
              <xsl:value-of select=""$var:v1"" />
            </C50601>
            <C50602>
              <xsl:value-of select=""OrderReference/OrderID/text()"" />
            </C50602>
          </ns0:C506>
        </ns0:RFF>
        <ns0:DTM_2>
          <ns0:C507_2>
            <xsl:if test=""$var:v2"" />
          </ns0:C507_2>
        </ns0:DTM_2>
      </ns0:RFFLoop1>
      <xsl:for-each select=""AccountingSupplier"">
        <ns0:NADLoop1>
          <ns0:NAD>
            <xsl:variable name=""var:v3"" select=""userCSharp:ReturnSU(string(AccSellerID/text()))"" />
            <NAD01>
              <xsl:value-of select=""$var:v3"" />
            </NAD01>
            <ns0:C082>
              <C08201>
                <xsl:value-of select=""AccSellerID/text()"" />
              </C08201>
            </ns0:C082>
            <ns0:C080>
              <xsl:if test=""Address/Name"">
                <C08001>
                  <xsl:value-of select=""Address/Name/text()"" />
                </C08001>
              </xsl:if>
            </ns0:C080>
            <ns0:C059>
              <xsl:if test=""Address/Street"">
                <C05901>
                  <xsl:value-of select=""Address/Street/text()"" />
                </C05901>
              </xsl:if>
            </ns0:C059>
            <xsl:if test=""Address/City"">
              <NAD06>
                <xsl:value-of select=""Address/City/text()"" />
              </NAD06>
            </xsl:if>
            <xsl:if test=""Address/PostalCode"">
              <NAD08>
                <xsl:value-of select=""Address/PostalCode/text()"" />
              </NAD08>
            </xsl:if>
            <xsl:if test=""Address/Country"">
              <NAD09>
                <xsl:value-of select=""Address/Country/text()"" />
              </NAD09>
            </xsl:if>
          </ns0:NAD>
        </ns0:NADLoop1>
      </xsl:for-each>
      <ns0:NADLoop1>
        <ns0:NAD>
          <xsl:variable name=""var:v4"" select=""userCSharp:ReturnIV(string(AccountingCustomer/AccBuyerID/text()))"" />
          <NAD01>
            <xsl:value-of select=""$var:v4"" />
          </NAD01>
          <ns0:C082>
            <C08201>
              <xsl:value-of select=""AccountingCustomer/AccBuyerID/text()"" />
            </C08201>
          </ns0:C082>
          <ns0:C080>
            <xsl:if test=""AccountingCustomer/Address/Name"">
              <C08001>
                <xsl:value-of select=""AccountingCustomer/Address/Name/text()"" />
              </C08001>
            </xsl:if>
          </ns0:C080>
          <ns0:C059>
            <xsl:if test=""AccountingCustomer/Address/Street"">
              <C05901>
                <xsl:value-of select=""AccountingCustomer/Address/Street/text()"" />
              </C05901>
            </xsl:if>
          </ns0:C059>
          <xsl:if test=""AccountingCustomer/Address/City"">
            <NAD06>
              <xsl:value-of select=""AccountingCustomer/Address/City/text()"" />
            </NAD06>
          </xsl:if>
          <xsl:if test=""AccountingCustomer/Address/PostalCode"">
            <NAD08>
              <xsl:value-of select=""AccountingCustomer/Address/PostalCode/text()"" />
            </NAD08>
          </xsl:if>
          <xsl:if test=""AccountingCustomer/Address/Country"">
            <NAD09>
              <xsl:value-of select=""AccountingCustomer/Address/Country/text()"" />
            </NAD09>
          </xsl:if>
        </ns0:NAD>
      </ns0:NADLoop1>
      <ns0:NADLoop1>
        <ns0:NAD>
          <xsl:variable name=""var:v5"" select=""userCSharp:ReturnBY(string(BuyerCustomer/PartyID/text()))"" />
          <NAD01>
            <xsl:value-of select=""$var:v5"" />
          </NAD01>
          <ns0:C082>
            <xsl:if test=""BuyerCustomer/PartyID"">
              <C08201>
                <xsl:value-of select=""BuyerCustomer/PartyID/text()"" />
              </C08201>
            </xsl:if>
          </ns0:C082>
          <ns0:C080>
            <C08001>
              <xsl:value-of select=""BuyerCustomer/Address/Name/text()"" />
            </C08001>
          </ns0:C080>
          <ns0:C059>
            <xsl:if test=""BuyerCustomer/Address/Street"">
              <C05901>
                <xsl:value-of select=""BuyerCustomer/Address/Street/text()"" />
              </C05901>
            </xsl:if>
          </ns0:C059>
          <xsl:if test=""BuyerCustomer/Address/City"">
            <NAD06>
              <xsl:value-of select=""BuyerCustomer/Address/City/text()"" />
            </NAD06>
          </xsl:if>
          <xsl:if test=""BuyerCustomer/Address/PostalCode"">
            <NAD08>
              <xsl:value-of select=""BuyerCustomer/Address/PostalCode/text()"" />
            </NAD08>
          </xsl:if>
          <xsl:if test=""BuyerCustomer/Address/Country"">
            <NAD09>
              <xsl:value-of select=""BuyerCustomer/Address/Country/text()"" />
            </NAD09>
          </xsl:if>
        </ns0:NAD>
      </ns0:NADLoop1>
      <xsl:for-each select=""DeliveryLocation"">
        <ns0:NADLoop1>
          <ns0:NAD>
            <xsl:variable name=""var:v6"" select=""userCSharp:ReturnDP(string(PartyID/text()))"" />
            <NAD01>
              <xsl:value-of select=""$var:v6"" />
            </NAD01>
            <ns0:C082>
              <xsl:if test=""PartyID"">
                <C08201>
                  <xsl:value-of select=""PartyID/text()"" />
                </C08201>
              </xsl:if>
            </ns0:C082>
            <ns0:C080>
              <C08001>
                <xsl:value-of select=""Address/Name/text()"" />
              </C08001>
            </ns0:C080>
            <ns0:C059>
              <xsl:if test=""Address/Street"">
                <C05901>
                  <xsl:value-of select=""Address/Street/text()"" />
                </C05901>
              </xsl:if>
            </ns0:C059>
            <xsl:if test=""Address/City"">
              <NAD06>
                <xsl:value-of select=""Address/City/text()"" />
              </NAD06>
            </xsl:if>
            <xsl:if test=""Address/PostalCode"">
              <NAD08>
                <xsl:value-of select=""Address/PostalCode/text()"" />
              </NAD08>
            </xsl:if>
            <xsl:if test=""Address/Country"">
              <NAD09>
                <xsl:value-of select=""Address/Country/text()"" />
              </NAD09>
            </xsl:if>
          </ns0:NAD>
        </ns0:NADLoop1>
      </xsl:for-each>
      <ns0:TAXLoop1>
        <xsl:for-each select=""TaxTotal"">
          <xsl:for-each select=""TaxSubtotal"">
            <xsl:for-each select=""TaxCategory"">
              <xsl:for-each select=""TaxScheme"">
                <ns0:TAX>
                  <ns0:C243>
                    <xsl:if test=""Navn"">
                      <C24301>
                        <xsl:value-of select=""Navn/text()"" />
                      </C24301>
                    </xsl:if>
                    <xsl:if test=""../../TaxPercent"">
                      <C24305>
                        <xsl:value-of select=""../../TaxPercent/text()"" />
                      </C24305>
                    </xsl:if>
                  </ns0:C243>
                  <xsl:if test=""TaxTypeCode"">
                    <TAX06>
                      <xsl:value-of select=""TaxTypeCode/text()"" />
                    </TAX06>
                  </xsl:if>
                </ns0:TAX>
              </xsl:for-each>
            </xsl:for-each>
          </xsl:for-each>
        </xsl:for-each>
      </ns0:TAXLoop1>
      <ns0:CUXLoop1>
        <ns0:CUX>
          <ns0:C504>
            <xsl:if test=""Currency"">
              <C50402>
                <xsl:value-of select=""Currency/text()"" />
              </C50402>
            </xsl:if>
          </ns0:C504>
        </ns0:CUX>
      </ns0:CUXLoop1>
      <xsl:for-each select=""Lines/Line"">
        <ns0:LINLoop1>
          <ns0:LIN>
            <LIN01>
              <xsl:value-of select=""LineNo/text()"" />
            </LIN01>
            <ns0:C212>
              <xsl:if test=""Item/StandardItemID"">
                <C21201>
                  <xsl:value-of select=""Item/StandardItemID/text()"" />
                </C21201>
              </xsl:if>
            </ns0:C212>
          </ns0:LIN>
          <ns0:PIA>
            <ns0:C212_2>
              <xsl:if test=""Item/SellerItemID"">
                <C21201>
                  <xsl:value-of select=""Item/SellerItemID/text()"" />
                </C21201>
              </xsl:if>
            </ns0:C212_2>
            <ns0:C212_3>
              <xsl:if test=""Item/AdditionalItemID"">
                <C21201>
                  <xsl:value-of select=""Item/AdditionalItemID/text()"" />
                </C21201>
              </xsl:if>
            </ns0:C212_3>
          </ns0:PIA>
          <ns0:IMD_2>
            <ns0:C273_2>
              <xsl:if test=""Item/Name"">
                <C27304>
                  <xsl:value-of select=""Item/Name/text()"" />
                </C27304>
              </xsl:if>
            </ns0:C273_2>
          </ns0:IMD_2>
          <ns0:MEA_2>
            <ns0:C502_2>
              <xsl:if test=""PriceNet/PristypeCode"">
                <C50201>
                  <xsl:value-of select=""PriceNet/PristypeCode/text()"" />
                </C50201>
              </xsl:if>
              <C50202>
                <xsl:value-of select=""PriceNet/Quantity/text()"" />
              </C50202>
            </ns0:C502_2>
            <ns0:C174_2>
              <xsl:if test=""PriceNet/UnitCode"">
                <C17401>
                  <xsl:value-of select=""PriceNet/UnitCode/text()"" />
                </C17401>
              </xsl:if>
            </ns0:C174_2>
          </ns0:MEA_2>
          <ns0:QTY_2>
            <ns0:C186_2>
              <C18602>
                <xsl:value-of select=""Quantity/text()"" />
              </C18602>
              <C18603>
                <xsl:value-of select=""UnitCode/text()"" />
              </C18603>
            </ns0:C186_2>
          </ns0:QTY_2>
          <ns0:FTX_5>
            <ns0:C108_5>
              <xsl:if test=""Note"">
                <C10801>
                  <xsl:value-of select=""Note/text()"" />
                </C10801>
              </xsl:if>
            </ns0:C108_5>
          </ns0:FTX_5>
          <ns0:MOALoop2>
            <ns0:MOA_5>
              <ns0:C516_5>
                <C51602>
                  <xsl:value-of select=""LineAmountTotal/text()"" />
                </C51602>
              </ns0:C516_5>
            </ns0:MOA_5>
          </ns0:MOALoop2>
          <ns0:PRILoop1>
            <ns0:PRI>
              <ns0:C509>
                <xsl:if test=""PriceGross/PristypeCode"">
                  <C50901>
                    <xsl:value-of select=""PriceGross/PristypeCode/text()"" />
                  </C50901>
                </xsl:if>
                <xsl:if test=""PriceGross/Price"">
                  <C50902>
                    <xsl:value-of select=""PriceGross/Price/text()"" />
                  </C50902>
                </xsl:if>
              </ns0:C509>
            </ns0:PRI>
          </ns0:PRILoop1>
        </ns0:LINLoop1>
      </xsl:for-each>
      <ns0:MOALoop4>
        <ns0:MOA_10>
          <ns0:C516_10>
            <C51602>
              <xsl:value-of select=""Total/LineTotalAmount/text()"" />
            </C51602>
          </ns0:C516_10>
        </ns0:MOA_10>
      </ns0:MOALoop4>
      <ns0:TAXLoop5>
        <ns0:TAX_5>
          <xsl:if test=""Total/TaxInclAmount"">
            <TAX04>
              <xsl:value-of select=""Total/TaxInclAmount/text()"" />
            </TAX04>
          </xsl:if>
        </ns0:TAX_5>
        <ns0:MOA_11>
          <ns0:C516_11>
            <xsl:if test=""Total/TaxExclAmount"">
              <C51602>
                <xsl:value-of select=""Total/TaxExclAmount/text()"" />
              </C51602>
            </xsl:if>
          </ns0:C516_11>
        </ns0:MOA_11>
      </ns0:TAXLoop5>
    </ns0:EFACT_D96A_INVOIC>
  </xsl:template>
  <msxsl:script language=""C#"" implements-prefix=""userCSharp""><![CDATA[
public string ReturnRFFON(string OrderID)
{
	return ""ON"";
}


public bool LogicalNe(string val1, string val2)
{
	bool ret = false;
	double d1 = 0;
	double d2 = 0;
	if (IsNumeric(val1, ref d1) && IsNumeric(val2, ref d2))
	{
		ret = d1 != d2;
	}
	else
	{
		ret = String.Compare(val1, val2, StringComparison.Ordinal) != 0;
	}
	return ret;
}


public bool LogicalExistence(bool val)
{
	return val;
}


public bool LogicalAnd(string param0, string param1)
{
	return ValToBool(param0) && ValToBool(param1);
	return false;
}


public string ReturnSU(string AccSellerID)
{
     return ""SU"";
}

public string ReturnIV(string AccBuyerID)
{
     return ""IV"";
}

public string ReturnDP(string PartyID)
{
     return ""DP"";
}

public string ReturnBY(string PartyID)
{
     return ""BY"";
}

public bool IsNumeric(string val)
{
	if (val == null)
	{
		return false;
	}
	double d = 0;
	return Double.TryParse(val, System.Globalization.NumberStyles.AllowThousands | System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out d);
}

public bool IsNumeric(string val, ref double d)
{
	if (val == null)
	{
		return false;
	}
	return Double.TryParse(val, System.Globalization.NumberStyles.AllowThousands | System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out d);
}

public bool ValToBool(string val)
{
	if (val != null)
	{
		if (string.Compare(val, bool.TrueString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return true;
		}
		if (string.Compare(val, bool.FalseString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return false;
		}
		val = val.Trim();
		if (string.Compare(val, bool.TrueString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return true;
		}
		if (string.Compare(val, bool.FalseString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return false;
		}
		double d = 0;
		if (IsNumeric(val, ref d))
		{
			return (d > 0);
		}
	}
	return false;
}


]]></msxsl:script>
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"BYGE.Integration.Core.Invoice.Invoice";
        
        private const global::BYGE.Integration.Core.Invoice.Invoice _srcSchemaTypeReference0 = null;
        
        private const string _strTrgSchemasList0 = @"BYGE.Integration.EDIFACTInvoice.EFACT_D96A_INVOIC";
        
        private const global::BYGE.Integration.EDIFACTInvoice.EFACT_D96A_INVOIC _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [1];
                _SrcSchemas[0] = @"BYGE.Integration.Core.Invoice.Invoice";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"BYGE.Integration.EDIFACTInvoice.EFACT_D96A_INVOIC";
                return _TrgSchemas;
            }
        }
    }
}
