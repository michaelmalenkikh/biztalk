namespace BYGE.Integration.EDIFACTInvoice {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.EDIFACTInvoice.EFACT_D96A_INVOIC", typeof(global::BYGE.Integration.EDIFACTInvoice.EFACT_D96A_INVOIC))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.Core.Invoice.Invoice", typeof(global::BYGE.Integration.Core.Invoice.Invoice))]
    public sealed class EDIFACTInvoice_to_CoreInvoice_KeepMandatoryFields : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" xmlns:userCSharp=""http://schemas.microsoft.com/BizTalk/2003/userCSharp"" exclude-result-prefixes=""msxsl var s0 userCSharp"" version=""1.0"" xmlns:ns0=""http://byg-e.dk/schemas/v10"" xmlns:s0=""http://schemas.microsoft.com/BizTalk/EDI/EDIFACT/2006"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/s0:EFACT_D96A_INVOIC"" />
  </xsl:template>
  <!-- In this map if incoming file does not contain mandatory MOA totals, NAD GLNs or CNT or MOA+203, PRI+AAA or PRI+AAB in Invoice line we terminate processing-->
  <!-- This mapping is being used. Make all adjustments here -->
  <!-- BDB/SJ 200401: New xslt - NOT FINISH -->
  <!-- BDB/SJ 200911-14: xslt corrected and commented -->
  <!-- *** Arminox don't send NAD+IV - so always Johannes Fog 5790001341633 *** -->
  <!-- Appended by MMA -->
  <xsl:template match=""/s0:EFACT_D96A_INVOIC"">
    <ns0:Invoice>
     <!-- Mandatory fields check section -->
      <xsl:if test=""string-length(s0:RFFLoop1/s0:RFF/s0:C506[C50601='ON']/C50602) = 0"">
        <xsl:message terminate=""yes"">RFF+ON not received in this message</xsl:message>
      </xsl:if>
      <xsl:if test=""string-length(s0:NADLoop1/s0:NAD[NAD01='SU']/s0:C082/C08201) = 0"">
        <xsl:message terminate=""yes"">NAD+SU or foeld 3039 for NAD+SU not received in this message</xsl:message>
      </xsl:if>
      <xsl:if test=""string-length(s0:NADLoop1/s0:NAD[NAD01='BY']/s0:C082/C08201) = 0"">
        <xsl:message terminate=""yes"">NAD+BY or foeld 3039 for NAD+SU not received in this message</xsl:message>
      </xsl:if>      
      <xsl:if test=""string-length(s0:MOALoop4/s0:MOA_10/s0:C516_10[C51601='79']/C51602) = 0"">
        <xsl:message terminate=""yes"">MOA+79 not received in this message</xsl:message>
      </xsl:if>
      <xsl:if test=""string-length(s0:MOALoop4/s0:MOA_10/s0:C516_10[C51601='86']/C51602) = 0"">
        <xsl:message terminate=""yes"">MOA+86 not received in this message</xsl:message>
      </xsl:if>
      <xsl:if test=""string-length(s0:MOALoop4/s0:MOA_10/s0:C516_10[C51601='125']/C51602) = 0"">
        <xsl:message terminate=""yes"">MOA+125 not received in this message</xsl:message>
      </xsl:if>
      <xsl:if test=""string-length(s0:MOALoop4/s0:MOA_10/s0:C516_10[C51601='176']/C51602) = 0""> 
        <xsl:message terminate=""yes"">MOA+176 not received in this message</xsl:message>
       </xsl:if> 
      <xsl:if test=""string-length(s0:CNT/s0:C270[C27001='2']/C27002) = 0""> 
        <xsl:message terminate=""yes"">CNT not received in this message</xsl:message>
       </xsl:if>      
     <!-- *** Global variables  *** -->
      <xsl:variable name=""MOA_176_Footer"">
        <xsl:value-of select=""s0:MOALoop4/s0:MOA_10/s0:C516_10[C51601='176']/C51602/text()"" />
      </xsl:variable>
      <xsl:variable name=""MOA_124_Footer"">
        <xsl:value-of select=""s0:MOALoop4/s0:MOA_10/s0:C516_10[C51601='124']/C51602/text()"" />
      </xsl:variable>
      <xsl:variable name=""MOA_125_Footer"">
        <xsl:value-of select=""s0:MOALoop4/s0:MOA_10/s0:C516_10[C51601='125']/C51602/text()"" />
      </xsl:variable>

      <xsl:variable name=""MOA_176_TAXFooter"">
        <xsl:value-of select=""s0:TAXLoop5/s0:MOA_11/s0:C516_10[C51601='176']/C51602/text()"" />
      </xsl:variable>
      <xsl:variable name=""MOA_124_TAXFooter"">
        <xsl:value-of select=""s0:TAXLoop5/s0:MOA_11/s0:C516_10[C51601='124']/C51602/text()"" />
      </xsl:variable>
      <xsl:variable name=""MOA_125_TAXFooter"">
        <xsl:value-of select=""s0:TAXLoop5/s0:MOA_11/s0:C516_10[C51601='125']/C51602/text()"" />
      </xsl:variable>
      
      <!-- *** BGM *** -->
      <InvoiceID>
        <xsl:value-of select=""s0:BGM/BGM02/text()"" />
      </InvoiceID>
      <xsl:if test=""s0:BGM/s0:C002/C00201"">
        <InvoiceType>
          <xsl:value-of select=""s0:BGM/s0:C002/C00201/text()"" />
        </InvoiceType>
      </xsl:if>      
      <UUID>
        <xsl:value-of select=""userCSharp:ReturnGUID()"" />       
      </UUID>
     <!-- *** End of  BGM *** --> 
      
     <!-- *** DTM *** -->
      <xsl:for-each select=""s0:DTM/s0:C507"">
        <xsl:choose>
          <xsl:when test=""C50701 = '137'"">
            <IssueDate>
              <xsl:value-of select=""userCSharp:editDate(C50702/text())"" /> 
            </IssueDate>
          </xsl:when>
         <xsl:otherwise>
          <DTMReference>
           <Node>
            <Code>
             <xsl:value-of select=""C50701/text()"" />
            </Code>
            <TimeStamp>
             <xsl:value-of select=""userCSharp:editDate(C50702/text())"" />
            </TimeStamp>
           </Node> 
          </DTMReference>         
        </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each>
     <!-- *** End  of  DTM *** --> 
      
      
      <!--  FTX SEGMENT -->      
       <xsl:for-each select=""s0:FTX"">
       <NoteLoop>    
         <Node>
       <xsl:if test=""FTX01"">
         <NoteQualifier>  
          <xsl:value-of select=""FTX01/text()"" />      
         </NoteQualifier>    
       </xsl:if>  
        <xsl:if test=""FTX02"">
         <NoteFunction>  
          <xsl:value-of select=""FTX02/text()"" />      
         </NoteFunction>    
       </xsl:if>  
        <xsl:if test=""FTX05"">
         <NoteLanguage>  
          <xsl:value-of select=""FTX05/text()"" />      
         </NoteLanguage>    
       </xsl:if>         
        <Note>
          <xsl:value-of select=""s0:C108/C10801/text()"" />
        </Note>  
       </Node>
         
          <xsl:if test=""s0:C108/C10802"">
         <Node>
           <xsl:if test=""FTX01"">
         <NoteQualifier>  
          <xsl:value-of select=""FTX01/text()"" />      
         </NoteQualifier>    
       </xsl:if>  
        <xsl:if test=""FTX02"">
         <NoteFunction>  
          <xsl:value-of select=""FTX02/text()"" />      
         </NoteFunction>    
       </xsl:if>  
        <xsl:if test=""FTX05"">
         <NoteLanguage>  
          <xsl:value-of select=""FTX05/text()"" />      
         </NoteLanguage>    
       </xsl:if>         
          <Note>
            <xsl:value-of select=""s0:C108/C10802/text()"" />        
          </Note>
         </Node>             
          </xsl:if>
       
         
          <xsl:if test=""s0:C108/C10803"">
            <Node>
              <xsl:if test=""FTX01"">
         <NoteQualifier>  
          <xsl:value-of select=""FTX01/text()"" />      
         </NoteQualifier>    
       </xsl:if>  
        <xsl:if test=""FTX02"">
         <NoteFunction>  
          <xsl:value-of select=""FTX02/text()"" />      
         </NoteFunction>    
       </xsl:if>  
        <xsl:if test=""FTX05"">
         <NoteLanguage>  
          <xsl:value-of select=""FTX05/text()"" />      
         </NoteLanguage>    
       </xsl:if>         
            <Note>
             <xsl:value-of select=""s0:C108/C10803/text()"" />        
            </Note>
           </Node>   
          </xsl:if>
         
          <xsl:if test=""s0:C108/C10804""> 
          <Node>
            <xsl:if test=""FTX01"">
         <NoteQualifier>  
          <xsl:value-of select=""FTX01/text()"" />      
         </NoteQualifier>    
       </xsl:if>  
        <xsl:if test=""FTX02"">
         <NoteFunction>  
          <xsl:value-of select=""FTX02/text()"" />      
         </NoteFunction>    
       </xsl:if>  
        <xsl:if test=""FTX05"">
         <NoteLanguage>  
          <xsl:value-of select=""FTX05/text()"" />      
         </NoteLanguage>    
       </xsl:if>         
           <Note>
            <xsl:value-of select=""s0:C108/C10804/text()"" />        
          </Note>
          </Node>  
          </xsl:if>

          <xsl:if test=""s0:C108/C10805""> 
           <Node>
             <xsl:if test=""FTX01"">
         <NoteQualifier>  
          <xsl:value-of select=""FTX01/text()"" />      
         </NoteQualifier>    
       </xsl:if>  
        <xsl:if test=""FTX02"">
         <NoteFunction>  
          <xsl:value-of select=""FTX02/text()"" />      
         </NoteFunction>    
       </xsl:if>  
        <xsl:if test=""FTX05"">
         <NoteLanguage>  
          <xsl:value-of select=""FTX05/text()"" />      
         </NoteLanguage>    
       </xsl:if>         
            <Note>
            <xsl:value-of select=""s0:C108/C10805/text()"" />        
           </Note>  
          </Node>   
         </xsl:if>      
       
      </NoteLoop>     
      </xsl:for-each>          
      <!--  END OF  FTX -->
      
     <!-- <xsl:if test=""s0:FTX"">
        
          <xsl:for-each select=""s0:FTX""> 
            <Note>
             <xsl:value-of select=""s0:C108/C10801/text()"" />          
            </Note>  
          </xsl:for-each> -->
        
          <!--
          <xsl:value-of select=""s0:FTX/s0:C108/C10801/text()"" />
          <xsl:if test=""s0:FTX/s0:C108/C10802""> 
            <xsl:value-of select=""' '"" />
            <xsl:value-of select=""s0:FTX/s0:C108/C10802/text()"" />        
          </xsl:if>
          <xsl:if test=""s0:FTX/s0:C108/C10803""> 
            <xsl:value-of select=""' '"" />
            <xsl:value-of select=""s0:FTX/s0:C108/C10803/text()"" />        
          </xsl:if>
          <xsl:if test=""s0:FTX/s0:C108/C10804""> 
            <xsl:value-of select=""' '"" />
            <xsl:value-of select=""s0:FTX/s0:C108/C10804/text()"" />        
          </xsl:if>
          <xsl:if test=""s0:FTX/s0:C108/C10805""> 
            <xsl:value-of select=""' '"" />
            <xsl:value-of select=""s0:FTX/s0:C108/C10805/text()"" />        
          </xsl:if>
         -->         
    <!--  </xsl:if> -->
      
     <!-- *** RFF *** -->
      <OrderReference>
        <xsl:for-each select=""s0:RFFLoop1"">      
            <xsl:choose>
              <xsl:when test=""s0:RFF/s0:C506/C50601 = 'ON'"">
                <OrderID>
                  <xsl:value-of select=""s0:RFF/s0:C506/C50602/text()"" />
                </OrderID>  
                <xsl:if test=""string-length(s0:DTM_2/s0:C507_2/C50702) != 0"">                 
                  <OrderDate>
                    <xsl:value-of select=""userCSharp:editDate(s0:DTM_2/s0:C507_2/C50702/text())"" />
                  </OrderDate>
                    <xsl:if test=""string-length(s0:DTM_2/s0:C507_2/C50703) != 0"">                                   
                      <OrderDateFormat>
                        <xsl:value-of select=""s0:DTM_2/s0:C507_2/C50703/text()"" />
                      </OrderDateFormat>    
                   </xsl:if>  
                   <xsl:if test=""string-length(s0:DTM_2/s0:C507_2/C50703) = 0"">                                   
                      <OrderDateFormat>
                        <xsl:text>102</xsl:text>
                      </OrderDateFormat>    
                   </xsl:if> 
                </xsl:if>  
              </xsl:when>
              <xsl:when test=""s0:RFF/s0:C506/C50601 = 'VN'"">
                <SalesOrderID>
                  <xsl:value-of select=""s0:RFF/s0:C506/C50602/text()"" />
                </SalesOrderID>               
              <xsl:if test=""string-length(s0:DTM_2/s0:C507_2/C50702) != 0"">                 
                  <SalesOrderDate>
                    <xsl:value-of select=""userCSharp:editDate(s0:DTM_2/s0:C507_2/C50702/text())"" />
                  </SalesOrderDate>
                    <xsl:if test=""string-length(s0:DTM_2/s0:C507_2/C50703) != 0"">                                   
                      <SalesOrderDateFormat>
                        <xsl:value-of select=""s0:DTM_2/s0:C507_2/C50703/text()"" />
                      </SalesOrderDateFormat>    
                   </xsl:if>  
                   <xsl:if test=""string-length(s0:DTM_2/s0:C507_2/C50703) = 0"">                                   
                      <SalesOrderDateFormat>
                        <xsl:text>102</xsl:text>
                      </SalesOrderDateFormat>    
                   </xsl:if>              
                </xsl:if>  
              </xsl:when>
           </xsl:choose>
        </xsl:for-each>  
      </OrderReference> 
      <xsl:for-each select=""s0:RFFLoop1"">      
        <xsl:choose>            
          <xsl:when test=""s0:RFF/s0:C506/C50601 = 'AAU'"">
            <DespatchDocumentReference>
              <ID>
                <xsl:value-of select=""s0:RFF/s0:C506/C50602/text()"" />
              </ID>
                     <xsl:if test=""string-length(s0:DTM_2/s0:C507_2/C50703) != 0"">                                   
                      <DateFormat>
                        <xsl:value-of select=""s0:DTM_2/s0:C507_2/C50703/text()"" />
                      </DateFormat>    
                   </xsl:if>  
                   <xsl:if test=""string-length(s0:DTM_2/s0:C507_2/C50703) = 0"">                                   
                      <DateFormat>
                        <xsl:text>102</xsl:text>
                      </DateFormat>    
                   </xsl:if> 
            </DespatchDocumentReference>  
          </xsl:when>
          <xsl:when test=""s0:RFF/s0:C506/C50601 = 'IV'"">
            <BillingReference>
              <InvoiceDocumentReference>
              <ID>
                <xsl:value-of select=""s0:RFF/s0:C506/C50602/text()"" />
              </ID>
                     <xsl:if test=""string-length(s0:DTM_2/s0:C507_2/C50703) != 0"">                                   
                      <DateFormat>
                        <xsl:value-of select=""s0:DTM_2/s0:C507_2/C50703/text()"" />
                      </DateFormat>    
                   </xsl:if>  
                   <xsl:if test=""string-length(s0:DTM_2/s0:C507_2/C50703) = 0"">                                   
                      <DateFormat>
                        <xsl:text>102</xsl:text>
                      </DateFormat>    
                   </xsl:if>               
             </InvoiceDocumentReference>   
            </BillingReference>
          </xsl:when>
          <xsl:otherwise>
            <xsl:if test=""s0:RFF/s0:C506/C50601 != 'ON' and s0:RFF/s0:C506/C50601 != 'VN'"">
              <ExtraOrderReference>
                <Node>
                  <Code>
                    <xsl:value-of select=""s0:RFF/s0:C506/C50601/text()"" />
                  </Code>
                  <Reference>
                    <xsl:value-of select=""s0:RFF/s0:C506/C50602/text()"" />
                  </Reference>
                  <xsl:if test=""string-length(s0:DTM_2/s0:C507_2/C50702) != 0"">
                    <DateCode>
                      <xsl:value-of select=""s0:DTM_2/s0:C507_2/C50701/text()"" />
                    </DateCode>
                    <Date>
                      <xsl:value-of select=""userCSharp:editDate(s0:DTM_2/s0:C507_2/C50702/text())"" />
                    </Date>
                    <xsl:if test=""string-length(s0:DTM_2/s0:C507_2/C50703) != 0"">
                      <DateFormat>
                        <xsl:value-of select=""s0:DTM_2/s0:C507_2/C50703/text()"" />
                      </DateFormat>
                    </xsl:if>
                    <xsl:if test=""string-length(s0:DTM_2/s0:C507_2/C50703) = 0"">
                      <DateFormat>
                        <xsl:text>102</xsl:text>
                      </DateFormat>
                    </xsl:if>
                  </xsl:if>
                </Node>
              </ExtraOrderReference>
            </xsl:if>  
          </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each>  
     <!-- End of  RFF --> 
      
      <!-- *** NAD *** -->
      <xsl:for-each select=""s0:NADLoop1"">
        <!-- *** NAD SU *** -->
        <xsl:choose>
          <xsl:when test=""s0:NAD/NAD01 = 'SU'"">
            <AccountingSupplier>
              <AccSellerID>
                <xsl:value-of select=""s0:NAD/s0:C082/C08201/text()"" />
              </AccSellerID>
              <PartyID>
                <xsl:value-of select=""s0:NAD/s0:C082/C08201/text()"" />
              </PartyID>
              <CompanyID>
                <xsl:value-of select=""s0:RFFLoop2/s0:RFF_2/s0:C506_2/C50602/text()"" />
              </CompanyID>
              <SchemeID>
                <xsl:if test=""s0:NAD/s0:C082/C08203/text() = '9'"">
                  <Endpoint>
                    <xsl:value-of select=""'GLN'"" />
                  </Endpoint>     
                  <Party>
                    <xsl:value-of select=""'GLN'"" />
                  </Party> 
                </xsl:if>   
                <Company>
                  <xsl:if test=""string-length(s0:RFFLoop2/s0:RFF_2/s0:C506_2/C50602) != 0""> 
                    <xsl:value-of select=""'DK:CVR'"" />
                  </xsl:if>   
                </Company>              
              </SchemeID>
              
                
                 <xsl:if test=""string-length(s0:NAD/s0:C058/C05801) != 0"">
                  <Address>
                    <Name>
                      <xsl:value-of select=""s0:NAD/s0:C058/C05801/text()"" />                    
                    </Name>
                    <Street>
                      <xsl:value-of select=""s0:NAD/s0:C058/C05802/text()"" />                    
                    </Street>
                    <City>
                      <xsl:value-of select=""s0:NAD/s0:C058/C05803/text()"" />                    
                    </City>
                    <Country>
                      <xsl:value-of select=""s0:NAD/s0:C058/C05804/text()"" />                    
                    </Country>
                  </Address>
                  
                </xsl:if>  
              
              
             <xsl:if test=""string-length(s0:NAD/s0:C058/C05801) = 0"">
               <xsl:if test=""string-length(s0:NAD/s0:C080/C08001) != 0"">
                 
               <Address>       
                    <Name>
                      <xsl:value-of select=""s0:NAD/s0:C080/C08001/text()"" />                    
                    </Name>
     
                    <Street>
                      <xsl:value-of select=""s0:NAD/s0:C059/C05901/text()"" />                    
                    </Street>
                 
                    <City>
                      <xsl:value-of select=""s0:NAD/NAD06/text()"" />                    
                    </City>
                                                     
                    <PostalCode>
                     <xsl:if test=""s0:NAD/NAD07 and string(number(s0:NAD/NAD07)) != 'NaN'"">
                      <xsl:value-of select=""s0:NAD/NAD07/text()"" />                    
                     </xsl:if>   
                     <xsl:if test=""not(s0:NAD/NAD07) or string(number(s0:NAD/NAD07)) = 'NaN'"">                       
                      <xsl:if test=""s0:NAD/NAD08 and string(number(s0:NAD/NAD08)) != 'NaN'"">
                       <xsl:value-of select=""s0:NAD/NAD08/text()"" />                    
                      </xsl:if>
                     </xsl:if>                     
                    </PostalCode>
                 
                    <Country>
                      <xsl:value-of select=""s0:NAD/NAD09/text()"" />                    
                    </Country>
               
                 </Address>
              
              </xsl:if>              
             </xsl:if> 
              
          <xsl:if test=""string-length(s0:FII/FII01) != 0"">
              <FinancialInstitution>
                <CodeQualifier>
                  <xsl:value-of select=""s0:FII/FII01/text()"" />
                </CodeQualifier>
                <xsl:if test=""string-length(s0:FII/s0:C078/C07801) != 0"">
                  <AccountHolderIdentifier>
                    <xsl:value-of select=""s0:FII/s0:C078/C07801/text()"" />
                  </AccountHolderIdentifier>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII/s0:C078/C07802) != 0"">
                  <AccountHolderName>
                    <xsl:value-of select=""s0:FII/s0:C078/C07802/text()"" />
                  </AccountHolderName>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII/s0:C078/C07804) != 0"">
                  <CurrencyIDCode>
                    <xsl:value-of select=""s0:FII/s0:C078/C07804/text()"" />
                  </CurrencyIDCode>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII/s0:C088/C08801) != 0"">
                  <InstitutionNameCode>
                    <xsl:value-of select=""s0:FII/s0:C088/C08801/text()"" />
                  </InstitutionNameCode>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII/s0:C088/C08802) != 0"">
                  <CodeListIDCode>
                    <xsl:value-of select=""s0:FII/s0:C088/C08802/text()"" />
                  </CodeListIDCode>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII/s0:C088/C08803) != 0"">
                  <CodeListResponsibleAgencyCode>
                    <xsl:value-of select=""s0:FII/s0:C088/C08803/text()"" />
                  </CodeListResponsibleAgencyCode>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII/s0:C088/C08804) != 0"">
                  <InstitutionBranchIdentifier>
                    <xsl:value-of select=""s0:FII/s0:C088/C08804/text()"" />
                  </InstitutionBranchIdentifier>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII/s0:C088/C08807) != 0"">
                  <InstitutionName>
                    <xsl:value-of select=""s0:FII/s0:C088/C08807/text()"" />
                  </InstitutionName>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII/s0:C088/C08808) != 0"">
                  <InstitutionBranchLocationName>
                    <xsl:value-of select=""s0:FII/s0:C088/C08808/text()"" />
                  </InstitutionBranchLocationName>
                </xsl:if>
              </FinancialInstitution>
            </xsl:if>               
                              
              <xsl:for-each select=""s0:CTALoop1"">
                <Concact>
                  <xsl:if test=""string-length(s0:CTA/s0:C056/C05601) != 0 or string-length(s0:CTA/s0:C056/C05602) != 0""> 
                    <ID>
                      <xsl:value-of select=""s0:CTA/CTA01/text()"" />
                    </ID>
                    <xsl:if test=""string-length(s0:CTA/s0:C056/C05601) != 0"">
                      <Code>
                       <xsl:value-of select=""s0:CTA/s0:C056/C05601/text()"" />                       
                      </Code>                    
                    </xsl:if>
                    <xsl:if test=""string-length(s0:CTA/s0:C056/C05602) != 0"">
                    <Name>
                      <xsl:value-of select=""s0:CTA/s0:C056/C05602/text()"" />                    
                    </Name>
                   </xsl:if>  
                  </xsl:if>
                  <xsl:for-each select=""s0:COM/s0:C076"">      
                    <xsl:choose>
                      <xsl:when test=""C07602 = 'TE'"">
                        <Telephone>
                          <xsl:value-of select=""C07601/text()"" />  
                        </Telephone>
                      </xsl:when>
                      <xsl:when test=""C07602 = 'FX'"">
                        <Telefax>
                          <xsl:value-of select=""C07601/text()"" />  
                        </Telefax>
                         </xsl:when>
                      <xsl:when test=""C07602 = 'EM'"">
                        <E-mail>
                          <xsl:value-of select=""C07601/text()"" />                         
                        </E-mail>
                      </xsl:when>
                    </xsl:choose>
                  </xsl:for-each>  
                </Concact>
              </xsl:for-each>
            
              <xsl:if test=""string-length(s0:RFFLoop2/s0:RFF_2/s0:C506_2/C50602) != 0"">
               <RFFNADLoop>
                <xsl:for-each select=""s0:RFFLoop2"">
                 <Node>
                  <Code>
                   <xsl:value-of select=""s0:RFF_2/s0:C506_2/C50601/text()"" />
                  </Code>
                  <Reference>
                   <xsl:value-of select=""s0:RFF_2/s0:C506_2/C50602/text()"" />
                  </Reference>
                 </Node>
                </xsl:for-each>
               </RFFNADLoop>
              </xsl:if>   
        
            </AccountingSupplier>
          </xsl:when>  
        </xsl:choose>    
        <!-- *** NAD IV *** -->
        <xsl:choose>
          <xsl:when test=""s0:NAD/NAD01 = 'IV'"">
            <AccountingCustomer>
              <AccBuyerID>
                <xsl:value-of select=""s0:NAD/s0:C082/C08201/text()"" />
              </AccBuyerID>
              <PartyID>
                <xsl:value-of select=""s0:NAD/s0:C082/C08201/text()"" />
              </PartyID>
              <CompanyID>
                <xsl:value-of select=""s0:RFFLoop2/s0:RFF_2/s0:C506_2/C50602/text()"" />
              </CompanyID>
              <SchemeID>
                <xsl:if test=""s0:NAD/s0:C082/C08203/text() = '9'"">
                  <Endpoint>
                    <xsl:value-of select=""'GLN'"" />
                  </Endpoint>     
                  <Party>
                    <xsl:value-of select=""'GLN'"" />
                  </Party> 
                </xsl:if>   
                <Company>
                  <xsl:if test=""string-length(s0:RFFLoop2/s0:RFF_2/s0:C506_2/C50602) != 0""> 
                    <xsl:value-of select=""'DK:CVR'"" />
                  </xsl:if>   
                </Company>
              </SchemeID>              
              
                 <xsl:if test=""string-length(s0:NAD/s0:C058/C05801) != 0"">
                  <Address>
                    <Name>
                      <xsl:value-of select=""s0:NAD/s0:C058/C05801/text()"" />                    
                    </Name>
                    <Street>
                      <xsl:value-of select=""s0:NAD/s0:C058/C05802/text()"" />                    
                    </Street>
                    <City>
                      <xsl:value-of select=""s0:NAD/s0:C058/C05803/text()"" />                    
                    </City>
                    <Country>
                      <xsl:value-of select=""s0:NAD/s0:C058/C05804/text()"" />                    
                    </Country>
                  </Address>
                  
                </xsl:if>  
              
              
             <xsl:if test=""string-length(s0:NAD/s0:C058/C05801) = 0"">
               <xsl:if test=""string-length(s0:NAD/s0:C080/C08001) != 0"">
                 
               <Address>       
                    <Name>
                      <xsl:value-of select=""s0:NAD/s0:C080/C08001/text()"" />                    
                    </Name>
     
                    <Street>
                      <xsl:value-of select=""s0:NAD/s0:C059/C05901/text()"" />                    
                    </Street>
                 
                    <City>
                      <xsl:value-of select=""s0:NAD/NAD06/text()"" />                    
                    </City>
                 
                    <PostalCode>
                     <xsl:if test=""s0:NAD/NAD07 and string(number(s0:NAD/NAD07)) != 'NaN'"">
                      <xsl:value-of select=""s0:NAD/NAD07/text()"" />                    
                     </xsl:if>   
                     <xsl:if test=""not(s0:NAD/NAD07) or string(number(s0:NAD/NAD07)) = 'NaN'"">                       
                      <xsl:if test=""s0:NAD/NAD08 and string(number(s0:NAD/NAD08)) != 'NaN'"">
                       <xsl:value-of select=""s0:NAD/NAD08/text()"" />                    
                      </xsl:if>
                     </xsl:if>                     
                    </PostalCode>
                 
                    <Country>
                      <xsl:value-of select=""s0:NAD/NAD09/text()"" />                    
                    </Country>
               
                 </Address>
              
              </xsl:if>              
             </xsl:if> 

          <xsl:if test=""string-length(s0:FII/FII01) != 0"">
              <FinancialInstitution>
                <CodeQualifier>
                  <xsl:value-of select=""s0:FII/FII01/text()"" />
                </CodeQualifier>
                <xsl:if test=""string-length(s0:FII/s0:C078/C07801) != 0"">
                  <AccountHolderIdentifier>
                    <xsl:value-of select=""s0:FII/s0:C078/C07801/text()"" />
                  </AccountHolderIdentifier>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII/s0:C078/C07802) != 0"">
                  <AccountHolderName>
                    <xsl:value-of select=""s0:FII/s0:C078/C07802/text()"" />
                  </AccountHolderName>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII/s0:C078/C07804) != 0"">
                  <CurrencyIDCode>
                    <xsl:value-of select=""s0:FII/s0:C078/C07804/text()"" />
                  </CurrencyIDCode>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII/s0:C088/C08801) != 0"">
                  <InstitutionNameCode>
                    <xsl:value-of select=""s0:FII/s0:C088/C08801/text()"" />
                  </InstitutionNameCode>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII/s0:C088/C08802) != 0"">
                  <CodeListIDCode>
                    <xsl:value-of select=""s0:FII/s0:C088/C08802/text()"" />
                  </CodeListIDCode>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII/s0:C088/C08803) != 0"">
                  <CodeListResponsibleAgencyCode>
                    <xsl:value-of select=""s0:FII/s0:C088/C08803/text()"" />
                  </CodeListResponsibleAgencyCode>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII/s0:C088/C08804) != 0"">
                  <InstitutionBranchIdentifier>
                    <xsl:value-of select=""s0:FII/s0:C088/C08804/text()"" />
                  </InstitutionBranchIdentifier>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII/s0:C088/C08807) != 0"">
                  <InstitutionName>
                    <xsl:value-of select=""s0:FII/s0:C088/C08807/text()"" />
                  </InstitutionName>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII/s0:C088/C08808) != 0"">
                  <InstitutionBranchLocationName>
                    <xsl:value-of select=""s0:FII/s0:C088/C08808/text()"" />
                  </InstitutionBranchLocationName>
                </xsl:if>
              </FinancialInstitution>
            </xsl:if>               
                              
              <xsl:for-each select=""s0:CTALoop1"">
                <Concact>
                  <xsl:if test=""string-length(s0:CTA/s0:C056/C05601) != 0 or string-length(s0:CTA/s0:C056/C05602) != 0""> 
                    <ID>
                      <xsl:value-of select=""s0:CTA/CTA01/text()"" />
                    </ID>
                    <xsl:if test=""string-length(s0:CTA/s0:C056/C05601) != 0"">
                      <Code>
                       <xsl:value-of select=""s0:CTA/s0:C056/C05601/text()"" />                       
                      </Code>                    
                    </xsl:if>
                   <xsl:if test=""string-length(s0:CTA/s0:C056/C05602) != 0"">
                    <Name>
                     <xsl:value-of select=""s0:CTA/s0:C056/C05602/text()"" />                    
                    </Name>
                   </xsl:if>  
                  </xsl:if>
                  <xsl:for-each select=""s0:COM/s0:C076"">      
                    <xsl:choose>
                      <xsl:when test=""C07602 = 'TE'"">
                        <Telephone>
                          <xsl:value-of select=""C07601/text()"" />  
                        </Telephone>
                      </xsl:when>
                      <xsl:when test=""C07602 = 'FX'"">
                        <Telefax>
                          <xsl:value-of select=""C07601/text()"" />  
                        </Telefax>
                         </xsl:when>
                      <xsl:when test=""C07602 = 'EM'"">
                        <E-mail>
                          <xsl:value-of select=""C07601/text()"" />                         
                        </E-mail>
                      </xsl:when>
                    </xsl:choose>
                  </xsl:for-each>  
                </Concact>
              </xsl:for-each>  
            
              <xsl:if test=""string-length(s0:RFFLoop2/s0:RFF_2/s0:C506_2/C50602) != 0"">
               <RFFNADLoop>
                <xsl:for-each select=""s0:RFFLoop2"">
                 <Node>
                  <Code>
                   <xsl:value-of select=""s0:RFF_2/s0:C506_2/C50601/text()"" />
                  </Code>
                  <Reference>
                   <xsl:value-of select=""s0:RFF_2/s0:C506_2/C50602/text()"" />
                  </Reference>
                 </Node>
                </xsl:for-each>
               </RFFNADLoop>
              </xsl:if>   
        
            </AccountingCustomer>
          </xsl:when>
        </xsl:choose>  
        <!-- *** NAD BY *** -->
        <xsl:choose>
          <xsl:when test=""s0:NAD/NAD01 = 'BY'"">            
            <BuyerCustomer>
              <BuyerID>
                <xsl:value-of select=""s0:NAD/s0:C082/C08201/text()"" />
              </BuyerID>
              <PartyID>
                <xsl:value-of select=""s0:NAD/s0:C082/C08201/text()"" />
              </PartyID>
              <CompanyID>
                <xsl:value-of select=""s0:RFFLoop2/s0:RFF_2/s0:C506_2/C50602/text()"" />
              </CompanyID>
              <SchemeID>
                <xsl:if test=""s0:NAD/s0:C082/C08203/text() = '9'"">
                  <Endpoint>
                    <xsl:value-of select=""'GLN'"" />
                  </Endpoint>     
                  <Party>
                    <xsl:value-of select=""'GLN'"" />
                  </Party> 
                </xsl:if>   
                <Company>
                  <xsl:if test=""string-length(s0:RFFLoop2/s0:RFF_2/s0:C506_2/C50602) != 0""> 
                    <xsl:value-of select=""'DK:CVR'"" />
                  </xsl:if>   
                </Company>
              </SchemeID>
              
              
                 <xsl:if test=""string-length(s0:NAD/s0:C058/C05801) != 0"">
                  <Address>
                    <Name>
                      <xsl:value-of select=""s0:NAD/s0:C058/C05801/text()"" />                    
                    </Name>
                    <Street>
                      <xsl:value-of select=""s0:NAD/s0:C058/C05802/text()"" />                    
                    </Street>
                    <City>
                      <xsl:value-of select=""s0:NAD/s0:C058/C05803/text()"" />                    
                    </City>
                    <Country>
                      <xsl:value-of select=""s0:NAD/s0:C058/C05804/text()"" />                    
                    </Country>
                  </Address>
                  
                </xsl:if>  
              
              
             <xsl:if test=""string-length(s0:NAD/s0:C058/C05801) = 0"">
               <xsl:if test=""string-length(s0:NAD/s0:C080/C08001) != 0"">
                 
               <Address>       
                    <Name>
                      <xsl:value-of select=""s0:NAD/s0:C080/C08001/text()"" />                    
                    </Name>
     
                    <Street>
                      <xsl:value-of select=""s0:NAD/s0:C059/C05901/text()"" />                    
                    </Street>
                 
                    <City>
                      <xsl:value-of select=""s0:NAD/NAD06/text()"" />                    
                    </City>
                 
                    <PostalCode>
                     <xsl:if test=""s0:NAD/NAD07 and string(number(s0:NAD/NAD07)) != 'NaN'"">
                      <xsl:value-of select=""s0:NAD/NAD07/text()"" />                    
                     </xsl:if>   
                     <xsl:if test=""not(s0:NAD/NAD07) or string(number(s0:NAD/NAD07)) = 'NaN'"">                       
                      <xsl:if test=""s0:NAD/NAD08 and string(number(s0:NAD/NAD08)) != 'NaN'"">
                       <xsl:value-of select=""s0:NAD/NAD08/text()"" />                    
                      </xsl:if>
                     </xsl:if>                     
                    </PostalCode>
                 
                    <Country>
                      <xsl:value-of select=""s0:NAD/NAD09/text()"" />                    
                    </Country>
               
                 </Address>
              
              </xsl:if>              
             </xsl:if>   
                                       
          <xsl:if test=""string-length(s0:FII/FII01) != 0"">
              <FinancialInstitution>
                <CodeQualifier>
                  <xsl:value-of select=""s0:FII/FII01/text()"" />
                </CodeQualifier>
                <xsl:if test=""string-length(s0:FII/s0:C078/C07801) != 0"">
                  <AccountHolderIdentifier>
                    <xsl:value-of select=""s0:FII/s0:C078/C07801/text()"" />
                  </AccountHolderIdentifier>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII/s0:C078/C07802) != 0"">
                  <AccountHolderName>
                    <xsl:value-of select=""s0:FII/s0:C078/C07802/text()"" />
                  </AccountHolderName>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII/s0:C078/C07804) != 0"">
                  <CurrencyIDCode>
                    <xsl:value-of select=""s0:FII/s0:C078/C07804/text()"" />
                  </CurrencyIDCode>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII/s0:C088/C08801) != 0"">
                  <InstitutionNameCode>
                    <xsl:value-of select=""s0:FII/s0:C088/C08801/text()"" />
                  </InstitutionNameCode>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII/s0:C088/C08802) != 0"">
                  <CodeListIDCode>
                    <xsl:value-of select=""s0:FII/s0:C088/C08802/text()"" />
                  </CodeListIDCode>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII/s0:C088/C08803) != 0"">
                  <CodeListResponsibleAgencyCode>
                    <xsl:value-of select=""s0:FII/s0:C088/C08803/text()"" />
                  </CodeListResponsibleAgencyCode>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII/s0:C088/C08804) != 0"">
                  <InstitutionBranchIdentifier>
                    <xsl:value-of select=""s0:FII/s0:C088/C08804/text()"" />
                  </InstitutionBranchIdentifier>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII/s0:C088/C08807) != 0"">
                  <InstitutionName>
                    <xsl:value-of select=""s0:FII/s0:C088/C08807/text()"" />
                  </InstitutionName>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII/s0:C088/C08808) != 0"">
                  <InstitutionBranchLocationName>
                    <xsl:value-of select=""s0:FII/s0:C088/C08808/text()"" />
                  </InstitutionBranchLocationName>
                </xsl:if>
              </FinancialInstitution>
            </xsl:if> 
  
              <xsl:for-each select=""s0:CTALoop1"">
                <Concact>
                  <xsl:if test=""string-length(s0:CTA/s0:C056/C05601) != 0 or string-length(s0:CTA/s0:C056/C05602) != 0""> 
                    <ID>
                      <xsl:value-of select=""s0:CTA/CTA01/text()"" />
                    </ID>
                    <xsl:if test=""string-length(s0:CTA/s0:C056/C05601) != 0"">
                      <Code>
                       <xsl:value-of select=""s0:CTA/s0:C056/C05601/text()"" />                       
                      </Code>                    
                    </xsl:if>
                    <xsl:if test=""string-length(s0:CTA/s0:C056/C05602) != 0"">
                    <Name>
                      <xsl:value-of select=""s0:CTA/s0:C056/C05602/text()"" />                    
                    </Name>
                   </xsl:if>  
                  </xsl:if>
                  <xsl:for-each select=""s0:COM/s0:C076"">      
                    <xsl:choose>
                      <xsl:when test=""C07602 = 'TE'"">
                        <Telephone>
                          <xsl:value-of select=""C07601/text()"" />  
                        </Telephone>
                      </xsl:when>
                      <xsl:when test=""C07602 = 'FX'"">
                        <Telefax>
                          <xsl:value-of select=""C07601/text()"" />  
                        </Telefax>
                         </xsl:when>
                      <xsl:when test=""C07602 = 'EM'"">
                        <E-mail>
                          <xsl:value-of select=""C07601/text()"" />                         
                        </E-mail>
                      </xsl:when>
                    </xsl:choose>
                  </xsl:for-each>  
                </Concact>
              </xsl:for-each>  
            
              <xsl:if test=""string-length(s0:RFFLoop2/s0:RFF_2/s0:C506_2/C50602) != 0"">
               <RFFNADLoop>
                <xsl:for-each select=""s0:RFFLoop2"">
                 <Node>
                  <Code>
                   <xsl:value-of select=""s0:RFF_2/s0:C506_2/C50601/text()"" />
                  </Code>
                  <Reference>
                   <xsl:value-of select=""s0:RFF_2/s0:C506_2/C50602/text()"" />
                  </Reference>
                 </Node>
                </xsl:for-each>
               </RFFNADLoop>
              </xsl:if>   
        
            </BuyerCustomer>
          </xsl:when>
        </xsl:choose>    
        <!-- *** NAD DP *** -->
        <xsl:choose>
          <xsl:when test=""s0:NAD/NAD01 = 'DP'"">
            <DeliveryLocation>
              <DeliveryID>
                <xsl:value-of select=""s0:NAD/s0:C082/C08201/text()"" />
              </DeliveryID>
              <PartyID>
                <xsl:value-of select=""s0:NAD/s0:C082/C08201/text()"" />
              </PartyID>
              <CompanyID>
                <xsl:value-of select=""s0:RFFLoop2/s0:RFF_2/s0:C506_2/C50602/text()"" />
              </CompanyID>
              <SchemeID>
                <xsl:if test=""s0:NAD/s0:C082/C08203/text() = '9'"">
                  <Endpoint>
                    <xsl:value-of select=""'GLN'"" />
                  </Endpoint>     
                  <Party>
                    <xsl:value-of select=""'GLN'"" />
                  </Party> 
                </xsl:if>   
                <Company>
                  <xsl:if test=""string-length(s0:RFFLoop2/s0:RFF_2/s0:C506_2/C50602) != 0""> 
                    <xsl:value-of select=""'DK:CVR'"" />
                  </xsl:if>   
                </Company>
              </SchemeID>
              
                 
                <xsl:if test=""string-length(s0:NAD/s0:C058/C05801) != 0"">
                  <Address>
                    <Name>
                      <xsl:value-of select=""s0:NAD/s0:C058/C05801/text()"" />                    
                    </Name>
                    <Street>
                      <xsl:value-of select=""s0:NAD/s0:C058/C05802/text()"" />                    
                    </Street>
                    <City>
                      <xsl:value-of select=""s0:NAD/s0:C058/C05803/text()"" />                    
                    </City>
                    <Country>
                      <xsl:value-of select=""s0:NAD/s0:C058/C05804/text()"" />                    
                    </Country>
                  </Address>
                  
                </xsl:if>  
              
              
             <xsl:if test=""string-length(s0:NAD/s0:C058/C05801) = 0"">
               <xsl:if test=""string-length(s0:NAD/s0:C080/C08001) != 0"">
                 
               <Address>       
                    <Name>
                      <xsl:value-of select=""s0:NAD/s0:C080/C08001/text()"" />                    
                    </Name>
     
                    <Street>
                      <xsl:value-of select=""s0:NAD/s0:C059/C05901/text()"" />                    
                    </Street>
                 
                    <City>
                      <xsl:value-of select=""s0:NAD/NAD06/text()"" />                    
                    </City>
                 
                    <PostalCode>
                     <xsl:if test=""s0:NAD/NAD07 and string(number(s0:NAD/NAD07)) != 'NaN'"">
                      <xsl:value-of select=""s0:NAD/NAD07/text()"" />                    
                     </xsl:if>   
                     <xsl:if test=""not(s0:NAD/NAD07) or string(number(s0:NAD/NAD07)) = 'NaN'"">                       
                      <xsl:if test=""s0:NAD/NAD08 and string(number(s0:NAD/NAD08)) != 'NaN'"">
                       <xsl:value-of select=""s0:NAD/NAD08/text()"" />                    
                      </xsl:if>
                     </xsl:if>                     
                    </PostalCode>
                 
                    <Country>
                      <xsl:value-of select=""s0:NAD/NAD09/text()"" />                    
                    </Country>
               
                 </Address>
              
              </xsl:if>              
             </xsl:if> 
                      
          <xsl:if test=""string-length(s0:FII/FII01) != 0"">
              <FinancialInstitution>
                <CodeQualifier>
                  <xsl:value-of select=""s0:FII/FII01/text()"" />
                </CodeQualifier>
                <xsl:if test=""string-length(s0:FII/s0:C078/C07801) != 0"">
                  <AccountHolderIdentifier>
                    <xsl:value-of select=""s0:FII/s0:C078/C07801/text()"" />
                  </AccountHolderIdentifier>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII/s0:C078/C07802) != 0"">
                  <AccountHolderName>
                    <xsl:value-of select=""s0:FII/s0:C078/C07802/text()"" />
                  </AccountHolderName>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII/s0:C078/C07804) != 0"">
                  <CurrencyIDCode>
                    <xsl:value-of select=""s0:FII/s0:C078/C07804/text()"" />
                  </CurrencyIDCode>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII/s0:C088/C08801) != 0"">
                  <InstitutionNameCode>
                    <xsl:value-of select=""s0:FII/s0:C088/C08801/text()"" />
                  </InstitutionNameCode>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII/s0:C088/C08802) != 0"">
                  <CodeListIDCode>
                    <xsl:value-of select=""s0:FII/s0:C088/C08802/text()"" />
                  </CodeListIDCode>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII/s0:C088/C08803) != 0"">
                  <CodeListResponsibleAgencyCode>
                    <xsl:value-of select=""s0:FII/s0:C088/C08803/text()"" />
                  </CodeListResponsibleAgencyCode>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII/s0:C088/C08804) != 0"">
                  <InstitutionBranchIdentifier>
                    <xsl:value-of select=""s0:FII/s0:C088/C08804/text()"" />
                  </InstitutionBranchIdentifier>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII/s0:C088/C08807) != 0"">
                  <InstitutionName>
                    <xsl:value-of select=""s0:FII/s0:C088/C08807/text()"" />
                  </InstitutionName>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII/s0:C088/C08808) != 0"">
                  <InstitutionBranchLocationName>
                    <xsl:value-of select=""s0:FII/s0:C088/C08808/text()"" />
                  </InstitutionBranchLocationName>
                </xsl:if>
              </FinancialInstitution>
            </xsl:if> 
  
              <xsl:for-each select=""s0:CTALoop1"">
                <Concact>
                  <xsl:if test=""string-length(s0:CTA/s0:C056/C05601) != 0 or string-length(s0:CTA/s0:C056/C05602) != 0""> 
                    <ID>
                      <xsl:value-of select=""s0:CTA/CTA01/text()"" />
                    </ID>
                    <xsl:if test=""string-length(s0:CTA/s0:C056/C05601) != 0"">
                      <Code>
                       <xsl:value-of select=""s0:CTA/s0:C056/C05601/text()"" />                       
                      </Code>                    
                    </xsl:if>
                    <xsl:if test=""string-length(s0:CTA/s0:C056/C05602) != 0"">
                    <Name>
                      <xsl:value-of select=""s0:CTA/s0:C056/C05602/text()"" />                    
                    </Name>
                   </xsl:if>  
                  </xsl:if>
                  <xsl:for-each select=""s0:COM/s0:C076"">      
                    <xsl:choose>
                      <xsl:when test=""C07602 = 'TE'"">
                        <Telephone>
                          <xsl:value-of select=""C07601/text()"" />  
                        </Telephone>
                      </xsl:when>
                      <xsl:when test=""C07602 = 'FX'"">
                        <Telefax>
                          <xsl:value-of select=""C07601/text()"" />  
                        </Telefax>
                         </xsl:when>
                      <xsl:when test=""C07602 = 'EM'"">
                        <E-mail>
                          <xsl:value-of select=""C07601/text()"" />                         
                        </E-mail>
                      </xsl:when>
                    </xsl:choose>
                  </xsl:for-each>  
                </Concact>
              </xsl:for-each>
            
              <xsl:if test=""string-length(s0:RFFLoop2/s0:RFF_2/s0:C506_2/C50602) != 0"">
               <RFFNADLoop>
                <xsl:for-each select=""s0:RFFLoop2"">
                 <Node>
                  <Code>
                   <xsl:value-of select=""s0:RFF_2/s0:C506_2/C50601/text()"" />
                  </Code>
                  <Reference>
                   <xsl:value-of select=""s0:RFF_2/s0:C506_2/C50602/text()"" />
                  </Reference>
                 </Node>
                </xsl:for-each>
               </RFFNADLoop>
              </xsl:if> 
           
            </DeliveryLocation>
          </xsl:when>
        </xsl:choose>    
      </xsl:for-each>      
      <!-- *** DTM *** -->
      <xsl:for-each select=""s0:DTM/s0:C507"">
        <xsl:choose>
          <xsl:when test=""C50701 = '35'"">
            <DeliveryInfo>
              <ActualDeliveryDate>
                <xsl:value-of select=""userCSharp:editDate(C50702/text())"" />
              </ActualDeliveryDate>
            </DeliveryInfo>
          </xsl:when>
        </xsl:choose>
      </xsl:for-each>
      <!--  End of DTM  --> 
     <!-- End of  NAD+DP  -->
    <!--  End of  NAD --> 

      <!-- *** CUX *** -->
      <xsl:if test=""s0:CUXLoop1/s0:CUX/s0:C504/C50402"">
        <Currency>
          <xsl:value-of select=""s0:CUXLoop1/s0:CUX/s0:C504/C50402/text()"" />
        </Currency>
      </xsl:if>
      <!-- End of CUX  -->

      <!-- *** PAT *** -->
      <xsl:for-each select=""s0:PATLoop1"">
        <xsl:if test=""string-length(s0:PAT/PAT01) != 0"">
          <PaymentTerms>
            <ID>
              <xsl:value-of select=""s0:PAT/PAT01/text()"" />
            </ID>
            <xsl:if test=""string-length(s0:PAT/s0:C110/C11001) != 0"">
              <TermsOfPaymentIdentification>
                <xsl:value-of select=""s0:PAT/s0:C110/C11001/text()"" />
              </TermsOfPaymentIdentification>
            </xsl:if>
            <xsl:if test=""string-length(s0:PAT/s0:C110/C11002) != 0"">
              <CodeListQualifier>
                <xsl:value-of select=""s0:PAT/s0:C110/C11002/text()"" />
              </CodeListQualifier>
            </xsl:if>
            <xsl:if test=""string-length(s0:PAT/s0:C110/C11003) != 0"">
              <CodeListResponsibleAgency>
                <xsl:value-of select=""s0:PAT/s0:C110/C11003/text()"" />
              </CodeListResponsibleAgency>
            </xsl:if>
            <xsl:if test=""string-length(s0:PAT/s0:C110/C11004) != 0"">
              <Note>
                <xsl:value-of select=""s0:PAT/s0:C110/C11004/text()"" />
              </Note>
            </xsl:if>
            <xsl:if test=""string-length(s0:PAT/s0:C110/C11005) != 0"">
              <Note>
                <xsl:value-of select=""s0:PAT/s0:C110/C11005/text()"" />
              </Note>
            </xsl:if>
            <xsl:if test=""string-length(s0:PAT/s0:C112/C11201) != 0"">
              <PaymentTimeReference>
                <xsl:value-of select=""s0:PAT/s0:C112/C11201/text()"" />
              </PaymentTimeReference>
            </xsl:if>
            <xsl:if test=""string-length(s0:PAT/s0:C112/C11202) != 0"">
              <TimeRelation>
                <xsl:value-of select=""s0:PAT/s0:C112/C11202/text()"" />
              </TimeRelation>
            </xsl:if>
            <xsl:if test=""string-length(s0:PAT/s0:C112/C11203) != 0"">
              <TypeOfPeriod>
                <xsl:value-of select=""s0:PAT/s0:C112/C11203/text()"" />
              </TypeOfPeriod>
            </xsl:if>
            <xsl:if test=""string-length(s0:PAT/s0:C112/C11204) != 0"">
              <NumberOfPeriods>
                <xsl:value-of select=""s0:PAT/s0:C112/C11204/text()"" />
              </NumberOfPeriods>
            </xsl:if>

            <xsl:choose>
              <xsl:when test=""s0:DTM_6/s0:C507_6/C50701 = '12'"">
                <TermsDiscountDueDate>
                  <Code>
                    <xsl:value-of select=""s0:DTM_6/s0:C507_6/C50701/text()"" />
                  </Code>
                  <Date>
                    <xsl:value-of select=""userCSharp:editDate(s0:DTM_6/s0:C507_6/C50702/text())"" />
                  </Date>
                  <DateFormatCode>
                    <xsl:value-of select=""s0:DTM_6/s0:C507_6/C50703/text()"" />
                  </DateFormatCode>
                </TermsDiscountDueDate>
              </xsl:when>
              <xsl:when test=""s0:DTM_6/s0:C507_6/C50701 = '13'"">
                <TermsNetDueDate>
                  <Code>
                    <xsl:value-of select=""s0:DTM_6/s0:C507_6/C50701/text()"" />
                  </Code>
                   <Date>
                    <xsl:value-of select=""userCSharp:editDate(s0:DTM_6/s0:C507_6/C50702/text())"" />
                  </Date> 
                  <DateFormatCode>
                    <xsl:value-of select=""s0:DTM_6/s0:C507_6/C50703/text()"" />
                  </DateFormatCode>
                </TermsNetDueDate>
              </xsl:when>
            </xsl:choose>  
            
            <xsl:if test=""string-length(s0:PCD/s0:C501/C50101) != 0"">
              <Percentage>
                <PercentageCode>
                  <xsl:value-of select=""s0:PCD/s0:C501/C50101/text()"" />
                </PercentageCode>
                <xsl:if test=""string-length(s0:PCD/s0:C501/C50102) != 0"">
                  <PercentageValue>
                    <xsl:value-of select=""s0:PCD/s0:C501/C50102/text()"" />
                  </PercentageValue>
                </xsl:if>
                <xsl:if test=""string-length(s0:PCD/s0:C501/C50103) != 0"">
                  <PercentageIDCode>
                    <xsl:value-of select=""s0:PCD/s0:C501/C50103/text()"" />
                  </PercentageIDCode>
                </xsl:if>
                <xsl:if test=""string-length(s0:PCD/s0:C501/C50104) != 0"">
                  <CodeListIDCode>
                    <xsl:value-of select=""s0:PCD/s0:C501/C50104/text()"" />
                  </CodeListIDCode>
                </xsl:if>
                <xsl:if test=""string-length(s0:PCD/s0:C501/C50105) != 0"">
                  <CodeListAgencyCode>
                    <xsl:value-of select=""s0:PCD/s0:C501/C50105/text()"" />
                  </CodeListAgencyCode>
                </xsl:if>
              </Percentage>
            </xsl:if>
            
          <xsl:if test=""string-length(s0:MOA_2/s0:C516_2/C51602) != 0"">
              <MonetaryAmount>
                <Code>
                  <xsl:value-of select=""s0:MOA_2/s0:C516_2/C51601/text()"" />
                </Code>
                <Amount>
                  <xsl:value-of select=""s0:MOA_2/s0:C516_2/C51602/text()"" />
                </Amount>
                <xsl:if test=""string-length(s0:MOA_2/s0:C516_2/C51603) != 0"">
                  <Currency>
                    <xsl:value-of select=""s0:MOA_2/s0:C516_2/C51603/text()"" />
                  </Currency>
                </xsl:if>
              </MonetaryAmount>
            </xsl:if>
            
          <xsl:if test=""string-length(s0:PAI_2/s0:C534_2/C53401) != 0 or string-length(s0:PAI_2/s0:C534_2/C53402) != 0 or string-length(s0:PAI_2/s0:C534_2/C53403) != 0 or string-length(s0:PAI_2/s0:C534_2/C53404) != 0 or string-length(s0:PAI_2/s0:C534_2/C53405) != 0"">
              <PaymentInstructions>
                <xsl:if test=""string-length(s0:PAI_2/s0:C534_2/C53401) != 0"">
                  <PaymentConditionsCode>
                    <xsl:value-of select=""s0:PAI_2/s0:C534_2/C53401/text()"" />
                  </PaymentConditionsCode>
                </xsl:if>
                <xsl:if test=""string-length(s0:PAI_2/s0:C534_2/C53402) != 0"">
                  <PaymentGuaranteeMeansCode>
                    <xsl:value-of select=""s0:PAI_2/s0:C534_2/C53402/text()"" />
                  </PaymentGuaranteeMeansCode>
                </xsl:if>
                <xsl:if test=""string-length(s0:PAI_2/s0:C534_2/C53403) != 0"">
                  <PaymentMeansCode>
                    <xsl:value-of select=""s0:PAI_2/s0:C534_2/C53403/text()"" />
                  </PaymentMeansCode>
                </xsl:if>
                <xsl:if test=""string-length(s0:PAI_2/s0:C534_2/C53404) != 0"">
                  <CodeListIDCode>
                    <xsl:value-of select=""s0:PAI_2/s0:C534_2/C53404/text()"" />
                  </CodeListIDCode>
                </xsl:if>
                <xsl:if test=""string-length(s0:PAI_2/s0:C534_2/C53405) != 0"">
                  <PaymentChannelCode>
                    <xsl:value-of select=""s0:PAI_2/s0:C534_2/C53405/text()"" />
                  </PaymentChannelCode>
                </xsl:if>
              </PaymentInstructions>
            </xsl:if>
            
          <xsl:if test=""string-length(s0:FII_2/FII01) != 0"">
              <FinancialInstitution>
                <CodeQualifier>
                  <xsl:value-of select=""s0:FII_2/FII01/text()"" />
                </CodeQualifier>
                <xsl:if test=""string-length(s0:FII_2/s0:C078_2/C07801) != 0"">
                  <AccountHolderIdentifier>
                    <xsl:value-of select=""s0:FII_2/s0:C078_2/C07801/text()"" />
                  </AccountHolderIdentifier>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII_2/s0:C078_2/C07802) != 0"">
                  <AccountHolderName>
                    <xsl:value-of select=""s0:FII_2/s0:C078_2/C07802/text()"" />
                  </AccountHolderName>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII_2/s0:C078_2/C07804) != 0"">
                  <CurrencyIDCode>
                    <xsl:value-of select=""s0:FII_2/s0:C078_2/C07804/text()"" />
                  </CurrencyIDCode>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII_2/s0:C088_2/C08801) != 0"">
                  <InstitutionNameCode>
                    <xsl:value-of select=""s0:FII_2/s0:C088_2/C08801/text()"" />
                  </InstitutionNameCode>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII_2/s0:C088_2/C08802) != 0"">
                  <CodeListIDCode>
                    <xsl:value-of select=""s0:FII_2/s0:C088_2/C08802/text()"" />
                  </CodeListIDCode>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII_2/s0:C088_2/C08803) != 0"">
                  <CodeListResponsibleAgencyCode>
                    <xsl:value-of select=""s0:FII_2/s0:C088_2/C08803/text()"" />
                  </CodeListResponsibleAgencyCode>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII_2/s0:C088_2/C08804) != 0"">
                  <InstitutionBranchIdentifier>
                    <xsl:value-of select=""s0:FII_2/s0:C088_2/C08804/text()"" />
                  </InstitutionBranchIdentifier>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII_2/s0:C088_2/C08807) != 0"">
                  <InstitutionName>
                    <xsl:value-of select=""s0:FII_2/s0:C088_2/C08807/text()"" />
                  </InstitutionName>
                </xsl:if>
                <xsl:if test=""string-length(s0:FII_2/s0:C088_2/C08808) != 0"">
                  <InstitutionBranchLocationName>
                    <xsl:value-of select=""s0:FII_2/s0:C088_2/C08808/text()"" />
                  </InstitutionBranchLocationName>
                </xsl:if>
              </FinancialInstitution>
            </xsl:if>  
           
          </PaymentTerms>
        </xsl:if>
      </xsl:for-each>
      <!--  End of  PAT --> 

      <!-- *** ALC *** -->
      <xsl:for-each select=""s0:ALCLoop1"">
        <AllowanceCharge>
          <xsl:choose>
            <xsl:when test=""s0:ALC/ALC01 = 'A'"">
              <ChargeIndicator>
                <xsl:value-of select=""'False'"" />
              </ChargeIndicator>
            </xsl:when>
            <xsl:when test=""s0:ALC/ALC01 = 'C'"">
              <ChargeIndicator>
                <xsl:value-of select=""'True'"" />
              </ChargeIndicator>
            </xsl:when>
            <xsl:otherwise>
              <ChargeIndicator>
                <xsl:value-of select=""'XZ'"" />
              </ChargeIndicator>
            </xsl:otherwise>
          </xsl:choose>

          <xsl:if test=""s0:ALC/s0:C552/C55201"">
            <ID>
              <xsl:value-of select=""s0:ALC/s0:C552/C55201/text()"" />
            </ID>
          </xsl:if>

          <xsl:if test=""s0:ALC/s0:C214/C21401"">
            <AllowanceChargeReasonCode>
              <xsl:value-of select=""s0:ALC/s0:C214/C21401/text()"" />
            </AllowanceChargeReasonCode>
          </xsl:if>
          <xsl:if test=""s0:ALC/s0:C214/C21404"">
            <AllowanceChargeReason>
              <xsl:value-of select=""s0:ALC/s0:C214/C21404/text()"" />
            </AllowanceChargeReason>
          </xsl:if>

          <xsl:if test=""s0:ALC/ALC04"">
            <SequenceNumeric>
              <xsl:value-of select=""s0:ALC/ALC04/text()"" />
            </SequenceNumeric>
          </xsl:if>

          <xsl:if test=""s0:PCDLoop1/s0:PCD_2/s0:C501_2"">
            <MultiplierFactorNumeric>
              <xsl:value-of select=""userCSharp:DivideAmount(s0:PCDLoop1/s0:PCD_2/s0:C501_2/C50102/text())"" />
            </MultiplierFactorNumeric>
            <xsl:if test=""s0:PCDLoop1/s0:PCD_2/s0:C501_2/C50101"">
              <MultiplierFactorNumericCode>
                <xsl:value-of select=""s0:PCDLoop1/s0:PCD_2/s0:C501_2/C50102/text()"" />
              </MultiplierFactorNumericCode>
            </xsl:if>
            <xsl:if test=""s0:PCDLoop1/s0:PCD_2/s0:C501_2/C50103"">
              <MultiplierFactorNumericIDCode>
                <xsl:value-of select=""s0:PCDLoop1/s0:PCD_2/s0:C501_2/C50103/text()"" />
              </MultiplierFactorNumericIDCode>
            </xsl:if>
          </xsl:if>

          <xsl:if test=""s0:MOALoop1/s0:MOA_3/s0:C516_3/C51602"">
            <Amount>
              <xsl:value-of select=""s0:MOALoop1/s0:MOA_3/s0:C516_3/C51602/text()"" />
            </Amount>
          </xsl:if>
          <xsl:if test=""string-length(s0:MOALoop1/s0:MOA_3/s0:C516_3/C51601) != 0"">
            <AmountCode>
              <xsl:value-of select=""s0:MOALoop1/s0:MOA_3/s0:C516_3/C51601/text()"" />
            </AmountCode>
          </xsl:if>
        </AllowanceCharge>
      </xsl:for-each>
      <!--  End of  ALC-->

      <!-- *** TAX *** -->
      <xsl:for-each select=""s0:TAXLoop1"">
        <TaxTotal>
    <!--      <xsl:for-each select=""s0:MOA"">
            <xsl:choose>
              <xsl:when test=""s0:C516/C51601 = '124'"">
                <TaxAmount>
                  <xsl:value-of select=""s0:C516/C51602/text()"" />
                </TaxAmount>
                <TaxAmountCode>
                  <xsl:text>124</xsl:text>
                </TaxAmountCode>
              </xsl:when>
            </xsl:choose>
          </xsl:for-each> -->
  <!--          <xsl:for-each select=""//s0:MOALoop4/s0:MOA_10/s0:C516_10"">
            <xsl:choose>
              <xsl:variable name=""MOA_124"">                
             <xsl:when test=""C51601 = '124'"">
               
                
               
               <TaxAmount>
                 <xsl:value-of select=""$MOA_124"" />
               </TaxAmount>
            </xsl:when>
              </xsl:variable>  
           </xsl:choose>    
             <xsl:value-of select=""C51602/text()"" />
          </xsl:for-each> -->
          <xsl:if test=""string-length($MOA_176_Footer) != 0 or string-length($MOA_124_Footer) !=0 or string-length($MOA_176_TAXFooter) != 0 or string-length($MOA_124_TAXFooter) != 0"">
            <TaxAmount>
              <xsl:if test=""string-length($MOA_176_Footer) != 0"">
               <xsl:value-of select=""$MOA_176_Footer"" />  
              </xsl:if>
              <xsl:if test=""string-length($MOA_176_Footer) = 0 and string-length($MOA_124_Footer) != 0"">
                <xsl:value-of select=""$MOA_124_Footer"" />
              </xsl:if>
              <xsl:if test=""string-length($MOA_176_Footer) = 0 and string-length($MOA_124_Footer) = 0 and string-length($MOA_176_TAXFooter) != 0"">
                <xsl:value-of select=""$MOA_176_TAXFooter"" />
              </xsl:if>
              <xsl:if test=""string-length($MOA_176_Footer) = 0 and string-length($MOA_124_Footer) = 0 and string-length($MOA_176_TAXFooter) = 0 and string-length($MOA_124_TAXFooter)"">
                <xsl:value-of select=""$MOA_124_TAXFooter"" />
              </xsl:if>            
            </TaxAmount>  
          </xsl:if>
          <!--  <TaxAmount>
                 <xsl:value-of select=""$MOA_176_Header"" />
               </TaxAmount>  -->
          
          <TaxSubtotal>
          <!--  <xsl:for-each select=""s0:MOA"">
              <xsl:choose>
                <xsl:when test=""s0:C516/C51601 = '124'"">
                  <TaxAmount>
                    <xsl:value-of select=""s0:C516/C51602/text()"" />
                  </TaxAmount>
                  <TaxAmountCode>
                    <xsl:text>124</xsl:text>
                  </TaxAmountCode>
                </xsl:when>
                <xsl:when test=""s0:C516/C51601 = '125'"">
                  <TaxableAmount>
                    <xsl:value-of select=""s0:C516/C51602/text()"" />
                  </TaxableAmount>
                  <TaxableAmountCode>
                    <xsl:text>125</xsl:text>
                  </TaxableAmountCode>
                </xsl:when>
              </xsl:choose>
            </xsl:for-each> -->
            
            <xsl:if test=""string-length($MOA_176_Footer) != 0 or string-length($MOA_124_Footer) != 0 or string-length($MOA_176_TAXFooter) != 0 or string-length($MOA_124_TAXFooter) != 0"">
            <TaxAmount>
              <xsl:if test=""string-length($MOA_176_Footer) != 0"">
               <xsl:value-of select=""$MOA_176_Footer"" />  
              </xsl:if>
              <xsl:if test=""string-length($MOA_176_Footer) = 0 and string-length($MOA_124_Footer) != 0"">
                <xsl:value-of select=""$MOA_124_Footer"" />
              </xsl:if>
              <xsl:if test=""string-length($MOA_176_Footer) = 0 and string-length($MOA_124_Footer) = 0 and string-length($MOA_176_TAXFooter) != 0"">
                <xsl:value-of select=""$MOA_176_TAXFooter"" />
              </xsl:if>
              <xsl:if test=""string-length($MOA_176_Footer) = 0 and string-length($MOA_124_Footer) = 0 and string-length($MOA_176_TAXFooter) = 0 and string-length($MOA_124_TAXFooter) != 0"">
                <xsl:value-of select=""$MOA_124_TAXFooter"" />
              </xsl:if>            
            </TaxAmount>  
          </xsl:if>  
            
            
            <xsl:if test=""string-length($MOA_125_Footer) != 0 or string-length($MOA_125_TAXFooter) != 0"">
              <TaxableAmount>   
               <xsl:if test=""string-length($MOA_125_Footer) != 0"">
                <xsl:value-of select=""$MOA_125_Footer"" />  
               </xsl:if>
               <xsl:if test=""string-length($MOA_125_Footer) = 0 and string-length($MOA_125_TAXFooter) != 0 "">
                <xsl:value-of select=""$MOA_125_TAXFooter"" />  
               </xsl:if>
              </TaxableAmount>                    
            </xsl:if>  

            <xsl:if test=""s0:TAX/TAX01"">
              <TaxCode>
                <xsl:value-of select=""s0:TAX/TAX01/text()"" />
              </TaxCode>
            </xsl:if>

            <xsl:if test=""s0:TAX/s0:C243/C24304"">
              <TaxPercent>
                <xsl:value-of select=""s0:TAX/s0:C243/C24304/text()"" />
              </TaxPercent>
            </xsl:if>

            <TaxCategory>
              <xsl:if test=""s0:TAX/s0:C243/C24304"">
                <Percent>
                  <xsl:value-of select=""s0:TAX/s0:C243/C24304/text()"" />
                </Percent>
              </xsl:if>

              <xsl:if test=""s0:TAX/TAX06"">
                <xsl:choose>
                  <xsl:when test=""s0:TAX/TAX06 = 'S'"">
                    <ID>
                      <xsl:text>StandardRated</xsl:text>
                    </ID>
                  </xsl:when>
                  <xsl:when test=""s0:TAX/TAX06 = 's'"">
                    <ID>
                      <xsl:text>StandardRated</xsl:text>
                    </ID>
                  </xsl:when>
                  <xsl:when test=""s0:TAX/TAX06 = 'E'"">
                    <ID>
                      <xsl:text>ZeroRated</xsl:text>
                    </ID>
                  </xsl:when>
                  <xsl:when test=""s0:TAX/TAX06 = 'e'"">
                    <ID>
                      <xsl:text>ZeroRated</xsl:text>
                    </ID>
                  </xsl:when>
                  <xsl:when test=""s0:TAX/TAX06 = 'AE'"">
                    <ID>
                      <xsl:text>ReverseCharge</xsl:text>
                    </ID>
                  </xsl:when>
                  <xsl:when test=""s0:TAX/TAX06 = 'ae'"">
                    <ID>
                      <xsl:text>ReverseCharge</xsl:text>
                    </ID>
                  </xsl:when>
                </xsl:choose>
              </xsl:if>

              <TaxScheme>
                <xsl:if test=""s0:TAX/s0:C241/C24101"">
                  <Navn>
                    <xsl:value-of select=""s0:TAX/s0:C241/C24101/text()"" />
                  </Navn>
                </xsl:if>
                <xsl:if test=""s0:TAX/TAX06"">
                  <TaxTypeCode>
                    <xsl:value-of select=""s0:TAX/TAX06/text()"" />
                  </TaxTypeCode>
                </xsl:if>
              </TaxScheme>
            </TaxCategory>
          </TaxSubtotal>
        </TaxTotal>
      </xsl:for-each>
      <!-- ***  END OF TAX *** -->

      <!-- *** MOA Totals *** -->
      <Total>
        <xsl:for-each select=""s0:MOALoop4/s0:MOA_10/s0:C516_10"">
          <xsl:choose>
            <xsl:when test=""C51601 = '79'"">
              <LineTotalAmount>
                <xsl:value-of select=""C51602/text()"" />
              </LineTotalAmount>
            </xsl:when>
            <xsl:when test=""C51601 = '125'"">
              <TaxableAmount>
                <xsl:value-of select=""C51602/text()"" />
              </TaxableAmount>
            </xsl:when>
            <xsl:when test=""C51601 = '124'"">
              <TaxAmount>
                <xsl:value-of select=""C51602/text()"" />
              </TaxAmount>
            </xsl:when>            
            <xsl:when test=""C51601 = '176'"">
              <DutyTaxFeeTotalAmount>
                <xsl:value-of select=""C51602/text()"" />
              </DutyTaxFeeTotalAmount>
              <TaxExclAmount>
                <xsl:value-of select=""C51602/text()"" />
              </TaxExclAmount>            
            </xsl:when>
            <xsl:when test=""C51601 = '204'"">
              <AllowanceTotalAmount>
                <xsl:value-of select=""C51602/text()"" />
              </AllowanceTotalAmount>
            </xsl:when>
            <xsl:when test=""C51601 = '23'"">
              <ChargeTotalAmount>
                <xsl:value-of select=""C51602/text()"" />
              </ChargeTotalAmount>
            </xsl:when>
            <xsl:when test=""C51601 = '113'"">
              <PrepaidAmount>
                <xsl:value-of select=""C51602/text()"" />
              </PrepaidAmount>
            </xsl:when>
            <xsl:when test=""C51601 = '86'"">
              <PayableAmount>
                <xsl:value-of select=""C51602/text()"" />
              </PayableAmount>
            </xsl:when>
            <xsl:otherwise>
              <ExtraTotalMOALoop1>
                <Node>
                  <Code>
                    <xsl:value-of select=""C51601/text()"" />
                  </Code>
                  <Amount>
                    <xsl:value-of select=""C51602/text()"" />
                  </Amount>
                </Node>
              </ExtraTotalMOALoop1>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
        <xsl:for-each select=""s0:TAXLoop5/s0:MOA_11/s0:C516_10"">
          <xsl:choose>
              <xsl:when test=""C51601 = '176'"">
                <xsl:if test=""string-length($MOA_176_Footer) = 0 and string-length($MOA_176_TAXFooter) != 0"">
                  <DutyTaxFeeTotalAmount>
                    <xsl:value-of select=""C51602/text()"" />
                  </DutyTaxFeeTotalAmount>
                  <TaxExclAmount>
                    <xsl:value-of select=""C51602/text()"" />
                  </TaxExclAmount>
                </xsl:if>  
             </xsl:when>
          </xsl:choose>        
        </xsl:for-each>              
      </Total>
      <!-- *** END  OF  MOA Totals *** -->
      
      <!-- *** LIN Lines *** -->
      <Lines>
        <xsl:for-each select=""s0:LINLoop1"">
          <Line>
            
     <!-- Mandatory fields check -->       
      <xsl:if test=""string-length(s0:QTY_2/s0:C186_2[C18601='47']/C18602) = 0"">
        <xsl:message terminate=""yes"">QTY+47 not received in this message</xsl:message>
      </xsl:if>  
      <xsl:if test=""string-length(s0:MOALoop2/s0:MOA_5/s0:C516_5[C51601='203']/C51602) = 0"">
        <xsl:message terminate=""yes"">MOA+203 not received in this message</xsl:message>
      </xsl:if>         
      <xsl:if test=""string-length(s0:PRILoop1/s0:PRI/s0:C509[C50901='AAA']/C50902) = 0"">
        <xsl:message terminate=""yes"">PRI+AAA not received in this message</xsl:message>
      </xsl:if>         
      <xsl:if test=""string-length(s0:PRILoop1/s0:PRI/s0:C509[C50901='AAB']/C50902) = 0"">
        <xsl:message terminate=""yes"">PRI+AAB not received in this message</xsl:message>
      </xsl:if>         
            
            <LineNo>
              <xsl:value-of select=""s0:LIN/LIN01/text()"" />
            </LineNo>
                                    
           <!-- *** LIN QTY *** -->
            <xsl:for-each select=""s0:QTY_2/s0:C186_2"">
             <xsl:choose>
              <xsl:when test=""C18601 = '47'"">  
               <Quantity>
                <xsl:value-of select=""C18602/text()"" />
                </Quantity>
                <xsl:if test=""C18603"">
                 <UnitCode>
                   <xsl:value-of select=""userCSharp:ConvertUnitCode('TUNtoUBL', C18603/text())"" />
                   <!--   <xsl:value-of select=""C18603/text()"" /> -->
                 </UnitCode>
                </xsl:if>
               </xsl:when>              
               <xsl:when test=""C18601 = '128'"">
                <Delivery> 
                 <ID>
                  <xsl:value-of select=""C18601/text()"" />
                 </ID>
                 <Quantity>
                  <xsl:value-of select=""C18602/text()"" />
                 </Quantity>
                 <xsl:if test=""C18603"">
                  <UnitCode>
                    <xsl:value-of select=""userCSharp:ConvertUnitCode('TUNtoUBL', C18603/text())"" />
                    <!--   <xsl:value-of select=""C18603/text()"" /> -->
                  </UnitCode>
                 </xsl:if>
                </Delivery>  
               </xsl:when>              
               <xsl:when test=""C18601 = '59'"">
                <ConsumerUnit> 
                 <Quantity>
                   <xsl:value-of select=""C18602/text()"" />
                 </Quantity>
                 <xsl:if test=""C18603"">
                  <UnitCode>
                    <xsl:value-of select=""userCSharp:ConvertUnitCode('TUNtoUBL', C18603/text())"" />
                    <!--   <xsl:value-of select=""C18603/text()"" /> -->
                  </UnitCode>
                 </xsl:if>
                </ConsumerUnit>  
               </xsl:when>                               
              </xsl:choose>  
             </xsl:for-each>  
            <!--  End of LIN  QTY-->
            
            <Item>
               <xsl:if test=""s0:LIN/s0:C212/C21201"">
                <StandardItemID>
                  <xsl:value-of select=""s0:LIN/s0:C212/C21201/text()"" />
                </StandardItemID>
                <ShemeID>
                  <xsl:if test=""s0:LIN/s0:C212/C21202"">
                  <StdItemID>
                    <xsl:value-of select=""s0:LIN/s0:C212/C21202/text()"" />                  
                  </StdItemID>
                  </xsl:if>
                </ShemeID>                
              </xsl:if>
              
              <!--    *** PIA *** -->                
              <xsl:for-each select=""s0:PIA"">
                <xsl:sort select=""s0:C212_2/C21202"" order=""ascending"" data-type=""text""></xsl:sort>
               <xsl:choose>            
                 
                     
                <xsl:when test=""s0:C212_2/C21202 = 'SA'"">
                  <SellerItemID>
                    <xsl:value-of select=""s0:C212_2/C21201/text()"" />
                  </SellerItemID>               
                </xsl:when>
                 
                 <xsl:when test=""s0:C212_2/C21202 = 'BP'"">
                  <BuyerItemID>
                    <xsl:value-of select=""s0:C212_2/C21201/text()"" />
                  </BuyerItemID>               
                </xsl:when>                                                     
                   
                 <xsl:when test=""s0:C212_2/C21202 = 'MP'"">
                  <AdditionalItemID>
                    <xsl:value-of select=""s0:C212_2/C21201/text()"" />
                  </AdditionalItemID>            
                </xsl:when>
               
                 <xsl:when test=""s0:C212_2/C21202 = 'MF'"">
                  <ManufacturerItemID>
                    <xsl:value-of select=""s0:C212_2/C21201/text()"" />
                  </ManufacturerItemID>               
                </xsl:when>

                 <xsl:otherwise>
                   <ExtraPIALoop>
                     <Node>
                       <xsl:if test=""PIA01"">
                        <DigitalCode>
                          <xsl:value-of select=""PIA01/text()"" />                       
                        </DigitalCode>
                       </xsl:if>
                       <PIA>
                         <xsl:value-of select=""s0:C212_2/C21201/text()"" />                       
                       </PIA>
                       <Code>
                         <xsl:value-of select=""s0:C212_2/C21202/text()"" />                                              
                       </Code>
                     </Node>                   
                   </ExtraPIALoop>                 
                 </xsl:otherwise> 
                          
              </xsl:choose>     
                
       <!-- 212_3-->         
               <xsl:choose>            
                                      
                <xsl:when test=""s0:C212_3/C21202 = 'SA'"">
                  <SellerItemID>
                    <xsl:value-of select=""s0:C212_3/C21201/text()"" />
                  </SellerItemID>               
                </xsl:when>
                 
                 <xsl:when test=""s0:C212_2/C21203 = 'BP'"">
                  <BuyerItemID>
                    <xsl:value-of select=""s0:C212_3/C21201/text()"" />
                  </BuyerItemID>               
                </xsl:when>                                                     
                   
                 <xsl:when test=""s0:C212_2/C21203 = 'MP'"">
                  <AdditionalItemID>
                    <xsl:value-of select=""s0:C212_3/C21201/text()"" />
                  </AdditionalItemID>              
                </xsl:when>
               
                 <xsl:when test=""s0:C212_2/C21203 = 'MF'"">
                  <ManufacturerItemID>
                    <xsl:value-of select=""s0:C212_3/C21201/text()"" />
                  </ManufacturerItemID>               
                </xsl:when>
               
              </xsl:choose>                 
                
       <!-- 212_4-->         
               <xsl:choose>            
                                      
                <xsl:when test=""s0:C212_4/C21202 = 'SA'"">
                  <SellerItemID>
                    <xsl:value-of select=""s0:C212_4/C21201/text()"" />
                  </SellerItemID>               
                </xsl:when>
                 
                 <xsl:when test=""s0:C212_4/C21203 = 'BP'"">
                  <BuyerItemID>
                    <xsl:value-of select=""s0:C212_4/C21201/text()"" />
                  </BuyerItemID>               
                </xsl:when>                                                     
                   
                 <xsl:when test=""s0:C212_4/C21203 = 'MP'"">
                  <AdditionalItemID>
                    <xsl:value-of select=""s0:C212_4/C21201/text()"" />
                  </AdditionalItemID>              
                </xsl:when>
               
                 <xsl:when test=""s0:C212_4/C21203 = 'MF'"">
                  <ManufacturerItemID>
                    <xsl:value-of select=""s0:C212_4/C21201/text()"" />
                  </ManufacturerItemID>               
                </xsl:when>
                             
              </xsl:choose>    
                
       <!-- 212_5-->         
               <xsl:choose>                                 
                <xsl:when test=""s0:C212_5/C21202 = 'SA'"">
                  <SellerItemID>
                    <xsl:value-of select=""s0:C212_5/C21201/text()"" />
                  </SellerItemID>               
                </xsl:when>
                 
                 <xsl:when test=""s0:C212_5/C21203 = 'BP'"">
                  <BuyerItemID>
                    <xsl:value-of select=""s0:C212_5/C21201/text()"" />
                  </BuyerItemID>               
                </xsl:when>                 
                   
                 <xsl:when test=""s0:C212_5/C21203 = 'MP'"">
                  <AdditionalItemID>
                    <xsl:value-of select=""s0:C212_5/C21201/text()"" />
                  </AdditionalItemID>              
                </xsl:when>
               
                 <xsl:when test=""s0:C212_5/C21203 = 'MF'"">
                  <ManufacturerItemID>
                    <xsl:value-of select=""s0:C212_5/C21201/text()"" />
                  </ManufacturerItemID>               
                </xsl:when>
                 
              </xsl:choose>   
                
       <!-- 212_6-->         
               <xsl:choose>                             
                     
                <xsl:when test=""s0:C212_6/C21202 = 'SA'"">
                  <SellerItemID>
                    <xsl:value-of select=""s0:C212_6/C21201/text()"" />
                  </SellerItemID>               
                </xsl:when>
                 
                 <xsl:when test=""s0:C212_6/C21203 = 'BP'"">
                  <BuyerItemID>
                    <xsl:value-of select=""s0:C212_6/C21201/text()"" />
                  </BuyerItemID>               
                </xsl:when>                                  
                                      
                 <xsl:when test=""s0:C212_6/C21203 = 'MP'"">
                  <AdditionalItemID>
                    <xsl:value-of select=""s0:C212_6/C21201/text()"" />
                  </AdditionalItemID>              
                </xsl:when>
               
                 <xsl:when test=""s0:C212_6/C21203 = 'MF'"">
                  <ManufacturerItemID>
                    <xsl:value-of select=""s0:C212_6/C21201/text()"" />
                  </ManufacturerItemID>               
                </xsl:when>   
                                
              </xsl:choose>                  
            </xsl:for-each>            
        <!--    *** END OF PIA  *** -->
              
        <!-- *** LIN IMD *** -->
              <Name>
                <xsl:value-of select=""s0:IMD_2/s0:C273_2/C27304/text()"" />
                <xsl:if test=""s0:IMD_2/s0:C273_2/C27305"">
                  <xsl:value-of select=""':'"" />
                  <xsl:value-of select=""s0:IMD_2/s0:C273_2/C27305/text()"" />
                </xsl:if>   
              </Name>
       <!-- ***End  of  LIN IMD *** -->    
            
            </Item>
    
     <!--  FTX SEGMENT -->      
       <xsl:for-each select=""s0:FTX_5"">
       <NoteLoop>    
         <Node>
       <xsl:if test=""FTX01"">
         <NoteQualifier>  
          <xsl:value-of select=""FTX01/text()"" />      
         </NoteQualifier>    
       </xsl:if>  
        <xsl:if test=""FTX02"">
         <NoteFunction>  
          <xsl:value-of select=""FTX02/text()"" />      
         </NoteFunction>    
       </xsl:if>  
        <xsl:if test=""FTX05"">
         <NoteLanguage>  
          <xsl:value-of select=""FTX05/text()"" />      
         </NoteLanguage>    
       </xsl:if>         
        <Note>
          <xsl:value-of select=""s0:C108_5/C10801/text()"" />
        </Note>  
       </Node>
         
          <xsl:if test=""s0:C108_5/C10802"">
         <Node>
           <xsl:if test=""FTX01"">
         <NoteQualifier>  
          <xsl:value-of select=""FTX01/text()"" />      
         </NoteQualifier>    
       </xsl:if>  
        <xsl:if test=""FTX02"">
         <NoteFunction>  
          <xsl:value-of select=""FTX02/text()"" />      
         </NoteFunction>    
       </xsl:if>  
        <xsl:if test=""FTX05"">
         <NoteLanguage>  
          <xsl:value-of select=""FTX05/text()"" />      
         </NoteLanguage>    
       </xsl:if>         
          <Note>
            <xsl:value-of select=""s0:C108_5/C10802/text()"" />        
          </Note>
         </Node>             
          </xsl:if>
       
         
          <xsl:if test=""s0:C108_5/C10803"">
            <Node>
              <xsl:if test=""FTX01"">
         <NoteQualifier>  
          <xsl:value-of select=""FTX01/text()"" />      
         </NoteQualifier>    
       </xsl:if>  
        <xsl:if test=""FTX02"">
         <NoteFunction>  
          <xsl:value-of select=""FTX02/text()"" />      
         </NoteFunction>    
       </xsl:if>  
        <xsl:if test=""FTX05"">
         <NoteLanguage>  
          <xsl:value-of select=""FTX05/text()"" />      
         </NoteLanguage>    
       </xsl:if>         
            <Note>
             <xsl:value-of select=""s0:C108_5/C10803/text()"" />        
            </Note>
           </Node>   
          </xsl:if>
         
          <xsl:if test=""s0:C108_5/C10804""> 
          <Node>
            <xsl:if test=""FTX01"">
         <NoteQualifier>  
          <xsl:value-of select=""FTX01/text()"" />      
         </NoteQualifier>    
       </xsl:if>  
        <xsl:if test=""FTX02"">
         <NoteFunction>  
          <xsl:value-of select=""FTX02/text()"" />      
         </NoteFunction>    
       </xsl:if>  
        <xsl:if test=""FTX05"">
         <NoteLanguage>  
          <xsl:value-of select=""FTX05/text()"" />      
         </NoteLanguage>    
       </xsl:if>         
           <Note>
            <xsl:value-of select=""s0:C108_5/C10804/text()"" />        
          </Note>
          </Node>  
          </xsl:if>

          <xsl:if test=""s0:C108_5/C10805""> 
           <Node>
             <xsl:if test=""FTX01"">
         <NoteQualifier>  
          <xsl:value-of select=""FTX01/text()"" />      
         </NoteQualifier>    
       </xsl:if>  
        <xsl:if test=""FTX02"">
         <NoteFunction>  
          <xsl:value-of select=""FTX02/text()"" />      
         </NoteFunction>    
       </xsl:if>  
        <xsl:if test=""FTX05"">
         <NoteLanguage>  
          <xsl:value-of select=""FTX05/text()"" />      
         </NoteLanguage>    
       </xsl:if>         
            <Note>
            <xsl:value-of select=""s0:C108_5/C10805/text()"" />        
           </Note>  
          </Node>   
         </xsl:if>      
       
      </NoteLoop>     
      </xsl:for-each>          
   <!--  END OF  FTX -->            
                                    
        <!-- ***LIN RFF *** -->
           <OrderReference>
            <xsl:for-each select=""s0:RFFLoop5"">      
             <Node>
              <Code>            
               <xsl:value-of select=""s0:RFF_7/s0:C506_7/C50601/text()"" />
              </Code>
              <Reference>
               <xsl:value-of select=""s0:RFF_7/s0:C506_7/C50602/text()"" />          
              </Reference>        
             </Node>                      
            </xsl:for-each>  
           </OrderReference>  
        <!-- ***End of  LIN RFF *** -->    
            
        <!-- *** LIN MOA 203 *** -->  
           <xsl:for-each select=""s0:MOALoop2"">
             <xsl:choose>
               <xsl:when test=""s0:MOA_5/s0:C516_5/C51601='203'"">
                 <LineAmountTotal>
                   <xsl:value-of select=""s0:MOA_5/s0:C516_5/C51602/text()"" />
                 </LineAmountTotal>
                 <xsl:if test=""s0:MOA_5/s0:C516_5/C51603"">
                   <Currency>
                     <xsl:value-of select=""s0:MOA_5/s0:C516_5/C51603/text()"" />
                   </Currency>
                 </xsl:if>
               </xsl:when>
             </xsl:choose>   
           </xsl:for-each>   
         <!-- *** End of   LIN MOA 203 *** --> 
         
        <!-- *** LIN MOA ALL *** -->    
          <ExtraMOALoop>
           <xsl:for-each select=""s0:MOALoop2"">
            <xsl:if test=""s0:MOA_5/s0:C516_5/C51601 != '203'"">
             <Node>
              <Code>
               <xsl:value-of select=""s0:MOA_5/s0:C516_5/C51601/text()"" />               
              </Code>  
              <Amount>
               <xsl:value-of select=""s0:MOA_5/s0:C516_5/C51602/text()"" /> 
              </Amount>
              <xsl:if test=""s0:MOA_5/s0:C516_5/C51603"">
               <Currency>
                <xsl:value-of select=""s0:MOA_5/s0:C516_5/C51603/text()"" />
               </Currency>
              </xsl:if> 
             </Node> 
            </xsl:if>      
           </xsl:for-each>   
          </ExtraMOALoop>
       <!-- *** LIN MOA ALL is good *** -->    
                   
      <!-- *** LIN PRI *** -->
            <xsl:for-each select=""s0:PRILoop1/s0:PRI/s0:C509"">
              <xsl:choose>
                <xsl:when test=""C50901 = 'AAA'"">
                  <PriceNet>
                    <Price>
                      <xsl:value-of select=""C50902/text()"" /> 
                    </Price>
                   <xsl:if test=""string-length(C50905) != 0""> 
                    <Quantity>
                      <xsl:value-of select=""C50905/text()"" />              
                    </Quantity>
                   </xsl:if>  
                   <xsl:if test=""string-length(C50906) != 0""> 
                    <UnitCode>
                      <xsl:value-of select=""userCSharp:ConvertUnitCode('TUNtoUBL', C50906/text())"" />
                      <!--  <xsl:value-of select=""C50906/text()"" /> --> 
                    </UnitCode>
                   </xsl:if>   
                    <PristypeCode>
                      <xsl:value-of select=""'AAA'"" />
                    </PristypeCode>
                  </PriceNet>  
                </xsl:when>
                <xsl:when test=""C50901 = 'AAB'"">
                  <PriceGross>
                    <Price>
                      <xsl:value-of select=""C50902/text()"" /> 
                    </Price>
                   <xsl:if test=""string-length(C50905) != 0""> 
                    <Quantity>
                      <xsl:value-of select=""C50905/text()"" />              
                    </Quantity>
                   </xsl:if>  
                   <xsl:if test=""string-length(C50906) != 0""> 
                    <UnitCode>
                      <xsl:value-of select=""userCSharp:ConvertUnitCode('TUNtoUBL', C50906/text())"" />
                      <!--  <xsl:value-of select=""C50906/text()"" /> --> 
                    </UnitCode>
                   </xsl:if>   
                    <PristypeCode>
                      <xsl:value-of select=""'AAB'"" />
                    </PristypeCode>
                  </PriceGross>  
                </xsl:when>
              </xsl:choose>
            </xsl:for-each>                                 
      <!-- *** END OF LIN PRI *** -->     
     
  <!-- *** LIN  TAX SEGMENT *** -->            
         <xsl:for-each select=""s0:TAXLoop3"">   
         <TaxTotal> 
          <xsl:for-each select=""s0:MOA_7"">  
          <!-- <xsl:if test=""s0:MOA_7/s0:C516_7/C51602""> -->
             <xsl:choose>
               <xsl:when test=""s0:C516_7/C51601 = '124'"">
                 <TaxAmount>
                   <xsl:value-of select=""s0:C516_7/C51602/text()"" />                 
                 </TaxAmount>
                 <TaxAmountCode>
                   <xsl:text>124</xsl:text>                 
                 </TaxAmountCode>
               </xsl:when>
             </xsl:choose>  
            </xsl:for-each>
          <!-- </xsl:if> -->
          <TaxSubtotal>            
           <xsl:for-each select=""s0:MOA_7""> 
           <!-- <xsl:if test=""s0:MOA_7/s0:C516_7/C51602""> -->
             <xsl:choose>
               <xsl:when test=""s0:C516_7/C51601 = '124'"">
                 <TaxAmount>
                   <xsl:value-of select=""s0:C516_7/C51602/text()"" />                 
                 </TaxAmount>
                 <TaxAmountCode>
                   <xsl:text>124</xsl:text>                 
                 </TaxAmountCode>
               </xsl:when>
                <xsl:when test=""s0:C516_7/C51601 = '125'"">
                 <TaxableAmount>
                   <xsl:value-of select=""s0:C516_7/C51602/text()"" />                 
                 </TaxableAmount>
                 <TaxableAmountCode>
                   <xsl:text>125</xsl:text>                 
                 </TaxableAmountCode>
               </xsl:when>             
             </xsl:choose>   
            </xsl:for-each> 
          <!-- </xsl:if>  -->
          <xsl:if test=""s0:TAX_3/TAX01""> 
            <TaxCode>
              <xsl:value-of select=""s0:TAX_3/TAX01/text()"" />
            </TaxCode>
          </xsl:if>  
          <xsl:if test=""s0:TAX_3/s0:C243_3/C24304"">
            <TaxPercent>
              <xsl:value-of select=""s0:TAX_3/s0:C243_3/C24304/text()"" />
            </TaxPercent>
          </xsl:if>
          <TaxCategory>
             <xsl:if test=""s0:TAX_3/TAX06"">
              <xsl:choose>
                <xsl:when test=""s0:TAX_3/TAX06 = 'S'"">
                  <ID>
                    <xsl:text>StandardRated</xsl:text>
                  </ID>                
                </xsl:when>
                <xsl:when test=""s0:TAX_3/TAX06 = 's'"">
                  <ID>
                    <xsl:text>StandardRated</xsl:text>
                  </ID>                
                </xsl:when>
                <xsl:when test=""s0:TAX_3/TAX06 = 'E'"">
                  <ID>
                    <xsl:text>ZeroRated</xsl:text>
                  </ID>                
                </xsl:when>
               <xsl:when test=""s0:TAX_3/TAX06 = 'e'"">
                  <ID>
                    <xsl:text>ZeroRated</xsl:text>
                  </ID>                
                </xsl:when> 
                <xsl:when test=""s0:TAX_3/TAX06 = 'AE'"">
                  <ID>
                    <xsl:text>ReverseCharge</xsl:text>
                  </ID>                
                </xsl:when>
              <xsl:when test=""s0:TAX_3/TAX06 = 'ae'"">
                  <ID>
                    <xsl:text>ReverseCharge</xsl:text>
                  </ID>                
                </xsl:when>
              </xsl:choose>            
            </xsl:if> 
            <xsl:if test=""s0:TAX_3/s0:C243_3/C24304"">
              <Percent>
                <xsl:value-of select=""s0:TAX_3/s0:C243_3/C24304/text()"" />
              </Percent>
            </xsl:if>
            <TaxScheme>
              <xsl:if test=""s0:TAX_3/s0:C241_3/C24101"">
                <Navn>
                  <xsl:value-of select=""s0:TAX_3/s0:C241_3/C24101/text()"" />
                </Navn>
              </xsl:if>
              <xsl:if test=""s0:TAX_3/TAX06"">
                <TaxTypeCode>
                  <xsl:value-of select=""s0:TAX_3/TAX06/text()"" />
                </TaxTypeCode>
              </xsl:if>
            </TaxScheme>
          </TaxCategory>
         </TaxSubtotal>
        </TaxTotal>                  
       </xsl:for-each>      
      <!-- ***  END OF  LIN TAX *** -->               
                                                                     
            <!-- *** LIN ALC *** -->
            <xsl:for-each select=""s0:ALCLoop2"">
              <AllowanceCharge>
                <xsl:choose>
                  <xsl:when test=""s0:ALC_2/ALC01 = 'A'"">
                    <ChargeIndicator>
                      <xsl:value-of select=""'False'"" />
                    </ChargeIndicator>
                  </xsl:when>
                  <xsl:when test=""s0:ALC_2/ALC01 = 'C'"">
                    <ChargeIndicator>
                      <xsl:value-of select=""'True'"" />
                    </ChargeIndicator>
                  </xsl:when>
                  <xsl:otherwise>
                    <ChargeIndicator>
                      <xsl:value-of select=""'XZ'"" />
                    </ChargeIndicator>                  
                  </xsl:otherwise>
                 </xsl:choose>
                
                 <xsl:if test=""s0:ALC_2/s0:C552_2/C55201""> 
                  <ID>
                   <xsl:value-of select=""s0:ALC_2/s0:C552_2/C55201/text()"" />
                  </ID>
                 </xsl:if>                  
                
                <xsl:if test=""s0:ALC_2/s0:C214_2/C21401""> 
                   <AllowanceChargeReasonCode>
                     <xsl:value-of select=""s0:ALC_2/s0:C214_2/C21401/text()"" />
                  </AllowanceChargeReasonCode>
                </xsl:if>
                
                <xsl:if test=""s0:ALC_2/s0:C214_2/C21404"">
                  <AllowanceChargeReason>
                    <xsl:value-of select=""s0:ALC_2/s0:C214_2/C21404/text()"" />
                  </AllowanceChargeReason>
                </xsl:if>
                <xsl:if test=""s0:ALC_2/ALC04"">
                 <SequenceNumeric>
                  <xsl:value-of select=""s0:ALC_2/ALC04/text()"" />
                 </SequenceNumeric>
                </xsl:if>                
                <xsl:if test=""s0:PCDLoop2/s0:PCD_5/s0:C501_5"">
                  <MultiplierFactorNumeric>
                    <xsl:value-of select=""userCSharp:DivideAmount(s0:PCDLoop2/s0:PCD_5/s0:C501_5/C50102/text())"" />
                  </MultiplierFactorNumeric>
                 <xsl:if test=""s0:PCDLoop2/s0:PCD_6/s0:C501_6/C50101"">
                  <MultiplierFactorNumericCode>
                   <xsl:value-of select=""s0:PCDLoop2/s0:PCD_6/s0:C501_6/C50101/text()"" />                     
                  </MultiplierFactorNumericCode>       
                 </xsl:if>   
                </xsl:if>
                <xsl:if test=""s0:MOALoop3/s0:MOA_8/s0:C516_8/C51602"">
                 <Amount>
                  <xsl:value-of select=""s0:MOALoop3/s0:MOA_8/s0:C516_8/C51602/text()"" />             
                 </Amount>
                 <xsl:if test=""string-length(s0:MOALoop3/s0:MOA_8/s0:C516_8/C51601) != 0""> 
                  <AmountCode>
                   <xsl:value-of select=""s0:MOALoop3/s0:MOA_8/s0:C516_8/C51601/text()"" />                  
                  </AmountCode>                
                </xsl:if>                  
               </xsl:if> 
              </AllowanceCharge>
            </xsl:for-each>
           <!--  End of LIN  ALC -->  
          
            
          </Line>
        </xsl:for-each>
      </Lines>
    </ns0:Invoice>
  </xsl:template>
  
  <msxsl:script language=""C#"" implements-prefix=""userCSharp"">
    <![CDATA[
    
    public void ThrowException(string ErrorMessage)    
     {
      throw new System.Exception(ErrorMessage);
     }
   
    public string editDate(string aDate)
  {
      string NewDate;
      NewDate = string.Concat(aDate.Substring(0,4), '-', aDate.Substring(4,2), '-', aDate.Substring(6,2));
      return NewDate;
  }
  
    public string DivideAmount(Double aAmount)
  {
      string NewAmount;
      NewAmount = string.Format(""{0:0.00000}"", (aAmount / 100));
      return NewAmount.Replace("","",""."");
  } 
  
  public string ReturnGUID()
{
 Guid g = Guid.NewGuid();
 String GUID = g.ToString();
 return GUID.ToUpper();
}

public string ConvertUnitCode(string ConversionDirection, string IngoingUnitCode)
  {
  string OutgoingUnitCode = """";

  switch (ConversionDirection)
  {
  case ""TUNtoUBL"":
  switch (IngoingUnitCode)
  {
  case ""%KG"":
  OutgoingUnitCode = ""HK"";
  break;
  case ""HMT"":
  OutgoingUnitCode = ""HMT"";
  break;
  case ""%ST"":
  OutgoingUnitCode = ""CNP"";
  break;
  case ""TUS"":
  OutgoingUnitCode = ""T3"";
  break;
  case ""ST"":
  OutgoingUnitCode = ""ST"";
  break;
  case ""BB"":
  OutgoingUnitCode = ""BB"";
  break;
  case ""BDT"":
  OutgoingUnitCode = ""BE"";
  break;
  case ""BLK"":
  OutgoingUnitCode = ""D64"";
  break;
  case ""BX"":
  OutgoingUnitCode = ""BX"";
  break;
  case ""PCE"":
  OutgoingUnitCode = ""C62"";
  break;
  case ""BEG"":
  OutgoingUnitCode = ""CU"";
  break;
  case ""BOT"":
  OutgoingUnitCode = ""2W"";
  break;
  case ""CMT"":
  OutgoingUnitCode = ""CMT"";
  break;
  case ""CH"":
  OutgoingUnitCode = ""CH"";
  break;
  case ""DAY"":
  OutgoingUnitCode = ""DAY"";
  break;
  case ""DIS"":
  OutgoingUnitCode = ""DS"";
  break;
  case ""DLT"":
  OutgoingUnitCode = ""DLT"";
  break;
  case ""DMT"":
  OutgoingUnitCode = ""DMT"";
  break;
  case ""DK"":
  OutgoingUnitCode = ""CA"";
  break;
  case ""DS"":
  OutgoingUnitCode = ""TN"";
  break;
  case ""FL"":
  OutgoingUnitCode = ""BO"";
  break;
  case ""FOT"":
  OutgoingUnitCode = ""FOT"";
  break;
  case ""GRM"":
  OutgoingUnitCode = ""GRM"";
  break;
  case ""GRO"":
  OutgoingUnitCode = ""GRO"";
  break;
  case ""HLT"":
  OutgoingUnitCode = ""HLT"";
  break;
  case ""KAR"":
  OutgoingUnitCode = ""CT"";
  break;
  case ""KS"":
  OutgoingUnitCode = ""Z2"";
  break;
  case ""KGM"":
  OutgoingUnitCode = ""KGM"";
  break;
  case ""CMQ"":
  OutgoingUnitCode = ""CMQ"";
  break;
  case ""MTQ"":
  OutgoingUnitCode = ""MTQ"";
  break;
  case ""CMK"":
  OutgoingUnitCode = ""CMK"";
  break;
  case ""MTK"":
  OutgoingUnitCode = ""MTK"";
  break;
  case ""LGD"":
  OutgoingUnitCode = ""LN"";
  break;
  case ""LTR"":
  OutgoingUnitCode = ""LTR"";
  break;
  case ""LES"":
  OutgoingUnitCode = ""NL"";
  break;
  case ""MTR"":
  OutgoingUnitCode = ""MTR"";
  break;
  case ""MLT"":
  OutgoingUnitCode = ""MLT"";
  break;
  case ""MMT"":
  OutgoingUnitCode = ""MMT"";
  break;
  case ""PAK"":
  OutgoingUnitCode = ""PK"";
  break;
  case ""PF"":
  OutgoingUnitCode = ""PF"";
  break;
  case ""PB"":
  OutgoingUnitCode = ""BB"";
  break;
  case ""PAR"":
  OutgoingUnitCode = ""PR"";
  break;
  case ""PAT"":
  OutgoingUnitCode = ""CQ"";
  break;
  case ""PL"":
  OutgoingUnitCode = ""PG"";
  break;
  case ""PS"":
  OutgoingUnitCode = ""BG"";
  break;
  case ""RIN"":
  OutgoingUnitCode = ""RG"";
  break;
  case ""NRL"":
  OutgoingUnitCode = ""RO"";
  break;
  case ""ROR"":
  OutgoingUnitCode = ""TU"";
  break;
  case ""SP"":
  OutgoingUnitCode = ""BJ"";
  break;
  case ""SPO"":
  OutgoingUnitCode = ""SO"";
  break;
  case ""SAK"":
  OutgoingUnitCode = ""SA"";
  break;
  case ""SET"":
  OutgoingUnitCode = ""SET"";
  break;
  case ""HUR"":
  OutgoingUnitCode = ""HUR"";
  break;
  case ""TNE"":
  OutgoingUnitCode = ""TNE"";
  break;
  case ""TR"":
  OutgoingUnitCode = ""DR"";
  break;
  case ""TB"":
  OutgoingUnitCode = ""TB"";
  break;
  case ""ASK"":
  OutgoingUnitCode = ""CS"";
  break;
  case ""BÅND"":
  OutgoingUnitCode = ""BND"";
  break;
  case ""BREV"":
  OutgoingUnitCode = ""BRV"";
  break;
  case ""COL"":
  OutgoingUnitCode = ""COL"";
  break;
  case ""HAP"":
  OutgoingUnitCode = ""HAP"";
  break;
  case ""KORT"":
  OutgoingUnitCode = ""KOR"";
  break;
  case ""ROND"":
  OutgoingUnitCode = ""RON"";
  break;
  case ""SKF"":
  OutgoingUnitCode = ""SKF"";
  break;
  case ""TUBE"":
  OutgoingUnitCode = ""TB"";
  break;
  case ""XXX"":
  OutgoingUnitCode = ""XXX"";
  break;
  default:
  OutgoingUnitCode = IngoingUnitCode;
  break;
  }
  break;

  case ""UBLtoTUN"":

  switch (IngoingUnitCode)
  {
  case ""HK"":
  OutgoingUnitCode = ""%KG"";
  break;
  case ""HMT"":
  OutgoingUnitCode = ""HMT"";
  break;
  case ""CNP"":
  OutgoingUnitCode = ""%ST"";
  break;
  case ""T3"":
  OutgoingUnitCode = ""TUS"";
  break;
  case ""ST"":
  OutgoingUnitCode = ""ST"";
  break;
  case ""BE"":
  OutgoingUnitCode = ""BDT"";
  break;
  case ""D64"":
  OutgoingUnitCode = ""BLK"";
  break;
  case ""BX"":
  OutgoingUnitCode = ""BX"";
  break;
  case ""C62"":
  OutgoingUnitCode = ""PCE"";
  break;
  case ""CU"":
  OutgoingUnitCode = ""BEG"";
  break;
  case ""2W"":
  OutgoingUnitCode = ""BOT"";
  break;
  case ""CMT"":
  OutgoingUnitCode = ""CMT"";
  break;
  case ""CH"":
  OutgoingUnitCode = ""CH"";
  break;
  case ""DAY"":
  OutgoingUnitCode = ""DAY"";
  break;
  case ""DS"":
  OutgoingUnitCode = ""DIS"";
  break;
  case ""DLT"":
  OutgoingUnitCode = ""DLT"";
  break;
  case ""DMT"":
  OutgoingUnitCode = ""DMT"";
  break;
  case ""CA"":
  OutgoingUnitCode = ""DK"";
  break;
  case ""TN"":
  OutgoingUnitCode = ""DS"";
  break;
  case ""BO"":
  OutgoingUnitCode = ""FL"";
  break;
  case ""FOT"":
  OutgoingUnitCode = ""FOT"";
  break;
  case ""GRM"":
  OutgoingUnitCode = ""GRM"";
  break;
  case ""GRO"":
  OutgoingUnitCode = ""GRO"";
  break;
  case ""HLT"":
  OutgoingUnitCode = ""HLT"";
  break;
  case ""CT"":
  OutgoingUnitCode = ""KAR"";
  break;
  case ""Z2"":
  OutgoingUnitCode = ""KS"";
  break;
  case ""KGM"":
  OutgoingUnitCode = ""KGM"";
  break;
  case ""CMQ"":
  OutgoingUnitCode = ""CMQ"";
  break;
  case ""MTQ"":
  OutgoingUnitCode = ""MTQ"";
  break;
  case ""CMK"":
  OutgoingUnitCode = ""CMK"";
  break;
  case ""MTK"":
  OutgoingUnitCode = ""MTK"";
  break;
  case ""LN"":
  OutgoingUnitCode = ""LGD"";
  break;
  case ""LTR"":
  OutgoingUnitCode = ""LTR"";
  break;
  case ""NL"":
  OutgoingUnitCode = ""LES"";
  break;
  case ""MTR"":
  OutgoingUnitCode = ""MTR"";
  break;
  case ""MLT"":
  OutgoingUnitCode = ""MLT"";
  break;
  case ""MMT"":
  OutgoingUnitCode = ""MMT"";
  break;
  case ""PK"":
  OutgoingUnitCode = ""PAK"";
  break;
  case ""PF"":
  OutgoingUnitCode = ""PF"";
  break;
  case ""BB"":
  OutgoingUnitCode = ""PB"";
  break;
  case ""PR"":
  OutgoingUnitCode = ""PAR"";
  break;
  case ""CQ"":
  OutgoingUnitCode = ""PAT"";
  break;
  case ""PG"":
  OutgoingUnitCode = ""PL"";
  break;
  case ""BG"":
  OutgoingUnitCode = ""PS"";
  break;
  case ""RG"":
  OutgoingUnitCode = ""RIN"";
  break;
  case ""RO"":
  OutgoingUnitCode = ""NRL"";
  break;
  case ""TU"":
  OutgoingUnitCode = ""ROR"";
  break;
  case ""BJ"":
  OutgoingUnitCode = ""SP"";
  break;
  case ""SO"":
  OutgoingUnitCode = ""SPO"";
  break;
  case ""SA"":
  OutgoingUnitCode = ""SAK"";
  break;
  case ""SET"":
  OutgoingUnitCode = ""SET"";
  break;
  case ""HUR"":
  OutgoingUnitCode = ""HUR"";
  break;
  case ""TNE"":
  OutgoingUnitCode = ""TNE"";
  break;
  case ""DR"":
  OutgoingUnitCode = ""TR"";
  break;
  case ""CS"":
  OutgoingUnitCode = ""ASK"";
  break;
  case ""BND"":
  OutgoingUnitCode = ""BÅND"";
  break;
  case ""BRV"":
  OutgoingUnitCode = ""BREV"";
  break;
  case ""COL"":
  OutgoingUnitCode = ""COL"";
  break;
  case ""HAP"":
  OutgoingUnitCode = ""HAP"";
  break;
  case ""KOR"":
  OutgoingUnitCode = ""KORT"";
  break;
  case ""PB"":
  OutgoingUnitCode = ""PB"";
  break;
  case ""RON"":
  OutgoingUnitCode = ""ROND"";
  break;
  case ""SKF"":
  OutgoingUnitCode = ""SKF"";
  break;
  case ""TB"":
  OutgoingUnitCode = ""TB"";
  break;
  case ""XXX"":
  OutgoingUnitCode = ""XXX"";
  break;
  }
  break;

  default:
  OutgoingUnitCode = IngoingUnitCode;
  break;
  }

  if (OutgoingUnitCode == """")
  {
  OutgoingUnitCode = IngoingUnitCode;
  }

  return OutgoingUnitCode;
  }
       
]]>
  </msxsl:script>


</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"BYGE.Integration.EDIFACTInvoice.EFACT_D96A_INVOIC";
        
        private const global::BYGE.Integration.EDIFACTInvoice.EFACT_D96A_INVOIC _srcSchemaTypeReference0 = null;
        
        private const string _strTrgSchemasList0 = @"BYGE.Integration.Core.Invoice.Invoice";
        
        private const global::BYGE.Integration.Core.Invoice.Invoice _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [1];
                _SrcSchemas[0] = @"BYGE.Integration.EDIFACTInvoice.EFACT_D96A_INVOIC";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"BYGE.Integration.Core.Invoice.Invoice";
                return _TrgSchemas;
            }
        }
    }
}
