<?xml version="1.0" encoding="UTF-16"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:var="http://schemas.microsoft.com/BizTalk/2003/var" exclude-result-prefixes="msxsl var s0 userCSharp" version="1.0" xmlns:s0="http://byg-e.dk/schemas/v10" xmlns:ns0="http://schemas.microsoft.com/BizTalk/EDI/EDIFACT/2006" xmlns:userCSharp="http://schemas.microsoft.com/BizTalk/2003/userCSharp">
  <xsl:output omit-xml-declaration="yes" method="xml" version="1.0" />
  <xsl:template match="/">
    <xsl:apply-templates select="/s0:Invoice" />
  </xsl:template>
  <xsl:template match="/s0:Invoice">
    <xsl:variable name="var:v2" select="userCSharp:LogicalExistence(boolean(OrderReference/OrderDate))" />
    <ns0:EFACT_D96A_INVOIC>
      <!-- *** Global variables  *** --> 
      <xsl:variable name="MOA_124_TaxTotal">
        <xsl:value-of select="TaxTotal/TaxSubtotal[TaxCategory/TaxScheme/Navn='Moms']/TaxAmount/text()" />
      </xsl:variable>
      <xsl:variable name="MOA_125_TaxTotal">
        <xsl:value-of select="TaxTotal/TaxSubtotal[TaxCategory/TaxScheme/Navn='Moms']/TaxableAmount/text()" />
      </xsl:variable>
      <xsl:variable name="Days" select="userCSharp:DaysPassedFromStartDate(IssueDate/text(), PaymentMeans/PaymentDueDate/text())">
      </xsl:variable>
        
        <!-- <xsl:variable name="IssueDate">
          <xsl:value-of select="IssueDate/text()" />
         <xsl:value-of select="xs:date(IssueDate/text())" />
        </xsl:variable>
        <xsl:variable name="PaymentDueDate">
          <xsl:value-of select="PaymentMeans/PaymentDueDate/text()" />
        </xsl:variable> -->
<!--      </xsl:if>  -->
      
      
     <!-- UNH -->   
      <UNH>
        <UNH1>
          <xsl:text>1</xsl:text>
        </UNH1>
        <UNH2>
          <UNH2.1>
            <xsl:text>INVOIC</xsl:text>
          </UNH2.1>
          <UNH2.2>
            <xsl:text>D</xsl:text>
          </UNH2.2>
          <UNH2.3>
            <xsl:text>96A</xsl:text>
          </UNH2.3>
          <UNH2.4>
            <xsl:text>UN</xsl:text>
          </UNH2.4>
          <UNH2.5>
            <xsl:text>EAN008</xsl:text>
          </UNH2.5>
        </UNH2>
      </UNH>
     <!-- END  OF  UNH --> 
      
     <!-- BGM SEGMENT --> 
      <ns0:BGM>
        <ns0:C002>
          <C00201>
           <xsl:if test="string-length(InvoiceType) != 0">
            <xsl:value-of select="InvoiceType/text()" />
            </xsl:if> 
          <xsl:if test="string-length(InvoiceType) = 0">
            <xsl:text>380</xsl:text>
            </xsl:if> 
          </C00201>
        </ns0:C002>
        <BGM02>
          <xsl:value-of select="InvoiceID/text()" />
        </BGM02>
      </ns0:BGM>
     <!-- End of BGM  --> 
     
     <!-- DTM SEGMENT --> 
      <ns0:DTM>
        <ns0:C507>
          <C50701>
            <xsl:text>137</xsl:text>
          </C50701>
          <C50702>
            <!--<xsl:value-of select="IssueDate/text()" />-->
          <xsl:value-of select="concat(substring(IssueDate/text(), 1, 4), substring(IssueDate/text(), 6, 2), substring(IssueDate/text(), 9, 2))" /> 
          </C50702>
          <C50703>
            <xsl:text>102</xsl:text>
          </C50703>
        </ns0:C507>
      </ns0:DTM>

      <xsl:for-each select="DTMReference/Node">
       <ns0:DTM> 
        <ns0:C507>         
         <C50701>
            <xsl:value-of select="Code/text()" /> 
          </C50701>
         <C50702> 
          <xsl:value-of select="concat(substring(TimeStamp/text(), 1, 4), substring(TimeStamp/text(), 6, 2), substring(TimeStamp/text(), 9, 2))" /> 
          </C50702>
          <C50703>
            <xsl:text>102</xsl:text>
          </C50703>
        </ns0:C507>
       </ns0:DTM> 
      </xsl:for-each>    
     <!-- End of DTM  --> 

  <!--  FTX SEGMENT -->      
      <xsl:for-each select="NoteLoop/Node">
        <FTX>
         <xsl:if test="string-length(NoteQualifier) != 0">
           <FTX01>
             <xsl:value-of select="NoteQualifier/text()" />           
           </FTX01>         
         </xsl:if>          
         <xsl:if test="string-length(NoteFunction) != 0">
           <FTX02>
             <xsl:value-of select="NoteFunction/text()" />           
           </FTX02>         
         </xsl:if> 
         <xsl:if test="string-length(NoteLanguage) != 0">
           <FTX05>
             <xsl:value-of select="NoteLanguage/text()" />           
           </FTX05>         
         </xsl:if>
          <ns0:C108>
            <C10801>
              <xsl:value-of select="Note/text()" />                       
            </C10801>          
          </ns0:C108>       
        </FTX>      
      </xsl:for-each>       
  <!-- END OF  FTX -->         

  <!-- RFF  SEGMENT -->
       <xsl:if test="string-length(OrderReference/OrderID) != 0">
        <ns0:RFFLoop1>
          <ns0:RFF>
            <ns0:C506>
              <C50601>
               <xsl:text>ON</xsl:text> 
              </C50601>
              <C50602>
                <xsl:value-of select="normalize-space(translate(OrderReference/OrderID/text(), '—', '-'))" />
              </C50602>
            </ns0:C506>
          </ns0:RFF>
          <xsl:if test="string-length(OrderReference/OrderDate) != 0">
           <ns0:DTM_2>
            <ns0:C507_2>            
             <C50701>
              <xsl:value-of select="171" />
             </C50701>            
             <C50702>            
              <xsl:value-of select="concat(substring(OrderReference/OrderDate/text(), 1, 4), substring(OrderReference/OrderDate/text(), 6, 2), substring(OrderReference/OrderDate/text(), 9, 2))" />
              </C50702>            
              <C50703>
               <xsl:value-of select="102" />
              </C50703>                                    
             </ns0:C507_2>            
            </ns0:DTM_2>          
           </xsl:if>                
         </ns0:RFFLoop1> 
        </xsl:if> 

       <xsl:if test="string-length(OrderReference/SalesOrderID) != 0">
        <ns0:RFFLoop1>
          <ns0:RFF>
            <ns0:C506>
              <C50601>
               <xsl:text>VN</xsl:text> 
              </C50601>
              <C50602>
                <xsl:value-of select="OrderReference/SalesOrderID/text()" />
              </C50602>
            </ns0:C506>
          </ns0:RFF>
          <xsl:if test="string-length(OrderReference/SalesOrderDate) != 0">
           <ns0:DTM_2>
            <ns0:C507_2>            
             <C50701>
              <xsl:value-of select="171" />
             </C50701>            
             <C50702>
               <xsl:value-of select="concat(substring(OrderReference/SalesOrderDate/text(), 1, 4), substring(OrderReference/SalesOrderDate/text(), 6, 2), substring(OrderReference/SalesOrderDate/text(), 9, 2))" /> 
               <!--  <xsl:text>zzz</xsl:text> -->
              </C50702>            
              <C50703>
               <xsl:value-of select="102" />
              </C50703>                                    
             </ns0:C507_2>            
            </ns0:DTM_2>          
           </xsl:if>    
         </ns0:RFFLoop1> 
        </xsl:if> 
       
      <xsl:if test="string-length(DespatchDocumentReference/ID) != 0">
        <ns0:RFFLoop1>
          <ns0:RFF>
            <ns0:C506>
              <C50601>
               <xsl:text>AAU</xsl:text> 
              </C50601>
              <C50602>
                <xsl:value-of select="DespatchDocumentReference/ID/text()" />
              </C50602>
            </ns0:C506>
          </ns0:RFF>
         </ns0:RFFLoop1> 
        </xsl:if>     
      
      <xsl:if test="string-length(BillingReference/InvoiceDocumentReference/ID) != 0">
        <ns0:RFFLoop1>
          <ns0:RFF>
            <ns0:C506>
              <C50601>
               <xsl:text>IV</xsl:text> 
              </C50601>
              <C50602>
                <xsl:value-of select="BillingReference/InvoiceDocumentReference/ID/text()" />
              </C50602>
            </ns0:C506>
          </ns0:RFF>
         </ns0:RFFLoop1> 
        </xsl:if>       
      
      <xsl:for-each select="ExtraOrderReference/Node">
       <ns0:RFFLoop1>
         <ns0:RFF>        
         <ns0:C506>         
         <C50601>
            <xsl:value-of select="Code/text()" /> 
          </C50601>
         <C50602> 
            <xsl:value-of select="Reference/text()" />         
          </C50602>
        </ns0:C506>
        </ns0:RFF>
       </ns0:RFFLoop1>  
      </xsl:for-each>  
     <!-- End of  RFF --> 
      
     <!-- NAD SEGMENT -->
      
     <!-- NAD+SU  SEGMENT -->   
      <ns0:NADLoop1>
<!--        <ns0:NAD>
          <xsl:variable name="var:v3" select="userCSharp:ReturnSU(string(AccountingSupplier/AccSellerID/text()))" />
          <NAD01>
            <xsl:value-of select="$var:v3" />
          </NAD01> -->
          <ns0:NAD>          
           <NAD01>
            <xsl:text>SU</xsl:text>
           </NAD01>
           <ns0:C082>
            <C08201>
              <xsl:if test="string-length(AccountingSupplier/PartyID) != 0">
               <xsl:value-of select="AccountingSupplier/PartyID/text()" />
              </xsl:if> 
               <xsl:if test="string-length(AccountingSupplier/PartyID) = 0">
           <!--    <xsl:value-of select="AccountingSupplier/AccSellerID/text()" /-->
                 <xsl:text>ZZZ</xsl:text>
              </xsl:if>               
            </C08201>
            <C08203>
              <xsl:if test="string-length(AccountingSupplier/SchemeID/Endpoint) != 0">
                <xsl:if test="AccountingSupplier/SchemeID/Endpoint/text() = 'GLN'">
                                <xsl:text>9</xsl:text>
               </xsl:if> 
                <xsl:if test="AccountingSupplier/SchemeID/Endpoint/text() != 'GLN'">
                  <!--    <xsl:value-of select="AccountingSupplier/SchemeID/Endpoint/text()" /> -->
                  <xsl:text>ZZZ</xsl:text>
                </xsl:if>
              </xsl:if>  
             <xsl:if test="string-length(AccountingSupplier/SchemeID/Endpoint) = 0"> 
              <xsl:text>9</xsl:text>
               </xsl:if> 
            </C08203>
          </ns0:C082>
          <xsl:if test="string-length(AccountingSupplier/Address/Name) != 0">
          <ns0:C080>
            
              <C08001>
                <xsl:value-of select="AccountingSupplier/Address/Name/text()" />
              </C08001>
            
          </ns0:C080>
          </xsl:if>
          <xsl:if test="string-length(AccountingSupplier/Address/Street) != 0">
          <ns0:C059>
            
              <C05901>
                <xsl:value-of select="AccountingSupplier/Address/Street/text()" />
              </C05901>
            
          </ns0:C059>
        </xsl:if>                
          <xsl:if test="string-length(AccountingSupplier/Address/City) != 0">
            <NAD06>
              <xsl:value-of select="AccountingSupplier/Address/City/text()" />
            </NAD06>
          </xsl:if>
          <xsl:if test="string-length(AccountingSupplier/Address/PostalCode) != 0">
            <NAD08>
              <xsl:value-of select="AccountingSupplier/Address/PostalCode/text()" />
            </NAD08>
          </xsl:if>
          <xsl:if test="string-length(AccountingSupplier/Address/Country) != 0">
            <NAD09>
              <xsl:value-of select="AccountingSupplier/Address/Country/text()" />
            </NAD09>
          </xsl:if>
        </ns0:NAD>
        
            <xsl:if test="string-length(//PaymentMeans/PaymentID) != 0 and string-length(//PaymentMeans/InstructionID) != 0 and string-length(//PaymentMeans/CreditAccountID) != 0">
           <!--  <ns0:FII>
              <FII01>
                <xsl:text>RB</xsl:text>
              </FII01>
               
               <ns0:C078_2>
                 <C07801>
                   <xsl:value-of select="//PaymentMeans//CreditAccountID/text()" />
                 </C07801>
               </ns0:C078_2>
               
              </ns0:FII> -->
             <ns0:RFFLoop2>
               <RFF_2>
                 <C506_2>
                   <C50601>
                     <xsl:text>PQ</xsl:text>
                   </C50601>
                   <C50602>
                     <xsl:value-of select="concat('+', //PaymentMeans/PaymentID/text(), '&lt;', //PaymentMeans/InstructionID/text(), '+', //PaymentMeans//CreditAccountID/text(), '&lt;')" />                   
                   </C50602>
       <!--            <C50602>
                     <xsl:value-of select="//PaymentMeans/InstructionID/text()" />
                   </C50602>
                   <xsl:if test="string-length(//PaymentMeans/PaymentID) != 0">
                     <C50603>
                       <xsl:value-of select="//PaymentMeans/PaymentID/text()" />
                     </C50603> 
                   </xsl:if>  -->
                 </C506_2>               
               </RFF_2>             
             </ns0:RFFLoop2>
          </xsl:if>    

      <xsl:if test="string-length(AccountingSupplier/FinancialInstitution/CodeQualifier) != 0">
       <ns0:FII>
   
        <FII01>
         <xsl:value-of select="AccountingSupplier/FinancialInstitution/CodeQualifier/text()" />          
        </FII01>

        <xsl:if test="string-length(AccountingSupplier/FinancialInstitution/AccountHolderIdentifier) != 0 or string-length(AccountingSupplier/FinancialInstitution/AccountHolderName) != 0 or string-length(AccountingSupplier/FinancialInstitution/CurrencyIDCode) != 0">
         <ns0:C078>
         
          <xsl:if test="string-length(AccountingSupplier/FinancialInstitution/AccountHolderIdentifier) != 0">
           <C07801>
            <xsl:value-of select="AccountingSupplier/FinancialInstitution/AccountHolderIdentifier/text()" />             
           </C07801>           
          </xsl:if>                   
          <xsl:if test="string-length(AccountingSupplier/FinancialInstitution/AccountHolderName) != 0">
           <C07802>
            <xsl:value-of select="AccountingSupplier/FinancialInstitution/AccountHolderName/text()" />             
           </C07802>           
          </xsl:if>
          <xsl:if test="string-length(AccountingSupplier/FinancialInstitution/CurrencyIDCode) != 0">
           <C07804>
            <xsl:value-of select="AccountingSupplier/FinancialInstitution/CurrencyIDCode/text()" />             
           </C07804>           
          </xsl:if>            
         
         </ns0:C078>         
        </xsl:if>

        <xsl:if test="string-length(AccountingSupplier/FinancialInstitution/InstitutionNameCode) != 0 or string-length(AccountingSupplier/FinancialInstitution/CodeListIDCode) != 0 or string-length(AccountingSupplier/FinancialInstitution/CodeListResponsibleAgencyCode) != 0 or string-length(AccountingSupplier/FinancialInstitution/InstitutionBranchIdentifier) != 0 or string-length(AccountingSupplier/FinancialInstitution/InstitutionName) != 0 or string-length(AccountingSupplier/FinancialInstitution/InstitutionBranchLocationName) != 0">
         <ns0:C088>
           
          <xsl:if test="string-length(AccountingSupplier/FinancialInstitution/InstitutionNameCode) != 0">
           <C08801>
            <xsl:value-of select="AccountingSupplier/FinancialInstitution/InstitutionNameCode/text()" />             
           </C08801>           
          </xsl:if> 
          <xsl:if test="string-length(AccountingSupplier/FinancialInstitution/CodeListIDCode) != 0">
           <C08802>
            <xsl:value-of select="AccountingSupplier/FinancialInstitution/CodeListIDCode/text()" />             
           </C08802>           
          </xsl:if> 
          <xsl:if test="string-length(AccountingSupplier/FinancialInstitution/CodeListResponsibleAgencyCode) != 0">
           <C08803>
            <xsl:value-of select="AccountingSupplier/FinancialInstitution/CodeListResponsibleAgencyCode/text()" />             
           </C08803>           
          </xsl:if>    
          <xsl:if test="string-length(AccountingSupplier/FinancialInstitution/InstitutionBranchIdentifier) != 0">
           <C08804>
            <xsl:value-of select="AccountingSupplier/FinancialInstitution/InstitutionBranchIdentifier/text()" />             
           </C08804>           
          </xsl:if>    
          <xsl:if test="string-length(AccountingSupplier/FinancialInstitution/InstitutionName) != 0">
           <C08807>
            <xsl:value-of select="AccountingSupplier/FinancialInstitution/InstitutionName/text()" />             
           </C08807>           
          </xsl:if>            
          <xsl:if test="string-length(AccountingSupplier/FinancialInstitution/InstitutionBranchLocationName) != 0">
           <C08808>
            <xsl:value-of select="AccountingSupplier/FinancialInstitution/InstitutionBranchLocationName/text()" />             
           </C08808>           
          </xsl:if>  
         
         </ns0:C088>
        </xsl:if>  
          
       </ns0:FII> 
      </xsl:if>
      
         <xsl:for-each select="AccountingSupplier/RFFNADLoop/Node">
          <ns0:RFFLoop2>
            <ns0:RFF_2>
              <ns0:C506_2>
                <C50601>
                  <xsl:value-of select="Code/text()" />
                </C50601>
                <C50602>
                  <xsl:value-of select="Reference/text()" />
                </C50602>
              </ns0:C506_2>
            </ns0:RFF_2>
          </ns0:RFFLoop2>
        </xsl:for-each>
          
       <xsl:if test="string-length(AccountingSupplier/Concact/ID) != 0">
        <ns0:CTALoop1>
          <ns0:CTA>
            <CTA01>              
             <xsl:if test="(string-length(AccountingSupplier/Concact/ID) &lt; 3 or AccountingSupplier/Concact/ID/text() = 'ZZZ') and string(number(AccountingSupplier/Concact/ID)) = 'NaN'">
             <xsl:value-of select="AccountingSupplier/Concact/ID/text()" />
               </xsl:if> 
            <xsl:if test="(string-length(AccountingSupplier/Concact/ID) &gt;= 3 and AccountingSupplier/Concact/ID/text() != 'ZZZ') or string(number(AccountingSupplier/Concact/ID)) != 'NaN'">
              <xsl:text>OC</xsl:text>
             </xsl:if> 
            </CTA01>
            <xsl:if test="string-length(AccountingSupplier/Concact/Code) != 0 or string-length(AccountingSupplier/Concact/Name) != 0">
              <ns0:C056>
                <xsl:if test="string-length(AccountingSupplier/Concact/Code) != 0">
                  <C05601>
                    <xsl:value-of select="AccountingSupplier/Concact/Code/text()" />
                  </C05601>
                </xsl:if>
                <xsl:if test="string-length(AccountingSupplier/Concact/Name) != 0">
                  <C05602>
                    <xsl:value-of select="AccountingSupplier/Concact/Name/text()" />
                  </C05602>
                </xsl:if>   
              </ns0:C056>
            </xsl:if>  
           </ns0:CTA>            
                              
            <xsl:if test="string-length(AccountingSupplier/Concact/E-mail) != 0">
              <ns0:COM>
              <ns0:C076>
                <C07601>
                  <xsl:value-of select="AccountingSupplier/Concact/E-mail/text()" />                              
               </C07601> 
                <C07602>
                  <xsl:text>EM</xsl:text>                
                </C07602>              
              </ns0:C076>
             </ns0:COM>   
            </xsl:if>                      

            <xsl:if test="string-length(AccountingSupplier/Concact/Telephone) != 0">
              <ns0:COM>
              <ns0:C076>
                <C07601>
                  <xsl:value-of select="AccountingSupplier/Concact/Telephone/text()" />                              
               </C07601>             
                <C07602>
                  <xsl:text>TE</xsl:text>                
                </C07602>              
              </ns0:C076>
                </ns0:COM>   
            </xsl:if>                      
          
        <xsl:if test="string-length(AccountingSupplier/Concact/Telefax) != 0">
          <ns0:COM>
              <ns0:C076>
                <C07601>
                  <xsl:value-of select="AccountingSupplier/Concact/Telefax/text()" />                              
               </C07601>             
                <C07602>
                  <xsl:text>FX</xsl:text>                
                </C07602>              
              </ns0:C076>
            </ns0:COM>
            </xsl:if>                      
        </ns0:CTALoop1>       
      </xsl:if>                        
     </ns0:NADLoop1>
   <!-- End of  NAD+SU  -->
      
   <!-- NAD+IV  SEGMENT -->      
         <ns0:NADLoop1>
<!--        <ns0:NAD>
          <xsl:variable name="var:v4" select="userCSharp:ReturnIV(string(AccountingCustomer/AccBuyerID/text()))" />
          <NAD01>
            <xsl:value-of select="$var:v4" />
          </NAD01> -->
          <ns0:NAD>          
           <NAD01>
            <xsl:text>IV</xsl:text>
           </NAD01>      
          <ns0:C082>
            <C08201>
              <xsl:if test="string-length(AccountingCustomer/PartyID) != 0">
                <xsl:value-of select="AccountingCustomer/PartyID/text()" />
              </xsl:if>
              <xsl:if test="string-length(AccountingCustomer/PartyID) = 0">
                <xsl:value-of select="AccountingCustomer/AccSellerID/text()" />
              </xsl:if>
            </C08201>
            <C08203>
              <xsl:if test="string-length(AccountingCustomer/SchemeID/Endpoint) != 0">
                <xsl:if test="AccountingCustomer/SchemeID/Endpoint/text() = 'GLN'">
                                <xsl:text>9</xsl:text>
                </xsl:if>
                <xsl:if test="AccountingCustomer/SchemeID/Endpoint/text() != 'GLN'">
             <!--   <xsl:value-of select="AccountingCustomer/SchemeID/Endpoint/text()" /> -->
                  <xsl:text>ZZZ</xsl:text>
                </xsl:if>
              </xsl:if>              
              <xsl:if test="string-length(AccountingCustomer/SchemeID/Endpoint) = 0">
              <xsl:text>9</xsl:text>
                </xsl:if> 
            </C08203>
          </ns0:C082>
           <xsl:if test="string-length(AccountingCustomer/Address/Name) != 0">
             <ns0:C080>
            <xsl:if test="string-length(substring-before(AccountingCustomer/Address/Name, ':')) = 0">             
                

                  <C08001>
                    <xsl:value-of select="AccountingCustomer/Address/Name/text()" />
                  </C08001>
                  </xsl:if>
                  <xsl:if test="string-length(substring-before(AccountingCustomer/Address/Name, ':')) != 0">
                                      <C08001>
                    <xsl:value-of select="substring-before(AccountingCustomer/Address/Name/text(), ':')" />
                  </C08001>
                    
                                                          <C08002>
                    <xsl:value-of select="substring-after(AccountingCustomer/Address/Name/text(), ':')" />
                  </C08002>
                    
                  
                  </xsl:if>
                </ns0:C080>
              
            </xsl:if>
          <xsl:if test="string-length(AccountingCustomer/Address/Street) != 0">
          <ns0:C059>
            
              <C05901>
                <xsl:value-of select="AccountingCustomer/Address/Street/text()" />
              </C05901>
            
          </ns0:C059>
          </xsl:if>
          <xsl:if test="string-length(AccountingCustomer/Address/City) != 0">
            <NAD06>
              <xsl:value-of select="AccountingCustomer/Address/City/text()" />
            </NAD06>
          </xsl:if>
          <xsl:if test="string-length(AccountingCustomer/Address/PostalCode) != 0">
            <NAD08>
              <xsl:value-of select="AccountingCustomer/Address/PostalCode/text()" />
            </NAD08>
          </xsl:if>
          <xsl:if test="string-length(AccountingCustomer/Address/Country) != 0">
            <NAD09>
              <xsl:value-of select="AccountingCustomer/Address/Country/text()" />
            </NAD09>
          </xsl:if>
        </ns0:NAD>

      <xsl:if test="string-length(AccountingCustomer/FinancialInstitution/CodeQualifier) != 0">
       <ns0:FII>
   
        <FII01>
         <xsl:value-of select="AccountingCustomer/FinancialInstitution/CodeQualifier/text()" />          
        </FII01>

        <xsl:if test="string-length(AccountingCustomer/FinancialInstitution/AccountHolderIdentifier) != 0 or string-length(AccountingCustomer/FinancialInstitution/AccountHolderName) != 0 or string-length(AccountingCustomer/FinancialInstitution/CurrencyIDCode) != 0">
         <ns0:C078>
         
          <xsl:if test="string-length(AccountingCustomer/FinancialInstitution/AccountHolderIdentifier) != 0">
           <C07801>
            <xsl:value-of select="AccountingCustomer/FinancialInstitution/AccountHolderIdentifier/text()" />             
           </C07801>           
          </xsl:if>                   
          <xsl:if test="string-length(AccountingCustomer/FinancialInstitution/AccountHolderName) != 0">
           <C07802>
            <xsl:value-of select="AccountingCustomer/FinancialInstitution/AccountHolderName/text()" />             
           </C07802>           
          </xsl:if>
          <xsl:if test="string-length(AccountingCustomer/FinancialInstitution/CurrencyIDCode) != 0">
           <C07804>
            <xsl:value-of select="AccountingCustomer/FinancialInstitution/CurrencyIDCode/text()" />             
           </C07804>           
          </xsl:if>            
         
         </ns0:C078>         
        </xsl:if>

        <xsl:if test="string-length(AccountingCustomer/FinancialInstitution/InstitutionNameCode) != 0 or string-length(AccountingCustomer/FinancialInstitution/CodeListIDCode) != 0 or string-length(AccountingCustomer/FinancialInstitution/CodeListResponsibleAgencyCode) != 0 or string-length(AccountingCustomer/FinancialInstitution/InstitutionBranchIdentifier) != 0 or string-length(AccountingCustomer/FinancialInstitution/InstitutionName) != 0 or string-length(AccountingCustomer/FinancialInstitution/InstitutionBranchLocationName) != 0">
         <ns0:C088>
           
          <xsl:if test="string-length(AccountingCustomer/FinancialInstitution/InstitutionNameCode) != 0">
           <C08801>
            <xsl:value-of select="AccountingCustomer/FinancialInstitution/InstitutionNameCode/text()" />             
           </C08801>           
          </xsl:if> 
          <xsl:if test="string-length(AccountingCustomer/FinancialInstitution/CodeListIDCode) != 0">
           <C08802>
            <xsl:value-of select="AccountingCustomer/FinancialInstitution/CodeListIDCode/text()" />             
           </C08802>           
          </xsl:if> 
          <xsl:if test="string-length(AccountingCustomer/FinancialInstitution/CodeListResponsibleAgencyCode) != 0">
           <C08803>
            <xsl:value-of select="AccountingCustomer/FinancialInstitution/CodeListResponsibleAgencyCode/text()" />             
           </C08803>           
          </xsl:if>    
          <xsl:if test="string-length(AccountingCustomer/FinancialInstitution/InstitutionBranchIdentifier) != 0">
           <C08804>
            <xsl:value-of select="AccountingCustomer/FinancialInstitution/InstitutionBranchIdentifier/text()" />             
           </C08804>           
          </xsl:if>    
          <xsl:if test="string-length(AccountingCustomer/FinancialInstitution/InstitutionName) != 0">
           <C08807>
            <xsl:value-of select="AccountingCustomer/FinancialInstitution/InstitutionName/text()" />             
           </C08807>           
          </xsl:if>            
          <xsl:if test="string-length(AccountingCustomer/FinancialInstitution/InstitutionBranchLocationName) != 0">
           <C08808>
            <xsl:value-of select="AccountingCustomer/FinancialInstitution/InstitutionBranchLocationName/text()" />             
           </C08808>           
          </xsl:if>  
         
         </ns0:C088>
        </xsl:if>  
          
       </ns0:FII> 
      </xsl:if>
           
         <xsl:for-each select="AccountingCustomer/RFFNADLoop/Node">
          <ns0:RFFLoop2>
            <ns0:RFF_2>
              <ns0:C506_2>
                <C50601>
                  <xsl:value-of select="Code/text()" />
                </C50601>
                <C50602>
                  <xsl:value-of select="Reference/text()" />
                </C50602>
              </ns0:C506_2>
            </ns0:RFF_2>
          </ns0:RFFLoop2>
        </xsl:for-each>
              
       <xsl:if test="string-length(AccountingCustomer/Concact/ID) != 0">
        <ns0:CTALoop1>
          <ns0:CTA>
            <CTA01>              
             <xsl:if test="(string-length(AccountingCustomer/Concact/ID) &lt; 3 or AccountingCustomer/Concact/ID/text() = 'ZZZ') and string(number(AccountingCustomer/Concact/ID)) = 'NaN'">
             <xsl:value-of select="AccountingCustomer/Concact/ID/text()" />
               </xsl:if> 
            <xsl:if test="(string-length(AccountingCustomer/Concact/ID) &gt;= 3 and AccountingCustomer/Concact/ID/text() != 'ZZZ') or string(number(AccountingCustomer/Concact/ID)) != 'NaN'">
              <xsl:text>OC</xsl:text>
             </xsl:if>        
            </CTA01>
            <xsl:if test="string-length(AccountingCustomer/Concact/Code) != 0 or string-length(AccountingCustomer/Concact/Name) != 0">
              <ns0:C056>
                <xsl:if test="string-length(AccountingCustomer/Concact/Code) != 0">
                  <C05601>
                    <xsl:value-of select="AccountingCustomer/Concact/Code/text()" />
                  </C05601>
                </xsl:if>
                <xsl:if test="string-length(AccountingCustomer/Concact/Name) != 0">
                  <C05602>
                    <xsl:value-of select="AccountingCustomer/Concact/Name/text()" />
                  </C05602>
                </xsl:if>
              </ns0:C056>
            </xsl:if>
          </ns0:CTA>            
          
            <xsl:if test="string-length(AccountingCustomer/Concact/E-mail) != 0">
              <ns0:COM>
              <ns0:C076>
                <C07601>
                  <xsl:value-of select="AccountingCustomer/Concact/E-mail/text()" />                              
               </C07601> 
                <C07602>
                  <xsl:text>EM</xsl:text>                
                </C07602>              
              </ns0:C076>
                </ns0:COM>
            </xsl:if>                      

            <xsl:if test="string-length(AccountingCustomer/Concact/Telephone) != 0">
              <ns0:COM>
              <ns0:C076>
                <C07601>
                  <xsl:value-of select="AccountingCustomer/Concact/Telephone/text()" />                              
               </C07601>             
                <C07602>
                  <xsl:text>TE</xsl:text>                
                </C07602>              
              </ns0:C076>
                </ns0:COM>  
            </xsl:if>                      
          
        <xsl:if test=" string-length(AccountingCustomer/Concact/Telefax) != 0">
          <ns0:COM>
              <ns0:C076>
                <C07601>
                  <xsl:value-of select="AccountingCustomer/Concact/Telefax/text()" />                              
               </C07601>             
                <C07602>
                  <xsl:text>FX</xsl:text>                
                </C07602>              
              </ns0:C076>                            
                       </ns0:COM>
            </xsl:if>                              
        </ns0:CTALoop1>        
      </xsl:if>                  
     </ns0:NADLoop1>
    <!--  End of  NAD+IV  -->
      
    <!--     SEGMENT -->  
      <ns0:NADLoop1>
<!--        <ns0:NAD>
          <xsl:variable name="var:v5" select="userCSharp:ReturnBY(string(BuyerCustomer/PartyID/text()))" />
          <NAD01>
            <xsl:value-of select="$var:v5" />
          </NAD01> -->
          <ns0:NAD>          
           <NAD01>
            <xsl:text>BY</xsl:text>
           </NAD01> 
          <ns0:C082>
              <C08201>
                <xsl:if test="string-length(BuyerCustomer/PartyID) != 0">
                  <xsl:value-of select="BuyerCustomer/PartyID/text()" />
                </xsl:if>
                <xsl:if test="string-length(BuyerCustomer/PartyID) = 0">
                  <xsl:value-of select="BuyerCustomer/BuyerID/text()" />
                </xsl:if>
              </C08201>
            <C08203>
                              <xsl:if test="string-length(BuyerCustomer/SchemeID/Endpoint) != 0">
                                <xsl:if test="BuyerCustomer/SchemeID/Endpoint/text() = 'GLN'">
                                  <xsl:text>9</xsl:text>
                </xsl:if>
                                <xsl:if test="BuyerCustomer/SchemeID/Endpoint/text() != 'GLN'">
                <!--! <xsl:value-of select="BuyerCustomer/SchemeID/Endpoint/text()" /> -->
                                  <xsl:text>ZZZ</xsl:text>
                                </xsl:if>
                </xsl:if>
              <xsl:if test="string-length(BuyerCustomer/SchemeID/Endpoint) = 0">
              <xsl:text>9</xsl:text>
                </xsl:if>
            </C08203>
          </ns0:C082>
          <xsl:if test="string-length(BuyerCustomer/Address/Name) != 0">
          <ns0:C080>
            
            <C08001>
              <xsl:value-of select="substring(normalize-space(BuyerCustomer/Address/Name/text()), 1, 35)" />
            </C08001>

            <xsl:if test="string-length(substring(normalize-space(BuyerCustomer/Address/Name/text()), 36, 70)) != 0">
              <C08002>
                <xsl:value-of select="substring(normalize-space(BuyerCustomer/Address/Name/text()), 36, 70)" />
              
              </C08002>
            
            </xsl:if>  
          
          </ns0:C080>
          </xsl:if>
          <xsl:if test="string-length(BuyerCustomer/Address/Street) != 0">
          <ns0:C059>
            
              <C05901>
                <xsl:value-of select="BuyerCustomer/Address/Street/text()" />
              </C05901>
            
          </ns0:C059>
          </xsl:if>
          <xsl:if test="string-length(BuyerCustomer/Address/City) != 0">
            <NAD06>
              <xsl:value-of select="BuyerCustomer/Address/City/text()" />
            </NAD06>
          </xsl:if>
          <xsl:if test="string-length(BuyerCustomer/Address/PostalCode) != 0">
            <NAD08>
              <xsl:value-of select="BuyerCustomer/Address/PostalCode/text()" />
            </NAD08>
          </xsl:if>
          <xsl:if test="string-length(BuyerCustomer/Address/Country) != 0">
            <NAD09>
              <xsl:value-of select="BuyerCustomer/Address/Country/text()" />
            </NAD09>
          </xsl:if>
        </ns0:NAD>    
        
      <xsl:if test="string-length(BuyerCustomer/FinancialInstitution/CodeQualifier) != 0">
       <ns0:FII>
   
        <FII01>
         <xsl:value-of select="BuyerCustomer/FinancialInstitution/CodeQualifier/text()" />          
        </FII01>

        <xsl:if test="string-length(BuyerCustomer/FinancialInstitution/AccountHolderIdentifier) != 0 or string-length(BuyerCustomer/FinancialInstitution/AccountHolderName) != 0 or string-length(BuyerCustomer/FinancialInstitution/CurrencyIDCode) != 0">
         <ns0:C078>
         
          <xsl:if test="string-length(BuyerCustomer/FinancialInstitution/AccountHolderIdentifier) != 0">
           <C07801>
            <xsl:value-of select="BuyerCustomer/FinancialInstitution/AccountHolderIdentifier/text()" />             
           </C07801>           
          </xsl:if>                   
          <xsl:if test="string-length(BuyerCustomer/FinancialInstitution/AccountHolderName) != 0">
           <C07802>
            <xsl:value-of select="BuyerCustomer/FinancialInstitution/AccountHolderName/text()" />             
           </C07802>           
          </xsl:if>
          <xsl:if test="string-length(BuyerCustomer/FinancialInstitution/CurrencyIDCode) != 0">
           <C07804>
            <xsl:value-of select="BuyerCustomer/FinancialInstitution/CurrencyIDCode/text()" />             
           </C07804>           
          </xsl:if>            
         
         </ns0:C078>         
        </xsl:if>

        <xsl:if test="string-length(BuyerCustomer/FinancialInstitution/InstitutionNameCode) != 0 or string-length(BuyerCustomer/FinancialInstitution/CodeListIDCode) != 0 or string-length(BuyerCustomer/FinancialInstitution/CodeListResponsibleAgencyCode) != 0 or string-length(BuyerCustomer/FinancialInstitution/InstitutionBranchIdentifier) != 0 or string-length(BuyerCustomer/FinancialInstitution/InstitutionName) != 0 or string-length(BuyerCustomer/FinancialInstitution/InstitutionBranchLocationName) != 0">
         <ns0:C088>
           
          <xsl:if test="string-length(BuyerCustomer/FinancialInstitution/InstitutionNameCode) != 0">
           <C08801>
            <xsl:value-of select="BuyerCustomer/FinancialInstitution/InstitutionNameCode/text()" />             
           </C08801>           
          </xsl:if> 
          <xsl:if test="string-length(BuyerCustomer/FinancialInstitution/CodeListIDCode) != 0">
           <C08802>
            <xsl:value-of select="BuyerCustomer/FinancialInstitution/CodeListIDCode/text()" />             
           </C08802>           
          </xsl:if> 
          <xsl:if test="string-length(BuyerCustomer/FinancialInstitution/CodeListResponsibleAgencyCode) != 0">
           <C08803>
            <xsl:value-of select="BuyerCustomer/FinancialInstitution/CodeListResponsibleAgencyCode/text()" />             
           </C08803>           
          </xsl:if>    
          <xsl:if test="string-length(BuyerCustomer/FinancialInstitution/InstitutionBranchIdentifier) != 0">
           <C08804>
            <xsl:value-of select="BuyerCustomer/FinancialInstitution/InstitutionBranchIdentifier/text()" />             
           </C08804>           
          </xsl:if>    
          <xsl:if test="string-length(BuyerCustomer/FinancialInstitution/InstitutionName) != 0">
           <C08807>
            <xsl:value-of select="BuyerCustomer/FinancialInstitution/InstitutionName/text()" />             
           </C08807>           
          </xsl:if>            
          <xsl:if test="string-length(BuyerCustomer/FinancialInstitution/InstitutionBranchLocationName) != 0">
           <C08808>
            <xsl:value-of select="BuyerCustomer/FinancialInstitution/InstitutionBranchLocationName/text()" />             
           </C08808>           
          </xsl:if>  
         
         </ns0:C088>
        </xsl:if>  
          
       </ns0:FII> 
      </xsl:if>        
                     
        <xsl:for-each select="BuyerCustomer/RFFNADLoop/Node">
          <ns0:RFFLoop2>
            <ns0:RFF_2>
              <ns0:C506_2>
                <C50601>
                  <xsl:value-of select="Code/text()" />
                </C50601>
                <C50602>
                  <xsl:value-of select="Reference/text()" />
                </C50602>
              </ns0:C506_2>
            </ns0:RFF_2>
          </ns0:RFFLoop2>
        </xsl:for-each> 
        
      <xsl:if test="string-length(BuyerCustomer/Concact/ID) != 0">
        <ns0:CTALoop1>
          <ns0:CTA>
            <CTA01>     
             <xsl:if test="(string-length(BuyerCustomer/Concact/ID) &lt; 3 or BuyerCustomer/Concact/ID/text() = 'ZZZ') and string(number(BuyerCustomer/Concact/ID)) = 'NaN'">
             <xsl:value-of select="BuyerCustomer/Concact/ID/text()" />
               </xsl:if> 
            <xsl:if test="(string-length(BuyerCustomer/Concact/ID) &gt;= 3 and BuyerCustomer/Concact/ID/text() != 'ZZZ') or string(number(BuyerCustomer/Concact/ID)) != 'NaN'">
              <xsl:text>OC</xsl:text>
             </xsl:if> 
            </CTA01>
            <xsl:if test="string-length(BuyerCustomer/Concact/Code) != 0 or string-length(BuyerCustomer/Concact/Name) != 0">
              <ns0:C056>
                <xsl:if test="string-length(BuyerCustomer/Concact/Code) != 0">
                  <C05601>
                    <xsl:value-of select="BuyerCustomer/Concact/Code/text()" />
                  </C05601>
                </xsl:if>
                <xsl:if test="string-length(BuyerCustomer/Concact/Name) != 0">
                  <C05602>
                    <xsl:value-of select="BuyerCustomer/Concact/Name/text()" />
                  </C05602>
                </xsl:if>
              </ns0:C056>
            </xsl:if>
          </ns0:CTA>            
          
            <xsl:if test="string-length(BuyerCustomer/Concact/E-mail) != 0">
             <ns0:COM>
              <ns0:C076>
                <C07601>
                  <xsl:value-of select="BuyerCustomer/Concact/E-mail/text()" />                              
               </C07601>             
                <C07602>
                  <xsl:text>EM</xsl:text>                
                </C07602>              
              </ns0:C076>
             </ns0:COM>                           
            </xsl:if>                      

            <xsl:if test="string-length(BuyerCustomer/Concact/Telephone) != 0">
             <ns0:COM>              
              <ns0:C076>
                <C07601>
                  <xsl:value-of select="BuyerCustomer/Concact/Telephone/text()" />                              
               </C07601>             
                <C07602>
                  <xsl:text>TE</xsl:text>                
                </C07602>              
              </ns0:C076>              
             </ns0:COM>                           
            </xsl:if>                      
          
        <xsl:if test="string-length(BuyerCustomer/Concact/Telefax) != 0">
             <ns0:COM>          
              <ns0:C076>
                <C07601>
                  <xsl:value-of select="BuyerCustomer/Concact/Telefax/text()" />                              
               </C07601>             
                <C07602>
                  <xsl:text>FX</xsl:text>                
                </C07602>                
              </ns0:C076>         
             </ns0:COM>                           
            </xsl:if>              
        
        </ns0:CTALoop1>       
      </xsl:if>                  
     </ns0:NADLoop1>
    <!--  End of NAD+BY --> 
      
    <!--  NAD+DP  SEGMENT -->  
      <xsl:for-each select="DeliveryLocation">
   <!--     <xsl:if test="string-length(PartyID) != 0"> -->
        <ns0:NADLoop1>
          <!--  <ns0:NAD>
            <xsl:variable name="var:v6" select="userCSharp:ReturnDP(string(PartyID/text()))" />
            <NAD01>
              <xsl:value-of select="$var:v6" />
            </NAD01>  -->
          <ns0:NAD>          
           <NAD01>
            <xsl:text>DP</xsl:text>
           </NAD01>           
            <xsl:if test="string-length(PartyID) != 0">
            <ns0:C082>
              
                <C08201>
                  <xsl:value-of select="PartyID/text()" />
                </C08201>
              
              <C08203>
                <xsl:text>9</xsl:text>
              </C08203>
            </ns0:C082>
            </xsl:if>
            
            <!--
           <xsl:if test="string-length(Address/Name) != 0">
             <ns0:C080>
            <xsl:if test="string-length(substring-before(Address/Name, ':')) = 0">             
                

                  <C08001>
                    <xsl:value-of select="Address/Name/text()" />
                  </C08001>
                  </xsl:if>
                  <xsl:if test="string-length(substring-before(Address/Name, ':')) != 0">
                                      <C08001>
                    <xsl:value-of select="substring-before(Address/Name/text(), ':')" />
                  </C08001>
                    
                                                          <C08002>
                    <xsl:value-of select="substring-after(Address/Name/text(), ':')" />
                  </C08002>
                    
                  
                  </xsl:if> 
                </ns0:C080>
              
            </xsl:if>
            -->


            <xsl:if test="string-length(Address/Name) != 0">
              <ns0:C080>

                <C08001>
                  <xsl:value-of select="substring(normalize-space(Address/Name/text()), 1, 35)" />
                </C08001>

                <xsl:if test="string-length(substring(normalize-space(Address/Name/text()), 36, 70)) != 0">
                  <C08002>
                    <xsl:value-of select="substring(normalize-space(Address/Name/text()), 36, 70)" />

                  </C08002>

                </xsl:if>

              </ns0:C080>
            </xsl:if>
            
            
            <xsl:if test="string-length(Address/Street) != 0">
            <ns0:C059>
              
                <C05901>
                  <xsl:value-of select="Address/Street/text()" />
                </C05901>
              
            </ns0:C059>
            </xsl:if>
            <xsl:if test="string-length(Address/City) != 0">
              <NAD06>
                <xsl:value-of select="Address/City/text()" />
              </NAD06>
            </xsl:if>
            <xsl:if test="string-length(Address/PostalCode) != 0">
              <NAD08>
                <xsl:value-of select="Address/PostalCode/text()" />
              </NAD08>
            </xsl:if>
            <xsl:if test="string-length(Address/Country) != 0">
              <NAD09>
                <xsl:value-of select="Address/Country/text()" />
              </NAD09>
            </xsl:if>
          </ns0:NAD>
          
      <xsl:if test="string-length(FinancialInstitution/CodeQualifier) != 0">
       <ns0:FII>
   
        <FII01>
         <xsl:value-of select="FinancialInstitution/CodeQualifier/text()" />          
        </FII01>

        <xsl:if test="string-length(FinancialInstitution/AccountHolderIdentifier) != 0 or string-length(FinancialInstitution/AccountHolderName) != 0 or string-length(FinancialInstitution/CurrencyIDCode) != 0">
         <ns0:C078>
         
          <xsl:if test="string-length(FinancialInstitution/AccountHolderIdentifier) != 0">
           <C07801>
            <xsl:value-of select="FinancialInstitution/AccountHolderIdentifier/text()" />             
           </C07801>           
          </xsl:if>                   
          <xsl:if test="string-length(FinancialInstitution/AccountHolderName) != 0">
           <C07802>
            <xsl:value-of select="FinancialInstitution/AccountHolderName/text()" />             
           </C07802>           
          </xsl:if>
          <xsl:if test="string-length(FinancialInstitution/CurrencyIDCode) != 0">
           <C07804>
            <xsl:value-of select="FinancialInstitution/CurrencyIDCode/text()" />             
           </C07804>           
          </xsl:if>            
         
         </ns0:C078>         
        </xsl:if>

        <xsl:if test="string-length(FinancialInstitution/InstitutionNameCode) != 0 or string-length(FinancialInstitution/CodeListIDCode) != 0 or string-length(FinancialInstitution/CodeListResponsibleAgencyCode) != 0 or string-length(FinancialInstitution/InstitutionBranchIdentifier) != 0 or string-length(FinancialInstitution/InstitutionName) != 0 or string-length(FinancialInstitution/InstitutionBranchLocationName) != 0">
         <ns0:C088>
           
          <xsl:if test="string-length(FinancialInstitution/InstitutionNameCode) != 0">
           <C08801>
            <xsl:value-of select="FinancialInstitution/InstitutionNameCode/text()" />             
           </C08801>           
          </xsl:if> 
          <xsl:if test="string-length(FinancialInstitution/CodeListIDCode) != 0">
           <C08802>
            <xsl:value-of select="FinancialInstitution/CodeListIDCode/text()" />             
           </C08802>           
          </xsl:if> 
          <xsl:if test="string-length(FinancialInstitution/CodeListResponsibleAgencyCode) != 0">
           <C08803>
            <xsl:value-of select="FinancialInstitution/CodeListResponsibleAgencyCode/text()" />             
           </C08803>           
          </xsl:if>    
          <xsl:if test="string-length(FinancialInstitution/InstitutionBranchIdentifier) != 0">
           <C08804>
            <xsl:value-of select="FinancialInstitution/InstitutionBranchIdentifier/text()" />             
           </C08804>           
          </xsl:if>    
          <xsl:if test="string-length(FinancialInstitution/InstitutionName) != 0">
           <C08807>
            <xsl:value-of select="FinancialInstitution/InstitutionName/text()" />             
           </C08807>           
          </xsl:if>            
          <xsl:if test="string-length(FinancialInstitution/InstitutionBranchLocationName) != 0">
           <C08808>
            <xsl:value-of select="FinancialInstitution/InstitutionBranchLocationName/text()" />             
           </C08808>           
          </xsl:if>  
         
         </ns0:C088>
        </xsl:if>  
          
       </ns0:FII> 
      </xsl:if>          
        
       <xsl:if test="string-length(Concact/ID) != 0">
        <ns0:CTALoop1>
          <ns0:CTA>
            <CTA01>              
             <xsl:if test="(string-length(Concact/ID) &lt; 3 or Concact/ID/text() = 'ZZZ') and string(number(Concact/ID)) = 'NaN'">
             <xsl:value-of select="Concact/ID/text()" />
               </xsl:if> 
            <xsl:if test="(string-length(Concact/ID) &gt;= 3 and Concact/ID/text() != 'ZZZ') or string(number(Concact/ID)) != 'NaN'">
              <xsl:text>OC</xsl:text>
             </xsl:if> 
            </CTA01>
            <xsl:if test="string-length(Concact/Code) != 0 or string-length(Concact/Name) != 0">
              <ns0:C056>
                <xsl:if test="string-length(Concact/Code) != 0">
                  <C05601>
                    <xsl:value-of select="Concact/Code/text()" />
                  </C05601>
                </xsl:if>
                <xsl:if test="string-length(Concact/Name) != 0">
                  <C05602>
                    <xsl:value-of select="Concact/Name/text()" />
                  </C05602>
                </xsl:if>
              </ns0:C056>
            </xsl:if>
          </ns0:CTA>            
          
            <xsl:if test="string-length(Concact/E-mail) != 0">
                         <ns0:COM>
              <ns0:C076>
                <C07601>
                  <xsl:value-of select="Concact/E-mail/text()" />                              
               </C07601>             
                <C07602>
                  <xsl:text>EM</xsl:text>                
                </C07602>              
              </ns0:C076>
                                      </ns0:COM>
            </xsl:if>                      

            <xsl:if test="string-length(Concact/Telephone) != 0">
                         <ns0:COM>
              <ns0:C076>
                <C07601>
                  <xsl:value-of select="Concact/Telephone/text()" />                              
               </C07601>             
                <C07602>
                  <xsl:text>TE</xsl:text>                
                </C07602>                
              </ns0:C076>         
                                      </ns0:COM>
            </xsl:if>                      
          
        <xsl:if test="string-length(Concact/Telefax) != 0">
                     <ns0:COM>
              <ns0:C076>
                <C07601>
                  <xsl:value-of select="Concact/Telefax/text()" />                              
               </C07601>             
                <C07602>
                  <xsl:text>FX</xsl:text>                
                </C07602>                
              </ns0:C076>     
                                  </ns0:COM>
            </xsl:if>      
                                    
        </ns0:CTALoop1>
        
      </xsl:if>                  
      
        </ns0:NADLoop1>
 
      </xsl:for-each>
  <!-- End of NAD+DP -->      
  
  <!--  End of NAD  -->
      
      
 <!-- TAX SEGMENT -->     
  <xsl:for-each select="TaxTotal">
   <xsl:for-each select="TaxSubtotal">
 <!--   <xsl:if test="string-length(TaxCategory/ID) != 0"> -->
      <ns0:TAXLoop1>
        <ns0:TAX>
         <xsl:if test="string-length(TaxCode) != 0">
          <TAX01> 
           <xsl:value-of select="TaxCode/text()" />
            </TAX01>
           </xsl:if>
           <xsl:if test="string-length(TaxCode) = 0">
            <TAX01>
             <xsl:text>7</xsl:text>
            </TAX01>
           </xsl:if>

         <xsl:if test="string-length(TaxCategory/TaxScheme/Navn) != 0">
          <ns0:C241>
           <C24101>
            <xsl:choose>
              <xsl:when test="translate(TaxCategory/TaxScheme/Navn, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')='MOMS'">
                <xsl:text>VAT</xsl:text>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="TaxCategory/TaxScheme/Navn/text()" />
              </xsl:otherwise>               
            </xsl:choose>  
           </C24101> 
          </ns0:C241>
         </xsl:if>
            
         <xsl:if test="string-length(TaxCategory/Percent) != 0">  
          <ns0:C243>
           <C24304>
            <xsl:value-of select="TaxCategory/Percent/text()" />
           </C24304>
          </ns0:C243>
         </xsl:if>
          
          <xsl:if test="string-length(TaxCategory/ID) != 0">
           <TAX06>
      <!--       <xsl:value-of select="TaxCategory/TaxScheme/TaxTypeCode/text()" />            -->
        
        
                    <xsl:choose>
                     
                     <xsl:when test="TaxCategory/ID = 'StandardRated'">
                       <xsl:text>S</xsl:text>                   
                     </xsl:when>    
                         
                     <xsl:when test="TaxCategory/ID = 'ZeroRated'">
                       <xsl:text>E</xsl:text>                   
                     </xsl:when>              
                           
                     <xsl:when test="TaxCategory/ID = 'ReverseCharge'">
                       <xsl:text>AE</xsl:text>                   
                     </xsl:when>   
                           
                    <xsl:otherwise>
                      <xsl:text>XZ</xsl:text>                    
                    </xsl:otherwise>                         
        
                           </xsl:choose>
        
     
  
      </TAX06>
     </xsl:if>       
     </ns0:TAX>
        
        <!-- Here we need to add a check if theme MOAs come from TAX or Totals (in Lines as well) -->
        <!--     
  <xsl:if test="string-length(TaxableAmount) != 0 and TaxableAmount != '0.00' and TaxableAmount != '0'">
    <ns0:MOA><ns0:C516>
      <C51601>
        <xsl:text>125</xsl:text>        
      </C51601>
      <C51602>
      <xsl:choose>
       <xsl:when test="TaxableAmount/text() = '0.00'">
        <xsl:text>0</xsl:text>                  
       </xsl:when>  
       <xsl:otherwise>        
        <xsl:value-of select="TaxableAmount/text()" />  
       </xsl:otherwise>                                         
       </xsl:choose> 

      </C51602>
     </ns0:C516>
    </ns0:MOA>     
   </xsl:if>

   <xsl:if test="string-length(TaxAmount) != 0 and TaxAmount != '0.00' and TaxAmount != '0'">
    <ns0:MOA>
     <ns0:C516>
      <C51601>
       <xsl:text>124</xsl:text>
      </C51601>
      <C51602>
      <xsl:choose>
       <xsl:when test="TaxableAmount/text() = '0.00'">
        <xsl:text>0</xsl:text>
       </xsl:when>
       <xsl:otherwise>
        <xsl:value-of select="TaxableAmount/text()" />
       </xsl:otherwise>
      </xsl:choose>
     </C51602>
    </ns0:C516>
   </ns0:MOA>
  </xsl:if>  -->
 </ns0:TAXLoop1>
    <!-- </xsl:if>  -->
   </xsl:for-each>  
  </xsl:for-each>
 <!-- End of  TAX -->     
      
  <!-- CUX SEGMENT-->  
      <xsl:if test="string-length(Currency) != 0">
      <ns0:CUXLoop1>
        <ns0:CUX>
          <ns0:C504>            
              <C50401>
                <xsl:text>2</xsl:text>              
              </C50401>
              <C50402>
                <xsl:value-of select="Currency/text()" />
              </C50402>
            <C50403>
              <xsl:text>4</xsl:text>            
          </C50403>
          </ns0:C504>
        </ns0:CUX>
      </ns0:CUXLoop1>
     </xsl:if>    
    <!--  End of  CUX -->     
      
    <!--  PAT SECTION-->
      <xsl:for-each select="PaymentTerms">
        <ns0:PATLoopt1>
         <ns0:PAT>
          <xsl:if test="string-length(ID) != 0">
           <PAT01>
             <xsl:if test="string-length(//PaymentMeans/PaymentDueDate) != 0">
               <xsl:text>3</xsl:text>
             </xsl:if>
             <xsl:if test="string-length(//PaymentMeans/PaymentDueDate) = 0">
               <xsl:text>1</xsl:text>
               <!-- <xsl:value-of select="ID/text()" /> -->
             </xsl:if> 
           </PAT01>          
          </xsl:if>

            <xsl:if test="string-length(TermsOfPaymentIdentification) != 0 or string-length(CodeListQualifier) != 0 or string-length(CodeListResponsibleAgency) != 0 or string-length(Note) != 0 or string-length(PaymentTimeReference) != 0 or string-length(TimeRelation) != 0 or string-length(TypeOfPeriod) != 0 or string-length(NumberOfPeriods) != 0 ">
             <ns0:C110>
                            
             <xsl:if test="string-length(TermsOfPaymentIdentification) != 0">  
              <C11001>
               <xsl:value-of select="TermsOfPaymentIdentification/text()" />               
              </C11001>              
             </xsl:if> 
             <xsl:if test="string-length(TermsOfPaymentIdentification) = 0">  
              <C11001>
               <xsl:text>ZZZ</xsl:text>
              </C11001>              
             </xsl:if>              
             <xsl:if test="string-length(CodeListQualifier) != 0">  
              <C11002>
               <xsl:value-of select="CodeListQualifier/text()" />               
              </C11002>              
             </xsl:if>   
             <xsl:if test="string-length(CodeListResponsibleAgency) != 0">  
              <C11003>
               <xsl:value-of select="CodeListResponsibleAgency/text()" />               
              </C11003>              
             </xsl:if>  
             <xsl:if test="string-length(Note) != 0">  
              <C11004>
               <xsl:value-of select="Note/text()" />               
              </C11004>              
             </xsl:if>                
                     
            </ns0:C110>
           </xsl:if>  
         <!--  
            <xsl:if test="string-length(PaymentTimeReference) != 0 or string-length(TimeRelation) != 0 or string-length(TypeOfPeriod) != 0 or string-length(NumberOfPeriods) != 0 ">
             <ns0:C112>

              <xsl:if test="string-length(PaymentTimeReference) != 0">
               <C11201>
                <xsl:value-of select="PaymentTimeReference/text()" />               
               </C11201>               
              </xsl:if>  
              <xsl:if test="string-length(TimeRelation) != 0">
               <C11202>
                <xsl:value-of select="TimeRelation/text()" />               
               </C11202>               
              </xsl:if>
              <xsl:if test="string-length(TypeOfPeriod) != 0">
               <C11203>
                <xsl:value-of select="TypeOfPeriod/text()" />               
               </C11203>               
              </xsl:if>
              <xsl:if test="string-length(NumberOfPeriods) != 0">
               <C11204>
                <xsl:value-of select="NumberOfPeriods/text()" />               
               </C11204>               
              </xsl:if>               
                              
            </ns0:C112>
           </xsl:if>
          -->
           <xsl:if test="string-length(//PaymentMeans/PaymentDueDate) != 0">
            <ns0:C112>
              <C11201>
                <xsl:text>5</xsl:text>              
              </C11201>
              <C11202>
                <xsl:text>3</xsl:text>
              </C11202>
              <C11203>
                <xsl:text>D</xsl:text>
              </C11203>
              <C11204>
                <xsl:value-of select="$Days" />
              </C11204>
            </ns0:C112>           
           </xsl:if>
      
          </ns0:PAT>

          <xsl:if test="string-length(//PaymentMeans/PaymentDueDate) != 0">
            <s0:DTM_6>
              <ns0:C507_6>
                <C50701>
                  <xsl:text>13</xsl:text>
                </C50701>
                <C50702>
                  <xsl:value-of select="concat(substring(//PaymentMeans/PaymentDueDate/text(), 1, 4), substring(//PaymentMeans/PaymentDueDate/text(), 6, 2), substring(//PaymentMeans/PaymentDueDate/text(), 9, 2))" />
                </C50702>
                <C50703>                  
                    <xsl:text>102</xsl:text>                  
                </C50703>
              </ns0:C507_6>
            </s0:DTM_6>
          </xsl:if>

          <xsl:if test="string-length(SettlementPeriod/EndDate) != 0">
           <s0:DTM_6>
            <ns0:C507_6>
             <C50701>
               <xsl:text>12</xsl:text>              
             </C50701>  
             <C50702>
              <xsl:value-of select="concat(substring(SettlementPeriod/EndDate/text(), 1, 4), substring(SettlementPeriod/EndDate/text(), 6, 2), substring(SettlementPeriod/EndDate/text(), 9, 2))" />               
             </C50702>
             <C50703>              
                 <xsl:text>102</xsl:text>              
             </C50703>            
            </ns0:C507_6>            
           </s0:DTM_6>        
          </xsl:if> 
          
          <xsl:if test="string-length(TermsNetDueDate/Date) != 0">
           <s0:DTM_6>
            <ns0:C507_6>
             <C50701>
               <xsl:text>13</xsl:text>              
             </C50701>  
             <C50702>
              <xsl:value-of select="concat(substring(TermsNetDueDate/Date/text(), 1, 4), substring(TermsNetDueDate/Date/text(), 6, 2), substring(TermsNetDueDate/Date/text(), 9, 2))" />               
             </C50702>
             <C50703>
               <xsl:if test="string-length(TermsNetDueDate/DateFormatCode) != 0">
                <xsl:value-of select="TermsNetDueDate/DateFormatCode/text()" />                               
               </xsl:if>    
               <xsl:if test="string-length(TermsNetDueDate/DateFormatCode) = 0">
                 <xsl:text>102</xsl:text>
               </xsl:if>  
             </C50703>            
            </ns0:C507_6>            
           </s0:DTM_6>        
          </xsl:if>
                     
          <xsl:if test="string-length(SettlementDiscountPercent) != 0 and SettlementDiscountPercent/text() != '0.0000'">
            <ns0:PCD>
              <ns0:C501>
                <C50101>
                  <xsl:text>12</xsl:text>
                </C50101>
                <C50102>
                  <xsl:value-of select="concat(substring-before(SettlementDiscountPercent/text(), '.'), '.', substring(substring-after(SettlementDiscountPercent/text(), '.'), 1, 2))" />
                </C50102>
              </ns0:C501>
            </ns0:PCD>
          </xsl:if>  
   <!--       
          <xsl:if test="string-length(PenaltySurchargePercent) != 0">
            <ns0:PCD>
              <ns0:C501>
                <C50101>
                  <xsl:text>15</xsl:text>
                </C50101>
                <C50102>
                  <xsl:value-of select="concat(substring-before(PenaltySurchargePercent/text(), '.'), '.', substring(substring-after(PenaltySurchargePercent/text(), '.'), 1, 2))" />
                </C50102>
              </ns0:C501>
            </ns0:PCD>
          </xsl:if>       -->    
<!--
          <xsl:if test="string-length(Percentage/PercentageCode) != 0">
            <ns0:PCD>
             <ns0:C501>

              <xsl:if test="string-length(Percentage/PercentageCode) != 0">
               <C50101>
                <xsl:value-of select="Percentage/PercentageCode/text()" />                 
               </C50101>               
              </xsl:if> 
              <xsl:if test="string-length(Percentage/PercentageValue) != 0">
               <C50102>
                <xsl:value-of select="Percentage/PercentageValue/text()" />                 
               </C50102>               
              </xsl:if> 
              <xsl:if test="string-length(Percentage/PercentageIDCode) != 0">
               <C50103>
                <xsl:value-of select="Percentage/PercentageIDCode/text()" />                 
               </C50103>               
              </xsl:if> 
              <xsl:if test="string-length(Percentage/CodeListIDCode) != 0">
               <C50104>
                <xsl:value-of select="Percentage/CodeListIDCode/text()" />                 
               </C50104>               
              </xsl:if> 
              <xsl:if test="string-length(Percentage/CodeListAgencyCode) != 0">
               <C50105>
                <xsl:value-of select="Percentage/CodeListAgencyCode/text()" />                 
               </C50105>               
              </xsl:if>               
                                                 
             </ns0:C501>             
            </ns0:PCD>           
          </xsl:if>
 -->
          <xsl:if test="string-length(MonetaryAmount/Amount) != 0">
           <ns0:MOA_2>
             <ns0:C516_2>

              <xsl:if test="string-length(MonetaryAmount/Code) != 0">
               <C51601>
                <xsl:value-of select="MonetaryAmount/Code/text()" />                
               </C51601>               
              </xsl:if>
               <xsl:if test="string-length(MonetaryAmount/Code) = 0">
                 <C51601>
                   <xsl:text>9</xsl:text>
                 </C51601>
               </xsl:if>               
              <C51602>
               <xsl:value-of select="MonetaryAmount/Amount/text()" />                
              </C51602>
              <xsl:if test="string-length(MonetaryAmount/Currency) != 0">
               <C51603>
                <xsl:value-of select="MonetaryAmount/Currency/text()" />
               </C51603>
              </xsl:if> 
             
             </ns0:C516_2>            
            </ns0:MOA_2>            
          </xsl:if>

      <xsl:if test="string-length(PaymentInstructions/PaymentConditionsCode) != 0 or string-length(PaymentInstructions/PaymentGuaranteeMeansCode) != 0 or string-length(PaymentInstructions/PaymentMeansCode) != 0 or string-length(PaymentInstructions/CodeListIDCode) != 0 or string-length(PaymentInstructions/PaymentChannelCode) != 0">
       <ns0:PAI_2>
        <ns0:C534_2>
          
         <xsl:if test="string-length(PaymentInstructions/PaymentConditionsCode) != 0">
          <C53401>
           <xsl:value-of select="PaymentInstructions/PaymentConditionsCode/text()" />           
          </C53401>          
         </xsl:if> 
         <xsl:if test="string-length(PaymentInstructions/PaymentGuaranteeMeansCode) != 0">
          <C53402>
           <xsl:value-of select="PaymentInstructions/PaymentGuaranteeMeansCode/text()" />           
          </C53402>          
         </xsl:if>
         <xsl:if test="string-length(PaymentInstructions/PaymentMeansCode) != 0">
          <C53403>
           <xsl:value-of select="PaymentInstructions/PaymentMeansCode/text()" />           
          </C53403>          
         </xsl:if> 
         <xsl:if test="string-length(PaymentInstructions/CodeListIDCode) != 0">
          <C53404>
           <xsl:value-of select="PaymentInstructions/CodeListIDCode/text()" />           
          </C53404>          
         </xsl:if> 
         <xsl:if test="string-length(PaymentInstructions/PaymentChannelCode) != 0">
          <C53405>
           <xsl:value-of select="PaymentInstructions/PaymentChannelCode/text()" />           
          </C53405>          
         </xsl:if>          
              
        </ns0:C534_2>
       </ns0:PAI_2>       
      </xsl:if>

          <xsl:if test="string-length(//PaymentMeans/PaymentMeansCode) != 0">
            <ns0:PAI_2>
              <ns0:C534_2>
                <C53403>
                  <xsl:value-of select="//PaymentMeans/PaymentMeansCode/text()" />                
                </C53403>
              </ns0:C534_2>
            </ns0:PAI_2> 
          </xsl:if>
          <!--
           <xsl:if test="string-length(//PaymentMeans/CreditAccountID) != 0">
             <ns0:FII_2>
              <FII01>
                <xsl:text>ZZZ</xsl:text>
              </FII01>
               <ns0:C078_2>
                 <C07801>
                   <xsl:value-of select="//PaymentMeans//CreditAccountID/text()" />
                 </C07801>
               </ns0:C078_2>
             </ns0:FII_2> 
          </xsl:if>    
         -->
      <xsl:if test="string-length(FinancialInstitution/CodeQualifier) != 0">
       <ns0:FII_2>
   
        <FII01>
         <xsl:value-of select="FinancialInstitution/CodeQualifier/text()" />          
        </FII01>

        <xsl:if test="string-length(FinancialInstitution/AccountHolderIdentifier) != 0 or string-length(FinancialInstitution/AccountHolderName) != 0 or string-length(FinancialInstitution/CurrencyIDCode) != 0">
         <ns0:C078_2>
         
          <xsl:if test="string-length(FinancialInstitution/AccountHolderIdentifier) != 0">
           <C07801>
            <xsl:value-of select="FinancialInstitution/AccountHolderIdentifier/text()" />             
           </C07801>           
          </xsl:if>                   
          <xsl:if test="string-length(FinancialInstitution/AccountHolderName) != 0">
           <C07802>
            <xsl:value-of select="FinancialInstitution/AccountHolderName/text()" />             
           </C07802>           
          </xsl:if>
          <xsl:if test="string-length(FinancialInstitution/CurrencyIDCode) != 0">
           <C07804>
            <xsl:value-of select="FinancialInstitution/CurrencyIDCode/text()" />             
           </C07804>           
          </xsl:if>            
         
         </ns0:C078_2>         
        </xsl:if>

        <xsl:if test="string-length(FinancialInstitution/InstitutionNameCode) != 0 or string-length(FinancialInstitution/CodeListIDCode) != 0 or string-length(FinancialInstitution/CodeListResponsibleAgencyCode) != 0 or string-length(FinancialInstitution/InstitutionBranchIdentifier) != 0 or string-length(FinancialInstitution/InstitutionName) != 0 or string-length(FinancialInstitution/InstitutionBranchLocationName) != 0">
         <ns0:C088_2>
           
          <xsl:if test="string-length(FinancialInstitution/InstitutionNameCode) != 0">
           <C08801>
            <xsl:value-of select="FinancialInstitution/InstitutionNameCode/text()" />             
           </C08801>           
          </xsl:if> 
          <xsl:if test="string-length(FinancialInstitution/CodeListIDCode) != 0">
           <C08802>
            <xsl:value-of select="FinancialInstitution/CodeListIDCode/text()" />             
           </C08802>           
          </xsl:if> 
          <xsl:if test="string-length(FinancialInstitution/CodeListResponsibleAgencyCode) != 0">
           <C08803>
            <xsl:value-of select="FinancialInstitution/CodeListResponsibleAgencyCode/text()" />             
           </C08803>           
          </xsl:if>    
          <xsl:if test="string-length(FinancialInstitution/InstitutionBranchIdentifier) != 0">
           <C08804>
            <xsl:value-of select="FinancialInstitution/InstitutionBranchIdentifier/text()" />             
           </C08804>           
          </xsl:if>    
          <xsl:if test="string-length(FinancialInstitution/InstitutionName) != 0">
           <C08807>
            <xsl:value-of select="FinancialInstitution/InstitutionName/text()" />             
           </C08807>           
          </xsl:if>            
          <xsl:if test="string-length(FinancialInstitution/InstitutionBranchLocationName) != 0">
           <C08808>
            <xsl:value-of select="FinancialInstitution/InstitutionBranchLocationName/text()" />             
           </C08808>           
          </xsl:if>  
         
         </ns0:C088_2>
        </xsl:if>  
          
       </ns0:FII_2> 
      </xsl:if>      
       
      </ns0:PATLoopt1>    
     </xsl:for-each>        
    <!--  End of  PAT -->  
      
      
      <!--
      <xsl:if test="string-length(PaymentMeans/PaymentDueDate) != 0">
  <ns0:PATLoop1>
          
        <ns0:DTM_6>


          <ns0:C507_6>
            
          <C50701>


            <xsl:value-of select="12" />

          </C50701>
            
            <C50702>
            

              <xsl:value-of select="concat(substring(PaymentMeans/PaymentDueDate/text(), 1, 4), substring(PaymentMeans/PaymentDueDate/text(), 6, 2), substring(PaymentMeans/PaymentDueDate/text(), 9, 2))" />

              </C50702>
            
<C50703>


            <xsl:value-of select="102" />

          </C50703>            
            
            
            </ns0:C507_6>
        </ns0:DTM_6>
    
    </ns0:PATLoop1>
        
        </xsl:if>                  
      -->
      
         <!-- ALC SEGMENT -->   
          <xsl:for-each select="AllowanceCharge">                
           <xsl:if test="string-length(ChargeIndicator) != 0">
            <ns0:ALCLoop1>
             <ns0:ALC>
              <ALC01>
               <xsl:choose>
                <xsl:when test="translate(ChargeIndicator, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')='FALSE'">
               <xsl:text>A</xsl:text>                  
              </xsl:when>                                                                   
              <xsl:when test="translate(ChargeIndicator, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')='TRUE'">
               <xsl:text>C</xsl:text>                   
              </xsl:when>                                                
             <xsl:otherwise>
              <xsl:text>XZ</xsl:text>                     
             </xsl:otherwise>                                         
            </xsl:choose>
           </ALC01>    
             
           <xsl:if test="string-length(ID) != 0">
            <ns0:C552>
             <C55201>
              <xsl:value-of select="ID/text()" />
             </C55201>
            </ns0:C552>                 
           </xsl:if>     
          
           <xsl:if test="string-length(SequenceNumeric) != 0">
            <ALC04>
             <xsl:value-of select="SequenceNumeric/text()" />
            </ALC04>
           </xsl:if>               
               
           <xsl:if test="string-length(AllowanceChargeReasonCode)!=0 or string-length(AllowanceChargeReason)!=0">
            <ns0:C214>
             <xsl:if test="string-length(AllowanceChargeReasonCode)!=0"> 
              <C21401>
               <xsl:value-of select="AllowanceChargeReasonCode/text()" />
              </C21401>
             </xsl:if>                        
             <xsl:if test="string-length(AllowanceChargeReason)!=0"> 
              <C21404>
               <xsl:value-of select="normalize-space(AllowanceChargeReason/text())" />
              </C21404>
             </xsl:if>                        
            </ns0:C214>                 
           </xsl:if>                                                                
          </ns0:ALC>                          
           
          <xsl:if test="string-length(MultiplierFactorNumeric) != 0">
           <ns0:PCDLoop1>
            <ns0:PCD_2>
             <ns0:C501_2>
              <C50101>                      

              <xsl:choose>
               <xsl:when test="translate(ChargeIndicator, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')='FALSE'">
                <xsl:text>1</xsl:text>                  
               </xsl:when>                                                                  
               <xsl:when test="translate(ChargeIndicator, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')='TRUE'">
                <xsl:text>2</xsl:text>                   
               </xsl:when>                         
               <xsl:otherwise>
                <xsl:text>XZ</xsl:text>                     
               </xsl:otherwise>                                         
               </xsl:choose>                                             
              </C50101>
               <C50102><xsl:value-of select="format-number(MultiplierFactorNumeric*100, '0.##')" />
              </C50102>                   
             </ns0:C501_2>
            </ns0:PCD_2>
           </ns0:PCDLoop1>
          </xsl:if>

              <xsl:if test="string-length(Amount) != 0">
                <ns0:MOALoop1>
                  <ns0:MOA_3>
                    <ns0:C516_3>

                      <xsl:if test="string-length(AmountCode) != 0">
                        <C51601>
                          <xsl:value-of select="AmountCode/text()" />
                        </C51601>
                      </xsl:if>

                      <xsl:if test="string-length(AmountCode) = 0">
                        <C51601>
                          <xsl:text>8</xsl:text>
                        </C51601>
                      </xsl:if>

                      <C51602>
                        <xsl:choose>
                          <xsl:when test="Amount/text() = '0.00'">
                            <xsl:text>0</xsl:text>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select="Amount/text()" />
                          </xsl:otherwise>
                        </xsl:choose>
                      </C51602>

                    </ns0:C516_3>
                  </ns0:MOA_3>
                </ns0:MOALoop1>

              </xsl:if>

          </ns0:ALCLoop1>
        </xsl:if>
      </xsl:for-each>
     <!-- End of  ALC --> 
                 
     <!-- LIN --> 
      <xsl:for-each select="Lines/Line">
        <ns0:LINLoop1>
          
         <!--  LIN Number and  ID  -->  
          <ns0:LIN>
            <LIN01>
              <xsl:value-of select="LineNo/text()" />
            </LIN01>
            <ns0:C212>
              <xsl:if test="string-length(Item/StandardItemID) != 0">
                <C21201>
                  <xsl:value-of select="Item/StandardItemID/text()" />
                </C21201>
              </xsl:if>
             <xsl:if test="string-length(Item/SchemeID/StdItemID) != 0"> 
              <C21202>
                <xsl:value-of select="Item/SchemeID/StdItemID/text()" />
              </C21202>
             </xsl:if>
              <xsl:if test="string-length(Item/SchemeID/StdItemID) = 0">
                <C21202>
                  <xsl:text>EN</xsl:text>
                </C21202>
              </xsl:if>
            </ns0:C212>
          </ns0:LIN>          
         <!--  END OF  LIN Number and  ID  -->
            
          <!--  PIA -->                      
            <xsl:if test="string-length(Item/SellerItemID) != 0">
            <ns0:PIA>  
             <ns0:PIA01>
              <xsl:text>1</xsl:text>
             </ns0:PIA01>
              <ns0:C212_2>
                <C21201>
                  <xsl:value-of select="Item/SellerItemID/text()" />
                </C21201>
                <C21202>
                  <xsl:text>SA</xsl:text>
                </C21202>
              </ns0:C212_2>
             </ns0:PIA>   
            </xsl:if>

           <xsl:if test="string-length(Item/BuyerItemID) != 0">
            <ns0:PIA>  
             <ns0:PIA01>
              <xsl:text>1</xsl:text>
             </ns0:PIA01>              
              <ns0:C212_2>
                <C21201>
                  <xsl:value-of select="Item/BuyerItemID/text()" />
                </C21201>
                <C21202>
                  <xsl:text>BP</xsl:text>
                </C21202>
              </ns0:C212_2>
             </ns0:PIA>  
            </xsl:if>

           <xsl:if test="string-length(Item/AdditionalItemID) != 0">
            <ns0:PIA>  
             <ns0:PIA01>
              <xsl:text>1</xsl:text>
             </ns0:PIA01>              
              <ns0:C212_2>
                <C21201>
                  <xsl:value-of select="Item/AdditionalItemID/text()" />
                </C21201>
                <C21202>
                  <xsl:text>MP</xsl:text>
                </C21202>
              </ns0:C212_2>
             </ns0:PIA>  
            </xsl:if>

           <xsl:if test="string-length(Item/CatalogueItemID) != 0">
            <ns0:PIA>   
             <ns0:PIA01>
              <xsl:text>1</xsl:text>
             </ns0:PIA01>
              <ns0:C212_2>
                <C21201>
                  <xsl:value-of select="Item/CatalogueItemID/text()" />
                </C21201>
                <C21202>
                  <xsl:text>MF</xsl:text>
                </C21202>
              </ns0:C212_2>
             </ns0:PIA>    
            </xsl:if>
            
           <xsl:for-each select="Item/ExtraPIALoop/Node"> 
             <ns0:PIA>
               <ns0:PIA01>
                 <xsl:value-of select="DigitalCode/text()" />               
               </ns0:PIA01>
               <ns0:C212_2>
                <C21201>
                  <xsl:value-of select="PIA/text()" />
                </C21201>
                <C21202>
                  <xsl:value-of select="Code/text()" />
                </C21202>
              </ns0:C212_2>   
             </ns0:PIA>   
           </xsl:for-each>                               
          <!--  End of  PIA -->
          
         <!-- IMD --> 
          <xsl:if test="string-length(Item/Name) != 0">            
           <ns0:IMD_2>
             <IMD01>
              <xsl:text>F</xsl:text> 
             </IMD01>            
              <xsl:if test="Item/Name">
               <ns0:C273_2> 
                <C27304>
                 <xsl:value-of select="substring(normalize-space(Item/Name/text()), 1, 35)" />
                </C27304>
                 <xsl:if test="string-length(substring(normalize-space(Item/Name/text()), 36, 70)) != 0">
                   <C27305>
                     <xsl:value-of select="substring(normalize-space(Item/Name/text()), 36, 70)" />
                   </C27305>
                 </xsl:if>
               </ns0:C273_2>  
              </xsl:if>            
             </ns0:IMD_2>
            </xsl:if>            
           <!-- End of  IMD -->          
<!--
          <ns0:MEA_2>




            <ns0:C502_2>
              <xsl:if test="PriceNet/PristypeCode">
                <C50201>
                  <xsl:value-of select="PriceNet/PristypeCode/text()" />
                </C50201>
              </xsl:if>
              <C50202>
                <xsl:value-of select="PriceNet/Quantity/text()" />
              </C50202>
            </ns0:C502_2>
            <ns0:C174_2>
              <xsl:if test="PriceNet/UnitCode">
                <C17401>
                  <xsl:value-of select="PriceNet/UnitCode/text()" />
                </C17401>
              </xsl:if>
            </ns0:C174_2>
          </ns0:MEA_2>
-->
         <!--  QTY -->  
          <ns0:QTY_2>
            <ns0:C186_2>
              <C18601>
                <xsl:text>47</xsl:text>              
              </C18601>
              <C18602>
                <xsl:value-of select="Quantity/text()" />
              </C18602>
              <xsl:if test="string-length(UnitCode) != 0">
                <C18603>
                  <xsl:value-of select="userCSharp:ConvertUnitCode('UBLtoTUN', UnitCode/text())" />
                  <!--         <xsl:value-of select="UnitCode/text()" /> -->
                </C18603>
              </xsl:if>  
            </ns0:C186_2>
          </ns0:QTY_2>

          <xsl:if test="string-length(Delivery/Quantity) != 0">
           <ns0:QTY_2>
            <ns0:C186_2>
              <C18601>
                <xsl:text>128</xsl:text>              
              </C18601>
              <C18602>
                <xsl:value-of select="Delivery/Quantity/text()" />
              </C18602>
              <xsl:if test="string-length(UnitCode) != 0">
                <C18603>
                  <xsl:value-of select="userCSharp:ConvertUnitCode('UBLtoTUN', UnitCode/text())" />
                  <!--         <xsl:value-of select="UnitCode/text()" /> -->
                </C18603>
              </xsl:if>  
            </ns0:C186_2>
          </ns0:QTY_2>          
         </xsl:if>            
          
          <xsl:if test="string-length(ConsumerUnit/Quantity) != 0">
           <ns0:QTY_2>
            <ns0:C186_2>
              <C18601>
                <xsl:text>59</xsl:text>              
              </C18601>
              <C18602>
                <xsl:value-of select="ConsumerUnit/Quantity/text()" />
              </C18602>
              <xsl:if test="string-length(UnitCode) != 0">
                <C18603>
                  <xsl:value-of select="userCSharp:ConvertUnitCode('UBLtoTUN', UnitCode/text())" />
                  <!--         <xsl:value-of select="UnitCode/text()" /> -->
                </C18603>
              </xsl:if>   
            </ns0:C186_2>
          </ns0:QTY_2>          
         </xsl:if>  
        <!--  End of QTY  -->                                  
                      
     <!--  FTX SEGMENT -->     
      <xsl:for-each select="NoteLoop/Node">
        <FTX_5>
         <xsl:if test="string-length(NoteQualifier) != 0">
           <FTX01>
             <xsl:value-of select="NoteQualifier/text()" />           
           </FTX01>         
         </xsl:if>
          <xsl:if test="string-length(NoteQualifier) = 0">
            <FTX01>
              <xsl:text>ZZZ</xsl:text>
            </FTX01>
          </xsl:if>
          <xsl:if test="string-length(NoteFunction) != 0">
           <FTX02>
             <xsl:value-of select="NoteFunction/text()" />           
           </FTX02>         
         </xsl:if> 
         <xsl:if test="string-length(NoteLanguage) != 0">
           <FTX05>
             <xsl:value-of select="NoteLanguage/text()" />           
           </FTX05>         
         </xsl:if>
          <ns0:C108_5>
            <C10801>
              <xsl:if test="string-length(Note) &lt;= 70">
               <xsl:value-of select="Note/text()" /> 
              </xsl:if>
              <xsl:if test="string-length(Note) &gt; 70">
               <xsl:value-of select="substring(Note/text(), 1, 70)" /> 
              </xsl:if>
            </C10801>
             <xsl:if test="string-length(substring(Note, 71, 70)) !=0">
               <C10802>
                <xsl:value-of select="substring(Note/text(), 71, 70)" />              
                </C10802> 
            </xsl:if>    
            <xsl:if test="string-length(substring(Note, 141, 70)) !=0">
               <C10803>
                <xsl:value-of select="substring(Note/text(), 141, 70)" />              
                </C10803> 
            </xsl:if>  
            <xsl:if test="string-length(substring(Note, 211, 70)) !=0">
               <C10804>
                <xsl:value-of select="substring(Note/text(), 211, 70)" />              
                </C10804> 
            </xsl:if>   
            <xsl:if test="string-length(substring(Note, 281, 70)) !=0">
               <C10805>
                <xsl:value-of select="substring(Note/text(), 281, 70)" />              
                </C10805> 
            </xsl:if> 
            
          </ns0:C108_5>       
        </FTX_5>      
      </xsl:for-each> 
     <!-- End of  FTX -->          
      
          
     <!-- MOA SEGMENT -->
     <xsl:if test="string-length(LineAmountTotal) != 0">
      <ns0:MOALoop2>  
       <ns0:MOA_5>
        <ns0:C516_5>
         <C51601>
          <xsl:text>203</xsl:text>
         </C51601>  
         <C51602>                  
         <xsl:choose>
          <xsl:when test="LineAmountTotal/text() = '0.00'">
           <xsl:text>0</xsl:text>                  
          </xsl:when>  
          <xsl:otherwise>        
           <xsl:value-of select="LineAmountTotal/text()" />  
          </xsl:otherwise>                                         
         </xsl:choose>        
        </C51602>
         <xsl:if test="string-length(Currency) != 0">
           <C51603>
             <xsl:value-of select="Currency/text()" />
           </C51603>
         </xsl:if>
        </ns0:C516_5>
       </ns0:MOA_5>
      </ns0:MOALoop2>  
     </xsl:if>  
     <xsl:for-each select="ExtraMOALoop/Node">
      <ns0:MOALoop2>  
       <ns0:MOA_5>
        <ns0:C516_5>
         <C51601>
          <xsl:value-of select="Code/text()" />
         </C51601>
         <C51602>
          <xsl:choose>
           <xsl:when test="Amount/text() = '0.00'">
            <xsl:text>0</xsl:text>                  
           </xsl:when>  
           <xsl:otherwise>        
            <xsl:value-of select="Amount/text()" />  
           </xsl:otherwise>                                         
          </xsl:choose>                                                
         </C51602>
         <xsl:if test="string-length(Currency) != 0">
          <C51603>
           <xsl:value-of select="Currency/text()" />
          </C51603>                      
         </xsl:if>           
        </ns0:C516_5>
       </ns0:MOA_5>
      </ns0:MOALoop2>       
     </xsl:for-each>    
    <!-- MOA is good :) -->     
          
     <!-- MOA+203       
          <ns0:MOALoop2>
            <ns0:MOA_5>
              <ns0:C516_5>
                <C51601>
                  <xsl:text>203</xsl:text>
              
                </C51601>
                <C51602>                  
           <xsl:choose>
                     <xsl:when test="LineAmountTotal/text() = '0.00'">
                       <xsl:text>0</xsl:text>                  
                     </xsl:when>  
      <xsl:otherwise>        
    <xsl:value-of select="LineAmountTotal/text()" />  
        </xsl:otherwise>                                         
                 </xsl:choose>        
                </C51602>
              </ns0:C516_5>
            </ns0:MOA_5>
          </ns0:MOALoop2>  -->
       
        <!--  PRI SEGMENT -->         
          <xsl:if test="string-length(PriceNet/Price) != 0 or string-length(PriceGross/Price) != 0">
            
          <ns0:PRILoop1>
           <xsl:if test="string-length(PriceNet/Price) != 0">  
            <ns0:PRI>                            
              <ns0:C509>                  
                <xsl:if test="string-length(PriceNet/PristypeCode) != 0">                    
                  <C50901>
                  <xsl:value-of select="PriceNet/PristypeCode/text()" />
                  </C50901>
                </xsl:if>                
                <xsl:if test="string-length(PriceNet/PristypeCode) = 0">
                 <C50901>
                  <xsl:text>AAA</xsl:text>
                 </C50901>
                </xsl:if>                
                <xsl:if test="string-length(PriceNet/Price) != 0">
                  <C50902>                                     
                   <xsl:choose>
                    <xsl:when test="PriceNet/Price/text() = '0.00'">
                     <xsl:text>000</xsl:text>                  <!-- Added 00 26.01.2023 -->
                    </xsl:when>  
                    <xsl:otherwise>
                      <!-- <xsl:value-of select="(round(number(PriceNet/Price/text())*100) div 100)*100" /> -->
                      <xsl:value-of select="round((round(number(PriceNet/Price/text())*100) div 100)*100)" />                      
                    </xsl:otherwise>                                         
                   </xsl:choose>                    
                  </C50902>                  
                </xsl:if> 
              <xsl:if test="string-length(PriceNet/Quantity) != 0">
                <C50905>
                  <xsl:value-of select="PriceNet/Quantity/text()" />                  
                </C50905> 
              </xsl:if>
              <xsl:if test="string-length(PriceNet/UnitCode) != 0">
                <C50906>
                 <xsl:value-of select="userCSharp:ConvertUnitCode('UBLtoTUN', PriceNet/UnitCode/text())" /> 
                  <!-- <xsl:value-of select="PriceNet/UnitCode/text()" />                  -->
                </C50906>                              
              </xsl:if>              
              </ns0:C509>                
             </ns0:PRI>            
            </xsl:if> 
          
          <xsl:if test="string-length(PriceGross/Price) != 0">   
           <ns0:PRI>
            <ns0:C509>                                  
             <xsl:if test="string-length(PriceGross/PristypeCode) != 0">
              <C50901>
               <xsl:value-of select="PriceGross/PristypeCode/text()" />
              </C50901>
             </xsl:if>               
             <xsl:if test="string-length(PriceGross/PristypeCode) = 0">
              <C50901>
               <xsl:text>AAB</xsl:text>
              </C50901>
             </xsl:if>
             <xsl:if test="string-length(PriceGross/Price) != 0">
              <C50902>               
               <xsl:choose>
                <xsl:when test="PriceGross/Price/text() = '0.00'">
                 <xsl:text>000</xsl:text>                  <!-- Added 00 26.01.2023 -->
                </xsl:when>  
                <xsl:otherwise>
                  <xsl:if test = "string-length(PriceGross/OrderableUnitFactorRate) != 0">
                 <!--   and substring-after(PriceGross/OrderableUnitFactorRate/text(), '.') != ''"> -->
                    <!--        <xsl:if test="string(number(substring(substring-after(PriceGross/OrderableUnitFactorRate/text(), '.'), 4))*number(substring(substring-after(PriceGross/Price/text(), '.'), 4))) != 'NaN'"> -->                    
                    <!--  <xsl:value-of select="string(number(substring(substring-after(PriceGross/OrderableUnitFactorRate/text(), '.'), 4))*number(substring(substring-after(PriceGross/Price/text(), '.'), 4)))" /> -->
                    <!--!        </xsl:if>  -->
                    <!-- <xsl:text>Test</xsl:text> -->
                   <!-- <xsl:value-of select="number(PriceGross/OrderableUnitFactorRate/text())*number(PriceGross/Price/text())" /> -->
                  <!-- <xsl:value-of select="round(number(round(number(PriceGross/OrderableUnitFactorRate/text())*10000) div 10000*round(number(PriceGross/Price/text())*100) div 100)*100) div 100" /> --> <!-- Commented out 26.01.2023 -->
                  <xsl:value-of select="round((round(number(round(number(PriceGross/OrderableUnitFactorRate/text())*10000) div 10000*round(number(PriceGross/Price/text())*100) div 100)*100) div 100)*100)" /> <!-- Added 26.01.2023 -->
                  </xsl:if>
                  <xsl:if test = "string-length(PriceGross/OrderableUnitFactorRate) = 0">
                   <!-- or substring-after(PriceGross/OrderableUnitFactorRate/text(), '.') = ''"> -->
                    <!-- <xsl:value-of select="round(number(PriceGross/Price/text())*100) div 100" /> --> <!-- Commented out 26.01.2023 -->
                  <xsl:value-of select="round((round(number(PriceGross/Price/text())*100) div 100)*100)" /> <!-- Added 26.01.2023 -->
                  </xsl:if>
                </xsl:otherwise>                                         
               </xsl:choose>                    
              </C50902>
             </xsl:if>
             <xsl:if test = "string-length(PriceGross/OrderableUnitFactorRate) != 0">
              <C50905>
                <xsl:value-of select="round(number(PriceGross/OrderableUnitFactorRate/text())*10000) div 10000" />             
              </C50905>
             </xsl:if>
              <xsl:if test = "string-length(PriceGross/OrderableUnitFactorRate) = 0">
                <xsl:if test="string-length(PriceGross/Quantity) != 0">
                  <C50905>
                    <xsl:value-of select="PriceGross/Quantity/text()" />
                  </C50905>
                </xsl:if>
               <xsl:if test="string-length(PriceGross/Quantity) = 0">
                 <xsl:text>0.01</xsl:text>
               </xsl:if>                     
              </xsl:if> 
              <xsl:if test="string-length(PriceGross/UnitCode) != 0">
                <C50906>
                  <xsl:value-of select="userCSharp:ConvertUnitCode('UBLtoTUN', PriceGross/UnitCode/text())" />  
                  <!--  <xsl:value-of select="PriceGross/UnitCode/text()" />                  -->
                </C50906>                              
              </xsl:if>
            </ns0:C509>                                          
           </ns0:PRI>
          </xsl:if> 
         </ns0:PRILoop1>   
            
        </xsl:if>              
       <!--  END OF  PRI -->           

         <!--  RFF  SEGMENT -->          
          <xsl:for-each select="OrderReference/Node">            
           <ns0:RFFLoop5>
             <ns0:RFF_7>
               <ns0:C506_7>
                 <C50601>
                   <xsl:value-of select="Code/text()" />                                                            
                   </C50601>
                   <C50602>
                     <xsl:value-of select="Reference/text()" />                                                              
                   </C50602>              
               </ns0:C506_7>             
             </ns0:RFF_7>           
           </ns0:RFFLoop5>          
          </xsl:for-each>          
         <!-- End of  RFF -->
          
                   <!--  RFF  SEGMENT -->          
          <xsl:for-each select="DocumentReference">   
            <xsl:if test="string-length(ID) != 0"> 
           <ns0:RFFLoop5>
             <ns0:RFF_7>
               <ns0:C506_7>
                 <C50601>
                   <xsl:value-of select="DocumentTypeCode/text()" />         
               <!--    <xsl:text>Test</xsl:text> -->
                   </C50601>
                   <C50602>
                     <xsl:value-of select="ID/text()" />                                                              
                   </C50602>              
               </ns0:C506_7>             
             </ns0:RFF_7>           
           </ns0:RFFLoop5>   
              </xsl:if>
          </xsl:for-each> 
          
         <!-- End of  RFF -->
    
               <!-- TAX --> 
                <xsl:for-each select="TaxTotal">          
                 <xsl:for-each select="TaxSubtotal">
                   <xsl:if test="TaxCategory/TaxScheme/ID/text() != 'EXC'">
                     <!--  <xsl:if test="string-length(TaxCategory/ID) != 0">  -->
                     <ns0:TAXLoop3>
                       <ns0:TAX_3>
                         <xsl:if test="string-length(TaxCode) != 0">
                           <TAX01>
                             <xsl:value-of select="TaxCode/text()" />
                           </TAX01>
                         </xsl:if>
                         <xsl:if test="string-length(TaxCode) = 0">
                           <TAX01>
                             <xsl:text>7</xsl:text>
                           </TAX01>
                         </xsl:if>

                         <xsl:if test="string-length(TaxCategory/TaxScheme/Navn) != 0">
                           <ns0:C241_3>
                   <!--         <xsl:if test="string-length(TaxCategory/TaxScheme/ID) != 0">
                               <C24101>
                                 <xsl:value-of select="TaxCategory/TaxScheme/ID/text()" />
                               </C24101>
                             </xsl:if>  -->
                       <!--      <xsl:if test="string-length(TaxCategory/TaxScheme/Navn) != 0">  -->
                               <C24104>
                                 <xsl:choose>
                                   <xsl:when test="translate(TaxCategory/TaxScheme/Navn, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')='MOMS'">
                                     <xsl:text>VAT</xsl:text>
                                   </xsl:when>
                                   <xsl:otherwise>
                                     <xsl:value-of select="TaxCategory/TaxScheme/Navn/text()" />
                                   </xsl:otherwise>
                                 </xsl:choose>
                               </C24104>
                             <!--    </xsl:if> -->
                           </ns0:C241_3>
                         </xsl:if>
                                              
  <!--    <TAX04>
            <xsl:choose>
                     <xsl:when test="TaxCategory/TaxScheme/ID/text() = '63'">
                      <xsl:value-of select="TaxableAmount/text()" />                                       
                     </xsl:when>                                             
                     <xsl:otherwise>
                       <xsl:value-of select="TaxableAmount/text()" />                     
                     </xsl:otherwise>                                         
                 </xsl:choose>   
  </TAX04> -->

                         <xsl:if test="string-length(TaxableAmount) != 0 and TaxableAmount != '0.00' and TaxableAmount != '0'">
                           <TAX04>
                             <xsl:choose>
                               <xsl:when test="TaxableAmount/text() = '0.00'">
                                 <xsl:text>0</xsl:text>
                               </xsl:when>
                               <xsl:otherwise>
                                 <xsl:value-of select="TaxableAmount/text()" />
                               </xsl:otherwise>
                             </xsl:choose>
                           </TAX04>
                         </xsl:if>

                         <xsl:if test="string-length(TaxCategory/Percent) != 0">
                           <ns0:C243_3>
                             <C24304>
                               <xsl:value-of select="TaxCategory/Percent/text()" />
                             </C24304>
                           </ns0:C243_3>
                         </xsl:if>

                         <xsl:if test="string-length(TaxCategory/TaxScheme/TaxTypeCode) != 0">
                           <TAX06>
                             <xsl:value-of select="TaxCategory/TaxScheme/TaxTypeCode/text()" />
                           </TAX06>
                         </xsl:if>

                       </ns0:TAX_3>

                       <!--          <xsl:if test="string-length(TaxableAmount) != 0 and TaxableAmount != '0.00' and TaxableAmount != '0'">
                  <ns0:MOA_7>
                    <ns0:C516_7>
                      <C51601>
                        <xsl:text>125</xsl:text>
                      </C51601>
                      <C51602>
                        <xsl:choose>
                          <xsl:when test="TaxableAmount/text() = '0.00'">
                            <xsl:text>0</xsl:text>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select="TaxableAmount/text()" />
                          </xsl:otherwise>
                        </xsl:choose>                        
                      </C51602>
                    </ns0:C516_7>
                  </ns0:MOA_7>
                </xsl:if>  -->

                       <xsl:if test="string-length(TaxAmount) != 0 and TaxAmount != '0.00' and TaxAmount != '0'">
                         <ns0:MOA_7>
                           <ns0:C516_7>
                             <C51601>
                               <xsl:text>124</xsl:text>
                             </C51601>
                             <C51602>
                               <xsl:choose>
                                 <xsl:when test="TaxAmount/text() = '0.00'">
                                   <xsl:text>0</xsl:text>
                                 </xsl:when>
                                 <xsl:otherwise>
                                   <xsl:value-of select="TaxAmount/text()" />
                                 </xsl:otherwise>
                               </xsl:choose>
                             </C51602>
                           </ns0:C516_7>
                         </ns0:MOA_7>

                       </xsl:if>

                     </ns0:TAXLoop3>
                     <!--  </xsl:if>  -->
                   </xsl:if>      
               </xsl:for-each>
              </xsl:for-each> 
          <!--  End of TAX  --> 
          
          
        
         <!-- LIN ALC -->              
          <xsl:for-each select="AllowanceCharge">
            <xsl:if test="string-length(ChargeIndicator) != 0">
              <xsl:if test="translate(AllowanceChargeReasonCode, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ') !='EXC'">
                <ns0:ALCLoop2>
                  <ns0:ALC_2>
                    <ALC01>
                      <xsl:choose>
                        <xsl:when test="translate(ChargeIndicator, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')='FALSE'">
                          <xsl:text>A</xsl:text>
                        </xsl:when>
                        <xsl:when test="translate(ChargeIndicator, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')='TRUE'">
                          <xsl:text>C</xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:text>XZ</xsl:text>
                        </xsl:otherwise>
                      </xsl:choose>
                    </ALC01>

                    <xsl:if test="string-length(ID) != 0">
                      <ns0:C552_2>
                        <C55201>
                          <xsl:value-of select="ID/text()" />
                        </C55201>
                      </ns0:C552_2>
                    </xsl:if>

                    <xsl:if test="string-length(SequenceNumeric) != 0">
                      <ALC04>
                        <xsl:value-of select="SequenceNumeric/text()" />
                      </ALC04>
                    </xsl:if>

                    <xsl:if test="string-length(AllowanceChargeReasonCode)!=0 or string-length(AllowanceChargeReason)!=0">
                      <ns0:C214_2>
                        <xsl:if test="string-length(AllowanceChargeReasonCode)!=0">
                          <C21401>
                            <xsl:value-of select="AllowanceChargeReasonCode/text()" />
                          </C21401>
                        </xsl:if>

                        <xsl:if test="string-length(AllowanceChargeReason)!=0">
                          <C21404>
                            <xsl:value-of select="normalize-space(AllowanceChargeReason/text())" />
                          </C21404>
                        </xsl:if>
                      </ns0:C214_2>
                    </xsl:if>

                  </ns0:ALC_2>

                  <xsl:if test="string-length(MultiplierFactorNumeric) != 0">
                    <ns0:PCDLoop2>
                      <ns0:PCD_5>
                        <ns0:C501_5>
                          <C50101>
                            <xsl:choose>
                              <xsl:when test="translate(ChargeIndicator, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')='FALSE'">
                                <xsl:text>1</xsl:text>
                              </xsl:when>
                              <xsl:when test="translate(ChargeIndicator, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')='TRUE'">
                                <xsl:text>2</xsl:text>
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:text>XZ</xsl:text>
                              </xsl:otherwise>
                            </xsl:choose>
                          </C50101>
                          <C50102>
                            <xsl:value-of select="format-number(MultiplierFactorNumeric*100, '0.##')" />
                          </C50102>
                        </ns0:C501_5>
                      </ns0:PCD_5>
                    </ns0:PCDLoop2>
                  </xsl:if>

                  <xsl:if test="string-length(Amount) != 0">
                    <ns0:MOALoop3>
                      <ns0:MOA_8>
                        <ns0:C516_8>

                          <xsl:if test="string-length(AmountCode) != 0">
                            <C51601>
                              <xsl:value-of select="AmountCode/text()" />
                            </C51601>
                          </xsl:if>

                          <xsl:if test="string-length(AmountCode) = 0">
                            <C51601>
                              <xsl:text>204</xsl:text>
                            </C51601>
                          </xsl:if>

                          <C51602>
                            <xsl:choose>
                              <xsl:when test="Amount/text() = '0.00'">
                                <xsl:text>0</xsl:text>
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:value-of select="Amount/text()" />
                              </xsl:otherwise>
                            </xsl:choose>
                          </C51602>

                        </ns0:C516_8>
                      </ns0:MOA_8>
                    </ns0:MOALoop3>
                  </xsl:if>

                </ns0:ALCLoop2>
              </xsl:if>
            </xsl:if>
            <xsl:if test="translate(AllowanceChargeReasonCode, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ') = 'EXC'">
               <xsl:if test="translate(ChargeIndicator, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ') = 'TRUE'">
                <ns0:TAXLoop3>
                 <ns0:TAX_3>
                  <TAX01>
                   <xsl:text>7</xsl:text>
                  </TAX01>                                                          
                         <xsl:if test="string-length(AllowanceChargeReasonCode) != 0 or string-length(AllowanceChargeReason) != 0">
                           <ns0:C241_3>
                             <xsl:if test="string-length(AllowanceChargeReasonCode) != 0">
                               <C24101>
                                 <xsl:value-of select="AllowanceChargeReasonCode/text()" />
                               </C24101>
                             </xsl:if>
                             <xsl:if test="string-length(AllowanceChargeReason) != 0">
                               <C24104>
                                 <xsl:choose>
                                   <xsl:when test="translate(AllowanceChargeReason, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')='MOMS'">
                                     <xsl:text>VAT</xsl:text>
                                   </xsl:when>
                                   <xsl:otherwise>
                                     <xsl:value-of select="AllowanceChargeReason/text()" />
                                   </xsl:otherwise>
                                 </xsl:choose>
                               </C24104>
                             </xsl:if>
                           </ns0:C241_3>
                         </xsl:if>
                     </ns0:TAX_3>                  
                   <xsl:if test="string-length(Amount) != 0">                     
                          <ns0:MOA_7>
                           <ns0:C516_7>
                             <C51601>
                               <xsl:text>124</xsl:text>
                             </C51601>
                             <C51602>
                               <xsl:choose>
                                 <xsl:when test="Amount/text() = '0.00'">
                                   <xsl:text>0</xsl:text>
                                 </xsl:when>
                                 <xsl:otherwise>
                                   <xsl:value-of select="Amount/text()" />
                                 </xsl:otherwise>
                               </xsl:choose>
                             </C51602>
                             <C51603>
                              <xsl:if test="string-length(Attributes/Amount/@CurrencyID) != 0">                              
                               <xsl:value-of select="Attributes/Amount/@CurrencyID" />                            
                              </xsl:if>   
                             </C51603>  
                           </ns0:C516_7>
                         </ns0:MOA_7>                   
                   </xsl:if>              
                </ns0:TAXLoop3>  
              </xsl:if>  
            </xsl:if>  
           </xsl:for-each>                              
          
          <!--  END OF  ALC LINE  -->         
          
        <!--  End of  LIN ALC -->          

         </ns0:LINLoop1> 
        </xsl:for-each> 
       <!-- End of  LIN -->      
          
     <!-- UNS -->
      <ns0:UNS>
        <UNS01>
          <xsl:text>S</xsl:text>
        </UNS01>
      </ns0:UNS>
     <!-- End of  UNS --> 
      
     <!-- CNT --> 
      <ns0:CNT>
        <ns0:C270>
        <C27001>
          <xsl:text>2</xsl:text>
        </C27001>
        <C27002>
          <!-- <xsl:text>2</xsl:text> -->
        <xsl:value-of select="count(Lines/Line)" />
        </C27002>
        </ns0:C270>
      </ns0:CNT> 
     <!-- End of  CNT -->       
      
  <!--  MOA SECTION -->      
     <xsl:if test="string-length(Total/LineTotalAmount) != 0">
      <ns0:MOALoopp4> 
       <ns0:MOA_10>
        <ns0:C516_10>
         <C51601>
          <xsl:text>79</xsl:text>
         </C51601>
         <C51602>  
          <xsl:choose>
           <xsl:when test="Total/LineTotalAmount/text() = '0.00'">
            <xsl:text>0</xsl:text>                  
           </xsl:when>  
           <xsl:otherwise>        
            <xsl:value-of select="Total/LineTotalAmount/text()" />  
           </xsl:otherwise>                                         
          </xsl:choose> 
         </C51602>
        </ns0:C516_10>
       </ns0:MOA_10>  
      </ns0:MOALoopp4>  
     </xsl:if>      
      
    <xsl:if test="string-length(Total/AllowanceTotalAmount) != 0"> 
      <ns0:MOALoopp4>  
       <ns0:MOA_10>
        <ns0:C516_10>
         <C51601>
          <xsl:text>204</xsl:text>
         </C51601>
         <C51602>  
          <xsl:choose>
           <xsl:when test="Total/AllowanceTotalAmount/text() = '0.00'">
            <xsl:text>0</xsl:text>                  
           </xsl:when>  
           <xsl:otherwise>        
            <xsl:value-of select="Total/AllowanceTotalAmount/text()" />  
           </xsl:otherwise>                                         
          </xsl:choose> 
         </C51602>
        </ns0:C516_10>
       </ns0:MOA_10>
      </ns0:MOALoopp4>  
     </xsl:if>         
 
     <xsl:if test="string-length(Total/ChargeTotalAmount) != 0">
      <ns0:MOALoopp4> 
       <ns0:MOA_10>
        <ns0:C516_10>
         <C51601>
          <xsl:text>23</xsl:text>
         </C51601>
         <C51602>  
          <xsl:choose>
           <xsl:when test="Total/ChargeTotalAmount/text() = '0.00'">
            <xsl:text>0</xsl:text>                  
           </xsl:when>  
           <xsl:otherwise>        
            <xsl:value-of select="Total/ChargeTotalAmount/text()" />  
           </xsl:otherwise>                                         
          </xsl:choose> 
         </C51602>
        </ns0:C516_10>
       </ns0:MOA_10>
      </ns0:MOALoopp4>  
     </xsl:if>    
      
     <xsl:if test="string-length(Total/PrepaidAmount) != 0">
      <ns0:MOALoopp4> 
       <ns0:MOA_10>
        <ns0:C516_10>
         <C51601>
          <xsl:text>113</xsl:text>
         </C51601>
         <C51602>  
          <xsl:choose>
           <xsl:when test="Total/PrepaidAmount/text() = '0.00'">
            <xsl:text>0</xsl:text>                  
           </xsl:when>  
           <xsl:otherwise>        
            <xsl:value-of select="Total/PrepaidAmount/text()" />  
           </xsl:otherwise>                                         
          </xsl:choose> 
         </C51602>
        </ns0:C516_10>
       </ns0:MOA_10>
      </ns0:MOALoopp4>  
     </xsl:if>   
      
     <xsl:if test="string-length(Total/PayableAmount) != 0">
      <ns0:MOALoopp4> 
       <ns0:MOA_10>
        <ns0:C516_10>
         <C51601>
          <xsl:text>86</xsl:text>
         </C51601>
         <C51602>  
          <xsl:choose>
           <xsl:when test="Total/PayableAmount/text() = '0.00'">
            <xsl:text>0</xsl:text>                  
           </xsl:when>  
           <xsl:otherwise>        
            <xsl:value-of select="Total/PayableAmount/text()" />  
           </xsl:otherwise>                                         
          </xsl:choose> 
         </C51602>
        </ns0:C516_10>
       </ns0:MOA_10>
      </ns0:MOALoopp4>   
     </xsl:if>       
      
     <xsl:if test="string-length(Total/TaxExclAmount) != 0">
      <ns0:MOALoopp4> 
       <ns0:MOA_10>
        <ns0:C516_10>
         <C51601>
          <xsl:text>176</xsl:text>
         </C51601>
         <C51602>  
          <xsl:choose>
           <xsl:when test="Total/TaxExclAmount/text() = '0.00'">
            <xsl:text>0</xsl:text>                  
           </xsl:when>  
           <xsl:otherwise>        
            <xsl:value-of select="Total/TaxExclAmount/text()" />  
           </xsl:otherwise>                                         
          </xsl:choose> 
         </C51602>
        </ns0:C516_10>
       </ns0:MOA_10>
      </ns0:MOALoopp4>   
     </xsl:if>   
      
     <xsl:if test="string-length(Total/TaxAmount) != 0">
      <ns0:MOALoopp4>  
       <ns0:MOA_10>
        <ns0:C516_10>
         <C51601>
          <xsl:text>124</xsl:text>
         </C51601>
         <C51602>  
          <xsl:choose>
           <xsl:when test="Total/TaxAmount/text() = '0.00'">
            <xsl:text>0</xsl:text>                  
           </xsl:when>  
           <xsl:otherwise>        
            <xsl:value-of select="Total/TaxAmount/text()" />  
           </xsl:otherwise>                                         
          </xsl:choose> 
         </C51602>
        </ns0:C516_10>
       </ns0:MOA_10>
      </ns0:MOALoopp4>   
     </xsl:if>

      <xsl:if test="string-length(Total/TaxAmount) = 0 and string-length(Total/TaxExclAmount) = 0"> 
        <xsl:if test="string-length($MOA_124_TaxTotal) != 0">
          <ns0:MOALoopp4>
            <ns0:MOA_10>
              <ns0:C516_10>
                <C51601>
                  <xsl:text>124</xsl:text>
                </C51601>
                <C51602>
                  <xsl:choose>
                    <xsl:when test="$MOA_124_TaxTotal = '0.00'">
                      <xsl:text>0</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="$MOA_124_TaxTotal" />
                    </xsl:otherwise>
                  </xsl:choose>
                </C51602>
              </ns0:C516_10>
            </ns0:MOA_10>
          </ns0:MOALoopp4>
        </xsl:if>   
      </xsl:if>      

      <xsl:if test="string-length(Total/TaxableAmount) != 0">
        <ns0:MOALoopp4>
          <ns0:MOA_10>
            <ns0:C516_10>
              <C51601>
                <xsl:text>125</xsl:text>
              </C51601>
              <C51602>
                <xsl:choose>
                  <xsl:when test="Total/TaxableAmount/text() = '0.00'">
                    <xsl:text>0</xsl:text>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="Total/TaxableAmount/text()" />
                  </xsl:otherwise>
                </xsl:choose>
              </C51602>
            </ns0:C516_10>
          </ns0:MOA_10>
        </ns0:MOALoopp4>
      </xsl:if>
      
      <xsl:if test="string-length(Total/TaxableAmount) = 0">
        <xsl:if test="string-length($MOA_125_TaxTotal) != 0">
          <ns0:MOALoopp4>
            <ns0:MOA_10>
              <ns0:C516_10>
                <C51601>
                  <xsl:text>125</xsl:text>
                </C51601>
                <C51602>
                  <xsl:choose>
                    <xsl:when test="$MOA_125_TaxTotal = '0.00'">
                      <xsl:text>0</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="$MOA_125_TaxTotal" />
                    </xsl:otherwise>
                  </xsl:choose>
                </C51602>
              </ns0:C516_10>
            </ns0:MOA_10>
          </ns0:MOALoopp4>
        </xsl:if>   
      </xsl:if>      
      <!--
     <xsl:if test="string-length(Total/DutyTaxFeeTotalAmount) != 0">
      <ns0:MOALoopp4> 
       <ns0:MOA_10>
        <ns0:C516_10>
         <C51601>
          <xsl:text>176</xsl:text>
         </C51601>
         <C51602>  
          <xsl:choose>
           <xsl:when test="Total/DutyTaxFeeTotalAmount/text() = '0.00'">
            <xsl:text>0</xsl:text>                  
           </xsl:when>  
           <xsl:otherwise>        
            <xsl:value-of select="Total/DutyTaxFeeTotalAmount/text()" />  
           </xsl:otherwise>                                         
          </xsl:choose> 
         </C51602>
        </ns0:C516_10>
       </ns0:MOA_10>
      </ns0:MOALoopp4>   
     </xsl:if>
     --> 
     <xsl:for-each select="Total/ExtraTotalMOALoop1/Node">
      <ns0:MOALoopp4>  
       <ns0:MOA_10>
        <ns0:C516_10>
         <C51601>
          <xsl:value-of select="Code/text()" />  
         </C51601>
         <C51602> 
          <xsl:choose>
           <xsl:when test="Amount/text() = '0.00'">
            <xsl:text>0</xsl:text>                  
           </xsl:when>  
           <xsl:otherwise>        
            <xsl:value-of select="Amount/text()" />  
           </xsl:otherwise>                                         
          </xsl:choose> 
         </C51602>
        </ns0:C516_10>
       </ns0:MOA_10>
      </ns0:MOALoopp4>   
     </xsl:for-each>              
  <!--  MOA IS  GOOD  :)  -->       
                                          
    </ns0:EFACT_D96A_INVOIC>
   </xsl:template>
  <msxsl:script language="C#" implements-prefix="userCSharp">
  <![CDATA[
public string ReturnRFFON(string OrderID)
{
	return "ON";
}


public bool LogicalNe(string val1, string val2)
{
	bool ret = false;
	double d1 = 0;
	double d2 = 0;
	if (IsNumeric(val1, ref d1) && IsNumeric(val2, ref d2))
	{
		ret = d1 != d2;
	}
	else
	{
		ret = String.Compare(val1, val2, StringComparison.Ordinal) != 0;
	}
	return ret;
}


public bool LogicalExistence(bool val)
{
	return val;
}


public bool LogicalAnd(string param0, string param1)
{
	return ValToBool(param0) && ValToBool(param1);
	return false;
}


public string ReturnSU(string AccSellerID)
{
     return "SU";
}

public string ReturnIV(string AccBuyerID)
{
     return "IV";
}

public string ReturnDP(string PartyID)
{
     return "DP";
}

public string ReturnBY(string PartyID)
{
     return "BY";
}

public bool IsNumeric(string val)
{
	if (val == null)
	{
		return false;
	}
	double d = 0;
	return Double.TryParse(val, System.Globalization.NumberStyles.AllowThousands | System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out d);
}

public bool IsNumeric(string val, ref double d)
{
	if (val == null)
	{
		return false;
	}
	return Double.TryParse(val, System.Globalization.NumberStyles.AllowThousands | System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out d);
}

public bool ValToBool(string val)
{
	if (val != null)
	{
		if (string.Compare(val, bool.TrueString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return true;
		}
		if (string.Compare(val, bool.FalseString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return false;
		}
		val = val.Trim();
		if (string.Compare(val, bool.TrueString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return true;
		}
		if (string.Compare(val, bool.FalseString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return false;
		}
		double d = 0;
		if (IsNumeric(val, ref d))
		{
			return (d > 0);
		}
	}
	return false;
}

public string DaysPassedFromStartDate(string StartDate, string EndDate)
{
if (string.IsNullOrWhiteSpace(EndDate))
{
 return "0";
}
 DateTime SDate = DateTime.Parse(StartDate);
 DateTime EDate = DateTime.Parse(EndDate);
 int Days = (EDate - SDate).Days;
 
 return Days.ToString();
}

public string ConvertUnitCode(string ConversionDirection, string IngoingUnitCode)
  {
  string OutgoingUnitCode = "";

  switch (ConversionDirection)
  {
  case "TUNtoUBL":
  switch (IngoingUnitCode)
  {
  case "%KG":
  OutgoingUnitCode = "HK";
  break;
  case "HMT":
  OutgoingUnitCode = "HMT";
  break;
  case "%ST":
  OutgoingUnitCode = "CNP";
  break;
  case "TUS":
  OutgoingUnitCode = "T3";
  break;
  case "ST":
  OutgoingUnitCode = "ST";
  break;
  case "BB":
  OutgoingUnitCode = "BB";
  break;
  case "BDT":
  OutgoingUnitCode = "BE";
  break;
  case "BLK":
  OutgoingUnitCode = "D64";
  break;
  case "BX":
  OutgoingUnitCode = "BX";
  break;
  case "PCE":
  OutgoingUnitCode = "C62";
  break;
  case "BEG":
  OutgoingUnitCode = "CU";
  break;
  case "BOT":
  OutgoingUnitCode = "2W";
  break;
  case "CMT":
  OutgoingUnitCode = "CMT";
  break;
  case "CH":
  OutgoingUnitCode = "CH";
  break;
  case "DAY":
  OutgoingUnitCode = "DAY";
  break;
  case "DIS":
  OutgoingUnitCode = "DS";
  break;
  case "DLT":
  OutgoingUnitCode = "DLT";
  break;
  case "DMT":
  OutgoingUnitCode = "DMT";
  break;
  case "DK":
  OutgoingUnitCode = "CA";
  break;
  case "DS":
  OutgoingUnitCode = "TN";
  break;
  case "FL":
  OutgoingUnitCode = "BO";
  break;
  case "FOT":
  OutgoingUnitCode = "FOT";
  break;
  case "GRM":
  OutgoingUnitCode = "GRM";
  break;
  case "GRO":
  OutgoingUnitCode = "GRO";
  break;
  case "HLT":
  OutgoingUnitCode = "HLT";
  break;
  case "KAR":
  OutgoingUnitCode = "CT";
  break;
  case "KS":
  OutgoingUnitCode = "Z2";
  break;
  case "KGM":
  OutgoingUnitCode = "KGM";
  break;
  case "CMQ":
  OutgoingUnitCode = "CMQ";
  break;
  case "MTQ":
  OutgoingUnitCode = "MTQ";
  break;
  case "CMK":
  OutgoingUnitCode = "CMK";
  break;
  case "MTK":
  OutgoingUnitCode = "MTK";
  break;
  case "LGD":
  OutgoingUnitCode = "LN";
  break;
  case "LTR":
  OutgoingUnitCode = "LTR";
  break;
  case "LES":
  OutgoingUnitCode = "NL";
  break;
  case "MTR":
  OutgoingUnitCode = "MTR";
  break;
  case "MLT":
  OutgoingUnitCode = "MLT";
  break;
  case "MMT":
  OutgoingUnitCode = "MMT";
  break;
  case "PAK":
  OutgoingUnitCode = "PK";
  break;
  case "PF":
  OutgoingUnitCode = "PF";
  break;
  case "PB":
  OutgoingUnitCode = "BB";
  break;
  case "PAR":
  OutgoingUnitCode = "PR";
  break;
  case "PAT":
  OutgoingUnitCode = "CQ";
  break;
  case "PL":
  OutgoingUnitCode = "PG";
  break;
  case "PS":
  OutgoingUnitCode = "BG";
  break;
  case "RIN":
  OutgoingUnitCode = "RG";
  break;
  case "NRL":
  OutgoingUnitCode = "RO";
  break;
  case "ROR":
  OutgoingUnitCode = "TU";
  break;
  case "SP":
  OutgoingUnitCode = "BJ";
  break;
  case "SPO":
  OutgoingUnitCode = "SO";
  break;
  case "SAK":
  OutgoingUnitCode = "SA";
  break;
  case "SET":
  OutgoingUnitCode = "SET";
  break;
  case "HUR":
  OutgoingUnitCode = "HUR";
  break;
  case "TNE":
  OutgoingUnitCode = "TNE";
  break;
  case "TR":
  OutgoingUnitCode = "DR";
  break;
  case "TB":
  OutgoingUnitCode = "TB";
  break;
  case "ASK":
  OutgoingUnitCode = "CS";
  break;
  case "BÅND":
  OutgoingUnitCode = "BND";
  break;
  case "BREV":
  OutgoingUnitCode = "BRV";
  break;
  case "COL":
  OutgoingUnitCode = "COL";
  break;
  case "HAP":
  OutgoingUnitCode = "HAP";
  break;
  case "KORT":
  OutgoingUnitCode = "KOR";
  break;
  case "ROND":
  OutgoingUnitCode = "RON";
  break;
  case "SKF":
  OutgoingUnitCode = "SKF";
  break;
  case "TUBE":
  OutgoingUnitCode = "TB";
  break;
  case "XXX":
  OutgoingUnitCode = "XXX";
  break;
  default:
  OutgoingUnitCode = IngoingUnitCode;
  break;
  }
  break;

  case "UBLtoTUN":

  switch (IngoingUnitCode)
  {
  case "HK":
  OutgoingUnitCode = "%KG";
  break;
  case "HMT":
  OutgoingUnitCode = "HMT";
  break;
  case "CNP":
  OutgoingUnitCode = "%ST";
  break;
  case "T3":
  OutgoingUnitCode = "TUS";
  break;
  case "ST":
  OutgoingUnitCode = "ST";
  break;
  case "BE":
  OutgoingUnitCode = "BDT";
  break;
  case "D64":
  OutgoingUnitCode = "BLK";
  break;
  case "BX":
  OutgoingUnitCode = "BX";
  break;
  case "C62":
  OutgoingUnitCode = "PCE";
  break;
  case "CU":
  OutgoingUnitCode = "BEG";
  break;
  case "2W":
  OutgoingUnitCode = "BOT";
  break;
  case "CMT":
  OutgoingUnitCode = "CMT";
  break;
  case "CH":
  OutgoingUnitCode = "CH";
  break;
  case "DAY":
  OutgoingUnitCode = "DAY";
  break;
  case "DS":
  OutgoingUnitCode = "DIS";
  break;
  case "DLT":
  OutgoingUnitCode = "DLT";
  break;
  case "DMT":
  OutgoingUnitCode = "DMT";
  break;
  case "CA":
  OutgoingUnitCode = "DK";
  break;
  case "TN":
  OutgoingUnitCode = "DS";
  break;
  case "BO":
  OutgoingUnitCode = "FL";
  break;
  case "FOT":
  OutgoingUnitCode = "FOT";
  break;
  case "GRM":
  OutgoingUnitCode = "GRM";
  break;
  case "GRO":
  OutgoingUnitCode = "GRO";
  break;
  case "HLT":
  OutgoingUnitCode = "HLT";
  break;
  case "CT":
  OutgoingUnitCode = "KAR";
  break;
  case "Z2":
  OutgoingUnitCode = "KS";
  break;
  case "KGM":
  OutgoingUnitCode = "KGM";
  break;
  case "CMQ":
  OutgoingUnitCode = "CMQ";
  break;
  case "MTQ":
  OutgoingUnitCode = "MTQ";
  break;
  case "CMK":
  OutgoingUnitCode = "CMK";
  break;
  case "MTK":
  OutgoingUnitCode = "MTK";
  break;
  case "LN":
  OutgoingUnitCode = "LGD";
  break;
  case "LTR":
  OutgoingUnitCode = "LTR";
  break;
  case "NL":
  OutgoingUnitCode = "LES";
  break;
  case "MTR":
  OutgoingUnitCode = "MTR";
  break;
  case "MLT":
  OutgoingUnitCode = "MLT";
  break;
  case "MMT":
  OutgoingUnitCode = "MMT";
  break;
  case "PK":
  OutgoingUnitCode = "PAK";
  break;
  case "PF":
  OutgoingUnitCode = "PF";
  break;
  case "BB":
  OutgoingUnitCode = "PB";
  break;
  case "PR":
  OutgoingUnitCode = "PAR";
  break;
  case "CQ":
  OutgoingUnitCode = "PAT";
  break;
  case "PG":
  OutgoingUnitCode = "PL";
  break;
  case "BG":
  OutgoingUnitCode = "PS";
  break;
  case "RG":
  OutgoingUnitCode = "RIN";
  break;
  case "RO":
  OutgoingUnitCode = "NRL";
  break;
  case "TU":
  OutgoingUnitCode = "ROR";
  break;
  case "BJ":
  OutgoingUnitCode = "SP";
  break;
  case "SO":
  OutgoingUnitCode = "SPO";
  break;
  case "SA":
  OutgoingUnitCode = "SAK";
  break;
  case "SET":
  OutgoingUnitCode = "SET";
  break;
  case "HUR":
  OutgoingUnitCode = "HUR";
  break;
  case "TNE":
  OutgoingUnitCode = "TNE";
  break;
  case "DR":
  OutgoingUnitCode = "TR";
  break;
  case "CS":
  OutgoingUnitCode = "ASK";
  break;
  case "BND":
  OutgoingUnitCode = "BÅND";
  break;
  case "BRV":
  OutgoingUnitCode = "BREV";
  break;
  case "COL":
  OutgoingUnitCode = "COL";
  break;
  case "HAP":
  OutgoingUnitCode = "HAP";
  break;
  case "KOR":
  OutgoingUnitCode = "KORT";
  break;
  case "PB":
  OutgoingUnitCode = "PB";
  break;
  case "RON":
  OutgoingUnitCode = "ROND";
  break;
  case "SKF":
  OutgoingUnitCode = "SKF";
  break;
  case "TB":
  OutgoingUnitCode = "TB";
  break;
  case "XXX":
  OutgoingUnitCode = "XXX";
  break;
  }
  break;

  default:
  OutgoingUnitCode = IngoingUnitCode;
  break;
  }

  if (OutgoingUnitCode == "")
  {
  OutgoingUnitCode = IngoingUnitCode;
  }

  return OutgoingUnitCode;
  }

]]></msxsl:script>
</xsl:stylesheet>