namespace ByggeBasen.Core.Items {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [Schema(@"http://byg-e.dk/schemas/TUN72/v10",@"CatalogData")]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"CatalogData"})]
    public sealed class CatalogDataFF : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xs:schema xmlns=""http://byg-e.dk/schemas/TUN72/v10"" xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" targetNamespace=""http://byg-e.dk/schemas/TUN72/v10"" xmlns:xs=""http://www.w3.org/2001/XMLSchema"">
  <xs:annotation>
    <xs:appinfo>
      <schemaEditorExtension:schemaInfo namespaceAlias=""b"" extensionClass=""Microsoft.BizTalk.FlatFileExtension.FlatFileExtension"" standardName=""Flat File"" xmlns:schemaEditorExtension=""http://schemas.microsoft.com/BizTalk/2003/SchemaEditorExtensions"" />
      <b:schemaInfo standard=""Flat File"" codepage=""1252"" default_pad_char="" "" pad_char_type=""char"" count_positions_by_byte=""false"" parser_optimization=""speed"" lookahead_depth=""3"" suppress_empty_nodes=""false"" generate_empty_nodes=""true"" allow_early_termination=""false"" early_terminate_optional_fields=""false"" allow_message_breakup_of_infix_root=""false"" compile_parse_tables=""false"" root_reference=""CatalogData"" />
    </xs:appinfo>
  </xs:annotation>
  <xs:element name=""CatalogData"">
    <xs:annotation>
      <xs:appinfo>
        <b:recordInfo structure=""delimited"" child_delimiter_type=""hex"" child_delimiter=""0xD 0xA"" child_order=""postfix"" sequence_number=""1"" preserve_delimiter_for_empty_data=""true"" suppress_trailing_delimiters=""false"" />
      </xs:appinfo>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:annotation>
          <xs:appinfo>
            <groupInfo sequence_number=""0"" xmlns=""http://schemas.microsoft.com/BizTalk/2003"" />
          </xs:appinfo>
        </xs:annotation>
        <xs:element name=""Header"">
          <xs:annotation>
            <xs:appinfo>
              <b:recordInfo structure=""delimited"" child_delimiter_type=""char"" child_delimiter="";"" child_order=""infix"" sequence_number=""1"" preserve_delimiter_for_empty_data=""true"" suppress_trailing_delimiters=""false"" />
            </xs:appinfo>
          </xs:annotation>
          <xs:complexType>
            <xs:sequence>
              <xs:annotation>
                <xs:appinfo>
                  <groupInfo sequence_number=""0"" xmlns=""http://schemas.microsoft.com/BizTalk/2003"" />
                </xs:appinfo>
              </xs:annotation>
              <xs:element name=""Input"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""1"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""SupplierNo"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""2"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Date"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""3"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child4"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""4"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child5"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""5"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child6"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""6"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child7"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""7"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child8"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""8"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child9"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""9"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child10"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""10"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child11"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""11"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child12"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""12"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child13"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""13"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child14"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""14"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child15"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""15"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child16"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""16"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child17"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""17"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child18"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""18"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child19"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""19"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child20"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""20"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child21"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""21"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child22"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""22"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child23"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""23"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child24"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""24"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child25"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""25"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child26"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""26"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child27"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""27"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child28"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""28"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child29"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""29"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child30"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""30"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child31"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""31"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child32"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""32"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child33"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""33"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child34"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""34"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child35"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""35"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child36"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""36"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child37"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""37"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child38"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""38"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child39"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""39"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child40"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""40"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child41"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""41"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child42"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""42"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child43"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""43"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child44"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""44"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child45"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""45"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child46"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""46"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child47"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""47"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child48"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""48"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child49"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""49"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child50"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""50"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child51"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""51"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child52"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""52"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child53"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""53"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child54"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""54"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child55"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""55"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child56"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""56"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child57"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""57"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child58"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""58"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child59"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""59"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child60"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""60"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child61"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""61"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child62"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""62"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child63"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""63"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child64"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""64"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child65"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""65"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child66"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""66"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child67"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""67"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child68"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""68"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child69"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""69"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child70"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""70"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child71"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""71"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child72"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""72"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child73"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""73"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child74"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""74"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child75"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""75"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child76"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""76"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child77"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""77"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child78"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""78"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child79"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""79"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child80"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""80"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child81"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""81"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child82"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""82"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child83"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""83"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child84"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""84"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child85"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""85"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child86"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""86"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Header_Child87"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""87"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element maxOccurs=""unbounded"" name=""Line"">
          <xs:annotation>
            <xs:appinfo>
              <b:recordInfo tag_name=""TUN-72"" structure=""delimited"" child_delimiter_type=""char"" child_delimiter="";"" child_order=""prefix"" sequence_number=""2"" preserve_delimiter_for_empty_data=""true"" suppress_trailing_delimiters=""false"" />
            </xs:appinfo>
          </xs:annotation>
          <xs:complexType>
            <xs:sequence>
              <xs:annotation>
                <xs:appinfo>
                  <groupInfo sequence_number=""0"" xmlns=""http://schemas.microsoft.com/BizTalk/2003"" />
                </xs:appinfo>
              </xs:annotation>
              <xs:element name=""SupplierNo"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""1"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Date"" type=""xs:date"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""2"" datetime_format=""yyMMdd"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""DB_Item_No"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""3"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""EAN_Number"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""4"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child5"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""5"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child6"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""6"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child7"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""7"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child8"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""8"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child9"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""9"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child10"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""10"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child11"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""11"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child12"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""12"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child13"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""13"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child14"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""14"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child15"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""15"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child16"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""16"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child17"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""17"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child18"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""18"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child19"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""19"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child20"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""20"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child21"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""21"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child22"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""22"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child23"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""23"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child24"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""24"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child25"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""25"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child26"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""26"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child27"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""27"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child28"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""28"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child29"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""29"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child30"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""30"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child31"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""31"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child32"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""32"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child33"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""33"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child34"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""34"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child35"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""35"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child36"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""36"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child37"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""37"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child38"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""38"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child39"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""39"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child40"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""40"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child41"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""41"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child42"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""42"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child43"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""43"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child44"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""44"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child45"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""45"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child46"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""46"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child47"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""47"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child48"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""48"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child49"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""49"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child50"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""50"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child51"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""51"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child52"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""52"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child53"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""53"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child54"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""54"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child55"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""55"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child56"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""56"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child57"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""57"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child58"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""58"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child59"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""59"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child60"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""60"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child61"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""61"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child62"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""62"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child63"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""63"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child64"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""64"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child65"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""65"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child66"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""66"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child67"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""67"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child68"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""68"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child69"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""69"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child70"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""70"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child71"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""71"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child72"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""72"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child73"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""73"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child74"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""74"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child75"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""75"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child76"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""76"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child77"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""77"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child78"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""78"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child79"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""79"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child80"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""80"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child81"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""81"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child82"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""82"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child83"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""83"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child84"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""84"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child85"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""85"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Line_Child86"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""86"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
</xs:schema>";
        
        public CatalogDataFF() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [1];
                _RootElements[0] = "CatalogData";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
    }
}
