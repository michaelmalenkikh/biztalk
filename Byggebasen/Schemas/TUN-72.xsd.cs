namespace Byggebasen.Schemas {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [Schema(@"http://Byggebasen.Schemas.TUN_72",@"TUN-72")]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"TUN-72"})]
    public sealed class TUN_72 : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xs:schema xmlns=""http://Byggebasen.Schemas.TUN_72"" xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" targetNamespace=""http://Byggebasen.Schemas.TUN_72"" xmlns:xs=""http://www.w3.org/2001/XMLSchema"">
  <xs:annotation>
    <xs:appinfo>
      <schemaEditorExtension:schemaInfo namespaceAlias=""b"" extensionClass=""Microsoft.BizTalk.FlatFileExtension.FlatFileExtension"" standardName=""Flat File"" xmlns:schemaEditorExtension=""http://schemas.microsoft.com/BizTalk/2003/SchemaEditorExtensions"" />
      <b:schemaInfo standard=""Flat File"" codepage=""65001"" default_pad_char="" "" pad_char_type=""char"" count_positions_by_byte=""false"" parser_optimization=""speed"" lookahead_depth=""3"" suppress_empty_nodes=""false"" generate_empty_nodes=""true"" allow_early_termination=""false"" early_terminate_optional_fields=""false"" allow_message_breakup_of_infix_root=""false"" compile_parse_tables=""false"" root_reference=""TUN-72"" />
    </xs:appinfo>
  </xs:annotation>
  <xs:element name=""TUN-72"">
    <xs:annotation>
      <xs:appinfo>
        <b:recordInfo structure=""delimited"" child_delimiter_type=""hex"" child_delimiter=""0xD 0xA"" child_order=""postfix"" sequence_number=""1"" preserve_delimiter_for_empty_data=""true"" suppress_trailing_delimiters=""false"" />
      </xs:appinfo>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:annotation>
          <xs:appinfo>
            <groupInfo sequence_number=""0"" xmlns=""http://schemas.microsoft.com/BizTalk/2003"" />
          </xs:appinfo>
        </xs:annotation>
        <xs:element maxOccurs=""unbounded"" name=""Tun-72"">
          <xs:annotation>
            <xs:appinfo>
              <b:recordInfo structure=""delimited"" child_delimiter_type=""char"" child_delimiter="";"" child_order=""infix"" sequence_number=""1"" preserve_delimiter_for_empty_data=""true"" suppress_trailing_delimiters=""false"" />
            </xs:appinfo>
          </xs:annotation>
          <xs:complexType>
            <xs:sequence>
              <xs:annotation>
                <xs:appinfo>
                  <groupInfo sequence_number=""0"" xmlns=""http://schemas.microsoft.com/BizTalk/2003"" />
                </xs:appinfo>
              </xs:annotation>
              <xs:element name=""DBInputType"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""1"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Leverandørnr"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""2"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Dato"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""3"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""DBVarenr"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""4"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""EAN-nummer"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""5"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Lev.varenr"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""6"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""ByggeBasen"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""7"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""EAN-nummer"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""8"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""UPC-nr"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""9"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Lev.Varenr"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""10"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Varegruppe"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""11"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Produkttype"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""12"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Enhed"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""13"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Sorteringskriterie"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""14"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Producent_Markedsføringsnavn"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""15"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""ProdukttypeTekst"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""16"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Model_Type"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""17"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Farve"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""18"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Bredde"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""19"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""EnhedBredde"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""20"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Højde"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""21"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""EnhedHøjde"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""22"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Længde"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""23"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""EnhedLængde"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""24"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Tykkelse"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""25"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""EnhedTykkelse"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""26"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Størrelse"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""27"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""EnhedStørrelse"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""28"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Varetekst1"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""29"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Varetekst2"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""30"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Oplysning"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""31"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Omregningsfaktor2"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""32"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Enhed2"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""33"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Omregningsfaktor3"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""34"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Enhed3"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""35"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""EAN-nummer3"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""36"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Omregningsfaktor4"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""37"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Enhed4"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""38"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""EAN-nummer4"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""39"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Ordrekvanta"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""40"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Ordreenhed"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""41"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Toldpositionsnummer"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""42"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Rumfang"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""43"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Vægt"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""44"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""U"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""45"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Stopkode"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""46"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Dato_For_Ikrafttrædelse"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""47"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Punktafgift_IBeløb"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""48"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Punktafgift_I_Procent"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""49"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Listepris"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""50"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Vejl.Udsalgspris"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""51"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Handler_ej_med_indkøbsforening"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""52"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""e-Handles_varen"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""53"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""VareU_x002F_B"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""54"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Skaffevare"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""55"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Logistik-Enhed1Bredde"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""56"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Logistik-_x0020_Enhed1EnhedsbetBredde"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""57"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Logistik-Enhed1Højde"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""58"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Logistik-Enhed1EnhedsbetHøjde"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""59"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Logistik-Enhed1Længde"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""60"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Logistik-Enhed1Enhedsbet.Længde"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""61"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Logistik-Enhed1Rumfang"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""62"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Logistik-Enhed1Vægt"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""63"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Logistik-Enhed2Bredde"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""64"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Logistik-Enhed2Enhedsbet.Bredde"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""65"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Logistik-Enhed2Højde"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""66"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Logistik-Enhed2Enhedsbet.Højde"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""67"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Logistik-Enhed2Længde"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""68"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Logistik-Enhed2Enhedsbet.Længde"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""69"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Logistik-Enhed2Rumfang"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""70"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Logistik-Enhed2Vægt"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""71"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Logistik-Enhed3Bredde"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""72"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Logistik-Enhed3Enhedsbet.Bredde"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""73"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Logistik-Enhed3Højde"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""74"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Logistik-Enhed3Enhedsbet.Højde"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""75"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Logistik-Enhed3Længde"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""76"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Logistik-Enhed3Enhedsbet.Længde"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""77"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Logistik-Enhed3Rumfang"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""78"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Logistik-Enhed3Vægt"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""79"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Logistik-Enhed4Bredde"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""80"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Logistik-Enhed4Enhedsbet.Bredde"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""81"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Logistik-Enhed4Højde"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""82"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Logistik-Enhed4Enhedsbet.Højde"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""83"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Logistik-Enhed4Længde"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""84"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Logistik-Enhed4Enhedsbet.Længde"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""85"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Logistik-Enhed4Rumfang"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""86"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
              <xs:element name=""Logistik-Enhed4Vægt"" type=""xs:string"">
                <xs:annotation>
                  <xs:appinfo>
                    <b:fieldInfo justification=""left"" sequence_number=""87"" />
                  </xs:appinfo>
                </xs:annotation>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
</xs:schema>";
        
        public TUN_72() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [1];
                _RootElements[0] = "TUN-72";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
    }
}
