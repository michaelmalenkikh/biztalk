<?xml version="1.0" encoding="UTF-16"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt"
  xmlns:var="http://schemas.microsoft.com/BizTalk/2003/var" exclude-result-prefixes="msxsl var s0" version="1.0"
  xmlns:ns0="http://byg-e.dk/schemas/v10"
  xmlns:s0="http://schemas.microsoft.com/BizTalk/EDI/EDIFACT/2006">
  <xsl:output omit-xml-declaration="yes" method="xml" version="1.0" />
  <xsl:template match="/">
    <xsl:apply-templates select="/s0:EFACT_D96A_ORDERS" />
  </xsl:template>
  <xsl:template match="/s0:EFACT_D96A_ORDERS">
    <ns0:Order>
      <OrderID>
        <xsl:value-of select="s0:BGM/BGM02/text()"/>
      </OrderID>
      <DocumentType>
        <xsl:text>220</xsl:text>
      </DocumentType>
      <TestIndicator>
        <xsl:text>0</xsl:text>
      </TestIndicator>
      <!-- YYYYmmDD ->  YYYY-mm-DD -->
      <xsl:variable name="issueDate" select="s0:DTM/s0:C507[C50701='137']/C50702" />
      <IssueDate>
        <xsl:value-of select="concat(substring($issueDate, 1,4), '-', substring($issueDate, 5,2), '-', substring($issueDate, 7,2))"/>
      </IssueDate>
      <xsl:variable name="deliveryDate" select="s0:DTM/s0:C507[C50701='2']/C50702" />
      <DeliveryDate>
        <xsl:value-of select="concat(substring($deliveryDate, 1,4), '-', substring($deliveryDate, 5,2), '-', substring($deliveryDate, 7,2))"/>
      </DeliveryDate>
      <Seller>
        <SellerID>
          <xsl:value-of select="s0:NADLoop1/s0:NAD[NAD01='SU']/s0:C082/C08201"/>
        </SellerID>
        <PartyID>
          <xsl:value-of select="s0:NADLoop1/s0:NAD[NAD01='SU']/s0:C082/C08201"/>
        </PartyID>
        <Address>
          <Name>
            <xsl:value-of select="s0:NADLoop1/s0:NAD[NAD01='SU']/s0:C058/C05801"/>
          </Name>
          <Street>
            <xsl:value-of select="s0:NADLoop1/s0:NAD[NAD01='SU']/s0:C080/C08001"/>
          </Street>
          <City>
            <xsl:value-of select="s0:NADLoop1/s0:NAD[NAD01='SU']/s0:C080/C08002"/>
          </City>
        </Address>
        <Concact>
          <ID>
            <xsl:value-of select="s0:NADLoop1[s0:NAD/NAD01='SU']/s0:CTALoop1/s0:CTA/CTA01"/>
          </ID>
          <Name>
            <xsl:value-of select="s0:NADLoop1[s0:NAD/NAD01='SU']/s0:CTALoop1/s0:CTA/s0:C056/C05602"/>
          </Name>
          <Telephone>
            <xsl:value-of select="s0:NADLoop1[s0:NAD/NAD01='SU']/s0:CTALoop1/s0:COM/s0:C076[C07602='TE']/C07601"/>
          </Telephone>
          <E-mail>
            <xsl:value-of select="s0:NADLoop1[s0:NAD/NAD01='SU']/s0:CTALoop1/s0:COM/s0:C076[C07602='EM']/C07601"/>
          </E-mail>
        </Concact>
      </Seller>
      <Buyer>
        <BuyerID>
          <xsl:value-of select="s0:NADLoop1/s0:NAD[NAD01='BY']/s0:C082/C08201"/>
        </BuyerID>
        <PartyID>
          <xsl:value-of select="s0:NADLoop1/s0:NAD[NAD01='BY']/s0:C082/C08201"/>
        </PartyID>
        <Address>
          <Name>
            <xsl:value-of select="s0:NADLoop1/s0:NAD[NAD01='BY']/s0:C058/C05801"/>
          </Name>
          <Street>
            <xsl:value-of select="s0:NADLoop1/s0:NAD[NAD01='BY']/s0:C080/C08001"/>
          </Street>
          <City>
            <xsl:value-of select="s0:NADLoop1/s0:NAD[NAD01='BY']/s0:C080/C08002"/>
          </City>
        </Address>
        <Concact>
          <ID>
            <xsl:value-of select="s0:NADLoop1[s0:NAD/NAD01='BY']/s0:CTALoop1/s0:CTA/CTA01"/>
          </ID>
          <Name>
            <xsl:value-of select="s0:NADLoop1[s0:NAD/NAD01='BY']/s0:CTALoop1/s0:CTA/s0:C056/C05602"/>
          </Name>
          <Telephone>
            <xsl:value-of select="s0:NADLoop1[s0:NAD/NAD01='BY']/s0:CTALoop1/s0:COM/s0:C076[C07602='TE']/C07601"/>
          </Telephone>
          <E-mail>
            <xsl:value-of select="s0:NADLoop1[s0:NAD/NAD01='BY']/s0:CTALoop1/s0:COM/s0:C076[C07602='EM']/C07601"/>
          </E-mail>
        </Concact>
      </Buyer>
      <Accounting>
        <AccountingID>
          <xsl:value-of select="s0:NADLoop1/s0:NAD[NAD01='IV']/s0:C082/C08201"/>
        </AccountingID>
        <PartyID>
          <xsl:value-of select="s0:NADLoop1/s0:NAD[NAD01='IV']/s0:C082/C08201"/>
        </PartyID>
        <Address>
          <Name>
            <xsl:value-of select="s0:NADLoop1/s0:NAD[NAD01='IV']/s0:C058/C05801"/>
          </Name>
          <Street>
            <xsl:value-of select="s0:NADLoop1/s0:NAD[NAD01='IV']/s0:C080/C08001"/>
          </Street>
          <City>
            <xsl:value-of select="s0:NADLoop1/s0:NAD[NAD01='IV']/s0:C080/C08002"/>
          </City>
        </Address>
        <Concact>
          <ID>
            <xsl:value-of select="s0:NADLoop1[s0:NAD/NAD01='IV']/s0:CTALoop1/s0:CTA/CTA01"/>
          </ID>
          <Name>
            <xsl:value-of select="s0:NADLoop1[s0:NAD/NAD01='IV']/s0:CTALoop1/s0:CTA/s0:C056/C05602"/>
          </Name>
          <Telephone>
            <xsl:value-of select="s0:NADLoop1[s0:NAD/NAD01='IV']/s0:CTALoop1/s0:COM/s0:C076[C07602='TE']/C07601"/>
          </Telephone>
          <E-mail>
            <xsl:value-of select="s0:NADLoop1[s0:NAD/NAD01='IV']/s0:CTALoop1/s0:COM/s0:C076[C07602='EM']/C07601"/>
          </E-mail>
        </Concact>
      </Accounting>
      <DeliveryInfo>
        <Address>
          <Name>
            <xsl:value-of select="s0:NADLoop1/s0:NAD[NAD01='DP']/s0:C080/C08001"/>
          </Name>
          <Street>
            <xsl:value-of select="s0:NADLoop1/s0:NAD[NAD01='DP']/s0:C059/C05901"/>
          </Street>
          <PostalCode>
            <xsl:value-of select="s0:NADLoop1/s0:NAD[NAD01='DP']/NAD08"/>
          </PostalCode>
          <City>
            <xsl:value-of select="s0:NADLoop1/s0:NAD[NAD01='DP']/NAD06"/>
          </City>
          <Country>
            <xsl:value-of select="s0:NADLoop1/s0:NAD[NAD01='DP']/NAD09"/>
          </Country>
        </Address>
        <Concact>
          <ID>
            <xsl:value-of select="s0:NADLoop1[s0:NAD/NAD01='DP']/s0:CTALoop1/s0:CTA/CTA01"/>
          </ID>
          <Name>
            <xsl:value-of select="s0:NADLoop1[s0:NAD/NAD01='DP']/s0:CTALoop1/s0:CTA/s0:C056/C05602"/>
          </Name>
          <Telephone>
            <xsl:value-of select="s0:NADLoop1[s0:NAD/NAD01='DP']/s0:CTALoop1/s0:COM/s0:C076[C07602='TE']/C07601"/>
          </Telephone>
          <E-mail>
            <xsl:value-of select="s0:NADLoop1[s0:NAD/NAD01='DP']/s0:CTALoop1/s0:COM/s0:C076[C07602='EM']/C07601"/>
          </E-mail>
        </Concact>
      </DeliveryInfo>
      <DeliveryTerms>
        <SpecialTerms>
          <xsl:value-of select="s0:TODLoop1/s0:TOD/s0:C100/C10004"/>
        </SpecialTerms>
        <DeliveryPeriod>
          <StartDate>
            <xsl:value-of select="s0:DTM/s0:C507[C50701='2']/C50702"/>
          </StartDate>
          <EndDate>
            <xsl:value-of select="s0:DTM/s0:C507[C50701='2']/C50702"/>
          </EndDate>
        </DeliveryPeriod>
      </DeliveryTerms>
      <PaymentTerms>
        <Currency>
          <xsl:value-of select="s0:CUXLoop1/s0:CUX/s0:C504/C50402"/>
        </Currency>
      </PaymentTerms>
      <xsl:for-each select="s0:LINLoop1">
        <Line>
          <LineNo>
            <xsl:value-of select="s0:LIN/LIN01/text()"/>
          </LineNo>
          <Quantity>
            <xsl:value-of select="s0:QTY_3/s0:C186_3[C18601='21']/C18602"/>
          </Quantity>
          <Qty_Unitcode>
            <xsl:value-of select="s0:QTY_3/s0:C186_3[C18601='21']/C18603"/>
          </Qty_Unitcode>
          <Description>
            <xsl:value-of select="s0:IMD_2/s0:C273_2/C27304"/>
          </Description>
          <BuyerItemID>
            <xsl:value-of select="s0:PIA/s0:C212_2[C21202='BP' or C21202='IN']/C21201"/>
          </BuyerItemID>
          <SellerItemID>
            <xsl:value-of select="s0:PIA/s0:C212_2[C21202='SA']/C21201"/>
          </SellerItemID>
          <StandardItemID>
            <xsl:value-of select="s0:LIN/s0:C212[C21202='EN']/C21201"/>
          </StandardItemID>
          <AdditionalItemID>
            <xsl:value-of select="s0:PIA/s0:C212_2[C21202='MP']/C21201"/>
          </AdditionalItemID>
          <CatalogueItemID>
            <xsl:value-of select="s0:PIA/s0:C212_2[C21202='MF']/C21201"/>
          </CatalogueItemID>
        </Line>
      </xsl:for-each>
    </ns0:Order>
  </xsl:template>
</xsl:stylesheet>