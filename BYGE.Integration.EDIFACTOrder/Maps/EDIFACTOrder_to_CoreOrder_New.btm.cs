namespace BYGE.Integration.EDIFACTOrder.Maps {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.EDIFACTOrder.Schemas.EFACT_D96A_ORDERS", typeof(global::BYGE.Integration.EDIFACTOrder.Schemas.EFACT_D96A_ORDERS))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.Core.Order.Order", typeof(global::BYGE.Integration.Core.Order.Order))]
    public sealed class EDIFACTOrder_to_CoreOrder_New : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" xmlns:userCSharp=""http://schemas.microsoft.com/BizTalk/2003/userCSharp"" exclude-result-prefixes=""msxsl var s0"" version=""1.0"" xmlns:ns0=""http://byg-e.dk/schemas/v10"" xmlns:s0=""http://schemas.microsoft.com/BizTalk/EDI/EDIFACT/2006"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/s0:EFACT_D96A_ORDERS"" />
  </xsl:template>
  <xsl:template match=""/s0:EFACT_D96A_ORDERS"">
    <ns0:Order>
      <ProfileID>
        <xsl:text>Dummy</xsl:text> 
      </ProfileID>
      <DocumentType>
        <xsl:text>220</xsl:text>
      </DocumentType>
      <TestIndicator>
        <xsl:text>0</xsl:text>
      </TestIndicator>
      <xsl:if test=""s0:BGM/BGM02"">
        <OrderID>
          <xsl:value-of select=""s0:BGM/BGM02/text()"" />
        </OrderID>
      </xsl:if>
      <xsl:if test=""s0:BGM/s0:C002/C00201"">
        <OrderType>
          <xsl:value-of select=""s0:BGM/s0:C002/C00201/text()"" />
        </OrderType>
      </xsl:if>    
      
      <!-- *** DTM *** -->
      <xsl:for-each select=""s0:DTM/s0:C507"">
        <xsl:choose>
          <xsl:when test=""C50701 = '137'"">
            <IssueDate>
              <xsl:value-of select=""userCSharp:editDate(C50702/text())"" />
            </IssueDate>
          </xsl:when>
          <xsl:otherwise>
            <DTMReference>
              <Node>
                <Code>
                  <xsl:value-of select=""C50701/text()"" />
                </Code>
                <TimeStamp>
                  <xsl:value-of select=""userCSharp:editDate(C50702/text())"" />
                </TimeStamp>
              </Node>
            </DTMReference> 
         </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each>
      <!-- *** END OF DTM *** -->      
      
      <!--  FTX SEGMENT -->
      
       <xsl:for-each select=""s0:FTX"">
       <NoteLoop>    
         <Node>
       <xsl:if test=""FTX01"">
         <NoteQualifier>  
          <xsl:value-of select=""FTX01/text()"" />      
         </NoteQualifier>    
       </xsl:if>  
        <xsl:if test=""FTX02"">
         <NoteFunction>  
          <xsl:value-of select=""FTX02/text()"" />      
         </NoteFunction>    
       </xsl:if>  
        <xsl:if test=""FTX05"">
         <NoteLanguage>  
          <xsl:value-of select=""FTX05/text()"" />      
         </NoteLanguage>    
       </xsl:if>         
        <Note>
          <xsl:value-of select=""s0:C108/C10801/text()"" />
        </Note>  
       </Node>
         
          <xsl:if test=""s0:C108/C10802"">
         <Node>
           <xsl:if test=""FTX01"">
         <NoteQualifier>  
          <xsl:value-of select=""FTX01/text()"" />      
         </NoteQualifier>    
       </xsl:if>  
        <xsl:if test=""FTX02"">
         <NoteFunction>  
          <xsl:value-of select=""FTX02/text()"" />      
         </NoteFunction>    
       </xsl:if>  
        <xsl:if test=""FTX05"">
         <NoteLanguage>  
          <xsl:value-of select=""FTX05/text()"" />      
         </NoteLanguage>    
       </xsl:if>         
          <Note>
            <xsl:value-of select=""s0:C108/C10802/text()"" />        
          </Note>
         </Node>             
          </xsl:if>
       
         
          <xsl:if test=""s0:C108/C10803"">
            <Node>
              <xsl:if test=""FTX01"">
         <NoteQualifier>  
          <xsl:value-of select=""FTX01/text()"" />      
         </NoteQualifier>    
       </xsl:if>  
        <xsl:if test=""FTX02"">
         <NoteFunction>  
          <xsl:value-of select=""FTX02/text()"" />      
         </NoteFunction>    
       </xsl:if>  
        <xsl:if test=""FTX05"">
         <NoteLanguage>  
          <xsl:value-of select=""FTX05/text()"" />      
         </NoteLanguage>    
       </xsl:if>         
            <Note>
             <xsl:value-of select=""s0:C108/C10803/text()"" />        
            </Note>
           </Node>   
          </xsl:if>
         
          <xsl:if test=""s0:C108/C10804""> 
          <Node>
            <xsl:if test=""FTX01"">
         <NoteQualifier>  
          <xsl:value-of select=""FTX01/text()"" />      
         </NoteQualifier>    
       </xsl:if>  
        <xsl:if test=""FTX02"">
         <NoteFunction>  
          <xsl:value-of select=""FTX02/text()"" />      
         </NoteFunction>    
       </xsl:if>  
        <xsl:if test=""FTX05"">
         <NoteLanguage>  
          <xsl:value-of select=""FTX05/text()"" />      
         </NoteLanguage>    
       </xsl:if>         
           <Note>
            <xsl:value-of select=""s0:C108/C10804/text()"" />        
          </Note>
          </Node>  
          </xsl:if>

          <xsl:if test=""s0:C108/C10805""> 
           <Node>
             <xsl:if test=""FTX01"">
         <NoteQualifier>  
          <xsl:value-of select=""FTX01/text()"" />      
         </NoteQualifier>    
       </xsl:if>  
        <xsl:if test=""FTX02"">
         <NoteFunction>  
          <xsl:value-of select=""FTX02/text()"" />      
         </NoteFunction>    
       </xsl:if>  
        <xsl:if test=""FTX05"">
         <NoteLanguage>  
          <xsl:value-of select=""FTX05/text()"" />      
         </NoteLanguage>    
       </xsl:if>         
            <Note>
            <xsl:value-of select=""s0:C108/C10805/text()"" />        
           </Note>  
          </Node>   
         </xsl:if>      
       
      </NoteLoop>     
      </xsl:for-each>    
      
      <!--  END OF  FTX -->
        
     <!-- *** CUX *** -->
      <xsl:if test=""s0:CUXLoop1/s0:CUX/s0:C504/C50402"">
        <Currency>
          <xsl:value-of select=""s0:CUXLoop1/s0:CUX/s0:C504/C50402/text()"" />
        </Currency>
      </xsl:if>
      
          <!-- *** RFF *** -->
     <OrderReference> 
      <xsl:for-each select=""s0:RFFLoop1"">
        <xsl:choose>
          <xsl:when test=""s0:RFF/s0:C506/C50601 = 'CR'"">       
              <CustomerReference>
                <xsl:value-of select=""s0:RFF/s0:C506/C50602/text()"" />
             </CustomerReference>            
          </xsl:when>
          <xsl:when test=""s0:RFF/s0:C506/C50601 = 'ON'"">       
              <SalesOrderID>
                <xsl:value-of select=""s0:RFF/s0:C506/C50602/text()"" />
             </SalesOrderID>            
          </xsl:when>
        <xsl:otherwise>
         <Node>
          <Code>            
           <xsl:value-of select=""s0:RFF/s0:C506/C50601/text()"" />
          </Code>
          <Reference>
            <xsl:value-of select=""s0:RFF/s0:C506/C50602/text()"" />          
          </Reference>        
         </Node>            
        </xsl:otherwise>
       </xsl:choose>   
      </xsl:for-each>
     </OrderReference>
      
      <!--  END OF  RFF -->
      
      <!-- *** NAD *** -->
      <xsl:for-each select=""s0:NADLoop1"">
        <!-- *** NAD SU *** -->
        <xsl:choose>
          <xsl:when test=""s0:NAD/NAD01 = 'SU'"">
            <SupplierCustomerParty>
              <AccSellerID>
                <xsl:value-of select=""s0:NAD/s0:C082/C08201/text()"" />
              </AccSellerID>
              <PartyID>
                <xsl:value-of select=""s0:NAD/s0:C082/C08201/text()"" />
              </PartyID>
              <CompanyID>
                <xsl:value-of select=""s0:RFFLoop2/s0:RFF_2/s0:C506_2/C50602/text()"" />
              </CompanyID>
              <SchemeID>
                <xsl:if test=""s0:NAD/s0:C082/C08203/text() = '9'"">
                  <Endpoint>
                    <xsl:value-of select=""'GLN'"" />
                  </Endpoint>     
                  <Party>
                    <xsl:value-of select=""'GLN'"" />
                  </Party> 
                </xsl:if>   
                <Company>
                  <xsl:if test=""string-length(s0:RFFLoop2/s0:RFF_2/s0:C506_2/C50602) != 0""> 
                    <xsl:value-of select=""'DK:CVR'"" />
                  </xsl:if>   
                </Company>              
              </SchemeID>
              <Address>
                <xsl:if test=""string-length(s0:NAD/s0:C080/C08001) != 0"">
                  <Name>
                    <xsl:value-of select=""s0:NAD/s0:C080/C08001/text()"" />
                  </Name>
                </xsl:if>

                <xsl:if test=""string-length(s0:NAD/s0:C059/C05902) != 0"">
                  <Postbox>
                    <xsl:value-of select=""s0:NAD/s0:C059/C05902/text()"" />
                  </Postbox>
                </xsl:if>

                <xsl:if test=""string-length(s0:NAD/s0:C059/C05901) != 0"">
                  <Street>
                    <xsl:value-of select=""s0:NAD/s0:C059/C05901/text()"" />
                  </Street>
                </xsl:if>                
               
              <xsl:if test=""string-length(s0:NAD/NAD06) != 0"">
                  <City>
                    <xsl:value-of select=""s0:NAD/NAD06/text()"" />
                  </City>
                </xsl:if>
                
                <xsl:if test=""string-length(s0:NAD/NAD08) != 0"">
                  <PostalCode>
                    <xsl:value-of select=""s0:NAD/NAD08/text()"" />
                  </PostalCode>
                </xsl:if>
                
                <xsl:if test=""string-length(s0:NAD/NAD09) != 0"">
                  <Country>
                    <xsl:value-of select=""s0:NAD/NAD09/text()"" />
                  </Country>
                </xsl:if>             
              </Address>
              <xsl:for-each select=""s0:CTALoop1"">
                <Concact>
                  <xsl:if test=""string-length(s0:CTA/s0:C056/C05602) != 0""> 
                    <ID>
                      <xsl:value-of select=""s0:CTA/CTA01/text()"" />
                    </ID>  
                    <Name>
                      <xsl:value-of select=""s0:CTA/s0:C056/C05602/text()"" />                    
                    </Name>
                  </xsl:if>
                  <xsl:for-each select=""s0:COM/s0:C076"">      
                    <xsl:choose>
                      <xsl:when test=""C07602 = 'TE'"">
                        <Telephone>
                          <xsl:value-of select=""C07601/text()"" />  
                        </Telephone>
                      </xsl:when>
                      <xsl:when test=""C07602 = 'FX'"">
                        <Telefax>
                          <xsl:value-of select=""C07601/text()"" />  
                        </Telefax>
                         </xsl:when>
                      <xsl:when test=""C07602 = 'EM'"">
                        <E-mail>
                          <xsl:value-of select=""C07601/text()"" />                         
                        </E-mail>
                      </xsl:when>
                    </xsl:choose>
                  </xsl:for-each>  
                </Concact>
              </xsl:for-each>

              <xsl:if test=""string-length(s0:RFFLoop2/s0:RFF_2/s0:C506_2/C50602) != 0"">
                <RFFNADLoop>
                  <xsl:for-each select=""s0:RFFLoop2"">
                    <Node>
                      <Code>
                        <xsl:value-of select=""s0:RFF_2/s0:C506_2/C50601/text()"" />
                      </Code>
                      <Reference>
                        <xsl:value-of select=""s0:RFF_2/s0:C506_2/C50602/text()"" />
                      </Reference>
                    </Node>
                  </xsl:for-each>
                </RFFNADLoop>
              </xsl:if>   
              
            </SupplierCustomerParty>
          </xsl:when>  
        </xsl:choose>    
        <!-- *** NAD IV *** -->
  
        <xsl:choose>
          <xsl:when test=""s0:NAD/NAD01 = 'IV'"">
            <AccountingCustomer>
              <AccBuyerID>
                <xsl:value-of select=""s0:NAD/s0:C082/C08201/text()"" />
              </AccBuyerID>
              <PartyID>
                <xsl:value-of select=""s0:NAD/s0:C082/C08201/text()"" />
              </PartyID>
              <CompanyID>
                <xsl:value-of select=""s0:RFFLoop2/s0:RFF_2/s0:C506_2/C50602/text()"" />
              </CompanyID>
              <SchemeID>
                <xsl:if test=""s0:NAD/s0:C082/C08203/text() = '9'"">
                  <Endpoint>
                    <xsl:value-of select=""'GLN'"" />
                  </Endpoint>     
                  <Party>
                    <xsl:value-of select=""'GLN'"" />
                  </Party> 
                </xsl:if>   
                <Company>
                  <xsl:if test=""string-length(s0:RFFLoop2/s0:RFF_2/s0:C506_2/C50602) != 0""> 
                    <xsl:value-of select=""'DK:CVR'"" />
                  </xsl:if>   
                </Company>
              </SchemeID>
              
              <Address>
                <xsl:if test=""string-length(s0:NAD/s0:C080/C08001) != 0"">
                  <Name>
                    <xsl:value-of select=""s0:NAD/s0:C080/C08001/text()"" />
                  </Name>
                </xsl:if>
                
                 <xsl:if test=""string-length(s0:NAD/s0:C059/C05902) != 0"">
                  <Postbox>
                    <xsl:value-of select=""s0:NAD/s0:C059/C05902/text()"" />
                  </Postbox>
                </xsl:if>                
                
              <xsl:if test=""string-length(s0:NAD/s0:C059/C05901) != 0"">
                  <Street>
                    <xsl:value-of select=""s0:NAD/s0:C059/C05901/text()"" />
                  </Street>
                </xsl:if>
                                
              <xsl:if test=""string-length(s0:NAD/NAD06) != 0"">
                  <City>
                    <xsl:value-of select=""s0:NAD/NAD06/text()"" />
                  </City>
                </xsl:if>
                
                <xsl:if test=""string-length(s0:NAD/NAD08) != 0"">
                  <PostalCode>
                    <xsl:value-of select=""s0:NAD/NAD08/text()"" />
                  </PostalCode>
                </xsl:if>
                
                <xsl:if test=""string-length(s0:NAD/NAD09) != 0"">
                  <Country>
                    <xsl:value-of select=""s0:NAD/NAD09/text()"" />
                  </Country>
                </xsl:if>
              
              </Address>
                              
              <xsl:for-each select=""s0:CTALoop1"">
                <Concact>
                  <xsl:if test=""string-length(s0:CTA/s0:C056/C05602) != 0""> 
                    <ID>
                      <xsl:value-of select=""s0:CTA/CTA01/text()"" />
                    </ID>  
                    <Name>
                      <xsl:value-of select=""s0:CTA/s0:C056/C05602/text()"" />                    
                    </Name>
                  </xsl:if>
                  <xsl:for-each select=""s0:COM/s0:C076"">      
                    <xsl:choose>
                      <xsl:when test=""C07602 = 'TE'"">
                        <Telephone>
                          <xsl:value-of select=""C07601/text()"" />  
                        </Telephone>
                      </xsl:when>
                      <xsl:when test=""C07602 = 'FX'"">
                        <Telefax>
                          <xsl:value-of select=""C07601/text()"" />  
                        </Telefax>
                         </xsl:when>
                      <xsl:when test=""C07602 = 'EM'"">
                        <E-mail>
                          <xsl:value-of select=""C07601/text()"" />                         
                        </E-mail>
                      </xsl:when>
                    </xsl:choose>
                  </xsl:for-each>  
                </Concact>
              </xsl:for-each>

              <xsl:if test=""string-length(s0:RFFLoop2/s0:RFF_2/s0:C506_2/C50602) != 0"">
                <RFFNADLoop>
                  <xsl:for-each select=""s0:RFFLoop2"">
                    <Node>
                      <Code>
                        <xsl:value-of select=""s0:RFF_2/s0:C506_2/C50601/text()"" />
                      </Code>
                      <Reference>
                        <xsl:value-of select=""s0:RFF_2/s0:C506_2/C50602/text()"" />
                      </Reference>
                    </Node>
                  </xsl:for-each>
                </RFFNADLoop>
              </xsl:if>  

            </AccountingCustomer>
          </xsl:when>
        </xsl:choose>         
        <!-- *** NAD BY *** -->
        <xsl:choose>
          <xsl:when test=""s0:NAD/NAD01 = 'BY'"">
            <BuyerCustomerParty>
              <BuyerID>
                <xsl:value-of select=""s0:NAD/s0:C082/C08201/text()"" />
              </BuyerID>
              <PartyID>
                <xsl:value-of select=""s0:NAD/s0:C082/C08201/text()"" />
              </PartyID>
              <CompanyID>
                <xsl:value-of select=""s0:RFFLoop2/s0:RFF_2/s0:C506_2/C50602/text()"" />
              </CompanyID>
              <SchemeID>
                <xsl:if test=""s0:NAD/s0:C082/C08203/text() = '9'"">
                  <Endpoint>
                    <xsl:value-of select=""'GLN'"" />
                  </Endpoint>     
                  <Party>
                    <xsl:value-of select=""'GLN'"" />
                  </Party> 
                </xsl:if>   
                <Company>
                  <xsl:if test=""string-length(s0:RFFLoop2/s0:RFF_2/s0:C506_2/C50602) != 0""> 
                    <xsl:value-of select=""'DK:CVR'"" />
                  </xsl:if>   
                </Company>
              </SchemeID>
              
              <Address>
                <xsl:if test=""string-length(s0:NAD/s0:C080/C08001) != 0"">
                  <Name>
                    <xsl:value-of select=""s0:NAD/s0:C080/C08001/text()"" />
                  </Name>
                </xsl:if>

                <xsl:if test=""string-length(s0:NAD/s0:C059/C05902) != 0"">
                  <Postbox>
                    <xsl:value-of select=""s0:NAD/s0:C059/C05902/text()"" />
                  </Postbox>
                </xsl:if>

                <xsl:if test=""string-length(s0:NAD/s0:C059/C05901) != 0"">
                  <Street>
                    <xsl:value-of select=""s0:NAD/s0:C059/C05901/text()"" />
                  </Street>
                </xsl:if>
                                
              <xsl:if test=""string-length(s0:NAD/NAD06) != 0"">
                  <City>
                    <xsl:value-of select=""s0:NAD/NAD06/text()"" />
                  </City>
                </xsl:if>
                
                <xsl:if test=""string-length(s0:NAD/NAD08) != 0"">
                  <PostalCode>
                    <xsl:value-of select=""s0:NAD/NAD08/text()"" />
                  </PostalCode>
                </xsl:if>
                
                <xsl:if test=""string-length(s0:NAD/NAD09) != 0"">
                  <Country>
                    <xsl:value-of select=""s0:NAD/NAD09/text()"" />
                  </Country>
                </xsl:if>
              
              </Address>
              <xsl:for-each select=""s0:CTALoop1"">
                <Concact>
                  <xsl:if test=""string-length(s0:CTA/s0:C056/C05602) != 0""> 
                    <ID>
                      <xsl:value-of select=""s0:CTA/CTA01/text()"" />
                    </ID>  
                    <Name>
                      <xsl:value-of select=""s0:CTA/s0:C056/C05602/text()"" />                    
                    </Name>
                  </xsl:if>
                  <xsl:for-each select=""s0:COM/s0:C076"">      
                    <xsl:choose>
                      <xsl:when test=""C07602 = 'TE'"">
                        <Telephone>
                          <xsl:value-of select=""C07601/text()"" />  
                        </Telephone>
                      </xsl:when>
                      <xsl:when test=""C07602 = 'FX'"">
                        <Telefax>
                          <xsl:value-of select=""C07601/text()"" />  
                        </Telefax>
                         </xsl:when>
                      <xsl:when test=""C07602 = 'EM'"">
                        <E-mail>
                          <xsl:value-of select=""C07601/text()"" />                         
                        </E-mail>
                      </xsl:when>
                    </xsl:choose>
                  </xsl:for-each>  
                </Concact>
              </xsl:for-each>

              <xsl:if test=""string-length(s0:RFFLoop2/s0:RFF_2/s0:C506_2/C50602) != 0"">
                <RFFNADLoop>
                  <xsl:for-each select=""s0:RFFLoop2"">
                    <Node>
                      <Code>
                        <xsl:value-of select=""s0:RFF_2/s0:C506_2/C50601/text()"" />
                      </Code>
                      <Reference>
                        <xsl:value-of select=""s0:RFF_2/s0:C506_2/C50602/text()"" />
                      </Reference>
                    </Node>
                  </xsl:for-each>
                </RFFNADLoop>
              </xsl:if>  
          
            </BuyerCustomerParty>
          </xsl:when>
        </xsl:choose>    
        <!-- *** NAD DP *** -->
       <xsl:choose>
          <xsl:when test=""s0:NAD/NAD01 = 'DP'"">
           <Delivery>
            <DeliveryLocation>
              <DeliveryID>
                <xsl:value-of select=""s0:NAD/s0:C082/C08201/text()"" />
              </DeliveryID>
              <PartyID>
                <xsl:value-of select=""s0:NAD/s0:C082/C08201/text()"" />
              </PartyID>
              <CompanyID>
                <xsl:value-of select=""s0:RFFLoop2/s0:RFF_2/s0:C506_2/C50602/text()"" />
              </CompanyID>
              <SchemeID>
                <xsl:if test=""s0:NAD/s0:C082/C08203/text() = '9'"">
                  <Endpoint>
                    <xsl:value-of select=""'GLN'"" />
                  </Endpoint>     
                  <Party>
                    <xsl:value-of select=""'GLN'"" />
                  </Party> 
                </xsl:if>   
                <Company>
                  <xsl:if test=""string-length(s0:RFFLoop2/s0:RFF_2/s0:C506_2/C50602) != 0""> 
                    <xsl:value-of select=""'DK:CVR'"" />
                  </xsl:if>   
                </Company>
              </SchemeID>
              
              <Address>
                <xsl:if test=""string-length(s0:NAD/s0:C080/C08001) != 0"">
                  <Name>
                    <xsl:value-of select=""s0:NAD/s0:C080/C08001/text()"" />
                  </Name>
                </xsl:if>

                <xsl:if test=""string-length(s0:NAD/s0:C059/C05902) != 0"">
                  <Postbox>
                    <xsl:value-of select=""s0:NAD/s0:C059/C05902/text()"" />
                  </Postbox>
                </xsl:if>                
                
              <xsl:if test=""string-length(s0:NAD/s0:C059/C05901) != 0"">
                  <Street>
                    <xsl:value-of select=""s0:NAD/s0:C059/C05901/text()"" />
                  </Street>
                </xsl:if>                
                
              <xsl:if test=""string-length(s0:NAD/NAD06) != 0"">
                  <City>
                    <xsl:value-of select=""s0:NAD/NAD06/text()"" />
                  </City>
                </xsl:if>
                
                <xsl:if test=""string-length(s0:NAD/NAD08) != 0"">
                  <PostalCode>
                    <xsl:value-of select=""s0:NAD/NAD08/text()"" />
                  </PostalCode>
                </xsl:if>
                
                <xsl:if test=""string-length(s0:NAD/NAD09) != 0"">
                  <Country>
                    <xsl:value-of select=""s0:NAD/NAD09/text()"" />
                  </Country>
                </xsl:if>
              
              </Address>
              <xsl:for-each select=""s0:CTALoop1"">
                <Concact>
                  <xsl:if test=""string-length(s0:CTA/s0:C056/C05602) != 0""> 
                    <ID>
                      <xsl:value-of select=""s0:CTA/CTA01/text()"" />
                    </ID>  
                    <Name>
                      <xsl:value-of select=""s0:CTA/s0:C056/C05602/text()"" />                    
                    </Name>
                  </xsl:if>
                  <xsl:for-each select=""s0:COM/s0:C076"">      
                    <xsl:choose>
                      <xsl:when test=""C07602 = 'TE'"">
                        <Telephone>
                          <xsl:value-of select=""C07601/text()"" />  
                        </Telephone>
                      </xsl:when>
                      <xsl:when test=""C07602 = 'FX'"">
                        <Telefax>
                          <xsl:value-of select=""C07601/text()"" />  
                        </Telefax>
                         </xsl:when>
                      <xsl:when test=""C07602 = 'EM'"">
                        <E-mail>
                          <xsl:value-of select=""C07601/text()"" />                         
                        </E-mail>
                      </xsl:when>
                    </xsl:choose>
                  </xsl:for-each>  
                </Concact>
              </xsl:for-each>
            </DeliveryLocation>

             <xsl:if test=""string-length(s0:RFFLoop2/s0:RFF_2/s0:C506_2/C50602) != 0"">
               <RFFNADLoop>
                 <xsl:for-each select=""s0:RFFLoop2"">
                   <Node>
                     <Code>
                       <xsl:value-of select=""s0:RFF_2/s0:C506_2/C50601/text()"" />
                     </Code>
                     <Reference>
                       <xsl:value-of select=""s0:RFF_2/s0:C506_2/C50602/text()"" />
                     </Reference>
                   </Node>
                 </xsl:for-each>
               </RFFNADLoop>
             </xsl:if>  
           
             
<!-- *** TOD *** -->
       <DeliveryInfo>
        <xsl:for-each select=""//s0:TODLoop1/s0:TOD"">           
           <DeliveryTerms>
             <xsl:if test=""string-length(s0:C100/C10001) != 0"">
              <SpecialTerms>
               <xsl:value-of select=""s0:C100/C10001/text()"" />
              </SpecialTerms> 
             </xsl:if>
             <xsl:if test=""string-length(s0:C100/C10002) != 0"">
              <SpecialTerms2>
               <xsl:value-of select=""s0:C100/C10002/text()"" />
              </SpecialTerms2> 
             </xsl:if>
             <xsl:if test=""string-length(s0:C100/C10003) != 0"">
              <SpecialTerms3>
               <xsl:value-of select=""s0:C100/C10003/text()"" />
              </SpecialTerms3> 
             </xsl:if>
             <xsl:if test=""string-length(s0:C100/C10004) != 0"">
              <SpecialTerms4>
               <xsl:value-of select=""s0:C100/C10004/text()"" />
              </SpecialTerms4> 
             </xsl:if>
            <xsl:if test=""string-length(s0:C100/C10005) != 0"">
              <SpecialTerms5>
               <xsl:value-of select=""s0:C100/C10005/text()"" />
              </SpecialTerms5> 
             </xsl:if>              
           <xsl:if test=""string-length(TOD01) != 0"">
              <SpecialTermsCode>
               <xsl:value-of select=""TOD01/text()"" />
              </SpecialTermsCode>            
           </xsl:if>   
           <xsl:if test=""string-length(TOD02) != 0"">
              <SpecialTermsCode2>
               <xsl:value-of select=""TOD02/text()"" />
              </SpecialTermsCode2>            
           </xsl:if>           
           </DeliveryTerms>                  
      </xsl:for-each>
<!-- ***  END OF TOD *** -->
             
<!-- ***  DTM *** -->                  
      <xsl:for-each select=""//s0:DTM/s0:C507"">
        <xsl:choose>
          <xsl:when test=""C50701 = '2'"">            
              <DeliveryDate>
               <xsl:value-of select=""userCSharp:editDate(C50702/text())"" />
             </DeliveryDate>
          </xsl:when>           
        </xsl:choose>
      </xsl:for-each> 
     </DeliveryInfo>    
<!-- ***  END OF  DTM *** -->    
           
           </Delivery>   
          </xsl:when>
        </xsl:choose>    
      </xsl:for-each> 
      
<!-- ***  END OF NAD  *** -->
      
  <!-- *** TAX *** -->                       
          <xsl:for-each select=""s0:TAXLoop1"">
            <TaxTotal>
              <xsl:for-each select=""s0:MOA"">
                <xsl:choose>
                  <xsl:when test=""s0:C516/C51601 = '124'"">
                    <TaxAmount>
                      <xsl:value-of select=""s0:C516/C51602/text()"" />
                    </TaxAmount>
                    <TaxAmountCode>
                      <xsl:text>124</xsl:text>
                    </TaxAmountCode>
                  </xsl:when>
                </xsl:choose> 
               </xsl:for-each> 
<!--              </xsl:if> -->         
          <TaxSubtotal>
            <xsl:for-each select=""s0:MOA""> 
<!--            <xsl:if test=""s0:MOA/s0:C516/C51602""> -->
             <xsl:choose>
               <xsl:when test=""s0:C516/C51601 = '124'"">
                 <TaxAmount>
                   <xsl:value-of select=""s0:C516/C51602/text()"" />                 
                 </TaxAmount>
                 <TaxAmountCode>
                   <xsl:text>124</xsl:text>                 
                 </TaxAmountCode>
               </xsl:when>
                <xsl:when test=""s0:C516/C51601 = '125'"">
                 <TaxableAmount>
                   <xsl:value-of select=""s0:C516/C51602/text()"" />                 
                 </TaxableAmount>
                 <TaxableAmountCode>
                   <xsl:text>125</xsl:text>                 
                 </TaxableAmountCode>
               </xsl:when>             
             </xsl:choose>             
<!--          </xsl:if>                                           -->
            </xsl:for-each>
                          
           <xsl:if test=""s0:TAX/TAX01"">
            <TaxCode>
              <xsl:value-of select=""s0:TAX/TAX01/text()"" />
            </TaxCode>
          </xsl:if>  
          <xsl:if test=""s0:TAX/s0:C243/C24304"">
            <TaxPercent>
              <xsl:value-of select=""s0:TAX/s0:C243/C24304/text()"" />
            </TaxPercent>
          </xsl:if>
          <TaxCategory>
            <xsl:if test=""s0:TAX/TAX06"">
              <xsl:choose>
                <xsl:when test=""s0:TAX/TAX06 = 'S'"">
                  <ID>
                    <xsl:text>StandardRated</xsl:text>
                  </ID>                
                </xsl:when>
                <xsl:when test=""s0:TAX/TAX06 = 's'"">
                  <ID>
                    <xsl:text>StandardRated</xsl:text>
                  </ID>                
                </xsl:when>
                <xsl:when test=""s0:TAX/TAX06 = 'E'"">
                  <ID>
                    <xsl:text>ZeroRated</xsl:text>
                  </ID>                
                </xsl:when>
               <xsl:when test=""s0:TAX/TAX06 = 'e'"">
                  <ID>
                    <xsl:text>ZeroRated</xsl:text>
                  </ID>                
                </xsl:when> 
                <xsl:when test=""s0:TAX/TAX06 = 'AE'"">
                  <ID>
                    <xsl:text>ReverseCharge</xsl:text>
                  </ID>                
                </xsl:when>
              <xsl:when test=""s0:TAX/TAX06 = 'ae'"">
                  <ID>
                    <xsl:text>ReverseCharge</xsl:text>
                  </ID>                
                </xsl:when> 
              </xsl:choose>            
            </xsl:if>  
            <xsl:if test=""s0:TAX/s0:C243/C24304"">
              <Percent>
                <xsl:value-of select=""s0:TAX/s0:C243/C24304/text()"" />
              </Percent>
            </xsl:if>
            <TaxScheme>
              <xsl:if test=""s0:TAX/s0:C241/C24101"">
                <Navn>
                  <xsl:value-of select=""s0:TAX/s0:C241/C24101/text()"" />
                </Navn>
              </xsl:if>
              <xsl:if test=""s0:TAX/TAX06"">
                <TaxTypeCode>
                  <xsl:value-of select=""s0:TAX/TAX06/text()"" />
                </TaxTypeCode>
              </xsl:if>
            </TaxScheme>
          </TaxCategory>          
        
       </TaxSubtotal>
      </TaxTotal>      
    </xsl:for-each>  
      <!-- ***  END OF TAX *** -->               
      
      <!-- *** ALC *** -->
      <xsl:for-each select=""s0:ALCLoop1"">
        <AllowanceCharge>
          <xsl:choose>
            <xsl:when test=""s0:ALC/ALC01 = 'A'"">
              <ChargeIndicator>
                <xsl:value-of select=""'False'"" />
              </ChargeIndicator>
            </xsl:when>
            <xsl:when test=""s0:ALC/ALC01 = 'C'"">
              <ChargeIndicator>
                <xsl:value-of select=""'True'"" />
              </ChargeIndicator>              
            </xsl:when>
            <xsl:otherwise>
               <xsl:value-of select=""'XZ'"" /> 
            </xsl:otherwise>         
          </xsl:choose>
          
          <xsl:if test=""s0:ALC/s0:C552_2/C55201""> 
           <ID>
            <xsl:value-of select=""s0:ALC/s0:C552_2/C55201/text()"" />
           </ID>
          </xsl:if>  
              
          <xsl:if test=""s0:ALC/s0:C214/C21401""> 
              <AllowanceChargeReasonCode>
                <xsl:value-of select=""s0:ALC/s0:C214/C21401/text()"" />
            </AllowanceChargeReasonCode>
          </xsl:if>
          <xsl:if test=""s0:ALC/s0:C214/C21404""> 
              <AllowanceChargeReason>
                <xsl:value-of select=""s0:ALC/s0:C214/C21404/text()"" />
            </AllowanceChargeReason>
          </xsl:if>          
          <xsl:if test=""s0:ALC/ALC04"">
           <SequenceNumeric>
            <xsl:value-of select=""s0:ALC/ALC04/text()"" />
           </SequenceNumeric>
          </xsl:if> 
          <xsl:if test=""s0:PCDLoop1/s0:PCD_3/s0:C501_3"">
            <MultiplierFactorNumeric>
              <xsl:value-of select=""userCSharp:DivideAmount(s0:PCDLoop1/s0:PCD_3/s0:C501_3/C50102/text())"" />                    
            </MultiplierFactorNumeric>
            <xsl:if test=""s0:PCDLoop1/s0:PCD_3/s0:C501_3/C50101"">
             <MultiplierFactorNumericCode>
              <xsl:value-of select=""s0:PCDLoop1/s0:PCD_3/s0:C501_3/C50102/text()"" />
             </MultiplierFactorNumericCode>             
            </xsl:if>          
          </xsl:if>
          <xsl:if test=""s0:MOALoop1/s0:MOA_3/s0:C516_3/C51602"">
           <Amount>
             <xsl:value-of select=""s0:MOALoop1/s0:MOA_3/s0:C516_3/C51602/text()"" />     
            </Amount>
             <xsl:if test=""string-length(s0:MOALoop1/s0:MOA_3/s0:C516_3/C51601) != 0""> 
              <AmountCode>
               <xsl:value-of select=""s0:MOALoop1/s0:MOA_3/s0:C516_3/C51601/text()"" />                  
              </AmountCode>                
             </xsl:if>                                       
           </xsl:if>  
        </AllowanceCharge>
      </xsl:for-each>
      <!--  END OF  ALC -->          
      
      <!-- *** MOA Totals *** -->
      <Total>
        <xsl:for-each select=""s0:MOA_11/s0:C516_11"">
          <xsl:choose>
            <xsl:when test=""C51601 = '79'"">
              <LineTotalAmount>
                <xsl:value-of select=""C51602/text()"" />  
              </LineTotalAmount>
            </xsl:when>
            <xsl:when test=""C51601 = '125'"">
              <TaxExclAmount>
                <xsl:value-of select=""C51602/text()"" />  
              </TaxExclAmount>
            </xsl:when>
            <!--
            <xsl:when test=""C51601 = '86'"">
              <TaxInclAmount>
                <xsl:value-of select=""C51602/text()"" />  
              </TaxInclAmount>
            </xsl:when> -->
            <xsl:when test=""C51601 = '204'"">
              <AllowanceTotalAmount>
                <xsl:value-of select=""C51602/text()"" />  
              </AllowanceTotalAmount>
            </xsl:when> 
            <xsl:when test=""C51601 = '23'"">
              <ChargeTotalAmount>
                <xsl:value-of select=""C51602/text()"" />  
              </ChargeTotalAmount>
            </xsl:when> 
            <xsl:when test=""C51601 = '113'"">
              <PrepaidAmount>
                <xsl:value-of select=""C51602/text()"" />  
              </PrepaidAmount>
            </xsl:when> 
            <xsl:when test=""C51601 = '86'"">
              <PayableAmount>
                <xsl:value-of select=""C51602/text()"" />  
              </PayableAmount>
            </xsl:when>
            <xsl:otherwise>
              <ExtraTotalMOALoop1>
                <Node>
                  <Code>
                    <xsl:value-of select=""C51601/text()"" />                    
                  </Code>
                  <Amount>
                    <xsl:value-of select=""C51602/text()"" />                    
                  </Amount>                
                </Node>              
              </ExtraTotalMOALoop1>            
            </xsl:otherwise>         
          </xsl:choose>          
        </xsl:for-each>
      </Total> 
      
      <!-- *** END  OF  MOA Totals *** -->
                   
<!-- *** LIN Lines *** -->
      <Lines>
        <xsl:for-each select=""s0:LINLoop1"">
          <Line>
            
            <LineNo>
              <xsl:value-of select=""s0:LIN/LIN01/text()"" />
            </LineNo>
            
           <!-- *** LIN QTY *** -->    
            <Quantity>
             <xsl:for-each select=""s0:QTY_3/s0:C186_3"">
              <Node>
               <Code>
                <xsl:value-of select=""C18601/text()"" />                                
               </Code>
               <Quantity>
                 <xsl:value-of select=""C18602/text()"" />               
               </Quantity>           
            <xsl:if test=""C18603"">
              <UnitCode>
                <xsl:value-of select=""userCSharp:ConvertUnitCode('TUNtoUBL', C18603/text())"" />
                <!--   <xsl:value-of select=""C18603/text()"" /> -->
              </UnitCode>
             </xsl:if> 
            </Node>   
              </xsl:for-each>
            </Quantity>                       
           <!-- *** END OF LIN QTY *** -->
            
                        <!-- *** LIN LIN/PIA *** -->
<!--             
              <xsl:if test=""s0:PIA/s0:C212_2/C21201"">
                <SellerItemID>
                  <xsl:value-of select=""s0:PIA/s0:C212_2/C21201/text()"" />
                </SellerItemID>
              </xsl:if>
              <xsl:if test=""s0:PIA/s0:C212_3/C21201"">
                <AdditionalItemID>
                  <xsl:value-of select=""s0:PIA/s0:C212_3/C21201/text()"" />
                </AdditionalItemID>
              </xsl:if>
-->              <Item>
  
               <xsl:if test=""s0:LIN/s0:C212/C21201"">
                <StandardItemID>
                  <xsl:value-of select=""s0:LIN/s0:C212/C21201/text()"" />
                </StandardItemID>
                <ShemeID>
                  <xsl:if test=""s0:LIN/s0:C212/C21202"">
                  <StdItemID>
                    <xsl:value-of select=""s0:LIN/s0:C212/C21202/text()"" />                  
                  </StdItemID>
                  </xsl:if>
                </ShemeID>                
              </xsl:if>
               
  <!--
              <xsl:for-each select=""s0:PIA"">
                <xsl:sort select=""s0:C212_2/C21202"" order=""ascending"" data-type=""text""></xsl:sort>
               <xsl:choose>            
                 
                     
                <xsl:when test=""s0:C212_2/C21202 = 'SA'"">
                  <SellerItemID>
                    <xsl:value-of select=""s0:C212_2/C21201/text()"" />
                  </SellerItemID>               
                </xsl:when>
                 
                                    <xsl:when test=""s0:C212_2/C21202 = 'BP'"">
                  <BuyerItemID>
                    <xsl:value-of select=""s0:C212_2/C21201/text()"" />
                  </BuyerItemID>               
                </xsl:when>                 
                 
                   
                   
                   <xsl:when test=""s0:C212_2/C21202 = 'MP'"">
                  <AdditionalItemID>
                    <xsl:value-of select=""s0:C212_2/C21201/text()"" />
                  </AdditionalItemID>               
                </xsl:when>
              </xsl:choose>              
            </xsl:for-each>                       
        -->
  
           <!--    *** PIA *** -->
                
              <xsl:for-each select=""s0:PIA"">
                <xsl:sort select=""s0:C212_2/C21202"" order=""ascending"" data-type=""text""></xsl:sort>
               <xsl:choose>            
                 
                     
                <xsl:when test=""s0:C212_2/C21202 = 'SA'"">
                  <SellerItemID>
                    <xsl:value-of select=""s0:C212_2/C21201/text()"" />
                  </SellerItemID>               
                </xsl:when>
                 
                 <xsl:when test=""s0:C212_2/C21202 = 'BP'"">
                  <BuyerItemID>
                    <xsl:value-of select=""s0:C212_2/C21201/text()"" />
                  </BuyerItemID>               
                </xsl:when>                                                     
                   
                 <xsl:when test=""s0:C212_2/C21202 = 'MP'"">
                  <AdditionalItemID>
                    <xsl:value-of select=""s0:C212_2/C21201/text()"" />
                  </AdditionalItemID>            
                </xsl:when>
               
                 <xsl:when test=""s0:C212_2/C21202 = 'MF'"">
                  <ManufacturerItemID>
                    <xsl:value-of select=""s0:C212_2/C21201/text()"" />
                  </ManufacturerItemID>               
                </xsl:when>

                 <xsl:otherwise>
                   <ExtraPIALoop>
                     <Node>
                       <xsl:if test=""PIA01"">
                        <DigitalCode>
                          <xsl:value-of select=""PIA01/text()"" />                       
                        </DigitalCode>
                       </xsl:if>
                       <PIA>
                         <xsl:value-of select=""s0:C212_2/C21201/text()"" />                       
                       </PIA>
                       <Code>
                         <xsl:value-of select=""s0:C212_2/C21202/text()"" />                                              
                       </Code>
                     </Node>                   
                   </ExtraPIALoop>                 
                 </xsl:otherwise> 
                          
              </xsl:choose>     
                
       <!-- 212_3-->         
               <xsl:choose>            
                                      
                <xsl:when test=""s0:C212_3/C21202 = 'SA'"">
                  <SellerItemID>
                    <xsl:value-of select=""s0:C212_3/C21201/text()"" />
                  </SellerItemID>               
                </xsl:when>
                 
                 <xsl:when test=""s0:C212_2/C21203 = 'BP'"">
                  <BuyerItemID>
                    <xsl:value-of select=""s0:C212_3/C21201/text()"" />
                  </BuyerItemID>               
                </xsl:when>                                                     
                   
                 <xsl:when test=""s0:C212_2/C21203 = 'MP'"">
                  <AdditionalItemID>
                    <xsl:value-of select=""s0:C212_3/C21201/text()"" />
                  </AdditionalItemID>              
                </xsl:when>
               
                 <xsl:when test=""s0:C212_2/C21203 = 'MF'"">
                  <ManufacturerItemID>
                    <xsl:value-of select=""s0:C212_3/C21201/text()"" />
                  </ManufacturerItemID>               
                </xsl:when>
               
              </xsl:choose>                 
                
       <!-- 212_4-->         
               <xsl:choose>            
                                      
                <xsl:when test=""s0:C212_4/C21202 = 'SA'"">
                  <SellerItemID>
                    <xsl:value-of select=""s0:C212_4/C21201/text()"" />
                  </SellerItemID>               
                </xsl:when>
                 
                 <xsl:when test=""s0:C212_4/C21203 = 'BP'"">
                  <BuyerItemID>
                    <xsl:value-of select=""s0:C212_4/C21201/text()"" />
                  </BuyerItemID>               
                </xsl:when>                                                     
                   
                 <xsl:when test=""s0:C212_4/C21203 = 'MP'"">
                  <AdditionalItemID>
                    <xsl:value-of select=""s0:C212_4/C21201/text()"" />
                  </AdditionalItemID>              
                </xsl:when>
               
                 <xsl:when test=""s0:C212_4/C21203 = 'MF'"">
                  <ManufacturerItemID>
                    <xsl:value-of select=""s0:C212_4/C21201/text()"" />
                  </ManufacturerItemID>               
                </xsl:when>
                             
              </xsl:choose>    
                
       <!-- 212_5-->         
               <xsl:choose>                                 
                <xsl:when test=""s0:C212_5/C21202 = 'SA'"">
                  <SellerItemID>
                    <xsl:value-of select=""s0:C212_5/C21201/text()"" />
                  </SellerItemID>               
                </xsl:when>
                 
                 <xsl:when test=""s0:C212_5/C21203 = 'BP'"">
                  <BuyerItemID>
                    <xsl:value-of select=""s0:C212_5/C21201/text()"" />
                  </BuyerItemID>               
                </xsl:when>                 
                   
                 <xsl:when test=""s0:C212_5/C21203 = 'MP'"">
                  <AdditionalItemID>
                    <xsl:value-of select=""s0:C212_5/C21201/text()"" />
                  </AdditionalItemID>              
                </xsl:when>
               
                 <xsl:when test=""s0:C212_5/C21203 = 'MF'"">
                  <ManufacturerItemID>
                    <xsl:value-of select=""s0:C212_5/C21201/text()"" />
                  </ManufacturerItemID>               
                </xsl:when>
                 
              </xsl:choose>   
                
       <!-- 212_6-->         
               <xsl:choose>                             
                     
                <xsl:when test=""s0:C212_6/C21202 = 'SA'"">
                  <SellerItemID>
                    <xsl:value-of select=""s0:C212_6/C21201/text()"" />
                  </SellerItemID>               
                </xsl:when>
                 
                 <xsl:when test=""s0:C212_6/C21203 = 'BP'"">
                  <BuyerItemID>
                    <xsl:value-of select=""s0:C212_6/C21201/text()"" />
                  </BuyerItemID>               
                </xsl:when>                                  
                                      
                 <xsl:when test=""s0:C212_6/C21203 = 'MP'"">
                  <AdditionalItemID>
                    <xsl:value-of select=""s0:C212_6/C21201/text()"" />
                  </AdditionalItemID>              
                </xsl:when>
               
                 <xsl:when test=""s0:C212_6/C21203 = 'MF'"">
                  <ManufacturerItemID>
                    <xsl:value-of select=""s0:C212_6/C21201/text()"" />
                  </ManufacturerItemID>               
                </xsl:when>   
                                
              </xsl:choose>                  
            </xsl:for-each>
            
        <!--    *** END OF PIA  *** --> 
              

               <!-- *** LIN IMD *** -->
              <Name>
                <xsl:value-of select=""s0:IMD_2/s0:C273_2/C27304/text()"" />
                <xsl:if test=""s0:IMD_2/s0:C273_2/C27305"">
                  <xsl:value-of select=""':'"" />
                  <xsl:value-of select=""s0:IMD_2/s0:C273_2/C27305/text()"" />
                </xsl:if>   
              </Name>

             <!-- *** END OF LIN IMD *** -->  
            
            </Item>
            
          <!--  *** DTM *** -->
            
       <DTMReference>
        <xsl:for-each select=""s0:DTM_13/s0:C507_13"">
          <Node>
           <Code>
             <xsl:value-of select=""C50701/text()"" />           
           </Code>
            <TimeStamp>
              <xsl:value-of select=""userCSharp:editDate(C50702/text())"" />            
            </TimeStamp>
          </Node>           
        </xsl:for-each>                      
      </DTMReference>
        
          <!--  *** END OF DTM *** -->        
            
      <!--  FTX SEGMENT -->
      
       <xsl:for-each select=""s0:FTX_6"">
       <NoteLoop>    
         <Node>
       <xsl:if test=""FTX01"">
         <NoteQualifier>  
          <xsl:value-of select=""FTX01/text()"" />      
         </NoteQualifier>    
       </xsl:if>  
        <xsl:if test=""FTX02"">
         <NoteFunction>  
          <xsl:value-of select=""FTX02/text()"" />      
         </NoteFunction>    
       </xsl:if>  
        <xsl:if test=""FTX05"">
         <NoteLanguage>  
          <xsl:value-of select=""FTX05/text()"" />      
         </NoteLanguage>    
       </xsl:if>         
        <Note>
          <xsl:value-of select=""s0:C108_6/C10801/text()"" />
        </Note>  
       </Node>
         
          <xsl:if test=""s0:C108_6/C10802"">
         <Node>
           <xsl:if test=""FTX01"">
         <NoteQualifier>  
          <xsl:value-of select=""FTX01/text()"" />      
         </NoteQualifier>    
       </xsl:if>  
        <xsl:if test=""FTX02"">
         <NoteFunction>  
          <xsl:value-of select=""FTX02/text()"" />      
         </NoteFunction>    
       </xsl:if>  
        <xsl:if test=""FTX05"">
         <NoteLanguage>  
          <xsl:value-of select=""FTX05/text()"" />      
         </NoteLanguage>    
       </xsl:if>         
          <Note>
            <xsl:value-of select=""s0:C108_6/C10802/text()"" />        
          </Note>
         </Node>             
          </xsl:if>
       
         
          <xsl:if test=""s0:C108_6/C10803"">
            <Node>
              <xsl:if test=""FTX01"">
         <NoteQualifier>  
          <xsl:value-of select=""FTX01/text()"" />      
         </NoteQualifier>    
       </xsl:if>  
        <xsl:if test=""FTX02"">
         <NoteFunction>  
          <xsl:value-of select=""FTX02/text()"" />      
         </NoteFunction>    
       </xsl:if>  
        <xsl:if test=""FTX05"">
         <NoteLanguage>  
          <xsl:value-of select=""FTX05/text()"" />      
         </NoteLanguage>    
       </xsl:if>         
            <Note>
             <xsl:value-of select=""s0:C108_6/C10803/text()"" />        
            </Note>
           </Node>   
          </xsl:if>
         
          <xsl:if test=""s0:C108_6/C10804""> 
          <Node>
            <xsl:if test=""FTX01"">
         <NoteQualifier>  
          <xsl:value-of select=""FTX01/text()"" />      
         </NoteQualifier>    
       </xsl:if>  
        <xsl:if test=""FTX02"">
         <NoteFunction>  
          <xsl:value-of select=""FTX02/text()"" />      
         </NoteFunction>    
       </xsl:if>  
        <xsl:if test=""FTX05"">
         <NoteLanguage>  
          <xsl:value-of select=""FTX05/text()"" />      
         </NoteLanguage>    
       </xsl:if>         
           <Note>
            <xsl:value-of select=""s0:C108_6/C10804/text()"" />        
          </Note>
          </Node>  
          </xsl:if>

          <xsl:if test=""s0:C108_6/C10805""> 
           <Node>
             <xsl:if test=""FTX01"">
         <NoteQualifier>  
          <xsl:value-of select=""FTX01/text()"" />      
         </NoteQualifier>    
       </xsl:if>  
        <xsl:if test=""FTX02"">
         <NoteFunction>  
          <xsl:value-of select=""FTX02/text()"" />      
         </NoteFunction>    
       </xsl:if>  
        <xsl:if test=""FTX05"">
         <NoteLanguage>  
          <xsl:value-of select=""FTX05/text()"" />      
         </NoteLanguage>    
       </xsl:if>         
            <Note>
            <xsl:value-of select=""s0:C108_6/C10805/text()"" />        
           </Note>  
          </Node>   
         </xsl:if>      
       
      </NoteLoop>     
      </xsl:for-each>    
      
      <!--  END OF  FTX -->   
            
    <!--  *** RFF *** -->

      <OrderReference>      
      <xsl:for-each select=""s0:RFFLoop3"">
        <Node>
          <Code>            
           <xsl:value-of select=""s0:RFF_6/s0:C506_6/C50601/text()"" />
          </Code>
          <Reference>
            <xsl:value-of select=""s0:RFF_6/s0:C506_6/C50602/text()"" />          
          </Reference>        
        </Node>          
      </xsl:for-each>              
      </OrderReference>    
      
      <!--  *** END OF RFF *** -->             

            <!-- *** LIN MOA 203 *** -->
            <xsl:if test=""s0:MOA_5/s0:C516_5/C51602"">
              <LineAmountTotal>
                <xsl:value-of select=""s0:MOA_5/s0:C516_5/C51602/text()"" />
              </LineAmountTotal>
              <xsl:if test=""s0:MOA_5/s0:C516_5/C51603"">
                <Currency>
                  <xsl:value-of select=""s0:MOA_5/s0:C516_5/C51603/text()"" />
                </Currency>
              </xsl:if>
            </xsl:if>
            <!-- *** END  OF  LIN MOA 203 *** -->

      <!-- *** LIN PRI *** -->
            <xsl:for-each select=""s0:PRILoop1/s0:PRI/s0:C509"">
              <xsl:choose>
                <xsl:when test=""C50901 = 'AAA'"">
                  <PriceNet>
                    <Price>
                      <xsl:value-of select=""C50902/text()"" /> 
                    </Price>
                   <xsl:if test=""string-length(C50905) != 0""> 
                    <Quantity>
                      <xsl:value-of select=""C50905/text()"" />              
                    </Quantity>
                   </xsl:if>  
                   <xsl:if test=""string-length(C50906) != 0""> 
                    <UnitCode>
                      <xsl:value-of select=""userCSharp:ConvertUnitCode('TUNtoUBL', C50906/text())"" />
                 <!--   <xsl:value-of select=""C50906/text()"" /> -->
                    </UnitCode>
                   </xsl:if>   
                    <PristypeCode>
                      <xsl:value-of select=""'AAA'"" />
                    </PristypeCode>
                  </PriceNet>  
                </xsl:when>
                <xsl:when test=""C50901 = 'AAB'"">
                  <PriceGross>
                    <Price>
                      <xsl:value-of select=""C50902/text()"" /> 
                    </Price>
                   <xsl:if test=""string-length(C50905) != 0""> 
                    <Quantity>
                      <xsl:value-of select=""C50905/text()"" />              
                    </Quantity>
                   </xsl:if>  
                   <xsl:if test=""string-length(C50906) != 0""> 
                    <UnitCode>
                      <xsl:value-of select=""userCSharp:ConvertUnitCode('TUNtoUBL', C50906/text())"" />
                      <!--   <xsl:value-of select=""C50906/text()"" /> -->
                    </UnitCode>
                   </xsl:if>   
                    <PristypeCode>
                      <xsl:value-of select=""'AAB'"" />
                    </PristypeCode>
                  </PriceGross>  
                </xsl:when>
              </xsl:choose>
            </xsl:for-each>                                 
      <!-- *** END OF LIN PRI *** -->  
            
     <!-- *** LIN  TAX SEGMENT *** -->
            
         <xsl:for-each select=""s0:TAXLoop3"">   
         <TaxTotal> 
          <xsl:for-each select=""s0:MOA_7"">  
          <!-- <xsl:if test=""s0:MOA_7/s0:C516_7/C51602""> -->
             <xsl:choose>
               <xsl:when test=""s0:C516_7/C51601 = '124'"">
                 <TaxAmount>
                   <xsl:value-of select=""s0:C516_7/C51602/text()"" />                 
                 </TaxAmount>
                 <TaxAmountCode>
                   <xsl:text>124</xsl:text>                 
                 </TaxAmountCode>
               </xsl:when>
             </xsl:choose>  
            </xsl:for-each>
          <!-- </xsl:if> -->
          <TaxSubtotal>
           <xsl:for-each select=""s0:MOA_7""> 
           <!-- <xsl:if test=""s0:MOA_7/s0:C516_7/C51602""> -->
             <xsl:choose>
               <xsl:when test=""s0:C516_7/C51601 = '124'"">
                 <TaxAmount>
                   <xsl:value-of select=""s0:C516_7/C51602/text()"" />                 
                 </TaxAmount>
                 <TaxAmountCode>
                   <xsl:text>124</xsl:text>                 
                 </TaxAmountCode>
               </xsl:when>
                <xsl:when test=""s0:C516_7/C51601 = '125'"">
                 <TaxableAmount>
                   <xsl:value-of select=""s0:C516_7/C51602/text()"" />                 
                 </TaxableAmount>
                 <TaxableAmountCode>
                   <xsl:text>125</xsl:text>                 
                 </TaxableAmountCode>
               </xsl:when>             
             </xsl:choose>   
            </xsl:for-each> 
          <!-- </xsl:if>  -->
          <xsl:if test=""s0:TAX_3/TAX01""> 
            <TaxCode>
              <xsl:value-of select=""s0:TAX_3/TAX01/text()"" />
            </TaxCode>
          </xsl:if>  
          <xsl:if test=""s0:TAX_3/s0:C243_3/C24304"">
            <TaxPercent>
              <xsl:value-of select=""s0:TAX_3/s0:C243_3/C24304/text()"" />
            </TaxPercent>
          </xsl:if>
          <TaxCategory>
             <xsl:if test=""s0:TAX_3/TAX06"">
              <xsl:choose>
                <xsl:when test=""s0:TAX_3/TAX06 = 'S'"">
                  <ID>
                    <xsl:text>StandardRated</xsl:text>
                  </ID>                
                </xsl:when>
                <xsl:when test=""s0:TAX_3/TAX06 = 's'"">
                  <ID>
                    <xsl:text>StandardRated</xsl:text>
                  </ID>                
                </xsl:when>
                <xsl:when test=""s0:TAX_3/TAX06 = 'E'"">
                  <ID>
                    <xsl:text>ZeroRated</xsl:text>
                  </ID>                
                </xsl:when>
               <xsl:when test=""s0:TAX_3/TAX06 = 'e'"">
                  <ID>
                    <xsl:text>ZeroRated</xsl:text>
                  </ID>                
                </xsl:when> 
                <xsl:when test=""s0:TAX_3/TAX06 = 'AE'"">
                  <ID>
                    <xsl:text>ReverseCharge</xsl:text>
                  </ID>                
                </xsl:when>
              <xsl:when test=""s0:TAX_3/TAX06 = 'ae'"">
                  <ID>
                    <xsl:text>ReverseCharge</xsl:text>
                  </ID>                
                </xsl:when>
              </xsl:choose>            
            </xsl:if> 
            <xsl:if test=""s0:TAX_3/s0:C243_3/C24304"">
              <Percent>
                <xsl:value-of select=""s0:TAX_3/s0:C243_3/C24304/text()"" />
              </Percent>
            </xsl:if>
            <TaxScheme>
              <xsl:if test=""s0:TAX_3/s0:C241_3/C24101"">
                <Navn>
                  <xsl:value-of select=""s0:TAX_3/s0:C241_3/C24101/text()"" />
                </Navn>
              </xsl:if>
              <xsl:if test=""s0:TAX_3/TAX06"">
                <TaxTypeCode>
                  <xsl:value-of select=""s0:TAX_3/TAX06/text()"" />
                </TaxTypeCode>
              </xsl:if>
            </TaxScheme>
          </TaxCategory>
         </TaxSubtotal>
        </TaxTotal>                  
       </xsl:for-each>
      
      <!-- ***  END OF  LIN TAX *** -->           

      <!-- *** LIN ALC *** -->
            
            <xsl:for-each select=""s0:ALCLoop2"">
              <AllowanceCharge>
                <xsl:choose>
                  <xsl:when test=""s0:ALC_2/ALC01 = 'A'"">
                    <ChargeIndicator>
                      <xsl:value-of select=""'False'"" />
                    </ChargeIndicator>
                  </xsl:when>
                  <xsl:when test=""s0:ALC_2/ALC01 = 'C'"">
                    <ChargeIndicator>
                      <xsl:value-of select=""'True'"" />
                    </ChargeIndicator>
                  </xsl:when>
                  <xsl:otherwise>
                    <ChargeIndicator>
                      <xsl:value-of select=""'XZ'"" />
                    </ChargeIndicator>                  
                 </xsl:otherwise>
                </xsl:choose>
                
                <xsl:if test=""s0:ALC_2/s0:C552_2/C55201""> 
                   <ID>
                     <xsl:value-of select=""s0:ALC_2/s0:C552_2/C55201/text()"" />
                  </ID>
                </xsl:if>
                
                <xsl:if test=""s0:ALC_2/s0:C214_2/C21401""> 
                   <AllowanceChargeReasonCode>
                     <xsl:value-of select=""s0:ALC_2/s0:C214_2/C21401/text()"" />
                  </AllowanceChargeReasonCode>
                </xsl:if>
                
                <xsl:if test=""s0:ALC_2/s0:C214_2/C21404"">
                  <AllowanceChargeReason>
                    <xsl:value-of select=""s0:ALC_2/s0:C214_2/C21404/text()"" />
                  </AllowanceChargeReason>
                </xsl:if>
                 <xsl:if test=""s0:ALC_2/ALC04"">
                  <SequenceNumeric>
                    <xsl:value-of select=""s0:ALC_2/ALC04/text()"" />
                  </SequenceNumeric>
                </xsl:if>
                <xsl:if test=""s0:PCDLoop2/s0:PCD_6/s0:C501_6"">
                  <MultiplierFactorNumeric>
                    <xsl:value-of select=""userCSharp:DivideAmount(s0:PCDLoop2/s0:PCD_6/s0:C501_6/C50102/text())"" />
                  </MultiplierFactorNumeric>
                  <xsl:if test=""s0:PCDLoop2/s0:PCD_6/s0:C501_6/C50101"">
                   <MultiplierFactorNumericCode>
                    <xsl:value-of select=""s0:PCDLoop2/s0:PCD_6/s0:C501_6/C50101/text()"" />                     
                   </MultiplierFactorNumericCode>       
                  </xsl:if>                                          
                </xsl:if>
              <xsl:if test=""s0:MOALoop2/s0:MOA_8/s0:C516_8/C51602"">
                <Amount>
                  <xsl:value-of select=""s0:MOALoop2/s0:MOA_8/s0:C516_8/C51602/text()"" />             
                </Amount>
                <xsl:if test=""string-length(s0:MOALoop2/s0:MOA_8/s0:C516_8/C51601) != 0""> 
                  <AmountCode>
                    <xsl:value-of select=""s0:MOALoop2/s0:MOA_8/s0:C516_8/C51601/text()"" />                  
                  </AmountCode>                
                </xsl:if>                  
               </xsl:if>   
              </AllowanceCharge>
            </xsl:for-each>     
            
        <!-- ***  END OF LIN ALC *** -->            
            
          </Line>
        </xsl:for-each>
      </Lines>  
      
    
<!-- *** END OF LIN Lines *** -->  
  
  
    </ns0:Order>
  </xsl:template>
  
  
  <msxsl:script language=""C#"" implements-prefix=""userCSharp"">
    <![CDATA[
   
    public string editDate(string aDate)
  {
      string NewDate;
      NewDate = string.Concat(aDate.Substring(0,4), '-', aDate.Substring(4,2), '-', aDate.Substring(6,2));
      return NewDate;
  }
  
    public string DivideAmount(Double aAmount)
  {
      string NewAmount;
      NewAmount = string.Format(""{0:0.00}"", (aAmount / 100));
      return NewAmount.Replace("","",""."");
  } 
  
public string ConvertUnitCode(string ConversionDirection, string IngoingUnitCode)
  {
  string OutgoingUnitCode = """";

  switch (ConversionDirection)
  {
  case ""TUNtoUBL"":
  switch (IngoingUnitCode)
  {
  case ""%KG"":
  OutgoingUnitCode = ""HK"";
  break;
  case ""HMT"":
  OutgoingUnitCode = ""HMT"";
  break;
  case ""%ST"":
  OutgoingUnitCode = ""CNP"";
  break;
  case ""TUS"":
  OutgoingUnitCode = ""T3"";
  break;
  case ""ST"":
  OutgoingUnitCode = ""ST"";
  break;
  case ""BB"":
  OutgoingUnitCode = ""BB"";
  break;
  case ""BDT"":
  OutgoingUnitCode = ""BE"";
  break;
  case ""BLK"":
  OutgoingUnitCode = ""D64"";
  break;
  case ""BX"":
  OutgoingUnitCode = ""BX"";
  break;
  case ""PCE"":
  OutgoingUnitCode = ""C62"";
  break;
  case ""BEG"":
  OutgoingUnitCode = ""CU"";
  break;
  case ""BOT"":
  OutgoingUnitCode = ""2W"";
  break;
  case ""CMT"":
  OutgoingUnitCode = ""CMT"";
  break;
  case ""CH"":
  OutgoingUnitCode = ""CH"";
  break;
  case ""DAY"":
  OutgoingUnitCode = ""DAY"";
  break;
  case ""DIS"":
  OutgoingUnitCode = ""DS"";
  break;
  case ""DLT"":
  OutgoingUnitCode = ""DLT"";
  break;
  case ""DMT"":
  OutgoingUnitCode = ""DMT"";
  break;
  case ""DK"":
  OutgoingUnitCode = ""CA"";
  break;
  case ""DS"":
  OutgoingUnitCode = ""TN"";
  break;
  case ""FL"":
  OutgoingUnitCode = ""BO"";
  break;
  case ""FOT"":
  OutgoingUnitCode = ""FOT"";
  break;
  case ""GRM"":
  OutgoingUnitCode = ""GRM"";
  break;
  case ""GRO"":
  OutgoingUnitCode = ""GRO"";
  break;
  case ""HLT"":
  OutgoingUnitCode = ""HLT"";
  break;
  case ""KAR"":
  OutgoingUnitCode = ""CT"";
  break;
  case ""KS"":
  OutgoingUnitCode = ""Z2"";
  break;
  case ""KGM"":
  OutgoingUnitCode = ""KGM"";
  break;
  case ""CMQ"":
  OutgoingUnitCode = ""CMQ"";
  break;
  case ""MTQ"":
  OutgoingUnitCode = ""MTQ"";
  break;
  case ""CMK"":
  OutgoingUnitCode = ""CMK"";
  break;
  case ""MTK"":
  OutgoingUnitCode = ""MTK"";
  break;
  case ""LGD"":
  OutgoingUnitCode = ""LN"";
  break;
  case ""LTR"":
  OutgoingUnitCode = ""LTR"";
  break;
  case ""LES"":
  OutgoingUnitCode = ""NL"";
  break;
  case ""MTR"":
  OutgoingUnitCode = ""MTR"";
  break;
  case ""MLT"":
  OutgoingUnitCode = ""MLT"";
  break;
  case ""MMT"":
  OutgoingUnitCode = ""MMT"";
  break;
  case ""PAK"":
  OutgoingUnitCode = ""PK"";
  break;
  case ""PF"":
  OutgoingUnitCode = ""PF"";
  break;
  case ""PB"":
  OutgoingUnitCode = ""BB"";
  break;
  case ""PAR"":
  OutgoingUnitCode = ""PR"";
  break;
  case ""PAT"":
  OutgoingUnitCode = ""CQ"";
  break;
  case ""PL"":
  OutgoingUnitCode = ""PG"";
  break;
  case ""PS"":
  OutgoingUnitCode = ""BG"";
  break;
  case ""RIN"":
  OutgoingUnitCode = ""RG"";
  break;
  case ""NRL"":
  OutgoingUnitCode = ""RO"";
  break;
  case ""ROR"":
  OutgoingUnitCode = ""TU"";
  break;
  case ""SP"":
  OutgoingUnitCode = ""BJ"";
  break;
  case ""SPO"":
  OutgoingUnitCode = ""SO"";
  break;
  case ""SAK"":
  OutgoingUnitCode = ""SA"";
  break;
  case ""SET"":
  OutgoingUnitCode = ""SET"";
  break;
  case ""HUR"":
  OutgoingUnitCode = ""HUR"";
  break;
  case ""TNE"":
  OutgoingUnitCode = ""TNE"";
  break;
  case ""TR"":
  OutgoingUnitCode = ""DR"";
  break;
  case ""TB"":
  OutgoingUnitCode = ""TB"";
  break;
  case ""ASK"":
  OutgoingUnitCode = ""CS"";
  break;
  case ""BÅND"":
  OutgoingUnitCode = ""BND"";
  break;
  case ""BREV"":
  OutgoingUnitCode = ""BRV"";
  break;
  case ""COL"":
  OutgoingUnitCode = ""COL"";
  break;
  case ""HAP"":
  OutgoingUnitCode = ""HAP"";
  break;
  case ""KORT"":
  OutgoingUnitCode = ""KOR"";
  break;
  case ""ROND"":
  OutgoingUnitCode = ""RON"";
  break;
  case ""SKF"":
  OutgoingUnitCode = ""SKF"";
  break;
  case ""TUBE"":
  OutgoingUnitCode = ""TB"";
  break;
  case ""XXX"":
  OutgoingUnitCode = ""XXX"";
  break;
  default:
  OutgoingUnitCode = IngoingUnitCode;
  break;
  }
  break;

  case ""UBLtoTUN"":

  switch (IngoingUnitCode)
  {
  case ""HK"":
  OutgoingUnitCode = ""%KG"";
  break;
  case ""HMT"":
  OutgoingUnitCode = ""HMT"";
  break;
  case ""CNP"":
  OutgoingUnitCode = ""%ST"";
  break;
  case ""T3"":
  OutgoingUnitCode = ""TUS"";
  break;
  case ""ST"":
  OutgoingUnitCode = ""ST"";
  break;
  case ""BE"":
  OutgoingUnitCode = ""BDT"";
  break;
  case ""D64"":
  OutgoingUnitCode = ""BLK"";
  break;
  case ""BX"":
  OutgoingUnitCode = ""BX"";
  break;
  case ""C62"":
  OutgoingUnitCode = ""PCE"";
  break;
  case ""CU"":
  OutgoingUnitCode = ""BEG"";
  break;
  case ""2W"":
  OutgoingUnitCode = ""BOT"";
  break;
  case ""CMT"":
  OutgoingUnitCode = ""CMT"";
  break;
  case ""CH"":
  OutgoingUnitCode = ""CH"";
  break;
  case ""DAY"":
  OutgoingUnitCode = ""DAY"";
  break;
  case ""DS"":
  OutgoingUnitCode = ""DIS"";
  break;
  case ""DLT"":
  OutgoingUnitCode = ""DLT"";
  break;
  case ""DMT"":
  OutgoingUnitCode = ""DMT"";
  break;
  case ""CA"":
  OutgoingUnitCode = ""DK"";
  break;
  case ""TN"":
  OutgoingUnitCode = ""DS"";
  break;
  case ""BO"":
  OutgoingUnitCode = ""FL"";
  break;
  case ""FOT"":
  OutgoingUnitCode = ""FOT"";
  break;
  case ""GRM"":
  OutgoingUnitCode = ""GRM"";
  break;
  case ""GRO"":
  OutgoingUnitCode = ""GRO"";
  break;
  case ""HLT"":
  OutgoingUnitCode = ""HLT"";
  break;
  case ""CT"":
  OutgoingUnitCode = ""KAR"";
  break;
  case ""Z2"":
  OutgoingUnitCode = ""KS"";
  break;
  case ""KGM"":
  OutgoingUnitCode = ""KGM"";
  break;
  case ""CMQ"":
  OutgoingUnitCode = ""CMQ"";
  break;
  case ""MTQ"":
  OutgoingUnitCode = ""MTQ"";
  break;
  case ""CMK"":
  OutgoingUnitCode = ""CMK"";
  break;
  case ""MTK"":
  OutgoingUnitCode = ""MTK"";
  break;
  case ""LN"":
  OutgoingUnitCode = ""LGD"";
  break;
  case ""LTR"":
  OutgoingUnitCode = ""LTR"";
  break;
  case ""NL"":
  OutgoingUnitCode = ""LES"";
  break;
  case ""MTR"":
  OutgoingUnitCode = ""MTR"";
  break;
  case ""MLT"":
  OutgoingUnitCode = ""MLT"";
  break;
  case ""MMT"":
  OutgoingUnitCode = ""MMT"";
  break;
  case ""PK"":
  OutgoingUnitCode = ""PAK"";
  break;
  case ""PF"":
  OutgoingUnitCode = ""PF"";
  break;
  case ""BB"":
  OutgoingUnitCode = ""PB"";
  break;
  case ""PR"":
  OutgoingUnitCode = ""PAR"";
  break;
  case ""CQ"":
  OutgoingUnitCode = ""PAT"";
  break;
  case ""PG"":
  OutgoingUnitCode = ""PL"";
  break;
  case ""BG"":
  OutgoingUnitCode = ""PS"";
  break;
  case ""RG"":
  OutgoingUnitCode = ""RIN"";
  break;
  case ""RO"":
  OutgoingUnitCode = ""NRL"";
  break;
  case ""TU"":
  OutgoingUnitCode = ""ROR"";
  break;
  case ""BJ"":
  OutgoingUnitCode = ""SP"";
  break;
  case ""SO"":
  OutgoingUnitCode = ""SPO"";
  break;
  case ""SA"":
  OutgoingUnitCode = ""SAK"";
  break;
  case ""SET"":
  OutgoingUnitCode = ""SET"";
  break;
  case ""HUR"":
  OutgoingUnitCode = ""HUR"";
  break;
  case ""TNE"":
  OutgoingUnitCode = ""TNE"";
  break;
  case ""DR"":
  OutgoingUnitCode = ""TR"";
  break;
  case ""CS"":
  OutgoingUnitCode = ""ASK"";
  break;
  case ""BND"":
  OutgoingUnitCode = ""BÅND"";
  break;
  case ""BRV"":
  OutgoingUnitCode = ""BREV"";
  break;
  case ""COL"":
  OutgoingUnitCode = ""COL"";
  break;
  case ""HAP"":
  OutgoingUnitCode = ""HAP"";
  break;
  case ""KOR"":
  OutgoingUnitCode = ""KORT"";
  break;
  case ""PB"":
  OutgoingUnitCode = ""PB"";
  break;
  case ""RON"":
  OutgoingUnitCode = ""ROND"";
  break;
  case ""SKF"":
  OutgoingUnitCode = ""SKF"";
  break;
  case ""TB"":
  OutgoingUnitCode = ""TB"";
  break;
  case ""XXX"":
  OutgoingUnitCode = ""XXX"";
  break;
  }
  break;

  default:
  OutgoingUnitCode = IngoingUnitCode;
  break;
  }

  if (OutgoingUnitCode == """")
  {
  OutgoingUnitCode = IngoingUnitCode;
  }

  return OutgoingUnitCode;
  }  
       
]]>
  </msxsl:script>
  
  
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"BYGE.Integration.EDIFACTOrder.Schemas.EFACT_D96A_ORDERS";
        
        private const global::BYGE.Integration.EDIFACTOrder.Schemas.EFACT_D96A_ORDERS _srcSchemaTypeReference0 = null;
        
        private const string _strTrgSchemasList0 = @"BYGE.Integration.Core.Order.Order";
        
        private const global::BYGE.Integration.Core.Order.Order _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [1];
                _SrcSchemas[0] = @"BYGE.Integration.EDIFACTOrder.Schemas.EFACT_D96A_ORDERS";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"BYGE.Integration.Core.Order.Order";
                return _TrgSchemas;
            }
        }
    }
}
