namespace BYGE.Integration.XLBYG.Compressed.Invoice.Maps {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.XLBYGCompressedInvoice", typeof(global::BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.XLBYGCompressedInvoice))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.Core.Invoice.Invoice", typeof(global::BYGE.Integration.Core.Invoice.Invoice))]
    public sealed class XLBYG_Compressed_Invoice_to_Core_Invoice : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var s0 userCSharp"" version=""1.0"" xmlns:ns0=""http://byg-e.dk/schemas/v10"" xmlns:s0=""http://BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.InvoiceTest01"" xmlns:userCSharp=""http://schemas.microsoft.com/BizTalk/2003/userCSharp"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/s0:XLBYGCompressedInvoice"" />
  </xsl:template>
  <xsl:template match=""/s0:XLBYGCompressedInvoice"">
    <xsl:variable name=""var:v1"" select=""userCSharp:StringRight(string(Header/IssueDate/text()) , &quot;6&quot;)"" />
    <ns0:Invoice>
      <ProfileID>
        <xsl:value-of select=""Header/TestInd/text()"" />
      </ProfileID>
      <InvoiceID>
        <xsl:value-of select=""Footer/InvoicID/text()"" />
      </InvoiceID>
      <UUID>
        <xsl:value-of select=""Footer/GUID/text()"" />
      </UUID>
      <IssueDate>
        <xsl:value-of select=""$var:v1"" />
      </IssueDate>
      <InvoiceType>
        <xsl:value-of select=""Header/DocType/text()"" />
      </InvoiceType>
      <OrderReference>
        <OrderID>
          <xsl:value-of select=""Header/OrderID/text()"" />
        </OrderID>
      </OrderReference>
      <AccountingSupplier>
        <AccSellerID>
          <xsl:value-of select=""Footer/NADSU/text()"" />
        </AccSellerID>
      </AccountingSupplier>
      <BuyerCustomer>
        <BuyerID>
          <xsl:value-of select=""Footer/NADBY/text()"" />
        </BuyerID>
      </BuyerCustomer>
      <PaymentMeans>
        <xsl:variable name=""var:v2"" select=""userCSharp:AdjustDate(string(Header/PaymentDate1/text()))"" />
        <PaymentDueDate>
          <xsl:value-of select=""$var:v2"" />
        </PaymentDueDate>
      </PaymentMeans>
      <AllowanceCharge>
        <xsl:variable name=""var:v3"" select=""userCSharp:ReturnPersentage(string(Header/TAX/text()))"" />
        <MultiplierFactorNumeric>
          <xsl:value-of select=""$var:v3"" />
        </MultiplierFactorNumeric>
      </AllowanceCharge>
      <TaxTotal>
        <xsl:variable name=""var:v4"" select=""userCSharp:ReturnTaxAmount(string(Footer/TaxTotal_TaxAmount/text()))"" />
        <TaxAmount>
          <xsl:value-of select=""$var:v4"" />
        </TaxAmount>
        <TaxSubtotal>
          <xsl:variable name=""var:v5"" select=""userCSharp:ReturnLineExtensionAmount(string(Footer/TaxSubtotal_TaxableAmount/text()))"" />
          <TaxableAmount>
            <xsl:value-of select=""$var:v5"" />
          </TaxableAmount>
        </TaxSubtotal>
      </TaxTotal>
      <Total>
        <xsl:variable name=""var:v6"" select=""userCSharp:ReturnAmount(string(Footer/LegalMonetaryTotal_PayableAmount/text()))"" />
        <LineTotalAmount>
          <xsl:value-of select=""$var:v6"" />
        </LineTotalAmount>
        <xsl:variable name=""var:v7"" select=""userCSharp:ReturnLineAmount(string(Footer/UBLExtensionsContent_ExtensionAmount/text()))"" />
        <PayableAmount>
          <xsl:value-of select=""$var:v7"" />
        </PayableAmount>
      </Total>
    </ns0:Invoice>
  </xsl:template>
  <msxsl:script language=""C#"" implements-prefix=""userCSharp""><![CDATA[
public string StringRight(string str, string count)
{
	string retval = """";
	double d = 0;
	if (str != null && IsNumeric(count, ref d))
	{
		int i = (int) d;
		if (i > 0)
		{
			if (i <= str.Length)
			{
				retval = str.Substring(str.Length-i);
			}
			else
			{
				retval = str;
			}
		}
	}
	return retval;
}


public string AdjustDate(string PaymentDate)
{
string AdjustedDate;

if (PaymentDate != """")
{
AdjustedDate = PaymentDate.Substring(2, 6);
}
else
{
AdjustedDate=""000000"";
}

return AdjustedDate;
}


public string ReturnPersentage(string Amount)
{

int AmountLen;
int CommaPosition;
string ipart;
string izeropart;
string dpart;
string dzeropart;
string OutputAmount;

if (Amount != """")
{
izeropart = """";
dzeropart = """";

AmountLen = Amount.Length;
CommaPosition = Amount.LastIndexOf(""."") ; 

if (CommaPosition != -1) 
{
ipart = Amount.Substring(0, CommaPosition);
dpart = Amount.Substring(CommaPosition+1, AmountLen-1-CommaPosition);

switch(dpart.Length)
{
case 2: break;
case 1: dzeropart = ""0""; break;
case 0: dzeropart = ""00""; break;
}
dpart = dpart + dzeropart;

switch(ipart.Length)
{
case 3: break;
case 2: izeropart = ""0""; break; 
case 1: izeropart = ""00""; break; 
}
ipart = izeropart + ipart;

OutputAmount = ipart+dpart;
}

else
{
switch(Amount.Length)
{
case 3: break;
case 2: izeropart = ""0""; break;
case 1: izeropart = ""00""; break;
}
OutputAmount = izeropart + Amount +""00"";
}
}
else
{
OutputAmount  = ""00000"";
}

return OutputAmount;
}

public string ReturnLineExtensionAmount(string Amount)
{

int AmountLen;
int CommaPosition;
string ipart;
string izeropart;
string dpart;
string dzeropart;
string OutputAmount;

izeropart = """";
dzeropart = """";

AmountLen = Amount.Length;
CommaPosition = Amount.LastIndexOf("","") ; 

if (CommaPosition != -1) 
{
ipart = Amount.Substring(0, CommaPosition);
dpart = Amount.Substring(CommaPosition+1, AmountLen-1-CommaPosition);

switch(dpart.Length)
{
case 2: break;
case 1: dzeropart = ""0""; break;
case 0: dzeropart = ""00""; break;
}
dpart = dpart + dzeropart;

switch(ipart.Length)
{
case 9: break;
case 8: izeropart = ""0""; break; 
case 7: izeropart = ""00""; break; 
case 6: izeropart = ""000""; break; 
case 5: izeropart = ""0000""; break; 
case 4: izeropart = ""00000""; break; 
case 3: izeropart = ""000000""; break; 
case 2: izeropart = ""0000000""; break; 
case 1: izeropart = ""00000000""; break; 
}
ipart = izeropart + ipart;

OutputAmount = ipart+dpart;
}

else
{
switch(Amount.Length)
{
case 9: break;
case 8: izeropart = ""0""; break;
case 7: izeropart = ""00""; break;
case 6: izeropart = ""000""; break;
case 5: izeropart = ""0000""; break;
case 4: izeropart = ""00000""; break;
case 3: izeropart = ""000000""; break;
case 2: izeropart = ""0000000""; break;
case 1: izeropart = ""00000000""; break;
}
OutputAmount = izeropart + Amount+""00"";
}

return OutputAmount;
}

public string ReturnTaxAmount(string Amount)
{

int AmountLen;
int CommaPosition;
string ipart;
string izeropart;
string dpart;
string dzeropart;
string OutputAmount;

izeropart = """";
dzeropart = """";

AmountLen = Amount.Length;
CommaPosition = Amount.LastIndexOf("","") ; 

if (CommaPosition != -1) 
{
ipart = Amount.Substring(0, CommaPosition);
dpart = Amount.Substring(CommaPosition+1, AmountLen-1-CommaPosition);

switch(dpart.Length)
{
case 2: break;
case 1: dzeropart = ""0""; break;
case 0: dzeropart = ""00""; break;
}
dpart = dpart + dzeropart;

switch(ipart.Length)
{
case 9: break;
case 8: izeropart = ""0""; break; 
case 7: izeropart = ""00""; break; 
case 6: izeropart = ""000""; break; 
case 5: izeropart = ""0000""; break; 
case 4: izeropart = ""00000""; break; 
case 3: izeropart = ""000000""; break; 
case 2: izeropart = ""0000000""; break; 
case 1: izeropart = ""00000000""; break; 
}
ipart = izeropart + ipart;

OutputAmount = ipart+dpart;
}

else
{
switch(Amount.Length)
{
case 9: break;
case 8: izeropart = ""0""; break;
case 7: izeropart = ""00""; break;
case 6: izeropart = ""000""; break;
case 5: izeropart = ""0000""; break;
case 4: izeropart = ""00000""; break;
case 3: izeropart = ""000000""; break;
case 2: izeropart = ""0000000""; break;
case 1: izeropart = ""00000000""; break;
}
OutputAmount = izeropart + Amount+""00"";
}

return OutputAmount;
}

public string ReturnAmount(string Amount)
{

int AmountLen;
int CommaPosition;
string ipart;
string izeropart;
string dpart;
string dzeropart;
string OutputAmount;

izeropart = """";
dzeropart = """";

AmountLen = Amount.Length;
CommaPosition = Amount.LastIndexOf("","") ; 

if (CommaPosition != -1) 
{
ipart = Amount.Substring(0, CommaPosition);
dpart = Amount.Substring(CommaPosition+1, AmountLen-1-CommaPosition);

switch(dpart.Length)
{
case 2: break;
case 1: dzeropart = ""0""; break;
case 0: dzeropart = ""00""; break;
}
dpart = dpart + dzeropart;

switch(ipart.Length)
{
case 9: break;
case 8: izeropart = ""0""; break; 
case 7: izeropart = ""00""; break; 
case 6: izeropart = ""000""; break; 
case 5: izeropart = ""0000""; break; 
case 4: izeropart = ""00000""; break; 
case 3: izeropart = ""000000""; break; 
case 2: izeropart = ""0000000""; break; 
case 1: izeropart = ""00000000""; break; 
}
ipart = izeropart + ipart;

OutputAmount = ipart+dpart;
}

else
{
switch(Amount.Length)
{
case 9: break;
case 8: izeropart = ""0""; break;
case 7: izeropart = ""00""; break;
case 6: izeropart = ""000""; break;
case 5: izeropart = ""0000""; break;
case 4: izeropart = ""00000""; break;
case 3: izeropart = ""000000""; break;
case 2: izeropart = ""0000000""; break;
case 1: izeropart = ""00000000""; break;
}
OutputAmount = izeropart + Amount+""00"";
}

return OutputAmount;
}

public string ReturnLineAmount(string Amount)
{

int AmountLen;
int CommaPosition;
string ipart;
string izeropart;
string dpart;
string dzeropart;
string OutputAmount;

if (Amount != """")
{
izeropart = """";
dzeropart = """";

AmountLen = Amount.Length;
CommaPosition = Amount.LastIndexOf(""."") ; 

if (CommaPosition != -1) 
{
ipart = Amount.Substring(0, CommaPosition);
dpart = Amount.Substring(CommaPosition+1, AmountLen-1-CommaPosition);

switch(dpart.Length)
{
case 2: break;
case 1: dzeropart = ""0""; break;
case 0: dzeropart = ""00""; break;
}
dpart = dpart + dzeropart;

switch(ipart.Length)
{
case 9: break;
case 8: izeropart = ""0""; break; 
case 7: izeropart = ""00""; break; 
case 6: izeropart = ""000""; break; 
case 5: izeropart = ""0000""; break; 
case 4: izeropart = ""00000""; break; 
case 3: izeropart = ""000000""; break; 
case 2: izeropart = ""0000000""; break; 
case 1: izeropart = ""00000000""; break; 
}
ipart = izeropart + ipart;

OutputAmount = ipart+dpart;
}

else
{
switch(Amount.Length)
{
case 9: break;
case 8: izeropart = ""0""; break;
case 7: izeropart = ""00""; break;
case 6: izeropart = ""000""; break;
case 5: izeropart = ""0000""; break;
case 4: izeropart = ""00000""; break;
case 3: izeropart = ""000000""; break;
case 2: izeropart = ""0000000""; break;
case 1: izeropart = ""00000000""; break;
}
OutputAmount = izeropart + Amount+""00"";
}
}
else
{
OutputAmount  = ""00000000000"";
}

return OutputAmount;
}

public bool IsNumeric(string val)
{
	if (val == null)
	{
		return false;
	}
	double d = 0;
	return Double.TryParse(val, System.Globalization.NumberStyles.AllowThousands | System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out d);
}

public bool IsNumeric(string val, ref double d)
{
	if (val == null)
	{
		return false;
	}
	return Double.TryParse(val, System.Globalization.NumberStyles.AllowThousands | System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out d);
}


]]></msxsl:script>
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.XLBYGCompressedInvoice";
        
        private const global::BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.XLBYGCompressedInvoice _srcSchemaTypeReference0 = null;
        
        private const string _strTrgSchemasList0 = @"BYGE.Integration.Core.Invoice.Invoice";
        
        private const global::BYGE.Integration.Core.Invoice.Invoice _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [1];
                _SrcSchemas[0] = @"BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.XLBYGCompressedInvoice";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"BYGE.Integration.Core.Invoice.Invoice";
                return _TrgSchemas;
            }
        }
    }
}
