namespace BYGE.Integration.XLBYG.Compressed.Invoice.Maps {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.XLBYGCompressedInvoice", typeof(global::BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.XLBYGCompressedInvoice))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.XLBYGCompressedInvoice", typeof(global::BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.XLBYGCompressedInvoice))]
    public sealed class XLBYG_Compressed_Invoice_to_XLBYG_Compressed_Invoice : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var"" version=""1.0"" xmlns:ns0=""http://BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.InvoiceTest01"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/ns0:XLBYGCompressedInvoice"" />
  </xsl:template>
  <xsl:template match=""/ns0:XLBYGCompressedInvoice"">
    <ns0:XLBYGCompressedInvoice>
      <xsl:copy-of select=""./@*"" />
      <xsl:copy-of select=""./*"" />
    </ns0:XLBYGCompressedInvoice>
  </xsl:template>
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.XLBYGCompressedInvoice";
        
        private const global::BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.XLBYGCompressedInvoice _srcSchemaTypeReference0 = null;
        
        private const string _strTrgSchemasList0 = @"BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.XLBYGCompressedInvoice";
        
        private const global::BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.XLBYGCompressedInvoice _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [1];
                _SrcSchemas[0] = @"BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.XLBYGCompressedInvoice";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.XLBYGCompressedInvoice";
                return _TrgSchemas;
            }
        }
    }
}
