namespace BYGE.Integration.XLBYG.Compressed.Invoice.Maps {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.Core.Invoice.Invoice", typeof(global::BYGE.Integration.Core.Invoice.Invoice))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.XLBYGCompressedInvoice", typeof(global::BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.XLBYGCompressedInvoice))]
    public sealed class Core_Invoice_to_XLBYG_Compressed_Invoice : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var s0 userCSharp"" version=""1.0"" xmlns:s0=""http://byg-e.dk/schemas/v10"" xmlns:ns0=""http://BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.InvoiceTest01"" xmlns:userCSharp=""http://schemas.microsoft.com/BizTalk/2003/userCSharp"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/s0:Invoice"" />
  </xsl:template>
  <xsl:template match=""/s0:Invoice"">
    <ns0:XLBYGCompressedInvoice>
      <xsl:for-each select=""PaymentTerms"">
        <xsl:for-each select=""SettlementPeriod"">
          <xsl:variable name=""var:v1"" select=""userCSharp:StringTrimLeft(string(../../AccountingSupplier/AccSellerID/text()))"" />
          <xsl:variable name=""var:v2"" select=""userCSharp:StringTrimRight(string($var:v1))"" />
          <xsl:variable name=""var:v4"" select=""userCSharp:StringTrimLeft(string(../../BuyerCustomer/PartyID/text()))"" />
          <xsl:variable name=""var:v5"" select=""userCSharp:StringTrimRight(string($var:v4))"" />
          <xsl:variable name=""var:v6"" select=""userCSharp:StringTrimLeft(string(../../AccountingSupplier/PartyID/text()))"" />
          <xsl:variable name=""var:v7"" select=""userCSharp:StringTrimRight(string($var:v6))"" />
          <xsl:variable name=""var:v12"" select=""userCSharp:StringTrimLeft(string(StartDate/text()))"" />
          <xsl:variable name=""var:v13"" select=""userCSharp:StringTrimRight(string($var:v12))"" />
          <xsl:variable name=""var:v14"" select=""userCSharp:LogicalNe(string($var:v13) , &quot;&quot;)"" />
          <xsl:variable name=""var:v15"" select=""userCSharp:StringTrimLeft(string(../../PaymentMeans/PaymentDueDate/text()))"" />
          <xsl:variable name=""var:v16"" select=""userCSharp:StringTrimRight(string($var:v15))"" />
          <xsl:variable name=""var:v17"" select=""userCSharp:LogicalNe(string($var:v16) , &quot;&quot;)"" />
          <xsl:variable name=""var:v18"" select=""userCSharp:LogicalAnd(string($var:v14) , string($var:v17))"" />
          <xsl:variable name=""var:v22"" select=""string(../../PaymentMeans/PaymentDueDate/text())"" />
          <xsl:variable name=""var:v23"" select=""userCSharp:StringTrimLeft($var:v22)"" />
          <xsl:variable name=""var:v24"" select=""userCSharp:StringTrimRight(string($var:v23))"" />
          <xsl:variable name=""var:v25"" select=""userCSharp:LogicalNe(string($var:v24) , &quot;&quot;)"" />
          <xsl:variable name=""var:v26"" select=""string(StartDate/text())"" />
          <xsl:variable name=""var:v27"" select=""userCSharp:StringTrimLeft($var:v26)"" />
          <xsl:variable name=""var:v28"" select=""userCSharp:StringTrimRight(string($var:v27))"" />
          <xsl:variable name=""var:v29"" select=""userCSharp:LogicalEq(string($var:v28) , &quot;&quot;)"" />
          <xsl:variable name=""var:v30"" select=""userCSharp:LogicalAnd(string($var:v25) , string($var:v29))"" />
          <xsl:variable name=""var:v32"" select=""userCSharp:LogicalNe(string($var:v28) , &quot;&quot;)"" />
          <xsl:variable name=""var:v35"" select=""userCSharp:StringTrimLeft(string(../../OrderReference/OrderID/text()))"" />
          <xsl:variable name=""var:v36"" select=""userCSharp:StringTrimRight(string($var:v35))"" />
          <xsl:variable name=""var:v37"" select=""userCSharp:LogicalNe(string($var:v36) , &quot;&quot;)"" />
          <xsl:variable name=""var:v38"" select=""userCSharp:LogicalNe(string($var:v36) , &quot;Blank&quot;)"" />
          <xsl:variable name=""var:v39"" select=""userCSharp:LogicalAnd(string($var:v37) , string($var:v38))"" />
          <xsl:variable name=""var:v43"" select=""userCSharp:LogicalEq(string($var:v36) , &quot;&quot;)"" />
          <xsl:variable name=""var:v44"" select=""userCSharp:LogicalEq(string($var:v36) , &quot;Blank&quot;)"" />
          <xsl:variable name=""var:v45"" select=""userCSharp:LogicalOr(string($var:v43) , string($var:v44))"" />
          <xsl:variable name=""var:v46"" select=""userCSharp:StringTrimLeft(string(../../OrderReference/CustomerReference/text()))"" />
          <xsl:variable name=""var:v47"" select=""userCSharp:StringTrimRight(string($var:v46))"" />
          <xsl:variable name=""var:v48"" select=""userCSharp:LogicalNe(string($var:v47) , &quot;&quot;)"" />
          <xsl:variable name=""var:v49"" select=""userCSharp:LogicalAnd(string($var:v45) , string($var:v48))"" />
          <xsl:variable name=""var:v53"" select=""userCSharp:LogicalEq(string($var:v47) , &quot;&quot;)"" />
          <xsl:variable name=""var:v54"" select=""userCSharp:LogicalAnd(string($var:v45) , string($var:v53))"" />
          <xsl:variable name=""var:v55"" select=""userCSharp:LogicalAnd(string($var:v37) , string($var:v54))"" />
          <xsl:variable name=""var:v59"" select=""userCSharp:StringTrimLeft(string(../../OrderReference/SalesOrderID/text()))"" />
          <xsl:variable name=""var:v60"" select=""userCSharp:StringTrimRight(string($var:v59))"" />
          <xsl:variable name=""var:v61"" select=""userCSharp:StringLeft(string($var:v60) , &quot;15&quot;)"" />
          <xsl:variable name=""var:v62"" select=""userCSharp:StringTrimLeft(string(../../BillingReference/InvoiceDocumentReference/ID/text()))"" />
          <xsl:variable name=""var:v63"" select=""userCSharp:StringTrimRight(string($var:v62))"" />
          <xsl:variable name=""var:v64"" select=""userCSharp:StringSubstring(string($var:v63) , &quot;1&quot; , &quot;15&quot;)"" />
          <xsl:variable name=""var:v65"" select=""userCSharp:LogicalNe(string($var:v64) , &quot;&quot;)"" />
          <xsl:variable name=""var:v80"" select=""userCSharp:StringTrimLeft(string(../../BuyerCustomer/Concact/Name/text()))"" />
          <xsl:variable name=""var:v81"" select=""userCSharp:StringTrimRight(string($var:v80))"" />
          <xsl:variable name=""var:v82"" select=""userCSharp:StringSubstring(string($var:v81) , &quot;1&quot; , &quot;20&quot;)"" />
          <xsl:variable name=""var:v83"" select=""userCSharp:LogicalNe(string($var:v82) , &quot;&quot;)"" />
          <xsl:variable name=""var:v92"" select=""userCSharp:StringTrimLeft(string(../../DeliveryLocation/Address/Name/text()))"" />
          <xsl:variable name=""var:v93"" select=""userCSharp:StringTrimRight(string($var:v92))"" />
          <xsl:variable name=""var:v94"" select=""userCSharp:StringSubstring(string($var:v93) , &quot;1&quot; , &quot;35&quot;)"" />
          <xsl:variable name=""var:v95"" select=""userCSharp:StringTrimLeft(string(../../DeliveryLocation/Address/MarkAttention/text()))"" />
          <xsl:variable name=""var:v96"" select=""userCSharp:StringTrimRight(string($var:v95))"" />
          <xsl:variable name=""var:v97"" select=""userCSharp:StringConcat(string(../../DeliveryLocation/Address/Street/text()) , &quot; &quot; , string(../../DeliveryLocation/Address/Number/text()))"" />
          <xsl:variable name=""var:v98"" select=""userCSharp:StringTrimLeft(string($var:v97))"" />
          <xsl:variable name=""var:v99"" select=""userCSharp:StringTrimRight(string($var:v98))"" />
          <xsl:variable name=""var:v100"" select=""userCSharp:StringTrimLeft(string(../../DeliveryLocation/Address/City/text()))"" />
          <xsl:variable name=""var:v101"" select=""userCSharp:StringTrimRight(string($var:v100))"" />
          <xsl:variable name=""var:v102"" select=""userCSharp:StringConcat(string(../../DeliveryLocation/Address/PostalCode/text()) , &quot; &quot; , string($var:v101))"" />
          <xsl:variable name=""var:v103"" select=""userCSharp:StringTrimLeft(string(../../DeliveryLocation/Address/Country/text()))"" />
          <xsl:variable name=""var:v104"" select=""userCSharp:StringTrimRight(string($var:v103))"" />
          <xsl:variable name=""var:v105"" select=""userCSharp:StringTrimLeft(string(../../DeliveryLocation/Address/Department/text()))"" />
          <xsl:variable name=""var:v106"" select=""userCSharp:StringTrimRight(string($var:v105))"" />
          <xsl:variable name=""var:v109"" select=""string(../../DeliveryLocation/Address/MarkAttention/text())"" />
          <xsl:variable name=""var:v110"" select=""userCSharp:StringTrimLeft($var:v109)"" />
          <xsl:variable name=""var:v111"" select=""userCSharp:StringTrimRight(string($var:v110))"" />
          <xsl:variable name=""var:v112"" select=""string(../../DeliveryLocation/Address/Street/text())"" />
          <xsl:variable name=""var:v113"" select=""string(../../DeliveryLocation/Address/Number/text())"" />
          <xsl:variable name=""var:v114"" select=""userCSharp:StringConcat($var:v112 , &quot; &quot; , $var:v113)"" />
          <xsl:variable name=""var:v115"" select=""userCSharp:StringTrimLeft(string($var:v114))"" />
          <xsl:variable name=""var:v116"" select=""userCSharp:StringTrimRight(string($var:v115))"" />
          <xsl:variable name=""var:v117"" select=""string(../../DeliveryLocation/Address/City/text())"" />
          <xsl:variable name=""var:v118"" select=""userCSharp:StringTrimLeft($var:v117)"" />
          <xsl:variable name=""var:v119"" select=""userCSharp:StringTrimRight(string($var:v118))"" />
          <xsl:variable name=""var:v120"" select=""string(../../DeliveryLocation/Address/PostalCode/text())"" />
          <xsl:variable name=""var:v121"" select=""userCSharp:StringConcat($var:v120 , &quot; &quot; , string($var:v119))"" />
          <xsl:variable name=""var:v122"" select=""string(../../DeliveryLocation/Address/Country/text())"" />
          <xsl:variable name=""var:v123"" select=""userCSharp:StringTrimLeft($var:v122)"" />
          <xsl:variable name=""var:v124"" select=""userCSharp:StringTrimRight(string($var:v123))"" />
          <xsl:variable name=""var:v125"" select=""string(../../DeliveryLocation/Address/Department/text())"" />
          <xsl:variable name=""var:v126"" select=""userCSharp:StringTrimLeft($var:v125)"" />
          <xsl:variable name=""var:v127"" select=""userCSharp:StringTrimRight(string($var:v126))"" />
          <xsl:variable name=""var:v132"" select=""userCSharp:StringTrimLeft(string(../../TaxTotal/TaxSubtotal/TaxCategory/Percent/text()))"" />
          <xsl:variable name=""var:v133"" select=""userCSharp:StringTrimRight(string($var:v132))"" />
          <xsl:variable name=""var:v136"" select=""userCSharp:StringTrimLeft(string(../../Currency/text()))"" />
          <xsl:variable name=""var:v137"" select=""userCSharp:StringTrimRight(string($var:v136))"" />
          <xsl:variable name=""var:v138"" select=""userCSharp:StringTrimLeft(string(../../AccountingSupplier/CompanyID/text()))"" />
          <xsl:variable name=""var:v139"" select=""userCSharp:StringTrimRight(string($var:v138))"" />
          <xsl:variable name=""var:v191"" select=""userCSharp:StringTrimLeft(string(../../AccountingSupplier/Address/Name/text()))"" />
          <xsl:variable name=""var:v192"" select=""userCSharp:StringTrimRight(string($var:v191))"" />
          <xsl:variable name=""var:v193"" select=""userCSharp:StringSubstring(string($var:v192) , &quot;1&quot; , &quot;35&quot;)"" />
          <xsl:variable name=""var:v194"" select=""userCSharp:StringTrimLeft(string(../../AccountingSupplier/Address/Street/text()))"" />
          <xsl:variable name=""var:v195"" select=""userCSharp:StringTrimRight(string($var:v194))"" />
          <xsl:variable name=""var:v196"" select=""userCSharp:StringSubstring(string($var:v195) , &quot;1&quot; , &quot;35&quot;)"" />
          <xsl:variable name=""var:v197"" select=""userCSharp:StringTrimLeft(string(../../AccountingSupplier/Address/City/text()))"" />
          <xsl:variable name=""var:v198"" select=""userCSharp:StringTrimRight(string($var:v197))"" />
          <xsl:variable name=""var:v199"" select=""userCSharp:StringConcat(string(../../AccountingSupplier/Address/PostalCode/text()) , &quot; &quot; , string($var:v198))"" />
          <xsl:variable name=""var:v200"" select=""userCSharp:StringTrimLeft(string($var:v199))"" />
          <xsl:variable name=""var:v201"" select=""userCSharp:StringTrimRight(string($var:v200))"" />
          <xsl:variable name=""var:v202"" select=""userCSharp:StringSubstring(string($var:v201) , &quot;1&quot; , &quot;35&quot;)"" />
          <xsl:variable name=""var:v203"" select=""userCSharp:StringTrimLeft(string(../../AccountingSupplier/Address/Country/text()))"" />
          <xsl:variable name=""var:v204"" select=""userCSharp:StringTrimRight(string($var:v203))"" />
          <xsl:variable name=""var:v205"" select=""userCSharp:StringSubstring(string($var:v204) , &quot;1&quot; , &quot;35&quot;)"" />
          <Header>
            <HeaderTag>
              <xsl:text>105</xsl:text>
            </HeaderTag>
            <TestInd>
              <xsl:text>0</xsl:text>
            </TestInd>
            <xsl:variable name=""var:v3"" select=""userCSharp:ReturnUNBSND(string($var:v2))"" />
            <UNBSND>
              <xsl:value-of select=""$var:v3"" />
            </UNBSND>
            <UNBRCV>
              <xsl:text>5790000252381</xsl:text>
            </UNBRCV>
            <NADBY>
              <xsl:value-of select=""$var:v5"" />
            </NADBY>
            <xsl:variable name=""var:v8"" select=""userCSharp:ReturnNADSU(string($var:v7))"" />
            <NADSU>
              <xsl:value-of select=""$var:v8"" />
            </NADSU>
            <NADIV>
              <xsl:text>5790000252381</xsl:text>
            </NADIV>
            <InvoicID>
              <xsl:value-of select=""../../InvoiceID/text()"" />
            </InvoicID>
            <xsl:variable name=""var:v9"" select=""userCSharp:ReturnInvoiceType(string(../../InvoiceType/text()))"" />
            <DocType>
              <xsl:value-of select=""$var:v9"" />
            </DocType>
            <xsl:variable name=""var:v10"" select=""userCSharp:ReturnIssueDate(string(../../IssueDate/text()))"" />
            <IssueDate>
              <xsl:value-of select=""$var:v10"" />
            </IssueDate>
            <xsl:variable name=""var:v11"" select=""userCSharp:ReturnDeliveryDate(string(../../DeliveryInfo/ActualDeliveryDate/text()))"" />
            <DeliveryDate>
              <xsl:value-of select=""$var:v11"" />
            </DeliveryDate>
            <xsl:if test=""string($var:v18)='true'"">
              <xsl:variable name=""var:v19"" select=""string(../../PaymentMeans/PaymentDueDate/text())"" />
              <xsl:variable name=""var:v20"" select=""userCSharp:StringTrimLeft($var:v19)"" />
              <xsl:variable name=""var:v21"" select=""userCSharp:StringTrimRight(string($var:v20))"" />
              <PaymentDate>
                <xsl:value-of select=""$var:v21"" />
              </PaymentDate>
            </xsl:if>
            <xsl:if test=""string($var:v30)='true'"">
              <xsl:variable name=""var:v31"" select=""userCSharp:ReturnDeliveryDueDate1(string($var:v24))"" />
              <PaymentDate1>
                <xsl:value-of select=""$var:v31"" />
              </PaymentDate1>
            </xsl:if>
            <xsl:if test=""string($var:v32)='true'"">
              <xsl:variable name=""var:v33"" select=""userCSharp:ReturnDeliveryDueDate(string($var:v28))"" />
              <PaymentDate1>
                <xsl:value-of select=""$var:v33"" />
              </PaymentDate1>
            </xsl:if>
            <PaymentTermsDiscount>
              <xsl:text> </xsl:text>
            </PaymentTermsDiscount>
            <xsl:variable name=""var:v34"" select=""userCSharp:ReturnIssueDate(string(../../OrderReference/OrderDate/text()))"" />
            <OrderDate>
              <xsl:value-of select=""$var:v34"" />
            </OrderDate>
            <xsl:if test=""string($var:v39)='true'"">
              <xsl:variable name=""var:v40"" select=""string(../../OrderReference/OrderID/text())"" />
              <xsl:variable name=""var:v41"" select=""userCSharp:StringTrimLeft($var:v40)"" />
              <xsl:variable name=""var:v42"" select=""userCSharp:StringTrimRight(string($var:v41))"" />
              <OrderID>
                <xsl:value-of select=""$var:v42"" />
              </OrderID>
            </xsl:if>
            <xsl:if test=""string($var:v49)='true'"">
              <xsl:variable name=""var:v50"" select=""string(../../OrderReference/CustomerReference/text())"" />
              <xsl:variable name=""var:v51"" select=""userCSharp:StringTrimLeft($var:v50)"" />
              <xsl:variable name=""var:v52"" select=""userCSharp:StringTrimRight(string($var:v51))"" />
              <OrderID>
                <xsl:value-of select=""$var:v52"" />
              </OrderID>
            </xsl:if>
            <xsl:if test=""string($var:v55)='true'"">
              <xsl:variable name=""var:v56"" select=""string(../../OrderReference/OrderID/text())"" />
              <xsl:variable name=""var:v57"" select=""userCSharp:StringTrimLeft($var:v56)"" />
              <xsl:variable name=""var:v58"" select=""userCSharp:StringTrimRight(string($var:v57))"" />
              <OrderID>
                <xsl:value-of select=""$var:v58"" />
              </OrderID>
            </xsl:if>
            <OrderSupplierID>
              <xsl:value-of select=""$var:v61"" />
            </OrderSupplierID>
            <DespatchNoteID>
              <xsl:value-of select=""../../DespatchDocumentReference/ID/text()"" />
            </DespatchNoteID>
            <xsl:if test=""string($var:v65)='true'"">
              <xsl:variable name=""var:v66"" select=""string(../../BillingReference/InvoiceDocumentReference/ID/text())"" />
              <xsl:variable name=""var:v67"" select=""userCSharp:StringTrimLeft($var:v66)"" />
              <xsl:variable name=""var:v68"" select=""userCSharp:StringTrimRight(string($var:v67))"" />
              <xsl:variable name=""var:v69"" select=""userCSharp:StringSubstring(string($var:v68) , &quot;1&quot; , &quot;15&quot;)"" />
              <InvoiceID>
                <xsl:value-of select=""$var:v69"" />
              </InvoiceID>
            </xsl:if>
            <xsl:for-each select=""../../Note"">
              <xsl:variable name=""var:v70"" select=""position()"" />
              <xsl:variable name=""var:v71"" select=""userCSharp:LogicalEq(string($var:v70) , &quot;1&quot;)"" />
              <xsl:if test=""$var:v71"">
                <FreeText1>
                  <xsl:value-of select=""./text()"" />
                </FreeText1>
              </xsl:if>
            </xsl:for-each>
            <xsl:for-each select=""../../Note"">
              <xsl:variable name=""var:v72"" select=""position()"" />
              <xsl:variable name=""var:v73"" select=""userCSharp:LogicalEq(string($var:v72) , &quot;2&quot;)"" />
              <xsl:if test=""$var:v73"">
                <FreeText2>
                  <xsl:value-of select=""./text()"" />
                </FreeText2>
              </xsl:if>
            </xsl:for-each>
            <xsl:for-each select=""../../Note"">
              <xsl:variable name=""var:v74"" select=""position()"" />
              <xsl:variable name=""var:v75"" select=""userCSharp:LogicalEq(string($var:v74) , &quot;3&quot;)"" />
              <xsl:if test=""$var:v75"">
                <FreeText3>
                  <xsl:value-of select=""./text()"" />
                </FreeText3>
              </xsl:if>
            </xsl:for-each>
            <xsl:for-each select=""../../Note"">
              <xsl:variable name=""var:v76"" select=""position()"" />
              <xsl:variable name=""var:v77"" select=""userCSharp:LogicalEq(string($var:v76) , &quot;4&quot;)"" />
              <xsl:if test=""$var:v77"">
                <FreeText4>
                  <xsl:value-of select=""./text()"" />
                </FreeText4>
              </xsl:if>
            </xsl:for-each>
            <xsl:for-each select=""../../Note"">
              <xsl:variable name=""var:v78"" select=""position()"" />
              <xsl:variable name=""var:v79"" select=""userCSharp:LogicalEq(string($var:v78) , &quot;5&quot;)"" />
              <xsl:if test=""$var:v79"">
                <FreeText5>
                  <xsl:value-of select=""./text()"" />
                </FreeText5>
              </xsl:if>
            </xsl:for-each>
            <xsl:if test=""string($var:v83)='true'"">
              <xsl:variable name=""var:v84"" select=""string(../../BuyerCustomer/Concact/Name/text())"" />
              <xsl:variable name=""var:v85"" select=""userCSharp:StringTrimLeft($var:v84)"" />
              <xsl:variable name=""var:v86"" select=""userCSharp:StringTrimRight(string($var:v85))"" />
              <xsl:variable name=""var:v87"" select=""userCSharp:StringSubstring(string($var:v86) , &quot;1&quot; , &quot;20&quot;)"" />
              <ContactName>
                <xsl:value-of select=""$var:v87"" />
              </ContactName>
            </xsl:if>
            <xsl:variable name=""var:v88"" select=""userCSharp:ReturnComm(string(../../BuyerCustomer/Concact/Telephone/text()) , string(../../BuyerCustomer/Concact/Telefax/text()) , string(../../BuyerCustomer/Concact/E-mail/text()) , string(../../AccountingCustomer/Concact/Telephone/text()) , string(../../AccountingCustomer/Concact/Telefax/text()) , string(../../AccountingCustomer/Concact/E-mail/text()) , string(../../AccountingSupplier/Concact/Telephone/text()) , string(../../AccountingSupplier/Concact/Telefax/text()) , string(../../AccountingSupplier/Concact/E-mail/text()))"" />
            <xsl:variable name=""var:v89"" select=""userCSharp:StringTrimLeft(string($var:v88))"" />
            <xsl:variable name=""var:v90"" select=""userCSharp:StringTrimRight(string($var:v89))"" />
            <xsl:variable name=""var:v91"" select=""userCSharp:StringSubstring(string($var:v90) , &quot;1&quot; , &quot;13&quot;)"" />
            <COMM>
              <xsl:value-of select=""$var:v91"" />
            </COMM>
            <DeliveryAddress1>
              <xsl:value-of select=""$var:v94"" />
            </DeliveryAddress1>
            <xsl:variable name=""var:v107"" select=""userCSharp:DeliveryAddress2(string($var:v96) , string($var:v99) , string($var:v102) , string($var:v104) , string($var:v106))"" />
            <xsl:variable name=""var:v108"" select=""userCSharp:StringSubstring(string($var:v107) , &quot;1&quot; , &quot;35&quot;)"" />
            <DeliveryAddress2>
              <xsl:value-of select=""$var:v108"" />
            </DeliveryAddress2>
            <xsl:variable name=""var:v128"" select=""userCSharp:DeliveryAddress3(string($var:v111) , string($var:v116) , string($var:v121) , string($var:v124) , string($var:v127))"" />
            <xsl:variable name=""var:v129"" select=""userCSharp:StringSubstring(string($var:v128) , &quot;1&quot; , &quot;35&quot;)"" />
            <DeliveryAddress3>
              <xsl:value-of select=""$var:v129"" />
            </DeliveryAddress3>
            <xsl:variable name=""var:v130"" select=""userCSharp:DeliveryAddress4(string($var:v111) , string($var:v116) , string($var:v121) , string($var:v124) , string($var:v127))"" />
            <xsl:variable name=""var:v131"" select=""userCSharp:StringSubstring(string($var:v130) , &quot;1&quot; , &quot;35&quot;)"" />
            <DeliveryAddress4>
              <xsl:value-of select=""$var:v131"" />
            </DeliveryAddress4>
            <xsl:variable name=""var:v134"" select=""userCSharp:RemoveDecimals(string($var:v133))"" />
            <xsl:variable name=""var:v135"" select=""userCSharp:StringSubstring(string($var:v134) , &quot;1&quot; , &quot;5&quot;)"" />
            <TAX>
              <xsl:value-of select=""$var:v135"" />
            </TAX>
            <Currency>
              <xsl:value-of select=""$var:v137"" />
            </Currency>
            <xsl:variable name=""var:v140"" select=""userCSharp:ReturnCVR(string($var:v139))"" />
            <xsl:variable name=""var:v141"" select=""userCSharp:StringSubstring(string($var:v140) , &quot;1&quot; , &quot;24&quot;)"" />
            <SupplierCVR>
              <xsl:value-of select=""$var:v141"" />
            </SupplierCVR>
            <Separator>
              <xsl:text> </xsl:text>
            </Separator>
            <xsl:variable name=""var:v142"" select=""userCSharp:InitCumulativeConcat(0)"" />
            <xsl:for-each select=""/s0:Invoice/AllowanceCharge"">
              <xsl:variable name=""var:v143"" select=""userCSharp:StringUpperCase(string(ChargeIndicator/text()))"" />
              <xsl:variable name=""var:v144"" select=""userCSharp:LogicalEq(string($var:v143) , &quot;TRUE&quot;)"" />
              <xsl:if test=""string($var:v144)='true'"">
                <xsl:variable name=""var:v145"" select=""Amount/text()"" />
                <xsl:variable name=""var:v146"" select=""userCSharp:ReturnALCAmount(string($var:v145))"" />
                <xsl:variable name=""var:v147"" select=""userCSharp:AddToCumulativeConcat(0,string($var:v146),&quot;1000&quot;)"" />
              </xsl:if>
            </xsl:for-each>
            <xsl:variable name=""var:v148"" select=""userCSharp:GetCumulativeConcat(0)"" />
            <AlcAmount1>
              <xsl:value-of select=""$var:v148"" />
            </AlcAmount1>
            <xsl:variable name=""var:v149"" select=""userCSharp:InitCumulativeConcat(1)"" />
            <xsl:for-each select=""/s0:Invoice/AllowanceCharge"">
              <xsl:variable name=""var:v150"" select=""string(ChargeIndicator/text())"" />
              <xsl:variable name=""var:v151"" select=""userCSharp:StringUpperCase($var:v150)"" />
              <xsl:variable name=""var:v152"" select=""userCSharp:LogicalEq(string($var:v151) , &quot;TRUE&quot;)"" />
              <xsl:if test=""string($var:v152)='true'"">
                <xsl:variable name=""var:v153"" select=""AllowanceChargeReasonCode/text()"" />
                <xsl:variable name=""var:v154"" select=""userCSharp:ReturnALCCode(string($var:v153))"" />
                <xsl:variable name=""var:v155"" select=""userCSharp:AddToCumulativeConcat(1,string($var:v154),&quot;1000&quot;)"" />
              </xsl:if>
            </xsl:for-each>
            <xsl:variable name=""var:v156"" select=""userCSharp:GetCumulativeConcat(1)"" />
            <ChargeType>
              <xsl:value-of select=""$var:v156"" />
            </ChargeType>
            <xsl:variable name=""var:v157"" select=""userCSharp:InitCumulativeConcat(2)"" />
            <xsl:for-each select=""/s0:Invoice/AllowanceCharge"">
              <xsl:variable name=""var:v158"" select=""string(ChargeIndicator/text())"" />
              <xsl:variable name=""var:v159"" select=""userCSharp:StringUpperCase($var:v158)"" />
              <xsl:variable name=""var:v160"" select=""userCSharp:LogicalEq(string($var:v159) , &quot;TRUE&quot;)"" />
              <xsl:if test=""string($var:v160)='true'"">
                <xsl:variable name=""var:v161"" select=""AllowanceChargeReason/text()"" />
                <xsl:variable name=""var:v162"" select=""userCSharp:ReturnALCReasonN(string($var:v161) , string(AllowanceChargeReasonCode/text()) , string(ID/text()))"" />
                <xsl:variable name=""var:v163"" select=""userCSharp:AddToCumulativeConcat(2,string($var:v162),&quot;1000&quot;)"" />
              </xsl:if>
            </xsl:for-each>
            <xsl:variable name=""var:v164"" select=""userCSharp:GetCumulativeConcat(2)"" />
            <ChargeText>
              <xsl:value-of select=""$var:v164"" />
            </ChargeText>
            <xsl:variable name=""var:v165"" select=""userCSharp:InitCumulativeConcat(3)"" />
            <xsl:for-each select=""/s0:Invoice/AllowanceCharge"">
              <xsl:variable name=""var:v166"" select=""string(ChargeIndicator/text())"" />
              <xsl:variable name=""var:v167"" select=""userCSharp:StringUpperCase($var:v166)"" />
              <xsl:variable name=""var:v168"" select=""userCSharp:LogicalEq(string($var:v167) , &quot;FALSE&quot;)"" />
              <xsl:if test=""string($var:v168)='true'"">
                <xsl:variable name=""var:v169"" select=""Amount/text()"" />
                <xsl:variable name=""var:v170"" select=""userCSharp:ReturnALCAmount(string($var:v169))"" />
                <xsl:variable name=""var:v171"" select=""userCSharp:AddToCumulativeConcat(3,string($var:v170),&quot;1000&quot;)"" />
              </xsl:if>
            </xsl:for-each>
            <xsl:variable name=""var:v172"" select=""userCSharp:GetCumulativeConcat(3)"" />
            <AlcAmount2>
              <xsl:value-of select=""$var:v172"" />
            </AlcAmount2>
            <xsl:variable name=""var:v173"" select=""userCSharp:InitCumulativeConcat(4)"" />
            <xsl:for-each select=""/s0:Invoice/AllowanceCharge"">
              <xsl:variable name=""var:v174"" select=""string(ChargeIndicator/text())"" />
              <xsl:variable name=""var:v175"" select=""userCSharp:StringUpperCase($var:v174)"" />
              <xsl:variable name=""var:v176"" select=""userCSharp:LogicalEq(string($var:v175) , &quot;FALSE&quot;)"" />
              <xsl:if test=""string($var:v176)='true'"">
                <xsl:variable name=""var:v177"" select=""AllowanceChargeReasonCode/text()"" />
                <xsl:variable name=""var:v178"" select=""userCSharp:ReturnALCode(string($var:v177))"" />
                <xsl:variable name=""var:v179"" select=""userCSharp:AddToCumulativeConcat(4,string($var:v178),&quot;1000&quot;)"" />
              </xsl:if>
            </xsl:for-each>
            <xsl:variable name=""var:v180"" select=""userCSharp:GetCumulativeConcat(4)"" />
            <AllowanceType>
              <xsl:value-of select=""$var:v180"" />
            </AllowanceType>
            <xsl:variable name=""var:v181"" select=""userCSharp:InitCumulativeConcat(5)"" />
            <xsl:for-each select=""/s0:Invoice/AllowanceCharge"">
              <xsl:variable name=""var:v182"" select=""string(ChargeIndicator/text())"" />
              <xsl:variable name=""var:v183"" select=""userCSharp:StringUpperCase($var:v182)"" />
              <xsl:variable name=""var:v184"" select=""userCSharp:LogicalEq(string($var:v183) , &quot;FALSE&quot;)"" />
              <xsl:if test=""string($var:v184)='true'"">
                <xsl:variable name=""var:v185"" select=""AllowanceChargeReason/text()"" />
                <xsl:variable name=""var:v186"" select=""string(AllowanceChargeReasonCode/text())"" />
                <xsl:variable name=""var:v187"" select=""string(ID/text())"" />
                <xsl:variable name=""var:v188"" select=""userCSharp:ReturnALReasonN(string($var:v185) , $var:v186 , $var:v187)"" />
                <xsl:variable name=""var:v189"" select=""userCSharp:AddToCumulativeConcat(5,string($var:v188),&quot;1000&quot;)"" />
              </xsl:if>
            </xsl:for-each>
            <xsl:variable name=""var:v190"" select=""userCSharp:GetCumulativeConcat(5)"" />
            <AllowanceText>
              <xsl:value-of select=""$var:v190"" />
            </AllowanceText>
            <Separator1>
              <xsl:text>,</xsl:text>
            </Separator1>
            <SupplierAddress1>
              <xsl:value-of select=""$var:v193"" />
            </SupplierAddress1>
            <SupplierAddress2>
              <xsl:value-of select=""$var:v196"" />
            </SupplierAddress2>
            <SupplierAddress3>
              <xsl:value-of select=""$var:v202"" />
            </SupplierAddress3>
            <SupplierAddress4>
              <xsl:value-of select=""$var:v205"" />
            </SupplierAddress4>
          </Header>
        </xsl:for-each>
      </xsl:for-each>
      <xsl:for-each select=""Lines/Line"">
        <xsl:variable name=""var:v206"" select=""string(../../AccountingSupplier/AccSellerID/text())"" />
        <xsl:variable name=""var:v207"" select=""userCSharp:StringTrimLeft($var:v206)"" />
        <xsl:variable name=""var:v208"" select=""userCSharp:StringTrimRight(string($var:v207))"" />
        <xsl:variable name=""var:v210"" select=""string(../../BuyerCustomer/PartyID/text())"" />
        <xsl:variable name=""var:v211"" select=""userCSharp:StringTrimLeft($var:v210)"" />
        <xsl:variable name=""var:v212"" select=""userCSharp:StringTrimRight(string($var:v211))"" />
        <xsl:variable name=""var:v213"" select=""string(../../AccountingSupplier/PartyID/text())"" />
        <xsl:variable name=""var:v214"" select=""userCSharp:StringTrimLeft($var:v213)"" />
        <xsl:variable name=""var:v215"" select=""userCSharp:StringTrimRight(string($var:v214))"" />
        <xsl:variable name=""var:v218"" select=""userCSharp:StringTrimLeft(string(Item/StandardItemID/text()))"" />
        <xsl:variable name=""var:v219"" select=""userCSharp:StringTrimRight(string($var:v218))"" />
        <xsl:variable name=""var:v220"" select=""userCSharp:StringSubstring(string($var:v219) , &quot;1&quot; , &quot;13&quot;)"" />
        <xsl:variable name=""var:v221"" select=""userCSharp:StringTrimLeft(string(Item/AdditionalItemID/text()))"" />
        <xsl:variable name=""var:v222"" select=""userCSharp:StringTrimRight(string($var:v221))"" />
        <xsl:variable name=""var:v223"" select=""userCSharp:LogicalNe(string($var:v222) , &quot;&quot;)"" />
        <xsl:variable name=""var:v228"" select=""userCSharp:LogicalEq(string($var:v222) , &quot;&quot;)"" />
        <xsl:variable name=""var:v231"" select=""userCSharp:StringTrimLeft(string(Item/SellerItemID/text()))"" />
        <xsl:variable name=""var:v232"" select=""userCSharp:StringTrimRight(string($var:v231))"" />
        <xsl:variable name=""var:v233"" select=""userCSharp:StringSubstring(string($var:v232) , &quot;1&quot; , &quot;15&quot;)"" />
        <xsl:variable name=""var:v241"" select=""userCSharp:StringTrimLeft(string(UnitCode/text()))"" />
        <xsl:variable name=""var:v242"" select=""userCSharp:StringTrimRight(string($var:v241))"" />
        <xsl:variable name=""var:v243"" select=""userCSharp:StringUpperCase(string($var:v242))"" />
        <xsl:variable name=""var:v245"" select=""userCSharp:StringTrimLeft(string(PriceGross/Price/text()))"" />
        <xsl:variable name=""var:v246"" select=""userCSharp:StringTrimRight(string($var:v245))"" />
        <xsl:variable name=""var:v249"" select=""userCSharp:StringTrimLeft(string(PriceNet/Price/text()))"" />
        <xsl:variable name=""var:v250"" select=""userCSharp:StringTrimRight(string($var:v249))"" />
        <xsl:variable name=""var:v253"" select=""userCSharp:LogicalEq(string(TaxTotal/TaxSubtotal/TaxCategory/TaxScheme/ID/text()) , &quot;63&quot;)"" />
        <xsl:variable name=""var:v259"" select=""userCSharp:StringTrimLeft(string(Delivery/UnitCode/text()))"" />
        <xsl:variable name=""var:v260"" select=""userCSharp:StringTrimRight(string($var:v259))"" />
        <xsl:variable name=""var:v261"" select=""userCSharp:StringUpperCase(string($var:v260))"" />
        <xsl:variable name=""var:v263"" select=""userCSharp:StringTrimLeft(string(ConsumerUnit/UnitCode/text()))"" />
        <xsl:variable name=""var:v264"" select=""userCSharp:StringTrimRight(string($var:v263))"" />
        <xsl:variable name=""var:v265"" select=""userCSharp:StringUpperCase(string($var:v264))"" />
        <xsl:variable name=""var:v293"" select=""userCSharp:StringTrimLeft(string(LineAmountTotal/text()))"" />
        <xsl:variable name=""var:v294"" select=""userCSharp:StringTrimRight(string($var:v293))"" />
        <Body>
          <BodyTag>
            <xsl:text>205</xsl:text>
          </BodyTag>
          <TestInd>
            <xsl:text>0</xsl:text>
          </TestInd>
          <xsl:variable name=""var:v209"" select=""userCSharp:ReturnUNBSND(string($var:v208))"" />
          <UNBSND>
            <xsl:value-of select=""$var:v209"" />
          </UNBSND>
          <UNBRCV>
            <xsl:text>5790000252381</xsl:text>
          </UNBRCV>
          <NADBY>
            <xsl:value-of select=""$var:v212"" />
          </NADBY>
          <xsl:variable name=""var:v216"" select=""userCSharp:ReturnNADSU(string($var:v215))"" />
          <NADSU>
            <xsl:value-of select=""$var:v216"" />
          </NADSU>
          <NADIV>
            <xsl:text>5790000252381</xsl:text>
          </NADIV>
          <InvoicID>
            <xsl:value-of select=""../../InvoiceID/text()"" />
          </InvoicID>
          <xsl:variable name=""var:v217"" select=""userCSharp:ReturnLineID(string(LineNo/text()))"" />
          <LineID>
            <xsl:value-of select=""$var:v217"" />
          </LineID>
          <GTIN>
            <xsl:value-of select=""$var:v220"" />
          </GTIN>
          <xsl:if test=""string($var:v223)='true'"">
            <xsl:variable name=""var:v224"" select=""string(Item/AdditionalItemID/text())"" />
            <xsl:variable name=""var:v225"" select=""userCSharp:StringTrimLeft($var:v224)"" />
            <xsl:variable name=""var:v226"" select=""userCSharp:StringTrimRight(string($var:v225))"" />
            <xsl:variable name=""var:v227"" select=""userCSharp:StringSubstring(string($var:v226) , &quot;1&quot; , &quot;7&quot;)"" />
            <BuyerItemID>
              <xsl:value-of select=""$var:v227"" />
            </BuyerItemID>
          </xsl:if>
          <xsl:if test=""string($var:v228)='true'"">
            <xsl:variable name=""var:v229"" select=""Item/BuyerItemID/text()"" />
            <xsl:variable name=""var:v230"" select=""userCSharp:StringSubstring(string($var:v229) , &quot;1&quot; , &quot;7&quot;)"" />
            <BuyerItemID>
              <xsl:value-of select=""$var:v230"" />
            </BuyerItemID>
          </xsl:if>
          <SellerItemID>
            <xsl:value-of select=""$var:v233"" />
          </SellerItemID>
          <xsl:variable name=""var:v234"" select=""userCSharp:ReturnRFFON(string(OrderReference/Node/Code/text()) , string(OrderReference/Node/Reference/text()))"" />
          <RFFID>
            <xsl:value-of select=""$var:v234"" />
          </RFFID>
          <xsl:variable name=""var:v235"" select=""userCSharp:InitCumulativeConcat(6)"" />
          <xsl:for-each select=""Item"">
            <xsl:variable name=""var:v236"" select=""userCSharp:ReturnName(string(Description/text()) , string(Name/text()))"" />
            <xsl:variable name=""var:v237"" select=""userCSharp:ReturnDescription(string($var:v236))"" />
            <xsl:variable name=""var:v238"" select=""userCSharp:AddToCumulativeConcat(6,string($var:v237),&quot;1&quot;)"" />
          </xsl:for-each>
          <xsl:variable name=""var:v239"" select=""userCSharp:GetCumulativeConcat(6)"" />
          <Description>
            <xsl:value-of select=""$var:v239"" />
          </Description>
          <xsl:variable name=""var:v240"" select=""userCSharp:RemoveDecimals(string(Quantity/text()))"" />
          <InvoicedQuantity>
            <xsl:value-of select=""$var:v240"" />
          </InvoicedQuantity>
          <xsl:variable name=""var:v244"" select=""userCSharp:ReturnUnitCode(string($var:v243))"" />
          <UnitCode>
            <xsl:value-of select=""$var:v244"" />
          </UnitCode>
          <xsl:variable name=""var:v247"" select=""userCSharp:AddDecimals(string($var:v246) , &quot;2&quot;)"" />
          <xsl:variable name=""var:v248"" select=""userCSharp:ReplaceDotToComma1(string($var:v247))"" />
          <AlternativePriceAmount>
            <xsl:value-of select=""$var:v248"" />
          </AlternativePriceAmount>
          <xsl:variable name=""var:v251"" select=""userCSharp:AddDecimals(string($var:v250) , &quot;2&quot;)"" />
          <xsl:variable name=""var:v252"" select=""userCSharp:ReplaceDotToComma2(string($var:v251))"" />
          <PriceAmount>
            <xsl:value-of select=""$var:v252"" />
          </PriceAmount>
          <xsl:if test=""string($var:v253)='true'"">
            <xsl:variable name=""var:v254"" select=""TaxTotal/TaxAmount/text()"" />
            <xsl:variable name=""var:v255"" select=""userCSharp:StringTrimLeft(string($var:v254))"" />
            <xsl:variable name=""var:v256"" select=""userCSharp:StringTrimRight(string($var:v255))"" />
            <xsl:variable name=""var:v257"" select=""userCSharp:AddDecimals(string($var:v256) , &quot;2&quot;)"" />
            <xsl:variable name=""var:v258"" select=""userCSharp:ReplaceDotToComma3(string($var:v257))"" />
            <TaxTotalTaxAmount>
              <xsl:value-of select=""$var:v258"" />
            </TaxTotalTaxAmount>
          </xsl:if>
          <xsl:if test=""Delivery/Quantity"">
            <DeliveredQuantity>
              <xsl:value-of select=""Delivery/Quantity/text()"" />
            </DeliveredQuantity>
          </xsl:if>
          <xsl:if test=""ConsumerUnit/Quantity"">
            <QTY59>
              <xsl:value-of select=""ConsumerUnit/Quantity/text()"" />
            </QTY59>
          </xsl:if>
          <xsl:variable name=""var:v262"" select=""userCSharp:ReturnUnitCode(string($var:v261))"" />
          <DeliveredQuantityCode>
            <xsl:value-of select=""$var:v262"" />
          </DeliveredQuantityCode>
          <xsl:variable name=""var:v266"" select=""userCSharp:ReturnUnitCode(string($var:v265))"" />
          <QTY59Code>
            <xsl:value-of select=""$var:v266"" />
          </QTY59Code>
          <xsl:variable name=""var:v267"" select=""userCSharp:InitCumulativeConcat(7)"" />
          <xsl:for-each select=""AllowanceCharge"">
            <xsl:variable name=""var:v268"" select=""userCSharp:AddDecimals(string(Amount/text()) , &quot;2&quot;)"" />
            <xsl:variable name=""var:v269"" select=""userCSharp:ReplaceDotToComma5(string($var:v268))"" />
            <xsl:variable name=""var:v270"" select=""userCSharp:ReturnALCLineAmount(string($var:v269) , string(MultiplierFactorNumeric/text()))"" />
            <xsl:variable name=""var:v271"" select=""userCSharp:AddToCumulativeConcat(7,string($var:v270),&quot;1&quot;)"" />
          </xsl:for-each>
          <xsl:variable name=""var:v272"" select=""userCSharp:GetCumulativeConcat(7)"" />
          <RabatTilTabel>
            <xsl:value-of select=""$var:v272"" />
          </RabatTilTabel>
          <xsl:variable name=""var:v273"" select=""userCSharp:InitCumulativeConcat(8)"" />
          <xsl:for-each select=""AllowanceCharge"">
            <xsl:variable name=""var:v274"" select=""string(ChargeIndicator/text())"" />
            <xsl:variable name=""var:v275"" select=""userCSharp:StringUpperCase($var:v274)"" />
            <xsl:variable name=""var:v276"" select=""string(Amount/text())"" />
            <xsl:variable name=""var:v277"" select=""userCSharp:AddDecimals($var:v276 , &quot;2&quot;)"" />
            <xsl:variable name=""var:v278"" select=""userCSharp:ReplaceDotToComma5(string($var:v277))"" />
            <xsl:variable name=""var:v279"" select=""string(MultiplierFactorNumeric/text())"" />
            <xsl:variable name=""var:v280"" select=""userCSharp:ReturnRabatTILMrk(string($var:v275) , $var:v279 , string($var:v278))"" />
            <xsl:variable name=""var:v281"" select=""userCSharp:AddToCumulativeConcat(8,string($var:v280),&quot;1&quot;)"" />
          </xsl:for-each>
          <xsl:variable name=""var:v282"" select=""userCSharp:GetCumulativeConcat(8)"" />
          <RabatTilMrk>
            <xsl:value-of select=""$var:v282"" />
          </RabatTilMrk>
          <xsl:variable name=""var:v283"" select=""userCSharp:InitCumulativeConcat(9)"" />
          <xsl:for-each select=""AllowanceCharge"">
            <xsl:variable name=""var:v284"" select=""string(ChargeIndicator/text())"" />
            <xsl:variable name=""var:v285"" select=""userCSharp:StringUpperCase($var:v284)"" />
            <xsl:variable name=""var:v286"" select=""string(Amount/text())"" />
            <xsl:variable name=""var:v287"" select=""userCSharp:AddDecimals($var:v286 , &quot;2&quot;)"" />
            <xsl:variable name=""var:v288"" select=""userCSharp:ReplaceDotToComma5(string($var:v287))"" />
            <xsl:variable name=""var:v289"" select=""string(MultiplierFactorNumeric/text())"" />
            <xsl:variable name=""var:v290"" select=""userCSharp:ReturnRabatTILMrkPcd(string($var:v285) , $var:v289 , string($var:v288))"" />
            <xsl:variable name=""var:v291"" select=""userCSharp:AddToCumulativeConcat(9,string($var:v290),&quot;1&quot;)"" />
          </xsl:for-each>
          <xsl:variable name=""var:v292"" select=""userCSharp:GetCumulativeConcat(9)"" />
          <RabatPctKrMrk>
            <xsl:value-of select=""$var:v292"" />
          </RabatPctKrMrk>
          <xsl:variable name=""var:v295"" select=""userCSharp:AddDecimals(string($var:v294) , &quot;2&quot;)"" />
          <xsl:variable name=""var:v296"" select=""userCSharp:ReplaceDotToComma4(string($var:v295))"" />
          <PriceAmount203>
            <xsl:value-of select=""$var:v296"" />
          </PriceAmount203>
          <xsl:variable name=""var:v297"" select=""userCSharp:InitCumulativeConcat(10)"" />
          <xsl:for-each select=""../Line"">
            <xsl:variable name=""var:v298"" select=""userCSharp:ReturnNote(string(Note/text()))"" />
            <xsl:variable name=""var:v299"" select=""userCSharp:AddToCumulativeConcat(10,string($var:v298),&quot;1&quot;)"" />
          </xsl:for-each>
          <xsl:variable name=""var:v300"" select=""userCSharp:GetCumulativeConcat(10)"" />
          <Note>
            <xsl:value-of select=""$var:v300"" />
          </Note>
        </Body>
      </xsl:for-each>
      <xsl:for-each select=""TaxTotal"">
        <xsl:for-each select=""TaxSubtotal"">
          <xsl:variable name=""var:v301"" select=""string(../../AccountingSupplier/AccSellerID/text())"" />
          <xsl:variable name=""var:v302"" select=""userCSharp:StringTrimLeft($var:v301)"" />
          <xsl:variable name=""var:v303"" select=""userCSharp:StringTrimRight(string($var:v302))"" />
          <xsl:variable name=""var:v305"" select=""string(../../BuyerCustomer/PartyID/text())"" />
          <xsl:variable name=""var:v306"" select=""userCSharp:StringTrimLeft($var:v305)"" />
          <xsl:variable name=""var:v307"" select=""userCSharp:StringTrimRight(string($var:v306))"" />
          <xsl:variable name=""var:v308"" select=""string(../../AccountingSupplier/PartyID/text())"" />
          <xsl:variable name=""var:v309"" select=""userCSharp:StringTrimLeft($var:v308)"" />
          <xsl:variable name=""var:v310"" select=""userCSharp:StringTrimRight(string($var:v309))"" />
          <Footer>
            <FooterTag>
              <xsl:text>305</xsl:text>
            </FooterTag>
            <TestInd>
              <xsl:text>0</xsl:text>
            </TestInd>
            <xsl:variable name=""var:v304"" select=""userCSharp:ReturnUNBSND(string($var:v303))"" />
            <UNBSND>
              <xsl:value-of select=""$var:v304"" />
            </UNBSND>
            <UNBRCV>
              <xsl:text>5790000252381</xsl:text>
            </UNBRCV>
            <NADBY>
              <xsl:value-of select=""$var:v307"" />
            </NADBY>
            <xsl:variable name=""var:v311"" select=""userCSharp:ReturnNADSU(string($var:v310))"" />
            <NADSU>
              <xsl:value-of select=""$var:v311"" />
            </NADSU>
            <NADIV>
              <xsl:text>5790000252381</xsl:text>
            </NADIV>
            <InvoicID>
              <xsl:value-of select=""../../InvoiceID/text()"" />
            </InvoicID>
            <xsl:variable name=""var:v312"" select=""userCSharp:AddDecimals(string(TaxableAmount/text()) , &quot;2&quot;)"" />
            <xsl:variable name=""var:v313"" select=""userCSharp:ReplaceDotToComma6(string($var:v312))"" />
            <TaxSubtotal_TaxableAmount>
              <xsl:value-of select=""$var:v313"" />
            </TaxSubtotal_TaxableAmount>
            <xsl:variable name=""var:v314"" select=""userCSharp:AddDecimals(string(../../Total/TaxExclAmount/text()) , &quot;2&quot;)"" />
            <xsl:variable name=""var:v315"" select=""userCSharp:ReplaceDotToComma7(string($var:v314))"" />
            <TaxTotal_TaxAmount>
              <xsl:value-of select=""$var:v315"" />
            </TaxTotal_TaxAmount>
            <xsl:variable name=""var:v316"" select=""userCSharp:AddDecimals(string(../../Total/PayableAmount/text()) , &quot;2&quot;)"" />
            <xsl:variable name=""var:v317"" select=""userCSharp:ReplaceDotToComma8(string($var:v316))"" />
            <LegalMonetaryTotal_PayableAmount>
              <xsl:value-of select=""$var:v317"" />
            </LegalMonetaryTotal_PayableAmount>
            <xsl:variable name=""var:v318"" select=""userCSharp:AddDecimals(string(../../Total/LineTotalAmount/text()) , &quot;2&quot;)"" />
            <xsl:variable name=""var:v319"" select=""userCSharp:ReplaceDotToComma9(string($var:v318))"" />
            <LegalMonetaryTotal_LineExtensionAmount>
              <xsl:value-of select=""$var:v319"" />
            </LegalMonetaryTotal_LineExtensionAmount>
            <xsl:variable name=""var:v320"" select=""userCSharp:AddDecimals(string(../../Total/PrepaidAmount/text()) , &quot;2&quot;)"" />
            <xsl:variable name=""var:v321"" select=""userCSharp:ReplaceDotToComma10(string($var:v320))"" />
            <UBLExtensionsContent_ExtensionAmount>
              <xsl:value-of select=""$var:v321"" />
            </UBLExtensionsContent_ExtensionAmount>
            <X>
              <xsl:text>X</xsl:text>
            </X>
            <xsl:variable name=""var:v322"" select=""userCSharp:ReturnGUID()"" />
            <GUID>
              <xsl:value-of select=""$var:v322"" />
            </GUID>
            <TotalSeparator1>
              <xsl:text> </xsl:text>
            </TotalSeparator1>
          </Footer>
        </xsl:for-each>
      </xsl:for-each>
    </ns0:XLBYGCompressedInvoice>
  </xsl:template>
  <msxsl:script language=""C#"" implements-prefix=""userCSharp""><![CDATA[
public string ReturnIssueDate(string IssueDate)
{
	return IssueDate.Replace(""-"", """");
}


public string ReturnInvoiceType(string InvoiceType)
{
String InvoiceTypeReturn;
switch (InvoiceType)
{
 case ""380"": InvoiceTypeReturn = ""D""; break;
 case ""381"": InvoiceTypeReturn = ""K""; break;
 default: InvoiceTypeReturn = ""D""; break;
}
	return InvoiceTypeReturn;
}


public string ReturnUNBSND(string AccSellerID)

{

string UNBSND;
string Prefix;
int AccSellerIDLen;

AccSellerIDLen = AccSellerID.Length;

if (AccSellerIDLen < 13) 
{
Prefix = ""9"";
for (int i = 0; i < 13 - (AccSellerIDLen + 1); i++)
{
Prefix = Prefix + ""0"";
}
UNBSND = Prefix + AccSellerID;
}
else
{
UNBSND = AccSellerID;
}

return UNBSND;

}

public string ReturnDeliveryDate(string ActualDeliveryDate)
{
	return ActualDeliveryDate.Replace(""-"", """");
}


public string StringTrimLeft(string str)
{
	if (str == null)
	{
		return """";
	}
	return str.TrimStart(null);
}


public string StringTrimRight(string str)
{
	if (str == null)
	{
		return """";
	}
	return str.TrimEnd(null);
}


public string ReturnNADSU(string PartyID)

{

string NADSU;
string Prefix;
int PartyIDLen;

PartyIDLen = PartyID.Length;

if (PartyIDLen < 13) 
{
Prefix = ""9"";
for (int i = 0; i < 13 - (PartyIDLen + 1); i++)
{
Prefix = Prefix + ""0"";
}
NADSU = Prefix + PartyID;
}
else
{
NADSU = PartyID;
}

return NADSU;

}

public bool LogicalNe(string val1, string val2)
{
	bool ret = false;
	double d1 = 0;
	double d2 = 0;
	if (IsNumeric(val1, ref d1) && IsNumeric(val2, ref d2))
	{
		ret = d1 != d2;
	}
	else
	{
		ret = String.Compare(val1, val2, StringComparison.Ordinal) != 0;
	}
	return ret;
}


public bool LogicalAnd(string param0, string param1)
{
	return ValToBool(param0) && ValToBool(param1);
	return false;
}


public bool LogicalEq(string val1, string val2)
{
	bool ret = false;
	double d1 = 0;
	double d2 = 0;
	if (IsNumeric(val1, ref d1) && IsNumeric(val2, ref d2))
	{
		ret = d1 == d2;
	}
	else
	{
		ret = String.Compare(val1, val2, StringComparison.Ordinal) == 0;
	}
	return ret;
}


public bool LogicalOr(string param0, string param1)
{
	return ValToBool(param0) || ValToBool(param1);
	return false;
}


public string StringLeft(string str, string count)
{
	string retval = """";
	double d = 0;
	if (str != null && IsNumeric(count, ref d))
	{
		int i = (int) d;
		if (i > 0)
		{ 
			if (i <= str.Length)
			{
				retval = str.Substring(0, i);
			}
			else
			{
				retval = str;
			}
		}
	}
	return retval;
}


public string StringSubstring(string str, string left, string right)
{
	string retval = """";
	double dleft = 0;
	double dright = 0;
	if (str != null && IsNumeric(left, ref dleft) && IsNumeric(right, ref dright))
	{
		int lt = (int)dleft;
		int rt = (int)dright;
		lt--; rt--;
		if (lt >= 0 && rt >= lt && lt < str.Length)
		{
			if (rt < str.Length)
			{
				retval = str.Substring(lt, rt-lt+1);
			}
			else
			{
				retval = str.Substring(lt, str.Length-lt);
			}
		}
	}
	return retval;
}


public string ReturnDeliveryDueDate1(string DeliveryDueDate1)
{
	return DeliveryDueDate1.Replace(""-"", """");
}


public string ReturnDeliveryDueDate(string DeliveryDueDate)
{
	return DeliveryDueDate.Replace(""-"", """");
}


public string ReturnComm(string BCT, string BCF, string BCE, string ACT, string ACF, string ACE, string AST, string ASF, string ASE)

{

string Comm;

Comm = """";

if (BCE != """")
{
Comm = BCE;
}
else if (BCE == """" && BCT != """")
{
Comm = BCT;
}

else if (BCE == """" && BCT == """" && BCF != """")
{
Comm = BCF;
}

else if (BCT == """" && BCF == """" && BCE == """" && ACE != """")
{
Comm = ACE;
}

else if (BCT == """" && BCF == """" && BCE == """" && ACE == """" && ACT != """")
{
Comm = ACT;
}

else if (BCT == """" && BCF == """" && BCE == """" && ACE == """" && ACT == """" && ACF != """")
{
Comm = ACF;
}

else if (BCT == """" && BCF == """" && BCE == """" && ACT == """" && ACF == """" && ACE == """" && ASE != """")
{
Comm = ASE;
}

else if (BCT == """" && BCF == """" && BCE == """" && ACT == """" && ACF == """" && ACE == """" && ASE == """" && AST != """")
{
Comm = AST;
}

else if (BCT == """" && BCF == """" && BCE == """" && ACT == """" && ACF == """" && ACE == """" && ASE == """" && AST == """" && ASF != """")
{
Comm = ASF;
}

return Comm;

}

public string StringConcat(string param0, string param1, string param2)
{
   return param0 + param1 + param2;
}


public string DeliveryAddress2(string MarkAtt, string Street, string City, string Country, string Department)

{

string DeliveryAddress2;

DeliveryAddress2 = """";

if (MarkAtt != """")
{
DeliveryAddress2 = MarkAtt;
}
else if (MarkAtt == """" && Street != """")
{
DeliveryAddress2 = Street;
}
else if (MarkAtt == """" && Street == """" && City != """")
{
DeliveryAddress2 = City;
}
else if (MarkAtt == """" && Street == """" && City == """" && Country != """")
{
DeliveryAddress2 = Country; 
}
else
{
DeliveryAddress2 = Department; 
}

return DeliveryAddress2; 

}

public string DeliveryAddress3(string MarkAtt, string Street, string City, string Country, string Department)

{

string DeliveryAddress3;

DeliveryAddress3 = """";

if (MarkAtt != """" && Street != """")
{
DeliveryAddress3 = Street;
}
else if (MarkAtt == """" && Street != """" && City != """")
{
DeliveryAddress3 = City;
}
else if (MarkAtt == """" && Street != """" && City == """" && Country != """")
{
DeliveryAddress3 = Country;
}

else
{
DeliveryAddress3 = Department; 
}

return DeliveryAddress3; 

}

public string DeliveryAddress4(string MarkAtt, string Street, string City, string Country, string Department)

{

string DeliveryAddress4;

DeliveryAddress4 = """";

if (MarkAtt != """" && Street != """" && City != """")
{
DeliveryAddress4 = City;
}
else if (MarkAtt == """" && Street != """" && City != """"  && Country != """")
{
DeliveryAddress4 = Country;
}

else
{
DeliveryAddress4 = Department; 
}

return DeliveryAddress4; 

}

public string StringUpperCase(string str)
{
	if (str == null)
	{
		return """";
	}
	return str.ToUpper(System.Globalization.CultureInfo.InvariantCulture);
}


public string InitCumulativeConcat(int index)
{
	if (index >= 0)
	{
		if (index >= myCumulativeConcatArray.Count)
		{
			int i = myCumulativeConcatArray.Count;
			for (; i<=index; i++)
			{
				myCumulativeConcatArray.Add("""");
			}
		}
		else
		{
			myCumulativeConcatArray[index] = """";
		}
	}
	return """";
}

public System.Collections.ArrayList myCumulativeConcatArray = new System.Collections.ArrayList();

public string AddToCumulativeConcat(int index, string val, string notused)
{
	if (index < 0 || index >= myCumulativeConcatArray.Count)
	{
		return """";
	}
	myCumulativeConcatArray[index] = (string)(myCumulativeConcatArray[index]) + val;
	return myCumulativeConcatArray[index].ToString();
}

public string GetCumulativeConcat(int index)
{
	if (index < 0 || index >= myCumulativeConcatArray.Count)
	{
		return """";
	}
	return myCumulativeConcatArray[index].ToString();
}

public string ReturnALCReasonN(string ALCReason, string ALCReasonCode, string ALCReasonID)

{

string ALCReasonText;
string Postfix;
int ALCReasonLen;
int ALCReasonIDLen;
string TempText;

ALCReasonLen = ALCReason.Length;
ALCReasonIDLen = ALCReasonID.Length;

if (ALCReasonLen != 0)
{ 
if (ALCReasonLen < 35) 
{
Postfix = """";
for (int i = 0; i < 35 - ALCReasonLen; i++)
{
Postfix = Postfix + "" "";
}
ALCReasonText = ALCReason + Postfix;
}
else
{
ALCReasonText = ALCReason;
}
}

else if (ALCReasonIDLen != 0)
{ 
if (ALCReasonIDLen < 35) 
{
Postfix = """";
for (int i = 0; i < 35 - ALCReasonIDLen; i++)
{
Postfix = Postfix + "" "";
}
ALCReasonText = ALCReasonID + Postfix;
}
else
{
ALCReasonText = ALCReasonID;
}
}


else
{
switch (ALCReasonCode)
      {
          case ""AJ"":
              TempText = ""Øreafrunding"";
              break;
          case ""CAC"":
              TempText = ""Kontantrabat"";
              break;
          case ""DBD"":
              TempText = ""Særligt Fradrag/Tillæg"";
              break;
          case ""DI"":
              TempText = ""Rabat"";
              break;
          case ""EP"":
              TempText = ""Miljøgebyr"";
              break;
          case ""FC"":
              TempText = ""Fragt"";
              break;
          case ""FI"":
              TempText = ""Finansielle tillæg"";
              break;
          case ""HD"":
              TempText = ""Håndtering"";
              break;
          case ""IN"":
              TempText = ""Forsikring"";
              break;
          case ""PC"":
              TempText = ""Emballage"";
              break;
          case ""SH"":
              TempText = ""Gebyr"";
              break;
          case ""QD"":
              TempText = ""Mængderabat"";
              break;
          default:
              TempText = ""Fragt/Porto/Gebyr"";
              break;
      }


ALCReasonLen = TempText.Length;

if (ALCReasonLen < 35) 
{
Postfix = """";
for (int i = 0; i < 35 - ALCReasonLen; i++)
{
Postfix = Postfix + "" "";
}
ALCReasonText = TempText + Postfix;
}
else
{
ALCReasonText = TempText;
}
}
return ALCReasonText;

}


public string ReturnALReasonN(string ALReason, string ALReasonCode, string ALReasonID)

{

string ALReasonText;
string Postfix;
int ALReasonLen;
int ALReasonIDLen;
string TempText1;

ALReasonLen = ALReason.Length;
ALReasonIDLen = ALReasonID.Length;


if (ALReasonLen != 0)
{ 
if (ALReasonLen < 35) 
{
Postfix = """";
for (int i = 0; i < 35 - ALReasonLen; i++)
{
Postfix = Postfix + "" "";
}
ALReasonText = ALReason + Postfix;
}
else
{
ALReasonText = ALReason;
}
}

else if (ALReasonIDLen != 0)
{ 
if (ALReasonIDLen < 35) 
{
Postfix = """";
for (int i = 0; i < 35 - ALReasonIDLen; i++)
{
Postfix = Postfix + "" "";
}
ALReasonText = ALReasonID + Postfix;
}
else
{
ALReasonText = ALReasonID;
}
}

else
{
switch (ALReasonCode)
      {
          case ""AJ"":
              TempText1 = ""Øreafrunding"";
              break;
          case ""CAC"":
              TempText1 = ""Kontantrabat"";
              break;
          case ""DBD"":
              TempText1 = ""Særligt Fradrag/Tillæg"";
              break;
          case ""DI"":
              TempText1 = ""Rabat"";
              break;
          case ""EP"":
              TempText1 = ""Miljøgebyr"";
              break;
          case ""FC"":
              TempText1 = ""Fragt"";
              break;
          case ""FI"":
              TempText1 = ""Finansielle tillæg"";
              break;
          case ""HD"":
              TempText1 = ""Håndtering"";
              break;
          case ""IN"":
              TempText1 = ""Forsikring"";
              break;
          case ""PC"":
              TempText1 = ""Emballage"";
              break;
          case ""SH"":
              TempText1 = ""Gebyr"";
              break;
          case ""QD"":
              TempText1 = ""Mængderabat"";
              break;
          default:
              TempText1 = ""Fragt/Porto/Gebyr"";
              break;
      }


ALReasonLen = TempText1.Length;

if (ALReasonLen < 35) 
{
Postfix = """";
for (int i = 0; i < 35 - ALReasonLen; i++)
{
Postfix = Postfix + "" "";
}
ALReasonText = TempText1 + Postfix;
}
else
{
ALReasonText = TempText1;
}
}
return ALReasonText;

}



public string ReturnALCCode(string ALCCode)

{

string ALCCodeText;
string Postfix;
int ALCCodeLen;

ALCCodeLen = ALCCode.Length;

if (ALCCodeLen != 0)
{ 
if (ALCCodeLen < 3) 
{
Postfix = """";
for (int i = 0; i < 3 - ALCCodeLen; i++)
{
Postfix = Postfix + "" "";
}
ALCCodeText = ALCCode + Postfix;
}
else
{
ALCCodeText = ALCCode;
}
}
else
{
ALCCodeText = ""FC "";
}

return ALCCodeText;

}

public string ReturnALCode(string ALCode)

{

string ALCodeText;
string Postfix;
int ALCodeLen;

ALCodeLen = ALCode.Length;

if (ALCodeLen != 0)
{ 
if (ALCodeLen < 3) 
{
Postfix = """";
for (int i = 0; i < 3 - ALCodeLen; i++)
{
Postfix = Postfix + "" "";
}
ALCodeText = ALCode + Postfix;
}
else
{
ALCodeText = ALCode;
}
}
else
{
ALCodeText = ""DI "";
}

return ALCodeText;

}

public string ReturnALCAmount(string ALCAmount)

{

string ALCAmountText;
string Postfix;
int ALCAmountLen;

ALCAmountLen = ALCAmount.Length;

if (ALCAmountLen != 0)
{ 
if (ALCAmountLen < 10) 
{
Postfix = """";
for (int i = 0; i < 10 - ALCAmountLen; i++)
{
Postfix = Postfix + "" "";
}
ALCAmountText = ALCAmount + Postfix;
}
else
{
ALCAmountText = ALCAmount;
}
}
else
{
ALCAmountText = ""          "";
}

return ALCAmountText.Replace(""."", "","");

}

public string ReturnCVR(string CVR)

{

string RetCVR;

RetCVR = """";

if (CVR.Substring(0, 2) == ""SE"" | CVR.Substring(0, 2) == ""NL"")
{
RetCVR = CVR;

}
else if (CVR.Substring(0, 2) == ""DK"")
{
RetCVR = ""CVR-NR.: "" + CVR;

}
else if (CVR.Length == 13)
{
 RetCVR = ""GLN: "" + CVR;

}
else
{
RetCVR = ""CVR-NR.: DK-"" + CVR;

}
return RetCVR;
}

public string RemoveDecimals(string OriginalAmount)
        {

            string NewAmount;
            string Separator;

            NewAmount = OriginalAmount; 
            Separator = ""."";
 
            if (string.IsNullOrWhiteSpace(OriginalAmount))
                return OriginalAmount;

            int charLocation = OriginalAmount.IndexOf(Separator, StringComparison.Ordinal);

            if (charLocation > 0)
            {
                NewAmount =  OriginalAmount.Substring(0, charLocation);
            }

          return NewAmount;
        }	

public string ReturnLineID(string LineID)

{

string LineIDText;
string Prefix;
int LineIDLen;

LineIDLen = LineID.Length;

if (LineIDLen < 6) 
{
Prefix = """";
for (int i = 0; i < 6 - LineIDLen; i++)
{
Prefix = Prefix + ""0"";
}
LineIDText = Prefix + LineID;
}
else
{
LineIDText = LineID;
}

return LineIDText;

}

public string ReturnRFFON(string Code, string RFF)
{
string RFFON = """";
if (Code == ""ON"" && RFF != """")
{
 RFFON = RFF;
}
return RFFON;
}

public string ReplaceDotToComma2(string InputValue)
{
                int Ln = InputValue.Length; 
                if (InputValue.IndexOf(""."") != -1)
                 {                   
                   if ((Ln - 2) > 0)
                    {
           if (InputValue.Substring(Ln -  2, 2) == ""00"")
            {
              InputValue = InputValue.Substring(0, Ln - 3);
             }
            else
             if (InputValue.Substring(Ln -  1, 1) == ""0"" && InputValue.Substring(Ln -  2, 2) != ""00"")
            {
              InputValue = InputValue.Substring(0, Ln - 1);
             }
                    }
                 }
	 return InputValue.Replace(""."", "","");
     
}


public string ReplaceDotToComma3(string InputValue)
{
                int Ln = InputValue.Length; 
                if (InputValue.IndexOf(""."") != -1)
                 {                   
                   if ((Ln - 2) > 0)
                    {
           if (InputValue.Substring(Ln -  2, 2) == ""00"")
            {
              InputValue = InputValue.Substring(0, Ln - 3);
             }
            else
             if (InputValue.Substring(Ln -  1, 1) == ""0"" && InputValue.Substring(Ln -  2, 2) != ""00"")
            {
              InputValue = InputValue.Substring(0, Ln - 1);
             }
                    }
                 }
	 return InputValue.Replace(""."", "","");
     
}


public string ReplaceDotToComma4(string InputValue)
{
                int Ln = InputValue.Length; 
                if (InputValue.IndexOf(""."") != -1)
                 {                   
                   if ((Ln - 2) > 0)
                    {
           if (InputValue.Substring(Ln -  2, 2) == ""00"")
            {
              InputValue = InputValue.Substring(0, Ln - 3);
             }
            else
             if (InputValue.Substring(Ln -  1, 1) == ""0"" && InputValue.Substring(Ln -  2, 2) != ""00"")
            {
              InputValue = InputValue.Substring(0, Ln - 1);
             }
                    }
                 }
	 return InputValue.Replace(""."", "","");
     
}


public string ReturnUnitCode(string UnitCode)
{
string TBFUnitCode;
TBFUnitCode = """";

switch (UnitCode)
{
case ""%ST"": TBFUnitCode = ""CEN""; break;
case ""TUS"": TBFUnitCode = ""MIL""; break;
case ""ST"": TBFUnitCode = ""ARK""; break;
case ""BDT"": TBFUnitCode = ""BNT""; break;
case ""BLK"": TBFUnitCode = ""BLO""; break;
case ""BX"": TBFUnitCode = ""BOX""; break;
case ""PCE"": TBFUnitCode = ""STK""; break;
case ""BOT"": TBFUnitCode = ""BØT""; break;
case ""CMT"": TBFUnitCode = ""CM""; break;
case ""CH"": TBFUnitCode = ""CON""; break;
case ""DIS"": TBFUnitCode = ""DSP""; break;
case ""DMT"": TBFUnitCode = ""DM""; break;
case ""DS"": TBFUnitCode = ""BOKS""; break;
case ""FL"": TBFUnitCode = ""FL""; break;
case ""GRM"": TBFUnitCode = ""G""; break;
case ""HLT"": TBFUnitCode = ""HL""; break;
case ""KAR"": TBFUnitCode = ""KRT""; break;
case ""KS"": TBFUnitCode = ""KAS""; break;
case ""KGM"": TBFUnitCode = ""KG""; break;
case ""MTQ"": TBFUnitCode = ""M3""; break;
case ""MTK"": TBFUnitCode = ""M2""; break;
case ""LGD"": TBFUnitCode = ""LGD""; break;
case ""LTR"": TBFUnitCode = ""LTR""; break;
case ""LES"": TBFUnitCode = ""LAS""; break;
case ""MTR"": TBFUnitCode = ""M""; break;
case ""MMT"": TBFUnitCode = ""MM""; break;
case ""PAK"": TBFUnitCode = ""PAK""; break;
case ""PF"": TBFUnitCode = ""PAL""; break;
case ""PAR"": TBFUnitCode = ""PAR""; break;
case ""PAT"": TBFUnitCode = ""PAT""; break;
case ""PL"": TBFUnitCode = ""PLA""; break;
case ""PS"": TBFUnitCode = ""POS""; break;
case ""RIN"": TBFUnitCode = ""KVL""; break;
case ""NRL"": TBFUnitCode = ""RUL""; break;
case ""ROR"": TBFUnitCode = ""RØR""; break;
case ""SP"": TBFUnitCode = ""SPA""; break;
case ""SPO"": TBFUnitCode = ""SNL""; break;
case ""SAK"": TBFUnitCode = ""SEK""; break;
case ""SET"": TBFUnitCode = ""SET""; break;
case ""HUR"": TBFUnitCode = ""T""; break;
case ""TNE"": TBFUnitCode = ""TON""; break;
case ""TR"": TBFUnitCode = ""TRM""; break;
case ""TB"": TBFUnitCode = ""TUB""; break;
case ""ASK"": TBFUnitCode = ""ESK""; break;
case ""BÅND"": TBFUnitCode = ""BND""; break;
case ""BREV"": TBFUnitCode = ""BRV""; break;
case ""COL"": TBFUnitCode = ""COL""; break;
case ""HAP"": TBFUnitCode = ""HAP""; break;
case ""KORT"": TBFUnitCode = ""KOR""; break;
case ""PB"": TBFUnitCode = ""PB""; break;
case ""ROND"": TBFUnitCode = ""RON""; break;
case ""SKF"": TBFUnitCode = ""SKF""; break;
case ""TUBE"": TBFUnitCode = ""TB""; break;
case ""XXX"": TBFUnitCode = ""XXX""; break;
case ""C62"": TBFUnitCode = ""STK""; break;
default: TBFUnitCode = UnitCode; break; 
}
	return TBFUnitCode;
}


public string AddDecimals(string OriginalAmount, string numberOfDecimals)
        {
            if (string.IsNullOrWhiteSpace(OriginalAmount))
                return OriginalAmount;
            decimal d = System.Convert.ToDecimal(OriginalAmount, System.Globalization.CultureInfo.InvariantCulture);
string format = ""0."" + new string('0', System.Convert.ToInt32(numberOfDecimals));
            string NewAmount = d.ToString(format, System.Globalization.CultureInfo.InvariantCulture);
            return NewAmount;
		}	

public string ReplaceDotToComma1(string InputValue)
{
                int Ln = InputValue.Length; 
                if (InputValue.IndexOf(""."") != -1)
                 {                   
                   if ((Ln - 2) > 0)
                    {
           if (InputValue.Substring(Ln -  2, 2) == ""00"")
            {
              InputValue = InputValue.Substring(0, Ln - 3);
             }
            else
             if (InputValue.Substring(Ln -  1, 1) == ""0"" && InputValue.Substring(Ln -  2, 2) != ""00"")
            {
              InputValue = InputValue.Substring(0, Ln - 1);
             }
                    }
                 }
	 return InputValue.Replace(""."", "","");
     
}


public string ReturnNote(string Note)

{

string Postfix;
int NoteLen;

Postfix = """";

NoteLen = Note.Length;

if (NoteLen < 35) 
{
for (int i = 0; i < 35 - NoteLen; i++)
{
Postfix = Postfix + "" "";
}
}

return Note+Postfix;

}

public string ReturnRabatTILMrk(string ChargeIndicator, string MultipleFactorNumeric, string Amount)

{

string RabatTILMrkText;

RabatTILMrkText = """";

switch (ChargeIndicator)
      {
          case ""TRUE"":
              if (MultipleFactorNumeric.Length != 0)
               {
                RabatTILMrkText = RabatTILMrkText + ""2"";
               }
              if (Amount.Length != 0)
               {
                RabatTILMrkText = RabatTILMrkText + ""2"";
               }
              break;
          case ""FALSE"":
              if (MultipleFactorNumeric.Length != 0)
               {
                RabatTILMrkText = RabatTILMrkText + ""1"";
               }
              if (Amount.Length != 0)
               {
                RabatTILMrkText = RabatTILMrkText + ""1"";
               }
              break;
     }

return RabatTILMrkText;

}

public string ReturnRabatTILMrkPcd(string ChargeIndicator, string MultipleFactorNumeric, string Amount)

{

string RabatTILMrkPcdText;

RabatTILMrkPcdText = """";

switch (ChargeIndicator)
      {
          case ""TRUE"":
              if (MultipleFactorNumeric.Length != 0)
               {
                RabatTILMrkPcdText = RabatTILMrkPcdText + ""1"";
               }
              if (Amount.Length != 0)
               {
                RabatTILMrkPcdText = RabatTILMrkPcdText + ""2"";
               }
              break;
          case ""FALSE"":
              if (MultipleFactorNumeric.Length != 0)
               {
                RabatTILMrkPcdText = RabatTILMrkPcdText + ""1"";
               }
              if (Amount.Length != 0)
               {
                RabatTILMrkPcdText = RabatTILMrkPcdText + ""2"";
               }
              break;
     }

return RabatTILMrkPcdText;

}


public string ReturnALCLineAmount(string ALCLineAmount, string ALCLinePercentage)

{

string ALCLineAmountText;
string Postfix;
int ALCLineAmountLen;


double PCD;
string TMP;
string ALCLinePCDText;
string Postfix2;
int ALCLinePCDLen;


Double.TryParse(ALCLinePercentage, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out PCD);
PCD = PCD*100;
TMP = PCD.ToString();

ALCLinePCDLen = TMP.Length;

if (ALCLinePCDLen != 0 && TMP != ""0"")
{ 
if (ALCLinePCDLen < 8) 
{
Postfix2 = """";
for (int i = 0; i < 8 - ALCLinePCDLen; i++)
{
Postfix2 = Postfix2 + "" "";
}
ALCLinePCDText = TMP + Postfix2;
}
else
{
ALCLinePCDText = TMP;
}
}
else
{
ALCLinePCDText = """";
}

ALCLineAmountLen = ALCLineAmount.Length;

if (ALCLineAmountLen != 0 && ALCLineAmount != ""0"" && ALCLineAmount != ""0.00"")
{ 
if (ALCLineAmountLen < 8) 
{
Postfix = """";
for (int i = 0; i < 8 - ALCLineAmountLen; i++)
{
Postfix = Postfix + "" "";
}
ALCLineAmountText = ALCLineAmount + Postfix;
}
else
{
ALCLineAmountText = ALCLineAmount;
}
}
else
{
ALCLineAmountText = ""        "";
}

return ALCLinePCDText.Replace(""."", "","") + ALCLineAmountText.Replace(""."", "","");

}

public string ReplaceDotToComma5(string InputValue)
{
                int Ln = InputValue.Length; 
                if (InputValue.IndexOf(""."") != -1)
                 {                   
                   if ((Ln - 2) > 0)
                    {
           if (InputValue.Substring(Ln -  2, 2) == ""00"")
            {
              InputValue = InputValue.Substring(0, Ln - 3);
             }
            else
             if (InputValue.Substring(Ln -  1, 1) == ""0"" && InputValue.Substring(Ln -  2, 2) != ""00"")
            {
              InputValue = InputValue.Substring(0, Ln - 1);
             }
                    }
                 }
	 return InputValue.Replace(""."", "","");
     
}


public string ReturnDescription(string Name)

{

string Postfix;
int NameLen;

Postfix = """";

NameLen = Name.Length;

if (NameLen < 35) 
{
for (int i = 0; i < 35 - NameLen; i++)
{
Postfix = Postfix + "" "";
}
}

return Name+Postfix;

}

public string ReturnName(string Name1, string Name2)

{

string Name;

Name = """";

if (Name1 != """")
{
Name = Name1;
}
else if (Name1 == """" && Name2 != """")
{
Name = Name2;
}

return Name.Replace(""#;#"", """"); 

}

public string ReplaceDotToComma6(string InputValue)
{
                int Ln = InputValue.Length; 
                if (InputValue.IndexOf(""."") != -1)
                 {                   
                   if ((Ln - 2) > 0)
                    {
           if (InputValue.Substring(Ln -  2, 2) == ""00"")
            {
              InputValue = InputValue.Substring(0, Ln - 3);
             }
            else
             if (InputValue.Substring(Ln -  1, 1) == ""0"" && InputValue.Substring(Ln -  2, 2) != ""00"")
            {
              InputValue = InputValue.Substring(0, Ln - 1);
             }
                    }
                 }
	 return InputValue.Replace(""."", "","");
     
}


public string ReplaceDotToComma7(string InputValue)
{
                int Ln = InputValue.Length; 
                if (InputValue.IndexOf(""."") != -1)
                 {                   
                   if ((Ln - 2) > 0)
                    {
           if (InputValue.Substring(Ln -  2, 2) == ""00"")
            {
              InputValue = InputValue.Substring(0, Ln - 3);
             }
            else
             if (InputValue.Substring(Ln -  1, 1) == ""0"" && InputValue.Substring(Ln -  2, 2) != ""00"")
            {
              InputValue = InputValue.Substring(0, Ln - 1);
             }
                    }
                 }
	 return InputValue.Replace(""."", "","");
     
}


public string ReplaceDotToComma8(string InputValue)
{
                int Ln = InputValue.Length; 
                if (InputValue.IndexOf(""."") != -1)
                 {                   
                   if ((Ln - 2) > 0)
                    {
           if (InputValue.Substring(Ln -  2, 2) == ""00"")
            {
              InputValue = InputValue.Substring(0, Ln - 3);
             }
            else
             if (InputValue.Substring(Ln -  1, 1) == ""0"" && InputValue.Substring(Ln -  2, 2) != ""00"")
            {
              InputValue = InputValue.Substring(0, Ln - 1);
             }
                    }
                 }
	 return InputValue.Replace(""."", "","");
     
}


public string ReplaceDotToComma9(string InputValue)
{
                int Ln = InputValue.Length; 
                if (InputValue.IndexOf(""."") != -1)
                 {                   
                   if ((Ln - 2) > 0)
                    {
           if (InputValue.Substring(Ln -  2, 2) == ""00"")
            {
              InputValue = InputValue.Substring(0, Ln - 3);
             }
            else
             if (InputValue.Substring(Ln -  1, 1) == ""0"" && InputValue.Substring(Ln -  2, 2) != ""00"")
            {
              InputValue = InputValue.Substring(0, Ln - 1);
             }
                    }
                 }
	 return InputValue.Replace(""."", "","");
     
}


public string ReplaceDotToComma10(string InputValue)
{
                int Ln = InputValue.Length; 
                if (InputValue.IndexOf(""."") != -1)
                 {                   
                   if ((Ln - 2) > 0)
                    {
           if (InputValue.Substring(Ln -  2, 2) == ""00"")
            {
              InputValue = InputValue.Substring(0, Ln - 3);
             }
            else
             if (InputValue.Substring(Ln -  1, 1) == ""0"" && InputValue.Substring(Ln -  2, 2) != ""00"")
            {
              InputValue = InputValue.Substring(0, Ln - 1);
             }
                    }
                 }
	 return InputValue.Replace(""."", "","");
     
}


public string ReturnGUID()
{
 Guid g = Guid.NewGuid();
 String GUID = g.ToString();
 return GUID.ToUpper();
}


public bool IsNumeric(string val)
{
	if (val == null)
	{
		return false;
	}
	double d = 0;
	return Double.TryParse(val, System.Globalization.NumberStyles.AllowThousands | System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out d);
}

public bool IsNumeric(string val, ref double d)
{
	if (val == null)
	{
		return false;
	}
	return Double.TryParse(val, System.Globalization.NumberStyles.AllowThousands | System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out d);
}

public bool ValToBool(string val)
{
	if (val != null)
	{
		if (string.Compare(val, bool.TrueString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return true;
		}
		if (string.Compare(val, bool.FalseString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return false;
		}
		val = val.Trim();
		if (string.Compare(val, bool.TrueString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return true;
		}
		if (string.Compare(val, bool.FalseString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return false;
		}
		double d = 0;
		if (IsNumeric(val, ref d))
		{
			return (d > 0);
		}
	}
	return false;
}


]]></msxsl:script>
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"BYGE.Integration.Core.Invoice.Invoice";
        
        private const global::BYGE.Integration.Core.Invoice.Invoice _srcSchemaTypeReference0 = null;
        
        private const string _strTrgSchemasList0 = @"BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.XLBYGCompressedInvoice";
        
        private const global::BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.XLBYGCompressedInvoice _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [1];
                _SrcSchemas[0] = @"BYGE.Integration.Core.Invoice.Invoice";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.XLBYGCompressedInvoice";
                return _TrgSchemas;
            }
        }
    }
}
