namespace BYGE.Integration.XLBYG.Compressed.Invoice.Maps {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.XLBYGCompressedInvoice", typeof(global::BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.XLBYGCompressedInvoice))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.Core.Invoice.Invoice", typeof(global::BYGE.Integration.Core.Invoice.Invoice))]
    public sealed class XLBYG_Compressed_Invoice_to_Core_Invoice : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var s0 userCSharp"" version=""1.0"" xmlns:ns0=""http://byg-e.dk/schemas/v10"" xmlns:s0=""http://BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.InvoiceTest01"" xmlns:userCSharp=""http://schemas.microsoft.com/BizTalk/2003/userCSharp"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/s0:XLBYGCompressedInvoice"" />
  </xsl:template>
  <xsl:template match=""/s0:XLBYGCompressedInvoice"">
    <xsl:variable name=""var:v1"" select=""userCSharp:StringSubstring(string(Header/IssueDate/text()) , &quot;1&quot; , &quot;4&quot;)"" />
    <xsl:variable name=""var:v2"" select=""string(Header/IssueDate/text())"" />
    <xsl:variable name=""var:v3"" select=""userCSharp:StringSubstring($var:v2 , &quot;5&quot; , &quot;6&quot;)"" />
    <xsl:variable name=""var:v4"" select=""userCSharp:StringSubstring($var:v2 , &quot;7&quot; , &quot;8&quot;)"" />
    <xsl:variable name=""var:v5"" select=""userCSharp:StringConcat(string($var:v1) , &quot;-&quot; , string($var:v3) , &quot;-&quot; , string($var:v4))"" />
    <ns0:Invoice>
      <ProfileID>
        <xsl:value-of select=""Header/TestInd/text()"" />
      </ProfileID>
      <InvoiceID>
        <xsl:value-of select=""Footer/InvoicID/text()"" />
      </InvoiceID>
      <UUID>
        <xsl:value-of select=""Footer/GUID/text()"" />
      </UUID>
      <TestIndicator>
        <xsl:text>0</xsl:text>
      </TestIndicator>
      <IssueDate>
        <xsl:value-of select=""$var:v5"" />
      </IssueDate>
      <InvoiceType>
        <xsl:value-of select=""Header/DocType/text()"" />
      </InvoiceType>
      <OrderReference>
        <OrderID>
          <xsl:value-of select=""Header/OrderID/text()"" />
        </OrderID>
        <SalesOrderID>
          <xsl:value-of select=""Footer/RFFSI/text()"" />
        </SalesOrderID>
      </OrderReference>
      <AccountingSupplier>
        <AccSellerID>
          <xsl:value-of select=""Footer/NADSU/text()"" />
        </AccSellerID>
      </AccountingSupplier>
      <BuyerCustomer>
        <BuyerID>
          <xsl:value-of select=""Footer/NADBY/text()"" />
        </BuyerID>
      </BuyerCustomer>
      <PaymentMeans>
        <xsl:variable name=""var:v6"" select=""userCSharp:AdjustDate(string(Header/PaymentDate1/text()))"" />
        <PaymentDueDate>
          <xsl:value-of select=""$var:v6"" />
        </PaymentDueDate>
      </PaymentMeans>
      <AllowanceCharge>
        <xsl:variable name=""var:v7"" select=""userCSharp:ReturnPersentage(string(Header/TAX/text()))"" />
        <MultiplierFactorNumeric>
          <xsl:value-of select=""$var:v7"" />
        </MultiplierFactorNumeric>
        <Amount>
          <xsl:value-of select=""Header/AlcAmount1/text()"" />
        </Amount>
        <BaseAmount>
          <xsl:value-of select=""Header/AlcAmount2/text()"" />
        </BaseAmount>
      </AllowanceCharge>
      <TaxTotal>
        <xsl:variable name=""var:v8"" select=""userCSharp:ReturnTaxAmount(string(Footer/TaxTotal_TaxAmount/text()))"" />
        <TaxAmount>
          <xsl:value-of select=""$var:v8"" />
        </TaxAmount>
        <TaxSubtotal>
          <xsl:variable name=""var:v9"" select=""userCSharp:ReturnLineExtensionAmount(string(Footer/TaxSubtotal_TaxableAmount/text()))"" />
          <TaxableAmount>
            <xsl:value-of select=""$var:v9"" />
          </TaxableAmount>
        </TaxSubtotal>
      </TaxTotal>
      <Total>
        <xsl:variable name=""var:v10"" select=""userCSharp:ReturnAmount(string(Footer/LegalMonetaryTotal_PayableAmount/text()))"" />
        <LineTotalAmount>
          <xsl:value-of select=""$var:v10"" />
        </LineTotalAmount>
        <xsl:variable name=""var:v11"" select=""userCSharp:ReturnLineAmount(string(Footer/UBLExtensionsContent_ExtensionAmount/text()))"" />
        <PayableAmount>
          <xsl:value-of select=""$var:v11"" />
        </PayableAmount>
      </Total>
      <xsl:for-each select=""Body"">
        <Lines>
          <Line>
            <LineAmountTotal>
              <xsl:value-of select=""PriceAmount203/text()"" />
            </LineAmountTotal>
          </Line>
        </Lines>
      </xsl:for-each>
    </ns0:Invoice>
  </xsl:template>
  <msxsl:script language=""C#"" implements-prefix=""userCSharp""><![CDATA[
public string AdjustDate(string PaymentDate)
{
string AdjustedDate;

if (PaymentDate != """")
{
AdjustedDate = PaymentDate.Substring(2, 6);
}
else
{
AdjustedDate=""000000"";
}

return AdjustedDate;
}


public string ReturnPersentage(string Amount)
{

int AmountLen;
int CommaPosition;
string ipart;
string izeropart;
string dpart;
string dzeropart;
string OutputAmount;

if (Amount != """")
{
izeropart = """";
dzeropart = """";

AmountLen = Amount.Length;
CommaPosition = Amount.LastIndexOf(""."") ; 

if (CommaPosition != -1) 
{
ipart = Amount.Substring(0, CommaPosition);
dpart = Amount.Substring(CommaPosition+1, AmountLen-1-CommaPosition);

switch(dpart.Length)
{
case 2: break;
case 1: dzeropart = ""0""; break;
case 0: dzeropart = ""00""; break;
}
dpart = dpart + dzeropart;

switch(ipart.Length)
{
case 3: break;
case 2: izeropart = ""0""; break; 
case 1: izeropart = ""00""; break; 
}
ipart = izeropart + ipart;

OutputAmount = ipart+dpart;
}

else
{
switch(Amount.Length)
{
case 3: break;
case 2: izeropart = ""0""; break;
case 1: izeropart = ""00""; break;
}
OutputAmount = izeropart + Amount +""00"";
}
}
else
{
OutputAmount  = ""00000"";
}

return OutputAmount;
}

public string ReturnLineExtensionAmount(string Amount)
{

int AmountLen;
int CommaPosition;
string ipart;
string izeropart;
string dpart;
string dzeropart;
string OutputAmount;

izeropart = """";
dzeropart = """";

AmountLen = Amount.Length;
CommaPosition = Amount.LastIndexOf("","") ; 

if (CommaPosition != -1) 
{
ipart = Amount.Substring(0, CommaPosition);
dpart = Amount.Substring(CommaPosition+1, AmountLen-1-CommaPosition);

switch(dpart.Length)
{
case 2: break;
case 1: dzeropart = ""0""; break;
case 0: dzeropart = ""00""; break;
}
dpart = dpart + dzeropart;

switch(ipart.Length)
{
case 9: break;
case 8: izeropart = ""0""; break; 
case 7: izeropart = ""00""; break; 
case 6: izeropart = ""000""; break; 
case 5: izeropart = ""0000""; break; 
case 4: izeropart = ""00000""; break; 
case 3: izeropart = ""000000""; break; 
case 2: izeropart = ""0000000""; break; 
case 1: izeropart = ""00000000""; break; 
}
ipart = izeropart + ipart;

OutputAmount = ipart+dpart;
}

else
{
switch(Amount.Length)
{
case 9: break;
case 8: izeropart = ""0""; break;
case 7: izeropart = ""00""; break;
case 6: izeropart = ""000""; break;
case 5: izeropart = ""0000""; break;
case 4: izeropart = ""00000""; break;
case 3: izeropart = ""000000""; break;
case 2: izeropart = ""0000000""; break;
case 1: izeropart = ""00000000""; break;
}
OutputAmount = izeropart + Amount+""00"";
}

return OutputAmount;
}

public string ReturnTaxAmount(string Amount)
{

int AmountLen;
int CommaPosition;
string ipart;
string izeropart;
string dpart;
string dzeropart;
string OutputAmount;

izeropart = """";
dzeropart = """";

AmountLen = Amount.Length;
CommaPosition = Amount.LastIndexOf("","") ; 

if (CommaPosition != -1) 
{
ipart = Amount.Substring(0, CommaPosition);
dpart = Amount.Substring(CommaPosition+1, AmountLen-1-CommaPosition);

switch(dpart.Length)
{
case 2: break;
case 1: dzeropart = ""0""; break;
case 0: dzeropart = ""00""; break;
}
dpart = dpart + dzeropart;

switch(ipart.Length)
{
case 9: break;
case 8: izeropart = ""0""; break; 
case 7: izeropart = ""00""; break; 
case 6: izeropart = ""000""; break; 
case 5: izeropart = ""0000""; break; 
case 4: izeropart = ""00000""; break; 
case 3: izeropart = ""000000""; break; 
case 2: izeropart = ""0000000""; break; 
case 1: izeropart = ""00000000""; break; 
}
ipart = izeropart + ipart;

OutputAmount = ipart+dpart;
}

else
{
switch(Amount.Length)
{
case 9: break;
case 8: izeropart = ""0""; break;
case 7: izeropart = ""00""; break;
case 6: izeropart = ""000""; break;
case 5: izeropart = ""0000""; break;
case 4: izeropart = ""00000""; break;
case 3: izeropart = ""000000""; break;
case 2: izeropart = ""0000000""; break;
case 1: izeropart = ""00000000""; break;
}
OutputAmount = izeropart + Amount+""00"";
}

return OutputAmount;
}

public string ReturnAmount(string Amount)
{

int AmountLen;
int CommaPosition;
string ipart;
string izeropart;
string dpart;
string dzeropart;
string OutputAmount;

izeropart = """";
dzeropart = """";

AmountLen = Amount.Length;
CommaPosition = Amount.LastIndexOf("","") ; 

if (CommaPosition != -1) 
{
ipart = Amount.Substring(0, CommaPosition);
dpart = Amount.Substring(CommaPosition+1, AmountLen-1-CommaPosition);

switch(dpart.Length)
{
case 2: break;
case 1: dzeropart = ""0""; break;
case 0: dzeropart = ""00""; break;
}
dpart = dpart + dzeropart;

switch(ipart.Length)
{
case 9: break;
case 8: izeropart = ""0""; break; 
case 7: izeropart = ""00""; break; 
case 6: izeropart = ""000""; break; 
case 5: izeropart = ""0000""; break; 
case 4: izeropart = ""00000""; break; 
case 3: izeropart = ""000000""; break; 
case 2: izeropart = ""0000000""; break; 
case 1: izeropart = ""00000000""; break; 
}
ipart = izeropart + ipart;

OutputAmount = ipart+dpart;
}

else
{
switch(Amount.Length)
{
case 9: break;
case 8: izeropart = ""0""; break;
case 7: izeropart = ""00""; break;
case 6: izeropart = ""000""; break;
case 5: izeropart = ""0000""; break;
case 4: izeropart = ""00000""; break;
case 3: izeropart = ""000000""; break;
case 2: izeropart = ""0000000""; break;
case 1: izeropart = ""00000000""; break;
}
OutputAmount = izeropart + Amount+""00"";
}

return OutputAmount;
}

public string ReturnLineAmount(string Amount)
{

int AmountLen;
int CommaPosition;
string ipart;
string izeropart;
string dpart;
string dzeropart;
string OutputAmount;

if (Amount != """")
{
izeropart = """";
dzeropart = """";

AmountLen = Amount.Length;
CommaPosition = Amount.LastIndexOf("","") ; 

if (CommaPosition != -1) 
{
ipart = Amount.Substring(0, CommaPosition);
dpart = Amount.Substring(CommaPosition+1, AmountLen-1-CommaPosition);

switch(dpart.Length)
{
case 2: break;
case 1: dzeropart = ""0""; break;
case 0: dzeropart = ""00""; break;
}
dpart = dpart + dzeropart;

switch(ipart.Length)
{
case 9: break;
case 8: izeropart = ""0""; break; 
case 7: izeropart = ""00""; break; 
case 6: izeropart = ""000""; break; 
case 5: izeropart = ""0000""; break; 
case 4: izeropart = ""00000""; break; 
case 3: izeropart = ""000000""; break; 
case 2: izeropart = ""0000000""; break; 
case 1: izeropart = ""00000000""; break; 
}
ipart = izeropart + ipart;

OutputAmount = ipart+dpart;
}

else
{
switch(Amount.Length)
{
case 9: break;
case 8: izeropart = ""0""; break;
case 7: izeropart = ""00""; break;
case 6: izeropart = ""000""; break;
case 5: izeropart = ""0000""; break;
case 4: izeropart = ""00000""; break;
case 3: izeropart = ""000000""; break;
case 2: izeropart = ""0000000""; break;
case 1: izeropart = ""00000000""; break;
}
OutputAmount = izeropart + Amount+""00"";
}
}
else
{
OutputAmount  = ""00000000000"";
}

return OutputAmount;
}

public string StringSubstring(string str, string left, string right)
{
	string retval = """";
	double dleft = 0;
	double dright = 0;
	if (str != null && IsNumeric(left, ref dleft) && IsNumeric(right, ref dright))
	{
		int lt = (int)dleft;
		int rt = (int)dright;
		lt--; rt--;
		if (lt >= 0 && rt >= lt && lt < str.Length)
		{
			if (rt < str.Length)
			{
				retval = str.Substring(lt, rt-lt+1);
			}
			else
			{
				retval = str.Substring(lt, str.Length-lt);
			}
		}
	}
	return retval;
}


public string StringConcat(string param0, string param1, string param2, string param3, string param4)
{
   return param0 + param1 + param2 + param3 + param4;
}


public bool IsNumeric(string val)
{
	if (val == null)
	{
		return false;
	}
	double d = 0;
	return Double.TryParse(val, System.Globalization.NumberStyles.AllowThousands | System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out d);
}

public bool IsNumeric(string val, ref double d)
{
	if (val == null)
	{
		return false;
	}
	return Double.TryParse(val, System.Globalization.NumberStyles.AllowThousands | System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out d);
}


]]></msxsl:script>
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.XLBYGCompressedInvoice";
        
        private const global::BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.XLBYGCompressedInvoice _srcSchemaTypeReference0 = null;
        
        private const string _strTrgSchemasList0 = @"BYGE.Integration.Core.Invoice.Invoice";
        
        private const global::BYGE.Integration.Core.Invoice.Invoice _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [1];
                _SrcSchemas[0] = @"BYGE.Integration.XLBYG.Compressed.Invoice.Schemas.XLBYGCompressedInvoice";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"BYGE.Integration.Core.Invoice.Invoice";
                return _TrgSchemas;
            }
        }
    }
}
