namespace BYGE.Integration.UBLInvoice {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"OrderReference", @"AdditionalDocumentReference", @"AccountingSupplierParty", @"AccountingCustomerParty", @"BuyerCustomerParty", @"Delivery", @"DeliveryTerms", @"PaymentMeans", @"PaymentTerms", @"PrepaidPayment", @"AllowanceCharge", @"TaxTotal", @"LegalMonetaryTotal", @"InvoiceLine", @"DespatchDocumentReference"})]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.UBLInvoice.UBLInvoice1", typeof(global::BYGE.Integration.UBLInvoice.UBLInvoice1))]
    public sealed class UBLInvoice2 : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xs:schema xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" xmlns:tns=""urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"" attributeFormDefault=""unqualified"" elementFormDefault=""qualified"" targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"" xmlns:xs=""http://www.w3.org/2001/XMLSchema"">
  <xs:import schemaLocation=""BYGE.Integration.UBLInvoice.UBLInvoice1"" namespace=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" />
  <xs:annotation>
    <xs:appinfo>
      <b:references>
        <b:reference targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" />
      </b:references>
    </xs:appinfo>
  </xs:annotation>
  <xs:element name=""OrderReference"">
    <xs:complexType>
      <xs:sequence>
        <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:ID"" />
        <xs:element xmlns:q2=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q2:SalesOrderID"" />
        <xs:element xmlns:q3=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q3:IssueDate"" />
        <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:CustomerReference"" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name=""AdditionalDocumentReference"">
    <xs:complexType>
      <xs:sequence>
        <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:ID"" />
        <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:DocumentTypeCode"" />
        <xs:element name=""Attachment"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:EmbeddedDocumentBinaryObject"" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name=""ExternalReference"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:URI"" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name=""AccountingSupplierParty"">
    <xs:complexType>
      <xs:sequence>
        <xs:element name=""Party"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q4=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q4:EndpointID"" />
              <xs:element name=""PartyIdentification"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q5=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q5:ID"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""PartyName"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q6=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q6:Name"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""PostalAddress"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q7=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q7:ID"" />
                    <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:AddressFormatCode"" />
                    <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:Postbox"" />
                    <xs:element xmlns:q2=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q2:StreetName"" />
                    <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:BuildingNumber"" />
                    <xs:element xmlns:q9=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q9:AdditionalStreetName"" />
                    <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:Department"" />
                    <xs:element name=""CitySubdivisionName"" type=""xs:string"" />
                    <xs:element xmlns:q10=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q10:CityName"" />
                    <xs:element xmlns:q11=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q11:PostalZone"" />
                    <xs:element name=""Country"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element xmlns:q12=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q12:IdentificationCode"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""PartyLegalEntity"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q13=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q13:CompanyID"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""PartyTaxScheme"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:CompanyID"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""Contact"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q14=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q14:ID"" />
                    <xs:element xmlns:q15=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q15:Name"" />
                    <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:Telephone"" />
                    <xs:element xmlns:q2=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q2:Telefax"" />
                    <xs:element xmlns:q3=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q3:ElectronicMail"" />
                    <xs:element xmlns:q4=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q4:Note"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name=""AccountingCustomerParty"">
    <xs:complexType>
      <xs:sequence>
        <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:SupplierAssignedAccountID"" />
        <xs:element name=""Party"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q16=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q16:EndpointID"" />
              <xs:element name=""PartyIdentification"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q17=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q17:ID"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""PartyName"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q18=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q18:Name"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""PostalAddress"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q19=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q19:ID"" />
                    <xs:element xmlns:q4=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q4:AddressFormatCode"" />
                    <xs:element xmlns:q5=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q5:Postbox"" />
                    <xs:element xmlns:q6=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q6:StreetName"" />
                    <xs:element xmlns:q7=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q7:BuildingNumber"" />
                    <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:AdditionalStreetName"" />
                    <xs:element xmlns:q8=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q8:Department"" />
                    <xs:element xmlns:q21=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q21:CityName"" />
                    <xs:element xmlns:q22=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q22:PostalZone"" />
                    <xs:element name=""Country"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element xmlns:q23=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q23:IdentificationCode"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""PartyLegalEntity"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:CompanyID"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""PartyTaxScheme"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:CompanyID"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""Contact"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q9=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q9:ID"" />
                    <xs:element xmlns:q10=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q10:Name"" />
                    <xs:element xmlns:q11=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q11:Telephone"" />
                    <xs:element xmlns:q12=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q12:Telefax"" />
                    <xs:element xmlns:q13=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q13:ElectronicMail"" />
                    <xs:element xmlns:q14=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q14:Note"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name=""BuyerCustomerParty"">
    <xs:complexType>
      <xs:sequence>
        <xs:element name=""Party"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q24=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q24:EndpointID"" />
              <xs:element name=""PartyIdentification"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q25=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q25:ID"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""PartyName"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q26=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q26:Name"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""PostalAddress"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q27=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q27:ID"" />
                    <xs:element xmlns:q15=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q15:AddressFormatCode"" />
                    <xs:element xmlns:q16=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q16:Postbox"" />
                    <xs:element xmlns:q28=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q28:StreetName"" />
                    <xs:element xmlns:q17=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q17:BuildingNumber"" />
                    <xs:element xmlns:q2=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q2:AdditionalStreetName"" />
                    <xs:element xmlns:q18=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q18:Department"" />
                    <xs:element xmlns:q29=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q29:CityName"" />
                    <xs:element xmlns:q30=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q30:PostalZone"" />
                    <xs:element name=""Country"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element xmlns:q31=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q31:IdentificationCode"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""PartyLegalEntity"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q2=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q2:CompanyID"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""PartyTaxScheme"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q2=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q2:CompanyID"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""Contact"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q19=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q19:ID"" />
                    <xs:element xmlns:q20=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q20:Name"" />
                    <xs:element xmlns:q21=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q21:Telephone"" />
                    <xs:element xmlns:q22=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q22:Telefax"" />
                    <xs:element xmlns:q23=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q23:ElectronicMail"" />
                    <xs:element xmlns:q24=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q24:Note"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name=""Delivery"">
    <xs:complexType>
      <xs:sequence>
        <xs:element xmlns:q46=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q46:ActualDeliveryDate"" />
        <xs:element name=""DeliveryLocation"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q47=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q47:Description"" />
              <xs:element name=""Address"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q25=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q25:AddressFormatCode"" />
                    <xs:element xmlns:q26=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q26:StreetName"" />
                    <xs:element xmlns:q27=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q27:BuildingNumber"" />
                    <xs:element xmlns:q49=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q49:MarkAttention"" />
                    <xs:element xmlns:q3=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q3:AdditionalStreetName"" />
                    <xs:element xmlns:q28=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q28:Department"" />
                    <xs:element xmlns:q50=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q50:CityName"" />
                    <xs:element xmlns:q51=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q51:PostalZone"" />
                    <xs:element name=""Country"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element xmlns:q52=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q52:IdentificationCode"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name=""DeliveryParty"">
          <xs:complexType>
            <xs:sequence>
              <xs:element name=""PartyName"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:Name"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""EndpointID"">
                <xs:complexType>
                  <xs:attribute name=""schemeAgencyID"" type=""xs:string"" />
                  <xs:attribute name=""schemeID"" type=""xs:string"" />
                </xs:complexType>
              </xs:element>
              <xs:element name=""PartyIdentification"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element name=""ID"">
                      <xs:complexType>
                        <xs:attribute name=""schemeAgencyID"" type=""xs:string"" />
                        <xs:attribute name=""schemeID"" type=""xs:string"" />
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""PostalAddress"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element name=""AddressFormatCode"">
                      <xs:complexType>
                        <xs:attribute name=""listAgencyID"" type=""xs:string"" />
                        <xs:attribute name=""listID"" type=""xs:string"" />
                      </xs:complexType>
                    </xs:element>
                    <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:StreetName"" />
                    <xs:element xmlns:q2=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q2:AdditionalStreetName"" />
                    <xs:element xmlns:q3=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q3:BuildingNumber"" />
                    <xs:element xmlns:q4=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q4:CityName"" />
                    <xs:element xmlns:q5=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q5:PostalZone"" />
                    <xs:element name=""Country"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element xmlns:q6=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q6:IdentificationCode"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name=""DeliveryTerms"">
    <xs:complexType>
      <xs:sequence>
        <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:SpecialTerms"" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name=""PaymentMeans"">
    <xs:complexType>
      <xs:sequence>
        <xs:element xmlns:q29=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q29:ID"" />
        <xs:element xmlns:q30=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q30:PaymentMeansCode"" />
        <xs:element xmlns:q54=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q54:PaymentDueDate"" />
        <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:InstructionID"" />
        <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:InstructionNote"" />
        <xs:element xmlns:q2=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q2:PaymentID"" />
        <xs:element name=""CreditAccount"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:AccountID"" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element xmlns:q2=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q2:PaymentChannelCode"" />
        <xs:element name=""PayeeFinancialAccount"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:ID"" />
              <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:Name"" />
              <xs:element name=""FinancialInstitutionBranch"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q2=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q2:ID"" />
                    <xs:element xmlns:q3=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q3:Name"" />
                    <xs:element name=""FinancialInstitution"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:ID"" />
                          <xs:element xmlns:q2=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q2:Name"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name=""PaymentTerms"">
    <xs:complexType>
      <xs:sequence>
        <xs:element xmlns:q56=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q56:ID"" />
        <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:PaymentMeansID"" />
        <xs:element xmlns:q2=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q2:PrepaidPaymentReferenceID"" />
        <xs:element xmlns:q3=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q3:Note"" />
        <xs:element xmlns:q4=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q4:ReferenceEventCode"" />
        <xs:element xmlns:q5=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q5:SettlementDiscountPercent"" />
        <xs:element xmlns:q58=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q58:PenaltySurchargePercent"" />
        <xs:element xmlns:q59=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q59:Amount"" />
        <xs:element name=""SettlementPeriod"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q6=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q6:StartDate"" />
              <xs:element xmlns:q7=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q7:EndDate"" />
              <xs:element xmlns:q8=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q8:Description"" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name=""PenaltyPeriod"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q60=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q60:StartDate"" />
              <xs:element xmlns:q9=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q9:EndDate"" />
              <xs:element xmlns:q10=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q10:Description"" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name=""PrepaidPayment"">
    <xs:complexType>
      <xs:sequence>
        <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:ID"" />
        <xs:element xmlns:q2=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q2:PaidAmount"" />
        <xs:element xmlns:q3=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q3:ReceivedDate"" />
        <xs:element xmlns:q4=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q4:PaidDate"" />
        <xs:element xmlns:q5=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q5:PaidTime"" />
        <xs:element xmlns:q6=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q6:InstructionID"" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name=""AllowanceCharge"">
    <xs:complexType>
      <xs:sequence>
        <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:ID"" />
        <xs:element xmlns:q2=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q2:ChargeIndicator"" />
        <xs:element xmlns:q3=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q3:AllowanceChargeReasonCode"" />
        <xs:element xmlns:q4=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q4:AllowanceChargeReason"" />
        <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:MultiplierFactorNumeric"" />
        <xs:element xmlns:q6=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q6:PrepaidIndicator"" />
        <xs:element xmlns:q7=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q7:SequenceNumeric"" />
        <xs:element xmlns:q8=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q8:Amount"" />
        <xs:element xmlns:q9=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q9:BaseAmount"" />
        <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:AccountingCost"" />
        <xs:element name=""TaxCategory"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:ID"" />
              <xs:element xmlns:q2=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q2:Percent"" />
              <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:PerUnitAmount"" />
              <xs:element name=""TaxScheme"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q2=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q2:ID"" />
                    <xs:element xmlns:q3=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q3:Name"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name=""TaxTotal"">
    <xs:complexType>
      <xs:sequence>
        <xs:element xmlns:q62=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q62:TaxAmount"" />
        <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:TaxEvidenceIndicator"" />
        <xs:element name=""TaxSubtotal"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q63=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q63:TaxableAmount"" />
              <xs:element xmlns:q64=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q64:TaxAmount"" />
              <xs:element xmlns:q65=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q65:Percent"" />
              <xs:element name=""TaxCategory"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q66=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q66:ID"" />
                    <xs:element xmlns:q67=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q67:Percent"" />
                    <xs:element name=""TaxScheme"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element xmlns:q4=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q4:ID"" />
                          <xs:element xmlns:q5=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q5:Name"" />
                          <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:TaxTypeCode"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name=""LegalMonetaryTotal"">
    <xs:complexType>
      <xs:sequence>
        <xs:element xmlns:q69=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q69:LineExtensionAmount"" />
        <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:TaxExclusiveAmount"" />
        <xs:element xmlns:q2=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q2:TaxInclusiveAmount"" />
        <xs:element xmlns:q3=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q3:AllowanceTotalAmount"" />
        <xs:element xmlns:q4=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q4:ChargeTotalAmount"" />
        <xs:element xmlns:q5=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q5:PrepaidAmount"" />
        <xs:element xmlns:q6=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q6:PayableRoundingAmount"" />
        <xs:element xmlns:q7=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q7:PayableAmount"" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name=""InvoiceLine"">
    <xs:complexType>
      <xs:sequence>
        <xs:element xmlns:q71=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q71:ID"" />
        <xs:element xmlns:q72=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q72:Note"" />
        <xs:element xmlns:q73=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q73:InvoicedQuantity"" />
        <xs:element xmlns:q74=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q74:LineExtensionAmount"" />
        <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:FreeOfChargeIndicator"" />
        <xs:element name=""OrderLineReference"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q75=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q75:LineID"" />
              <xs:element name=""OrderReference"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q2=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q2:ID"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element xmlns:q76=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q76:SalesOrderLineID"" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name=""PricingReference"">
          <xs:complexType>
            <xs:sequence>
              <xs:element name=""AlternativeConditionPrice"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:PriceAmount"" />
                    <xs:element xmlns:q2=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q2:BaseQuantity"" />
                    <xs:element xmlns:q3=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q3:PriceTypeCode"" />
                    <xs:element xmlns:q4=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q4:PriceType"" />
                    <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:OrderableUnitFactorRate"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name=""Delivery"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:ID"" />
              <xs:element xmlns:q2=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q2:Quantity"" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""0"" maxOccurs=""unbounded"" name=""AllowanceCharge"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q3=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q3:ID"" />
              <xs:element xmlns:q4=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q4:ChargeIndicator"" />
              <xs:element xmlns:q5=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q5:AllowanceChargeReasonCode"" />
              <xs:element xmlns:q6=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q6:AllowanceChargeReason"" />
              <xs:element xmlns:q7=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q7:MultiplierFactorNumeric"" />
              <xs:element xmlns:q8=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q8:PrepaidIndicator"" />
              <xs:element xmlns:q9=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q9:SequenceNumeric"" />
              <xs:element xmlns:q10=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q10:Amount"" />
              <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:AccountingCost"" />
              <xs:element xmlns:q11=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q11:BaseAmount"" />
              <xs:element name=""TaxCategory"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q12=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q12:ID"" />
                    <xs:element xmlns:q13=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q13:Percent"" />
                    <xs:element xmlns:q2=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q2:PerUnitAmount"" />
                    <xs:element name=""TaxScheme"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element xmlns:q14=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q14:ID"" />
                          <xs:element xmlns:q15=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q15:Name"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""0"" maxOccurs=""10"" name=""TaxTotal"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q16=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q16:TaxAmount"" />
              <xs:element xmlns:q2=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q2:TaxEvidenceIndicator"" />
              <xs:element name=""TaxSubtotal"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q17=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q17:TaxableAmount"" />
                    <xs:element xmlns:q18=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q18:TaxAmount"" />
                    <xs:element xmlns:q19=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q19:Percent"" />
                    <xs:element name=""TaxCategory"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element xmlns:q20=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q20:ID"" />
                          <xs:element xmlns:q21=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q21:Percent"" />
                          <xs:element name=""TaxScheme"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element xmlns:q22=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q22:ID"" />
                                <xs:element xmlns:q23=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q23:Name"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name=""Item"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q77=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q77:Description"" />
              <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:PackQuantity"" />
              <xs:element xmlns:q24=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q24:Name"" />
              <xs:element name=""BuyersItemIdentification"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:ID"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""SellersItemIdentification"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q78=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q78:ID"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""StandardItemIdentification"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q25=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q25:ID"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""AdditionalItemIdentification"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q26=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q26:ID"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name=""CatalogueItemIdentification"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element xmlns:q27=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q27:ID"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name=""Price"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q79=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q79:PriceAmount"" />
              <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:BaseQuantity"" />
              <xs:element xmlns:q5=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q5:PriceTypeCode"" />
              <xs:element xmlns:q6=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q6:PriceType"" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""0"" maxOccurs=""unbounded"" name=""DocumentReference"">
          <xs:complexType>
            <xs:sequence>
              <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" maxOccurs=""1"" ref=""q1:ID"" />
              <xs:element xmlns:q2=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" minOccurs=""0"" maxOccurs=""1"" ref=""q2:DocumentType"" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name=""DespatchDocumentReference"">
    <xs:complexType>
      <xs:sequence>
        <xs:element xmlns:q1=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ref=""q1:ID"" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>
</xs:schema>";
        
        public UBLInvoice2() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [15];
                _RootElements[0] = "OrderReference";
                _RootElements[1] = "AdditionalDocumentReference";
                _RootElements[2] = "AccountingSupplierParty";
                _RootElements[3] = "AccountingCustomerParty";
                _RootElements[4] = "BuyerCustomerParty";
                _RootElements[5] = "Delivery";
                _RootElements[6] = "DeliveryTerms";
                _RootElements[7] = "PaymentMeans";
                _RootElements[8] = "PaymentTerms";
                _RootElements[9] = "PrepaidPayment";
                _RootElements[10] = "AllowanceCharge";
                _RootElements[11] = "TaxTotal";
                _RootElements[12] = "LegalMonetaryTotal";
                _RootElements[13] = "InvoiceLine";
                _RootElements[14] = "DespatchDocumentReference";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2",@"OrderReference")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"OrderReference"})]
        public sealed class OrderReference : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public OrderReference() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "OrderReference";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2",@"AdditionalDocumentReference")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"AdditionalDocumentReference"})]
        public sealed class AdditionalDocumentReference : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public AdditionalDocumentReference() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "AdditionalDocumentReference";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2",@"AccountingSupplierParty")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"AccountingSupplierParty"})]
        public sealed class AccountingSupplierParty : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public AccountingSupplierParty() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "AccountingSupplierParty";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2",@"AccountingCustomerParty")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"AccountingCustomerParty"})]
        public sealed class AccountingCustomerParty : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public AccountingCustomerParty() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "AccountingCustomerParty";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2",@"BuyerCustomerParty")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"BuyerCustomerParty"})]
        public sealed class BuyerCustomerParty : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public BuyerCustomerParty() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "BuyerCustomerParty";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2",@"Delivery")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"Delivery"})]
        public sealed class Delivery : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public Delivery() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "Delivery";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2",@"DeliveryTerms")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"DeliveryTerms"})]
        public sealed class DeliveryTerms : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public DeliveryTerms() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "DeliveryTerms";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2",@"PaymentMeans")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"PaymentMeans"})]
        public sealed class PaymentMeans : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public PaymentMeans() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "PaymentMeans";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2",@"PaymentTerms")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"PaymentTerms"})]
        public sealed class PaymentTerms : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public PaymentTerms() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "PaymentTerms";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2",@"PrepaidPayment")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"PrepaidPayment"})]
        public sealed class PrepaidPayment : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public PrepaidPayment() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "PrepaidPayment";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2",@"AllowanceCharge")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"AllowanceCharge"})]
        public sealed class AllowanceCharge : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public AllowanceCharge() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "AllowanceCharge";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2",@"TaxTotal")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"TaxTotal"})]
        public sealed class TaxTotal : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public TaxTotal() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "TaxTotal";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2",@"LegalMonetaryTotal")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"LegalMonetaryTotal"})]
        public sealed class LegalMonetaryTotal : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public LegalMonetaryTotal() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "LegalMonetaryTotal";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2",@"InvoiceLine")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"InvoiceLine"})]
        public sealed class InvoiceLine : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public InvoiceLine() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "InvoiceLine";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2",@"DespatchDocumentReference")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"DespatchDocumentReference"})]
        public sealed class DespatchDocumentReference : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public DespatchDocumentReference() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "DespatchDocumentReference";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
    }
}
