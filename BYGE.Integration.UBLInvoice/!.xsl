<?xml version="1.0" encoding="UTF-16"?>
<!-- ------------------------------------------------------------------------------------ -->
<!-- SJ: This one is not used yet, so it must be auto-restored when it becomes current -->
<!-- ------------------------------------------------------------------------------------ -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:var="http://schemas.microsoft.com/BizTalk/2003/var" exclude-result-prefixes="msxsl var s2 s1 s0 userCSharp" version="1.0" xmlns:s2="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2" xmlns:ns0="http://byg-e.dk/schemas/v10" xmlns:s1="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:s0="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2" xmlns:userCSharp="http://schemas.microsoft.com/BizTalk/2003/userCSharp">
  <xsl:output omit-xml-declaration="yes" method="xml" version="1.0" />
  <xsl:template match="/">
    <xsl:apply-templates select="/s2:Invoice" />
  </xsl:template>
  <xsl:template match="/s2:Invoice">
    <ns0:Invoice>
      <InvoiceID>
        <xsl:value-of select="s0:ID/text()" />
      </InvoiceID>
      <UUID>
        <xsl:value-of select="userCSharp:ReturnGUID()" />
      </UUID>
      <IssueDate>
        <xsl:value-of select="s0:IssueDate/text()" />
      </IssueDate>
      <InvoiceType>
        <xsl:value-of select="s0:InvoiceTypeCode/text()" />
      </InvoiceType>
      <Currency>
        <xsl:value-of select="s0:DocumentCurrencyCode/text()" />
      </Currency>
      <OrderReference>
        <OrderID>
          <xsl:value-of select="s1:OrderReference/s0:ID/text()" />
        </OrderID>
        <SalesOrderID>
          <xsl:value-of select="s1:OrderReference/s0:SalesOrderID/text()" />
        </SalesOrderID>
        <OrderDate>
          <xsl:value-of select="s1:OrderReference/s0:IssueDate/text()" />
        </OrderDate>
      </OrderReference>
      <AccountingSupplier>
        <AccSellerID>
          <xsl:value-of select="s1:AccountingSupplierParty/s1:Party/s0:EndpointID/text()" />
        </AccSellerID>
        <PartyID>
          <xsl:value-of select="s1:AccountingSupplierParty/s1:Party/s1:PartyIdentification/s0:ID/text()" />
        </PartyID>
        <CompanyID>
          <xsl:value-of select="s1:AccountingSupplierParty/s1:Party/s1:PartyTaxScheme/s0:CompanyID/text()" />
        </CompanyID>
        <Address>
          <Name>
            <xsl:value-of select="s1:AccountingSupplierParty/s1:Party/s1:PartyName/s0:Name/text()" />
          </Name>
          <Street>
            <xsl:value-of select="s1:AccountingSupplierParty/s1:Party/s1:PostalAddress/s0:StreetName/text()" />
          </Street>
          <PostalCode>
            <xsl:value-of select="s1:AccountingSupplierParty/s1:Party/s1:PostalAddress/s0:PostalZone/text()" />
          </PostalCode>
          <City>
            <xsl:value-of select="s1:AccountingSupplierParty/s1:Party/s1:PostalAddress/s0:CityName/text()" />
          </City>
        </Address>
        <Concact>
          <ID>
            <xsl:value-of select="s1:AccountingSupplierParty/s1:Party/s1:Contact/s0:ID/text()" />
          </ID>
          <Name>
            <xsl:value-of select="s1:AccountingSupplierParty/s1:Party/s1:Contact/s0:Name/text()" />
          </Name>
        </Concact>
      </AccountingSupplier>
      <AccountingCustomer>
        <AccBuyerID>
          <xsl:value-of select="s1:AccountingCustomerParty/s1:Party/s0:EndpointID/text()" />
        </AccBuyerID>
        <PartyID>
          <xsl:value-of select="s1:AccountingCustomerParty/s1:Party/s1:PartyIdentification/s0:ID/text()" />
        </PartyID>
        <Address>
          <Name>
            <xsl:value-of select="s1:AccountingCustomerParty/s1:Party/s1:PartyName/s0:Name/text()" />
          </Name>
          <Street>
            <xsl:value-of select="s1:AccountingCustomerParty/s1:Party/s1:PostalAddress/s0:StreetName/text()" />
          </Street>
          <PostalCode>
            <xsl:value-of select="s1:AccountingCustomerParty/s1:Party/s1:PostalAddress/s0:PostalZone/text()" />
          </PostalCode>
          <City>
            <xsl:value-of select="s1:AccountingCustomerParty/s1:Party/s1:PostalAddress/s0:CityName/text()" />
          </City>
          <Country>
            <xsl:value-of select="s1:AccountingCustomerParty/s1:Party/s1:PostalAddress/s1:Country/s0:IdentificationCode/text()" />
          </Country>
        </Address>
      </AccountingCustomer>
      <BuyerCustomer>
        <BuyerID>
          <xsl:value-of select="s1:BuyerCustomerParty/s1:Party/s0:EndpointID/text()" />
        </BuyerID>
        <PartyID>
          <xsl:value-of select="s1:BuyerCustomerParty/s1:Party/s1:PartyIdentification/s0:ID/text()" />
        </PartyID>
        <Address>
          <Name>
            <xsl:value-of select="s1:BuyerCustomerParty/s1:Party/s1:PartyName/s0:Name/text()" />
          </Name>
          <Street>
            <xsl:value-of select="s1:BuyerCustomerParty/s1:Party/s1:PostalAddress/s0:StreetName/text()" />
          </Street>
          <PostalCode>
            <xsl:value-of select="s1:BuyerCustomerParty/s1:Party/s1:PostalAddress/s0:PostalZone/text()" />
          </PostalCode>
          <City>
            <xsl:value-of select="s1:BuyerCustomerParty/s1:Party/s1:PostalAddress/s0:CityName/text()" />
          </City>
          <Country>
            <xsl:value-of select="s1:BuyerCustomerParty/s1:Party/s1:PostalAddress/s1:Country/s0:IdentificationCode/text()" />
          </Country>
        </Address>
        <Concact>
          <ID>
            <xsl:value-of select="s1:BuyerCustomerParty/s1:Party/s1:Contact/s0:ID/text()" />
          </ID>
          <Name>
            <xsl:value-of select="s1:BuyerCustomerParty/s1:Party/s1:Contact/s0:Name/text()" />
          </Name>
        </Concact>
      </BuyerCustomer>
      <!-- DeliveryLocation - Only map if present -->
      <xsl:for-each select="s1:Delivery/s1:DeliveryLocation">
        <DeliveryLocation>
          <Address>
            <Name>
              <xsl:value-of select="s0:Description/text()" />
            </Name>
            <MarkAttention>
              <xsl:value-of select="s1:Address/s0:MarkAttention/text()" />
            </MarkAttention>
            <Street>
              <xsl:value-of select="s1:Address/s0:StreetName/text()" />
            </Street>
            <PostalCode>
              <xsl:value-of select="s1:Address/s0:PostalZone/text()" />
            </PostalCode>
            <City>
              <xsl:value-of select="s1:Address/s0:CityName/text()" />
            </City>
            <Country>
              <xsl:value-of select="s1:Address/s1:Country/s0:IdentificationCode/text()" />
            </Country>
          </Address>
        </DeliveryLocation>
      </xsl:for-each>
      <DeliveryInfo>
        <ActualDeliveryDate>
          <xsl:value-of select="s1:Delivery/s0:ActualDeliveryDate/text()" />
        </ActualDeliveryDate>
      </DeliveryInfo>
      <PaymentMeans>
        <PaymentMeansCode>
          <xsl:value-of select="s1:PaymentMeans/s0:PaymentMeansCode/text()" />
        </PaymentMeansCode>
        <PaymentDueDate>
          <xsl:value-of select="s1:PaymentMeans/s0:PaymentDueDate/text()" />
        </PaymentDueDate>
        <PaymentChannelCode>
          <xsl:value-of select="s1:PaymentMeans/s0:PaymentChannelCode/text()" />
        </PaymentChannelCode>
      </PaymentMeans>
      <TaxTotal>
        <TaxAmount>
          <xsl:value-of select="s1:TaxTotal/s0:TaxAmount/text()" />
        </TaxAmount>
        <TaxSubtotal>
          <TaxAmount>
            <xsl:value-of select="s1:TaxTotal/s1:TaxSubtotal/s0:TaxAmount/text()" />
          </TaxAmount>
          <TaxableAmount>
            <xsl:value-of select="s1:TaxTotal/s1:TaxSubtotal/s0:TaxableAmount/text()" />
          </TaxableAmount>
          <TaxPercent>
            <xsl:value-of select="s1:TaxTotal/s1:TaxSubtotal/s0:Percent/text()" />
          </TaxPercent>
          <TaxCategory>
            <ID>
              <xsl:value-of select="s1:TaxTotal/s1:TaxSubtotal/s1:TaxCategory/s0:ID/text()" />
            </ID>
            <Percent>
              <xsl:value-of select="s1:TaxTotal/s1:TaxSubtotal/s1:TaxCategory/s0:Percent/text()" />
            </Percent>
            <TaxTypeCode>
              <xsl:value-of select="s1:TaxTotal/s1:TaxSubtotal/s1:TaxCategory/s1:TaxScheme/s0:TaxTypeCode/text()" />
            </TaxTypeCode>
          </TaxCategory>
        </TaxSubtotal>
      </TaxTotal>
      <Total>
        <AmountInclTax>
          <xsl:value-of select="s1:LegalMonetaryTotal/s0:PayableAmount/text()" />
        </AmountInclTax>
        <AmountExclTax>
          <xsl:value-of select="s1:LegalMonetaryTotal/s0:LineExtensionAmount/text()" />
        </AmountExclTax>
      </Total>
      <Lines>
        <xsl:for-each select="s1:InvoiceLine">
          <Line>
            <LineNo>
              <xsl:value-of select="s0:ID/text()" />
            </LineNo>
            <Note>
              <xsl:value-of select="s0:Note/text()" />
            </Note>
            <Quantity>
              <xsl:value-of select="s0:InvoicedQuantity/text()" />
            </Quantity>
            <UnitCode>
              <xsl:value-of select="s0:InvoicedQuantity/@unitCode" />
            </UnitCode>
            <!--<xsl:value-of select="format-number(./Rate, '##0.00')"/>-->
            <LineAmountTotal>
              <xsl:value-of select="format-number(s0:LineExtensionAmount/text(), '##0.00')" />
            </LineAmountTotal>
            <OrderLineReference>
              <xsl:value-of select="s1:OrderLineReference/text()" />
            </OrderLineReference>
            <Item>
              <BuyerItemID>
                <xsl:value-of select="s1:Item/s1:BuyersItemIdentification/s0:ID/text()" />
              </BuyerItemID>
              <SellerItemID>
                <xsl:value-of select="s1:Item/s1:SellersItemIdentification/s0:ID/text()" />
              </SellerItemID>
              <Description>
                <xsl:value-of select="s1:Item/s0:Description/text()" />
              </Description>
            </Item>
            <Price>
              <xsl:variable name="var:v1" select="userCSharp:AddDecimals(string(s1:Price/s0:PriceAmount/text()))" />
              <PriceNet>
                <xsl:value-of select="$var:v1" />
              </PriceNet>
            </Price>
          </Line>
        </xsl:for-each>
      </Lines>
    </ns0:Invoice>
  </xsl:template>
  <msxsl:script language="C#" implements-prefix="userCSharp">
    <![CDATA[

public string AddDecimals(Double OriginalAmount)
        {
            string NewAmount;
            NewAmount = string.Format("{0:0.00}", OriginalAmount);
            return NewAmount;

        }

  public string ReturnGUID()
{
 Guid g = Guid.NewGuid();
 String GUID = g.ToString();
 return GUID.ToUpper();
}


]]>
  </msxsl:script>
</xsl:stylesheet>