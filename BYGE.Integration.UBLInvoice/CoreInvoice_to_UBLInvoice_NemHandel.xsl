﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:var="http://schemas.microsoft.com/BizTalk/2003/var" exclude-result-prefixes="msxsl var s0 userCSharp" version="1.0" xmlns:ns0="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2" xmlns:s0="http://byg-e.dk/schemas/v10" xmlns:userCSharp="http://schemas.microsoft.com/BizTalk/2003/userCSharp" xmlns="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2" xmlns:ccts="urn:oasis:names:specification:ubl:schema:xsd:CoreComponentParameters-2" xmlns:sdt="urn:oasis:names:specification:ubl:schema:xsd:SpecializedDatatypes-2" xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2" xmlns:ext="urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2" xmlns:qdt="urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2" xmlns:udt="urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <!-- This mapping is being used. Make all adjustments here --> 
  <!-- BDB/SJ 161219: for-each removed 3 times in Delivery -->
  <!-- BDB/SJ 090120: </cbc:Delivery> changed to </cac:Delivery> -->
  <!-- BDB/SJ 210120: DeliveryLocation added before Address/Number -->
  <xsl:template match="/">
    <xsl:apply-templates select="/s0:Invoice" />
  </xsl:template>
  <xsl:template match="/s0:Invoice">
    <ns0:Invoice xsi:schemaLocation="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2 UBLInvoice.xsd">
      <!-- Saving fixed values for later - Start -->
      <xsl:variable name="Text320" select="'320'" />
      <!-- Saving fixed values for later - End -->
      <cbc:UBLVersionID>
        <xsl:value-of select="'2.0'" />
      </cbc:UBLVersionID>
      <cbc:CustomizationID>
        <xsl:value-of select="'OIOUBL-2.02'" />
      </cbc:CustomizationID>
      <cbc:ProfileID>
        <xsl:attribute name="schemeID">
          <xsl:value-of select="'urn:oioubl:id:profileid-1.2'" />
        </xsl:attribute>
        <xsl:attribute name="schemeAgencyID">
          <xsl:value-of select="$Text320" />
        </xsl:attribute>
        <xsl:value-of select="ProfileID/text()" />
      </cbc:ProfileID>
      <cbc:ID>
        <xsl:value-of select="InvoiceID/text()" />
      </cbc:ID>
      <xsl:if test="string-length(CopyIndicator/text()) != 0">  
        <cbc:CopyIndicator>  
          <xsl:value-of select="CopyIndicator/text()" />        
        </cbc:CopyIndicator>
      </xsl:if> 
      <cbc:IssueDate>
        <xsl:value-of select="IssueDate/text()" />
      </cbc:IssueDate>
      <xsl:if test="InvoiceType">
        <cbc:InvoiceTypeCode>
          <xsl:attribute name="listAgencyID">
            <xsl:value-of select="$Text320" />
          </xsl:attribute>
          <xsl:attribute name="listID">
            <xsl:value-of select="'urn:oioubl:codelist:invoicetypecode-1.1'" />
          </xsl:attribute>
          <!-- xsl:value-of select="InvoiceType/text()" / ...if ingoing is PIE from oioxml, we got truble -->  
          <xsl:value-of select="'380'" />
        </cbc:InvoiceTypeCode>
      </xsl:if>
      <xsl:if test="string-length(Note/text()) != 0">
        <cbc:Note>
          <xsl:value-of select="Note/text()" />
        </cbc:Note>
      </xsl:if>
      <!-- Saving Currency for later to all Amount and Price segments -->
      <xsl:variable name="CurrencyCode" select="Currency/text()" />
      <xsl:if test="string-length($CurrencyCode) = 0">
        <xsl:value-of select="'DKK'" />
      </xsl:if>  
      <cbc:DocumentCurrencyCode>
        <xsl:value-of select="$CurrencyCode" />
      </cbc:DocumentCurrencyCode>
      <cac:OrderReference>
        <cbc:ID>
          <xsl:value-of select="OrderReference/OrderID/text()" />
        </cbc:ID>
        <xsl:if test="string-length(OrderReference/SalesOrderID/text()) != 0">
          <cbc:SalesOrderID>
            <xsl:value-of select="OrderReference/SalesOrderID/text()" />
          </cbc:SalesOrderID>
        </xsl:if>
        <xsl:if test="string-length(OrderReference/OrderDate/text()) != 0">
          <cbc:IssueDate>
            <xsl:value-of select="OrderReference/OrderDate/text()" />
          </cbc:IssueDate>
        </xsl:if>
        <xsl:if test="string-length(OrderReference/CustomerReference/text()) != 0">
          <cbc:CustomerReference>
            <xsl:value-of select="OrderReference/CustomerReference/text()" />
          </cbc:CustomerReference>
        </xsl:if>
      </cac:OrderReference>
      <!-- DespatchDocumentreference -->
      <!-- AccountingSupplierParty -->
      <cac:AccountingSupplierParty>
        <cac:Party>
          <cbc:EndpointID>
            <xsl:attribute name="schemeID">
              <xsl:value-of select="AccountingSupplier/SchemeID/Endpoint/text()" />
            </xsl:attribute>
            <xsl:value-of select="AccountingSupplier/AccSellerID/text()" />  
          </cbc:EndpointID>
          <cac:PartyIdentification>
            <cbc:ID>
              <xsl:attribute name="schemeID">
                <xsl:if test="AccountingSupplier/SchemeID/Party='EAN'"> 
                  <xsl:value-of select="'GLN'" /> 
                </xsl:if>  
                <xsl:if test="AccountingSupplier/SchemeID/Party!='EAN'"> 
                  <xsl:value-of select="AccountingSupplier/SchemeID/Party/text()" /> 
                </xsl:if> 
              </xsl:attribute>
              <xsl:value-of select="AccountingSupplier/PartyID/text()" />
            </cbc:ID>
          </cac:PartyIdentification>
          <!-- AccountingSupplier/Address - Only map if present -->
          <xsl:for-each select="AccountingSupplier/Address">
            <cac:PartyName>
              <xsl:if test="Name/text()">
                <cbc:Name>
                  <xsl:value-of select="Name/text()" />
                </cbc:Name>
              </xsl:if>
            </cac:PartyName>
          </xsl:for-each>
          <!-- AccountingSupplier/Address - Only map if present -->
          <xsl:for-each select="AccountingSupplier/Address">
            <cac:PostalAddress>
              <cbc:AddressFormatCode>
                <xsl:attribute name="listAgencyID">
                  <xsl:value-of select="$Text320" />
                </xsl:attribute>
                <xsl:attribute name="listID">
                  <xsl:value-of select="'urn:oioubl:codelist:addressformatcode-1.1'" />
                </xsl:attribute>
                <xsl:if test="string-length(AddressFormatCode/text()) = 0">
                  <xsl:value-of select="'StructuredDK'" /> 
                </xsl:if> 
                <xsl:if test="string-length(AddressFormatCode/text()) != 0">  
                  <xsl:value-of select="AddressFormatCode/text()" /> 
                </xsl:if> 
              </cbc:AddressFormatCode> 
              <xsl:if test="string-length(Postbox/text()) != 0">
                <cbc:Postbox>
                  <xsl:value-of select="Postbox/text()" />
                </cbc:Postbox>
              </xsl:if>
              <xsl:if test="string-length(Street/text()) != 0">
                <xsl:variable name="fullStreetName" select="Street/text()" />
                <cbc:StreetName>
                  <!-- xsl:value-of select="Street/text()" / -->
                  <xsl:value-of select="userCSharp:GetStreetName($fullStreetName)" />
                </cbc:StreetName>
                <cbc:BuildingNumber>
                  <xsl:choose>
                    <xsl:when test="string-length(Number/text()) = 0">
                      <xsl:value-of select="userCSharp:GetStreetNumber($fullStreetName)" />
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="Number/text()" />
                    </xsl:otherwise>
                  </xsl:choose>
                </cbc:BuildingNumber>
              </xsl:if>  
              <xsl:if test="string-length(Department/text()) != 0">
                <cbc:Department>
                  <xsl:value-of select="Department/text()" />
                </cbc:Department>
              </xsl:if>
              <!-- cbc:AdditionalStreetName - not allowed in NemHandel -->
              <xsl:if test="string-length(AdditionalStreetName/text()) != 0">
                <cbc:CitySubdivisionName>
                  <xsl:value-of select="AdditionalStreetName/text()" />
                </cbc:CitySubdivisionName>  
              </xsl:if>
              <xsl:if test="string-length(City/text()) != 0">
                <cbc:CityName>
                  <xsl:value-of select="City/text()" />
                </cbc:CityName>
              </xsl:if>
              <xsl:if test="string-length(PostalCode/text()) != 0">
                <cbc:PostalZone>
                  <xsl:value-of select="PostalCode/text()" />
                </cbc:PostalZone>
              </xsl:if>
              <xsl:if test="string-length(Country/text()) != 0">
                <cac:Country>
                  <cbc:IdentificationCode>
                    <xsl:value-of select="Country/text()" />
                  </cbc:IdentificationCode>
                </cac:Country>
              </xsl:if>  
            </cac:PostalAddress>
          </xsl:for-each>
          <cac:PartyLegalEntity>
            <cbc:CompanyID>              
              <xsl:if test="string-length(AccountingSupplier/CompanyID/text()) != 0">
                <xsl:attribute name="schemeID">
                  <xsl:value-of select="AccountingSupplier/SchemeID/Company/text()" />
                </xsl:attribute>
                <xsl:value-of select="AccountingSupplier/CompanyID/text()" />
              </xsl:if>            
              <xsl:if test="string-length(AccountingSupplier/CompanyTaxID/text()) != 0">
                <xsl:attribute name="schemeID">
                  <xsl:value-of select="'DK:'" />
                  <xsl:value-of select="AccountingSupplier/SchemeID/CompanyTax/text()" />
                </xsl:attribute>
                <xsl:value-of select="'DK'" />
                <xsl:value-of select="AccountingSupplier/CompanyTaxID/text()" />
              </xsl:if>
            </cbc:CompanyID>
          </cac:PartyLegalEntity>
          <xsl:for-each select="AccountingSupplier/Concact">
            <cac:Contact>
              <cbc:ID>
                <xsl:if test="string-length(ID/text()) = 0">
                  <xsl:value-of select="'x'" />
                </xsl:if> 
                <xsl:if test="string-length(ID/text()) != 0">
                  <xsl:value-of select="ID/text()" />
                </xsl:if>  
              </cbc:ID>
              <xsl:if test="string-length(ID/text()) != 0">
                <xsl:if test="string-length(Name/text()) != 0"> 
                  <cbc:Name>
                    <xsl:value-of select="Name/text()" />
                  </cbc:Name>
                </xsl:if>
                <xsl:if test="string-length(Telephone/text()) != 0">
                  <cbc:Telephone>
                    <xsl:value-of select="Telephone/text()" />
                  </cbc:Telephone>
                </xsl:if>
                <xsl:if test="string-length(Telefax/text()) != 0">
                  <cbc:Telefax>
                    <xsl:value-of select="Telefax/text()" />
                  </cbc:Telefax>
                </xsl:if>
                <xsl:if test="string-length(E-mail/text()) != 0">
                  <cbc:ElectronicMail>
                    <xsl:value-of select="E-mail/text()" />
                  </cbc:ElectronicMail>
                </xsl:if>
                <xsl:if test="string-length(Note/text()) != 0">
                  <cbc:Note>
                    <xsl:value-of select="Note/text()" />
                  </cbc:Note>
                </xsl:if>
              </xsl:if>  
            </cac:Contact>
          </xsl:for-each>
        </cac:Party>
      </cac:AccountingSupplierParty>
      <!-- AccountingCustomerParty-->
      <cac:AccountingCustomerParty>
        <xsl:if test="string-length(AccountingCustomer/SupplierAssignedAccountID/text()) != 0">
          <cbc:SupplierAssignedAccountID>
            <xsl:value-of select="AccountingCustomer/SupplierAssignedAccountID/text()" />
          </cbc:SupplierAssignedAccountID>
        </xsl:if>
        <cac:Party>
          <cbc:EndpointID>
            <xsl:attribute name="schemeID">
              <xsl:value-of select="AccountingCustomer/SchemeID/Endpoint/text()" />
            </xsl:attribute>
            <xsl:value-of select="AccountingCustomer/AccBuyerID/text()" />
          </cbc:EndpointID>
          <cac:PartyIdentification>
            <cbc:ID>
              <xsl:attribute name="schemeID">
                <xsl:if test="AccountingCustomer/SchemeID/Party='EAN'"> 
                  <xsl:value-of select="'GLN'" /> 
                </xsl:if>  
                <xsl:if test="AccountingCustomer/SchemeID/Party!='EAN'"> 
                  <xsl:value-of select="AccountingCustomer/SchemeID/Party/text()" /> 
                </xsl:if> 
              </xsl:attribute>
              <xsl:value-of select="AccountingCustomer/PartyID/text()" />
            </cbc:ID>
          </cac:PartyIdentification>
          <cac:PartyName>
            <cbc:Name>
              <xsl:value-of select="AccountingCustomer/Address/Name/text()" />
            </cbc:Name>
          </cac:PartyName>
          <cac:PostalAddress>
            <cbc:AddressFormatCode>
              <xsl:attribute name="listAgencyID">
                <xsl:value-of select="$Text320" />
              </xsl:attribute>
              <xsl:attribute name="listID">
                <xsl:value-of select="'urn:oioubl:codelist:addressformatcode-1.1'" />
              </xsl:attribute>
              <xsl:if test="string-length(AccountingCustomer/Address/AddressFormatCode/text()) = 0">
                  <xsl:value-of select="'StructuredDK'" /> 
              </xsl:if> 
              <xsl:if test="string-length(AccountingCustomer/Address/AddressFormatCode/text()) != 0">
                <xsl:value-of select="AccountingCustomer/Address/AddressFormatCode/text()" /> 
              </xsl:if> 
            </cbc:AddressFormatCode>
            <xsl:if test="string-length(AccountingCustomer/Address/Postbox/text()) != 0">
              <cbc:Postbox>
                <xsl:value-of select="AccountingCustomer/Address/Postbox/text()" />
              </cbc:Postbox>
            </xsl:if> 
            <!-- suss suss suss -->
            <xsl:if test="string-length(AccountingCustomer/Address/Street/text()) != 0">
                <xsl:variable name="fullStreetName" select="AccountingCustomer/Address/Street/text()" />
                <cbc:StreetName>
                  <!-- xsl:value-of select="AccountingCustomer/Address/Street/text()" / -->
                  <xsl:value-of select="userCSharp:GetStreetName($fullStreetName)" />
                </cbc:StreetName>
                <cbc:BuildingNumber>
                  <xsl:choose>
                    <xsl:when test="string-length(AccountingCustomer/Address/Number/text()) = 0">
                      <xsl:value-of select="userCSharp:GetStreetNumber($fullStreetName)" />
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="AccountingCustomer/Address/Number/text()" />
                    </xsl:otherwise>
                  </xsl:choose>
                </cbc:BuildingNumber>
              </xsl:if>    
            <xsl:if test="string-length(AccountingCustomer/Address/Department/text()) != 0">
              <cbc:Department>
                <xsl:value-of select="AccountingCustomer/Address/Department/text()" />
              </cbc:Department>
            </xsl:if>
            <xsl:if test="string-length(AccountingCustomer/Address/City/text()) != 0">
              <cbc:CityName>
                <xsl:value-of select="AccountingCustomer/Address/City/text()" />
              </cbc:CityName>
            </xsl:if>
            <xsl:if test="string-length(AccountingCustomer/Address/PostalCode/text()) != 0"> 
              <cbc:PostalZone>
                <xsl:value-of select="AccountingCustomer/Address/PostalCode/text()" />
              </cbc:PostalZone>
            </xsl:if>
            <xsl:if test="string-length(AccountingCustomer/Address/Country/text()) != 0"> 
              <cac:Country>
                <cbc:IdentificationCode>
                  <xsl:value-of select="AccountingCustomer/Address/Country/text()" />
                </cbc:IdentificationCode>
              </cac:Country>
            </xsl:if>  
          </cac:PostalAddress>
          <xsl:if test="string-length(AccountingCustomer/CompanyID/text()) != 0">
            <cac:PartyLegalEntity>
              <cbc:CompanyID>
                <xsl:attribute name="schemeID">
                  <xsl:value-of select="AccountingCustomer/SchemeID/Company/text()" />
                </xsl:attribute>
                <xsl:value-of select="AccountingCustomer/CompanyID/text()" />
              </cbc:CompanyID>
            </cac:PartyLegalEntity>
          </xsl:if>  
          <!-- AccountingCustomer/Concact - Only map if present -->
          <xsl:for-each select="AccountingCustomer/Concact">
            <cac:Contact>
              <cbc:ID>
                <xsl:if test="string-length(ID/text()) = 0"> 
                  <xsl:value-of select="'x'" />
                </xsl:if>  
                <xsl:if test="string-length(ID/text()) != 0">
                  <xsl:value-of select="ID/text()" />
                </xsl:if>  
              </cbc:ID>
              <xsl:if test="string-length(ID/text()) != 0">
                <xsl:if test="string-length(Name/text()) != 0">
                  <cbc:Name>
                    <xsl:value-of select="Name/text()" />
                  </cbc:Name>
                </xsl:if>
                <xsl:if test="string-length(Telephone/text()) != 0"> 
                  <cbc:Telephone>
                    <xsl:value-of select="Telephone/text()" />
                  </cbc:Telephone>
                </xsl:if>
                <xsl:if test="string-length(E-mail/text()) != 0">
                  <cbc:ElectronicMail>
                    <xsl:value-of select="E-mail/text()" />
                  </cbc:ElectronicMail>
                </xsl:if>
                <xsl:if test="string-length(Note/text()) != 0">
                  <cbc:Note>
                    <xsl:value-of select="Note/text()" />
                  </cbc:Note>
                </xsl:if>
              </xsl:if>    
            </cac:Contact>
          </xsl:for-each>
        </cac:Party>
      </cac:AccountingCustomerParty>
      <!-- BuyerCustomerParty -->
      <cac:BuyerCustomerParty>
        <cac:Party>
          <cbc:EndpointID>
            <xsl:attribute name="schemeID">
              <xsl:value-of select="BuyerCustomer/SchemeID/Endpoint/text()" />
            </xsl:attribute>
            <xsl:value-of select="BuyerCustomer/BuyerID/text()" />
          </cbc:EndpointID>
          <cac:PartyIdentification>
            <cbc:ID>
              <xsl:attribute name="schemeID">
                <xsl:if test="BuyerCustomer/SchemeID/Party='EAN'"> 
                  <xsl:value-of select="'GLN'" /> 
                </xsl:if>  
                <xsl:if test="BuyerCustomer/SchemeID/Party!='EAN'"> 
                  <xsl:value-of select="BuyerCustomer/SchemeID/Party/text()" /> 
                </xsl:if> 
              </xsl:attribute>
              <xsl:value-of select="BuyerCustomer/PartyID/text()" /> 
            </cbc:ID>
          </cac:PartyIdentification>
          <!-- BuyerCustomer/Address - Only map if present -->
          <xsl:for-each select="BuyerCustomer/Address">
            <cac:PartyName>
              <cbc:Name>
                <xsl:value-of select="Name/text()" />
              </cbc:Name>
            </cac:PartyName>
          </xsl:for-each>
          <!-- BuyerCustomer/Address - Only map if present -->
          <xsl:for-each select="BuyerCustomer/Address">
            <cac:PostalAddress>
              <cbc:AddressFormatCode>
                <xsl:attribute name="listAgencyID">
                  <xsl:value-of select="$Text320" />
                </xsl:attribute>
                <xsl:attribute name="listID">
                  <xsl:value-of select="'urn:oioubl:codelist:addressformatcode-1.1'" />
                </xsl:attribute>
                <xsl:if test="string-length(AddressFormatCode/text()) = 0">
                  <xsl:value-of select="'StructuredDK'" /> 
                </xsl:if> 
                <xsl:if test="string-length(AddressFormatCode/text()) != 0">
                  <xsl:value-of select="AddressFormatCode/text()" /> 
                </xsl:if> 
              </cbc:AddressFormatCode>
              <xsl:if test="string-length(Postbox/text()) != 0">
                <cbc:Postbox>
                  <xsl:value-of select="Postbox/text()" />
                </cbc:Postbox>
              </xsl:if>
              <xsl:if test="string-length(Street/text()) != 0">
                <xsl:variable name="fullStreetName" select="Street/text()" />
                <cbc:StreetName>
                  <!-- xsl:value-of select="Street/text()" / -->
                  <xsl:value-of select="userCSharp:GetStreetName($fullStreetName)" />
                </cbc:StreetName>
                <cbc:BuildingNumber>
                  <xsl:choose>
                    <xsl:when test="string-length(Number/text()) = 0">
                      <xsl:value-of select="userCSharp:GetStreetNumber($fullStreetName)" />
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="Number/text()" />
                    </xsl:otherwise>
                  </xsl:choose>
                </cbc:BuildingNumber>
              </xsl:if> 
              <xsl:if test="string-length(City/text()) != 0">
                <cbc:CityName>
                  <xsl:value-of select="City/text()" />
                </cbc:CityName>
              </xsl:if>
              <xsl:if test="string-length(Department/text()) != 0 and string-length(City/text()) = 0">
                <cbc:CityName>
                  <xsl:value-of select="Department/text()" />
                </cbc:CityName>
              </xsl:if>
              <xsl:if test="string-length(PostalCode/text()) != 0">
                <cbc:PostalZone>
                  <xsl:value-of select="PostalCode/text()" />
                </cbc:PostalZone>
              </xsl:if>
              <xsl:if test="string-length(Country/text()) != 0">
                <cac:Country>
                  <cbc:IdentificationCode>
                    <xsl:value-of select="Country/text()" />
                  </cbc:IdentificationCode>
                </cac:Country>
              </xsl:if>  
            </cac:PostalAddress>
          </xsl:for-each>
          <xsl:if test="string-length(BuyerCustomer/CompanyID/text()) != 0">
            <cac:PartyLegalEntity>
              <cbc:CompanyID>
                <xsl:attribute name="schemeID">
                  <xsl:value-of select="BuyerCustomer/SchemeID/Company/text()" />
                </xsl:attribute>
                <xsl:value-of select="BuyerCustomer/CompanyID/text()" />
              </cbc:CompanyID> 
            </cac:PartyLegalEntity>
          </xsl:if>
          <!-- BuyerCustomer/Concact - Only map if present -->
          <xsl:for-each select="BuyerCustomer/Concact">
            <xsl:if test="string-length(ID/text()) != 0">
              <cac:Contact>
                <cbc:ID>
                  <xsl:value-of select="ID/text()" />
                </cbc:ID>
                <xsl:if test="string-length(Name/text()) != 0">
                  <cbc:Name>
                    <xsl:value-of select="Name/text()" />
                  </cbc:Name>
                </xsl:if>
                <xsl:if test="string-length(Telephone/text()) != 0">
                  <cbc:Telephone>
                    <xsl:value-of select="Telephone/text()" />
                  </cbc:Telephone>
                </xsl:if>
                <xsl:if test="string-length(Telefax/text()) != 0">
                  <cbc:Telefax>
                    <xsl:value-of select="Telefax/text()" />
                  </cbc:Telefax>
                </xsl:if>
                <xsl:if test="string-length(E-mail/text()) != 0">
                  <cbc:ElectronicMail>
                    <xsl:value-of select="E-mail/text()" />
                  </cbc:ElectronicMail>
                </xsl:if>
                <xsl:if test="string-length(Note/text()) != 0">
                  <cbc:Note>
                    <xsl:value-of select="Note/text()" />
                  </cbc:Note>
                </xsl:if>
              </cac:Contact>
            </xsl:if>  
          </xsl:for-each>
        </cac:Party>
      </cac:BuyerCustomerParty>
      <!-- Delivery -->
      <!-- DeliveryInfo - Only map if present -->
      <!-- BDB/SJ 191216: for-each removed -->
      <cac:Delivery>
        <xsl:if test="string-length(DeliveryInfo/ActualDeliveryDate/text()) != 0">
          <cbc:ActualDeliveryDate>
            <xsl:value-of select="DeliveryInfo/ActualDeliveryDate/text()" />
          </cbc:ActualDeliveryDate>
        </xsl:if>
        <!-- BDB/SJ 191216: for-each removed -->
        <xsl:if test="string-length(DeliveryTerms/SpecialTerms/text()) != 0">
          <cac:DeliveryTerms>
            <cbc:SpecialTerms>
              <xsl:value-of select="DeliveryTerms/SpecialTerms/text()" />
            </cbc:SpecialTerms>
          </cac:DeliveryTerms>
        </xsl:if>
        <!-- DeliveryLocation - Only map if present -->
        <!-- BDB/SJ 191216: for-each removed -->
        <cac:DeliveryLocation>
          <xsl:if test="string-length(DeliveryLocation/Address/Name/text()) != 0">
            <cbc:Description>
              <xsl:value-of select="DeliveryLocation/Address/Name/text()" />
            </cbc:Description>
          </xsl:if>  
          <cac:Address>
            <cbc:AddressFormatCode>
              <xsl:attribute name="listAgencyID">
                <xsl:value-of select="$Text320" />
              </xsl:attribute>
              <xsl:attribute name="listID">
                <xsl:value-of select="'urn:oioubl:codelist:addressformatcode-1.1'" />
              </xsl:attribute>
              <xsl:if test="string-length(DeliveryLocation/Address/AddressFormatCode/text()) = 0">
                <xsl:value-of select="'StructuredDK'" /> 
              </xsl:if> 
              <xsl:if test="string-length(DeliveryLocation/Address/AddressFormatCode/text()) != 0">
                <xsl:value-of select="DeliveryLocation/Address/AddressFormatCode/text()" /> 
              </xsl:if>
            </cbc:AddressFormatCode>
            <!-- BDB/SJ 210120:  DeliveryLocation added before Address/Number -->
            <xsl:if test="string-length(DeliveryLocation/Address/Street/text()) != 0">
              <xsl:variable name="fullStreetName" select="DeliveryLocation/Address/Street/text()" />
              <cbc:StreetName>
                <!-- xsl:value-of select="Address/Street/text())" / --> 
                <xsl:value-of select="userCSharp:GetStreetName($fullStreetName)" />
              </cbc:StreetName>
              <cbc:BuildingNumber>
                <xsl:choose>
                  <xsl:when test="string-length(DeliveryLocation/Address/Number/text()) = 0">
                    <xsl:value-of select="userCSharp:GetStreetNumber($fullStreetName)" />
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="DeliveryLocation/Address/Number/text()" />
                  </xsl:otherwise>
                </xsl:choose>
              </cbc:BuildingNumber>
            </xsl:if> 
            <xsl:if test="string-length(DeliveryLocation/Address/MarkAttention/text()) != 0"> 
              <cbc:MarkAttention>
                <xsl:value-of select="DeliveryLocation/Address/MarkAttention/text()" />
              </cbc:MarkAttention>
            </xsl:if>
            <xsl:if test="string-length(DeliveryLocation/Address/Department/text()) != 0"> 
              <cbc:Department>
                <xsl:value-of select="DeliveryLocation/Address/Department/text()" />
              </cbc:Department>
            </xsl:if>
            <xsl:if test="string-length(DeliveryLocation/Address/City/text()) != 0">
              <cbc:CityName>
                <xsl:value-of select="DeliveryLocation/Address/City/text()" />
              </cbc:CityName>
            </xsl:if>
            <xsl:if test="string-length(DeliveryLocation/Address/PostalCode/text()) != 0">
              <cbc:PostalZone>
                <xsl:value-of select="DeliveryLocation/Address/PostalCode/text()" />
              </cbc:PostalZone>
            </xsl:if>
            <xsl:if test="string-length(DeliveryLocation/Address/Country/text()) != 0">
            <cac:Country>             
                <cbc:IdentificationCode>
                  <xsl:value-of select="DeliveryLocation/Address/Country/text()" />
                </cbc:IdentificationCode>
              </cac:Country>
            </xsl:if>            
          </cac:Address>
        </cac:DeliveryLocation>
      </cac:Delivery>
     <!-- PrepaidPayment -->       
      <xsl:for-each select="PrepaidPayment">
        <xsl:if test ="string-length(PaidAmount/text()) != 0">
          <cac:PrepaidPayment>
            <xsl:if test ="string-length(ID/text()) != 0">
              <cbc:ID>
                <xsl:value-of select="ID/text()" />
              </cbc:ID>
            </xsl:if>
            <xsl:if test ="string-length(PaidAmount/text()) != 0">
              <cbc:PaidAmount>
                <xsl:attribute name="currencyID">
                  <xsl:value-of select="$CurrencyCode" />
                </xsl:attribute>
                <xsl:value-of select="PaidAmount/text()" />
              </cbc:PaidAmount>
            </xsl:if>
            <xsl:if test ="string-length(ReceivedDate/text()) != 0">
              <cbc:ReceivedDate>
                <xsl:value-of select="ReceivedDate/text()" />
              </cbc:ReceivedDate>
            </xsl:if>
            <xsl:if test ="string-length(PaidDate/text()) != 0">
              <cbc:PaidDate>
                <xsl:value-of select="PaidDate/text()" />
              </cbc:PaidDate>
            </xsl:if>
            <xsl:if test ="string-length(PaidTime/text()) != 0">
              <cbc:PaidTime>
                <xsl:value-of select="PaidTime/text()" />
              </cbc:PaidTime>
            </xsl:if>
            <xsl:if test ="string-length(InstructionID/text()) != 0">
              <cbc:InstructionID>
                <xsl:value-of select="InstructionID/text()" />
              </cbc:InstructionID>
            </xsl:if>            
          </cac:PrepaidPayment>
        </xsl:if>
       </xsl:for-each>     
      <!-- ALC -->
      <xsl:for-each select ="AllowanceCharge">
        <xsl:if test ="string-length(ChargeIndicator/text()) != 0">
        <cac:AllowanceCharge>
          <xsl:if test ="string-length(ID/text()) != 0">
        <cbc:ID>
          <xsl:value-of select="ID/text()"/>
       </cbc:ID>
          </xsl:if>

          <xsl:if test ="string-length(ChargeIndicator/text()) != 0">
            <cbc:ChargeIndicator>
              <xsl:value-of select="ChargeIndicator/text()"/>
            </cbc:ChargeIndicator>
          </xsl:if>

          <xsl:if test ="string-length(AllowanceChargeReasonCode/text()) != 0">
            <cbc:AllowanceChargeReasonCode>
              <xsl:value-of select="AllowanceChargeReasonCode/text()"/>
            </cbc:AllowanceChargeReasonCode>
          </xsl:if>

          <xsl:if test ="string-length(AllowanceChargeReason/text()) != 0">
            <cbc:AllowanceChargeReason>
              <xsl:value-of select="AllowanceChargeReason/text()"/>
            </cbc:AllowanceChargeReason>
          </xsl:if>

          <xsl:if test ="string-length(MultipleFactorNumeric/text()) != 0">
            <cbc:MultipleFactorNumeric>
              <xsl:value-of select="MultipleFactorNumeric/text()"/>
            </cbc:MultipleFactorNumeric>
          </xsl:if>

          <xsl:if test ="string-length(PrepaidIndicator/text()) != 0">
            <cbc:PrepaidIndicator>
              <xsl:value-of select="PrepaidIndicator/text()"/>
            </cbc:PrepaidIndicator>
          </xsl:if>

          <xsl:if test ="string-length(Amount/text()) != 0">
            <cbc:Amount>
                <xsl:attribute name="currencyID">
                  <xsl:value-of select="$CurrencyCode" />
                </xsl:attribute>              
              <xsl:value-of select="Amount/text()"/>
            </cbc:Amount>
          </xsl:if>

          <xsl:if test ="string-length(BaseAmount/text()) != 0">
            <cbc:BaseAmount>
                <xsl:attribute name="currencyID">
                  <xsl:value-of select="$CurrencyCode" />
                </xsl:attribute>              
              <xsl:value-of select="BaseAmount/text()"/>
            </cbc:BaseAmount>
          </xsl:if>

          <xsl:if test ="string-length(AccountingCost/text()) != 0">
            <cbc:AccountingCost>
              <xsl:value-of select="AccountingCost/text()"/>
            </cbc:AccountingCost>
          </xsl:if>
          
          <cac:TaxCategory>
            <cbc:ID>
              
         
                  <xsl:attribute name="schemeAgencyID">
                    <xsl:value-of select="$Text320" />
                  </xsl:attribute>
                  <xsl:attribute name="schemeID">
                    <xsl:value-of select="'urn:oioubl:id:taxcategoryid-1.1'" />
                  </xsl:attribute>
                
            <xsl:if test ="string-length(TaxCategory/ID/text()) != 0">
              
                <xsl:value-of select="TaxCategory/ID/text()"/>
              
            </xsl:if>
                         
            <xsl:if test ="string-length(TaxCategory/ID/text()) = 0">

                <xsl:text>StandardRated</xsl:text>

            </xsl:if>
              </cbc:ID>

            <xsl:if test ="string-length(TaxCategory/Percent/text()) != 0">
              <cbc:Percent>
                <xsl:value-of select="TaxCategory/Percent/text()"/>
              </cbc:Percent>
            </xsl:if>
            
            <xsl:if test ="string-length(TaxCategory/Percent/text()) = 0">

              <xsl:if test ="string-length(//TaxTotal/TaxSubtotal/TaxCategory/Percent/text()) != 0"> 
                <cbc:Percent>
                <xsl:value-of select="//TaxTotal/TaxSubtotal/TaxCategory/Percent/text()"/>
                </cbc:Percent>
              </xsl:if>
              <xsl:if test ="string-length(//TaxTotal/TaxSubtotal/TaxCategory/Percent/text()) = 0">              
              <cbc:Percent> 
                <xsl:text>0.0000</xsl:text>
             </cbc:Percent>
                           </xsl:if>
              
            </xsl:if>

            <xsl:if test ="string-length(TaxCategory/PerUnitAmount/text()) != 0">
              <cbc:PerUnitAmount>
                <xsl:value-of select="TaxCategory/PerUnitAmount/text()"/>
              </cbc:PerUnitAmount>
            </xsl:if>

            <cac:TaxScheme>
              <cbc:ID>
               <xsl:attribute name="schemeAgencyID">
                      <xsl:value-of select="$Text320" />
                    </xsl:attribute>
                    <xsl:attribute name="schemeID">
                      <xsl:value-of select="'urn:oioubl:id:taxschemeid-1.1'" />
                    </xsl:attribute>
              <xsl:if test ="string-length(TaxCategory/TaxScheme/ID/text()) != 0">
                
                  <xsl:value-of select="TaxCategory/TaxScheme/ID/text()"/>
                
              </xsl:if>
              
              <xsl:if test ="string-length(TaxCategory/TaxScheme/ID/text()) = 0">
                
                  <xsl:text>63</xsl:text>
                
              </xsl:if>
                
              </cbc:ID>  
              <xsl:if test ="string-length(TaxCategory/TaxScheme/Navn/text()) != 0">
                <cbc:Name>
                  <xsl:value-of select="TaxCategory/TaxScheme/Navn/text()"/>
                </cbc:Name>
              </xsl:if>
             <xsl:if test ="string-length(TaxCategory/TaxScheme/Navn/text()) = 0">
                <cbc:Name>
                  <xsl:text>Moms</xsl:text>
                </cbc:Name>
              </xsl:if>
            </cac:TaxScheme>

          </cac:TaxCategory>

        </cac:AllowanceCharge>
        </xsl:if>
      </xsl:for-each>
      <!-- PaymentMeans -->
      <xsl:if test="string-length(PaymentMeans/PaymentMeansCode/text()) != 0">
        <cac:PaymentMeans>
          <cbc:ID>
            <xsl:if test="string-length(PaymentMeans/ID/text()) != 0">
              <xsl:value-of select="PaymentMeans/ID/text()" />
            </xsl:if>
          </cbc:ID>
          <xsl:if test="string-length(PaymentMeans/PaymentMeansCode/text()) != 0"> 
            <cbc:PaymentMeansCode>
              <xsl:choose>
                <!-- Payment for a giro card. For example, GIK., 01, 04 and 15 -->
		            <xsl:when test="PaymentMeans/PaymentMeansCode/text() = '01'">
			            <xsl:value-of select="'50'" />
		            </xsl:when>	
		            <xsl:when test="PaymentMeans/PaymentMeansCode/text() = '04'">
			            <xsl:value-of select="'50'" />
		            </xsl:when>	
		            <xsl:when test="PaymentMeans/PaymentMeansCode/text() = '15'">
			            <xsl:value-of select="'50'" />
		            </xsl:when>	
                <!-- Payment for a deposit card. Eg. FIK., 71, 73 and 75 -->
		            <xsl:when test="PaymentMeans/PaymentMeansCode/text() = '71'">
			            <xsl:value-of select="'93'" />
		            </xsl:when>	
		            <xsl:when test="PaymentMeans/PaymentMeansCode/text() = '73'">
			            <xsl:value-of select="'93'" />
		            </xsl:when>	
		            <xsl:when test="PaymentMeans/PaymentMeansCode/text() = '75'">
			            <xsl:value-of select="'93'" />
		            </xsl:when>	
		            <xsl:otherwise>
			            <xsl:value-of select="PaymentMeans/PaymentMeansCode/text()" />
		            </xsl:otherwise>
              </xsl:choose>      
            </cbc:PaymentMeansCode>
          </xsl:if>
          <cbc:PaymentDueDate>
            <xsl:value-of select="PaymentMeans/PaymentDueDate/text()" />
          </cbc:PaymentDueDate>
          <cbc:PaymentChannelCode>
            <xsl:attribute name="listAgencyID">
              <xsl:value-of select="$Text320" />
            </xsl:attribute>
            <xsl:attribute name="listID">
              <xsl:value-of select="'urn:oioubl:codelist:paymentchannelcode-1.1'" />
            </xsl:attribute>
            <xsl:value-of select="PaymentMeans/PaymentChannelCode/text()" />
          </cbc:PaymentChannelCode>
          <cbc:InstructionID>
            <xsl:value-of select="PaymentMeans/InstructionID/text()" />
          </cbc:InstructionID>
          <xsl:if test="string-length(PaymentMeans/InstructionNote/text()) != 0">
            <cbc:InstructionNote>
              <xsl:value-of select="PaymentMeans/InstructionNote/text()" />
            </cbc:InstructionNote>
          </xsl:if>
          <xsl:if test="string-length(PaymentMeans/PaymentID/text()) != 0">
            <cbc:PaymentID>
              <xsl:attribute name="schemeAgencyID">
                <xsl:value-of select="$Text320" />
              </xsl:attribute>
              <xsl:attribute name="schemeID">
                <xsl:value-of select="'urn:oioubl:id:paymentid-1.1'" />
              </xsl:attribute>
              <xsl:value-of select="PaymentMeans/PaymentID/text()" />
            </cbc:PaymentID>
          </xsl:if>
          <xsl:if test="string-length(PaymentMeans/CreditAccountID/text()) != 0">
            <cac:CreditAccount>
              <cbc:AccountID>
                <xsl:value-of select="PaymentMeans/CreditAccountID/text()" />
              </cbc:AccountID>
            </cac:CreditAccount>
          </xsl:if>
          <xsl:if test="string-length(PaymentMeans/PayeeFinancialAccount/ID/text()) != 0">
            <cac:PayeeFinancialAccount>
              <cbc:ID>
                 <xsl:value-of select="PaymentMeans/PayeeFinancialAccount/ID/text()" />              
              </cbc:ID>
              <xsl:if test="string-length(PaymentMeans/PayeeFinancialAccount/Name/text()) != 0">
              <cbc:Name>
                 <xsl:value-of select="PaymentMeans/PayeeFinancialAccount/Name/text()" />              
              </cbc:Name>                              
              </xsl:if>  
              <cac:FinancialInstitutionBranch>
                <xsl:if test="string-length(PaymentMeans/PayeeFinancialAccount/FinancialInstitutionBranch/ID/text()) != 0">
                  <cbc:ID>
                    <xsl:value-of select="PaymentMeans/PayeeFinancialAccount/FinancialInstitutionBranch/ID/text()" />                                
                  </cbc:ID>
                 </xsl:if>
                <xsl:if test="string-length(PaymentMeans/PayeeFinancialAccount/FinancialInstitutionBranch/Name/text()) != 0">
                  <cbc:Name>
                    <xsl:value-of select="PaymentMeans/PayeeFinancialAccount/FinancialInstitutionBranch/Name/text()" />
                  </cbc:Name>
                </xsl:if> 
               <xsl:if test="string-length(PaymentMeans/PayeeFinancialAccount/FinancialInstitutionBranch/FinancialInstitution/ID/text()) != 0 or string-length(PaymentMeans/PayeeFinancialAccount/FinancialInstitutionBranch/FinancialInstitution/Name/text()) != 0"> 
                <cac:FinancialInstitution>
                  <xsl:if test="string-length(PaymentMeans/PayeeFinancialAccount/FinancialInstitutionBranch/FinancialInstitution/ID/text()) != 0">
                  <cbc:ID>
                    <xsl:value-of select="PaymentMeans/PayeeFinancialAccount/FinancialInstitutionBranch/FinancialInstitution/ID/text()" />                                
                  </cbc:ID>
                  </xsl:if> 
                  <xsl:if test="string-length(PaymentMeans/PayeeFinancialAccount/FinancialInstitutionBranch/FinancialInstitution/Name/text()) != 0">
                  <cbc:Name>
                    <xsl:value-of select="PaymentMeans/PayeeFinancialAccount/FinancialInstitutionBranch/FinancialInstitution/Name/text()" />                                                                      
                  </cbc:Name>    
                 </xsl:if> 
                </cac:FinancialInstitution>              
               </xsl:if>  
              </cac:FinancialInstitutionBranch>                            
            </cac:PayeeFinancialAccount>  
          </xsl:if>  
        </cac:PaymentMeans>
      </xsl:if>
      <!-- PaymentTerms -->
      <xsl:if test="string-length(PaymentTerms/ID/text()) != 0">  
        <cac:PaymentTerms>
          <cbc:ID>
            <xsl:value-of select="PaymentTerms/ID/text()" />
          </cbc:ID>
          <xsl:if test="string-length(PaymentTerms/PaymentMeansID/text()) != 0">
            <cbc:PaymentMeansID>
              <xsl:value-of select="PaymentTerms/PaymentMeansID/text()" />
            </cbc:PaymentMeansID>
          </xsl:if>
          <xsl:if test="string-length(PaymentTerms/PrepaidPaymentReferenceID/text()) != 0">
            <cbc:PrepaidPaymentReferenceID>
              <xsl:value-of select="PaymentTerms/PrepaidPaymentReferenceID/text()" />
            </cbc:PrepaidPaymentReferenceID>
          </xsl:if>
          <xsl:if test="string-length(PaymentTerms/Note/text()) != 0">
            <cbc:Note>
              <xsl:value-of select="PaymentTerms/Note/text()" />
            </cbc:Note>
          </xsl:if>
          <xsl:if test="string-length(PaymentTerms/ReferenceEventCode/text()) != 0">
            <cbc:ReferenceEventCode>
              <xsl:value-of select="PaymentTerms/ReferenceEventCode/text()" />
            </cbc:ReferenceEventCode>
          </xsl:if>
          <xsl:if test="string-length(PaymentTerms/SettlementDiscountPercent/text()) != 0">
            <cbc:SettlementDiscountPercent>
              <xsl:value-of select="PaymentTerms/SettlementDiscountPercent/text()" />
            </cbc:SettlementDiscountPercent>
          </xsl:if>
          <xsl:if test="string-length(PaymentTerms/PenaltySurchargePercent/text()) != 0">
            <cbc:PenaltySurchargePercent>
              <xsl:value-of select="PaymentTerms/PenaltySurchargePercent/text()" />
            </cbc:PenaltySurchargePercent>
          </xsl:if>
          <xsl:if test="string-length(PaymentTerms/Amount/text()) != 0">
            <cbc:Amount>
                <xsl:attribute name="currencyID">
                  <xsl:value-of select="$CurrencyCode" />
                </xsl:attribute>
                <xsl:value-of select="PaymentTerms/Amount/text()" />
            </cbc:Amount>
          </xsl:if>
          <cac:SettlementPeriod>
            <xsl:if test="string-length(PaymentTerms/SettlementPeriod/StartDate/text()) != 0">
              <cbc:StartDate>
                <xsl:value-of select="PaymentTerms/SettlementPeriod/StartDate/text()" />
              </cbc:StartDate>
            </xsl:if>
            <xsl:if test="string-length(PaymentTerms/SettlementPeriod/EndDate/text()) != 0">
              <cbc:EndDate>
                <xsl:value-of select="PaymentTerms/SettlementPeriod/EndDate/text()" />
              </cbc:EndDate>
            </xsl:if>
            <xsl:if test="string-length(PaymentTerms/SettlementPeriod/Description/text()) != 0">
              <cbc:Description>
                <xsl:value-of select="PaymentTerms/SettlementPeriod/Description/text()" />
              </cbc:Description>
            </xsl:if>
          </cac:SettlementPeriod>
          <cac:PenaltyPeriod>
            <xsl:if test="string-length(PaymentTerms/PenaltyPeriod/StartDate/text()) != 0">
              <cbc:StartDate>
                <xsl:value-of select="PaymentTerms/PenaltyPeriod/StartDate/text()" />
              </cbc:StartDate>
            </xsl:if>
            <xsl:if test="string-length(PaymentTerms/PenaltyPeriod/EndDate/text()) != 0">
              <cbc:EndDate>
                <xsl:value-of select="PaymentTerms/PenaltyPeriod/EndDate/text()" />
              </cbc:EndDate>
            </xsl:if>
            <xsl:if test="string-length(PaymentTerms/PenaltyPeriod/Description/text()) != 0">
              <cbc:Description>
                <xsl:value-of select="PaymentTerms/PenaltyPeriod/Description/text()" />
              </cbc:Description>
            </xsl:if>
          </cac:PenaltyPeriod>
        </cac:PaymentTerms>
      </xsl:if>  
      <cac:TaxTotal>
        <cbc:TaxAmount> 
          <xsl:attribute name="currencyID">
            <xsl:value-of select="$CurrencyCode" />
          </xsl:attribute>
          <xsl:if test="string-length(TaxTotal/TaxAmount/text()) = 0">
            <xsl:value-of select="'0.00'" />
          </xsl:if>
          <xsl:if test="string-length(TaxTotal/TaxAmount/text()) != 0">
            <xsl:value-of select="TaxTotal/TaxAmount/text()" />
          </xsl:if>  
        </cbc:TaxAmount>
        <!-- TaxTotal/TaxSubtotal - Only map if present -->
        <xsl:for-each select="TaxTotal/TaxSubtotal">
          <cac:TaxSubtotal>
            <cbc:TaxableAmount>
              <xsl:attribute name="currencyID">
                <xsl:value-of select="$CurrencyCode" />
              </xsl:attribute>
              <xsl:if test="string-length(TaxableAmount/text()) = 0">
                <xsl:value-of select="'0.00'" />
              </xsl:if>
              <xsl:if test="string-length(TaxableAmount/text()) != 0">
                <xsl:value-of select="TaxableAmount/text()" />
              </xsl:if>  
            </cbc:TaxableAmount>
            <cbc:TaxAmount>
              <xsl:attribute name="currencyID">
                <xsl:value-of select="$CurrencyCode" />
              </xsl:attribute>
              <xsl:if test="string-length(TaxAmount/text()) = 0">
                <xsl:value-of select="'0.00'" />
              </xsl:if>
              <xsl:if test="string-length(TaxAmount/text()) != 0">
                <xsl:value-of select="TaxAmount/text()" />
              </xsl:if>  
            </cbc:TaxAmount>
            <!-- xsl:if test="string-length(TaxPercent/text()) != 0" ... not allowed in NemHandel>
              <cbc:Percent>
                <xsl:value-of select="TaxPercent/text()" />
              </cbc:Percent>
            </xsl:if -->
            <!-- TaxCategory - Only map if present -->
            <xsl:for-each select="TaxCategory">
              <cac:TaxCategory>
                <cbc:ID>
                  <xsl:attribute name="schemeAgencyID">
                    <xsl:value-of select="$Text320" />
                  </xsl:attribute>
                  <xsl:attribute name="schemeID">
                    <xsl:value-of select="'urn:oioubl:id:taxcategoryid-1.1'" />
                  </xsl:attribute>
                  <xsl:value-of select="ID/text()" />
                </cbc:ID>
                <cbc:Percent>
                  <xsl:if test="string-length(Percent/text()) = 0">
                    <xsl:value-of select="'0.0000'" />
                  </xsl:if>
                  <xsl:if test="string-length(Percent/text()) != 0">
                    <xsl:value-of select="Percent/text()" />
                  </xsl:if>  
                </cbc:Percent>                
                <cac:TaxScheme>
                  <cbc:ID>
                    <xsl:attribute name="schemeAgencyID">
                      <xsl:value-of select="$Text320" />
                    </xsl:attribute>
                    <xsl:attribute name="schemeID">
                      <xsl:value-of select="'urn:oioubl:id:taxschemeid-1.1'" />
                    </xsl:attribute>
                    <xsl:if test="string-length(TaxScheme/ID/text()) = 0"> 
                      <xsl:value-of select="'63'" />
                    </xsl:if>  
                    <xsl:if test="string-length(TaxScheme/ID/text()) != 0"> 
                      <xsl:value-of select="TaxScheme/ID/text()" />
                    </xsl:if>  
                  </cbc:ID>
                  <!-- If no Name use TaxTypeCode -->
                  <xsl:if test="string-length(TaxScheme/Navn/text()) != 0">
                    <cbc:Name>
                      <xsl:value-of select="TaxScheme/Navn/text()" />
                    </cbc:Name>
                  </xsl:if>
                  <xsl:if test="string-length(TaxScheme/Navn/text()) = 0">
                    <cbc:Name>
                      <!-- xsl:value-of select="TaxScheme/TaxTypeCode/text()" / -->
                      <xsl:value-of select="'Moms'" />
                    </cbc:Name>
                  </xsl:if>
                </cac:TaxScheme>
              </cac:TaxCategory>
            </xsl:for-each>
          </cac:TaxSubtotal>
        </xsl:for-each>
      </cac:TaxTotal>
      <!-- LegalMonetaryTotal -->
      <cac:LegalMonetaryTotal>
        <cbc:LineExtensionAmount>
          <xsl:attribute name="currencyID">
            <xsl:value-of select="$CurrencyCode" />
          </xsl:attribute>
          <xsl:if test="string-length(Total/LineTotalAmount/text()) = 0">
            <xsl:value-of select="'0.00'" />
          </xsl:if> 
          <xsl:if test="string-length(Total/LineTotalAmount/text()) != 0">
            <xsl:value-of select="Total/LineTotalAmount/text()" />
          </xsl:if>  
        </cbc:LineExtensionAmount>
        <xsl:if test="string-length(Total/TaxExclAmount/text()) != 0">
          <cbc:TaxExclusiveAmount>
              <xsl:attribute name="currencyID">
                <xsl:value-of select="$CurrencyCode" />
              </xsl:attribute>
              <xsl:value-of select="Total/TaxExclAmount/text()" />
          </cbc:TaxExclusiveAmount>
        </xsl:if>
        <xsl:if test="string-length(Total/TaxInclAmount/text()) != 0">
          <cbc:TaxInclusiveAmount>
            <xsl:attribute name="currencyID">
              <xsl:value-of select="$CurrencyCode" />
            </xsl:attribute>
            <xsl:value-of select="Total/TaxInclAmount/text()" />
          </cbc:TaxInclusiveAmount>
        </xsl:if>
        <xsl:if test="string-length(Total/AllowanceTotalAmount/text()) != 0">
          <cbc:AllowanceTotalAmount>
            <xsl:attribute name="currencyID">
              <xsl:value-of select="$CurrencyCode" />
            </xsl:attribute>
            <xsl:value-of select="Total/AllowanceTotalAmount/text()" />
          </cbc:AllowanceTotalAmount>
        </xsl:if>
        <xsl:if test="string-length(Total/ChargeTotalAmount/text()) != 0"> 
          <cbc:ChargeTotalAmount>
            <xsl:attribute name="currencyID">
              <xsl:value-of select="$CurrencyCode" />
            </xsl:attribute>
            <xsl:value-of select="Total/ChargeTotalAmount/text()" />
          </cbc:ChargeTotalAmount>
        </xsl:if> 
        <xsl:if test="string-length(Total/PrepaidAmount/text()) != 0">
          <cbc:PrepaidAmount> 
            <xsl:attribute name="currencyID">
              <xsl:value-of select="$CurrencyCode" />
            </xsl:attribute>
            <xsl:value-of select="Total/PrepaidAmount/text()" />
          </cbc:PrepaidAmount>
        </xsl:if>
        <xsl:if test="string-length(Total/PayableRoundingAmount/text()) != 0">
          <cbc:PayableRoundingAmount>
            <xsl:attribute name="currencyID">
              <xsl:value-of select="$CurrencyCode" />
            </xsl:attribute>
            <xsl:value-of select="Total/PayableRoundingAmount/text()" />
          </cbc:PayableRoundingAmount>
        </xsl:if>  
        <cbc:PayableAmount>
          <xsl:attribute name="currencyID">
            <xsl:value-of select="$CurrencyCode" />
          </xsl:attribute>
          <xsl:if test="string-length(Total/PayableAmount/text()) = 0">
            <xsl:value-of select="'0.00'" />
          </xsl:if>
          <xsl:if test="string-length(Total/PayableAmount/text()) != 0">
            <xsl:value-of select="Total/PayableAmount/text()" />
          </xsl:if>  
        </cbc:PayableAmount>
      </cac:LegalMonetaryTotal>
      <!-- Here comes the invoice lines .........................................................................-->
      <!-- Lines/Line - Only map if present -->
      <xsl:for-each select="Lines/Line">
        <cac:InvoiceLine>
          <cbc:ID>
            <xsl:value-of select="LineNo/text()" />
          </cbc:ID>
          <xsl:if test="string-length(Note/text()) != 0">
            <cbc:Note>
              <xsl:value-of select="Note/text()" />
            </cbc:Note>
          </xsl:if>
          <cbc:InvoicedQuantity>
            <xsl:attribute name="unitCode">
              <xsl:choose>
                <xsl:when test="UnitCode = ''">
                  <xsl:value-of select="'EA'" />
                </xsl:when>
                <xsl:when test="UnitCode = 'PCE'">
                  <xsl:value-of select="'EA'" />
                </xsl:when>
                <xsl:when test="UnitCode = 'STK'">
                  <xsl:value-of select="'EA'" />
                </xsl:when>
                <xsl:when test="UnitCode = 'SÆT'">
                  <xsl:value-of select="'SET'" />
                </xsl:when>
                <xsl:otherwise>
                   <xsl:value-of select="UnitCode/text()" />
                </xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>  
            <xsl:value-of select="Quantity/text()" />
          </cbc:InvoicedQuantity>
          <cbc:LineExtensionAmount>
            <xsl:attribute name="currencyID">
              <xsl:value-of select="$CurrencyCode" />
            </xsl:attribute>
            <xsl:value-of select="LineAmountTotal/text()" />
          </cbc:LineExtensionAmount>
          <xsl:if test="string-length(FreeOfChargeIndicator/text()) != 0">
            <cbc:FreeOfChargeIndicator>
              <xsl:value-of select="FreeOfChargeIndicator/text()" />
            </cbc:FreeOfChargeIndicator>
          </xsl:if>
          <xsl:if test="string-length(OrderLineReference/OrderReference/ID/text()) != 0">
            <cac:OrderLineReference>
              <xsl:if test="OrderLineReference/LineID!=''">
                <cbc:LineID>
                  <xsl:value-of select="OrderLineReference/LineID/text()" />
                </cbc:LineID>
              </xsl:if>  
              <cac:OrderReference>
                <cbc:ID>
                  <xsl:value-of select="OrderLineReference/OrderReference/ID/text()" />
                </cbc:ID>
              </cac:OrderReference>
              <xsl:if test="string-length(OrderLineReference/SalesOrderLineID/text()) != 0">
                <cbc:SalesOrderLineID>
                  <xsl:value-of select="OrderLineReference/SalesOrderLineID/text()" />
                </cbc:SalesOrderLineID>
              </xsl:if>
            </cac:OrderLineReference>
          </xsl:if> 
          <!-- PricingReference -->
          <cac:PricingReference>
            <cac:AlternativeConditionPrice>
              <cbc:PriceAmount>
                <xsl:attribute name="currencyID">
                  <xsl:value-of select="$CurrencyCode" />
                </xsl:attribute>
                <xsl:if test="string-length(PriceGross/Price/text()) = 0">
                  <xsl:value-of select="'1.00'" />
                </xsl:if>
                <xsl:if test="string-length(PriceGross/Price/text()) != 0">
                  <xsl:value-of select="PriceGross/Price/text()" />
                </xsl:if>
              </cbc:PriceAmount>
              <cbc:BaseQuantity>
                <xsl:attribute name="unitCode">
                  <xsl:choose>
                    <xsl:when test="string-length(PriceGross/UnitCode/text()) = 0">
                      <xsl:value-of select="'EA'" />
                    </xsl:when>
                    <xsl:when test="PriceGross/UnitCode/text() = 'PCE'">
                      <xsl:value-of select="'EA'" />
                    </xsl:when>
                    <xsl:when test="PriceGross/UnitCode/text() = 'STK'">
                      <xsl:value-of select="'EA'" />
                    </xsl:when>
                    <xsl:when test="PriceGross/UnitCode/text() = 'SÆT'">
                      <xsl:value-of select="'SET'" />
                    </xsl:when>                    
                    <xsl:otherwise>
                       <xsl:value-of select="PriceGross/UnitCode/text()" />
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute> 
                <xsl:if test="string-length(PriceGross/Quantity/text()) = 0">
                  <xsl:value-of select="'1.000'" />
                </xsl:if>
                <xsl:if test="string-length(PriceGross/Quantity/text()) != 0">
                  <xsl:value-of select="PriceGross/Quantity/text()" />
                </xsl:if>
              </cbc:BaseQuantity>
              <xsl:if test="string-length(PriceGross/PristypeCode/text()) != 0">
                <cbc:PriceTypeCode>
                  <xsl:value-of select="PriceGross/PristypeCode/text()" />
                </cbc:PriceTypeCode>
              </xsl:if>
              <xsl:if test="string-length(PriceGross/PristypeTekst/text()) != 0">
                <cbc:PriceType>
                  <xsl:value-of select="PriceGross/PristypeTekst/text()" />
                </cbc:PriceType>
              </xsl:if>
            </cac:AlternativeConditionPrice>
          </cac:PricingReference>
          <!-- Delivery -->
          <!-- MMA - 22.02,2023 - Commented out because not supported by NemHandel
          <xsl:if test="string-length(Delivery/Quantity/text()) != 0">
            <cac:Delivery>
              <xsl:if test="string-length(Delivery/ID/text()) != 0">
                <cac:ID>
                  <xsl:value-of select="Delivery/ID/text()" />
                </cac:ID>
              </xsl:if>  
              <cac:Quantity>
                <xsl:attribute name="unitCode">
                  <xsl:choose>
                    <xsl:when test="string-length(Delivery/UnitCode/text()) = 0">
                      <xsl:value-of select="'EA'" />
                    </xsl:when>
                    <xsl:when test="Delivery/UnitCode/text() = 'PCE'">
                      <xsl:value-of select="'EA'" />
                    </xsl:when>
                    <xsl:otherwise>
                       <xsl:value-of select="Delivery/UnitCode/text()" />
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute> 
                <xsl:value-of select="Delivery/Quantity/text()" />
              </cac:Quantity>
            </cac:Delivery>             
          </xsl:if>  
          -->
          <!-- AllowanceCharge -->
          <xsl:if test="string-length(AllowanceCharge/ChargeIndicator/text()) != 0 and string-length(AllowanceCharge/Amount/text()) != 0 and AllowanceCharge/Amount/text() != '0' and AllowanceCharge/Amount/text() != '0.00'">
            <cac:AllowanceCharge>
              <cbc:ID>
                <xsl:if test="string-length(AllowanceCharge/ID/text()) != 0">
                  <xsl:value-of select="AllowanceCharge/ID/text()" />
                </xsl:if>
              </cbc:ID>
              <cbc:ChargeIndicator>
                <xsl:value-of select="AllowanceCharge/ChargeIndicator/text()" />
              </cbc:ChargeIndicator>
              <xsl:if test="string-length(AllowanceCharge/AllowanceChargeReasonCode/text()) != 0">
                <cbc:AllowanceChargeReasonCode>
                  <xsl:value-of select="AllowanceCharge/AllowanceChargeReasonCode/text()" />
                </cbc:AllowanceChargeReasonCode>
              </xsl:if>
              <xsl:if test="string-length(AllowanceCharge/AllowanceChargeReason/text()) != 0">
                <cbc:AllowanceChargeReason>
                  <xsl:value-of select="AllowanceCharge/AllowanceChargeReason/text()" />
                </cbc:AllowanceChargeReason>
              </xsl:if>
              <xsl:if test="string-length(AllowanceCharge/MultiplierFactorNumeric/text()) != 0">
                <cbc:MultiplierFactorNumeric>
                  <xsl:value-of select="format-number(round(number(AllowanceCharge/MultiplierFactorNumeric)*100)*10000 div 10000, '0.0000')" />
                  <!-- <xsl:value-of select="format-number(number(MultiplierFactorNumeric)*100, '0.##')" /> -->
                <!--  <xsl:value-of select="AllowanceCharge/MultiplierFactorNumeric/text()" /> -->
                </cbc:MultiplierFactorNumeric>
              </xsl:if>
              <xsl:if test="string-length(AllowanceCharge/PrepaidIndicator/text()) != 0">
                <cbc:PrepaidIndicator>
                  <xsl:value-of select="AllowanceCharge/PrepaidIndicator/text()" />
                </cbc:PrepaidIndicator>
              </xsl:if>
              <xsl:if test="string-length(AllowanceCharge/SequenceNumeric/text()) != 0">
                <cbc:SequenceNumeric>
                  <xsl:value-of select="AllowanceCharge/SequenceNumeric/text()" />
                </cbc:SequenceNumeric>
              </xsl:if>
              <xsl:if test="string-length(AllowanceCharge/Amount/text()) != 0 and AllowanceCharge/Amount/text() != '0' and AllowanceCharge/Amount/text() != '0.00'">
                <cbc:Amount>
                  <xsl:attribute name="currencyID">
                    <xsl:value-of select="$CurrencyCode" />
                  </xsl:attribute>
                  <xsl:value-of select="AllowanceCharge/Amount/text()" />
                </cbc:Amount>
              </xsl:if>
              <xsl:if test="string-length(AllowanceCharge/BaseAmount/text()) != 0">
                <cbc:BaseAmount>
                  <xsl:attribute name="currencyID">
                    <xsl:value-of select="$CurrencyCode" />
                  </xsl:attribute>
                  <xsl:value-of select="AllowanceCharge/BaseAmount/text()" />
                </cbc:BaseAmount>
              </xsl:if>  
              <!-- SJ 12.09.2019 - default values added -->
              <cac:TaxCategory>
                <cbc:ID>
                  <xsl:attribute name="schemeAgencyID">
                    <xsl:value-of select="$Text320" />
                  </xsl:attribute>
                  <xsl:attribute name="schemeID">
                    <xsl:value-of select="'urn:oioubl:id:taxcategoryid-1.1'" />
                  </xsl:attribute>
                  <xsl:if test="string-length(AllowanceCharge/TaxCategory/ID/text()) = 0">
                    <xsl:value-of select="'StandardRated'" />
                  </xsl:if>
                  <xsl:if test="string-length(AllowanceCharge/TaxCategory/ID/text()) != 0">
                    <xsl:value-of select="AllowanceCharge/TaxCategory/ID/text()" />
                  </xsl:if>
                </cbc:ID>                
                  <xsl:if test="string-length(AllowanceCharge/TaxCategory/Percent/text()) = 0">               
                      <xsl:if test ="string-length(//TaxTotal/TaxSubtotal/TaxCategory/Percent/text()) != 0">
                        <cbc:Percent>
                          <xsl:value-of select="//TaxTotal/TaxSubtotal/TaxCategory/Percent/text()"/>
                        </cbc:Percent>
                      </xsl:if>
                      <xsl:if test ="string-length(//TaxTotal/TaxSubtotal/TaxCategory/Percent/text()) = 0">
                        <cbc:Percent>
                          <xsl:text>0.0000</xsl:text>
                        </cbc:Percent>
                      </xsl:if>                                                            
                  </xsl:if>
                  <xsl:if test="string-length(AllowanceCharge/TaxCategory/Percent/text()) != 0">
                    <cbc:Percent>
                     <xsl:value-of select="AllowanceCharge/TaxCategory/Percent/text()" />
                    </cbc:Percent>                      
                    </xsl:if>  
               
                <cac:TaxScheme>
                  <cbc:ID>
                    <xsl:attribute name="schemeAgencyID">
                      <xsl:value-of select="$Text320" />
                    </xsl:attribute>
                    <xsl:attribute name="schemeID">
                      <xsl:value-of select="'urn:oioubl:id:taxschemeid-1.1'" />
                    </xsl:attribute>
                    <xsl:if test="string-length(AllowanceCharge/TaxCategory/TaxScheme/ID/text()) = 0">
                      <xsl:value-of select="'63'" />
                    </xsl:if>
                    <xsl:if test="string-length(AllowanceCharge/TaxCategory/TaxScheme/ID/text()) != 0">
                      <xsl:value-of select="AllowanceCharge/TaxCategory/TaxScheme/ID/text()" />
                    </xsl:if>
                  </cbc:ID>
                  <cbc:Name>
                    <xsl:if test="string-length(AllowanceCharge/TaxCategory/TaxScheme/Navn/text()) = 0">
                      <xsl:value-of select="'Moms'" />
                    </xsl:if>
                    <xsl:if test="string-length(AllowanceCharge/TaxCategory/TaxScheme/Navn/text()) != 0">
                      <xsl:value-of select="AllowanceCharge/TaxCategory/TaxScheme/Navn/text()" />
                    </xsl:if>
                  </cbc:Name>    
                </cac:TaxScheme>
              </cac:TaxCategory>
            </cac:AllowanceCharge>
          </xsl:if>  
          <cac:TaxTotal>
            <cbc:TaxAmount>
                <xsl:attribute name="currencyID">
                  <xsl:value-of select="$CurrencyCode" />
                </xsl:attribute>
                <xsl:if test="string-length(TaxTotal/TaxAmount/text()) = 0"> 
                  <xsl:value-of select="'0.00'" />
                </xsl:if>
                <xsl:if test="string-length(TaxTotal/TaxAmount/text()) != 0">           
                  <xsl:value-of select="TaxTotal/TaxAmount/text()" />
                </xsl:if>  
            </cbc:TaxAmount>
            <cac:TaxSubtotal>
              <cbc:TaxableAmount>
                <xsl:attribute name="currencyID">
                  <xsl:value-of select="$CurrencyCode" />
                </xsl:attribute>
                <xsl:if test="string-length(TaxTotal/TaxSubtotal/TaxableAmount/text()) = 0">
                  <xsl:value-of select="'0.00'" />
                </xsl:if>
                <xsl:if test="string-length(TaxTotal/TaxSubtotal/TaxableAmount/text()) != 0">
                  <xsl:value-of select="TaxTotal/TaxSubtotal/TaxableAmount/text()" />
                </xsl:if>  
              </cbc:TaxableAmount>
              <cbc:TaxAmount>
                <xsl:attribute name="currencyID">
                  <xsl:value-of select="$CurrencyCode" />
                </xsl:attribute>
                <xsl:if test="string-length(TaxTotal/TaxSubtotal/TaxAmount/text()) = 0">
                  <xsl:value-of select="'0.00'" />
                </xsl:if>
                <xsl:if test="string-length(TaxTotal/TaxSubtotal/TaxAmount/text()) != 0">
                  <xsl:value-of select="TaxTotal/TaxSubtotal/TaxAmount/text()" />
                </xsl:if>   
              </cbc:TaxAmount>
              <xsl:if test="string-length(TaxTotal/TaxSubtotal/TaxPercent/text()) != 0">
                <cbc:Percent>
                  <xsl:value-of select="TaxTotal/TaxSubtotal/TaxPercent/text()" />
                </cbc:Percent>
              </xsl:if>
              <cac:TaxCategory>
                <cbc:ID>
                  <xsl:attribute name="schemeAgencyID">
                    <xsl:value-of select="$Text320" />
                  </xsl:attribute>
                  <xsl:attribute name="schemeID">
                    <xsl:value-of select="'urn:oioubl:id:taxcategoryid-1.1'" />
                  </xsl:attribute>
                  <xsl:if test="string-length(TaxTotal/TaxSubtotal/TaxCategory/ID/text()) = 0">
                    <xsl:value-of select="'StandardRated'" />
                  </xsl:if>
                  <xsl:if test="string-length(TaxTotal/TaxSubtotal/TaxCategory/ID/text()) != 0">
                    <xsl:value-of select="TaxTotal/TaxSubtotal/TaxCategory/ID/text()" />
                  </xsl:if>
                </cbc:ID>
                <cbc:Percent>
                  <xsl:if test="string-length(TaxTotal/TaxSubtotal/TaxCategory/Percent/text()) = 0">
                    <xsl:value-of select="'25.0000'" />
                  </xsl:if>  
                  <xsl:if test="string-length(TaxTotal/TaxSubtotal/TaxCategory/Percent/text()) != 0">
                    <xsl:value-of select="TaxTotal/TaxSubtotal/TaxCategory/Percent/text()" />
                  </xsl:if>  
                </cbc:Percent>
                <cac:TaxScheme>
                  <cbc:ID>
                    <xsl:attribute name="schemeAgencyID">
                      <xsl:value-of select="$Text320" />
                    </xsl:attribute>
                    <xsl:attribute name="schemeID">
                      <xsl:value-of select="'urn:oioubl:id:taxschemeid-1.1'" />
                    </xsl:attribute>
                    <xsl:if test="string-length(TaxTotal/TaxSubtotal/TaxCategory/TaxScheme/ID/text()) = 0">
                      <xsl:value-of select="'63'" />
                    </xsl:if>
                    <xsl:if test="string-length(TaxTotal/TaxSubtotal/TaxCategory/TaxScheme/ID/text()) != 0">
                      <xsl:value-of select="TaxTotal/TaxSubtotal/TaxCategory/TaxScheme/ID/text()" />
                    </xsl:if>
                  </cbc:ID>
                  <xsl:if test="string-length(TaxTotal/TaxSubtotal/TaxCategory/TaxScheme/Navn/text()) = 0">
                    <cbc:Name>
                      <xsl:value-of select="'Moms'" />
                    </cbc:Name>
                  </xsl:if>
                  <xsl:if test="string-length(TaxTotal/TaxSubtotal/TaxCategory/TaxScheme/Navn/text()) != 0">
                    <cbc:Name>
                      <xsl:value-of select="TaxTotal/TaxSubtotal/TaxCategory/TaxScheme/Navn/text()" />
                    </cbc:Name>
                  </xsl:if>
                </cac:TaxScheme>
              </cac:TaxCategory>
            </cac:TaxSubtotal>
          </cac:TaxTotal>
          <cac:Item> 
             <xsl:if test="string-length(Item/Description/text()) != 0">
              <cbc:Description>
                <xsl:value-of select="Item/Description/text()" />
              </cbc:Description>
            </xsl:if>
            <xsl:if test="string-length(Item/Name/text()) = 0">
              <xsl:if test="string-length(Item/Description/text()) != 0">
                <cbc:Name>
                  <xsl:value-of select="Item/Description/text()" />
               </cbc:Name>
              </xsl:if>
            </xsl:if>       
            <xsl:if test="string-length(Item/Name/text()) != 0">
              <cbc:Name>
                <xsl:value-of select="Item/Name/text()" />
              </cbc:Name>
            </xsl:if>
            <xsl:if test="string-length(Item/BuyerItemID/text()) != 0">
              <cac:BuyersItemIdentification>
                <cbc:ID>            
                  <xsl:value-of select="Item/BuyerItemID/text()" />
                </cbc:ID>
              </cac:BuyersItemIdentification>
            </xsl:if>
            <xsl:if test="string-length(Item/SellerItemID/text()) != 0">
              <cac:SellersItemIdentification>
                <cbc:ID>
                  <xsl:value-of select="Item/SellerItemID/text()" />
                </cbc:ID>
              </cac:SellersItemIdentification>
            </xsl:if>  
            <xsl:if test="string-length(Item/StandardItemID/text()) != 0">
              <cac:StandardItemIdentification>
                <cbc:ID>
                  <xsl:attribute name="schemeID">
                    <xsl:value-of select="'EAN'" />
                  </xsl:attribute>
                  <xsl:value-of select="Item/StandardItemID/text()" />
                </cbc:ID>
              </cac:StandardItemIdentification>
            </xsl:if>
            <xsl:if test="string-length(Item/AdditionalItemID/text()) != 0">
              <cac:AdditionalItemIdentification>
                <cbc:ID>
                  <xsl:attribute name="schemeID">
                    <xsl:value-of select="'DB'" />
                  </xsl:attribute>
                  <xsl:value-of select="Item/AdditionalItemID/text()" />
                </cbc:ID>
              </cac:AdditionalItemIdentification>
            </xsl:if>
            <!-- MMA - 22.02,2023 - Commented out because not supported by NemHandel -->
            <!--<xsl:if test="string-length(Item/CatalogueItemID/text()) != 0">
              <cac:CatalogueItemIdentification>
                <cbc:ID>
                  <xsl:attribute name="schemeID">
                    <xsl:value-of select="'DB'" />
                  </xsl:attribute>
                  <xsl:value-of select="Item/CatalogueItemID/text()" />
                </cbc:ID>
              </cac:CatalogueItemIdentification>
            </xsl:if>-->  
          </cac:Item>
          <cac:Price>
            <cbc:PriceAmount>
              <xsl:attribute name="currencyID">
                <xsl:value-of select="$CurrencyCode" />
              </xsl:attribute>
              <xsl:if test="string-length(PriceNet/Price/text()) = 0">
                <xsl:value-of select="'1.00'" />
              </xsl:if>
              <xsl:if test="string-length(PriceNet/Price/text()) != 0">
                <xsl:value-of select="PriceNet/Price/text()" />
              </xsl:if>  
            </cbc:PriceAmount>
            <cbc:BaseQuantity>
              <xsl:attribute name="unitCode">
                <xsl:choose>
                  <xsl:when test="string-length(PriceNet/UnitCode/text()) = 0">
                    <xsl:value-of select="'EA'" />
                  </xsl:when>
                  <xsl:when test="PriceNet/UnitCode/text() ='PCE'">
                    <xsl:value-of select="'EA'" />
                  </xsl:when>
                  <xsl:when test="PriceNet/UnitCode/text() ='STK'">
                    <xsl:value-of select="'EA'" />
                  </xsl:when>
                  <xsl:when test="PriceNet/UnitCode/text() ='SÆT'">
                    <xsl:value-of select="'SET'" />
                  </xsl:when>
                  <xsl:otherwise>
                     <xsl:value-of select="PriceNet/UnitCode/text()" />
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:attribute> 
              <xsl:if test="string-length(PriceNet/Quantity/text()) = 0">
                <xsl:value-of select="'1.000'" />
              </xsl:if>
              <xsl:if test="string-length(PriceNet/Quantity/text()) != 0">
                <xsl:value-of select="PriceNet/Quantity/text()" />
              </xsl:if>            
            </cbc:BaseQuantity>
            <xsl:if test="string-length(PriceNet/PristypeCode/text()) != 0">
              <cbc:PriceTypeCode>
                <xsl:value-of select="PriceNet/PristypeCode/text()" />
              </cbc:PriceTypeCode>  
            </xsl:if>  
            <xsl:if test="string-length(PriceNet/PristypeTekst/text()) != 0">
              <cbc:PriceType>
                <xsl:value-of select="PriceNet/PristypeTekst/text()" />
              </cbc:PriceType>
            </xsl:if>
          </cac:Price>
        </cac:InvoiceLine>
      </xsl:for-each>
    </ns0:Invoice>
  </xsl:template>

<msxsl:script language="C#" implements-prefix="userCSharp">

  <!-- Used to be 
    <![CDATA[

  public string GetStreetNumber(string fullStreetName)
  {
      System.Text.RegularExpressions.Match match = System.Text.RegularExpressions.Regex.Match(fullStreetName, @"[0-9]");
      int indexOfFirstNumber = match.Index;
     if (!match.Success)
      {
          return "0";
      }
                  
      string streetName = fullStreetName.Substring(0, indexOfFirstNumber-1);
      string streetNumber = fullStreetName.Substring(indexOfFirstNumber);
      return streetNumber;
  }

  public string GetStreetName(string fullStreetName)
  {
      System.Text.RegularExpressions.Match match = System.Text.RegularExpressions.Regex.Match(fullStreetName, @"[0-9]");
      int indexOfFirstNumber = match.Index;
      if (!match.Success)
      {
          return fullStreetName;
      }
      if (indexOfFirstNumber < 1)
      {
          return fullStreetName;
      }
      string streetName = fullStreetName.Substring(0, indexOfFirstNumber-1);
      string streetNumber = fullStreetName.Substring(indexOfFirstNumber);
      return streetName;
  }
]]>
 
-->
  
  <!-- Ought to be if we consider that we can only have [BuildingNO StreetName] or [StreetName BuildingNO]. 01.04.2020 MMA. -->
  <![CDATA[

  public string GetStreetNumber(string fullStreetName)
  {
      System.Text.RegularExpressions.Match match = System.Text.RegularExpressions.Regex.Match(fullStreetName, @"[0-9]");
      System.Text.RegularExpressions.Match rmatch = System.Text.RegularExpressions.Regex.Match(fullStreetName, @"[0-9]", RegexOptions.RightToLeft);
      int indexOfFirstNumber = match.Index;
      int indexOfLastNumber = rmatch.Index;
     if (!match.Success)
      {
          return "0";
      }
      
      if (indexOfFirstNumber < 1)
      {
          string streetNum = fullStreetName.Substring(0, indexOfLastNumber+1);
          return streetNum;
      }    

      
      string streetName = fullStreetName.Substring(0, indexOfFirstNumber-1);
      string streetNumber = fullStreetName.Substring(indexOfFirstNumber);
      return streetNumber;
  }

  public string GetStreetName(string fullStreetName)
  {
      System.Text.RegularExpressions.Match match = System.Text.RegularExpressions.Regex.Match(fullStreetName, @"[0-9]");
      System.Text.RegularExpressions.Match rmatch = System.Text.RegularExpressions.Regex.Match(fullStreetName, @"[0-9]", RegexOptions.RightToLeft);
      int indexOfFirstNumber = match.Index;
      int indexOfLastNumber = rmatch.Index;
      
      if (!match.Success)
      {
          return fullStreetName;
      }
      if (indexOfFirstNumber < 1)
      {
          string streetNam = fullStreetName.Substring(indexOfLastNumber+1).Trim(' ');
         
           if (String.IsNullOrWhiteSpace(streetNam))
            {
             return  fullStreetName.Trim(' ');
            }      
           
          return streetNam;
      }
      string streetName = fullStreetName.Substring(0, indexOfFirstNumber-1);
      string streetNumber = fullStreetName.Substring(indexOfFirstNumber);
      return streetName;
  }
]]>

</msxsl:script>
</xsl:stylesheet>