namespace BYGE.Integration.UBLInvoice {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.UBLInvoice.UBLInvoice", typeof(global::BYGE.Integration.UBLInvoice.UBLInvoice))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.Core.Invoice.Invoice", typeof(global::BYGE.Integration.Core.Invoice.Invoice))]
    public sealed class UBLInvoice_to_CoreInvoice : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var s2 s1 s0 userCSharp"" version=""1.0"" xmlns:s2=""urn:oasis:names:specification:ubl:schema:xsd:Invoice-2"" xmlns:ns0=""http://byg-e.dk/schemas/v10"" xmlns:s1=""urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"" xmlns:s0=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" xmlns:userCSharp=""http://schemas.microsoft.com/BizTalk/2003/userCSharp"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/s2:Invoice"" />
  </xsl:template>
  <xsl:template match=""/s2:Invoice"">
    <xsl:variable name=""var:v1"" select=""userCSharp:LogicalEq(string(s1:AccountingSupplierParty/s1:Party/s0:EndpointID/text()) , &quot;DK15150033&quot;)"" />
    <xsl:variable name=""var:v3"" select=""userCSharp:LogicalEq(string(s1:AccountingCustomerParty/s1:Party/s0:EndpointID/text()) , &quot;5790002502347&quot;)"" />
    <xsl:variable name=""var:v5"" select=""string(s1:AccountingSupplierParty/s1:Party/s0:EndpointID/text())"" />
    <xsl:variable name=""var:v6"" select=""userCSharp:LogicalEq($var:v5 , &quot;DK15150033&quot;)"" />
    <xsl:variable name=""var:v8"" select=""string(s1:AccountingCustomerParty/s1:Party/s0:EndpointID/text())"" />
    <xsl:variable name=""var:v9"" select=""userCSharp:LogicalEq($var:v8 , &quot;5790002502347&quot;)"" />
    <xsl:variable name=""var:v21"" select=""string(s1:LegalMonetaryTotal/s0:PrepaidAmount/text())"" />
    <xsl:variable name=""var:v27"" select=""string(s1:LegalMonetaryTotal/s0:LineExtensionAmount/text())"" />
    <xsl:variable name=""var:v29"" select=""string(s1:TaxTotal/s1:TaxSubtotal/s1:TaxCategory/s0:Percent/text())"" />
    <xsl:variable name=""var:v30"" select=""string(s1:TaxTotal/s1:TaxSubtotal/s0:TaxAmount/text())"" />
    <xsl:variable name=""var:v47"" select=""string(s1:LegalMonetaryTotal/s0:ChargeTotalAmount/text())"" />
    <xsl:variable name=""var:v51"" select=""string(s1:LegalMonetaryTotal/s0:PayableAmount/text())"" />
    <ns0:Invoice>
      <ProfileID>
        <xsl:value-of select=""s0:ProfileID/text()"" />
      </ProfileID>
      <InvoiceID>
        <xsl:value-of select=""s0:ID/text()"" />
      </InvoiceID>
      <CopyIndicator>
        <xsl:value-of select=""s0:CopyIndicator/text()"" />
      </CopyIndicator>
      <UUID>
        <xsl:value-of select=""s0:UUID/text()"" />
      </UUID>
      <TestIndicator>
        <xsl:text>0</xsl:text>
      </TestIndicator>
      <xsl:if test=""string($var:v1)='true'"">
        <xsl:variable name=""var:v2"" select=""&quot;5790002429149&quot;"" />
        <UNB2Alt>
          <xsl:value-of select=""$var:v2"" />
        </UNB2Alt>
      </xsl:if>
      <xsl:if test=""string($var:v3)='true'"">
        <xsl:variable name=""var:v4"" select=""&quot;5790002305733&quot;"" />
        <UNB3Alt>
          <xsl:value-of select=""$var:v4"" />
        </UNB3Alt>
      </xsl:if>
      <xsl:if test=""string($var:v6)='true'"">
        <xsl:variable name=""var:v7"" select=""&quot;14&quot;"" />
        <UNB2AltQualifier>
          <xsl:value-of select=""$var:v7"" />
        </UNB2AltQualifier>
      </xsl:if>
      <xsl:if test=""string($var:v9)='true'"">
        <xsl:variable name=""var:v10"" select=""&quot;14&quot;"" />
        <UNB3AltQualifier>
          <xsl:value-of select=""$var:v10"" />
        </UNB3AltQualifier>
      </xsl:if>
      <IssueDate>
        <xsl:value-of select=""s0:IssueDate/text()"" />
      </IssueDate>
      <InvoiceType>
        <xsl:value-of select=""s0:InvoiceTypeCode/text()"" />
      </InvoiceType>
      <xsl:for-each select=""s0:Note"">
        <Note>
          <xsl:value-of select=""./text()"" />
        </Note>
      </xsl:for-each>
      <Currency>
        <xsl:value-of select=""s0:DocumentCurrencyCode/text()"" />
      </Currency>
      <OrderReference>
        <OrderID>
          <xsl:value-of select=""s1:OrderReference/s0:ID/text()"" />
        </OrderID>
        <SalesOrderID>
          <xsl:value-of select=""s1:OrderReference/s0:SalesOrderID/text()"" />
        </SalesOrderID>
        <OrderDate>
          <xsl:value-of select=""s1:OrderReference/s0:IssueDate/text()"" />
        </OrderDate>
        <CustomerReference>
          <xsl:value-of select=""s1:OrderReference/s0:CustomerReference/text()"" />
        </CustomerReference>
      </OrderReference>
      <DespatchDocumentReference>
        <ID>
          <xsl:value-of select=""s1:DespatchDocumentReference/s0:ID/text()"" />
        </ID>
      </DespatchDocumentReference>
      <AdditionalDocumentReference>
        <ID>
          <xsl:if test=""s1:AdditionalDocumentReference/s0:ID/@schemeID"">
            <xsl:attribute name=""schemeID"">
              <xsl:value-of select=""s1:AdditionalDocumentReference/s0:ID/@schemeID"" />
            </xsl:attribute>
          </xsl:if>
          <xsl:value-of select=""s1:AdditionalDocumentReference/s0:ID/text()"" />
        </ID>
        <DocumentTypeCode>
          <xsl:value-of select=""s1:AdditionalDocumentReference/s0:DocumentTypeCode/text()"" />
        </DocumentTypeCode>
        <Attachment>
          <EmbeddedDocumentBinaryObject>
            <xsl:if test=""s1:AdditionalDocumentReference/s1:Attachment/s0:EmbeddedDocumentBinaryObject/@mimeCode"">
              <xsl:attribute name=""mimeCode"">
                <xsl:value-of select=""s1:AdditionalDocumentReference/s1:Attachment/s0:EmbeddedDocumentBinaryObject/@mimeCode"" />
              </xsl:attribute>
            </xsl:if>
            <xsl:if test=""s1:AdditionalDocumentReference/s1:Attachment/s0:EmbeddedDocumentBinaryObject/@filename"">
              <xsl:attribute name=""filename"">
                <xsl:value-of select=""s1:AdditionalDocumentReference/s1:Attachment/s0:EmbeddedDocumentBinaryObject/@filename"" />
              </xsl:attribute>
            </xsl:if>
            <xsl:value-of select=""s1:AdditionalDocumentReference/s1:Attachment/s0:EmbeddedDocumentBinaryObject/text()"" />
          </EmbeddedDocumentBinaryObject>
          <ExternalReference>
            <URI>
              <xsl:value-of select=""s1:AdditionalDocumentReference/s1:ExternalReference/s0:URI/text()"" />
            </URI>
          </ExternalReference>
        </Attachment>
      </AdditionalDocumentReference>
      <AccountingSupplier>
        <AccSellerID>
          <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s0:EndpointID/text()"" />
        </AccSellerID>
        <PartyID>
          <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PartyIdentification/s0:ID/text()"" />
        </PartyID>
        <CompanyID>
          <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PartyLegalEntity/s0:CompanyID/text()"" />
        </CompanyID>
        <CompanyTaxID>
          <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PartyTaxScheme/s0:CompanyID/text()"" />
        </CompanyTaxID>
        <SchemeID>
          <Endpoint>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s0:EndpointID/@schemeID"" />
          </Endpoint>
          <xsl:if test=""s1:AccountingSupplierParty/s1:Party/s1:PartyIdentification/s0:ID/@schemeID"">
            <Party>
              <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PartyIdentification/s0:ID/@schemeID"" />
            </Party>
          </xsl:if>
          <xsl:if test=""s1:AccountingSupplierParty/s1:Party/s1:PartyLegalEntity/s0:CompanyID/@schemeID"">
            <Company>
              <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PartyLegalEntity/s0:CompanyID/@schemeID"" />
            </Company>
          </xsl:if>
          <xsl:if test=""s1:AccountingSupplierParty/s1:Party/s1:PartyTaxScheme/s0:CompanyID/@schemeID"">
            <CompanyTax>
              <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PartyTaxScheme/s0:CompanyID/@schemeID"" />
            </CompanyTax>
          </xsl:if>
        </SchemeID>
        <Address>
          <Name>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PartyName/s0:Name/text()"" />
          </Name>
          <Postbox>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PostalAddress/s0:Postbox/text()"" />
          </Postbox>
          <AddressFormatCode>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PostalAddress/s0:AddressFormatCode/text()"" />
          </AddressFormatCode>
          <Street>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PostalAddress/s0:StreetName/text()"" />
          </Street>
          <Number>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PostalAddress/s0:BuildingNumber/text()"" />
          </Number>
          <AdditionalStreetName>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PostalAddress/s0:AdditionalStreetName/text()"" />
          </AdditionalStreetName>
          <Department>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PostalAddress/s0:Department/text()"" />
          </Department>
          <City>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PostalAddress/s0:CityName/text()"" />
          </City>
          <PostalCode>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PostalAddress/s0:PostalZone/text()"" />
          </PostalCode>
          <Country>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:PostalAddress/s1:Country/s0:IdentificationCode/text()"" />
          </Country>
        </Address>
        <Concact>
          <ID>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:Contact/s0:ID/text()"" />
          </ID>
          <Name>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:Contact/s0:Name/text()"" />
          </Name>
          <Telephone>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:Contact/s0:Telephone/text()"" />
          </Telephone>
          <Telefax>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:Contact/s0:Telefax/text()"" />
          </Telefax>
          <E-mail>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:Contact/s0:ElectronicMail/text()"" />
          </E-mail>
          <Note>
            <xsl:value-of select=""s1:AccountingSupplierParty/s1:Party/s1:Contact/s0:Note/text()"" />
          </Note>
        </Concact>
      </AccountingSupplier>
      <AccountingCustomer>
        <SupplierAssignedAccountID>
          <xsl:value-of select=""s1:AccountingCustomerParty/s0:SupplierAssignedAccountID/text()"" />
        </SupplierAssignedAccountID>
        <AccBuyerID>
          <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s0:EndpointID/text()"" />
        </AccBuyerID>
        <PartyID>
          <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PartyIdentification/s0:ID/text()"" />
        </PartyID>
        <CompanyID>
          <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PartyLegalEntity/s0:CompanyID/text()"" />
        </CompanyID>
        <CompanyTaxID>
          <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PartyTaxScheme/s0:CompanyID/text()"" />
        </CompanyTaxID>
        <SchemeID>
          <Endpoint>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s0:EndpointID/@schemeID"" />
          </Endpoint>
          <xsl:if test=""s1:AccountingCustomerParty/s1:Party/s1:PartyIdentification/s0:ID/@schemeID"">
            <Party>
              <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PartyIdentification/s0:ID/@schemeID"" />
            </Party>
          </xsl:if>
          <xsl:if test=""s1:AccountingCustomerParty/s1:Party/s1:PartyLegalEntity/s0:CompanyID/@schemeID"">
            <Company>
              <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PartyLegalEntity/s0:CompanyID/@schemeID"" />
            </Company>
          </xsl:if>
        </SchemeID>
        <Address>
          <Name>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PartyName/s0:Name/text()"" />
          </Name>
          <AddressFormatCode>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PostalAddress/s0:AddressFormatCode/text()"" />
          </AddressFormatCode>
          <Postbox>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PostalAddress/s0:Postbox/text()"" />
          </Postbox>
          <Street>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PostalAddress/s0:StreetName/text()"" />
          </Street>
          <Number>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PostalAddress/s0:BuildingNumber/text()"" />
          </Number>
          <AdditionalStreetName>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PostalAddress/s0:AdditionalStreetName/text()"" />
          </AdditionalStreetName>
          <Department>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PostalAddress/s0:Department/text()"" />
          </Department>
          <City>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PostalAddress/s0:CityName/text()"" />
          </City>
          <PostalCode>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PostalAddress/s0:PostalZone/text()"" />
          </PostalCode>
          <Country>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:PostalAddress/s1:Country/s0:IdentificationCode/text()"" />
          </Country>
        </Address>
        <Concact>
          <ID>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:Contact/s0:ID/text()"" />
          </ID>
          <Name>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:Contact/s0:Name/text()"" />
          </Name>
          <Telephone>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:Contact/s0:Telephone/text()"" />
          </Telephone>
          <Telefax>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:Contact/s0:Telefax/text()"" />
          </Telefax>
          <E-mail>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:Contact/s0:ElectronicMail/text()"" />
          </E-mail>
          <Note>
            <xsl:value-of select=""s1:AccountingCustomerParty/s1:Party/s1:Contact/s0:Note/text()"" />
          </Note>
        </Concact>
      </AccountingCustomer>
      <BuyerCustomer>
        <BuyerID>
          <xsl:value-of select=""s1:BuyerCustomerParty/s1:Party/s0:EndpointID/text()"" />
        </BuyerID>
        <PartyID>
          <xsl:value-of select=""s1:BuyerCustomerParty/s1:Party/s1:PartyIdentification/s0:ID/text()"" />
        </PartyID>
        <CompanyID>
          <xsl:value-of select=""s1:BuyerCustomerParty/s1:Party/s1:PartyLegalEntity/s0:CompanyID/text()"" />
        </CompanyID>
        <CompanyTaxID>
          <xsl:value-of select=""s1:BuyerCustomerParty/s1:Party/s1:PartyTaxScheme/s0:CompanyID/text()"" />
        </CompanyTaxID>
        <SchemeID>
          <Endpoint>
            <xsl:value-of select=""s1:BuyerCustomerParty/s1:Party/s0:EndpointID/@schemeID"" />
          </Endpoint>
          <xsl:if test=""s1:BuyerCustomerParty/s1:Party/s1:PartyIdentification/s0:ID/@schemeID"">
            <Party>
              <xsl:value-of select=""s1:BuyerCustomerParty/s1:Party/s1:PartyIdentification/s0:ID/@schemeID"" />
            </Party>
          </xsl:if>
          <xsl:if test=""s1:BuyerCustomerParty/s1:Party/s1:PartyLegalEntity/s0:CompanyID/@schemeID"">
            <Company>
              <xsl:value-of select=""s1:BuyerCustomerParty/s1:Party/s1:PartyLegalEntity/s0:CompanyID/@schemeID"" />
            </Company>
          </xsl:if>
        </SchemeID>
        <Address>
          <Name>
            <xsl:value-of select=""s1:BuyerCustomerParty/s1:Party/s1:PartyName/s0:Name/text()"" />
          </Name>
          <AddressFormatCode>
            <xsl:value-of select=""s1:BuyerCustomerParty/s1:Party/s1:PostalAddress/s0:AddressFormatCode/text()"" />
          </AddressFormatCode>
          <Postbox>
            <xsl:value-of select=""s1:BuyerCustomerParty/s1:Party/s1:PostalAddress/s0:Postbox/text()"" />
          </Postbox>
          <Street>
            <xsl:value-of select=""s1:BuyerCustomerParty/s1:Party/s1:PostalAddress/s0:StreetName/text()"" />
          </Street>
          <Number>
            <xsl:value-of select=""s1:BuyerCustomerParty/s1:Party/s1:PostalAddress/s0:BuildingNumber/text()"" />
          </Number>
          <AdditionalStreetName>
            <xsl:value-of select=""s1:BuyerCustomerParty/s1:Party/s1:PostalAddress/s0:AdditionalStreetName/text()"" />
          </AdditionalStreetName>
          <Department>
            <xsl:value-of select=""s1:BuyerCustomerParty/s1:Party/s1:PostalAddress/s0:Department/text()"" />
          </Department>
          <City>
            <xsl:value-of select=""s1:BuyerCustomerParty/s1:Party/s1:PostalAddress/s0:CityName/text()"" />
          </City>
          <PostalCode>
            <xsl:value-of select=""s1:BuyerCustomerParty/s1:Party/s1:PostalAddress/s0:PostalZone/text()"" />
          </PostalCode>
          <Country>
            <xsl:value-of select=""s1:BuyerCustomerParty/s1:Party/s1:PostalAddress/s1:Country/s0:IdentificationCode/text()"" />
          </Country>
        </Address>
        <Concact>
          <ID>
            <xsl:value-of select=""s1:BuyerCustomerParty/s1:Party/s1:Contact/s0:ID/text()"" />
          </ID>
          <Name>
            <xsl:value-of select=""s1:BuyerCustomerParty/s1:Party/s1:Contact/s0:Name/text()"" />
          </Name>
          <Telephone>
            <xsl:value-of select=""s1:BuyerCustomerParty/s1:Party/s1:Contact/s0:Telephone/text()"" />
          </Telephone>
          <Telefax>
            <xsl:value-of select=""s1:BuyerCustomerParty/s1:Party/s1:Contact/s0:Telefax/text()"" />
          </Telefax>
          <E-mail>
            <xsl:value-of select=""s1:BuyerCustomerParty/s1:Party/s1:Contact/s0:ElectronicMail/text()"" />
          </E-mail>
          <Note>
            <xsl:value-of select=""s1:BuyerCustomerParty/s1:Party/s1:Contact/s0:Note/text()"" />
          </Note>
        </Concact>
      </BuyerCustomer>
      <DeliveryLocation>
        <Address>
          <xsl:variable name=""var:v11"" select=""userCSharp:ReturnName(string(s1:Delivery/s1:DeliveryLocation/s0:Description/text()) , string(s1:Delivery/s1:DeliveryParty/s1:PartyName/s0:Name/text()))"" />
          <Name>
            <xsl:value-of select=""$var:v11"" />
          </Name>
          <AddressFormatCode>
            <xsl:value-of select=""s1:Delivery/s1:DeliveryLocation/s1:Address/s0:AddressFormatCode/text()"" />
          </AddressFormatCode>
          <Street>
            <xsl:value-of select=""s1:Delivery/s1:DeliveryLocation/s1:Address/s0:StreetName/text()"" />
          </Street>
          <Number>
            <xsl:value-of select=""s1:Delivery/s1:DeliveryLocation/s1:Address/s0:BuildingNumber/text()"" />
          </Number>
          <AdditionalStreetName>
            <xsl:value-of select=""s1:Delivery/s1:DeliveryLocation/s1:Address/s0:AdditionalStreetName/text()"" />
          </AdditionalStreetName>
          <Department>
            <xsl:value-of select=""s1:Delivery/s1:DeliveryLocation/s1:Address/s0:Department/text()"" />
          </Department>
          <MarkAttention>
            <xsl:value-of select=""s1:Delivery/s1:DeliveryLocation/s1:Address/s0:MarkAttention/text()"" />
          </MarkAttention>
          <City>
            <xsl:value-of select=""s1:Delivery/s1:DeliveryLocation/s1:Address/s0:CityName/text()"" />
          </City>
          <PostalCode>
            <xsl:value-of select=""s1:Delivery/s1:DeliveryLocation/s1:Address/s0:PostalZone/text()"" />
          </PostalCode>
          <Country>
            <xsl:value-of select=""s1:Delivery/s1:DeliveryLocation/s1:Address/s1:Country/s0:IdentificationCode/text()"" />
          </Country>
        </Address>
      </DeliveryLocation>
      <DeliveryInfo>
        <ActualDeliveryDate>
          <xsl:value-of select=""s1:Delivery/s0:ActualDeliveryDate/text()"" />
        </ActualDeliveryDate>
        <DeliveryTerms>
          <SpecialTerms>
            <xsl:value-of select=""s1:DeliveryTerms/s0:SpecialTerms/text()"" />
          </SpecialTerms>
        </DeliveryTerms>
      </DeliveryInfo>
      <PaymentMeans>
        <ID>
          <xsl:value-of select=""s1:PaymentMeans/s0:ID/text()"" />
        </ID>
        <PaymentMeansCode>
          <xsl:value-of select=""s1:PaymentMeans/s0:PaymentMeansCode/text()"" />
        </PaymentMeansCode>
        <PaymentDueDate>
          <xsl:value-of select=""s1:PaymentMeans/s0:PaymentDueDate/text()"" />
        </PaymentDueDate>
        <InstructionID>
          <xsl:value-of select=""s1:PaymentMeans/s0:InstructionID/text()"" />
        </InstructionID>
        <InstructionNote>
          <xsl:value-of select=""s1:PaymentMeans/s0:InstructionNote/text()"" />
        </InstructionNote>
        <PaymentID>
          <xsl:value-of select=""s1:PaymentMeans/s0:PaymentID/text()"" />
        </PaymentID>
        <CreditAccountID>
          <xsl:value-of select=""s1:PaymentMeans/s1:CreditAccount/s0:AccountID/text()"" />
        </CreditAccountID>
        <PayeeFinancialAccount>
          <ID>
            <xsl:value-of select=""s1:PaymentMeans/s1:PayeeFinancialAccount/s0:ID/text()"" />
          </ID>
          <Name>
            <xsl:value-of select=""s1:PaymentMeans/s1:PayeeFinancialAccount/s0:Name/text()"" />
          </Name>
          <FinancialInstitutionBranch>
            <ID>
              <xsl:value-of select=""s1:PaymentMeans/s1:PayeeFinancialAccount/s1:FinancialInstitutionBranch/s0:ID/text()"" />
            </ID>
            <Name>
              <xsl:value-of select=""s1:PaymentMeans/s1:PayeeFinancialAccount/s1:FinancialInstitutionBranch/s0:Name/text()"" />
            </Name>
            <FinancialInstitution>
              <ID>
                <xsl:value-of select=""s1:PaymentMeans/s1:PayeeFinancialAccount/s1:FinancialInstitutionBranch/s1:FinancialInstitution/s0:ID/text()"" />
              </ID>
              <Name>
                <xsl:value-of select=""s1:PaymentMeans/s1:PayeeFinancialAccount/s1:FinancialInstitutionBranch/s1:FinancialInstitution/s0:Name/text()"" />
              </Name>
            </FinancialInstitution>
          </FinancialInstitutionBranch>
        </PayeeFinancialAccount>
        <PaymentChannelCode>
          <xsl:if test=""s1:PaymentMeans/s0:PaymentChannelCode/@listAgencyID"">
            <xsl:attribute name=""listAgencyID"">
              <xsl:value-of select=""s1:PaymentMeans/s0:PaymentChannelCode/@listAgencyID"" />
            </xsl:attribute>
          </xsl:if>
          <xsl:if test=""s1:PaymentMeans/s0:PaymentChannelCode/@listID"">
            <xsl:attribute name=""listID"">
              <xsl:value-of select=""s1:PaymentMeans/s0:PaymentChannelCode/@listID"" />
            </xsl:attribute>
          </xsl:if>
          <xsl:value-of select=""s1:PaymentMeans/s0:PaymentChannelCode/text()"" />
        </PaymentChannelCode>
      </PaymentMeans>
      <PaymentTerms>
        <ID>
          <xsl:value-of select=""s1:PaymentTerms/s0:ID/text()"" />
        </ID>
        <PaymentMeansID>
          <xsl:value-of select=""s1:PaymentTerms/s0:PaymentMeansID/text()"" />
        </PaymentMeansID>
        <PrepaidPaymentReferenceID>
          <xsl:value-of select=""s1:PaymentTerms/s0:PrepaidPaymentReferenceID/text()"" />
        </PrepaidPaymentReferenceID>
        <Note>
          <xsl:value-of select=""s1:PaymentTerms/s0:Note/text()"" />
        </Note>
        <ReferenceEventCode>
          <xsl:value-of select=""s1:PaymentTerms/s0:ReferenceEventCode/text()"" />
        </ReferenceEventCode>
        <xsl:variable name=""var:v12"" select=""userCSharp:AddDecimals(string(s1:PaymentTerms/s0:SettlementDiscountPercent/text()) , &quot;4&quot;)"" />
        <SettlementDiscountPercent>
          <xsl:value-of select=""$var:v12"" />
        </SettlementDiscountPercent>
        <xsl:variable name=""var:v13"" select=""userCSharp:AddDecimals(string(s1:PaymentTerms/s0:PenaltySurchargePercent/text()) , &quot;4&quot;)"" />
        <PenaltySurchargePercent>
          <xsl:value-of select=""$var:v13"" />
        </PenaltySurchargePercent>
        <xsl:variable name=""var:v14"" select=""userCSharp:AddDecimals(string(s1:PaymentTerms/s0:Amount/text()) , &quot;2&quot;)"" />
        <Amount>
          <xsl:value-of select=""$var:v14"" />
        </Amount>
        <TermsOfPaymentIdentification>
          <xsl:value-of select=""s1:PaymentTerms/s0:PaymentMeansID/text()"" />
        </TermsOfPaymentIdentification>
        <SettlementPeriod>
          <StartDate>
            <xsl:value-of select=""s1:PaymentTerms/s1:SettlementPeriod/s0:StartDate/text()"" />
          </StartDate>
          <EndDate>
            <xsl:value-of select=""s1:PaymentTerms/s1:SettlementPeriod/s0:EndDate/text()"" />
          </EndDate>
          <Description>
            <xsl:value-of select=""s1:PaymentTerms/s1:SettlementPeriod/s0:Description/text()"" />
          </Description>
        </SettlementPeriod>
        <PenaltyPeriod>
          <StartDate>
            <xsl:value-of select=""s1:PaymentTerms/s1:PenaltyPeriod/s0:StartDate/text()"" />
          </StartDate>
          <EndDate>
            <xsl:value-of select=""s1:PaymentTerms/s1:PenaltyPeriod/s0:EndDate/text()"" />
          </EndDate>
          <Description>
            <xsl:value-of select=""s1:PaymentTerms/s1:PenaltyPeriod/s0:Description/text()"" />
          </Description>
        </PenaltyPeriod>
        <MonetaryAmount>
          <Amount>
            <xsl:value-of select=""s1:PaymentTerms/s0:Amount/text()"" />
          </Amount>
          <Currency>
            <xsl:value-of select=""s1:PaymentTerms/s0:Amount/@currencyID"" />
          </Currency>
        </MonetaryAmount>
      </PaymentTerms>
      <PrepaidPayment>
        <ID>
          <xsl:value-of select=""s1:PrepaidPayment/s0:ID/text()"" />
        </ID>
        <xsl:variable name=""var:v15"" select=""userCSharp:GetPaidAmount(string(s1:PrepaidPayment/s0:PaidAmount/text()) , string(s1:LegalMonetaryTotal/s0:PrepaidAmount/text()) , &quot;2&quot;)"" />
        <PaidAmount>
          <xsl:value-of select=""$var:v15"" />
        </PaidAmount>
        <ReceivedDate>
          <xsl:value-of select=""s1:PrepaidPayment/s0:ReceivedDate/text()"" />
        </ReceivedDate>
        <PaidDate>
          <xsl:value-of select=""s1:PrepaidPayment/s0:PaidDate/text()"" />
        </PaidDate>
        <PaidTime>
          <xsl:value-of select=""s1:PrepaidPayment/s0:PaidTime/text()"" />
        </PaidTime>
        <InstructionID>
          <xsl:value-of select=""s1:PrepaidPayment/s0:InstructionID/text()"" />
        </InstructionID>
      </PrepaidPayment>
      <xsl:for-each select=""s1:AllowanceCharge"">
        <AllowanceCharge>
          <ID>
            <xsl:value-of select=""s0:ID/text()"" />
          </ID>
          <ChargeIndicator>
            <xsl:value-of select=""s0:ChargeIndicator/text()"" />
          </ChargeIndicator>
          <AllowanceChargeReasonCode>
            <xsl:value-of select=""s0:AllowanceChargeReasonCode/text()"" />
          </AllowanceChargeReasonCode>
          <AllowanceChargeReason>
            <xsl:value-of select=""s0:AllowanceChargeReason/text()"" />
          </AllowanceChargeReason>
          <xsl:variable name=""var:v16"" select=""userCSharp:AddDecimals(string(s0:MultiplierFactorNumeric/text()) , &quot;4&quot;)"" />
          <MultiplierFactorNumeric>
            <xsl:value-of select=""$var:v16"" />
          </MultiplierFactorNumeric>
          <PrepaidIndicator>
            <xsl:value-of select=""s0:PrepaidIndicator/text()"" />
          </PrepaidIndicator>
          <SequenceNumeric>
            <xsl:value-of select=""s0:SequenceNumeric/text()"" />
          </SequenceNumeric>
          <xsl:variable name=""var:v17"" select=""userCSharp:AddDecimals(string(s0:Amount/text()) , &quot;2&quot;)"" />
          <Amount>
            <xsl:value-of select=""$var:v17"" />
          </Amount>
          <xsl:variable name=""var:v18"" select=""userCSharp:AddDecimals(string(s0:BaseAmount/text()) , &quot;2&quot;)"" />
          <BaseAmount>
            <xsl:value-of select=""$var:v18"" />
          </BaseAmount>
          <AccountingCost>
            <xsl:value-of select=""s0:AccountingCost/text()"" />
          </AccountingCost>
          <TaxCategory>
            <ID>
              <xsl:value-of select=""s1:TaxCategory/s0:ID/text()"" />
            </ID>
            <xsl:variable name=""var:v19"" select=""userCSharp:AddDecimals(string(s1:TaxCategory/s0:Percent/text()) , &quot;4&quot;)"" />
            <Percent>
              <xsl:value-of select=""$var:v19"" />
            </Percent>
            <xsl:variable name=""var:v20"" select=""userCSharp:AddDecimals(string(s1:TaxCategory/s0:PerUnitAmount/text()) , &quot;2&quot;)"" />
            <PerUnitAmount>
              <xsl:value-of select=""$var:v20"" />
            </PerUnitAmount>
            <TaxScheme>
              <ID>
                <xsl:value-of select=""s1:TaxCategory/s1:TaxScheme/s0:ID/text()"" />
              </ID>
              <Navn>
                <xsl:value-of select=""s1:TaxCategory/s1:TaxScheme/s0:Name/text()"" />
              </Navn>
            </TaxScheme>
          </TaxCategory>
          <Attributes>
            <Amount>
              <CurrencyID>
                <xsl:value-of select=""s0:Amount/@currencyID"" />
              </CurrencyID>
            </Amount>
            <BaseAmount>
              <CurrencyID>
                <xsl:value-of select=""s0:BaseAmount/@currencyID"" />
              </CurrencyID>
            </BaseAmount>
          </Attributes>
        </AllowanceCharge>
      </xsl:for-each>
      <TaxTotal>
        <xsl:variable name=""var:v22"" select=""userCSharp:GetTaxAmount(string(s1:TaxTotal/s0:TaxAmount/text()) , string(s1:LegalMonetaryTotal/s0:PayableAmount/text()) , string(s1:LegalMonetaryTotal/s0:LineExtensionAmount/text()) , $var:v21 , &quot;2&quot;)"" />
        <TaxAmount>
          <xsl:value-of select=""$var:v22"" />
        </TaxAmount>
        <TaxSubtotal>
          <xsl:variable name=""var:v23"" select=""userCSharp:AddDecimals(string(s1:TaxTotal/s1:TaxSubtotal/s0:TaxAmount/text()) , &quot;2&quot;)"" />
          <TaxAmount>
            <xsl:value-of select=""$var:v23"" />
          </TaxAmount>
          <xsl:variable name=""var:v24"" select=""userCSharp:AddDecimals(string(s1:TaxTotal/s1:TaxSubtotal/s0:TaxableAmount/text()) , &quot;2&quot;)"" />
          <TaxableAmount>
            <xsl:value-of select=""$var:v24"" />
          </TaxableAmount>
          <xsl:variable name=""var:v25"" select=""userCSharp:AddDecimals(string(s1:TaxTotal/s1:TaxSubtotal/s0:Percent/text()) , &quot;4&quot;)"" />
          <TaxPercent>
            <xsl:value-of select=""$var:v25"" />
          </TaxPercent>
          <TaxCategory>
            <ID>
              <xsl:value-of select=""s1:TaxTotal/s1:TaxSubtotal/s1:TaxCategory/s0:ID/text()"" />
            </ID>
            <xsl:variable name=""var:v26"" select=""userCSharp:AddDecimals(string(s1:TaxTotal/s1:TaxSubtotal/s1:TaxCategory/s0:Percent/text()) , &quot;4&quot;)"" />
            <Percent>
              <xsl:value-of select=""$var:v26"" />
            </Percent>
            <TaxScheme>
              <ID>
                <xsl:value-of select=""s1:TaxTotal/s1:TaxSubtotal/s1:TaxCategory/s1:TaxScheme/s0:ID/text()"" />
              </ID>
              <Navn>
                <xsl:value-of select=""s1:TaxTotal/s1:TaxSubtotal/s1:TaxCategory/s1:TaxScheme/s0:Name/text()"" />
              </Navn>
              <TaxTypeCode>
                <xsl:value-of select=""s1:TaxTotal/s1:TaxSubtotal/s1:TaxCategory/s1:TaxScheme/s0:TaxTypeCode/text()"" />
              </TaxTypeCode>
            </TaxScheme>
          </TaxCategory>
        </TaxSubtotal>
      </TaxTotal>
      <Total>
        <xsl:variable name=""var:v28"" select=""userCSharp:AddDecimals($var:v27 , &quot;2&quot;)"" />
        <LineTotalAmount>
          <xsl:value-of select=""$var:v28"" />
        </LineTotalAmount>
        <xsl:variable name=""var:v31"" select=""userCSharp:GetTaxExclusiveAmount(string(s1:LegalMonetaryTotal/s0:TaxExclusiveAmount/text()) , $var:v27 , $var:v21 , string(s1:LegalMonetaryTotal/s0:ChargeTotalAmount/text()) , $var:v29 , &quot;2&quot; , $var:v30)"" />
        <TaxExclAmount>
          <xsl:value-of select=""$var:v31"" />
        </TaxExclAmount>
        <xsl:variable name=""var:v32"" select=""userCSharp:AddDecimals(string(s1:LegalMonetaryTotal/s0:TaxInclusiveAmount/text()) , &quot;2&quot;)"" />
        <TaxInclAmount>
          <xsl:value-of select=""$var:v32"" />
        </TaxInclAmount>
        <xsl:variable name=""var:v33"" select=""userCSharp:InitCumulativeSum(0)"" />
        <xsl:for-each select=""/s2:Invoice/s1:AllowanceCharge"">
          <xsl:variable name=""var:v34"" select=""userCSharp:StringUpperCase(string(s0:ChargeIndicator/text()))"" />
          <xsl:variable name=""var:v35"" select=""userCSharp:LogicalEq(string($var:v34) , &quot;FALSE&quot;)"" />
          <xsl:if test=""string($var:v35)='true'"">
            <xsl:variable name=""var:v36"" select=""s0:Amount/text()"" />
            <xsl:variable name=""var:v37"" select=""userCSharp:AddToCumulativeSum(0,string($var:v36),&quot;1000&quot;)"" />
          </xsl:if>
        </xsl:for-each>
        <xsl:variable name=""var:v38"" select=""userCSharp:GetCumulativeSum(0)"" />
        <xsl:variable name=""var:v39"" select=""userCSharp:GetAllowanceTotalAmount(string(s1:LegalMonetaryTotal/s0:AllowanceTotalAmount/text()) , string($var:v38) , &quot;2&quot;)"" />
        <AllowanceTotalAmount>
          <xsl:value-of select=""$var:v39"" />
        </AllowanceTotalAmount>
        <xsl:variable name=""var:v40"" select=""userCSharp:InitCumulativeSum(1)"" />
        <xsl:for-each select=""/s2:Invoice/s1:AllowanceCharge"">
          <xsl:variable name=""var:v41"" select=""string(s0:ChargeIndicator/text())"" />
          <xsl:variable name=""var:v42"" select=""userCSharp:StringUpperCase($var:v41)"" />
          <xsl:variable name=""var:v43"" select=""userCSharp:LogicalEq(string($var:v42) , &quot;TRUE&quot;)"" />
          <xsl:if test=""string($var:v43)='true'"">
            <xsl:variable name=""var:v44"" select=""s0:Amount/text()"" />
            <xsl:variable name=""var:v45"" select=""userCSharp:AddToCumulativeSum(1,string($var:v44),&quot;1000&quot;)"" />
          </xsl:if>
        </xsl:for-each>
        <xsl:variable name=""var:v46"" select=""userCSharp:GetCumulativeSum(1)"" />
        <xsl:variable name=""var:v48"" select=""userCSharp:GetChargeTotalAmount($var:v47 , string($var:v46) , &quot;2&quot;)"" />
        <ChargeTotalAmount>
          <xsl:value-of select=""$var:v48"" />
        </ChargeTotalAmount>
        <xsl:variable name=""var:v49"" select=""userCSharp:AddDecimals($var:v21 , &quot;2&quot;)"" />
        <PrepaidAmount>
          <xsl:value-of select=""$var:v49"" />
        </PrepaidAmount>
        <xsl:variable name=""var:v50"" select=""userCSharp:AddDecimals(string(s1:LegalMonetaryTotal/s0:PayableRoundingAmount/text()) , &quot;2&quot;)"" />
        <PayableRoundingAmount>
          <xsl:value-of select=""$var:v50"" />
        </PayableRoundingAmount>
        <xsl:variable name=""var:v52"" select=""userCSharp:AddDecimals($var:v51 , &quot;2&quot;)"" />
        <PayableAmount>
          <xsl:value-of select=""$var:v52"" />
        </PayableAmount>
      </Total>
      <Lines>
        <xsl:for-each select=""s1:InvoiceLine"">
          <xsl:variable name=""var:v55"" select=""userCSharp:LogicalNe(string(s1:OrderLineReference/s1:OrderReference/s0:ID/text()) , &quot;''&quot;)"" />
          <Line>
            <LineNo>
              <xsl:value-of select=""s0:ID/text()"" />
            </LineNo>
            <Note>
              <xsl:value-of select=""s0:Note/text()"" />
            </Note>
            <NoteLoop>
              <Node>
                <Note>
                  <xsl:value-of select=""s0:Note/text()"" />
                </Note>
              </Node>
            </NoteLoop>
            <xsl:variable name=""var:v53"" select=""userCSharp:AddDecimals(string(s0:InvoicedQuantity/text()) , &quot;3&quot;)"" />
            <Quantity>
              <xsl:value-of select=""$var:v53"" />
            </Quantity>
            <UnitCode>
              <xsl:value-of select=""s0:InvoicedQuantity/@unitCode"" />
            </UnitCode>
            <xsl:variable name=""var:v54"" select=""userCSharp:AddDecimals(string(s0:LineExtensionAmount/text()) , &quot;2&quot;)"" />
            <LineAmountTotal>
              <xsl:value-of select=""$var:v54"" />
            </LineAmountTotal>
            <FreeOfChargeIndicator>
              <xsl:value-of select=""s0:FreeOfChargeIndicator/text()"" />
            </FreeOfChargeIndicator>
            <Delivery>
              <ID>
                <xsl:value-of select=""s1:Delivery/s0:ID/text()"" />
              </ID>
              <Quantity>
                <xsl:value-of select=""s1:Delivery/s0:Quantity/text()"" />
              </Quantity>
              <UnitCode>
                <xsl:value-of select=""s1:Delivery/s0:Quantity/@unitCode"" />
              </UnitCode>
            </Delivery>
            <OrderReference>
              <Node>
                <xsl:if test=""string($var:v55)='true'"">
                  <xsl:variable name=""var:v56"" select=""&quot;ON&quot;"" />
                  <Code>
                    <xsl:value-of select=""$var:v56"" />
                  </Code>
                </xsl:if>
                <Reference>
                  <xsl:value-of select=""s1:OrderLineReference/s1:OrderReference/s0:ID/text()"" />
                </Reference>
              </Node>
            </OrderReference>
            <xsl:for-each select=""s1:DocumentReference"">
              <DocumentReference>
                <ID>
                  <xsl:if test=""s0:ID/@schemeID"">
                    <xsl:attribute name=""schemeID"">
                      <xsl:value-of select=""s0:ID/@schemeID"" />
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:if test=""s0:ID"">
                    <xsl:value-of select=""s0:ID/text()"" />
                  </xsl:if>
                </ID>
                <xsl:if test=""s0:DocumentType"">
                  <DocumentTypeCode>
                    <xsl:value-of select=""s0:DocumentType/text()"" />
                  </DocumentTypeCode>
                </xsl:if>
              </DocumentReference>
            </xsl:for-each>
            <Item>
              <BuyerItemID>
                <xsl:value-of select=""s1:Item/s1:BuyersItemIdentification/s0:ID/text()"" />
              </BuyerItemID>
              <SellerItemID>
                <xsl:value-of select=""s1:Item/s1:SellersItemIdentification/s0:ID/text()"" />
              </SellerItemID>
              <StandardItemID>
                <xsl:value-of select=""s1:Item/s1:StandardItemIdentification/s0:ID/text()"" />
              </StandardItemID>
              <AdditionalItemID>
                <xsl:value-of select=""s1:Item/s1:AdditionalItemIdentification/s0:ID/text()"" />
              </AdditionalItemID>
              <CatalogueItemID>
                <xsl:value-of select=""s1:Item/s1:CatalogueItemIdentification/s0:ID/text()"" />
              </CatalogueItemID>
              <Name>
                <xsl:value-of select=""s1:Item/s0:Name/text()"" />
              </Name>
              <Description>
                <xsl:value-of select=""s1:Item/s0:Description/text()"" />
              </Description>
              <PackQuantity>
                <xsl:value-of select=""s1:Item/s0:PackQuantity/text()"" />
              </PackQuantity>
              <UnitCode>
                <xsl:value-of select=""s1:Item/s0:PackQuantity/@unitCode"" />
              </UnitCode>
            </Item>
            <PriceNet>
              <xsl:variable name=""var:v57"" select=""userCSharp:AddDecimals(string(s1:Price/s0:PriceAmount/text()) , &quot;2&quot; , &quot;2&quot;)"" />
              <Price>
                <xsl:value-of select=""$var:v57"" />
              </Price>
              <xsl:variable name=""var:v58"" select=""userCSharp:AddDecimals(string(s1:Price/s0:BaseQuantity/text()) , &quot;2&quot;)"" />
              <Quantity>
                <xsl:value-of select=""$var:v58"" />
              </Quantity>
              <UnitCode>
                <xsl:value-of select=""s1:Price/s0:BaseQuantity/@unitCode"" />
              </UnitCode>
              <PristypeCode>
                <xsl:value-of select=""s1:Price/s0:PriceTypeCode/text()"" />
              </PristypeCode>
              <PristypeTekst>
                <xsl:value-of select=""s1:Price/s0:PriceType/text()"" />
              </PristypeTekst>
            </PriceNet>
            <PriceGross>
              <xsl:variable name=""var:v59"" select=""userCSharp:AddDecimals(string(s1:PricingReference/s1:AlternativeConditionPrice/s0:PriceAmount/text()) , &quot;2&quot; , &quot;2&quot;)"" />
              <Price>
                <xsl:value-of select=""$var:v59"" />
              </Price>
              <xsl:variable name=""var:v60"" select=""userCSharp:AddDecimals(string(s1:PricingReference/s1:AlternativeConditionPrice/s0:BaseQuantity/text()) , &quot;2&quot;)"" />
              <Quantity>
                <xsl:value-of select=""$var:v60"" />
              </Quantity>
              <UnitCode>
                <xsl:value-of select=""s1:PricingReference/s1:AlternativeConditionPrice/s0:BaseQuantity/@unitCode"" />
              </UnitCode>
              <PristypeCode>
                <xsl:value-of select=""s1:PricingReference/s1:AlternativeConditionPrice/s0:PriceTypeCode/text()"" />
              </PristypeCode>
              <PristypeTekst>
                <xsl:value-of select=""s1:PricingReference/s1:AlternativeConditionPrice/s0:PriceType/text()"" />
              </PristypeTekst>
              <OrderableUnitFactorRate>
                <xsl:value-of select=""s1:PricingReference/s1:AlternativeConditionPrice/s0:OrderableUnitFactorRate/text()"" />
              </OrderableUnitFactorRate>
            </PriceGross>
            <xsl:for-each select=""s1:AllowanceCharge"">
              <xsl:variable name=""var:v61"" select=""string(s0:ChargeIndicator/text())"" />
              <xsl:variable name=""var:v62"" select=""userCSharp:StringLowerCase($var:v61)"" />
              <xsl:variable name=""var:v63"" select=""string(s0:MultiplierFactorNumeric/text())"" />
              <xsl:variable name=""var:v66"" select=""string(s0:Amount/text())"" />
              <xsl:variable name=""var:v68"" select=""string(s0:BaseAmount/text())"" />
              <xsl:variable name=""var:v70"" select=""string(s1:TaxCategory/s0:Percent/text())"" />
              <xsl:variable name=""var:v72"" select=""string(s1:TaxCategory/s0:PerUnitAmount/text())"" />
              <AllowanceCharge>
                <ID>
                  <xsl:value-of select=""s0:ID/text()"" />
                </ID>
                <ChargeIndicator>
                  <xsl:value-of select=""$var:v62"" />
                </ChargeIndicator>
                <AllowanceChargeReasonCode>
                  <xsl:value-of select=""s0:AllowanceChargeReasonCode/text()"" />
                </AllowanceChargeReasonCode>
                <AllowanceChargeReason>
                  <xsl:value-of select=""s0:AllowanceChargeReason/text()"" />
                </AllowanceChargeReason>
                <xsl:variable name=""var:v64"" select=""userCSharp:ReturnDivide100($var:v63)"" />
                <xsl:variable name=""var:v65"" select=""userCSharp:AddDecimals(string($var:v64) , &quot;4&quot;)"" />
                <MultiplierFactorNumeric>
                  <xsl:value-of select=""$var:v65"" />
                </MultiplierFactorNumeric>
                <PrepaidIndicator>
                  <xsl:value-of select=""s0:PrepaidIndicator/text()"" />
                </PrepaidIndicator>
                <SequenceNumeric>
                  <xsl:value-of select=""s0:SequenceNumeric/text()"" />
                </SequenceNumeric>
                <xsl:variable name=""var:v67"" select=""userCSharp:AddDecimals($var:v66 , &quot;2&quot;)"" />
                <Amount>
                  <xsl:value-of select=""$var:v67"" />
                </Amount>
                <AmountCode>
                  <xsl:value-of select=""s0:AccountingCost/text()"" />
                </AmountCode>
                <xsl:variable name=""var:v69"" select=""userCSharp:AddDecimals($var:v68 , &quot;2&quot;)"" />
                <BaseAmount>
                  <xsl:value-of select=""$var:v69"" />
                </BaseAmount>
                <TaxCategory>
                  <ID>
                    <xsl:value-of select=""s1:TaxCategory/s0:ID/text()"" />
                  </ID>
                  <xsl:variable name=""var:v71"" select=""userCSharp:AddDecimals($var:v70 , &quot;4&quot;)"" />
                  <Percent>
                    <xsl:value-of select=""$var:v71"" />
                  </Percent>
                  <xsl:variable name=""var:v73"" select=""userCSharp:AddDecimals($var:v72 , &quot;2&quot;)"" />
                  <PerUnitAmount>
                    <xsl:value-of select=""$var:v73"" />
                  </PerUnitAmount>
                  <TaxScheme>
                    <ID>
                      <xsl:value-of select=""s1:TaxCategory/s1:TaxScheme/s0:ID/text()"" />
                    </ID>
                    <Navn>
                      <xsl:value-of select=""s1:TaxCategory/s1:TaxScheme/s0:Name/text()"" />
                    </Navn>
                  </TaxScheme>
                </TaxCategory>
                <Attributes>
                  <Amount>
                    <xsl:attribute name=""CurrencyID"">
                      <xsl:value-of select=""s0:Amount/@currencyID"" />
                    </xsl:attribute>
                  </Amount>
                  <BaseAmount>
                    <xsl:attribute name=""CurrencyID"">
                      <xsl:value-of select=""s0:BaseAmount/@currencyID"" />
                    </xsl:attribute>
                  </BaseAmount>
                </Attributes>
              </AllowanceCharge>
            </xsl:for-each>
            <xsl:for-each select=""s1:TaxTotal"">
              <TaxTotal>
                <xsl:variable name=""var:v74"" select=""userCSharp:AddDecimals(string(s0:TaxAmount/text()) , &quot;2&quot;)"" />
                <TaxAmount>
                  <xsl:value-of select=""$var:v74"" />
                </TaxAmount>
                <Currency>
                  <xsl:value-of select=""s0:TaxAmount/@currencyID"" />
                </Currency>
                <TaxEvidenceIndicator>
                  <xsl:value-of select=""s0:TaxEvidenceIndicator/text()"" />
                </TaxEvidenceIndicator>
                <TaxSubtotal>
                  <xsl:variable name=""var:v75"" select=""userCSharp:AddDecimals(string(s1:TaxSubtotal/s0:TaxAmount/text()) , &quot;2&quot;)"" />
                  <TaxAmount>
                    <xsl:value-of select=""$var:v75"" />
                  </TaxAmount>
                  <TaxAmountCurrency>
                    <xsl:value-of select=""s1:TaxSubtotal/s0:TaxAmount/@currencyID"" />
                  </TaxAmountCurrency>
                  <xsl:variable name=""var:v76"" select=""userCSharp:AddDecimals(string(s1:TaxSubtotal/s0:TaxableAmount/text()) , &quot;2&quot;)"" />
                  <TaxableAmount>
                    <xsl:value-of select=""$var:v76"" />
                  </TaxableAmount>
                  <TaxableAmountCurrency>
                    <xsl:value-of select=""s1:TaxSubtotal/s0:TaxableAmount/@currencyID"" />
                  </TaxableAmountCurrency>
                  <xsl:variable name=""var:v77"" select=""userCSharp:AddDecimals(string(s1:TaxSubtotal/s0:Percent/text()) , &quot;4&quot;)"" />
                  <TaxPercent>
                    <xsl:value-of select=""$var:v77"" />
                  </TaxPercent>
                  <TaxCategory>
                    <ID>
                      <xsl:value-of select=""s1:TaxSubtotal/s1:TaxCategory/s0:ID/text()"" />
                    </ID>
                    <xsl:variable name=""var:v78"" select=""userCSharp:AddDecimals(string(s1:TaxSubtotal/s1:TaxCategory/s0:Percent/text()) , &quot;4&quot;)"" />
                    <Percent>
                      <xsl:value-of select=""$var:v78"" />
                    </Percent>
                    <TaxScheme>
                      <ID>
                        <xsl:value-of select=""s1:TaxSubtotal/s1:TaxCategory/s1:TaxScheme/s0:ID/text()"" />
                      </ID>
                      <Navn>
                        <xsl:value-of select=""s1:TaxSubtotal/s1:TaxCategory/s1:TaxScheme/s0:Name/text()"" />
                      </Navn>
                    </TaxScheme>
                  </TaxCategory>
                </TaxSubtotal>
              </TaxTotal>
            </xsl:for-each>
          </Line>
        </xsl:for-each>
      </Lines>
    </ns0:Invoice>
  </xsl:template>
  <msxsl:script language=""C#"" implements-prefix=""userCSharp""><![CDATA[
public string ReturnName(string Name1, string Name2)

{

string Name;

Name = """";

if (Name1 != """")
{
Name = Name1;
}
else if (Name1 == """" && Name2 != """")
{
Name = Name2;
}

return Name; 

}


public string AddDecimals(string OriginalAmount, string numberOfDecimals)
        {
            if (string.IsNullOrWhiteSpace(OriginalAmount))
                return OriginalAmount;
            decimal d = System.Convert.ToDecimal(OriginalAmount, System.Globalization.CultureInfo.InvariantCulture);
string format = ""0."" + new string('0', System.Convert.ToInt32(numberOfDecimals));
            string NewAmount = d.ToString(format, System.Globalization.CultureInfo.InvariantCulture);
            return NewAmount;
		}	


public string GetPaidAmount(string Amount, string PrepaidAmount, string numberOfDecimals)
{
string PD = Amount;

if ((string.IsNullOrWhiteSpace(Amount)) && (string.IsNullOrWhiteSpace(PrepaidAmount)))
{
return Amount;
}

if (!(string.IsNullOrWhiteSpace(Amount)))
{
 PD = Amount;
}

if ((string.IsNullOrWhiteSpace(Amount)) && (!(string.IsNullOrWhiteSpace(PrepaidAmount))))
{
PD = PrepaidAmount;
}

decimal d = System.Convert.ToDecimal(PD, System.Globalization.CultureInfo.InvariantCulture);
string format = ""0."" + new string('0', System.Convert.ToInt32(numberOfDecimals));
string PaidAmount = d.ToString(format, System.Globalization.CultureInfo.InvariantCulture);
return PaidAmount;
}

public string GetTaxAmount(string Tax, string PayableAmount, string LineExtAmount, string PrepaidAmount, string numberOfDecimals)
{
 string TaxAmount = Tax;
 decimal PA = 0;
 if (!(string.IsNullOrWhiteSpace(Tax)))
 {
 decimal d = System.Convert.ToDecimal(Tax, System.Globalization.CultureInfo.InvariantCulture); 
 string nformat = ""0."" + new string('0', System.Convert.ToInt32(numberOfDecimals));
 TaxAmount = d.ToString(nformat, System.Globalization.CultureInfo.InvariantCulture);
 }
if ((string.IsNullOrWhiteSpace(Tax)) && (!(string.IsNullOrWhiteSpace(PayableAmount))) && (!(string.IsNullOrWhiteSpace(LineExtAmount))))
{    
if (!(string.IsNullOrWhiteSpace(PrepaidAmount)))
{
 PA = System.Convert.ToDecimal(PrepaidAmount, System.Globalization.CultureInfo.InvariantCulture);
}
 decimal TXA = System.Convert.ToDecimal(PayableAmount, System.Globalization.CultureInfo.InvariantCulture) - System.Convert.ToDecimal(LineExtAmount, System.Globalization.CultureInfo.InvariantCulture) - PA;
 string format = ""0."" + new string('0', System.Convert.ToInt32(numberOfDecimals));
 TaxAmount = TXA.ToString(format, System.Globalization.CultureInfo.InvariantCulture);
}

 return TaxAmount;
}

public string GetAllowanceTotalAmount(string Allowance, string ALC, string numberOfDecimals)
{
string ALCA = Allowance;

if ((string.IsNullOrWhiteSpace(Allowance)) && (string.IsNullOrWhiteSpace(ALC)))
{
return Allowance;
}

if (!(string.IsNullOrWhiteSpace(Allowance)))
{
 ALCA = Allowance;
}

if ((string.IsNullOrWhiteSpace(Allowance)) && (!(string.IsNullOrWhiteSpace(ALC))))
{
ALCA = ALC;
}

decimal d = System.Convert.ToDecimal(ALCA, System.Globalization.CultureInfo.InvariantCulture);
string format = ""0."" + new string('0', System.Convert.ToInt32(numberOfDecimals));
string AllowanceTotalAmount = d.ToString(format, System.Globalization.CultureInfo.InvariantCulture);
return AllowanceTotalAmount;
}	

public string GetChargeTotalAmount(string Charge, string ALC, string numberOfDecimals)
{
string CHA = Charge;

if ((string.IsNullOrWhiteSpace(Charge)) && (string.IsNullOrWhiteSpace(ALC)))
{
return Charge;
}

if (!(string.IsNullOrWhiteSpace(Charge)))
{
 CHA = Charge;
}

if ((string.IsNullOrWhiteSpace(Charge)) && (!(string.IsNullOrWhiteSpace(ALC))))
{
CHA = ALC;
}

decimal d = System.Convert.ToDecimal(CHA, System.Globalization.CultureInfo.InvariantCulture);
string format = ""0."" + new string('0', System.Convert.ToInt32(numberOfDecimals));
string ChargeTotalAmount = d.ToString(format, System.Globalization.CultureInfo.InvariantCulture);
return ChargeTotalAmount;
}

public string GetTaxExclusiveAmount(string TaxExclAmount, string LineExtAmount, string PrepaidAmount, string TAXAmount, string VAT, string numberOfDecimals, string TAX)
{
 string TaxExclusiveAmount = TaxExclAmount;
 decimal PA = 0;
 decimal TA = 0;

 if (!(string.IsNullOrWhiteSpace(TaxExclAmount)))
 {
 decimal d = System.Convert.ToDecimal(TaxExclAmount, System.Globalization.CultureInfo.InvariantCulture);
 string nformat = ""0."" + new string('0', System.Convert.ToInt32(numberOfDecimals));
 TaxExclusiveAmount = d.ToString(nformat, System.Globalization.CultureInfo.InvariantCulture);
 }

 if ((string.IsNullOrWhiteSpace(TaxExclAmount)) && (!(string.IsNullOrWhiteSpace(TAX))))
 {
 decimal d = System.Convert.ToDecimal(TAX, System.Globalization.CultureInfo.InvariantCulture);
 string nformat = ""0."" + new string('0', System.Convert.ToInt32(numberOfDecimals));
 TaxExclusiveAmount = d.ToString(nformat, System.Globalization.CultureInfo.InvariantCulture);
 }

 return TaxExclusiveAmount;
}

public string InitCumulativeSum(int index)
{
	if (index >= 0)
	{
		if (index >= myCumulativeSumArray.Count)
		{
			int i = myCumulativeSumArray.Count;
			for (; i<=index; i++)
			{
				myCumulativeSumArray.Add("""");
			}
		}
		else
		{
			myCumulativeSumArray[index] = """";
		}
	}
	return """";
}

public System.Collections.ArrayList myCumulativeSumArray = new System.Collections.ArrayList();

public string AddToCumulativeSum(int index, string val, string notused)
{
	if (index < 0 || index >= myCumulativeSumArray.Count)
	{
		return """";
    }
	double d = 0;
	if (IsNumeric(val, ref d))
	{
		if (myCumulativeSumArray[index] == """")
		{
			myCumulativeSumArray[index] = d;
		}
		else
		{
			myCumulativeSumArray[index] = (double)(myCumulativeSumArray[index]) + d;
		}
	}
	return (myCumulativeSumArray[index] is double) ? ((double)myCumulativeSumArray[index]).ToString(System.Globalization.CultureInfo.InvariantCulture) : """";
}

public string GetCumulativeSum(int index)
{
	if (index < 0 || index >= myCumulativeSumArray.Count)
	{
		return """";
	}
	return (myCumulativeSumArray[index] is double) ? ((double)myCumulativeSumArray[index]).ToString(System.Globalization.CultureInfo.InvariantCulture) : """";
}

public string StringUpperCase(string str)
{
	if (str == null)
	{
		return """";
	}
	return str.ToUpper(System.Globalization.CultureInfo.InvariantCulture);
}


public bool LogicalEq(string val1, string val2)
{
	bool ret = false;
	double d1 = 0;
	double d2 = 0;
	if (IsNumeric(val1, ref d1) && IsNumeric(val2, ref d2))
	{
		ret = d1 == d2;
	}
	else
	{
		ret = String.Compare(val1, val2, StringComparison.Ordinal) == 0;
	}
	return ret;
}


public string AddDecimals(string OriginalAmount, string numberOfDecimals, string numberOfDashes)
        {
            if (string.IsNullOrWhiteSpace(OriginalAmount))
                return OriginalAmount;
            decimal d = System.Convert.ToDecimal(OriginalAmount, System.Globalization.CultureInfo.InvariantCulture);
string format = ""0."" + new string('0', System.Convert.ToInt32(numberOfDecimals)) + new string('#', System.Convert.ToInt32(numberOfDashes));
            string NewAmount = d.ToString(format, System.Globalization.CultureInfo.InvariantCulture);
            return NewAmount;
		}	

public bool LogicalNe(string val1, string val2)
{
	bool ret = false;
	double d1 = 0;
	double d2 = 0;
	if (IsNumeric(val1, ref d1) && IsNumeric(val2, ref d2))
	{
		ret = d1 != d2;
	}
	else
	{
		ret = String.Compare(val1, val2, StringComparison.Ordinal) != 0;
	}
	return ret;
}


public string StringLowerCase(string str)
{
	if (str == null)
	{
		return """";
	}
	return str.ToLower(System.Globalization.CultureInfo.InvariantCulture);
}


public string ReturnDivide100(string OrigAmount)
{
            if (string.IsNullOrWhiteSpace(OrigAmount))
                return OrigAmount;
double d = System.Convert.ToDouble(OrigAmount.Replace('.', ','));
d = d / 100;
string NewAm = System.Convert.ToString(d);

return NewAm.Replace(',', '.');
}

public bool IsNumeric(string val)
{
	if (val == null)
	{
		return false;
	}
	double d = 0;
	return Double.TryParse(val, System.Globalization.NumberStyles.AllowThousands | System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out d);
}

public bool IsNumeric(string val, ref double d)
{
	if (val == null)
	{
		return false;
	}
	return Double.TryParse(val, System.Globalization.NumberStyles.AllowThousands | System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out d);
}


]]></msxsl:script>
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"BYGE.Integration.UBLInvoice.UBLInvoice";
        
        private const global::BYGE.Integration.UBLInvoice.UBLInvoice _srcSchemaTypeReference0 = null;
        
        private const string _strTrgSchemasList0 = @"BYGE.Integration.Core.Invoice.Invoice";
        
        private const global::BYGE.Integration.Core.Invoice.Invoice _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [1];
                _SrcSchemas[0] = @"BYGE.Integration.UBLInvoice.UBLInvoice";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"BYGE.Integration.Core.Invoice.Invoice";
                return _TrgSchemas;
            }
        }
    }
}
