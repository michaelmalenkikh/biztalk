namespace BYGE.Integration.UBLInvoice {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [Schema(@"urn:oasis:names:specification:ubl:schema:xsd:Invoice-2",@"Invoice")]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"Invoice"})]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.UBLInvoice.UBLInvoice1", typeof(global::BYGE.Integration.UBLInvoice.UBLInvoice1))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.UBLInvoice.UBLInvoice2", typeof(global::BYGE.Integration.UBLInvoice.UBLInvoice2))]
    public sealed class UBLInvoice : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xs:schema xmlns:ext=""urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2"" xmlns:cac=""urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"" xmlns:qdt=""urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:udt=""urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2"" xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" xmlns:sdt=""urn:oasis:names:specification:ubl:schema:xsd:SpecializedDatatypes-2"" xmlns:byg=""urn:urn:byg-e.dk:example"" xmlns:cbc=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" xmlns:ccts=""urn:oasis:names:specification:ubl:schema:xsd:CoreComponentParameters-2"" attributeFormDefault=""unqualified"" elementFormDefault=""qualified"" targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:Invoice-2"" xmlns:xs=""http://www.w3.org/2001/XMLSchema"">
  <xs:import schemaLocation=""BYGE.Integration.UBLInvoice.UBLInvoice1"" namespace=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" />
  <xs:import schemaLocation=""BYGE.Integration.UBLInvoice.UBLInvoice2"" namespace=""urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"" />
  <xs:annotation>
    <xs:appinfo>
      <b:references>
        <b:reference targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" />
        <b:reference targetNamespace=""urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"" />
      </b:references>
    </xs:appinfo>
  </xs:annotation>
  <xs:element name=""Invoice"">
    <xs:complexType>
      <xs:sequence>
        <xs:element ref=""cbc:ProfileID"" />
        <xs:element ref=""cbc:ID"" />
        <xs:element ref=""cbc:CopyIndicator"" />
        <xs:element ref=""cbc:UUID"" />
        <xs:element ref=""cbc:IssueDate"" />
        <xs:element ref=""cbc:InvoiceTypeCode"" />
        <xs:element maxOccurs=""unbounded"" ref=""cbc:Note"" />
        <xs:element ref=""cbc:DocumentCurrencyCode"" />
        <xs:element ref=""cac:OrderReference"" />
        <xs:element ref=""cac:AdditionalDocumentReference"" />
        <xs:element ref=""cac:DespatchDocumentReference"" />
        <xs:element ref=""cac:AccountingSupplierParty"" />
        <xs:element ref=""cac:AccountingCustomerParty"" />
        <xs:element ref=""cac:BuyerCustomerParty"" />
        <xs:element ref=""cac:Delivery"" />
        <xs:element ref=""cac:DeliveryTerms"" />
        <xs:element ref=""cac:PaymentMeans"" />
        <xs:element ref=""cac:PaymentTerms"" />
        <xs:element ref=""cac:PrepaidPayment"" />
        <xs:element maxOccurs=""unbounded"" ref=""cac:AllowanceCharge"" />
        <xs:element ref=""cac:TaxTotal"" />
        <xs:element ref=""cac:LegalMonetaryTotal"" />
        <xs:element maxOccurs=""unbounded"" ref=""cac:InvoiceLine"" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>
</xs:schema>";
        
        public UBLInvoice() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [1];
                _RootElements[0] = "Invoice";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
    }
}
