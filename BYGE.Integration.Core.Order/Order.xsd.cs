namespace BYGE.Integration.Core.Order {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [Schema(@"http://byg-e.dk/schemas/v10",@"Order")]
    [Microsoft.XLANGs.BaseTypes.PropertyAttribute(typeof(global::BYGE.Integration.Core.Properties.MessageID), XPath = @"/*[local-name()='Order' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='OrderID' and namespace-uri()='']", XsdType = @"string")]
    [Microsoft.XLANGs.BaseTypes.PropertyAttribute(typeof(global::BYGE.Integration.Core.Properties.BuyerID), XPath = @"/*[local-name()='Order' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='BuyerCustomerParty' and namespace-uri()='']/*[local-name()='BuyerID' and namespace-uri()='']", XsdType = @"string")]
    [Microsoft.XLANGs.BaseTypes.PropertyAttribute(typeof(global::BYGE.Integration.Core.Properties.AccountingBuyerID), XPath = @"/*[local-name()='Order' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='AccountingCustomer' and namespace-uri()='']/*[local-name()='AccBuyerID' and namespace-uri()='']", XsdType = @"string")]
    [Microsoft.XLANGs.BaseTypes.PropertyAttribute(typeof(global::BYGE.Integration.Core.Properties.ReceiverID), XPath = @"/*[local-name()='Order' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='SupplierCustomerParty' and namespace-uri()='']/*[local-name()='AccSellerID' and namespace-uri()='']", XsdType = @"string")]
    [Microsoft.XLANGs.BaseTypes.PropertyAttribute(typeof(global::BYGE.Integration.Core.Properties.ReceiverName), XPath = @"/*[local-name()='Order' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='SupplierCustomerParty' and namespace-uri()='']/*[local-name()='Address' and namespace-uri()='']/*[local-name()='Name' and namespace-uri()='']", XsdType = @"string")]
    [Microsoft.XLANGs.BaseTypes.PropertyAttribute(typeof(global::BYGE.Integration.Core.Properties.BuyerName), XPath = @"/*[local-name()='Order' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='AccountingCustomer' and namespace-uri()='']/*[local-name()='Address' and namespace-uri()='']/*[local-name()='Name' and namespace-uri()='']", XsdType = @"string")]
    [Microsoft.XLANGs.BaseTypes.PropertyAttribute(typeof(global::BYGE.Integration.Core.Properties.TestIndicator), XPath = @"/*[local-name()='Order' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='TestIndicator' and namespace-uri()='']", XsdType = @"string")]
    [Microsoft.XLANGs.BaseTypes.PropertyAttribute(typeof(global::BYGE.Integration.Core.Properties.InvoiceType), XPath = @"/*[local-name()='Order' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='DocumentType' and namespace-uri()='']", XsdType = @"string")]
    [Microsoft.XLANGs.BaseTypes.PropertyAttribute(typeof(global::BYGE.Integration.Core.Properties.SenderID), XPath = @"/*[local-name()='Order' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='AccountingCustomer' and namespace-uri()='']/*[local-name()='AccBuyerID' and namespace-uri()='']", XsdType = @"string")]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"Order"})]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.Core.Properties.CoreProperties", typeof(global::BYGE.Integration.Core.Properties.CoreProperties))]
    public sealed class Order : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xs:schema xmlns=""http://byg-e.dk/schemas/v10"" xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" xmlns:ns0=""https://BYGE.Integration.Core.Properties.CoreProperties"" targetNamespace=""http://byg-e.dk/schemas/v10"" xmlns:xs=""http://www.w3.org/2001/XMLSchema"">
  <xs:annotation>
    <xs:appinfo>
      <b:imports>
        <b:namespace prefix=""ns0"" uri=""https://BYGE.Integration.Core.Properties.CoreProperties"" location=""BYGE.Integration.Core.Properties.CoreProperties"" />
      </b:imports>
    </xs:appinfo>
  </xs:annotation>
  <xs:element name=""Order"">
    <xs:annotation>
      <xs:appinfo>
        <b:properties>
          <b:property name=""ns0:MessageID"" xpath=""/*[local-name()='Order' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='OrderID' and namespace-uri()='']"" />
          <b:property name=""ns0:BuyerID"" xpath=""/*[local-name()='Order' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='BuyerCustomerParty' and namespace-uri()='']/*[local-name()='BuyerID' and namespace-uri()='']"" />
          <b:property name=""ns0:AccountingBuyerID"" xpath=""/*[local-name()='Order' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='AccountingCustomer' and namespace-uri()='']/*[local-name()='AccBuyerID' and namespace-uri()='']"" />
          <b:property name=""ns0:ReceiverID"" xpath=""/*[local-name()='Order' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='SupplierCustomerParty' and namespace-uri()='']/*[local-name()='AccSellerID' and namespace-uri()='']"" />
          <b:property name=""ns0:ReceiverName"" xpath=""/*[local-name()='Order' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='SupplierCustomerParty' and namespace-uri()='']/*[local-name()='Address' and namespace-uri()='']/*[local-name()='Name' and namespace-uri()='']"" />
          <b:property name=""ns0:BuyerName"" xpath=""/*[local-name()='Order' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='AccountingCustomer' and namespace-uri()='']/*[local-name()='Address' and namespace-uri()='']/*[local-name()='Name' and namespace-uri()='']"" />
          <b:property name=""ns0:TestIndicator"" xpath=""/*[local-name()='Order' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='TestIndicator' and namespace-uri()='']"" />
          <b:property name=""ns0:InvoiceType"" xpath=""/*[local-name()='Order' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='DocumentType' and namespace-uri()='']"" />
          <b:property name=""ns0:SenderID"" xpath=""/*[local-name()='Order' and namespace-uri()='http://byg-e.dk/schemas/v10']/*[local-name()='AccountingCustomer' and namespace-uri()='']/*[local-name()='AccBuyerID' and namespace-uri()='']"" />
        </b:properties>
      </xs:appinfo>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name=""UBLVersionID"" type=""xs:string"" />
        <xs:element name=""CustomizationID"" type=""xs:string"" />
        <xs:element name=""TestIndicator"" type=""xs:string"" />
        <xs:element minOccurs=""0"" default=""220"" name=""DocumentType"" type=""xs:string"" />
        <xs:element name=""ProfileID"">
          <xs:complexType>
            <xs:simpleContent>
              <xs:extension base=""xs:string"">
                <xs:attribute name=""schemeID"" type=""xs:string"" />
                <xs:attribute name=""schemeAgencyID"" type=""xs:string"" />
              </xs:extension>
            </xs:simpleContent>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""1"" maxOccurs=""1"" name=""OrderID"" type=""xs:string"" />
        <xs:element minOccurs=""0"" name=""UUID"" type=""xs:string"" />
        <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CopyIndicator"" type=""xs:string"" />
        <xs:element minOccurs=""1"" maxOccurs=""1"" name=""IssueDate"" type=""xs:date"" />
        <xs:element minOccurs=""0"" name=""OrderType"" type=""xs:string"" />
        <xs:element minOccurs=""0"" maxOccurs=""99"" name=""NoteLoop"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""0"" maxOccurs=""99"" name=""Node"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""99"" name=""NoteQualifier"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""99"" name=""NoteFunction"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""99"" name=""NoteLanguage"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""99"" name=""Note"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Currency"" type=""xs:string"" />
        <xs:element minOccurs=""0"" maxOccurs=""1"" name=""DTMReference"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""0"" maxOccurs=""99"" name=""Node"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" name=""Code"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" name=""TimeStamp"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""0"" maxOccurs=""1"" name=""OrderReference"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""SalesOrderID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CustomerReference"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""99"" name=""Node"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Code"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Reference"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""1"" maxOccurs=""1"" name=""SupplierCustomerParty"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""1"" maxOccurs=""1"" name=""AccSellerID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PartyID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CompanyID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CompanyTaxID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""SchemeID"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Endpoint"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Party"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Company"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CompanyTax"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" name=""SchemeAgencyID"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" name=""Endpoint"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" name=""Party"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" name=""Company"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Address"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Name"" type=""xs:string"" />
                    <xs:element name=""AddressFormatCode"">
                      <xs:complexType>
                        <xs:simpleContent>
                          <xs:extension base=""xs:string"">
                            <xs:attribute name=""listAgencyID"" type=""xs:string"" />
                            <xs:attribute name=""listID"" type=""xs:string"" />
                          </xs:extension>
                        </xs:simpleContent>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Postbox"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Street"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Number"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AdditionalStreetName"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Department"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""City"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PostalCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Country"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Concact"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element name=""ID"">
                      <xs:complexType>
                        <xs:simpleContent>
                          <xs:extension base=""xs:string"">
                            <xs:attribute name=""schemeID"" type=""xs:string"" />
                            <xs:attribute name=""schemeAgencyID"" type=""xs:string"" />
                          </xs:extension>
                        </xs:simpleContent>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Name"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Telephone"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Telefax"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""E-mail"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Note"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""RFFNADLoop"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""5"" name=""Node"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Code"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Reference"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name=""AccountingCustomer"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""1"" maxOccurs=""1"" name=""AccBuyerID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PartyID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CompanyID"" type=""xs:string"" />
              <xs:element name=""SchemeID"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Endpoint"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Party"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Company"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" name=""SchemeAgencyID"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" name=""Endpoint"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" name=""Party"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" name=""Company"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Address"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Name"" type=""xs:string"" />
                    <xs:element name=""AddressFormatCode"">
                      <xs:complexType>
                        <xs:simpleContent>
                          <xs:extension base=""xs:string"">
                            <xs:attribute name=""listAgencyID"" type=""xs:string"" />
                            <xs:attribute name=""listID"" type=""xs:string"" />
                          </xs:extension>
                        </xs:simpleContent>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Postbox"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Street"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Number"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AdditionalStreetName"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Department"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""City"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PostalCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Country"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Concact"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element name=""ID"">
                      <xs:complexType>
                        <xs:simpleContent>
                          <xs:extension base=""xs:string"">
                            <xs:attribute name=""schemeID"" type=""xs:string"" />
                            <xs:attribute name=""schemeAgencyID"" type=""xs:string"" />
                          </xs:extension>
                        </xs:simpleContent>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Name"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Telephone"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Telefax"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""E-mail"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Note"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""RFFNADLoop"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""5"" name=""Node"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Code"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Reference"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element maxOccurs=""1"" name=""BuyerCustomerParty"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""1"" maxOccurs=""1"" name=""BuyerID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PartyID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CompanyID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""SchemeID"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Endpoint"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Party"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Company"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" name=""SchemeAgencyID"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" name=""Endpoint"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" name=""Party"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" name=""Company"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Address"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Name"" type=""xs:string"" />
                    <xs:element name=""AddressFormatCode"">
                      <xs:complexType>
                        <xs:simpleContent>
                          <xs:extension base=""xs:string"">
                            <xs:attribute name=""listAgencyID"" type=""xs:string"" />
                            <xs:attribute name=""listID"" type=""xs:string"" />
                          </xs:extension>
                        </xs:simpleContent>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Postbox"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Street"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Number"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AdditionalStreetName"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Department"" type=""xs:string"" />
                    <xs:element name=""City"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PostalCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Country"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Concact"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element name=""ID"">
                      <xs:complexType>
                        <xs:simpleContent>
                          <xs:extension base=""xs:string"">
                            <xs:attribute name=""schemeID"" type=""xs:string"" />
                            <xs:attribute name=""schemeAgencyID"" type=""xs:string"" />
                          </xs:extension>
                        </xs:simpleContent>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Name"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Telephone"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Telefax"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""E-mail"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Note"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""RFFNADLoop"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""5"" name=""Node"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Code"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Reference"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name=""Delivery"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""0"" name=""DeliveryLocation"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" name=""DeliveryID"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" name=""Description"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" name=""PartyID"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" name=""CompanyID"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" name=""Address"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" name=""Name"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" name=""AddressFormatCode"">
                            <xs:complexType>
                              <xs:simpleContent>
                                <xs:extension base=""xs:string"">
                                  <xs:attribute name=""listAgencyID"" type=""xs:string"" />
                                  <xs:attribute name=""listID"" type=""xs:string"" />
                                </xs:extension>
                              </xs:simpleContent>
                            </xs:complexType>
                          </xs:element>
                          <xs:element minOccurs=""0"" name=""Postbox"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" name=""Street"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" name=""Number"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" name=""AdditionalStreetName"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" name=""Department"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" name=""MarkAttention"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" name=""City"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" name=""PostalCode"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" name=""Country"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" name=""SchemeID"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" name=""Endpoint"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" name=""Party"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" name=""Company"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" name=""SchemeAgencyID"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" name=""Endpoint"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" name=""Party"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" name=""Company"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" name=""Concact"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" name=""ID"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" name=""Name"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" name=""Telephone"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" name=""Telefax"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" name=""E-mail"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" name=""Note"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" name=""DeliveryInfo"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" name=""DeliveryDate"" type=""xs:date"" />
                    <xs:element minOccurs=""0"" name=""DeliveryPeriod"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" name=""StartDate"" type=""xs:date"" />
                          <xs:element minOccurs=""0"" name=""EndDate"" type=""xs:date"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" name=""DeliveryTerms"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""SpecialTerms"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""SpecialTerms2"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""SpecialTerms3"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""SpecialTerms4"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""SpecialTerms5"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""SpecialTermsCode"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""SpecialTermsCode2"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""RFFNADLoop"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""5"" name=""Node"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Code"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Reference"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""0"" maxOccurs=""5"" name=""TaxTotal"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxAmount"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxAmountCode"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Currency"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxSubtotal"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxAmount"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxAmountCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxAmountCurrency"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxableAmount"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxableAmountCode"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxableAmountCurrency"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxPercent"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxCategory"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Percent"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxScheme"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Navn"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxTypeCode"" type=""xs:string"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""0"" maxOccurs=""99"" name=""AllowanceCharge"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ChargeIndicator"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AllowanceChargeReasonCode"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AllowanceChargeReason"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""MultiplierFactorNumeric"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PrepaidIndicator"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""SequenceNumeric"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Amount"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""BaseAmount"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AccountingCost"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxCategory"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Percent"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PerUnitAmount"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxScheme"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Navn"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name=""Total"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""LineTotalAmount"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxExclAmount"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxInclAmount"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AllowanceTotalAmount"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ChargeTotalAmount"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PrepaidAmount"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PayableRoundingAmount"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PayableAmount"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""5"" name=""ExtraTotalMOALoop1"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" maxOccurs=""99"" name=""Node"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Code"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Amount"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Currency"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""1"" maxOccurs=""unbounded"" name=""Lines"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""1"" maxOccurs=""unbounded"" name=""Line"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""1"" maxOccurs=""1"" name=""LineNo"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Note"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" name=""LineAmountTotal"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" name=""Currency"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" maxOccurs=""99"" name=""NoteLoop"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" maxOccurs=""99"" name=""Node"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element minOccurs=""0"" maxOccurs=""99"" name=""NoteQualifier"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""99"" name=""NoteFunction"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""99"" name=""NoteLanguage"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""99"" name=""Note"" type=""xs:string"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Quantity"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" maxOccurs=""99"" name=""Node"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Code"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Quantity"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""UnitCode"" type=""xs:string"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""DTMReference"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" maxOccurs=""99"" name=""Node"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Code"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TimeStamp"" type=""xs:string"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" name=""OrderReference"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" name=""SalesOrderID"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" name=""CustomerReference"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""99"" name=""Node"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element minOccurs=""0"" name=""Code"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" name=""Reference"" type=""xs:string"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""1"" maxOccurs=""1"" name=""Item"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" name=""StandardItemID"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""BuyerItemID"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""SellerItemID"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AdditionalItemID"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CatalogueItemID"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ManufacturerItemID"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""2"" name=""Name"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""2"" name=""Description"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PackQuantity"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""UnitCode"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ReferenceNo"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""5"" name=""ExtraPIALoop"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element minOccurs=""0"" maxOccurs=""99"" name=""Node"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element minOccurs=""0"" maxOccurs=""1"" name=""DigialCode"" type=""xs:string"" />
                                      <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PIA"" type=""xs:string"" />
                                      <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Code"" type=""xs:string"" />
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element minOccurs=""0"" name=""SchemeID"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""StdItemID"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""BuyItemID"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""SelItemID"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AddItemId"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""CatItemID"" type=""xs:string"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element minOccurs=""0"" name=""SchemeAgencyID"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element minOccurs=""0"" name=""StdItemID"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" name=""BuyItemID"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" name=""SelItemID"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" name=""AddItemId"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" name=""CatItemID"" type=""xs:string"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PriceNet"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Price"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Quantity"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""UnitCode"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PristypeCode"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PristypeTekst"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PriceGross"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Price"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Quantity"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""UnitCode"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PristypeCode"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PristypeTekst"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""5"" name=""TaxTotal"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxAmount"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxAmountCode"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Currency"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxSubtotal"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxAmount"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxAmountCode"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxAmountCurrency"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxableAmount"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxableAmountCode"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxableAmountCurrency"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxPercent"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxCategory"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
                                      <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Percent"" type=""xs:string"" />
                                      <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxScheme"">
                                        <xs:complexType>
                                          <xs:sequence>
                                            <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
                                            <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Navn"" type=""xs:string"" />
                                            <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxTypeCode"" type=""xs:string"" />
                                          </xs:sequence>
                                        </xs:complexType>
                                      </xs:element>
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" maxOccurs=""99"" name=""AllowanceCharge"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ChargeIndicator"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AllowanceChargeReasonCode"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""AllowanceChargeReason"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""MultiplierFactorNumeric"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PrepaidIndicator"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""SequenceNumeric"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Amount"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""BaseAmount"" type=""xs:string"" />
                          <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxCategory"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Percent"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""PerUnitAmount"" type=""xs:string"" />
                                <xs:element minOccurs=""0"" maxOccurs=""1"" name=""TaxScheme"">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ID"" type=""xs:string"" />
                                      <xs:element minOccurs=""0"" maxOccurs=""1"" name=""Navn"" type=""xs:string"" />
                                    </xs:sequence>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" name=""SchemeID"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" name=""LineID"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element minOccurs=""0"" name=""SchemeAgencyID"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element minOccurs=""0"" name=""LineID"" type=""xs:string"" />
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Extra"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""0"" maxOccurs=""1"" name=""ExtraID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" maxOccurs=""unbounded"" name=""SpecialText"" type=""xs:string"" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
</xs:schema>";
        
        private const global::BYGE.Integration.Core.Properties.CoreProperties  __DummyVar0 = null;
        
        public Order() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [1];
                _RootElements[0] = "Order";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
    }
}
