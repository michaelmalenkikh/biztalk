namespace EDIFACTPipelineComponents.Library
{
    using System;
    using System.Collections.Generic;
    using Microsoft.BizTalk.PipelineOM;
    using Microsoft.BizTalk.Component;
    using Microsoft.BizTalk.Component.Interop;
    
    
    public sealed class BYGE_OriginalEDISend : Microsoft.BizTalk.PipelineOM.SendPipeline
    {
        
        private const string _strPipeline = "<?xml version=\"1.0\" encoding=\"utf-16\"?><Document xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instanc"+
"e\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" MajorVersion=\"1\" MinorVersion=\"0\">  <Description /> "+
" <CategoryId>8c6b051c-0ff5-4fc2-9ae5-5016cb726282</CategoryId>  <FriendlyName>Transmit</FriendlyName"+
">  <Stages>    <Stage>      <PolicyFileStage _locAttrData=\"Name\" _locID=\"1\" Name=\"Pre-Assemble\" minO"+
"ccurs=\"0\" maxOccurs=\"-1\" execMethod=\"All\" stageId=\"9d0e4101-4cce-4536-83fa-4a5040674ad6\" />      <Co"+
"mponents>        <Component>          <Name>EDIFACTPipelineComponents.Library.ManipulateEDIFACTCompo"+
"nent,EDIFACTPipelineComponents.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null</Name>"+
"          <ComponentName>ManipulateEDIFACTComponent</ComponentName>          <Description>Generic Ve"+
"rtica PipelineComponent Base Class</Description>          <Version>1.0.0.0</Version>          <Prope"+
"rties>            <Property Name=\"PropertyNamespace\" />            <Property Name=\"UNB21PropertyName"+
"\" />            <Property Name=\"UNB22PropertyName\" />            <Property Name=\"UNB31PropertyName\" "+
"/>            <Property Name=\"UNB32PropertyName\" />            <Property Name=\"TestFlagPropertyName\""+
" />            <Property Name=\"UNB21AltPropertyName\" />            <Property Name=\"UNB22AltPropertyN"+
"ame\" />            <Property Name=\"UNB31AltPropertyName\" />            <Property Name=\"UNB32AltPrope"+
"rtyName\" />            <Property Name=\"OriginalEDIFACTOverride\">              <Value xsi:type=\"xsd:b"+
"oolean\">true</Value>            </Property>            <Property Name=\"OverrideInterchangeId\">      "+
"        <Value xsi:type=\"xsd:boolean\">true</Value>            </Property>            <Property Name="+
"\"Debug\">              <Value xsi:type=\"xsd:boolean\">false</Value>            </Property>            "+
"<Property Name=\"Enabled\">              <Value xsi:type=\"xsd:boolean\">true</Value>            </Prope"+
"rty>            <Property Name=\"Encoding\">              <Value xsi:type=\"xsd:string\">ISO-8859-1</Val"+
"ue>            </Property>          </Properties>          <CachedDisplayName>ManipulateEDIFACTCompo"+
"nent</CachedDisplayName>          <CachedIsManaged>true</CachedIsManaged>        </Component>      <"+
"/Components>    </Stage>    <Stage>      <PolicyFileStage _locAttrData=\"Name\" _locID=\"2\" Name=\"Assem"+
"ble\" minOccurs=\"0\" maxOccurs=\"1\" execMethod=\"All\" stageId=\"9d0e4107-4cce-4536-83fa-4a5040674ad6\" /> "+
"     <Components>        <Component>          <Name>Microsoft.BizTalk.Edi.Pipelines.EdiAssembler,Mic"+
"rosoft.BizTalk.Edi.PipelineComponents, Version=3.0.1.0, Culture=neutral, PublicKeyToken=31bf3856ad36"+
"4e35</Name>          <ComponentName>EDI Assembler</ComponentName>          <Description>EDI Assemble"+
"r</Description>          <Version>1.1</Version>          <Properties>            <Property Name=\"Ove"+
"rrideFallbackSettings\">              <Value xsi:type=\"xsd:boolean\">true</Value>            </Propert"+
"y>            <Property Name=\"XmlSchemaValidation\">              <Value xsi:type=\"xsd:boolean\">false"+
"</Value>            </Property>            <Property Name=\"EdiDataValidation\">              <Value x"+
"si:type=\"xsd:boolean\">true</Value>            </Property>            <Property Name=\"AllowTrailingDe"+
"limiters\">              <Value xsi:type=\"xsd:boolean\">false</Value>            </Property>          "+
"  <Property Name=\"CharacterSet\">              <Value xsi:type=\"xsd:string\">UTF8</Value>            <"+
"/Property>          </Properties>          <CachedDisplayName>EDI Assembler</CachedDisplayName>     "+
"     <CachedIsManaged>true</CachedIsManaged>        </Component>      </Components>    </Stage>    <"+
"Stage>      <PolicyFileStage _locAttrData=\"Name\" _locID=\"3\" Name=\"Encode\" minOccurs=\"0\" maxOccurs=\"-"+
"1\" execMethod=\"All\" stageId=\"9d0e4108-4cce-4536-83fa-4a5040674ad6\" />      <Components />    </Stage"+
">  </Stages></Document>";
        
        private const string _versionDependentGuid = "bac26507-294d-41be-af55-13c80b318a8e";
        
        public BYGE_OriginalEDISend()
        {
            Microsoft.BizTalk.PipelineOM.Stage stage = this.AddStage(new System.Guid("9d0e4101-4cce-4536-83fa-4a5040674ad6"), Microsoft.BizTalk.PipelineOM.ExecutionMode.all);
            IBaseComponent comp0 = Microsoft.BizTalk.PipelineOM.PipelineManager.CreateComponent("EDIFACTPipelineComponents.Library.ManipulateEDIFACTComponent,EDIFACTPipelineComponents.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null");;
            if (comp0 is IPersistPropertyBag)
            {
                string comp0XmlProperties = "<?xml version=\"1.0\" encoding=\"utf-16\"?><PropertyBag xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-inst"+
"ance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">  <Properties>    <Property Name=\"PropertyNamespa"+
"ce\" />    <Property Name=\"UNB21PropertyName\" />    <Property Name=\"UNB22PropertyName\" />    <Propert"+
"y Name=\"UNB31PropertyName\" />    <Property Name=\"UNB32PropertyName\" />    <Property Name=\"TestFlagPr"+
"opertyName\" />    <Property Name=\"UNB21AltPropertyName\" />    <Property Name=\"UNB22AltPropertyName\" "+
"/>    <Property Name=\"UNB31AltPropertyName\" />    <Property Name=\"UNB32AltPropertyName\" />    <Prope"+
"rty Name=\"OriginalEDIFACTOverride\">      <Value xsi:type=\"xsd:boolean\">true</Value>    </Property>  "+
"  <Property Name=\"OverrideInterchangeId\">      <Value xsi:type=\"xsd:boolean\">true</Value>    </Prope"+
"rty>    <Property Name=\"Debug\">      <Value xsi:type=\"xsd:boolean\">false</Value>    </Property>    <"+
"Property Name=\"Enabled\">      <Value xsi:type=\"xsd:boolean\">true</Value>    </Property>    <Property"+
" Name=\"Encoding\">      <Value xsi:type=\"xsd:string\">ISO-8859-1</Value>    </Property>  </Properties>"+
"</PropertyBag>";
                PropertyBag pb = PropertyBag.DeserializeFromXml(comp0XmlProperties);;
                ((IPersistPropertyBag)(comp0)).Load(pb, 0);
            }
            this.AddComponent(stage, comp0);
            stage = this.AddStage(new System.Guid("9d0e4107-4cce-4536-83fa-4a5040674ad6"), Microsoft.BizTalk.PipelineOM.ExecutionMode.all);
            IBaseComponent comp1 = Microsoft.BizTalk.PipelineOM.PipelineManager.CreateComponent("Microsoft.BizTalk.Edi.Pipelines.EdiAssembler,Microsoft.BizTalk.Edi.PipelineComponents, Version=3.0.1.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35");;
            if (comp1 is IPersistPropertyBag)
            {
                string comp1XmlProperties = "<?xml version=\"1.0\" encoding=\"utf-16\"?><PropertyBag xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-inst"+
"ance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">  <Properties>    <Property Name=\"OverrideFallbac"+
"kSettings\">      <Value xsi:type=\"xsd:boolean\">true</Value>    </Property>    <Property Name=\"XmlSch"+
"emaValidation\">      <Value xsi:type=\"xsd:boolean\">false</Value>    </Property>    <Property Name=\"E"+
"diDataValidation\">      <Value xsi:type=\"xsd:boolean\">true</Value>    </Property>    <Property Name="+
"\"AllowTrailingDelimiters\">      <Value xsi:type=\"xsd:boolean\">false</Value>    </Property>    <Prope"+
"rty Name=\"CharacterSet\">      <Value xsi:type=\"xsd:string\">UTF8</Value>    </Property>  </Properties"+
"></PropertyBag>";
                PropertyBag pb = PropertyBag.DeserializeFromXml(comp1XmlProperties);;
                ((IPersistPropertyBag)(comp1)).Load(pb, 0);
            }
            this.AddComponent(stage, comp1);
        }
        
        public override string XmlContent
        {
            get
            {
                return _strPipeline;
            }
        }
        
        public override System.Guid VersionDependentGuid
        {
            get
            {
                return new System.Guid(_versionDependentGuid);
            }
        }
    }
}
