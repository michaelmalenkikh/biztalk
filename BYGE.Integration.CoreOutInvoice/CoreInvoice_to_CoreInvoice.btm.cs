namespace BYGE.Integration.CoreOutInvoice {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.Core.Invoice.Invoice", typeof(global::BYGE.Integration.Core.Invoice.Invoice))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.Core.Invoice.Invoice", typeof(global::BYGE.Integration.Core.Invoice.Invoice))]
    public sealed class CoreInvoice_to_CoreInvoice : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var"" version=""1.0"" xmlns:ns0=""http://byg-e.dk/schemas/v10"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/ns0:Invoice"" />
  </xsl:template>
  <xsl:template match=""/ns0:Invoice"">
    <ns0:Invoice>
      <ProfileID>
        <xsl:value-of select=""ProfileID/text()"" />
      </ProfileID>
      <InvoiceID>
        <xsl:value-of select=""InvoiceID/text()"" />
      </InvoiceID>
      <xsl:if test=""CopyIndicator"">
        <CopyIndicator>
          <xsl:value-of select=""CopyIndicator/text()"" />
        </CopyIndicator>
      </xsl:if>
      <xsl:if test=""UUID"">
        <UUID>
          <xsl:value-of select=""UUID/text()"" />
        </UUID>
      </xsl:if>
      <IssueDate>
        <xsl:value-of select=""IssueDate/text()"" />
      </IssueDate>
      <InvoiceType>
        <xsl:value-of select=""InvoiceType/text()"" />
      </InvoiceType>
      <xsl:for-each select=""Note"">
        <Note>
          <xsl:value-of select=""./text()"" />
        </Note>
      </xsl:for-each>
      <xsl:if test=""Currency"">
        <Currency>
          <xsl:value-of select=""Currency/text()"" />
        </Currency>
      </xsl:if>
      <OrderReference>
        <OrderID>
          <xsl:value-of select=""OrderReference/OrderID/text()"" />
        </OrderID>
        <xsl:if test=""OrderReference/SalesOrderID"">
          <SalesOrderID>
            <xsl:value-of select=""OrderReference/SalesOrderID/text()"" />
          </SalesOrderID>
        </xsl:if>
        <xsl:if test=""OrderReference/OrderDate"">
          <OrderDate>
            <xsl:value-of select=""OrderReference/OrderDate/text()"" />
          </OrderDate>
        </xsl:if>
        <xsl:if test=""OrderReference/CustomerReference"">
          <CustomerReference>
            <xsl:value-of select=""OrderReference/CustomerReference/text()"" />
          </CustomerReference>
        </xsl:if>
        <xsl:value-of select=""OrderReference/text()"" />
      </OrderReference>
      <xsl:for-each select=""DespatchDocumentReference"">
        <DespatchDocumentReference>
          <ID>
            <xsl:value-of select=""ID/text()"" />
          </ID>
          <xsl:value-of select=""./text()"" />
        </DespatchDocumentReference>
      </xsl:for-each>
      <AccountingSupplier>
        <AccSellerID>
          <xsl:value-of select=""AccountingSupplier/AccSellerID/text()"" />
        </AccSellerID>
        <xsl:if test=""AccountingSupplier/PartyID"">
          <PartyID>
            <xsl:value-of select=""AccountingSupplier/PartyID/text()"" />
          </PartyID>
        </xsl:if>
        <xsl:if test=""AccountingSupplier/CompanyID"">
          <CompanyID>
            <xsl:value-of select=""AccountingSupplier/CompanyID/text()"" />
          </CompanyID>
        </xsl:if>
        <CompanyTaxID>
          <xsl:value-of select=""AccountingSupplier/CompanyTaxID/text()"" />
        </CompanyTaxID>
        <SchemeID>
          <xsl:if test=""AccountingSupplier/SchemeID/Endpoint"">
            <Endpoint>
              <xsl:value-of select=""AccountingSupplier/SchemeID/Endpoint/text()"" />
            </Endpoint>
          </xsl:if>
          <xsl:if test=""AccountingSupplier/SchemeID/Party"">
            <Party>
              <xsl:value-of select=""AccountingSupplier/SchemeID/Party/text()"" />
            </Party>
          </xsl:if>
          <xsl:if test=""AccountingSupplier/SchemeID/Company"">
            <Company>
              <xsl:value-of select=""AccountingSupplier/SchemeID/Company/text()"" />
            </Company>
          </xsl:if>
          <xsl:if test=""AccountingSupplier/SchemeID/CompanyTax"">
            <CompanyTax>
              <xsl:value-of select=""AccountingSupplier/SchemeID/CompanyTax/text()"" />
            </CompanyTax>
          </xsl:if>
          <xsl:value-of select=""AccountingSupplier/SchemeID/text()"" />
        </SchemeID>
        <xsl:for-each select=""AccountingSupplier/Address"">
          <Address>
            <xsl:if test=""Name"">
              <Name>
                <xsl:value-of select=""Name/text()"" />
              </Name>
            </xsl:if>
            <xsl:if test=""Postbox"">
              <Postbox>
                <xsl:value-of select=""Postbox/text()"" />
              </Postbox>
            </xsl:if>
            <xsl:if test=""AddressFormatCode"">
              <AddressFormatCode>
                <xsl:value-of select=""AddressFormatCode/text()"" />
              </AddressFormatCode>
            </xsl:if>
            <xsl:if test=""Street"">
              <Street>
                <xsl:value-of select=""Street/text()"" />
              </Street>
            </xsl:if>
            <xsl:if test=""Number"">
              <Number>
                <xsl:value-of select=""Number/text()"" />
              </Number>
            </xsl:if>
            <xsl:if test=""AdditionalStreetName"">
              <AdditionalStreetName>
                <xsl:value-of select=""AdditionalStreetName/text()"" />
              </AdditionalStreetName>
            </xsl:if>
            <xsl:if test=""Department"">
              <Department>
                <xsl:value-of select=""Department/text()"" />
              </Department>
            </xsl:if>
            <xsl:if test=""City"">
              <City>
                <xsl:value-of select=""City/text()"" />
              </City>
            </xsl:if>
            <xsl:if test=""PostalCode"">
              <PostalCode>
                <xsl:value-of select=""PostalCode/text()"" />
              </PostalCode>
            </xsl:if>
            <xsl:if test=""Country"">
              <Country>
                <xsl:value-of select=""Country/text()"" />
              </Country>
            </xsl:if>
            <xsl:value-of select=""./text()"" />
          </Address>
        </xsl:for-each>
        <xsl:for-each select=""AccountingSupplier/Concact"">
          <Concact>
            <ID>
              <xsl:value-of select=""ID/text()"" />
            </ID>
            <xsl:if test=""Name"">
              <Name>
                <xsl:value-of select=""Name/text()"" />
              </Name>
            </xsl:if>
            <xsl:if test=""Telephone"">
              <Telephone>
                <xsl:value-of select=""Telephone/text()"" />
              </Telephone>
            </xsl:if>
            <xsl:if test=""Telefax"">
              <Telefax>
                <xsl:value-of select=""Telefax/text()"" />
              </Telefax>
            </xsl:if>
            <xsl:if test=""E-mail"">
              <E-mail>
                <xsl:value-of select=""E-mail/text()"" />
              </E-mail>
            </xsl:if>
            <xsl:if test=""Note"">
              <Note>
                <xsl:value-of select=""Note/text()"" />
              </Note>
            </xsl:if>
            <xsl:value-of select=""./text()"" />
          </Concact>
        </xsl:for-each>
        <xsl:value-of select=""AccountingSupplier/text()"" />
      </AccountingSupplier>
      <AccountingCustomer>
        <xsl:if test=""AccountingCustomer/SupplierAssignedAccountID"">
          <SupplierAssignedAccountID>
            <xsl:value-of select=""AccountingCustomer/SupplierAssignedAccountID/text()"" />
          </SupplierAssignedAccountID>
        </xsl:if>
        <AccBuyerID>
          <xsl:value-of select=""AccountingCustomer/AccBuyerID/text()"" />
        </AccBuyerID>
        <xsl:if test=""AccountingCustomer/PartyID"">
          <PartyID>
            <xsl:value-of select=""AccountingCustomer/PartyID/text()"" />
          </PartyID>
        </xsl:if>
        <xsl:if test=""AccountingCustomer/CompanyID"">
          <CompanyID>
            <xsl:value-of select=""AccountingCustomer/CompanyID/text()"" />
          </CompanyID>
        </xsl:if>
        <SchemeID>
          <xsl:if test=""AccountingCustomer/SchemeID/Endpoint"">
            <Endpoint>
              <xsl:value-of select=""AccountingCustomer/SchemeID/Endpoint/text()"" />
            </Endpoint>
          </xsl:if>
          <xsl:if test=""AccountingCustomer/SchemeID/Party"">
            <Party>
              <xsl:value-of select=""AccountingCustomer/SchemeID/Party/text()"" />
            </Party>
          </xsl:if>
          <xsl:if test=""AccountingCustomer/SchemeID/Company"">
            <Company>
              <xsl:value-of select=""AccountingCustomer/SchemeID/Company/text()"" />
            </Company>
          </xsl:if>
          <xsl:value-of select=""AccountingCustomer/SchemeID/text()"" />
        </SchemeID>
        <xsl:for-each select=""AccountingCustomer/Address"">
          <Address>
            <xsl:if test=""Name"">
              <Name>
                <xsl:value-of select=""Name/text()"" />
              </Name>
            </xsl:if>
            <xsl:if test=""AddressFormatCode"">
              <AddressFormatCode>
                <xsl:value-of select=""AddressFormatCode/text()"" />
              </AddressFormatCode>
            </xsl:if>
            <xsl:if test=""Postbox"">
              <Postbox>
                <xsl:value-of select=""Postbox/text()"" />
              </Postbox>
            </xsl:if>
            <xsl:if test=""Street"">
              <Street>
                <xsl:value-of select=""Street/text()"" />
              </Street>
            </xsl:if>
            <xsl:if test=""Number"">
              <Number>
                <xsl:value-of select=""Number/text()"" />
              </Number>
            </xsl:if>
            <xsl:if test=""AdditionalStreetName"">
              <AdditionalStreetName>
                <xsl:value-of select=""AdditionalStreetName/text()"" />
              </AdditionalStreetName>
            </xsl:if>
            <xsl:if test=""Department"">
              <Department>
                <xsl:value-of select=""Department/text()"" />
              </Department>
            </xsl:if>
            <xsl:if test=""City"">
              <City>
                <xsl:value-of select=""City/text()"" />
              </City>
            </xsl:if>
            <xsl:if test=""PostalCode"">
              <PostalCode>
                <xsl:value-of select=""PostalCode/text()"" />
              </PostalCode>
            </xsl:if>
            <xsl:if test=""Country"">
              <Country>
                <xsl:value-of select=""Country/text()"" />
              </Country>
            </xsl:if>
            <xsl:value-of select=""./text()"" />
          </Address>
        </xsl:for-each>
        <xsl:for-each select=""AccountingCustomer/Concact"">
          <Concact>
            <ID>
              <xsl:value-of select=""ID/text()"" />
            </ID>
            <xsl:if test=""Name"">
              <Name>
                <xsl:value-of select=""Name/text()"" />
              </Name>
            </xsl:if>
            <xsl:if test=""Telephone"">
              <Telephone>
                <xsl:value-of select=""Telephone/text()"" />
              </Telephone>
            </xsl:if>
            <xsl:if test=""Telefax"">
              <Telefax>
                <xsl:value-of select=""Telefax/text()"" />
              </Telefax>
            </xsl:if>
            <xsl:if test=""E-mail"">
              <E-mail>
                <xsl:value-of select=""E-mail/text()"" />
              </E-mail>
            </xsl:if>
            <xsl:if test=""Note"">
              <Note>
                <xsl:value-of select=""Note/text()"" />
              </Note>
            </xsl:if>
            <xsl:value-of select=""./text()"" />
          </Concact>
        </xsl:for-each>
        <xsl:value-of select=""AccountingCustomer/text()"" />
      </AccountingCustomer>
      <BuyerCustomer>
        <BuyerID>
          <xsl:value-of select=""BuyerCustomer/BuyerID/text()"" />
        </BuyerID>
        <xsl:if test=""BuyerCustomer/PartyID"">
          <PartyID>
            <xsl:value-of select=""BuyerCustomer/PartyID/text()"" />
          </PartyID>
        </xsl:if>
        <xsl:if test=""BuyerCustomer/CompanyID"">
          <CompanyID>
            <xsl:value-of select=""BuyerCustomer/CompanyID/text()"" />
          </CompanyID>
        </xsl:if>
        <xsl:for-each select=""BuyerCustomer/SchemeID"">
          <SchemeID>
            <xsl:if test=""Endpoint"">
              <Endpoint>
                <xsl:value-of select=""Endpoint/text()"" />
              </Endpoint>
            </xsl:if>
            <xsl:if test=""Party"">
              <Party>
                <xsl:value-of select=""Party/text()"" />
              </Party>
            </xsl:if>
            <xsl:if test=""Company"">
              <Company>
                <xsl:value-of select=""Company/text()"" />
              </Company>
            </xsl:if>
            <xsl:value-of select=""./text()"" />
          </SchemeID>
        </xsl:for-each>
        <xsl:for-each select=""BuyerCustomer/Address"">
          <Address>
            <Name>
              <xsl:value-of select=""Name/text()"" />
            </Name>
            <xsl:if test=""AddressFormatCode"">
              <AddressFormatCode>
                <xsl:value-of select=""AddressFormatCode/text()"" />
              </AddressFormatCode>
            </xsl:if>
            <xsl:if test=""Postbox"">
              <Postbox>
                <xsl:value-of select=""Postbox/text()"" />
              </Postbox>
            </xsl:if>
            <xsl:if test=""Street"">
              <Street>
                <xsl:value-of select=""Street/text()"" />
              </Street>
            </xsl:if>
            <xsl:if test=""Number"">
              <Number>
                <xsl:value-of select=""Number/text()"" />
              </Number>
            </xsl:if>
            <xsl:if test=""AdditionalStreetName"">
              <AdditionalStreetName>
                <xsl:value-of select=""AdditionalStreetName/text()"" />
              </AdditionalStreetName>
            </xsl:if>
            <xsl:if test=""Department"">
              <Department>
                <xsl:value-of select=""Department/text()"" />
              </Department>
            </xsl:if>
            <xsl:if test=""City"">
              <City>
                <xsl:value-of select=""City/text()"" />
              </City>
            </xsl:if>
            <xsl:if test=""PostalCode"">
              <PostalCode>
                <xsl:value-of select=""PostalCode/text()"" />
              </PostalCode>
            </xsl:if>
            <xsl:if test=""Country"">
              <Country>
                <xsl:value-of select=""Country/text()"" />
              </Country>
            </xsl:if>
            <xsl:value-of select=""./text()"" />
          </Address>
        </xsl:for-each>
        <xsl:for-each select=""BuyerCustomer/Concact"">
          <Concact>
            <ID>
              <xsl:value-of select=""ID/text()"" />
            </ID>
            <xsl:if test=""Name"">
              <Name>
                <xsl:value-of select=""Name/text()"" />
              </Name>
            </xsl:if>
            <xsl:if test=""Telephone"">
              <Telephone>
                <xsl:value-of select=""Telephone/text()"" />
              </Telephone>
            </xsl:if>
            <xsl:if test=""Telefax"">
              <Telefax>
                <xsl:value-of select=""Telefax/text()"" />
              </Telefax>
            </xsl:if>
            <xsl:if test=""E-mail"">
              <E-mail>
                <xsl:value-of select=""E-mail/text()"" />
              </E-mail>
            </xsl:if>
            <xsl:if test=""Note"">
              <Note>
                <xsl:value-of select=""Note/text()"" />
              </Note>
            </xsl:if>
            <xsl:value-of select=""./text()"" />
          </Concact>
        </xsl:for-each>
        <xsl:value-of select=""BuyerCustomer/text()"" />
      </BuyerCustomer>
      <xsl:for-each select=""DeliveryLocation"">
        <DeliveryLocation>
          <xsl:if test=""DeliveryID"">
            <DeliveryID>
              <xsl:value-of select=""DeliveryID/text()"" />
            </DeliveryID>
          </xsl:if>
          <xsl:if test=""PartyID"">
            <PartyID>
              <xsl:value-of select=""PartyID/text()"" />
            </PartyID>
          </xsl:if>
          <xsl:if test=""CompanyID"">
            <CompanyID>
              <xsl:value-of select=""CompanyID/text()"" />
            </CompanyID>
          </xsl:if>
          <xsl:for-each select=""SchemeID"">
            <SchemeID>
              <xsl:if test=""Endpoint"">
                <Endpoint>
                  <xsl:value-of select=""Endpoint/text()"" />
                </Endpoint>
              </xsl:if>
              <xsl:if test=""Party"">
                <Party>
                  <xsl:value-of select=""Party/text()"" />
                </Party>
              </xsl:if>
              <xsl:if test=""Company"">
                <Company>
                  <xsl:value-of select=""Company/text()"" />
                </Company>
              </xsl:if>
              <xsl:value-of select=""./text()"" />
            </SchemeID>
          </xsl:for-each>
          <xsl:for-each select=""Address"">
            <Address>
              <Name>
                <xsl:value-of select=""Name/text()"" />
              </Name>
              <xsl:if test=""AddressFormatCode"">
                <AddressFormatCode>
                  <xsl:value-of select=""AddressFormatCode/text()"" />
                </AddressFormatCode>
              </xsl:if>
              <xsl:if test=""Street"">
                <Street>
                  <xsl:value-of select=""Street/text()"" />
                </Street>
              </xsl:if>
              <xsl:if test=""Number"">
                <Number>
                  <xsl:value-of select=""Number/text()"" />
                </Number>
              </xsl:if>
              <xsl:if test=""AdditionalStreetName"">
                <AdditionalStreetName>
                  <xsl:value-of select=""AdditionalStreetName/text()"" />
                </AdditionalStreetName>
              </xsl:if>
              <xsl:if test=""Department"">
                <Department>
                  <xsl:value-of select=""Department/text()"" />
                </Department>
              </xsl:if>
              <xsl:if test=""MarkAttention"">
                <MarkAttention>
                  <xsl:value-of select=""MarkAttention/text()"" />
                </MarkAttention>
              </xsl:if>
              <xsl:if test=""City"">
                <City>
                  <xsl:value-of select=""City/text()"" />
                </City>
              </xsl:if>
              <xsl:if test=""PostalCode"">
                <PostalCode>
                  <xsl:value-of select=""PostalCode/text()"" />
                </PostalCode>
              </xsl:if>
              <xsl:if test=""Country"">
                <Country>
                  <xsl:value-of select=""Country/text()"" />
                </Country>
              </xsl:if>
              <xsl:value-of select=""./text()"" />
            </Address>
          </xsl:for-each>
          <xsl:for-each select=""Concact"">
            <Concact>
              <ID>
                <xsl:value-of select=""ID/text()"" />
              </ID>
              <xsl:if test=""Name"">
                <Name>
                  <xsl:value-of select=""Name/text()"" />
                </Name>
              </xsl:if>
              <xsl:if test=""Telephone"">
                <Telephone>
                  <xsl:value-of select=""Telephone/text()"" />
                </Telephone>
              </xsl:if>
              <xsl:if test=""Telefax"">
                <Telefax>
                  <xsl:value-of select=""Telefax/text()"" />
                </Telefax>
              </xsl:if>
              <xsl:if test=""E-mail"">
                <E-mail>
                  <xsl:value-of select=""E-mail/text()"" />
                </E-mail>
              </xsl:if>
              <xsl:if test=""Note"">
                <Note>
                  <xsl:value-of select=""Note/text()"" />
                </Note>
              </xsl:if>
              <xsl:value-of select=""./text()"" />
            </Concact>
          </xsl:for-each>
          <xsl:value-of select=""./text()"" />
        </DeliveryLocation>
      </xsl:for-each>
      <xsl:for-each select=""DeliveryInfo"">
        <DeliveryInfo>
          <xsl:if test=""ActualDeliveryDate"">
            <ActualDeliveryDate>
              <xsl:value-of select=""ActualDeliveryDate/text()"" />
            </ActualDeliveryDate>
          </xsl:if>
          <xsl:for-each select=""DeliveryTerms"">
            <DeliveryTerms>
              <xsl:if test=""SpecialTerms"">
                <SpecialTerms>
                  <xsl:value-of select=""SpecialTerms/text()"" />
                </SpecialTerms>
              </xsl:if>
              <xsl:value-of select=""./text()"" />
            </DeliveryTerms>
          </xsl:for-each>
          <xsl:value-of select=""./text()"" />
        </DeliveryInfo>
      </xsl:for-each>
      <PaymentMeans>
        <xsl:if test=""PaymentMeans/ID"">
          <ID>
            <xsl:value-of select=""PaymentMeans/ID/text()"" />
          </ID>
        </xsl:if>
        <xsl:if test=""PaymentMeans/PaymentMeansCode"">
          <PaymentMeansCode>
            <xsl:value-of select=""PaymentMeans/PaymentMeansCode/text()"" />
          </PaymentMeansCode>
        </xsl:if>
        <PaymentDueDate>
          <xsl:value-of select=""PaymentMeans/PaymentDueDate/text()"" />
        </PaymentDueDate>
        <xsl:if test=""PaymentMeans/PaymentChannelCode"">
          <PaymentChannelCode>
            <xsl:value-of select=""PaymentMeans/PaymentChannelCode/text()"" />
          </PaymentChannelCode>
        </xsl:if>
        <xsl:if test=""PaymentMeans/InstructionID"">
          <InstructionID>
            <xsl:value-of select=""PaymentMeans/InstructionID/text()"" />
          </InstructionID>
        </xsl:if>
        <xsl:if test=""PaymentMeans/InstructionNote"">
          <InstructionNote>
            <xsl:value-of select=""PaymentMeans/InstructionNote/text()"" />
          </InstructionNote>
        </xsl:if>
        <xsl:if test=""PaymentMeans/PaymentID"">
          <PaymentID>
            <xsl:value-of select=""PaymentMeans/PaymentID/text()"" />
          </PaymentID>
        </xsl:if>
        <xsl:if test=""PaymentMeans/CreditAccountID"">
          <CreditAccountID>
            <xsl:value-of select=""PaymentMeans/CreditAccountID/text()"" />
          </CreditAccountID>
        </xsl:if>
        <xsl:value-of select=""PaymentMeans/text()"" />
      </PaymentMeans>
      <xsl:for-each select=""PaymentTerms"">
        <PaymentTerms>
          <xsl:if test=""ID"">
            <ID>
              <xsl:value-of select=""ID/text()"" />
            </ID>
          </xsl:if>
          <xsl:if test=""PaymentMeansID"">
            <PaymentMeansID>
              <xsl:value-of select=""PaymentMeansID/text()"" />
            </PaymentMeansID>
          </xsl:if>
          <xsl:if test=""PrepaidPaymentReferenceID"">
            <PrepaidPaymentReferenceID>
              <xsl:value-of select=""PrepaidPaymentReferenceID/text()"" />
            </PrepaidPaymentReferenceID>
          </xsl:if>
          <xsl:if test=""Note"">
            <Note>
              <xsl:value-of select=""Note/text()"" />
            </Note>
          </xsl:if>
          <xsl:if test=""ReferenceEventCode"">
            <ReferenceEventCode>
              <xsl:value-of select=""ReferenceEventCode/text()"" />
            </ReferenceEventCode>
          </xsl:if>
          <xsl:if test=""SettlementDiscountPercent"">
            <SettlementDiscountPercent>
              <xsl:value-of select=""SettlementDiscountPercent/text()"" />
            </SettlementDiscountPercent>
          </xsl:if>
          <xsl:if test=""PenaltySurchargePercent"">
            <PenaltySurchargePercent>
              <xsl:value-of select=""PenaltySurchargePercent/text()"" />
            </PenaltySurchargePercent>
          </xsl:if>
          <xsl:if test=""Amount"">
            <Amount>
              <xsl:value-of select=""Amount/text()"" />
            </Amount>
          </xsl:if>
          <xsl:for-each select=""SettlementPeriod"">
            <SettlementPeriod>
              <xsl:if test=""StartDate"">
                <StartDate>
                  <xsl:value-of select=""StartDate/text()"" />
                </StartDate>
              </xsl:if>
              <xsl:if test=""EndDate"">
                <EndDate>
                  <xsl:value-of select=""EndDate/text()"" />
                </EndDate>
              </xsl:if>
              <xsl:if test=""Description"">
                <Description>
                  <xsl:value-of select=""Description/text()"" />
                </Description>
              </xsl:if>
              <xsl:value-of select=""./text()"" />
            </SettlementPeriod>
          </xsl:for-each>
          <xsl:for-each select=""PenaltyPeriod"">
            <PenaltyPeriod>
              <xsl:if test=""StartDate"">
                <StartDate>
                  <xsl:value-of select=""StartDate/text()"" />
                </StartDate>
              </xsl:if>
              <xsl:if test=""EndDate"">
                <EndDate>
                  <xsl:value-of select=""EndDate/text()"" />
                </EndDate>
              </xsl:if>
              <xsl:if test=""Description"">
                <Description>
                  <xsl:value-of select=""Description/text()"" />
                </Description>
              </xsl:if>
              <xsl:value-of select=""./text()"" />
            </PenaltyPeriod>
          </xsl:for-each>
          <xsl:value-of select=""./text()"" />
        </PaymentTerms>
      </xsl:for-each>
      <xsl:for-each select=""AllowanceCharge"">
        <AllowanceCharge>
          <xsl:if test=""ID"">
            <ID>
              <xsl:value-of select=""ID/text()"" />
            </ID>
          </xsl:if>
          <xsl:if test=""ChargeIndicator"">
            <ChargeIndicator>
              <xsl:value-of select=""ChargeIndicator/text()"" />
            </ChargeIndicator>
          </xsl:if>
          <xsl:if test=""AllowanceChargeReasonCode"">
            <AllowanceChargeReasonCode>
              <xsl:value-of select=""AllowanceChargeReasonCode/text()"" />
            </AllowanceChargeReasonCode>
          </xsl:if>
          <xsl:if test=""AllowanceChargeReason"">
            <AllowanceChargeReason>
              <xsl:value-of select=""AllowanceChargeReason/text()"" />
            </AllowanceChargeReason>
          </xsl:if>
          <xsl:if test=""MultiplierFactorNumeric"">
            <MultiplierFactorNumeric>
              <xsl:value-of select=""MultiplierFactorNumeric/text()"" />
            </MultiplierFactorNumeric>
          </xsl:if>
          <xsl:if test=""PrepaidIndicator"">
            <PrepaidIndicator>
              <xsl:value-of select=""PrepaidIndicator/text()"" />
            </PrepaidIndicator>
          </xsl:if>
          <xsl:if test=""SequenceNumeric"">
            <SequenceNumeric>
              <xsl:value-of select=""SequenceNumeric/text()"" />
            </SequenceNumeric>
          </xsl:if>
          <xsl:if test=""Amount"">
            <Amount>
              <xsl:value-of select=""Amount/text()"" />
            </Amount>
          </xsl:if>
          <xsl:if test=""BaseAmount"">
            <BaseAmount>
              <xsl:value-of select=""BaseAmount/text()"" />
            </BaseAmount>
          </xsl:if>
          <xsl:if test=""AccountingCost"">
            <AccountingCost>
              <xsl:value-of select=""AccountingCost/text()"" />
            </AccountingCost>
          </xsl:if>
          <xsl:for-each select=""TaxCategory"">
            <TaxCategory>
              <xsl:if test=""ID"">
                <ID>
                  <xsl:value-of select=""ID/text()"" />
                </ID>
              </xsl:if>
              <xsl:if test=""Percent"">
                <Percent>
                  <xsl:value-of select=""Percent/text()"" />
                </Percent>
              </xsl:if>
              <xsl:if test=""PerUnitAmount"">
                <PerUnitAmount>
                  <xsl:value-of select=""PerUnitAmount/text()"" />
                </PerUnitAmount>
              </xsl:if>
              <xsl:for-each select=""TaxScheme"">
                <TaxScheme>
                  <xsl:if test=""ID"">
                    <ID>
                      <xsl:value-of select=""ID/text()"" />
                    </ID>
                  </xsl:if>
                  <xsl:if test=""Navn"">
                    <Navn>
                      <xsl:value-of select=""Navn/text()"" />
                    </Navn>
                  </xsl:if>
                  <xsl:value-of select=""./text()"" />
                </TaxScheme>
              </xsl:for-each>
              <xsl:value-of select=""./text()"" />
            </TaxCategory>
          </xsl:for-each>
          <xsl:value-of select=""./text()"" />
        </AllowanceCharge>
      </xsl:for-each>
      <xsl:for-each select=""TaxTotal"">
        <TaxTotal>
          <TaxAmount>
            <xsl:value-of select=""TaxAmount/text()"" />
          </TaxAmount>
          <xsl:for-each select=""TaxSubtotal"">
            <TaxSubtotal>
              <xsl:if test=""TaxAmount"">
                <TaxAmount>
                  <xsl:value-of select=""TaxAmount/text()"" />
                </TaxAmount>
              </xsl:if>
              <xsl:if test=""TaxableAmount"">
                <TaxableAmount>
                  <xsl:value-of select=""TaxableAmount/text()"" />
                </TaxableAmount>
              </xsl:if>
              <xsl:if test=""TaxPercent"">
                <TaxPercent>
                  <xsl:value-of select=""TaxPercent/text()"" />
                </TaxPercent>
              </xsl:if>
              <xsl:for-each select=""TaxCategory"">
                <TaxCategory>
                  <xsl:if test=""ID"">
                    <ID>
                      <xsl:value-of select=""ID/text()"" />
                    </ID>
                  </xsl:if>
                  <xsl:if test=""Percent"">
                    <Percent>
                      <xsl:value-of select=""Percent/text()"" />
                    </Percent>
                  </xsl:if>
                  <xsl:for-each select=""TaxScheme"">
                    <TaxScheme>
                      <xsl:if test=""ID"">
                        <ID>
                          <xsl:value-of select=""ID/text()"" />
                        </ID>
                      </xsl:if>
                      <xsl:if test=""Navn"">
                        <Navn>
                          <xsl:value-of select=""Navn/text()"" />
                        </Navn>
                      </xsl:if>
                      <xsl:if test=""TaxTypeCode"">
                        <TaxTypeCode>
                          <xsl:value-of select=""TaxTypeCode/text()"" />
                        </TaxTypeCode>
                      </xsl:if>
                      <xsl:value-of select=""./text()"" />
                    </TaxScheme>
                  </xsl:for-each>
                  <xsl:value-of select=""./text()"" />
                </TaxCategory>
              </xsl:for-each>
              <xsl:value-of select=""./text()"" />
            </TaxSubtotal>
          </xsl:for-each>
          <xsl:value-of select=""./text()"" />
        </TaxTotal>
      </xsl:for-each>
      <Total>
        <xsl:if test=""Total/LineTotalAmount"">
          <LineTotalAmount>
            <xsl:value-of select=""Total/LineTotalAmount/text()"" />
          </LineTotalAmount>
        </xsl:if>
        <xsl:if test=""Total/TaxExclAmount"">
          <TaxExclAmount>
            <xsl:value-of select=""Total/TaxExclAmount/text()"" />
          </TaxExclAmount>
        </xsl:if>
        <xsl:if test=""Total/TaxInclAmount"">
          <TaxInclAmount>
            <xsl:value-of select=""Total/TaxInclAmount/text()"" />
          </TaxInclAmount>
        </xsl:if>
        <xsl:if test=""Total/AllowanceTotalAmount"">
          <AllowanceTotalAmount>
            <xsl:value-of select=""Total/AllowanceTotalAmount/text()"" />
          </AllowanceTotalAmount>
        </xsl:if>
        <xsl:if test=""Total/ChargeTotalAmount"">
          <ChargeTotalAmount>
            <xsl:value-of select=""Total/ChargeTotalAmount/text()"" />
          </ChargeTotalAmount>
        </xsl:if>
        <xsl:if test=""Total/PrepaidAmount"">
          <PrepaidAmount>
            <xsl:value-of select=""Total/PrepaidAmount/text()"" />
          </PrepaidAmount>
        </xsl:if>
        <xsl:if test=""Total/PayableRoundingAmount"">
          <PayableRoundingAmount>
            <xsl:value-of select=""Total/PayableRoundingAmount/text()"" />
          </PayableRoundingAmount>
        </xsl:if>
        <PayableAmount>
          <xsl:value-of select=""Total/PayableAmount/text()"" />
        </PayableAmount>
        <xsl:value-of select=""Total/text()"" />
      </Total>
      <Lines>
        <xsl:for-each select=""Lines/Line"">
          <Line>
            <LineNo>
              <xsl:value-of select=""LineNo/text()"" />
            </LineNo>
            <xsl:if test=""Note"">
              <Note>
                <xsl:value-of select=""Note/text()"" />
              </Note>
            </xsl:if>
            <Quantity>
              <xsl:value-of select=""Quantity/text()"" />
            </Quantity>
            <UnitCode>
              <xsl:value-of select=""UnitCode/text()"" />
            </UnitCode>
            <LineAmountTotal>
              <xsl:value-of select=""LineAmountTotal/text()"" />
            </LineAmountTotal>
            <xsl:if test=""FreeOfChargeIndicator"">
              <FreeOfChargeIndicator>
                <xsl:value-of select=""FreeOfChargeIndicator/text()"" />
              </FreeOfChargeIndicator>
            </xsl:if>
            <xsl:for-each select=""Delivery"">
              <Delivery>
                <xsl:if test=""ID"">
                  <ID>
                    <xsl:value-of select=""ID/text()"" />
                  </ID>
                </xsl:if>
                <xsl:if test=""Quantity"">
                  <Quantity>
                    <xsl:value-of select=""Quantity/text()"" />
                  </Quantity>
                </xsl:if>
                <xsl:if test=""UnitCode"">
                  <UnitCode>
                    <xsl:value-of select=""UnitCode/text()"" />
                  </UnitCode>
                </xsl:if>
                <xsl:value-of select=""./text()"" />
              </Delivery>
            </xsl:for-each>
            <xsl:for-each select=""OrderLineReference"">
              <OrderLineReference>
                <xsl:if test=""LineID"">
                  <LineID>
                    <xsl:value-of select=""LineID/text()"" />
                  </LineID>
                </xsl:if>
                <xsl:for-each select=""OrderReference"">
                  <OrderReference>
                    <xsl:if test=""ID"">
                      <ID>
                        <xsl:value-of select=""ID/text()"" />
                      </ID>
                    </xsl:if>
                    <xsl:value-of select=""./text()"" />
                  </OrderReference>
                </xsl:for-each>
                <xsl:if test=""SalesOrderLineID"">
                  <SalesOrderLineID>
                    <xsl:value-of select=""SalesOrderLineID/text()"" />
                  </SalesOrderLineID>
                </xsl:if>
                <xsl:value-of select=""./text()"" />
              </OrderLineReference>
            </xsl:for-each>
            <Item>
              <xsl:if test=""Item/BuyerItemID"">
                <BuyerItemID>
                  <xsl:value-of select=""Item/BuyerItemID/text()"" />
                </BuyerItemID>
              </xsl:if>
              <xsl:if test=""Item/SellerItemID"">
                <SellerItemID>
                  <xsl:value-of select=""Item/SellerItemID/text()"" />
                </SellerItemID>
              </xsl:if>
              <xsl:if test=""Item/StandardItemID"">
                <StandardItemID>
                  <xsl:value-of select=""Item/StandardItemID/text()"" />
                </StandardItemID>
              </xsl:if>
              <xsl:if test=""Item/AdditionalItemID"">
                <AdditionalItemID>
                  <xsl:value-of select=""Item/AdditionalItemID/text()"" />
                </AdditionalItemID>
              </xsl:if>
              <xsl:if test=""Item/CatalogueItemID"">
                <CatalogueItemID>
                  <xsl:value-of select=""Item/CatalogueItemID/text()"" />
                </CatalogueItemID>
              </xsl:if>
              <xsl:for-each select=""Item/Name"">
                <Name>
                  <xsl:value-of select=""./text()"" />
                </Name>
              </xsl:for-each>
              <xsl:if test=""Item/Description"">
                <Description>
                  <xsl:value-of select=""Item/Description/text()"" />
                </Description>
              </xsl:if>
              <xsl:if test=""Item/PackQuantity"">
                <PackQuantity>
                  <xsl:value-of select=""Item/PackQuantity/text()"" />
                </PackQuantity>
              </xsl:if>
              <xsl:if test=""Item/UnitCode"">
                <UnitCode>
                  <xsl:value-of select=""Item/UnitCode/text()"" />
                </UnitCode>
              </xsl:if>
              <xsl:value-of select=""Item/text()"" />
            </Item>
            <PriceNet>
              <Price>
                <xsl:value-of select=""PriceNet/Price/text()"" />
              </Price>
              <Quantity>
                <xsl:value-of select=""PriceNet/Quantity/text()"" />
              </Quantity>
              <xsl:if test=""PriceNet/UnitCode"">
                <UnitCode>
                  <xsl:value-of select=""PriceNet/UnitCode/text()"" />
                </UnitCode>
              </xsl:if>
              <xsl:if test=""PriceNet/PristypeCode"">
                <PristypeCode>
                  <xsl:value-of select=""PriceNet/PristypeCode/text()"" />
                </PristypeCode>
              </xsl:if>
              <xsl:if test=""PriceNet/PristypeTekst"">
                <PristypeTekst>
                  <xsl:value-of select=""PriceNet/PristypeTekst/text()"" />
                </PristypeTekst>
              </xsl:if>
              <xsl:value-of select=""PriceNet/text()"" />
            </PriceNet>
            <xsl:for-each select=""PriceGross"">
              <PriceGross>
                <xsl:if test=""Price"">
                  <Price>
                    <xsl:value-of select=""Price/text()"" />
                  </Price>
                </xsl:if>
                <xsl:if test=""Quantity"">
                  <Quantity>
                    <xsl:value-of select=""Quantity/text()"" />
                  </Quantity>
                </xsl:if>
                <xsl:if test=""UnitCode"">
                  <UnitCode>
                    <xsl:value-of select=""UnitCode/text()"" />
                  </UnitCode>
                </xsl:if>
                <xsl:if test=""PristypeCode"">
                  <PristypeCode>
                    <xsl:value-of select=""PristypeCode/text()"" />
                  </PristypeCode>
                </xsl:if>
                <xsl:if test=""PristypeTekst"">
                  <PristypeTekst>
                    <xsl:value-of select=""PristypeTekst/text()"" />
                  </PristypeTekst>
                </xsl:if>
                <xsl:value-of select=""./text()"" />
              </PriceGross>
            </xsl:for-each>
            <xsl:for-each select=""AllowanceCharge"">
              <AllowanceCharge>
                <xsl:if test=""ID"">
                  <ID>
                    <xsl:value-of select=""ID/text()"" />
                  </ID>
                </xsl:if>
                <xsl:if test=""ChargeIndicator"">
                  <ChargeIndicator>
                    <xsl:value-of select=""ChargeIndicator/text()"" />
                  </ChargeIndicator>
                </xsl:if>
                <xsl:if test=""AllowanceChargeReasonCode"">
                  <AllowanceChargeReasonCode>
                    <xsl:value-of select=""AllowanceChargeReasonCode/text()"" />
                  </AllowanceChargeReasonCode>
                </xsl:if>
                <xsl:if test=""AllowanceChargeReason"">
                  <AllowanceChargeReason>
                    <xsl:value-of select=""AllowanceChargeReason/text()"" />
                  </AllowanceChargeReason>
                </xsl:if>
                <xsl:if test=""MultiplierFactorNumeric"">
                  <MultiplierFactorNumeric>
                    <xsl:value-of select=""MultiplierFactorNumeric/text()"" />
                  </MultiplierFactorNumeric>
                </xsl:if>
                <xsl:if test=""PrepaidIndicator"">
                  <PrepaidIndicator>
                    <xsl:value-of select=""PrepaidIndicator/text()"" />
                  </PrepaidIndicator>
                </xsl:if>
                <xsl:if test=""SequenceNumeric"">
                  <SequenceNumeric>
                    <xsl:value-of select=""SequenceNumeric/text()"" />
                  </SequenceNumeric>
                </xsl:if>
                <xsl:if test=""Amount"">
                  <Amount>
                    <xsl:value-of select=""Amount/text()"" />
                  </Amount>
                </xsl:if>
                <xsl:if test=""BaseAmount"">
                  <BaseAmount>
                    <xsl:value-of select=""BaseAmount/text()"" />
                  </BaseAmount>
                </xsl:if>
                <xsl:for-each select=""TaxCategory"">
                  <TaxCategory>
                    <xsl:if test=""ID"">
                      <ID>
                        <xsl:value-of select=""ID/text()"" />
                      </ID>
                    </xsl:if>
                    <xsl:if test=""Percent"">
                      <Percent>
                        <xsl:value-of select=""Percent/text()"" />
                      </Percent>
                    </xsl:if>
                    <xsl:if test=""PerUnitAmount"">
                      <PerUnitAmount>
                        <xsl:value-of select=""PerUnitAmount/text()"" />
                      </PerUnitAmount>
                    </xsl:if>
                    <xsl:for-each select=""TaxScheme"">
                      <TaxScheme>
                        <xsl:if test=""ID"">
                          <ID>
                            <xsl:value-of select=""ID/text()"" />
                          </ID>
                        </xsl:if>
                        <xsl:if test=""Navn"">
                          <Navn>
                            <xsl:value-of select=""Navn/text()"" />
                          </Navn>
                        </xsl:if>
                        <xsl:value-of select=""./text()"" />
                      </TaxScheme>
                    </xsl:for-each>
                    <xsl:value-of select=""./text()"" />
                  </TaxCategory>
                </xsl:for-each>
                <xsl:value-of select=""./text()"" />
              </AllowanceCharge>
            </xsl:for-each>
            <xsl:for-each select=""TaxTotal"">
              <TaxTotal>
                <xsl:if test=""TaxAmount"">
                  <TaxAmount>
                    <xsl:value-of select=""TaxAmount/text()"" />
                  </TaxAmount>
                </xsl:if>
                <xsl:for-each select=""TaxSubtotal"">
                  <TaxSubtotal>
                    <xsl:if test=""TaxAmount"">
                      <TaxAmount>
                        <xsl:value-of select=""TaxAmount/text()"" />
                      </TaxAmount>
                    </xsl:if>
                    <xsl:if test=""TaxableAmount"">
                      <TaxableAmount>
                        <xsl:value-of select=""TaxableAmount/text()"" />
                      </TaxableAmount>
                    </xsl:if>
                    <xsl:if test=""TaxPercent"">
                      <TaxPercent>
                        <xsl:value-of select=""TaxPercent/text()"" />
                      </TaxPercent>
                    </xsl:if>
                    <xsl:for-each select=""TaxCategory"">
                      <TaxCategory>
                        <xsl:if test=""ID"">
                          <ID>
                            <xsl:value-of select=""ID/text()"" />
                          </ID>
                        </xsl:if>
                        <xsl:if test=""Percent"">
                          <Percent>
                            <xsl:value-of select=""Percent/text()"" />
                          </Percent>
                        </xsl:if>
                        <xsl:for-each select=""TaxScheme"">
                          <TaxScheme>
                            <xsl:if test=""ID"">
                              <ID>
                                <xsl:value-of select=""ID/text()"" />
                              </ID>
                            </xsl:if>
                            <xsl:if test=""Navn"">
                              <Navn>
                                <xsl:value-of select=""Navn/text()"" />
                              </Navn>
                            </xsl:if>
                            <xsl:value-of select=""./text()"" />
                          </TaxScheme>
                        </xsl:for-each>
                        <xsl:value-of select=""./text()"" />
                      </TaxCategory>
                    </xsl:for-each>
                    <xsl:value-of select=""./text()"" />
                  </TaxSubtotal>
                </xsl:for-each>
                <xsl:value-of select=""./text()"" />
              </TaxTotal>
            </xsl:for-each>
            <xsl:for-each select=""Period"">
              <Period>
                <xsl:if test=""ID"">
                  <ID>
                    <xsl:value-of select=""ID/text()"" />
                  </ID>
                </xsl:if>
                <xsl:if test=""FromDate"">
                  <FromDate>
                    <xsl:value-of select=""FromDate/text()"" />
                  </FromDate>
                </xsl:if>
                <xsl:if test=""ToDate"">
                  <ToDate>
                    <xsl:value-of select=""ToDate/text()"" />
                  </ToDate>
                </xsl:if>
                <xsl:value-of select=""./text()"" />
              </Period>
            </xsl:for-each>
            <xsl:value-of select=""./text()"" />
          </Line>
        </xsl:for-each>
        <xsl:value-of select=""Lines/text()"" />
      </Lines>
    </ns0:Invoice>
  </xsl:template>
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"BYGE.Integration.Core.Invoice.Invoice";
        
        private const global::BYGE.Integration.Core.Invoice.Invoice _srcSchemaTypeReference0 = null;
        
        private const string _strTrgSchemasList0 = @"BYGE.Integration.Core.Invoice.Invoice";
        
        private const global::BYGE.Integration.Core.Invoice.Invoice _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [1];
                _SrcSchemas[0] = @"BYGE.Integration.Core.Invoice.Invoice";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"BYGE.Integration.Core.Invoice.Invoice";
                return _TrgSchemas;
            }
        }
    }
}
