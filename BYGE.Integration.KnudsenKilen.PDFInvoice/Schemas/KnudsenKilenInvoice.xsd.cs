namespace BYGE.Integration.KnudsenKilen.PDFInvoice.Schemas {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [Schema(@"http://www.nuance.com/2011/Zones",@"Page")]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"Page"})]
    public sealed class KnudsenKilenInvoice : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xs:schema xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" attributeFormDefault=""unqualified"" elementFormDefault=""qualified"" targetNamespace=""http://www.nuance.com/2011/Zones"" xmlns:xs=""http://www.w3.org/2001/XMLSchema"">
  <xs:element name=""Page"">
    <xs:complexType>
      <xs:sequence>
        <xs:element minOccurs=""1"" maxOccurs=""unbounded"" name=""Zones"">
          <xs:complexType>
            <xs:sequence>
              <xs:element maxOccurs=""unbounded"" name=""Zone"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" name=""Table"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name=""Header"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element maxOccurs=""unbounded"" name=""Cell"" type=""xs:string"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element maxOccurs=""unbounded"" name=""Row"">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element maxOccurs=""unbounded"" name=""Cell"" type=""xs:string"" />
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                        <xs:attribute name=""RowCount"" type=""xs:unsignedByte"" use=""required"" />
                        <xs:attribute name=""ColumnCount"" type=""xs:unsignedByte"" use=""required"" />
                      </xs:complexType>
                    </xs:element>
                    <xs:element name=""BoundingRect"">
                      <xs:complexType>
                        <xs:attribute name=""Left"" type=""xs:unsignedShort"" use=""required"" />
                        <xs:attribute name=""Right"" type=""xs:unsignedShort"" use=""required"" />
                        <xs:attribute name=""Top"" type=""xs:unsignedShort"" use=""required"" />
                        <xs:attribute name=""Bottom"" type=""xs:unsignedShort"" use=""required"" />
                      </xs:complexType>
                    </xs:element>
                    <xs:element name=""Shape"">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element maxOccurs=""unbounded"" name=""Point"">
                            <xs:complexType>
                              <xs:attribute name=""X"" type=""xs:unsignedShort"" use=""required"" />
                              <xs:attribute name=""Y"" type=""xs:unsignedShort"" use=""required"" />
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                  <xs:attribute name=""Id"" type=""xs:string"" use=""required"" />
                  <xs:attribute name=""Type"" type=""xs:string"" use=""required"" />
                  <xs:attribute name=""Value"" type=""xs:string"" use=""required"" />
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
      <xs:attribute name=""PageId"" type=""xs:unsignedByte"" use=""required"" />
      <xs:attribute name=""DocumentId"" type=""xs:unsignedByte"" use=""required"" />
      <xs:attribute name=""MatchingTemplate"" type=""xs:string"" use=""required"" />
      <xs:attribute name=""Confidence"" type=""xs:unsignedByte"" use=""required"" />
    </xs:complexType>
  </xs:element>
</xs:schema>";
        
        public KnudsenKilenInvoice() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [1];
                _RootElements[0] = "Page";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
    }
}
