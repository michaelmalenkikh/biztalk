namespace BYGE.Integration.KnudsenKilen.PDFInvoice.Maps {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.KnudsenKilen.PDFInvoice.Schemas.KnudsenKilenInvoice", typeof(global::BYGE.Integration.KnudsenKilen.PDFInvoice.Schemas.KnudsenKilenInvoice))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"BYGE.Integration.Core.Invoice.Invoice", typeof(global::BYGE.Integration.Core.Invoice.Invoice))]
    public sealed class KnudsenKilenInvoice_to_CoreInvoice : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var s0 userCSharp"" version=""1.0"" xmlns:ns0=""http://byg-e.dk/schemas/v10"" xmlns:s0=""http://www.nuance.com/2011/Zones"" xmlns:userCSharp=""http://schemas.microsoft.com/BizTalk/2003/userCSharp"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/s0:Page"" />
  </xsl:template>
  <xsl:template match=""/s0:Page"">
    <xsl:variable name=""var:v1"" select=""userCSharp:LogicalEq(string(s0:Zones/s0:Zone/@Id) , &quot;InvoiceID&quot;)"" />
    <ns0:Invoice>
      <xsl:for-each select=""s0:Zones/s0:Zone"">
        <xsl:choose>
          <xsl:when test=""@Id = 'InvoiceID'"">
            <InvoiceID>
              <xsl:value-of select=""@Value"" />
            </InvoiceID>
          </xsl:when>
          <xsl:when test=""@Id = 'InvoiceType'"">
            <InvoiceType>
               <xsl:choose>
                <xsl:when test=""contains(normalize-space(@Value), 'aktura')"">
                  <xsl:text>380</xsl:text>
                </xsl:when>
                <xsl:when test=""contains(normalize-space(@Value), 'redit')"">
                  <xsl:text>381</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:text>380</xsl:text>
                </xsl:otherwise>
              </xsl:choose> 
            </InvoiceType>
          </xsl:when>
          <xsl:when test=""@Id = 'IssueDate'"">
            <IssueDate>
              <xsl:value-of select=""concat(substring(normalize-space(@Value), 7, 4), '-', substring(normalize-space(@Value), 4, 2), '-', substring(normalize-space(@Value), 1, 2))"" />
            </IssueDate>
          </xsl:when>
          <xsl:when test=""@Id = 'PaymentTerms'"">
            <PaymentTerms>
              <Note>
                <xsl:value-of select=""@Value"" />
              </Note>
            </PaymentTerms>
          </xsl:when>
          <xsl:when test=""@Id = 'PaymentDueDate'"">
            <PaymentMeans>
              <PaymentDueDate>
                <xsl:value-of select=""concat(substring(normalize-space(@Value), 7, 4), '-', substring(normalize-space(@Value), 4, 2), '-', substring(normalize-space(@Value), 1, 2))"" />
              </PaymentDueDate>
            </PaymentMeans>
          </xsl:when>          
        </xsl:choose>
      </xsl:for-each>

      <!--  Order Reference Segment -->
      <OrderReference>
        <xsl:for-each select=""s0:Zones/s0:Zone"">
          <xsl:choose>
            <xsl:when test=""@Id = 'OrderID'"">
              <OrderID>
                <xsl:value-of select=""@Value"" />
              </OrderID>
            </xsl:when>
            <xsl:when test=""@Id = 'CustomerID'"">
              <CustomerReference>
                <xsl:value-of select=""@Value"" />
              </CustomerReference>
            </xsl:when>
          </xsl:choose>
        </xsl:for-each>
      </OrderReference>
      <!-- End  of  Order Reference -->   
      
      <!-- AccountingCustomer -->
      <AccountingCustomer>        
       <xsl:for-each select=""s0:Zones/s0:Zone"">
         <xsl:choose>
           <xsl:when test=""@Id = 'IVGLN'"">
             <AccBuyerID>
               <xsl:if test=""substring(@Value, 1, 2) = 'F:'"">
                <xsl:value-of select=""substring(@Value, 3)"" />
               </xsl:if>                  
               <xsl:if test=""substring(@Value, 1, 2) != 'F:'"">
                <xsl:value-of select=""@Value"" />
               </xsl:if>
             </AccBuyerID>
             <PartyID>
               <xsl:if test=""substring(@Value, 1, 2) = 'F:'"">
                <xsl:value-of select=""substring(@Value, 3)"" />
               </xsl:if>                  
               <xsl:if test=""substring(@Value, 1, 2) != 'F:'"">
                <xsl:value-of select=""@Value"" />
               </xsl:if>
             </PartyID>           
           </xsl:when>
         </xsl:choose>        
       </xsl:for-each>  
      </AccountingCustomer>
      <!-- End of AccountingCustomer  -->
      
      <!--  BuyerCustomer -->
      <BuyerCustomer>
        <xsl:for-each select=""s0:Zones/s0:Zone"">
          <xsl:choose>
           <xsl:when test=""@Id = 'BuyerFax'"">
             <xsl:if test=""substring(@Value, 1, 2) = 'B:'"">
              <BuyerID>
               <xsl:value-of select=""substring(@Value, 3)"" />
              </BuyerID>
              <PartyID>
               <xsl:value-of select=""substring(@Value, 3)"" />
              </PartyID>
             </xsl:if>
             <xsl:if test=""substring(@Value, 1, 2) != 'B:'"">
               <xsl:if test=""string-length(//s0:Zones/s0:Zone[@Id = 'IVGLN']/@Value) != 0"">
                 <xsl:if test=""substring(//s0:Zones/s0:Zone[@Id = 'IVGLN']/@Value, 1, 2) != 'F:'"">
                  <BuyerID>
                   <xsl:value-of select=""//s0:Zones/s0:Zone[@Id = 'IVGLN']/@Value"" />
                  </BuyerID>
                  <PartyID>
                   <xsl:value-of select=""//s0:Zones/s0:Zone[@Id = 'IVGLN']/@Value"" />
                  </PartyID>                 
                 </xsl:if>               
                 <xsl:if test=""substring(//s0:Zones/s0:Zone[@Id = 'IVGLN']/@Value, 1, 2) = 'F:'"">
                  <BuyerID>
                   <xsl:value-of select=""substring(//s0:Zones/s0:Zone[@Id = 'IVGLN']/@Value, 3)"" />
                  </BuyerID>
                  <PartyID>
                   <xsl:value-of select=""substring(//s0:Zones/s0:Zone[@Id = 'IVGLN']/@Value, 3)"" />
                  </PartyID>                 
                 </xsl:if>   
               </xsl:if>             
             </xsl:if>                  
            </xsl:when>
           </xsl:choose>
          </xsl:for-each>            
        <Address>
         <xsl:for-each select=""s0:Zones/s0:Zone"">
          <xsl:choose>
           <xsl:when test=""@Id = 'BuyerAddress1'"">
            <Name>
             <xsl:value-of select=""@Value"" />
            </Name>
           </xsl:when>
            <xsl:when test=""@Id = 'BuyerAddress2'"">
             <xsl:if test=""not(contains(normalize-space(@Value), 'everan'))"">
              <Street>
                <xsl:value-of select=""@Value"" />
              </Street>
             </xsl:if>   
            </xsl:when>
 <!--            <xsl:when test=""@Id = 'BuyerStreetNumber'"">
              <Number>
                <xsl:value-of select=""@Value"" />
              </Number>
            </xsl:when> 
 -->            
 <!--       <xsl:when test=""@Id = 'BuyerCityName'"">
             <City>
              <xsl:value-of select=""@Value"" />
             </City>
            </xsl:when> 
            <xsl:when test=""@Id = 'BuyerPostalCode'"">
             <PostalCode>
              <xsl:value-of select=""@Value"" />
             </PostalCode>
            </xsl:when>
 -->            
         <xsl:when test=""@Id = 'BuyerAddress3'"">          
           <xsl:if test=""string(number(substring(@Value, 1, 4))) != 'NaN'"">
             <City>
               <xsl:value-of select=""normalize-space(substring(@Value, 5))"" />
             </City>
             <PostalCode>
               <xsl:value-of select=""substring(@Value, 1, 4)"" />
             </PostalCode>
           </xsl:if>
           <xsl:if test=""string(number(substring(@Value, 1, 4))) = 'NaN'"">           
               <xsl:if test=""not(contains(normalize-space(@Value), 'everan'))"">
                 <xsl:if test=""contains(normalize-space(//s0:Zones/s0:Zone[@Id = 'BuyerAddress2']/@Value), 'everan')"">
                 <Street>
                  <xsl:value-of select=""@Value"" /> 
                 </Street>               
               </xsl:if>            
              </xsl:if>  
             </xsl:if>           
         </xsl:when>
            <xsl:when test=""@Id = 'BuyerAddress4'"">
              <xsl:if test=""string(number(substring(@Value, 1, 4))) != 'NaN'"">
                <xsl:if test=""string(number(substring(normalize-space(//s0:Zones/s0:Zone[@Id = 'BuyerAddress3']/@Value), 1, 4))) = 'NaN'"">
                 <City>
                  <xsl:value-of select=""normalize-space(substring(@Value, 5))"" />
                 </City>
                 <PostalCode>
                  <xsl:value-of select=""substring(@Value, 1, 4)"" />
                 </PostalCode>                
                </xsl:if> 
              </xsl:if>
            </xsl:when> 
             
          </xsl:choose>
         </xsl:for-each>  
        </Address>
        <Concact>
         <xsl:for-each select=""s0:Zones/s0:Zone"">
          <xsl:choose>
           <xsl:when test=""@Id = 'BuyerPhone'"">
             <Telephone>
             <xsl:value-of select=""@Value"" />
             </Telephone>
            </xsl:when>
            <xsl:when test=""@Id = 'BuyerFax'"">
             <xsl:if test=""substring(@Value, 1, 2) != 'B:'"">
              <Telefax>
               <xsl:value-of select=""@Value"" />
              </Telefax>
             </xsl:if>    
            </xsl:when>
           </xsl:choose>
          </xsl:for-each>
        </Concact>
      </BuyerCustomer>
      <!--  End of  BuyerCustomer -->

      <!--  DeliveryLocation  -->
      <DeliveryLocation>
        <Address>
         <xsl:for-each select=""s0:Zones/s0:Zone"">
          <xsl:choose>
           <xsl:when test=""@Id = 'DeliveryAddress1'"">
            <Name>
             <xsl:value-of select=""@Value"" />
            </Name>
           </xsl:when> 
           <xsl:when test=""@Id = 'DeliveryAddress2'"">
            <Street>
             <xsl:value-of select=""@Value"" />
            </Street>
           </xsl:when> 
            
           <xsl:when test=""@Id = 'DeliveryAddress3'"">
            <xsl:if test=""string(number(substring(@Value, 1, 4))) != 'NaN'"">
             <City>
               <xsl:value-of select=""normalize-space(substring(@Value, 5))"" />
             </City>
             <PostalCode>
               <xsl:value-of select=""substring(@Value, 1, 4)"" />
             </PostalCode>
           </xsl:if>
          </xsl:when> 
            
         <xsl:when test=""@Id = 'FillText'"">
            <xsl:if test=""string(number(substring(@Value, 1, 4))) != 'NaN'"">
              <xsl:if test=""string(number(substring(normalize-space(//s0:Zones/s0:Zone[@Id = 'DeliveryAddress3']/@Value), 1, 4))) = 'NaN'""> 
              <City>
                <xsl:value-of select=""normalize-space(substring(@Value, 5))"" />
              </City>
              <PostalCode>
               <xsl:value-of select=""substring(@Value, 1, 4)"" />
              </PostalCode>
             </xsl:if> 
           </xsl:if>
          </xsl:when>    
            
<!--           <xsl:when test=""@Id = 'DeliveryStreetNumber'"">
            <Number>
             <xsl:value-of select=""@Value"" />
            </Number>
           </xsl:when> 
           <xsl:when test=""@Id = 'DeliveryPostalCode'"">
             <PostalCode>
             <xsl:value-of select=""@Value"" />
            </PostalCode>
           </xsl:when>  
           <xsl:when test=""@Id = 'DeliveryCityName'"">
            <City>
             <xsl:value-of select=""@Value"" />
            </City> 
           </xsl:when> -->
          </xsl:choose>
         </xsl:for-each>  
        </Address> 
      </DeliveryLocation>
      <!--  End of  DeliveryLocation  -->

      <!--  Hardcoding  KnudsenKilen  data  -->
      <AccountingSupplier>
        <AccSellerID>
          <xsl:text>5790001331528</xsl:text>
        </AccSellerID>
        <PartyID>
          <xsl:text>5790001331528</xsl:text>
        </PartyID>
        <CompanyID>
          <xsl:text>DK87432815</xsl:text>
        </CompanyID>
        <SchemeID>
          <Endpoint>
            <xsl:text>GLN</xsl:text>
          </Endpoint>
          <Party>
            <xsl:text>GLN</xsl:text>  
          </Party>
        </SchemeID>
        <Address>
         <Name>
           <xsl:text>Knudsen Kilen A/S</xsl:text>          
         </Name>
         <Street>
           <xsl:text>Industrivej</xsl:text> 
        </Street>
         <Number>
           <xsl:text>21</xsl:text>    
         </Number>
         <City>
           <xsl:text>Frederiksværk</xsl:text>   
         </City>
         <PostalCode>
           <xsl:text>3300</xsl:text>          
         </PostalCode>
        </Address>
        <Contact>
          <Telephone>
            <xsl:text>45 4776 0101</xsl:text> 
          </Telephone>
          <Email>
            <xsl:text>info@knudsen-kilen.dk</xsl:text> 
          </Email>
        </Contact>
      </AccountingSupplier>
      <!--  End of  KnudsenKilen  data  -->
      
      <!--  Totals  Segment -->
      <Total>
        <xsl:for-each select=""s0:Zones/s0:Zone"">
          <xsl:choose>
            <xsl:when test=""@Id = 'LineTotalAmount'"">
              <LineTotalAmount>
                <xsl:value-of select=""translate(translate(translate(@Value, '.', ''), ',', '.'), '-', '')"" />
              </LineTotalAmount>
            </xsl:when>
            <xsl:when test=""@Id = 'TaxableAmount'"">
              <TaxExclAmount>
                <xsl:value-of select=""translate(translate(translate(@Value, '.', ''), ',', '.'), '-', '')"" />
              </TaxExclAmount>
            </xsl:when>
            <xsl:when test=""@Id = 'TaxAmount'"">
              <TaxAmount>
                <xsl:value-of select=""translate(translate(translate(@Value, '.', ''), ',', '.'), '-', '')"" />
              </TaxAmount>
            </xsl:when> 
            <xsl:when test=""@Id = 'TotalAmount'"">
              <PayableAmount>
                <xsl:value-of select=""translate(translate(translate(@Value, '.', ''), ',', '.'), '-', '')"" />
              </PayableAmount>
            </xsl:when>
          </xsl:choose>
        </xsl:for-each> 
      </Total>
      <!--  End of  Totals  -->

      <!--  TAX Segment -->
      <TaxTotal>
        <xsl:for-each select=""s0:Zones/s0:Zone"">
          <xsl:choose>
            <xsl:when test=""@Id = 'TaxAmount'"">
              <TaxAmount>
                <xsl:value-of select=""translate(translate(translate(@Value, '.', ''), ',', '.'), '-', '')"" />
              </TaxAmount>
              <TaxSubtotal>
               <TaxAmount>
                <xsl:value-of select=""translate(translate(translate(@Value, '.', ''), ',', '.'), '-', '')"" />
               </TaxAmount>
              </TaxSubtotal>
            </xsl:when>
          </xsl:choose>
        </xsl:for-each>
      </TaxTotal>      
      <!--  End of TAX  -->

      <!--  InvoiceLine Segment -->
      <Lines>
        <xsl:for-each select=""s0:Zones/s0:Zone"">
          <xsl:choose>
            <xsl:when test=""@Id = 'Table'"">
              <xsl:for-each select=""s0:Table/s0:Row"">
               <!-- Cell  2 as  a  key cell  --> 
                <xsl:if test=""(string-length(normalize-space(s0:Cell[1])) = 9 or string-length(normalize-space(s0:Cell[1])) = 8 or string-length(normalize-space(s0:Cell[1])) = 11 or string-length(normalize-space(s0:Cell[1])) = 10) and (string(number(substring(normalize-space(s0:Cell[1]), 1, 1))) != 'NaN')"">
                  <xsl:variable name = ""RowPos"">
                   <xsl:value-of select=""position()"" />
                  </xsl:variable>
                  <Line>
                   <LineNo>
                     <xsl:value-of select=""$RowPos""/>
                   </LineNo> 
                   <Quantity>
                    <xsl:value-of select=""normalize-space(translate(substring-before(s0:Cell[3], ' '), '-', ''))"" />
                   </Quantity>
                   <UnitCode>
                    <xsl:value-of select=""normalize-space(substring-after(s0:Cell[3], ' '))"" />
                   </UnitCode>
                   <Item>
                    <SellerItemID>
                     <xsl:value-of select=""s0:Cell[1]"" />
                    </SellerItemID>
                    <Name>
                     <xsl:value-of select=""s0:Cell[2]"" />
                    </Name>

                     <xsl:if test=""contains(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+2)]/s0:Cell[2]), 'Tun')"">
                       <AdditionalItemID>
                         <xsl:value-of select=""substring(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+2)]/s0:Cell[2]), number(string-length(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+2)]/s0:Cell[2])) - 6), 7)"" />
                       </AdditionalItemID>
                     </xsl:if>
                     <xsl:if test=""contains(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+3)]/s0:Cell[2]), 'Tun')"">
                       <AdditionalItemID>
                         <xsl:value-of select=""substring(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+3)]/s0:Cell[2]), number(string-length(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+3)]/s0:Cell[2])) - 6), 7)"" />
                       </AdditionalItemID>
                     </xsl:if>
                    <xsl:if test=""contains(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+4)]/s0:Cell[2]), 'Tun')"">
                      <AdditionalItemID>
                        <xsl:value-of select=""substring(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+4)]/s0:Cell[2]), number(string-length(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+4)]/s0:Cell[2])) - 6), 7)"" />
                      </AdditionalItemID>
                    </xsl:if>
                    <xsl:if test=""contains(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+5)]/s0:Cell[2]), 'Tun')"">
                      <xsl:if test=""not(contains(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+2)]/s0:Cell[2]), 'Tun'))"">
                       <AdditionalItemID>
                        <xsl:value-of select=""substring(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+5)]/s0:Cell[2]), number(string-length(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+5)]/s0:Cell[2])) - 6), 7)"" />
                       </AdditionalItemID>
                      </xsl:if>
                    </xsl:if> 
                                          
                     <xsl:if test=""contains(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+3)]/s0:Cell[2]), 'EAN')"">
                      <StandardItemID>
                        <xsl:value-of select=""substring(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+3)]/s0:Cell[2]), number(string-length(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+3)]/s0:Cell[2])) - 12), 13)"" /> 
                      </StandardItemID>
                    </xsl:if>                     
                     <xsl:if test=""contains(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+4)]/s0:Cell[2]), 'EAN')"">
                      <StandardItemID>
                        <xsl:value-of select=""substring(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+4)]/s0:Cell[2]), number(string-length(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+4)]/s0:Cell[2])) - 12), 13)"" /> 
                      </StandardItemID>
                    </xsl:if> 
                    <xsl:if test=""contains(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+5)]/s0:Cell[2]), 'EAN')"">
                      <StandardItemID>
                        <xsl:value-of select=""substring(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+5)]/s0:Cell[2]), number(string-length(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+5)]/s0:Cell[2])) - 12), 13)"" /> 
                      </StandardItemID>
                    </xsl:if> 
                    <xsl:if test=""contains(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+6)]/s0:Cell[2]), 'EAN')"">
                      <StandardItemID>
                        <xsl:value-of select=""substring(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+6)]/s0:Cell[2]), number(string-length(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+6)]/s0:Cell[2])) - 12), 13)"" /> 
                      </StandardItemID>                      
                    </xsl:if>
                    <xsl:if test=""contains(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+7)]/s0:Cell[2]), 'EAN')"">
                      <xsl:if test=""not(contains(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+3)]/s0:Cell[2]), 'EAN'))"">
                       <StandardItemID>
                         <xsl:value-of select=""substring(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+7)]/s0:Cell[2]), number(string-length(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+7)]/s0:Cell[2])) - 12), 13)"" /> 
                       </StandardItemID>                      
                      </xsl:if>
                    </xsl:if>                        
                                          
                    <xsl:if test=""contains(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+1)]/s0:Cell[2]), 'Tarifnummer')"">
                     <CatalogueItemID>
                      <xsl:value-of select=""substring(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+1)]/s0:Cell[2]), number(string-length(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+1)]/s0:Cell[2])) - 7), 8)"" /> 
                     </CatalogueItemID>
                    </xsl:if>
                   </Item>
                    
                  <PriceGross>
                    <Price>
                      <xsl:value-of select=""translate(translate(translate(s0:Cell[4], '.', ''), ',', '.'), '-', '')"" />
                    </Price>
                  </PriceGross>
                  <LineAmountTotal>
                    <xsl:value-of select=""translate(translate(translate(s0:Cell[6], '.', ''), ',', '.'), '-', '')"" /> 
                  </LineAmountTotal>

                    <xsl:if test=""contains(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+2)]/s0:Cell[2]), 'rabat')"">
                      <AllowanceCharge>
                        <ChargeIndicator>
                          <xsl:text>False</xsl:text>
                        </ChargeIndicator>
                         <xsl:if test=""string-length(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+2)]/s0:Cell[3]) != 0"">
                           <MultiplierFactorNumeric>
                            <xsl:value-of select=""number(translate(translate(translate(translate(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+2)]/s0:Cell[3]), ' ', ''), '%', ''), '.', ''), ',', '.')) div 100"" />
                           </MultiplierFactorNumeric>  
                         </xsl:if>
                <!--        <xsl:if test=""string(number(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+2)]/s0:Cell[3])) = 'NaN'"">
                          <MultiplierFactorNumeric>
                            <xsl:value-of select=""number(substring-before(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+2)]/s0:Cell[3]), ' ')) div 100"" />
                          </MultiplierFactorNumeric>
                        </xsl:if>  
                        <xsl:if test=""string(number(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+2)]/s0:Cell[3])) != 'NaN'"">
                          <MultiplierFactorNumeric>
                            <xsl:value-of select=""number(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+2)]/s0:Cell[3])) div 100"" />
                          </MultiplierFactorNumeric>
                        </xsl:if>                         -->
                       <xsl:if test =""string-length(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+2)]/s0:Cell[6]) != 0"">
                        <Amount>
                          <xsl:value-of select=""translate(translate(translate(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+2)]/s0:Cell[6]), '.', ''), ',', '.'), '-', '')"" />
                        </Amount>
                       </xsl:if>
                         <xsl:if test =""string-length(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+2)]/s0:Cell[6]) = 0"">
                          <xsl:if test =""string-length(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+3)]/s0:Cell[6]) != 0"">
                           <Amount>
                             <xsl:value-of select=""translate(translate(translate(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+3)]/s0:Cell[6]), '.', ''), ',', '.'), '-', '')"" />
                           </Amount>
                          </xsl:if>                        
                        </xsl:if>                           
                      </AllowanceCharge>
                    </xsl:if> 
                    
                    <xsl:if test=""contains(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+3)]/s0:Cell[2]), 'rabat')"">
                      <AllowanceCharge>
                        <ChargeIndicator>
                          <xsl:text>False</xsl:text>
                        </ChargeIndicator>
                         <xsl:if test=""string-length(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+3)]/s0:Cell[3]) != 0"">
                           <MultiplierFactorNumeric>
                            <xsl:value-of select=""number(translate(translate(translate(translate(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+3)]/s0:Cell[3]), ' ', ''), '%', ''), '.', ''), ',', '.')) div 100"" />                                            
                           </MultiplierFactorNumeric>  
                         </xsl:if>                        
                  <!--     <xsl:if test=""string(number(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+3)]/s0:Cell[3])) = 'NaN'"">
                        <MultiplierFactorNumeric>
                         <xsl:value-of select=""number(substring-before(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+3)]/s0:Cell[3]), ' ')) div 100"" />
                        </MultiplierFactorNumeric>
                       </xsl:if>   
                       <xsl:if test=""string(number(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+3)]/s0:Cell[3])) != 'NaN'"">                       
                        <MultiplierFactorNumeric>
                        <xsl:value-of select=""number(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+3)]/s0:Cell[3])) div 100"" />                         
                        </MultiplierFactorNumeric>
                        </xsl:if>                        -->
                       <xsl:if test =""string-length(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+3)]/s0:Cell[6]) != 0"">
                          <Amount>
                            <xsl:value-of select=""translate(translate(translate(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+3)]/s0:Cell[6]), '.', ''), ',', '.'), '-', '')"" /> 
                          </Amount>                        
                       </xsl:if> 
                        <xsl:if test =""string-length(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+3)]/s0:Cell[6]) = 0"">
                         <xsl:if test =""string-length(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+4)]/s0:Cell[6]) != 0"">
                          <Amount>
                            <xsl:value-of select=""translate(translate(translate(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+4)]/s0:Cell[6]), '.', ''), ',', '.'), '-', '')"" />
                          </Amount>
                         </xsl:if> 
                        </xsl:if>                        
                      </AllowanceCharge>
                    </xsl:if>
                   <xsl:if test=""contains(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+4)]/s0:Cell[2]), 'rabat')"">
                      <AllowanceCharge>
                        <ChargeIndicator>
                          <xsl:text>False</xsl:text>
                        </ChargeIndicator>
                         <xsl:if test=""string-length(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+4)]/s0:Cell[3]) != 0"">
                           <MultiplierFactorNumeric>
                            <xsl:value-of select=""number(translate(translate(translate(translate(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+4)]/s0:Cell[3]), ' ', ''), '%', ''), '.', ''), ',', '.')) div 100"" />
                           </MultiplierFactorNumeric>  
                         </xsl:if>                        
                   <!--     <xsl:if test=""string(number(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+4)]/s0:Cell[3])) = 'NaN'"">
                          <MultiplierFactorNumeric>
                            <xsl:value-of select=""number(substring-before(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+4)]/s0:Cell[3]), ' ')) div 100"" />
                          </MultiplierFactorNumeric>
                        </xsl:if>  
                        <xsl:if test=""string(number(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+4)]/s0:Cell[3])) != 'NaN'"">
                          <MultiplierFactorNumeric>
                            <xsl:value-of select=""number(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+4)]/s0:Cell[3])) div 100"" />
                          </MultiplierFactorNumeric>
                        </xsl:if>                        -->
                        <xsl:if test =""string-length(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+4)]/s0:Cell[6]) != 0"">
                          <Amount>
                            <xsl:value-of select=""translate(translate(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+4)]/s0:Cell[6]), '.', ''), ',', '.')"" />
                          </Amount>
                        </xsl:if>
                        <xsl:if test =""string-length(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+4)]/s0:Cell[6]) = 0"">
                          <xsl:if test =""string-length(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+5)]/s0:Cell[6]) != 0"">
                            <Amount>
                              <xsl:value-of select=""translate(translate(normalize-space(//s0:Zones/s0:Zone/s0:Table/s0:Row[number($RowPos+5)]/s0:Cell[6]), '.', ''), ',', '.')"" />
                            </Amount>
                          </xsl:if>
                        </xsl:if>   
                      </AllowanceCharge>
                    </xsl:if>
                  </Line>       
                </xsl:if>

              </xsl:for-each>
            </xsl:when>
          </xsl:choose>
        </xsl:for-each>
      </Lines>
      <!--  End of  InvoiceLine -->

    </ns0:Invoice>
  </xsl:template>


  <msxsl:script language=""C#"" implements-prefix=""userCSharp"">
    <![CDATA[
public bool LogicalEq(string val1, string val2)
{
	bool ret = false;
	double d1 = 0;
	double d2 = 0;
	if (IsNumeric(val1, ref d1) && IsNumeric(val2, ref d2))
	{
		ret = d1 == d2;
	}
	else
	{
		ret = String.Compare(val1, val2, StringComparison.Ordinal) == 0;
	}
	return ret;
}


public bool IsNumeric(string val)
{
	if (val == null)
	{
		return false;
	}
	double d = 0;
	return Double.TryParse(val, System.Globalization.NumberStyles.AllowThousands | System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out d);
}

public bool IsNumeric(string val, ref double d)
{
	if (val == null)
	{
		return false;
	}
	return Double.TryParse(val, System.Globalization.NumberStyles.AllowThousands | System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out d);
}


]]>
  </msxsl:script>

</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"BYGE.Integration.KnudsenKilen.PDFInvoice.Schemas.KnudsenKilenInvoice";
        
        private const global::BYGE.Integration.KnudsenKilen.PDFInvoice.Schemas.KnudsenKilenInvoice _srcSchemaTypeReference0 = null;
        
        private const string _strTrgSchemasList0 = @"BYGE.Integration.Core.Invoice.Invoice";
        
        private const global::BYGE.Integration.Core.Invoice.Invoice _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [1];
                _SrcSchemas[0] = @"BYGE.Integration.KnudsenKilen.PDFInvoice.Schemas.KnudsenKilenInvoice";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"BYGE.Integration.Core.Invoice.Invoice";
                return _TrgSchemas;
            }
        }
    }
}
